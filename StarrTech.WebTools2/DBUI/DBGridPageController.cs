using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Paging buttons and status-message for the DBUI AdminDataGrid.
	/// </summary>
	public class DBGridPageController : Table, INamingContainer {
		
		#region Properties 
		/// <summary>
		/// Reference to the AdminDataGrid we're doing the grid-page controls for.
		/// </summary>
		protected AdminDataGrid parent;		
		
		/// <summary>
		/// Label control for displaying the paging-status messages.
		/// </summary>
		public Label StatusMessageLabel {
			get {
				this.EnsureChildControls();
				return ( this.statusMessageLabel );
			}
		}
		private Label statusMessageLabel = new Label();
		
		/// <summary>
		/// Exposes the Text property of the StatusMessageLabel.
		/// </summary>
		public string StatusMessage {
			get { return ( StatusMessageLabel.Text ); }
			set { StatusMessageLabel.Text = value; }
		}
		
		/// <summary>
		/// Button to display the first page of records in the associated grid.
		/// </summary>
		public StarrTech.WebTools.Controls.NavButton FirstButton {
			get {
				this.EnsureChildControls();
				return ( this.firstButton );
			}
		}
		private StarrTech.WebTools.Controls.NavButton firstButton;
		
		/// <summary>
		/// Button to display the previous page of records in the associated grid.
		/// </summary>
		public StarrTech.WebTools.Controls.NavButton PrevButton {
			get {
				this.EnsureChildControls();
				return ( this.prevButton );
			}
		}
		private StarrTech.WebTools.Controls.NavButton prevButton;
		
		/// <summary>
		/// Button to display the next page of records in the associated grid.
		/// </summary>
		public StarrTech.WebTools.Controls.NavButton NextButton {
			get {
				this.EnsureChildControls();
				return ( this.nextButton );
			}
		}
		private StarrTech.WebTools.Controls.NavButton nextButton;
		
		/// <summary>
		/// Button to display the last page of records in the associated grid.
		/// </summary>
		public StarrTech.WebTools.Controls.NavButton LastButton {
			get {
				this.EnsureChildControls();
				return ( this.lastButton );
			}
		}
		private StarrTech.WebTools.Controls.NavButton lastButton;
		
		/// <summary>
		/// Button to trigger the DownloadData method of the associated grid.
		/// </summary>
		public System.Web.UI.WebControls.Button DownloadButton {
			get {
				this.EnsureChildControls();
				return ( this.downloadButton );
			}
		}
		private System.Web.UI.WebControls.Button downloadButton;
		
		///// <summary>
		///// Drop-down list to allow the user to select a specific page of records
		///// </summary>
		//public DropDownList PagePicker {
		//	get {
		//		this.EnsureChildControls();
		//		return ( this.pagePicker );
		//	}
		//}
		//private DropDownList pagePicker;
		
		/// <summary>
		/// Flag to show/hide the Download button.
		/// </summary>
		public bool IncludeDownloadButton {
			get {
				object obj = ViewState["IncludeDownloadButton"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["IncludeDownloadButton"] = value; }
		}
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion
		
		#region Events 
		/// <summary>
		/// Event raised when any of the button controls are clicked.
		/// </summary>
		public event CommandEventHandler ButtonClick {
			add { Events.AddHandler( ButtonClickEventKey, value ); }
			remove { Events.RemoveHandler( ButtonClickEventKey, value ); }
		}
		protected virtual void OnButtonClick( Object sender, CommandEventArgs args ) {
			CommandEventHandler handler = (CommandEventHandler) Events[ButtonClickEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object ButtonClickEventKey = new object();
		
		///// <summary>
		///// Event raised when a page is selected from the page-picker.
		///// </summary>
		//public event EventHandler PageSelected {
		//	add { Events.AddHandler( PageSelectedEventKey, value ); }
		//	remove { Events.RemoveHandler( PageSelectedEventKey, value ); }
		//}
		//protected virtual void OnPageSelected( Object sender, EventArgs args ) {
		//	EventHandler handler = (EventHandler) Events[PageSelectedEventKey];
		//	if ( handler != null ) handler( sender, args );
		//}
		//private static readonly object PageSelectedEventKey = new object();
		
		#endregion
		
		#region Constructor 
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="Parent">Reference to the AdminDataGrid we're controlling.</param>
		public DBGridPageController( AdminDataGrid Parent ) {
			this.parent = Parent;
			this.CellSpacing = 3;
			this.CellPadding = 0;
			this.BorderStyle = BorderStyle.None;
			this.Width = new Unit( "100%" );
			this.Height = new Unit( "24px" );
		}
		
		#endregion
		
		#region Protected Methods 
		
		protected override void CreateChildControls() {
			this.Rows.Clear();
			TableRow row = new TableRow();
			// Build the status-message cell
			TableCell cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Left; 
			cell.VerticalAlign = VerticalAlign.Middle;
			if ( this.parent.Header3CssClass != "" ) {
				this.StatusMessageLabel.CssClass = this.parent.Header3CssClass;
			}
			else {
				this.StatusMessageLabel.Font.Name = "Verdana";
				this.StatusMessageLabel.Font.Size = 8;
				this.StatusMessageLabel.Font.Italic = true;
				this.StatusMessageLabel.Style["margin"] = "3px";
			}
			this.StatusMessageLabel.Text = "";
			cell.Controls.Add( StatusMessageLabel );
			row.Cells.Add( cell );
			
			// Build the nav-buttons cell
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Right; 
			cell.VerticalAlign = VerticalAlign.Middle;
			
			// Add the download button
			if ( this.IncludeDownloadButton ) {
				this.downloadButton = new System.Web.UI.WebControls.Button();
				this.DownloadButton.CommandName = CommandNames.Download;
				this.DownloadButton.ID = this.NamingContainer.ID + "_" + CommandNames.Download + "_btn";
				this.DownloadButton.Text = ButtonLabels.Download;
				this.DownloadButton.Width = ButtonSizes.StandardButtonWidth;
				this.DownloadButton.CausesValidation = false;
				this.DownloadButton.Command += new CommandEventHandler( OnButtonClick );
				this.DownloadButton.ToolTip = ButtonTooltips.Download;
				cell.Controls.Add( DownloadButton );
				Literal spacer = new Literal();
				spacer.Text = "&nbsp;";
				cell.Controls.Add( spacer );
			}
			
			this.firstButton = new StarrTech.WebTools.Controls.NavButton();
			this.FirstButton.ID = this.NamingContainer.ID + "_" + CommandNames.GridPageFirst + "_btn";
			this.FirstButton.Text = ButtonLabels.GridPageFirst;
			this.FirstButton.Width = ButtonSizes.NavButtonWidth;
			this.FirstButton.CausesValidation = false;
			this.FirstButton.Command += new CommandEventHandler( OnButtonClick );
			this.FirstButton.CommandName = CommandNames.GridPageFirst;
			this.FirstButton.ToolTip = ButtonTooltips.GridPageFirst;
			cell.Controls.Add( FirstButton );
			
			this.prevButton = new StarrTech.WebTools.Controls.NavButton();
			this.PrevButton.ID = this.NamingContainer.ID + "_" + CommandNames.GridPagePrev + "_btn";
			this.PrevButton.Text = ButtonLabels.GridPagePrev;
			this.PrevButton.Width = ButtonSizes.NavButtonWidth;
			this.PrevButton.CausesValidation = false;
			this.PrevButton.Command += new CommandEventHandler( OnButtonClick );
			this.PrevButton.CommandName = CommandNames.GridPagePrev;
			this.PrevButton.ToolTip = ButtonTooltips.GridPagePrev;
			cell.Controls.Add( PrevButton );
			
			//this.pagePicker = new DropDownList();
			//this.PagePicker.Width = new Unit( "40" );
			//this.PagePicker.ToolTip = ButtonTooltips.GridPagePicker;
			//this.PagePicker.ID = string.Format( "{0}_PagePicker", this.ID );
			//this.PagePicker.SelectedIndexChanged += new EventHandler( this.OnPageSelected );
			//this.PagePicker.AutoPostBack = true;
			//cell.Controls.Add( PagePicker );
			
			this.nextButton = new StarrTech.WebTools.Controls.NavButton();
			this.NextButton.ID = this.NamingContainer.ID + "_" + CommandNames.GridPageNext + "_btn";
			this.NextButton.Text = ButtonLabels.GridPageNext;
			this.NextButton.Width = ButtonSizes.NavButtonWidth;
			this.NextButton.CausesValidation = false;
			this.NextButton.Command += new CommandEventHandler( OnButtonClick );
			this.NextButton.CommandName = CommandNames.GridPageNext;
			this.NextButton.ToolTip = ButtonTooltips.GridPageNext;
			cell.Controls.Add( NextButton );
			
			this.lastButton = new StarrTech.WebTools.Controls.NavButton();
			this.LastButton.ID = this.NamingContainer.ID + "_" + CommandNames.GridPageLast + "_btn";
			this.LastButton.Width = ButtonSizes.NavButtonWidth;
			this.LastButton.Text = ButtonLabels.GridPageLast;
			this.LastButton.CausesValidation = false;
			this.LastButton.Command += new CommandEventHandler( OnButtonClick );
			this.LastButton.CommandName = CommandNames.GridPageLast;
			this.LastButton.ToolTip = ButtonTooltips.GridPageLast;
			cell.Controls.Add( LastButton );
			
			row.Cells.Add( cell );
			this.Rows.Add( row );
		}
		
		#endregion
		
		#region Public Methods 
		/// <summary>
		/// Called by the AdminDataGrid when it's state changes, to enable/disable the paging buttons and reset the status message.
		/// </summary>
		public virtual void SetupControls() {
			int pageCount = this.parent.Grid.PageCount;
			int currentPage = this.parent.Grid.CurrentPageIndex;
			bool canPageBack = currentPage > 0;
			bool canPageNext = currentPage < ( pageCount - 1 );
			this.FirstButton.Enabled = canPageBack;
			this.PrevButton.Enabled = canPageBack;
			this.LastButton.Enabled = canPageNext;
			this.NextButton.Enabled = canPageNext;
			//this.PagePicker.Enabled = pageCount > 0;
			//this.PagePicker.Items.Clear();
			//for ( int i = 1 ; i <= pageCount ; i++ ) {
			//	this.PagePicker.Items.Add( new ListItem( i.ToString(), i.ToString() ) ); 
			//}
			//this.PagePicker.SelectedIndex = currentPage;
			this.StatusMessage = String.Format( Messages.ListPageMessage, currentPage + 1, pageCount );
		}

		/// <summary>
		/// Called to completely enable/disable the paging buttons.
		/// </summary>
		/// <param name="enabled">True to enable.</param>
		public virtual void EnableButtons( bool enabled ) {
			this.FirstButton.Enabled = enabled;
			this.PrevButton.Enabled = enabled;
			this.NextButton.Enabled = enabled;
			this.LastButton.Enabled = enabled;
		}
		
		#endregion
	}
	
}
