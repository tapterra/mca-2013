using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Messages and New button for the DBUI AdminDataGrid
	/// </summary>
	public class DBGridControllerPanel : Table {
		
		#region Properties 
		
		/// <summary>
		/// Reference to the AdminDataGrid we're working for.
		/// </summary>
		protected AdminDataGrid parent;
		
		/// <summary>
		/// Label control for displaying the result-count status messages.
		/// </summary>
		public Label StatusMessageLabel {
			get {
				this.EnsureChildControls();
				return ( statusMessageLabel );
			}
		}
		private Label statusMessageLabel = new Label();
		
		/// <summary>
		/// Exposes the Text property of the StatusMessageLabel.
		/// </summary>
		public string StatusMessage {
			get { return ( StatusMessageLabel.Text ); }
			set { StatusMessageLabel.Text = value; }
		}
		
		
		/// <summary>
		/// String expression suitable for use in a SQL ORDER BY clause
		/// </summary>
		public string SortExpression {
			get { 
				string expr = (string) ViewState["SortExpression"];
				return ( expr == null ? "" : expr ); 
			}
			set { ViewState["SortExpression"] = value; }
		}
		
		
		/// <summary>
		/// ASC or DESC qualifier for the SortExpression
		/// </summary>
		public string SortOrder {
			get { 
				string expr = (string) ViewState["SortOrder"];
				return ( string.IsNullOrEmpty( expr ) ? "asc" : expr ); 
			}
			set { ViewState["SortOrder"] = value; }
		}
		
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
				
		
		#endregion
		
		#region Constructor 
		
		public DBGridControllerPanel( AdminDataGrid Parent ) {
			this.parent = Parent;
			CellSpacing = 0;
			CellPadding = 0;
			BorderStyle = BorderStyle.None;
			Width = new Unit( "100%" );
		}
				
		
		#endregion
		
		#region Protected Methods
		
		protected override void CreateChildControls() {
			this.Rows.Clear();
			TableRow row;
			TableCell cell;
			// Initialize our controls
			if ( this.parent.Header3CssClass != "" ) {
				this.StatusMessageLabel.CssClass = this.parent.Header3CssClass;
			} else {
				this.StatusMessageLabel.Font.Name = "Verdana";
				this.StatusMessageLabel.Font.Size = 9;
				this.StatusMessageLabel.Font.Italic = true;
				this.StatusMessageLabel.Style["margin"] = "3px";
			}
			this.StatusMessageLabel.Text = "";
			
			// Create the status-message row
			row = new TableRow();
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Left; 
			cell.VerticalAlign = VerticalAlign.Middle;
			cell.Style["padding-left"] = "3px";
			cell.Style["padding-top"] = "6px";
			cell.Style["padding-bottom"] = "6px";
			//cell.ColumnSpan = 2;
			cell.Controls.Add( this.StatusMessageLabel );
			row.Cells.Add( cell );
			this.Rows.Add( row );
		}
		
		
		#endregion
		
	}
	
	
}
