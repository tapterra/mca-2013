using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Buttons and such for the DBUI Search Panel.
	/// </summary>
	public class DBSearchController : Table {
		#region Properties 
		
		protected AdminDataGrid parent;
		
		public Label StatusMessageLabel {
			get {
				this.EnsureChildControls();
				return ( this.statusMessageLabel );
			}
		}
		private Label statusMessageLabel = new Label();
		
		public string StatusMessage {
			get { 
				this.EnsureChildControls();
				return ( this.StatusMessageLabel.Text ); 
			}
			set { this.StatusMessageLabel.Text = value; }
		}

		public System.Web.UI.WebControls.Button SearchButton {
			get {
				this.EnsureChildControls();
				return ( this.searchButton );
			}
		}
		private System.Web.UI.WebControls.Button searchButton = new System.Web.UI.WebControls.Button();

		public System.Web.UI.WebControls.Button ShowAllButton {
			get {
				this.EnsureChildControls();
				return ( this.showAllButton );
			}
		}
		private System.Web.UI.WebControls.Button showAllButton = new System.Web.UI.WebControls.Button();

		public System.Web.UI.WebControls.Button NewButton {
			get {
				this.EnsureChildControls();
				return ( this.newButton );
			}
		}
		private System.Web.UI.WebControls.Button newButton = new System.Web.UI.WebControls.Button();
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
				
		#endregion
		
		#region Events 
		
		// OnButtonClick
		private static readonly object ButtonClickEventKey = new object();
		public event CommandEventHandler ButtonClick {
			add { Events.AddHandler( ButtonClickEventKey, value ); }
			remove { Events.RemoveHandler( ButtonClickEventKey, value ); }
		}
		protected virtual void OnButtonClick( Object sender, CommandEventArgs args ) {
			CommandEventHandler btnClickHandler = (CommandEventHandler) Events[ButtonClickEventKey];
			if ( btnClickHandler != null ) btnClickHandler( sender, args );
		}
		
		
		#endregion
		
		#region Constructor
		
		public DBSearchController( AdminDataGrid Parent ) {
			this.parent = Parent;
			this.CellSpacing = 3;
			this.CellPadding = 0;
			this.Width = new Unit( "100%" );
		}
		
		
		#endregion
		
		#region Protected Methods
		
		protected override void OnPreRender( EventArgs args ) {
			base.OnPreRender( args );
			//this.NewButton.Visible = parent.CanAddRecord;
			//this.ShowAllButton.Visible = parent.IncludeShowAllButton;
		}
		
		protected override void CreateChildControls() {
			Rows.Clear();
			TableRow row = new TableRow();
			// Initialize our controls
			if ( this.parent.Header1CssClass != "" ) {
				this.StatusMessageLabel.CssClass = this.parent.Header2CssClass;
			} else {
				this.StatusMessageLabel.Font.Name = "Verdana";
				this.StatusMessageLabel.Font.Size = 10;
				this.StatusMessageLabel.Font.Bold = true;
				this.StatusMessageLabel.Style["margin"] = "3px";
			}
			this.StatusMessageLabel.Text = "";
			
			this.SearchButton.CommandName = CommandNames.Search;
			this.SearchButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.Search );
			this.SearchButton.Text = ButtonLabels.Search;
			this.SearchButton.Width = ButtonSizes.StandardButtonWidth;
			this.SearchButton.CausesValidation = true;
			this.SearchButton.Command += new CommandEventHandler(OnButtonClick);
			this.SearchButton.ToolTip = ButtonTooltips.Search;
			
			this.ShowAllButton.CommandName = CommandNames.ShowAll;
			this.ShowAllButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.ShowAll );
			this.ShowAllButton.Text = ButtonLabels.ShowAll;
			this.ShowAllButton.Width = ButtonSizes.StandardButtonWidth;
			this.ShowAllButton.CausesValidation = false;
			this.ShowAllButton.Command += new CommandEventHandler(OnButtonClick);
			this.ShowAllButton.ToolTip = ButtonTooltips.ShowAll;
			
			this.NewButton.CommandName = CommandNames.NewRecord;
			this.NewButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.NewRecord );
			this.NewButton.Text = ButtonLabels.NewRecord;
			this.NewButton.Width = ButtonSizes.StandardButtonWidth;
			this.NewButton.CausesValidation = false;
			this.NewButton.Command += new CommandEventHandler(OnButtonClick);
			this.NewButton.ToolTip = ButtonTooltips.NewRecord;
			
			// Create the status-message cell
			TableCell cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Left; 
			cell.VerticalAlign = VerticalAlign.Middle;
			cell.Controls.Add( this.StatusMessageLabel );
			Literal spacer = new Literal();
			spacer.Text = "<br />";
			cell.Controls.Add( spacer );
			row.Cells.Add( cell );
			
			// Create the buttons cells
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Right; 
			cell.VerticalAlign = VerticalAlign.Middle;
			cell.Width = new Unit( ButtonSizes.StandardButtonWidth + 6 );
			cell.Controls.Add( this.SearchButton );
			row.Cells.Add( cell );

			if ( parent.IncludeShowAllButton ) {
				cell = new TableCell();
				cell.HorizontalAlign = HorizontalAlign.Right;
				cell.VerticalAlign = VerticalAlign.Middle;
				cell.Width = new Unit( ButtonSizes.StandardButtonWidth + 6 );
				cell.Controls.Add( this.ShowAllButton );
				row.Cells.Add( cell );
			}
			if ( parent.CanAddRecord ) {
				cell = new TableCell();
				cell.HorizontalAlign = HorizontalAlign.Right;
				cell.VerticalAlign = VerticalAlign.Middle;
				cell.Width = new Unit( ButtonSizes.StandardButtonWidth + 6 );
				cell.Controls.Add( this.NewButton );
				row.Cells.Add( cell );
			}
			Rows.Add( row );
		}
		
		#endregion
	}
	
}
