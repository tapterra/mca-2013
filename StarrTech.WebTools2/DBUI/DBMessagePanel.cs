using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Error-message panel for the AdminDataGrid
	/// </summary>
	public class DBMessagePanel : Panel {
		
		#region Properties 
		
		protected AdminDataGrid parent;
		
		public Label ErrorMessageLabel {
			get {
				EnsureChildControls();
				return ( errorMessageLabel );
			}
		}
		private Label errorMessageLabel = new Label();
		
		public ValidationSummary ValidationMessage {
			get {
				EnsureChildControls();
				return ( validationMessage );
			}
		}
		private ValidationSummary validationMessage = new ValidationSummary();
		
		public override ControlCollection Controls {
			get {
				EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		
		#endregion
		
		#region Methods
		
		// Constructor
		public DBMessagePanel( AdminDataGrid Parent ) {
			this.parent = Parent;
		}
		
		
		protected override void CreateChildControls() {
			Controls.Clear();
			ErrorMessageLabel.ForeColor = Color.Red;
			ErrorMessageLabel.Font.Bold = true;
			ErrorMessageLabel.Font.Name = "Verdana";
			ErrorMessageLabel.Font.Size = new FontUnit( "10pt" );
			ErrorMessageLabel.Text = "";
			Controls.Add( ErrorMessageLabel );
			ValidationMessage.DisplayMode = ValidationSummaryDisplayMode.BulletList;
			ValidationMessage.ShowSummary = false;
			ValidationMessage.ShowMessageBox = true;
			ValidationMessage.Font.Bold = true;
			ValidationMessage.Font.Name = "Verdana";
			ValidationMessage.Font.Size = new FontUnit( "10pt" );
			ValidationMessage.HeaderText = "Please correct the following errors before proceeding:";
			ValidationMessage.BorderStyle = BorderStyle.Solid;
			ValidationMessage.BorderWidth = new Unit( "1px" );
			ValidationMessage.BorderColor = Color.FromName( "#993333" );
			ValidationMessage.BackColor = Color.FromName( "#FFEEEE" );
			ValidationMessage.Width = new Unit( "500px" );
			ValidationMessage.Style["margin"] = "10px";
			ValidationMessage.Style["padding"] = "10px";
			Controls.Add( ValidationMessage );
		}
		
		
		#endregion
		
	}
	
	
}
