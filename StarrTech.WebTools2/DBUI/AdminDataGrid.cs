using System;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using StarrTech.WebTools.Data;
using System.Collections;

namespace StarrTech.WebTools.DBUI {

	/// <summary>
	/// Composite control for managing a search editor/results grid/data-edit form.
	/// </summary>
	[ParseChildren( ChildrenAsProperties = true )]
	public class AdminDataGrid : WebControl, INamingContainer, IPostBackEventHandler {

		#region Properties

		#region Permissions

		public bool CanEditRecord {
			get {
				object obj = ViewState["CanEditRecord"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["CanEditRecord"] = value; }
		}

		public bool DisableFormControlsOnCantEditRecord {
			get {
				object obj = ViewState["DisableFormControlsOnCantEditRecord"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["DisableFormControlsOnCantEditRecord"] = value; }
		}

		public bool CanAddRecord {
			get {
				object obj = ViewState["CanAddRecord"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["CanAddRecord"] = value; }
		}

		public bool CanDeleteRecord {
			get {
				object obj = ViewState["CanDeleteRecord"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["CanDeleteRecord"] = value; }
		}

		public bool CanSearch {
			get {
				object obj = ViewState["CanSearch"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["CanSearch"] = value; }
		}

		#endregion

		#region Features

		/// <summary>
		/// Use DHTML to allow the user to click on any part of a row to edit its record
		/// </summary>
		public bool UseClickableRows {
			get {
				object obj = ViewState["UseClickableRows"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["UseClickableRows"] = value; }
		}

		/// <summary>
		/// Use DHTML to allow the user to click on any part of a row to select/hilight it
		/// </summary>
		public bool UseSelectableRows {
			get {
				object obj = ViewState["UseSelectableRows"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["UseSelectableRows"] = value; }
		}

		public bool IncludeShowAllButton {
			get {
				object obj = ViewState["IncludeShowAllButton"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["IncludeShowAllButton"] = value; }
		}

		public DataGridItem SelectedItem {
			get { return ( this.Grid.SelectedItem ); }
		}

		public int SelectedIndex {
			get { return ( this.Grid.SelectedIndex ); }
			set { this.Grid.SelectedIndex = value; }
		}

		/// <summary>
		/// Use DHTML to allow the user to click on any column header to sort the grid
		/// </summary>
		public bool AllowUserSorting {
			get {
				object obj = ViewState["AllowUserSorting"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["AllowUserSorting"] = value; }
		}

		public bool IncludeDownloadButton {
			get {
				this.EnsureChildControls();
				return ( this.GridPageController.IncludeDownloadButton );
			}
			set {
				this.EnsureChildControls();
				this.GridPageController.IncludeDownloadButton = value;
			}
		}

		public bool IncludeReorderControls {
			get {
				if ( !this.CanEditRecord ) {
					return ( false );
				}
				object obj = ViewState["IncludeReorderControls"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["IncludeReorderControls"] = value; }
		}

		public bool IncludeGridStatusMessage {
			get {
				object obj = ViewState["IncludeGridStatusMessage"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["IncludeGridStatusMessage"] = value; }
		}

		public bool ShowAllOnLoad {
			get {
				object obj = ViewState["ShowAllOnLoad"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["ShowAllOnLoad"] = value; }
		}

		#endregion

		#region Formatting

		/// <summary>
		/// Background color for column-headers being moused-over
		/// </summary>
		public Color ColHilightColor {
			get {
				object obj = ViewState["ColHilightColor"];
				return ( obj == null ? ColorTranslator.FromHtml( "#bbbbbb" ) : (Color) obj );
			}
			set { ViewState["ColHilightColor"] = value; }
		}

		/// <summary>
		/// Background color for rows being moused-over
		/// </summary>
		public Color RowHilightColor {
			get {
				object obj = ViewState["RowHilightColor"];
				return ( obj == null ? ColorTranslator.FromHtml( "#bbbbbb" ) : (Color) obj );
			}
			set { ViewState["RowHilightColor"] = value; }
		}

		public string Header1CssClass {
			get {
				object obj = ViewState["Header1CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header1CssClass"] = value; }
		}

		public string Header2CssClass {
			get {
				object obj = ViewState["Header2CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header2CssClass"] = value; }
		}

		public string Header3CssClass {
			get {
				object obj = ViewState["Header3CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header3CssClass"] = value; }
		}

		public TableItemStyle GridRowStyle {
			get {
				this.EnsureChildControls();
				return ( this.Grid.ItemStyle );
			}
		}

		public TableItemStyle GridAlternatingRowStyle {
			get {
				this.EnsureChildControls();
				return ( this.Grid.AlternatingItemStyle );
			}
		}

		public TableItemStyle GridSelectedRowStyle {
			get {
				this.EnsureChildControls();
				return ( this.Grid.SelectedItemStyle );
			}
		}

		public TableItemStyle GridHeaderStyle {
			get {
				this.EnsureChildControls();
				return ( this.Grid.HeaderStyle );
			}
		}

		protected string MoveUpImageUrl {
			get {
				if ( this.moveUpImageUrl == "" ) {
					this.moveUpImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveUpImagePath );
				}
				return ( this.moveUpImageUrl );
			}
		}
		private string moveUpImageUrl = "";
		private string moveUpImagePath = "StarrTech.WebTools.Resources.treeui.move-up.gif";

		protected string MoveDownImageUrl {
			get {
				if ( this.moveDownImageUrl == "" ) {
					this.moveDownImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveDownImagePath );
				}
				return ( this.moveDownImageUrl );
			}
		}
		private string moveDownImageUrl = "";
		private string moveDownImagePath = "StarrTech.WebTools.Resources.treeui.move-down.gif";

		#endregion

		#region Data Source

		public IList DataList {
			get { return ( this.dataList ); }
			set { this.dataList = value; }
		}
		private IList dataList;

		public int DataListCount {
			get { return ( this.DataList.Count ); }
		}

		public int CurrentRecordIndex {
			get { return ( this.formController.RecordIndex ); }
			set { this.formController.RecordIndex = value; }
		}

		#endregion

		#region DBItem/Type

		public Type DBItemType {
			get { return ( (Type) ViewState["DBItemType"] ); }
			set {
				ViewState["DBItemType"] = value;
				dbItemPluralLabel = "";
				dbItemSingularLabel = "";
			}
		}

		public String DBItemClass {
			get { return ( this.DBItemType.ToString() ); }
			set {
				this.DBItemType = Type.GetType( value, true, true );
				this.DBItem = this.CreateDBItem( -1 );
			}
		}

		public BaseSqlPersistable DBItem {
			get {
				if ( this.dbItem == null )
					this.dbItem = this.CreateDBItem( -1 );
				return ( this.dbItem );
			}
			set { this.dbItem = value; }
		}
		private BaseSqlPersistable dbItem;

		public string DBItemPluralLabel() {
			if ( string.IsNullOrEmpty( this.dbItemPluralLabel ) ) {
				this.dbItemPluralLabel = SqlDatabase.GetTableAttribute( this.DBItemType ).PluralLabel;
			}
			return ( this.dbItemPluralLabel );
		}
		private string dbItemPluralLabel = "";

		public string DBItemSingularLabel() {
			if ( string.IsNullOrEmpty( this.dbItemSingularLabel ) ) {
				this.dbItemSingularLabel = SqlDatabase.GetTableAttribute( this.DBItemType ).SingularLabel;
			}
			return ( this.dbItemSingularLabel );
		}
		private string dbItemSingularLabel = "";

		#endregion

		#region Templates
		[TemplateContainer( typeof( SearchControlsContainer ) )]
		public ITemplate SearchControls {
			get { return this.searchControls; }
			set { this.searchControls = value; }
		}
		private ITemplate searchControls;
		private Control searchControlsContainer;


		[TemplateContainer( typeof( FormControlsContainer ) )]
		public ITemplate FormControls {
			get { return this.formControls; }
			set { this.formControls = value; }
		}
		private ITemplate formControls;
		private Control formControlsContainer;

		public DataGridColumnCollection GridColumns {
			get {
				this.EnsureChildControls();
				return ( this.Grid.Columns );
			}
		}

		#endregion

		#region Components

		public DBMessagePanel MessagePanel {
			get {
				this.EnsureChildControls();
				return ( this.messagePanel );
			}
		}
		protected DBMessagePanel messagePanel;

		public String ErrorMessage {
			get {
				this.EnsureChildControls();
				return ( this.MessagePanel.ErrorMessageLabel.Text );
			}
			set {
				this.EnsureChildControls();
				this.MessagePanel.ErrorMessageLabel.Text = value;
			}
		}

		public Panel SearchPanel {
			get { return ( this.searchPanel ); }
		}
		protected Panel searchPanel = new Panel();

		public Panel FormControlsPanel {
			get { return ( this.formControlsPanel ); }
		}
		private Panel formControlsPanel = new Panel();

		public DBSearchController SearchController {
			get {
				this.EnsureChildControls();
				return ( this.searchController );
			}
		}
		protected DBSearchController searchController;

		public Panel GridPanel {
			get { return ( this.gridPanel ); }
		}
		protected Panel gridPanel = new Panel();

		public DBGridControllerPanel GridController {
			get {
				this.EnsureChildControls();
				return ( this.gridController );
			}
		}
		protected DBGridControllerPanel gridController;

		public DBGridPageController GridPageController {
			get {
				this.EnsureChildControls();
				return ( this.gridPageController );
			}
		}
		protected DBGridPageController gridPageController;

		public DataGrid Grid {
			get {
				//this.EnsureChildControls();
				return ( this.grid );
			}
		}
		protected DataGrid grid = new DataGrid();

		public Panel FormPanel {
			get { return ( this.formPanel ); }
		}
		protected Panel formPanel = new Panel();

		public DBFormController FormController {
			get {
				this.EnsureChildControls();
				return ( this.formController );
			}
		}
		protected DBFormController formController;

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}

		#endregion

		#endregion

		#region Events

		#region DataBindCompleted
		/// <summary>
		/// Event raised when the BindGrid method has completed
		/// </summary>
		public event EventHandler DataBindCompleted {
			add { Events.AddHandler( DataBindCompletedEventKey, value ); }
			remove { Events.RemoveHandler( DataBindCompletedEventKey, value ); }
		}
		protected virtual void OnDataBindCompleted( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[DataBindCompletedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object DataBindCompletedEventKey = new object();
		#endregion

		#region SearchControlsInit
		/// <summary>
		/// Event raised when the SearchPanel controls need to be initialized.
		/// </summary>
		public event EventHandler SearchControlsInit {
			add { Events.AddHandler( SearchControlsInitEventKey, value ); }
			remove { Events.RemoveHandler( SearchControlsInitEventKey, value ); }
		}
		protected virtual void OnSearchControlsInit( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[SearchControlsInitEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object SearchControlsInitEventKey = new object();
		#endregion

		#region SearchControlsRead
		/// <summary>
		/// Event raised when the SearchPanel controls need to be read.
		/// </summary>
		public event EventHandler SearchControlsRead {
			add { Events.AddHandler( SearchControlsReadEventKey, value ); }
			remove { Events.RemoveHandler( SearchControlsReadEventKey, value ); }
		}
		protected virtual void OnSearchControlsRead( Object sender, EventArgs args ) {
			if ( !this.Visible )
				return;
			EventHandler handler = (EventHandler) Events[SearchControlsReadEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object SearchControlsReadEventKey = new object();
		#endregion

		#region SearchControlsClear
		/// <summary>
		/// Event raised when the SearchPanel controls need to be cleared.
		/// </summary>
		public event EventHandler SearchControlsClear {
			add { Events.AddHandler( SearchControlsClearEventKey, value ); }
			remove { Events.RemoveHandler( SearchControlsClearEventKey, value ); }
		}
		protected virtual void OnSearchControlsClear( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[SearchControlsClearEventKey];
			if ( handler != null )
				handler( sender, args );
			//this.IsGridBound = false;
		}
		private static readonly object SearchControlsClearEventKey = new object();
		#endregion

		#region FormControlsInit
		/// <summary>
		/// Event raised when the FormPanel controls need to be initialized.
		/// </summary>
		public event EventHandler FormControlsInit {
			add { Events.AddHandler( FormControlsInitEventKey, value ); }
			remove { Events.RemoveHandler( FormControlsInitEventKey, value ); }
		}
		protected virtual void OnFormControlsInit( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[FormControlsInitEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object FormControlsInitEventKey = new object();
		#endregion

		#region FormControlsRead
		/// <summary>
		/// Event raised when the FormPanel controls need to be read.
		/// </summary>
		public event EventHandler FormControlsRead {
			add { Events.AddHandler( FormControlsReadEventKey, value ); }
			remove { Events.RemoveHandler( FormControlsReadEventKey, value ); }
		}
		protected virtual void OnFormControlsRead( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[FormControlsReadEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object FormControlsReadEventKey = new object();
		#endregion

		#region FormClosing
		/// <summary>
		/// Event raised when the FormPanel is closed (for any reason).
		/// </summary>
		public event EventHandler FormClosing {
			add { Events.AddHandler( FormClosingEventKey, value ); }
			remove { Events.RemoveHandler( FormClosingEventKey, value ); }
		}
		protected virtual void OnFormClosing( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[FormClosingEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object FormClosingEventKey = new object();
		#endregion

		#region FormItemPersisted
		/// <summary>
		/// Event raised when the current record is saved.
		/// </summary>
		public event EventHandler FormItemPersisted {
			add { Events.AddHandler( FormItemPersistedEventKey, value ); }
			remove { Events.RemoveHandler( FormItemPersistedEventKey, value ); }
		}
		protected virtual void OnFormItemPersisted( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[FormItemPersistedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object FormItemPersistedEventKey = new object();
		#endregion

		#region ReorderUp
		/// <summary>
		/// Event raised when the Reorder-Up control is clicked
		/// </summary>
		public event EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs> ReorderUp {
			add { Events.AddHandler( ReorderUpEventKey, value ); }
			remove { Events.RemoveHandler( ReorderUpEventKey, value ); }
		}
		protected virtual void OnReorderUp( StarrTech.WebTools.Controls.DataGridClickEventArgs args ) {
			EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs> handler = (EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs>) Events[ReorderUpEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderUpEventKey = new object();
		#endregion

		#region ReorderDown
		/// <summary>
		/// Event raised when the Reorder-Up control is clicked
		/// </summary>
		public event EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs> ReorderDown {
			add { Events.AddHandler( ReorderDownEventKey, value ); }
			remove { Events.RemoveHandler( ReorderDownEventKey, value ); }
		}
		protected virtual void OnReorderDown( StarrTech.WebTools.Controls.DataGridClickEventArgs args ) {
			EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs> handler = (EventHandler<StarrTech.WebTools.Controls.DataGridClickEventArgs>) Events[ReorderDownEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderDownEventKey = new object();
		#endregion

		#region DownloadClick
		/// <summary>
		/// Event raised when the Download button is clicked.
		/// </summary>
		public event EventHandler DownloadClick {
			add { Events.AddHandler( DownloadClickEventKey, value ); }
			remove { Events.RemoveHandler( DownloadClickEventKey, value ); }
		}
		protected virtual void OnDownloadClick( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[DownloadClickEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object DownloadClickEventKey = new object();
		#endregion

		#endregion

		#region Constructors

		public AdminDataGrid() {
			this.SetupDataGrid();
		}

		#endregion

		#region Methods

		#region Control-construction

		private void SetupDataGrid() {
			// Set some default properties...
			this.Grid.AutoGenerateColumns = false;
			this.Grid.ShowHeader = true;
			this.Grid.ShowFooter = false;
			this.Grid.EnableViewState = true;
			this.Grid.DataKeyField = "ID";
			this.Grid.AllowPaging = true;
			this.Grid.PageSize = 20;
			this.Grid.PagerStyle.Visible = false;
			this.Grid.Style.Add( "margin", "0px" );
			this.Grid.Style.Add( "width", "100%" );
			this.Grid.SortCommand += new DataGridSortCommandEventHandler( this.GridSortClick );
			this.Grid.ItemCommand += new DataGridCommandEventHandler( this.GridItemButtonClick );
			this.Grid.ItemDataBound += new DataGridItemEventHandler( this.GridItemBound );
			this.Grid.ItemCreated += new DataGridItemEventHandler( this.GridItemCreated );
			//this.Grid.EnableViewState = false;
		}

		protected override void CreateChildControls() {
			this.messagePanel = new DBMessagePanel( this );
			this.Controls.Add( this.messagePanel );
			this.CreateSearchPanel();
			this.CreateDataGridPanel();
			this.CreateDataFormPanel();
			this.CreateCustomGridColumns();
			base.CreateChildControls();
		}

		protected virtual void CreateSearchPanel() {
			// Build the SearchPanel section
			// Add the SearchController first...
			this.searchController = new DBSearchController( this );
			this.SearchController.ButtonClick += new CommandEventHandler( this.GridButtonClick );
			this.SearchPanel.Controls.Add( this.SearchController );
			// Now add the user's search-controls...
			if ( this.SearchControls != null ) {
				this.searchControlsContainer = new SearchControlsContainer( this );
				this.searchControlsContainer.ID = "SearchControls";
				this.SearchControls.InstantiateIn( this.searchControlsContainer );
				this.SearchPanel.Controls.Add( this.searchControlsContainer );
			} else {
				if ( !this.CanAddRecord ) {
					this.SearchController.Visible = false;
				}
			}
			if ( this.SearchPanel.Controls.Count == 1 ) {
				// We've only got the SearchController...
				if ( this.CanAddRecord ) {
					this.SearchController.StatusMessage = ""; //String.Format( Messages.NewButtonMessage, this.DBItemSingularLabel() );
					this.SearchController.SearchButton.Visible = false;
					this.SearchController.ShowAllButton.Visible = false;
					this.SearchController.NewButton.Visible = true;
				}
			}
			// We're done
			this.Controls.Add( this.SearchPanel );
		}

		protected virtual void CreateDataFormPanel() {
			// Build the data-form (record editor) section
			// Add the form-controller first
			this.formController = new DBFormController( this );
			this.FormController.ButtonClick += new CommandEventHandler( this.FormButtonClick );
			this.FormPanel.Controls.Add( this.FormController );
			// Now add the user's form-controls...
			if ( this.FormControls != null ) {
				this.formControlsContainer = new FormControlsContainer( this );
				this.formControlsContainer.ID = "FormControls";
				this.FormControls.InstantiateIn( this.formControlsContainer );
				this.FormControlsPanel.Controls.Add( this.formControlsContainer );
			}
			this.FormPanel.Controls.Add( this.FormControlsPanel );
			this.Controls.Add( this.FormPanel );
		}

		protected void CreateDataGridPanel() {
			if ( this.Grid != null ) {
				this.gridController = new DBGridControllerPanel( this );
				this.GridController.ID = this.ID + "_GridController";
				this.GridPanel.Controls.Add( this.GridController );
				this.GridPanel.Controls.Add( this.Grid );
				this.gridPageController = new DBGridPageController( this );
				this.GridPageController.ID = this.ID + "_GridPageController";
				this.GridPageController.ButtonClick += new CommandEventHandler( this.GridButtonClick );
				//this.GridPageController.PageSelected +=new EventHandler( this.GridPageSelected );
				this.GridPanel.Controls.Add( this.GridPageController );
				this.Controls.Add( this.GridPanel );
			}
		}

		protected internal void CreateCustomGridColumns() {
			// Add the invisible ID column to the DataGrid...
			BoundColumn idcol = new BoundColumn();
			idcol.DataField = "id";
			idcol.HeaderText = "ID";
			idcol.ReadOnly = true;
			idcol.Visible = false;
			this.Grid.Columns.AddAt( 0, idcol );
			if ( !this.UseClickableRows ) {
				// Add the edit-button column...
				ButtonColumn btncol = new ButtonColumn();
				btncol.ButtonType = ButtonColumnType.LinkButton;
				btncol.CommandName = "edit";
				btncol.HeaderText = "&nbsp;";
				btncol.Text = ( this.CanEditRecord ? ButtonLabels.EditRecord : ButtonLabels.ViewRecord );
				btncol.ItemStyle.Font.Bold = true;
				btncol.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
				btncol.ItemStyle.Width = 30;
				btncol.Visible = true;
				this.Grid.Columns.AddAt( this.Grid.Columns.Count, btncol );
			}
		}

		protected void GridItemCreated( object sender, DataGridItemEventArgs args ) {
			DataGridItem row = args.Item;
			// Modify the column-headers...
			if ( row.ItemType == ListItemType.Header ) {
				if ( this.AllowUserSorting ) {
					// Set up the column-headers for click-to-sort
					int c = 0;
					foreach ( TableCell cell in row.Cells ) {
						if ( c >= this.grid.Columns.Count )
							break;
						if ( ! string.IsNullOrEmpty( this.grid.Columns[c].SortExpression ) ) {
							cell.Attributes["onmouseover"] = string.Format(
								"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.ColHilightColor )
							);
							cell.Attributes["onmouseout"] = string.Format(
								"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.grid.HeaderStyle.BackColor )
							);
							cell.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink( this, c.ToString() );
						}
						c++;
					}
				}
				// Add extra columns for the re-order controls
				if ( this.IncludeReorderControls ) {
					TableCell cell = new TableCell();
					cell.ColumnSpan = 2;
					cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
					row.Cells.Add( cell );
				}
			}
			if ( row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem || row.ItemType == ListItemType.SelectedItem ) {
				bool rowIsSelected = this.UseSelectableRows ? ( row.ItemType == ListItemType.SelectedItem ) : false;
				if ( row.ItemIndex >= 0 ) {
					// Set the "mouse-over" color
					if ( rowIsSelected ) {
						row.Attributes["onmouseover"] = "javascript:this.style.cursor='hand';";
					} else {
						row.Attributes["onmouseover"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.RowHilightColor )
						);
					}
					// Set up the "mouse-out" color
					if ( rowIsSelected ) {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.grid.SelectedItemStyle.BackColor )
						);
					} else if ( row.ItemType == ListItemType.Item ) {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.grid.ItemStyle.BackColor )
						);
					} else {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.grid.AlternatingItemStyle.BackColor )
						);
					}
					// Add extra columns for the re-order controls
					if ( this.IncludeReorderControls ) {
						int recCount = this.DataList == null ? this.grid.DataKeys.Count : this.DataList.Count;
						TableCell cell = new TableCell();
						LiteralControl lit = new LiteralControl();
						lit.Text = row.ItemIndex == 0 ? "&nbsp;" : string.Format( "<img src='{0}' alt='Move this page up' />", this.MoveUpImageUrl );
						cell.Width = new Unit( "15" );
						cell.HorizontalAlign = HorizontalAlign.Center;
						cell.Controls.Add( lit );
						row.Cells.Add( cell );
						cell = new TableCell();
						lit = new LiteralControl();
						lit.Text = row.ItemIndex == ( recCount - 1 ) ? "&nbsp;" : string.Format( "<img src='{0}' alt='Move this page down' />", this.MoveDownImageUrl );
						cell.Width = new Unit( "15" );
						cell.HorizontalAlign = HorizontalAlign.Center;
						cell.Controls.Add( lit );
						row.Cells.Add( cell );
					}
				}
			}
		}

		protected void GridItemBound( Object sender, DataGridItemEventArgs args ) {
			DataGridItem row = args.Item;
			if ( this.UseClickableRows ) {
				if ( row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem || row.ItemType == ListItemType.SelectedItem ) {
					// Set up the row click-back script
					// Have to do this when the grid is databound, because it depends on record IDs
					int colIndex = 0;
					foreach ( TableCell cell in row.Cells ) {
						string recID = args.Item.Cells[0].Text;
						string attribValue = this.Page.ClientScript.GetPostBackClientHyperlink( this,
							string.Format( "{0};{1};{2}", recID, args.Item.ItemIndex, colIndex )
						);
						cell.Attributes["onclick"] = attribValue;
						colIndex++;
					}
				}
			}
		}

		#endregion

		#region DBItem interaction

		private BaseSqlPersistable CreateDBItem( int ID ) {
			BaseSqlPersistable item = (BaseSqlPersistable) DBItemType.InvokeMember( null,
				BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance,
				null, null, new Object[] { ID } );
			return ( item );
		}

		protected internal DataSet DBItemDoSearch() {
			return ( (DataSet) this.DBItemType.InvokeMember( "DoSearch", BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
				null, null, new Object[] { } ) );
		}

		protected internal IList DBItemDoSearchList() {
			return ( (IList) this.DBItemType.InvokeMember( "DoSearchList", BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
				null, null, new Object[] { } ) );
		}

		protected internal void DBItemClearSearch() {
			this.DBItemType.InvokeMember( "NewSearch", BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
				null, null, new Object[] { } );
		}

		protected internal string DBItemGetSearchSql() {
			return ( (string) this.DBItemType.InvokeMember( "GetSearchSql", BindingFlags.Public | BindingFlags.Static | BindingFlags.InvokeMethod,
				null, null, new Object[] { } ) );
		}

		protected internal string DBItemSetSortExpression( string expr ) {
			return ( (string) this.DBItemType.InvokeMember( "SortExpression", BindingFlags.Public | BindingFlags.Static | BindingFlags.SetProperty |
				BindingFlags.FlattenHierarchy, null, null, new Object[] { expr } ) );
		}

		#endregion

		#region Command dispatch

		protected void GridButtonClick( Object sender, CommandEventArgs args ) {
			this.Grid.EditItemIndex = -1;
			string cmd = args.CommandName;
			switch ( cmd ) {
				case ( CommandNames.ShowAll ):
					this.Grid.EditItemIndex = -1;
					this.Grid.CurrentPageIndex = 0;
					OnSearchControlsClear( this, new EventArgs() );
					break;
				case ( CommandNames.Search ):
					this.Grid.EditItemIndex = -1;
					this.Grid.CurrentPageIndex = 0;
					break;
				case ( CommandNames.NewRecord ):
					this.SetupFormView( -1 );
					this.Page.DataBind();
					break;
				case ( CommandNames.GridPageFirst ):
					this.Grid.CurrentPageIndex = 0;
					break;
				case ( CommandNames.GridPagePrev ):
					if ( this.Grid.CurrentPageIndex > 0 )
						this.Grid.CurrentPageIndex--;
					break;
				case ( CommandNames.GridPageNext ):
					if ( this.Grid.CurrentPageIndex < ( this.Grid.PageCount - 1 ) )
						this.Grid.CurrentPageIndex++;
					break;
				case ( CommandNames.GridPageLast ):
					this.Grid.CurrentPageIndex = ( this.Grid.PageCount - 1 );
					break;
				case ( CommandNames.Download ):
					this.OnDownloadClick( this, new EventArgs() );
					break;
				default:  // page number
					this.Grid.CurrentPageIndex = Convert.ToInt32( cmd );
					break;
			}
			BindGrid();
		}

		protected void GridItemButtonClick( Object sender, DataGridCommandEventArgs args ) {
			string cmd = args.CommandName;
			if ( cmd == DataGrid.SortCommandName ) {
				this.Grid.EditItemIndex = -1;
				this.Grid.CurrentPageIndex = 0;
				this.GridController.SortExpression = args.CommandArgument.ToString();
				BindGrid();
				return;
			} else { // Edit
				this.EditGridRecord( Convert.ToInt32( args.Item.Cells[0].Text ), args.Item.ItemIndex );
			}
		}

		protected void GridSortClick( Object sender, DataGridSortCommandEventArgs args ) {
			this.SortBySortExpression( args.SortExpression );
		}

		protected void SortBySortExpression( string newSort ) {
			this.Grid.EditItemIndex = -1;
			this.Grid.CurrentPageIndex = 0;
			if ( this.GridController.SortExpression == newSort ) {
				// Clicked on the same column - reverse the sort order
				this.GridController.SortOrder = this.GridController.SortOrder.ToLower() == "asc" ? "desc" : "asc";
				//this.IsGridBound = false;
			} else {
				this.GridController.SortOrder = "asc";
			}
			this.GridController.SortExpression = newSort;
			this.BindGrid();
		}

		protected void FormButtonClick( Object sender, CommandEventArgs args ) {
			string cmd = args.CommandName;
			if ( cmd == CommandNames.FormPageCancel ) {
				this.ReturnToGrid();
			} else if ( cmd == CommandNames.FormPageDelete ) {
				this.DBItem = this.CreateDBItem( this.FormController.RecordID );
				if ( this.DBItem.Delete() ) {
					this.Grid.CurrentPageIndex = 0;
					this.ReturnToGrid();
				}
			} else {
				if ( this.SaveFormRecord() ) {
					switch ( cmd ) {
						case ( CommandNames.FormPageSave ):
							this.ReturnToGrid();
							break;
						case ( CommandNames.FormPageFirst ):
							this.BindGrid();
							this.FormController.RecordIndex = 0;
							this.EditRecordForm();
							break;
						case ( CommandNames.FormPagePrev ):
							this.BindGrid();
							this.FormController.RecordIndex = ( this.FormController.RecordIndex <= 0 ? 0 : this.FormController.RecordIndex - 1 );
							this.EditRecordForm();
							break;
						case ( CommandNames.FormPageNext ):
							this.BindGrid();
							this.FormController.RecordIndex = ( this.FormController.RecordIndex < this.DataListCount - 1 ? this.FormController.RecordIndex + 1 : this.DataListCount - 1 );
							this.EditRecordForm();
							break;
						case ( CommandNames.FormPageLast ):
							this.BindGrid();
							this.FormController.RecordIndex = this.DataListCount - 1;
							this.EditRecordForm();
							break;
					}
				}
				this.FormController.SetupControls();
			}
			this.SetupFormErrorMessages();
		}

		protected override void OnDataBinding( EventArgs e ) {
			this.EnsureChildControls();
			base.OnDataBinding( e );
		}

		protected void SetupEditForm( int id ) {
			this.OnFormControlsInit( this, new EventArgs() );
			if ( this.DisableFormControlsOnCantEditRecord ) {
				if ( id <= 0 ) {
					this.FormControlsPanel.Enabled = this.CanAddRecord;
				} else {
					this.FormControlsPanel.Enabled = this.CanEditRecord;
				}
			}
			this.FormPanel.Visible = true;
		}

		protected void ReturnToGrid() {
			this.Grid.EditItemIndex = -1;
			this.GridPanel.Visible = true;
			this.SearchPanel.Visible = this.CanSearch;
			this.FormPanel.Visible = false;
			this.FormController.RecordIndex = -1;
			this.OnFormClosing( this, new EventArgs() );
			BindGrid();
		}

		protected void DBError( string msg, string source, string sql ) {
#if DEBUG
			this.ErrorMessage = String.Format( Messages.DBErrorMessage, msg, source, sql );
#else
			this.ErrorMessage = String.Format( Messages.DBErrorMessage, msg, "", "" );
#endif
		}

		protected void EditRecordForm() {
			this.BindGrid();
			this.FormController.RecordID = ( this.DataList[this.FormController.RecordIndex] as BaseSqlPersistable ).ID;
			this.SetupFormView( this.FormController.RecordID );
			this.Page.DataBind();
		}

		protected void SetupFormView( int id ) {
			this.DBItem = this.CreateDBItem( id );
			this.Grid.EditItemIndex = -1;
			this.GridPanel.Visible = false;
			this.SearchPanel.Visible = false;
			this.FormPanel.Visible = true;
			this.FormController.RecordID = id;
			this.SetupEditForm( id );
			this.FormController.SetupControls();
		}

		protected void SetupFormErrorMessages() {
			//this.ClearDBError();
			if ( this.DBItem != null ) {
				foreach ( string msg in this.DBItem.ValidationMessages ) {
					this.AddErrorMessage( msg );
				}
			}
		}

		public virtual bool SaveFormRecord() {
			if ( !( this.CanEditRecord || this.CanAddRecord ) ) {
				return ( true );
			}
			this.OnFormControlsRead( this, new EventArgs() );
			this.ClearDBError();
			this.Page.Validate();
			if ( !this.Page.IsValid )
				return ( false );
			try {
				if ( this.DBItem.Persist() ) {
					this.FormController.RecordID = this.DBItem.ID;
					this.OnFormItemPersisted( this, new EventArgs() );
				} else {
					this.OnFormControlsInit( this, new EventArgs() );
					return ( false );
				}
			} catch ( Exception e ) {
				DBError( e.Message, e.Source, "SqlDataPage.SaveFormRecord" );
				return ( false );
			}
			return ( true );
		}

		#endregion

		#region Downloads

		private string tabDelimit( string value ) {
			return ( value.Replace( "\t", " " ).Replace( "\r", " " ).Replace( "\n", " " ).Trim() + "\t" );
		}

		private string csvDelimit( string value ) {
			return ( "\"" + value.Replace( "\"", "\"\"" ).Trim() + "\"," );
		}

		//protected virtual void DoDatabaseDownload() {
		//  // Reload the dataview
		//  this.BindGrid();
		//  // Set up
		//  int colCount = this.CurrentDataView.Table.Columns.Count;
		//  StringBuilder sb = new StringBuilder();
		//  string crlf = "\r\n";
		//  // Write the column-titles
		//  for ( int c = 0 ; c < colCount ; c++ ) {
		//    sb.Append( tabDelimit( this.CurrentDataView.Table.Columns[c].ColumnName ) );
		//  }
		//  sb.Append( crlf );
		//  // Write the data for each row
		//  foreach ( DataRowView row in this.CurrentDataView ) {
		//    for ( int c = 0 ; c < colCount ; c++ )
		//      sb.Append( tabDelimit( row[this.CurrentDataView.Table.Columns[c].ColumnName].ToString() ) );
		//    sb.Append( crlf );
		//  }
		//  // Set up the response stream & send the data...
		//  this.Page.Response.Clear();
		//  this.Page.Response.ContentType = "Application/x-msexcel"; // for Microsoft Excel files
		//  this.Page.Response.AppendHeader( "Content-Disposition", string.Format( "attachment; filename={0}.txt", this.DBItemPluralLabel() ) );
		//  this.Page.Response.AppendHeader( "Content-Length", sb.Length.ToString() );
		//  this.Page.Response.Write( sb.ToString() );
		//  this.Page.Response.Flush();
		//}

		#endregion

		#region Public

		public virtual void InitPanel() {
			this.EnsureChildControls();
			this.Grid.EditItemIndex = -1;
			this.GridPanel.Visible = true;
			this.SearchPanel.Visible = this.CanSearch;
			this.FormPanel.Visible = false;
			this.FormController.RecordIndex = -1;
			this.OnSearchControlsInit( this, new EventArgs() );
			if ( this.ShowAllOnLoad ) {
				this.BindGrid();
			} else {
				this.PreBindSetup();
				this.PostBindSetup();
			}
		}

		public virtual void RaisePostBackEvent( String arg ) {
			string[] args = arg.Split( ';' );
			int recID = Convert.ToInt32( args[0] );
			if ( args.Length == 3 ) {
				int recIndex = Convert.ToInt32( args[1] );
				int colIndex = Convert.ToInt32( args[2] );
				if ( this.UseSelectableRows ) {
					if ( recIndex == this.grid.SelectedIndex ) {
						// We clicked on the selected row - edit the record
						this.EditGridRecord( recID, recIndex );
					} else {
						// We clicked on an unselected row - select it
						this.grid.SelectedIndex = recIndex;
					}
				} else {
					// We clicked on an item-row ...
					if ( this.IncludeReorderControls ) {
						int recCount = this.DataList == null ? this.grid.DataKeys.Count : this.DataList.Count;
						int reorderUpCol = this.grid.Columns.Count;
						int reorderDownCol = reorderUpCol + 1;
						if ( colIndex == reorderUpCol && recIndex > 0 ) {
							this.OnReorderUp( new StarrTech.WebTools.Controls.DataGridClickEventArgs( recID, recIndex ) );
						} else if ( colIndex == reorderDownCol && recIndex < recCount - 1 ) {
							this.OnReorderDown( new StarrTech.WebTools.Controls.DataGridClickEventArgs( recID, recIndex
								) );
						} else {
							// Edit the record
							this.EditGridRecord( recID, recIndex );
						}
					} else {
						// Edit the record
						this.EditGridRecord( recID, recIndex );
					}
				}
			} else {
				// Only 1 arg - we clicked on a column-header and this is it's column-index
				string newSort = this.Grid.Columns[recID].SortExpression;
				this.SortBySortExpression( newSort );
			}
		}

		public void EditGridRecord( int RecID, int RecIndex ) {
			this.FormController.RecordID = RecID;
			this.FormController.RecordIndex = RecIndex + ( this.Grid.CurrentPageIndex * this.Grid.PageSize );
			this.EditRecordForm();
		}

		public void BindGrid() {
			PreBindSetup();
			OnSearchControlsRead( this, new EventArgs() );
			try {
				this.DataList = this.DBItemDoSearchList();
				this.Grid.DataSource = this.DataList;
				this.Grid.DataBind();
				PostBindSetup();
			} catch ( Exception e ) {
				DBError( e.Message, e.Source, this.DBItemGetSearchSql() );
			} finally {
				this.OnDataBindCompleted( this, new EventArgs() );
			}
		}

		private void PreBindSetup() {
			if ( this.SearchController != null ) {
				if ( this.SearchControls != null ) {
					this.SearchController.StatusMessage = String.Format( Messages.SearchMessage, this.DBItemPluralLabel() );
				} else {
					if ( this.CanAddRecord ) {
						this.SearchController.StatusMessage = String.Format( Messages.NewButtonMessage, this.DBItemSingularLabel() );
					} else {
						this.SearchController.StatusMessage = "";
						this.SearchController.NewButton.Visible = false;
					}
				}
			}
			this.GridPageController.StatusMessage = "";
			this.GridController.StatusMessage = "";
			this.GridPageController.Visible = this.IncludeDownloadButton;
			this.Grid.Visible = false;

			if ( this.GridController.SortExpression != "" ) {
				this.DBItemSetSortExpression( this.GridController.SortExpression + " " + this.GridController.SortOrder );
			}
			for ( int c = 0 ; c < this.Grid.Columns.Count ; c++ ) {
				if ( !this.AllowUserSorting ) {
					this.Grid.Columns[c].HeaderStyle.CssClass = "unsorted";
				} else if ( this.Grid.Columns[c].SortExpression == this.GridController.SortExpression ) {
					this.Grid.Columns[c].HeaderStyle.CssClass = "sorted" + this.GridController.SortOrder;
				} else {
					this.Grid.Columns[c].HeaderStyle.CssClass = "unsorted";
				}
			}
		}

		private void PostBindSetup() {
			if ( this.DataList != null ) {
				this.FormController.RecordCount = this.DataList.Count;
			} else {
				this.FormController.RecordCount = 0;
			}
			int firstRec = this.Grid.CurrentPageIndex * this.Grid.PageSize + 1;
			int lastRec = firstRec + this.Grid.PageSize - 1;
			lastRec = ( lastRec > this.FormController.RecordCount ? this.FormController.RecordCount : lastRec );
			if ( this.IncludeGridStatusMessage ) {
				if ( this.FormController.RecordCount == 0 ) {
					this.GridController.StatusMessage =
						String.Format( Messages.ListResultsMessage, this.FormController.RecordCount, this.DBItemPluralLabel() );
				} else if ( this.FormController.RecordCount > 1 ) {
					this.GridController.StatusMessage =
						String.Format( Messages.ListResultsMessage, this.FormController.RecordCount, this.DBItemPluralLabel() )
						+ String.Format( Messages.ListCurrentRecsMessage, this.DBItemPluralLabel(), firstRec, lastRec );
				} else {
					this.GridController.StatusMessage =
						String.Format( Messages.ListResultsMessage, this.FormController.RecordCount, this.DBItemSingularLabel() );
				}
			} else {
				this.GridController.StatusMessageLabel.Visible = false;
			}
			if ( this.FormController.RecordCount > 0 ) {
				this.Grid.Visible = true;
				if ( this.FormController.RecordCount > this.Grid.PageSize ) {
					this.GridPageController.Visible = true;
					this.GridPageController.StatusMessage =
						String.Format( Messages.ListPageMessage, this.Grid.CurrentPageIndex + 1, this.Grid.PageCount );
				}
			} else {
				this.GridPageController.Visible = false;
			}
			this.ClearDBError();
			this.GridPageController.SetupControls();
		}

		public void AddErrorMessage( string msg ) {
			if ( msg != "" ) {
				this.ErrorMessage += string.Format( "<li>{0}</li>", msg );
			}
		}

		public void AddErrorMessage( bool isValid, BaseValidator validator ) {
			if ( !isValid )
				this.AddErrorMessage( validator.ErrorMessage );
		}

		public void ClearDBError() {
			this.ErrorMessage = "";
		}

		#endregion

		#endregion

	}

	#region Template-Container Classes

	public class SearchControlsContainer : Control, INamingContainer {
		private AdminDataGrid parent;

		public SearchControlsContainer( AdminDataGrid parent ) {
			this.parent = parent;
		}
	}

	public class FormControlsContainer : Control, INamingContainer {
		private AdminDataGrid parent;

		public FormControlsContainer( AdminDataGrid parent ) {
			this.parent = parent;
		}
	}

	public class DataGridContainer : Control, INamingContainer {
		private AdminDataGrid parent;

		public DataGridContainer( AdminDataGrid parent ) {
			this.parent = parent;
		}
	}

	#endregion

}
