using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Buttons and such for the DBUI AdminDataGrid Form Panel
	/// </summary>
	public class DBFormController : Table {
		#region Properties 
		
		protected AdminDataGrid parent;
		
		public int RecordID {
			get { 
				object id = ViewState["RecordID"];
				return ( id == null ? -1 : (int) id ); 
			}
			set { ViewState["RecordID"] = value; }
		}
		
		public int RecordIndex {
			get { 
				object idx = ViewState["RecordIndex"];
				return ( idx == null ? -1 : (int) idx ); 
			}
			set { ViewState["RecordIndex"] = value; }
		}
		
		public int RecordCount {
			get { 
				object count = ViewState["RecordCount"];
				return ( count == null ? 0 : (int) count ); 
			}
			set { ViewState["RecordCount"] = value; }
		}
		
		public Label StatusMessageLabel {
			get {
				this.EnsureChildControls();
				return ( statusMessageLabel );
			}
			set { statusMessageLabel = value; }
		}
		private Label statusMessageLabel;
		
		public string StatusMessage {
			get { 
				this.EnsureChildControls();
				return ( StatusMessageLabel.Text ); 
			}
			set { 
				this.EnsureChildControls();
				StatusMessageLabel.Text = value.Replace(" ", "&nbsp;" ); 
			}
		}

		public System.Web.UI.WebControls.Button DeleteButton {
			get {
				this.EnsureChildControls();
				return ( deleteButton );
			}
			set { deleteButton = value; }
		}
		private System.Web.UI.WebControls.Button deleteButton;

		public System.Web.UI.WebControls.Button CancelButton {
			get {
				this.EnsureChildControls();
				return ( cancelButton );
			}
			set { cancelButton = value; }
		}
		private System.Web.UI.WebControls.Button cancelButton;
		
		public StarrTech.WebTools.Controls.NavButton FirstButton {
			get {
				this.EnsureChildControls();
				return ( firstButton );
			}
			set { firstButton = value; }
		}
		private StarrTech.WebTools.Controls.NavButton firstButton;
		
		public StarrTech.WebTools.Controls.NavButton PrevButton {
			get {
				this.EnsureChildControls();
				return ( prevButton );
			}
			set { prevButton = value; }
		}
		private StarrTech.WebTools.Controls.NavButton prevButton;
		
		public StarrTech.WebTools.Controls.NavButton NextButton {
			get {
				this.EnsureChildControls();
				return ( nextButton );
			}
			set { nextButton = value; }
		}
		private StarrTech.WebTools.Controls.NavButton nextButton;
		
		public StarrTech.WebTools.Controls.NavButton LastButton {
			get {
				this.EnsureChildControls();
				return ( lastButton );
			}
			set { lastButton = value; }
		}
		private StarrTech.WebTools.Controls.NavButton lastButton;

		public System.Web.UI.WebControls.Button SaveButton {
			get {
				this.EnsureChildControls();
				return ( saveButton );
			}
			set { saveButton = value; }
		}
		private System.Web.UI.WebControls.Button saveButton;
		
		public string EntityName {
			get { return ( this.parent.DBItemSingularLabel() ); }
		}
		
		public bool CanEditRecord {
			get { return ( this.parent.CanEditRecord ); }
			set { this.parent.CanEditRecord = value; } 
		}
		
		public bool CanDeleteRecord {
			get { return ( this.parent.CanDeleteRecord ); }
			set { this.parent.CanDeleteRecord = value; }
		}
					
		public string DeleteClientScript = string.Format( ClientScripts.ConfirmDeleteScript, "record" );
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion

		#region ButtonClick Event
		/// <summary>
		/// Event raised when any of the button controls are clicked.
		/// </summary>
		public event CommandEventHandler ButtonClick {
			add { Events.AddHandler( ButtonClickEventKey, value ); }
			remove { Events.RemoveHandler( ButtonClickEventKey, value ); }
		}
		protected virtual void OnButtonClick( Object sender, CommandEventArgs args ) {
			CommandEventHandler btnClickHandler = (CommandEventHandler) Events[ButtonClickEventKey];
			if ( btnClickHandler != null ) btnClickHandler( sender, args );
		}
		private static readonly object ButtonClickEventKey = new object();
		#endregion
		
		#region Constructor
		
		public DBFormController( AdminDataGrid Parent ) {
			this.parent = Parent;
			this.RecordID = -1;
			this.RecordIndex = -1;
			this.CellSpacing = 3;
			this.CellPadding = 0;
			this.BorderStyle = BorderStyle.None;
		}
		
		
		#endregion
		
		#region Protected Methods
		
		protected override void CreateChildControls() {
			// Clear the Rows collection (just to be anal)
			this.Rows.Clear();
						
			TableRow row = new TableRow();
			// Initialize our controls
			this.statusMessageLabel = new Label();
			this.StatusMessageLabel.Style["margin"] = "3px";
			this.StatusMessageLabel.Text = "";
			if ( this.parent.Header1CssClass != "" ) {
				this.StatusMessageLabel.CssClass = this.parent.Header2CssClass;
			} else {
				this.StatusMessageLabel.Font.Name = "Verdana";
				this.StatusMessageLabel.Font.Size = 10;
				this.StatusMessageLabel.Font.Bold = true;
			}
			
			if ( this.CanDeleteRecord ) {
				this.deleteButton = new System.Web.UI.WebControls.Button();
				//this.DeleteButton.ID = ID + "_" + CommandNames.FormPageDelete + "_btn";
				this.DeleteButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageDelete);
				this.DeleteButton.Text = ButtonLabels.FormPageDelete;
				this.DeleteButton.CausesValidation = false;
				this.DeleteButton.OnClientClick = DeleteClientScript;
				this.DeleteButton.Command += new CommandEventHandler(OnButtonClick);
				this.DeleteButton.CommandName = CommandNames.FormPageDelete;
				this.DeleteButton.ToolTip = ButtonTooltips.FormPageDelete;
			}
			this.cancelButton = new System.Web.UI.WebControls.Button();
			//this.CancelButton.ID = ID + "_" + CommandNames.FormPageCancel + "_btn";
			this.CancelButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageCancel );
			this.CancelButton.Text = ButtonLabels.FormPageCancel;
			this.CancelButton.CausesValidation = false;
			this.CancelButton.Command += new CommandEventHandler(OnButtonClick);
			this.CancelButton.CommandName = CommandNames.FormPageCancel;
			this.CancelButton.ToolTip = ButtonTooltips.FormPageCancel;
			
			this.firstButton = new StarrTech.WebTools.Controls.NavButton();
			this.FirstButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageFirst );
			this.FirstButton.Text = ButtonLabels.FormPageFirst;
			this.FirstButton.Command += new CommandEventHandler(OnButtonClick);
			this.FirstButton.CommandName = CommandNames.FormPageFirst;
			this.FirstButton.ToolTip = ButtonTooltips.FormPageFirst;
			
			this.prevButton = new StarrTech.WebTools.Controls.NavButton();
			this.PrevButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPagePrev );
			this.PrevButton.Text = ButtonLabels.FormPagePrev;
			this.PrevButton.Command += new CommandEventHandler(OnButtonClick);
			this.PrevButton.CommandName = CommandNames.FormPagePrev;
			this.PrevButton.ToolTip = ButtonTooltips.FormPagePrev;
			
			this.nextButton = new StarrTech.WebTools.Controls.NavButton();
			this.NextButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageNext );
			this.NextButton.Text = ButtonLabels.FormPageNext;
			this.NextButton.Command += new CommandEventHandler(OnButtonClick);
			this.NextButton.CommandName = CommandNames.FormPageNext;
			this.NextButton.ToolTip = ButtonTooltips.FormPageNext;
			
			this.LastButton = new StarrTech.WebTools.Controls.NavButton();
			this.LastButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageLast );
			this.LastButton.Text = ButtonLabels.FormPageLast;
			this.LastButton.Command += new CommandEventHandler(OnButtonClick);
			this.LastButton.CommandName = CommandNames.FormPageLast;
			this.LastButton.ToolTip = ButtonTooltips.FormPageLast;

			this.SaveButton = new System.Web.UI.WebControls.Button();
			this.SaveButton.ID = string.Format( "{0}_{1}_btn", ID, CommandNames.FormPageSave );
			this.SaveButton.Text = ButtonLabels.FormPageSave;
			this.SaveButton.Command += new CommandEventHandler(OnButtonClick);
			this.SaveButton.CommandName = CommandNames.FormPageSave;
			this.SaveButton.ToolTip = ButtonTooltips.FormPageSave;
			
			// Create the status-message cell
			TableCell cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Left; 
			cell.VerticalAlign = VerticalAlign.Middle;
			cell.Controls.Add( StatusMessageLabel );
			row.Cells.Add( cell );
			
			// Create the buttons-cell
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Right; 
			cell.VerticalAlign = VerticalAlign.Middle;
			cell.Width = new Unit( "100%" );
			cell.Controls.Add( SaveButton );
			Literal spacer = new Literal();
			spacer.Text = "&nbsp;";
			cell.Controls.Add( spacer );
			cell.Controls.Add( CancelButton );
			spacer = new Literal();
			spacer.Text = "&nbsp;";
			cell.Controls.Add( spacer );
			cell.Controls.Add( FirstButton );
			cell.Controls.Add( PrevButton );
			cell.Controls.Add( NextButton );
			cell.Controls.Add( LastButton );
			if ( this.CanDeleteRecord ) {
				spacer = new Literal();
				spacer.Text = "&nbsp;";
				cell.Controls.Add( spacer );
				cell.Controls.Add( DeleteButton );
			}
			// Assemble the row and add it to our Rows collection
			row.Cells.Add( cell );
			this.Rows.Add( row );
			
			this.SetupControls();
		}
			
		
		#endregion
		
		#region Public Methods
		
		public virtual void SetupControls() {
			if ( RecordID == -1 ) {
				StatusMessage = "Add New " + EntityName + "..."; 
				FirstButton.Visible = false; 
				PrevButton.Visible = false; 
				NextButton.Visible = false; 
				LastButton.Visible = false;
				if ( DeleteButton != null ) {
					DeleteButton.Visible = false;
				}
				SaveButton.Visible = true;
				CancelButton.Visible = true;
				SaveButton.Text = ButtonLabels.FormPageSave;
			} else {
				StatusMessage = String.Format( 
					( this.CanEditRecord ? Messages.FormStatusEditMessage : Messages.FormStatusViewMessage ), 
					EntityName, RecordIndex + 1, RecordCount, RecordID 
					);
				if ( CanEditRecord ) {
					FirstButton.Visible = true;
					PrevButton.Visible = true;
					NextButton.Visible = true;
					LastButton.Visible = true;
					CancelButton.Visible = true;
					SaveButton.Visible = true;
					SaveButton.Text = ButtonLabels.FormPageSave;
					FirstButton.CausesValidation = true;
					PrevButton.CausesValidation = true;
					NextButton.CausesValidation = true;
					LastButton.CausesValidation = true;
					SaveButton.CausesValidation = true;
				} else {
					FirstButton.Visible = true;
					PrevButton.Visible = true;
					NextButton.Visible = true;
					LastButton.Visible = true;
					if ( this.CanDeleteRecord ) {
						if ( DeleteButton != null ) {
							DeleteButton.Visible = false;
						}
					}
					CancelButton.Visible = false;
					SaveButton.Visible = true;
					SaveButton.Text = ButtonLabels.FormPageDone;
					FirstButton.CausesValidation = false;
					PrevButton.CausesValidation = false;
					NextButton.CausesValidation = false;
					LastButton.CausesValidation = false;
					SaveButton.CausesValidation = false;
				}
				if ( this.CanDeleteRecord ) {
					if ( DeleteButton != null ) {
						DeleteButton.Visible = true;
					}
				}
				bool canGoBack = ( RecordIndex > 0 );
				bool canGoNext = ( RecordIndex < (RecordCount - 1) );
				FirstButton.Enabled = canGoBack;
				PrevButton.Enabled = canGoBack;
				LastButton.Enabled = canGoNext;
				NextButton.Enabled = canGoNext;
				SaveButton.Enabled = true;
				CancelButton.Enabled = true;
			}
		}
		
		
		public virtual void EnableButtons( bool enabled ) {
			if ( this.CanDeleteRecord ) DeleteButton.Enabled = enabled;
			CancelButton.Enabled = enabled;
			FirstButton.Enabled = enabled;
			PrevButton.Enabled = enabled;
			NextButton.Enabled = enabled;
			LastButton.Enabled = enabled;
			SaveButton.Enabled = enabled;
		}
		
		
		#endregion
		
	}
	
	
}
