using System;

namespace StarrTech.WebTools.DBUI {
	/// <summary>
	/// Static strings for use in identifying event commands
	/// </summary>
	public class CommandNames {
		public const string ShowAll = "showall";
		public const string Search = "search";
		public const string Sort = "sort";
		public const string ToggleSearchPanel = "togglesearchpanel";
		public const string NewRecord = "newrecord";
		public const string Download = "download";
		public const string GridPageFirst = "gridpagefirst";
		public const string GridPagePrev = "gridpageprev";
		public const string GridPageNext = "gridpagenext";
		public const string GridPageLast = "gridpagelast";
		public const string GridPagePick = "gridpagepick";
		public const string FormPageFirst = "formpagefirst";
		public const string FormPagePrev = "formpageprev";
		public const string FormPageNext = "formpagenext";
		public const string FormPageLast = "formpagelast";
		public const string FormPageDelete = "formpagedelete";
		public const string FormPageSave = "formpagesave";
		public const string FormPageCancel = "formpagecancel";
	}
	
	
	/// <summary>
	/// Static strings for labeling buttons
	/// </summary>
	public class ButtonLabels {
		
		public const string Search = "Search";
		public const string ShowAll = "Show All";
		public const string NewRecord = "New";
		public const string EditRecord = "Edit";
		public const string ViewRecord = "View";
		public const string Download = "Download";
		
		public const string FormPageDelete = "Delete";
		public const string FormPageSave = "Save";
		public const string FormPageCancel = "Cancel";
		public const string FormPageDone = "Done";
		
		public static string FormPageFirst {
			get {
				if ( SitePrefs.IsIE ) return ( "9" );
				else return ( "|<" );
			}
		}
		public static string FormPagePrev {
			get {
                if ( SitePrefs.IsIE ) return ( "7" );
				else return ( "<" );
			}
		}
		public static string FormPageNext {
			get {
                if ( SitePrefs.IsIE ) return ( "8" );
				else return ( ">" );
			}
		}
		public static string FormPageLast {
			get {
                if ( SitePrefs.IsIE ) return ( ":" );
				else return ( ">|" );
			}
		}
		
		
		public static string GridPageFirst {
			get {
                if ( SitePrefs.IsIE ) return ( "9" );
				else return ( "|<" );
			}
		}
		public static string GridPagePrev {
			get {
                if ( SitePrefs.IsIE ) return ( "7" );
				else return ( "<" );
			}
		}
		public static string GridPageNext {
			get {
                if ( SitePrefs.IsIE ) return ( "8" );
				else return ( ">" );
			}
		}
		public static string GridPageLast {
			get {
                if ( SitePrefs.IsIE ) return ( ":" );
				else return ( ">|" );
			}
		}
			

	}
	
	
	/// <summary>
	/// Static strings for button tooltips
	/// </summary>
	public class ButtonTooltips {
		public const string Search = "Execute the search, and show the results below";
		public const string ShowAll = "Clear the search controls and show all records";
		public const string NewRecord = "Add a new record to the database";
		public const string Download = "Download the results to your computer";
		public const string GridPageFirst = "Show the first page of records";
		public const string GridPagePrev = "Show the previous page of records";
		public const string GridPageNext = "Show the next page of records";
		public const string GridPageLast = "Show the last page of records";
		public const string GridPagePicker = "Show a specific page of records";
		public const string FormPageFirst = "Show the first record";
		public const string FormPagePrev = "Show the previous record";
		public const string FormPageNext = "Show the next record";
		public const string FormPageLast = "Show the last record";
		public const string FormPageDelete = "Delete this record";
		public const string FormPageSave = "Save this record and return to the list view";
		public const string FormPageCancel = "Return to the list view without saving changes";
	}
	
	
	/// <summary>
	/// Static integers for use in sizing buttons
	/// </summary>
	public class ButtonSizes { 
		public const int StandardButtonWidth = 70;
		public const int NavButtonWidth = 30;
	}
	
	
	/// <summary>
	/// Static strings for titling and messages
	/// </summary>
	public class Messages { 
		public const string SearchMessage = "Search for {0}";
		public const string NewButtonMessage = "Click the New button to add a new {0}";
		public const string ListPageMessage = "Page {0} of {1}.";
		public const string FormStatusEditMessage = "Editing {0} {1} of {2} (ID: {3}) ...";
		public const string FormStatusViewMessage = "Viewing {0} {1} of {2} (ID: {3}) ...";
		public const string DBErrorMessage = "<p>Database Error: {0} [{1}]<br />{2}</p>";
		public const string ListResultsMessage = "{0} {1} Found";
		public const string ListCurrentRecsMessage = " - Viewing {0} {1} to {2}.";
	}

	
	/// <summary>
	/// Javascript strings
	/// </summary>
	public class ClientScripts {
		public const string ConfirmDeleteScript = "return confirm( 'Are you sure you want to delete this {0}?' );";
	}	
	
	
}
