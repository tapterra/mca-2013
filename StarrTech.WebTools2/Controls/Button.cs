using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// A customized button control, adds the OnClientClick property for assigning client-side click-handler script
	/// </summary>
	//	[Obsolete( "Not a CSS-compatible control - No longer needed as of ASP.NET 2.0", false )]
	public class Button : System.Web.UI.WebControls.Button {

		public override string OnClientClick {
			get { return ( onclientclick ); }
			set {
				onclientclick = value;
				this.CausesValidation = ( string.IsNullOrEmpty( value ) );
			}
		}
		private string onclientclick = "";

		/// <summary>
		/// An alias for the inherited Text property
		/// </summary>
		public string Label {
			get { return ( this.Text ); }
			set { this.Text = value; }
		}


		public Button() {
			// This is pretty useless considering the Enabled property won't be set until after this is called
			Style.Add( "Cursor", Enabled ? "hand" : "default" );
		}


		/// <summary>
		/// Adds support for the OnClientClick attribute (client-side js click handler)
		/// </summary>
		/// <param name="writer">The current response's HtmlTextWriter</param>
		protected override void AddAttributesToRender( HtmlTextWriter writer ) {
			base.AddAttributesToRender( writer );
			if ( OnClientClick != "" ) {
				writer.AddAttribute( "onclick", OnClientClick );
			}
		}

	}


	/// <summary>
	/// Custom button for WebDings labels
	/// </summary>
	public class NavButton : System.Web.UI.WebControls.Button {

		// Constructor
		public NavButton() : base() {
			this.Width = new Unit( "30px" );
			this.Font.Names = new string[] { "WebDings", "Verdana", "Arial", "Helvetica", "sans-serif" };
			this.Font.Size = new FontUnit( "10pt" );
			this.Font.Bold = false;
			this.Style.Add( "Cursor", this.Enabled ? "hand" : "default" );
		}

	}


}
