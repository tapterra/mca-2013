using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// A labeled HtmlInputFile control
	/// </summary>
	public class LabeledFileBox : FileUpload {
		
		#region Properties
		
		/// <summary>
		/// The LabelFramer object to handle the label & its position
		/// </summary>
		public  LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();
		
		/// <summary>
		/// Color used to fill in the content area when the control is disabled
		/// </summary>
		public Color DisabledBackColor {
			get { return ( this.Frame.DisabledBackColor ); }
			set { this.Frame.DisabledBackColor = value; }
		}

		/// <summary>
		/// Color used for text, etc. in the content area when the control is disabled
		/// </summary>
		public Color DisabledForeColor {
			get { return ( this.Frame.DisabledForeColor ); }
			set { this.Frame.DisabledForeColor = value; }
		}
		
		/// <summary>
		/// Exposes the Frame's Label control
		/// </summary>
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}
		
		/// <summary>
		/// Exposes the TableStyle for the frame
		/// </summary>
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}
					
		/// <summary>
		/// Exposes the TableItemStyle for the frame label
		/// </summary>
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}
					
		/// <summary>
		/// Exposes the TableItemStyle for the frame content 
		/// </summary>
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}

		/// <summary>
		/// Overriden to set up the frame
		/// </summary>
		public override Unit Width {
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}
		
		#endregion
		
		#region Constructor
		
		/// <summary>
		/// Set up the basic display props to fit with the other Labeled controls
		/// </summary>
		public LabeledFileBox() : base() {}
		
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Generate the HTML
		/// </summary>
		/// <param name="writer">HtmlTextWriter for the current Response</param>
		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			Frame.PreRender( writer );
			//if ( Enabled ) {
				base.Render( writer );
			//} else {
			//	Frame.RenderDisabled( writer, Font, "" );
			//}
			Frame.PostRender( writer );
		}
		
		#endregion
		
	}
	
}
