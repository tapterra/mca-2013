using System;
using System.Collections;
using System.Collections.Specialized;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// DataGrid subclass that adds DHTML-clickable rows.
	/// </summary>
	public class DataGrid : System.Web.UI.WebControls.DataGrid, IPostBackEventHandler {

		#region Properties

		public Color RowHilightColor {
			get {
				object obj = ViewState["RowHilightColor"];
				return ( obj == null ? ColorTranslator.FromHtml( "#ffdddd" ) : (Color) obj );
			}
			set { ViewState["RowHilightColor"] = value; }
		}

		public Color ColHilightColor {
			get {
				object obj = ViewState["ColHilightColor"];
				return ( obj == null ? ColorTranslator.FromHtml( "#bbbbbb" ) : (Color) obj );
			}
			set { ViewState["ColHilightColor"] = value; }
		}
		
		public bool AllowUserSorting {
			get {
				object obj = ViewState["AllowUserSorting"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["AllowUserSorting"] = value; }
		}

		public bool IncludeReorderControls {
			get {
				object obj = ViewState["IncludeReorderControls"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["IncludeReorderControls"] = value; }
		}

		public bool UseClickableRows {
			get {
				object obj = ViewState["UseClickableRows"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["UseClickableRows"] = value; }
		}

		public bool UseSelectableRows {
			get {
				object obj = ViewState["UseSelectableRows"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["UseSelectableRows"] = value; }
		}

		public int LastRowClickedIndex {
			get {
				object obj = ViewState["LastRowClickedIndex"];
				return ( obj == null ? -1 : (int) obj );
			}
			set { ViewState["LastRowClickedIndex"] = value; }
		}

		public int LastColumnClickedIndex {
			get {
				object obj = ViewState["LastColumnClickedIndex"];
				return ( obj == null ? -1 : (int) obj );
			}
			set { ViewState["LastColumnClickedIndex"] = value; }
		}

		public string LastRowClickedKey {
			get {
				if ( this.LastRowClickedIndex < 0 )
					return ( "" );
				if ( this.DataKeyField.Length == 0 ) {
					return ( this.Items[this.LastRowClickedIndex].Cells[0].Text );
				} else {
					return ( this.DataKeys[this.LastRowClickedIndex].ToString() );
				}
			}
		}

		protected string MoveUpImageUrl {
			get {
				if ( this.moveUpImageUrl == "" ) {
					this.moveUpImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveUpImagePath );
				}
				return ( this.moveUpImageUrl );
			}
		}
		private string moveUpImageUrl = "";
		private string moveUpImagePath = "StarrTech.WebTools.Resources.treeui.move-up.gif";

		protected string MoveDownImageUrl {
			get {
				if ( this.moveDownImageUrl == "" ) {
					this.moveDownImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveDownImagePath );
				}
				return ( this.moveDownImageUrl );
			}
		}
		private string moveDownImageUrl = "";
		private string moveDownImagePath = "StarrTech.WebTools.Resources.treeui.move-down.gif";

		#endregion

		#region Constructor

		public DataGrid() : base() { }

		#endregion

		#region Events management

		protected override void OnInit( EventArgs e ) {
			this.ItemDataBound += new DataGridItemEventHandler( this.GridItemBound );
			this.ItemCreated += new DataGridItemEventHandler( this.GridItemCreated );
			base.OnInit( e );
		}


		protected void GridItemCreated( object sender, DataGridItemEventArgs args ) {
			DataGridItem row = args.Item;
			// Modify the column-headers...
			if ( row.ItemType == ListItemType.Header ) {
				if ( this.AllowUserSorting ) {
					// Set up the column-headers for click-to-sort
					int c = 0;
					foreach ( System.Web.UI.WebControls.TableCell cell in row.Cells ) {
						if ( c >= this.Columns.Count ) {
							break;
						}
						if ( !string.IsNullOrEmpty( this.Columns[c].SortExpression ) ) {
							cell.Attributes["onmouseover"] = string.Format(
								"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.ColHilightColor )
							);
							cell.Attributes["onmouseout"] = string.Format(
								"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.HeaderStyle.BackColor )
							);
							cell.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink( this, c.ToString() );
						}
						c++;
					}
				}
				if ( this.IncludeReorderControls ) {
					// Add extra columns for the re-order controls
					System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
					cell.ColumnSpan = 2;
					cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
					row.Cells.Add( cell );
				}
			}
			if ( ( row.ItemType == ListItemType.Item ) || ( row.ItemType == ListItemType.AlternatingItem ) || ( row.ItemType == ListItemType.SelectedItem ) ) {
				bool rowIsSelected = row.ItemType == ListItemType.SelectedItem;
				if ( row.ItemIndex >= 0 ) {
					// Set the "mouse-over" color
					if ( rowIsSelected ) {
						row.Attributes["onmouseover"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';this.style.color='{1}';",
								ColorTranslator.ToHtml( this.SelectedItemStyle.BackColor ), 
								ColorTranslator.ToHtml( this.SelectedItemStyle.ForeColor )
						);
					} else {
						row.Attributes["onmouseover"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
								ColorTranslator.ToHtml( this.RowHilightColor )
						);
					}
					// Set up the "mouse-out" color
					if ( rowIsSelected ) {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.SelectedItemStyle.BackColor )
						);
					} else if ( row.ItemType == ListItemType.Item ) {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.ItemStyle.BackColor )
						);
					} else {
						row.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.AlternatingItemStyle.BackColor )
						);
					}
					// Add extra columns for the re-order controls
					if ( this.IncludeReorderControls ) {
						IList recs = this.DataSource as IList;
						int recCount = recs != null ? recs.Count : this.DataKeys.Count;
						System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
						LiteralControl lit = new LiteralControl();
						lit.Text = row.ItemIndex == 0 ? "&nbsp;" : string.Format( "<img src='{0}' alt='Move this item up' />", this.MoveUpImageUrl );
						cell.Width = new Unit( "15" );
						cell.HorizontalAlign = HorizontalAlign.Center;
						cell.Controls.Add( lit );
						row.Cells.Add( cell );
						cell = new System.Web.UI.WebControls.TableCell();
						lit = new LiteralControl();
						lit.Text = row.ItemIndex == ( recCount - 1 ) ? "&nbsp;" : string.Format( "<img src='{0}' alt='Move this item down' />", this.MoveDownImageUrl );
						cell.Width = new Unit( "15" );
						cell.HorizontalAlign = HorizontalAlign.Center;
						cell.Controls.Add( lit );
						row.Cells.Add( cell );
					}
				}
			}
		}

		protected void GridItemBound( Object sender, DataGridItemEventArgs args ) {
			DataGridItem row = args.Item;
			if ( this.UseClickableRows ) {
				if ( row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem || row.ItemType == ListItemType.SelectedItem ) {
					// Set up the row click-back script
					int colIndex = 0;
					foreach ( System.Web.UI.WebControls.TableCell cell in row.Cells ) {
						string attribValue = this.Page.ClientScript.GetPostBackClientHyperlink( this, string.Format( "{0};{1}", args.Item.ItemIndex, colIndex ) );
						cell.Attributes["onclick"] = attribValue;
						colIndex++;
					}
				}
			}
		}

		public virtual void RaisePostBackEvent( String arg ) {
			string[] args = arg.Split( ';' );
			int recIndex = Convert.ToInt32( args[0] );
			if ( args.Length == 2 ) {
				int colIndex = Convert.ToInt32( args[1] );
				if ( this.UseSelectableRows ) {
					// We clicked on a row - make sure it's selected
					this.SelectedIndex = recIndex;
					this.LastRowClickedIndex = recIndex;
					this.OnRowClicked( this, new EventArgs() );
				} else {
					this.SelectedIndex = -1;
					// We clicked on an item-row ...
					if ( this.IncludeReorderControls ) {
						int recCount = this.DataKeys.Count;
						int reorderUpCol = this.Columns.Count;
						int reorderDownCol = reorderUpCol + 1;
						int recordID = -1;
						Int32.TryParse( this.DataKeys[recIndex].ToString(), out recordID );
						if ( colIndex == reorderUpCol && recIndex > 0 ) {
							this.OnReorderUp( new DataGridClickEventArgs( recordID, recIndex ) );
						} else if ( colIndex == reorderDownCol && recIndex < recCount - 1 ) {
							this.OnReorderDown( new DataGridClickEventArgs( recordID, recIndex ) );
						} else {
							this.LastRowClickedIndex = recIndex;
							this.OnRowClicked( this, new EventArgs() );
						}
					} else {
						this.LastRowClickedIndex = recIndex;
						this.OnRowClicked( this, new EventArgs() );
					}
				}
			} else {
				// Only 1 arg - we clicked on a column-header and "recIndex" is actually the column-index
				this.LastColumnClickedIndex = Convert.ToInt32( recIndex );
				this.OnColumnClicked( this, new EventArgs() );
			}
		}

		#endregion

		#region Events

		#region ReorderUp
		public event EventHandler<DataGridClickEventArgs> ReorderUp {
			add { Events.AddHandler( ReorderUpEventKey, value ); }
			remove { Events.RemoveHandler( ReorderUpEventKey, value ); }
		}
		protected virtual void OnReorderUp( DataGridClickEventArgs args ) {
			EventHandler<DataGridClickEventArgs> handler = (EventHandler<DataGridClickEventArgs>) Events[ReorderUpEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderUpEventKey = new object();
		#endregion

		#region ReorderDown
		public event EventHandler<DataGridClickEventArgs> ReorderDown {
			add { Events.AddHandler( ReorderDownEventKey, value ); }
			remove { Events.RemoveHandler( ReorderDownEventKey, value ); }
		}
		protected virtual void OnReorderDown( DataGridClickEventArgs args ) {
			EventHandler<DataGridClickEventArgs> handler = (EventHandler<DataGridClickEventArgs>) Events[ReorderDownEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderDownEventKey = new object();
		#endregion

		#region RowClicked
		public event EventHandler RowClicked {
			add { Events.AddHandler( RowClickedEventKey, value ); }
			remove { Events.RemoveHandler( RowClickedEventKey, value ); }
		}
		protected virtual void OnRowClicked( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[RowClickedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object RowClickedEventKey = new object();
		#endregion

		#region ColumnClicked
		public event EventHandler ColumnClicked {
			add { Events.AddHandler( ColumnClickedEventKey, value ); }
			remove { Events.RemoveHandler( ColumnClickedEventKey, value ); }
		}
		protected virtual void OnColumnClicked( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[ColumnClickedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object ColumnClickedEventKey = new object();
		#endregion

		#endregion

	}

	public class DataGridClickEventArgs : EventArgs {
		public int RecordID;
		public int RecordSeq;
		public DataGridClickEventArgs( int ID, int Seq ) {
			this.RecordID = ID;
			this.RecordSeq = Seq;
		}
	}

}
