using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace StarrTech.WebTools.Controls {

	public class GridView : System.Web.UI.WebControls.GridView, IPostBackEventHandler {

		#region Properties

		public Color RowHilightColor {
			get {
				object obj = ViewState["RowHilightColor"];
				return ( obj == null ? Color.FromName( rowHilightColor ) : (Color) obj );
			}
			set { ViewState["RowHilightColor"] = value; }
		}
		private string rowHilightColor = "#ffdddd";

		public Color ColHilightColor {
			get {
				object obj = ViewState["ColHilightColor"];
				return ( obj == null ? ColorTranslator.FromHtml( "#bbbbbb" ) : (Color) obj );
			}
			set { ViewState["ColHilightColor"] = value; }
		}

		public bool AllowUserSorting {
			get {
				if ( this.IncludeReorderControls ) {
					return ( false );
				}
				object obj = ViewState["AllowUserSorting"];
				return ( obj == null ? false : (bool) obj );
			}
			set {
				//base.AllowSorting = value;
				ViewState["AllowUserSorting"] = value; 
			}
		}

		public override bool AllowPaging {
			get {
				if ( this.IncludeReorderControls ) {
					return ( false );
				}
				return ( base.AllowPaging );
			}
			set { base.AllowPaging = value; }
		}

		public bool IncludeReorderControls {
			get {
				object obj = ViewState["IncludeReorderControls"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["IncludeReorderControls"] = value; }
		}

		public bool UseClickableRows {
			get {
				object obj = ViewState["UseClickableRows"];
				return ( obj == null ? true : (bool) obj );
			}
			set { ViewState["UseClickableRows"] = value; }
		}

		public bool UseSelectableRows {
			get {
				object obj = ViewState["UseSelectableRows"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["UseSelectableRows"] = value; }
		}

		public int LastRowClickedIndex {
			get {
				object obj = ViewState["LastRowClickedIndex"];
				return ( obj == null ? -1 : (int) obj );
			}
			set { ViewState["LastRowClickedIndex"] = value; }
		}

		public int LastColumnClickedIndex {
			get {
				object obj = ViewState["LastColumnClickedIndex"];
				return ( obj == null ? -1 : (int) obj );
			}
			set { ViewState["LastColumnClickedIndex"] = value; }
		}

		public string LastRowClickedKey {
			get {
				object obj = ViewState["LastRowClickedKey"];
				return ( obj == null ? "-1" : (string) obj );
			}
			set { ViewState["LastRowClickedKey"] = value; }
		}

		protected string MoveUpImageUrl {
			get {
				if ( this.moveUpImageUrl == "" ) {
					this.moveUpImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveUpImagePath );
				}
				return ( this.moveUpImageUrl );
			}
		}
		private string moveUpImageUrl = "";
		private string moveUpImagePath = "StarrTech.WebTools.Resources.treeui.move-up.gif";

		protected string MoveDownImageUrl {
			get {
				if ( this.moveDownImageUrl == "" ) {
					this.moveDownImageUrl = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.moveDownImagePath );
				}
				return ( this.moveDownImageUrl );
			}
		}
		private string moveDownImageUrl = "";
		private string moveDownImagePath = "StarrTech.WebTools.Resources.treeui.move-down.gif";

		#endregion

		public GridView() : base() {
			this.AutoGenerateColumns = false;
		}

		#region Events

		#region ReorderUp
		public event EventHandler<DataGridClickEventArgs> ReorderUp {
			add { Events.AddHandler( ReorderUpEventKey, value ); }
			remove { Events.RemoveHandler( ReorderUpEventKey, value ); }
		}
		protected virtual void OnReorderUp( DataGridClickEventArgs args ) {
			EventHandler<DataGridClickEventArgs> handler = (EventHandler<DataGridClickEventArgs>) Events[ReorderUpEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderUpEventKey = new object();
		#endregion

		#region ReorderDown
		public event EventHandler<DataGridClickEventArgs> ReorderDown {
			add { Events.AddHandler( ReorderDownEventKey, value ); }
			remove { Events.RemoveHandler( ReorderDownEventKey, value ); }
		}
		protected virtual void OnReorderDown( DataGridClickEventArgs args ) {
			EventHandler<DataGridClickEventArgs> handler = (EventHandler<DataGridClickEventArgs>) Events[ReorderDownEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object ReorderDownEventKey = new object();
		#endregion

		#region RowClicked
		public event EventHandler<DataGridClickEventArgs> RowClicked {
			add { Events.AddHandler( RowClickedEventKey, value ); }
			remove { Events.RemoveHandler( RowClickedEventKey, value ); }
		}
		protected virtual void OnRowClicked( Object sender, DataGridClickEventArgs args ) {
			this.LastRowClickedKey = args.RecordID.ToString();
			this.LastRowClickedIndex = args.RecordSeq;
			EventHandler<DataGridClickEventArgs> handler = (EventHandler<DataGridClickEventArgs>) Events[RowClickedEventKey];
			if ( handler != null ) {
				handler( sender, args );
			}
		}
		private static readonly object RowClickedEventKey = new object();
		#endregion

		#region ColumnClicked
		public event EventHandler ColumnClicked {
			add { Events.AddHandler( ColumnClickedEventKey, value ); }
			remove { Events.RemoveHandler( ColumnClickedEventKey, value ); }
		}
		protected virtual void OnColumnClicked( Object sender, EventArgs args ) {
			EventHandler handler = (EventHandler) Events[ColumnClickedEventKey];
			if ( handler != null ) {
				handler( sender, args );
			}
		}
		private static readonly object ColumnClickedEventKey = new object();
		#endregion

		#endregion

		#region Setup methods

		protected override void OnInit( EventArgs e ) {
			this.RowDataBound += new GridViewRowEventHandler( this.GridItemBound );
			this.RowCreated += new GridViewRowEventHandler( this.GridItemCreated );
			base.OnInit( e );
		}

		protected void GridItemBound( Object sender, GridViewRowEventArgs args ) {
			GridViewRow row = args.Row;
			if ( this.UseClickableRows ) {
				if ( row.RowType == DataControlRowType.DataRow ) {
					// Set up the row click-back script
					int colIndex = 0;
					foreach ( System.Web.UI.WebControls.TableCell cell in row.Cells ) {
						string attribValue = this.Page.ClientScript.GetPostBackClientHyperlink( 
							this, string.Format( "{0};{1}", row.RowIndex, colIndex ) 
						);
						cell.Attributes["onclick"] = attribValue;
						colIndex++;
					}
				}
			}
		}

		protected void GridItemCreated( object sender, GridViewRowEventArgs args ) {
			GridViewRow row = args.Row;
			// Modify the column-headers...
			if ( row.RowType == DataControlRowType.Header ) {
				SetupGridHeaderRow( row );
			}
			if ( row.RowType == DataControlRowType.DataRow ) {
				SetupClickableDataRow( row );
			}
		}

		private void SetupGridHeaderRow( GridViewRow row ) {
			if ( this.AllowUserSorting ) {
				// Set up the column-headers for click-to-sort
				int c = 0;
				foreach ( System.Web.UI.WebControls.TableCell cell in row.Cells ) {
					if ( c >= this.Columns.Count )
						break;
					if ( !string.IsNullOrEmpty( this.Columns[c].SortExpression ) ) {
						cell.Attributes["onmouseover"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.ColHilightColor )
						);
						cell.Attributes["onmouseout"] = string.Format(
							"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.HeaderStyle.BackColor )
						);
						cell.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink( this, c.ToString() );
					}
					c++;
				}
			}
			if ( this.IncludeReorderControls ) {
				// Add extra columns for the re-order controls
				System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
				cell.ColumnSpan = 2;
				cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
				row.Cells.Add( cell );
			}
		}

		private void SetupClickableDataRow( GridViewRow row ) {
			//if ( row.RowIndex >= 0 ) {
				// Set the "mouse-over" color
				if ( row.RowState == DataControlRowState.Selected ) {
					row.Attributes["onmouseover"] = string.Format(
						"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';this.style.color='{1}';",
							ColorTranslator.ToHtml( this.SelectedRowStyle.BackColor ), ColorTranslator.ToHtml( this.SelectedRowStyle.ForeColor )
					);
				} else {
					row.Attributes["onmouseover"] = string.Format(
						"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
							ColorTranslator.ToHtml( this.RowHilightColor )
					);
				}
				// Set up the "mouse-out" color
				if ( row.RowState == DataControlRowState.Selected ) {
					row.Attributes["onmouseout"] = string.Format(
						"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
						ColorTranslator.ToHtml( this.SelectedRowStyle.BackColor )
					);
				} else if ( row.RowState == DataControlRowState.Normal ) {
					row.Attributes["onmouseout"] = string.Format(
						"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
						ColorTranslator.ToHtml( this.RowStyle.BackColor )
					);
				} else if ( row.RowState == DataControlRowState.Alternate ) {
					row.Attributes["onmouseout"] = string.Format(
						"javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';",
						ColorTranslator.ToHtml( this.AlternatingRowStyle.BackColor )
					);
				}
				// Add extra columns for the re-order controls
				if ( this.IncludeReorderControls ) {
					int rowCount = this.GetRecordCount();
					System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
					LiteralControl lit = new LiteralControl();
					lit.Text = row.DataItemIndex == 0 ? "&nbsp;" : 
						string.Format( "<img src='{0}' alt='Move this item up' />", this.MoveUpImageUrl );
					cell.Width = new Unit( "15" );
					cell.HorizontalAlign = HorizontalAlign.Center;
					cell.Controls.Add( lit );
					row.Cells.Add( cell );
					cell = new System.Web.UI.WebControls.TableCell();
					lit = new LiteralControl();
					lit.Text = row.DataItemIndex == ( rowCount - 1 ) ? "&nbsp;" : 
						string.Format( "<img src='{0}' alt='Move this item down' />", this.MoveDownImageUrl );
					cell.Width = new Unit( "15" );
					cell.HorizontalAlign = HorizontalAlign.Center;
					cell.Controls.Add( lit );
					row.Cells.Add( cell );
				}
			//}
		}

		private int GetRecordCount() {
			DataSourceView dsv = this.GetData();
			if ( dsv != null && dsv.CanRetrieveTotalRowCount ) {
				
			}
			return ( -1 );
		}

		private void SetupGridPagerRows() {
			if ( this.IncludeReorderControls ) {
				// Add extra column-span for the reorder-button columns
				if ( this.BottomPagerRow != null ) {
					this.BottomPagerRow.Cells[0].ColumnSpan += 2;
				}
				if ( this.TopPagerRow != null ) {
					this.TopPagerRow.Cells[0].ColumnSpan += 2;
				}
			}
		}

		protected override void RenderChildren( HtmlTextWriter writer ) {
			this.SetupGridPagerRows();
			base.RenderChildren( writer );
		}

		#endregion

		void IPostBackEventHandler.RaisePostBackEvent( string arg ) {
			string[] args = arg.Split( ';' );
			//int recIndex = Convert.ToInt32( args[0] );
			int recIndex = -1;
			if ( Int32.TryParse( args[0], out recIndex ) ) {
				if ( args.Length == 2 ) {
					// We clicked on an item-row ...
					int recID = Convert.ToInt32( this.DataKeys[recIndex].Value );
					if ( this.UseSelectableRows ) {
						this.SelectedIndex = recIndex;
						this.OnRowClicked( this, new DataGridClickEventArgs( recID, recIndex ) );
					} else {
						this.SelectedIndex = -1;
						if ( this.IncludeReorderControls ) {
							int recCount = this.DataKeys.Count;
							int reorderUpCol = this.Columns.Count;
							int reorderDownCol = reorderUpCol + 1;
							int colIndex = Convert.ToInt32( args[1] );
							if ( colIndex == reorderUpCol && recIndex > 0 ) {
								this.OnReorderUp( new DataGridClickEventArgs( -1, recIndex ) );
							} else if ( colIndex == reorderDownCol && recIndex < recCount - 1 ) {
								this.OnReorderDown( new DataGridClickEventArgs( -1, recIndex ) );
							}
						}
						this.OnRowClicked( this, new DataGridClickEventArgs( recID, recIndex ) );
					}
				} else {
					// Only 1 arg - we clicked on a column-header and "recIndex" is actually the column-index
					this.LastColumnClickedIndex = Convert.ToInt32( recIndex );
					if ( this.AllowUserSorting ) {
						this.UserSort( this.Columns[this.LastColumnClickedIndex].SortExpression );
					} else {
						this.OnColumnClicked( this, new EventArgs() );
					}
				}
			}
		}

		private void UserSort( string SortExpr ) {
			SortDirection sd = SortDirection.Ascending;
			if ( this.SortExpression == SortExpr ) {
				sd = this.SortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
			}
			this.Sort( SortExpr, sd );
		}

	}

}
