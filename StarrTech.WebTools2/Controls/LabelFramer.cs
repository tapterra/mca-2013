using System;
using System.Collections.Specialized;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools;
using StarrTech.WebTools.Panels;

namespace StarrTech.WebTools.Controls {
	
	// LabelFramer - helper class for framing/labeling form controls
	public class LabelFramer {

		#region Properties
		
		// FramerType - Orientation of the label
		public FramerType FramerType {
			get { return ( this.framerType ); }
			set { this.framerType = value; }
		}
		protected FramerType framerType = FramerType.TopTitleFrame;
		
		// Label - Text label for the frame
		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label = "";
		
		// LabelMark - Text added to the Label in red
		public string LabelMark {
			get { return ( this.labelMark ); }
			set { this.labelMark = value; }
		}
		private string labelMark = "";
		
		// FrameStyle - TableStyle for the frame
		public TableStyle FrameStyle {
			get { return ( this.frameStyle ); }
		}
		private TableStyle frameStyle = new TableStyle();
		
		// LabelStyle - TableItemStyle for the frame label
		public TableItemStyle LabelStyle {
			get { return ( this.labelStyle ); }
		}
		private TableItemStyle labelStyle = new TableItemStyle();
		
		// BodyStyle - TableItemStyle for the frame content
		public TableItemStyle BodyStyle {
			get { return ( this.bodyStyle ); }
		}
		private TableItemStyle bodyStyle = new TableItemStyle();
		
		// LabelMargin - Padding between the label and content
		public int LabelMargin {
			get { return ( this.labelMargin ); }
			set { this.labelMargin = value; }
		}
		private int labelMargin = 2;
		
		// FixedHeight - Fixed height to use for scrolling frames
		public Unit FixedHeight {
			get { return ( this.fixedHeight ); }
			set { this.fixedHeight = value; }
		}
		private Unit fixedHeight = Unit.Empty;
		
		// FixedWidth - Fixed width to use for scrolling frames
		public Unit FixedWidth {
			get { return ( this.fixedWidth ); }
			set { this.fixedWidth = value; }
		}
		private Unit fixedWidth = Unit.Empty;
		
		// ScrollingContent - if true, the content-cell will have a border & vertical-scrollbar
		public bool ScrollingContent {
			get { return ( this.scrollingContent ); }
			set { this.scrollingContent = value; }
		}
		private bool scrollingContent = false;
		
		public HybridDictionary Style {
			get { return ( this.style ); }
		}
		private HybridDictionary style = new HybridDictionary();
		
		public Color DisabledBackColor {
			get { return ( this.disabledBackColor ); }
			set { this.disabledBackColor = value; }
		}
		private Color disabledBackColor = SitePrefs.CheckColor( "disabledbackcolor", "#F6F6F6" );

		public Color DisabledForeColor {
			get { return ( this.disabledForeColor ); }
			set { this.disabledForeColor = value; }
		}
		private Color disabledForeColor = SitePrefs.CheckColor( "disabledforecolor", "#111111" );

		#endregion

		#region Methods

		// Constructor
		public LabelFramer() {
			FrameStyle.CellSpacing = 0;
			FrameStyle.CellPadding = 0;
		}
		
		private void RenderTableTag( HtmlTextWriter writer ) {
			FrameStyle.AddAttributesToRender( writer );
			foreach ( string key in Style.Keys ) {
				writer.AddStyleAttribute( key, Style[key].ToString() );
			}
			writer.RenderBeginTag( HtmlTextWriterTag.Table );
		}

		private void RenderLabelCell( HtmlTextWriter writer ) {
			writer.AddStyleAttribute( "padding", "0" );
			LabelStyle.AddAttributesToRender( writer );
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
					writer.AddStyleAttribute( "padding-top", LabelMargin.ToString() );
					writer.AddStyleAttribute( "padding-right", LabelMargin.ToString() );
					break;
				case FramerType.RightTitleFrame:
					writer.AddStyleAttribute( "padding-top", LabelMargin.ToString() );
					writer.AddStyleAttribute( "padding-left", LabelMargin.ToString() );
					break;
				case FramerType.TopTitleFrame:
					writer.AddStyleAttribute( "padding-bottom", LabelMargin.ToString() );
					break;
				case FramerType.BottomTitleFrame:
					writer.AddStyleAttribute( "padding-top", LabelMargin.ToString() );
					writer.AddStyleAttribute( "padding-right", LabelMargin.ToString() );
					break;
			}
			writer.RenderBeginTag( HtmlTextWriterTag.Td );
			writer.Write( this.Label );
			if ( this.LabelMark != "" ) {
				writer.Write( "<span style=\"color:red\">" );
				writer.Write( this.LabelMark );
				writer.Write( "</span>" );
			}
			writer.RenderEndTag(); // td
		}

		private void RenderBodyCellTag( HtmlTextWriter writer ) {
			BodyStyle.AddAttributesToRender( writer );
			writer.AddStyleAttribute( "padding", "0" );
			writer.AddStyleAttribute( "margin", "0" );
			writer.RenderBeginTag( HtmlTextWriterTag.Td );
			if ( this.ScrollingContent ) {
				if ( ! this.FixedHeight.IsEmpty ) {
					writer.AddStyleAttribute( "height", this.FixedHeight.ToString() );
				} else {
					writer.AddStyleAttribute( "height", "100%" );
				}
				if ( ! this.FixedWidth.IsEmpty ) {
					writer.AddStyleAttribute( "width", this.FixedWidth.ToString() );
				} else {
					writer.AddStyleAttribute( "width", "100%" );
				}
				writer.AddStyleAttribute( "border", "1px solid gray" );
                writer.AddStyleAttribute( "overflow", "auto" );
                writer.AddStyleAttribute( "background-color", "white" );
				writer.RenderBeginTag( HtmlTextWriterTag.Div );
			}
		}
		
		public virtual void SetCellWidths( Unit width ) {
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
				case FramerType.RightTitleFrame:
					BodyStyle.Width = width;
					break;
				case FramerType.TopTitleFrame:
				case FramerType.BottomTitleFrame:
					FrameStyle.Width = width;
					break;
			}
		}

		public virtual void RenderDisabled( HtmlTextWriter writer, FontInfo font, string text ) {
			Table table = new Table();
			table.CellPadding = 0;
			table.BorderStyle = BorderStyle.Solid;
			table.BorderWidth = 1;
			table.BorderColor = Color.LightGray;
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
				case FramerType.RightTitleFrame:
					table.Width = BodyStyle.Width;
					break;
				case FramerType.TopTitleFrame:
				case FramerType.BottomTitleFrame:
					table.Width = FrameStyle.Width;
					break;
			}
			string ht = "100%";
			if ( ! this.FixedHeight.IsEmpty ) ht = this.FixedHeight.ToString();
			cell.Height = new Unit( ht );
			cell.Style["height"] = ht;
			cell.ForeColor = this.DisabledForeColor;
			table.BackColor = this.DisabledBackColor;
			cell.Font.CopyFrom( font );
			cell.Text = text + "&nbsp;";
			row.Cells.Add( cell );
			table.Rows.Add( row );
			table.RenderControl( writer );
		}
		
		public virtual void PreRender( HtmlTextWriter writer ) {
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
					LabelStyle.HorizontalAlign = HorizontalAlign.Right;
					LabelStyle.VerticalAlign = VerticalAlign.Top;
					BodyStyle.HorizontalAlign = HorizontalAlign.Left;
					BodyStyle.VerticalAlign = VerticalAlign.Top;
					break;
				case FramerType.TopTitleFrame:
					LabelStyle.HorizontalAlign = HorizontalAlign.Left;
					LabelStyle.VerticalAlign = VerticalAlign.Bottom;
					BodyStyle.HorizontalAlign = HorizontalAlign.Left;
					BodyStyle.VerticalAlign = VerticalAlign.Top;
					break;
				case FramerType.RightTitleFrame:
					LabelStyle.HorizontalAlign = HorizontalAlign.Left;
					LabelStyle.VerticalAlign = VerticalAlign.Top;
					BodyStyle.HorizontalAlign = HorizontalAlign.Left;
					BodyStyle.VerticalAlign = VerticalAlign.Top;
					break;
				case FramerType.BottomTitleFrame:
					LabelStyle.HorizontalAlign = HorizontalAlign.Center;
					LabelStyle.VerticalAlign = VerticalAlign.Top;
					BodyStyle.HorizontalAlign = HorizontalAlign.Center;
					BodyStyle.VerticalAlign = VerticalAlign.Bottom;
					break;
			}
			RenderTableTag( writer );
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
					// single row, label then body
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderLabelCell( writer );
					RenderBodyCellTag( writer );
					break;
				case FramerType.TopTitleFrame:
					// label-row then body-row
					// Render the label-row
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderLabelCell( writer );
					writer.RenderEndTag(); // tr
					// Render the content-row opener
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderBodyCellTag( writer );
					break;
				case FramerType.RightTitleFrame:
					// single row, body then label
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderBodyCellTag( writer );
					break;
				case FramerType.BottomTitleFrame:
					// body-row then label-row
					// Render the content-row opener
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderBodyCellTag( writer );
					break;
				case FramerType.NoTitleFrame:
					// single row, bodycell only
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderBodyCellTag( writer );
					break;
			}
		}

		public virtual void PostRender( HtmlTextWriter writer ) {
			if ( this.ScrollingContent ) {
				writer.RenderEndTag(); // div
			}
			switch ( FramerType ) {
				case FramerType.LeftTitleFrame:
					// single row, label then body
					writer.RenderEndTag(); // td
					break;
				case FramerType.TopTitleFrame:
					// label-row then body-row
					writer.RenderEndTag(); // td
					break;
				case FramerType.RightTitleFrame:
					writer.RenderEndTag(); // td
					RenderLabelCell( writer );
					break;
				case FramerType.BottomTitleFrame:
					// body-row then label-row
					// Render the label-row
					writer.RenderEndTag(); // td
					writer.RenderEndTag(); // tr
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderLabelCell( writer );
					break;
				case FramerType.NoTitleFrame:
					// single row, bodycell only
					writer.RenderEndTag(); // td
					break;
			}
			writer.RenderEndTag(); // tr
			writer.RenderEndTag(); // table
		}
				
		#endregion

	}
		
}
