using System;
using System.Web;
using System.Web.UI;

namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// Standard ImageButton subclass that adds the RolloverImageUrl property
	/// </summary>
	public class ImageButton : System.Web.UI.WebControls.ImageButton {

		private const string RolloverScriptRegistrationKey = "StiImageButtonRollovers";

		/// <summary>
		/// Allows a second image URL to be used for roll-over effects
		/// </summary>
		public string RolloverImageUrl {
			get {
				object o = ViewState["RolloverImageUrl"];
				return ( o == null ? "" : (string) o );
			}
			set { ViewState["RolloverImageUrl"] = value; }
		}


		protected override void OnInit( EventArgs e ) {
			string imageUrl = this.ResolveClientUrl( this.RolloverImageUrl );
			string preloadScript = string.Format( "sti_preloadImages('{0}');", imageUrl );
			this.Page.ClientScript.RegisterStartupScript( this.GetType(), RolloverScriptRegistrationKey + this.ClientID, preloadScript, true );
			
			if ( !this.Page.ClientScript.IsClientScriptBlockRegistered( RolloverScriptRegistrationKey ) ) {
				// Script stolen brazenly from Dreamweaver, renamed globals ("MM_" to "sti_") so they don't conflict if used on the same page
				string scriptText = @"function sti_swapImgRestore() {
  var i,x,a=document.sti_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function sti_preloadImages() {
  var d=document; if(d.images){ if(!d.sti_p) d.sti_p=new Array();
    var i,j=d.sti_p.length,a=sti_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf('#')!=0){ d.sti_p[j]=new Image; d.sti_p[j++].src=a[i];}}
}

function sti_findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf('?'))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=sti_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function sti_swapImage() { 
  var i,j=0,x,a=sti_swapImage.arguments; document.sti_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=sti_findObj(a[i]))!=null){document.sti_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
";
				this.Page.ClientScript.RegisterClientScriptBlock( this.GetType(), RolloverScriptRegistrationKey, scriptText, true );
				this.Attributes.Add( "onMouseOut", "sti_swapImgRestore();" );
				this.Attributes.Add( "onMouseOver", String.Format( "sti_swapImage('{0}','','{1}',1);", this.ClientID, imageUrl ) );
			}
		}

	}

}
