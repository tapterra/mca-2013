using System;
using System.Web;
using System.Web.UI;

namespace StarrTech.WebTools.Controls {

	// Class: ImagerButton - aspx-tag for ButtonImager
	public class ImagerButton : System.Web.UI.WebControls.ImageButton {

		#region Properties
		// Text          t
		public new string Text {
			get {
				object obj = ViewState["Text"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set { ViewState["Text"] = value; }
		}


		// FontSize      s
		public int FontSize {
			get {
				object obj = ViewState["FontSize"];
				return ( obj == null ? 10 : (int) obj );
			}
			set { ViewState["FontSize"] = value; }
		}


		// FontName      f
		public string FontName {
			get {
				object obj = ViewState["FontName"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set { ViewState["FontName"] = value; }
		}


		// FontStyle     z=biu
		public string FontStyle {
			get {
				object obj = ViewState["FontStyle"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set { ViewState["FontStyle"] = value; }
		}


		// TextColor     c
		public string TextColor {
			get {
				object obj = ViewState["TextColor"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set { ViewState["TextColor"] = value; }
		}


		// BGColor       b
		public string BGColor {
			get {
				object obj = ViewState["BGColor"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set {
				ViewState["BGColor"] = value;
			}
		}


		// FrameColor    g
		public string FrameColor {
			get {
				object obj = ViewState["FrameColor"];
				return ( obj == null ? String.Empty : (string) obj );
			}
			set {
				ViewState["FrameColor"] = value;
			}
		}


		// BaseUrl = "buttonimager.ashx"
		private string _baseurl = "buttonimager.ashx";
		public string baseurl {
			get { return ( _baseurl ); }
			set { _baseurl = value; }
		}

		#endregion

		#region Methods

		protected override void CreateChildControls() {
			string url = baseurl + "?";
			url += "f=" + HttpUtility.UrlEncode( FontName );
			url += "&s=" + FontSize.ToString();
			url += "&w=" + Width.ToString().Replace( "px", "" );
			url += "&h=" + Height.ToString().Replace( "px", "" );
			if ( FontStyle != "" )
				url += "&z=" + FontStyle;
			if ( TextColor != "" )
				url += "&c=" + TextColor.Replace( '#', '_' );
			if ( FrameColor != "" )
				url += "&g=" + FrameColor.Replace( '#', '_' );
			if ( BGColor != "" )
				url += "&b=" + BGColor.Replace( '#', '_' );
			url += "&t=" + HttpUtility.UrlEncode( Text );
			ImageUrl = url;
			if ( string.IsNullOrEmpty( AlternateText ) )
				AlternateText = Text;
			base.CreateChildControls();
		}

		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			base.Render( writer );
		}

		public virtual void render( HtmlTextWriter writer ) {
			Render( writer );
		}

		#endregion

	}

}
