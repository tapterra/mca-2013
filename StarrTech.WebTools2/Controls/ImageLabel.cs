using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Web.UI.WebControls;
using StarrTech.WebTools.Imaging;

namespace StarrTech.WebTools.Controls {

	// ImageLabel - aspx-tag for TextImager HttpHandler 
	public class ImageLabel : System.Web.UI.WebControls.Image {
		
		public string HandlerUrl {
			get { 
				object obj = ViewState["HandlerUrl"];
				return ( obj == null ? "~/textimager.ashx" : (string) obj ); 
			}
			set { ViewState["HandlerUrl"] = value; }
		}
		
		public override string ImageUrl {
			get { return ( CreateTextImagerUrl() ); }
		}
		
		public string Text {
			get { 
				object obj = ViewState["Text"];
				return ( obj == null ? string.Empty : (string) obj ); 
			}
			set { ViewState["Text"] = value; }
		}
		
		public TextImagerBackgroundMode BackgroundMode {
			get { 
				object obj = ViewState["BackgroundMode"];
				return ( obj == null ? TextImagerBackgroundMode.NotSet : (TextImagerBackgroundMode) obj ); 
			}
			set { ViewState["BackgroundMode"] = value; }
		}
		
		public HorizontalAlign HorizontalAlign {
			get { 
				object obj = ViewState["HorizontalAlign"];
				return ( obj == null ? HorizontalAlign.NotSet : (HorizontalAlign) obj ); 
			}
			set { ViewState["HorizontalAlign"] = value; }
		}
		
		protected string CreateTextImagerUrl() {
			StringBuilder url = new StringBuilder( 1024 );
			bool firstArg = true;
			string str = "";
			url.Append( HandlerUrl );
			url.Append( "?" );
			// Text
			str = Text;
			if ( str.Length != 0 ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "t=" );
				url.Append( str );
				firstArg = false;
			}
			
			// Font name
			str = Font.Name;
			if ( str.Length != 0 ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "f=" );
				url.Append( str );
				firstArg = false;
			}
			
			// Font size
			FontUnit fsize = Font.Size;
			if ( (!fsize.IsEmpty) && (fsize.Type == FontSize.AsUnit) && (fsize.Unit.Type == UnitType.Point) ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "s=" );
				url.Append( ((int)fsize.Unit.Value).ToString() );
				firstArg = false;
			}
			
			// Font styles
			if ( Font.Bold || Font.Italic || Font.Underline ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "z=" );
				if ( Font.Bold ) url.Append( "b" );
				if ( Font.Italic ) url.Append( "i" );
				if ( Font.Underline ) url.Append( "u" );
				firstArg = false;
			}
			
			// ForeColor
			if ( ! ForeColor.IsEmpty ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "c=" );
				url.Append( TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(ForeColor) );
				firstArg = false;
			}
						
			// BackColor
			if ( ! BackColor.IsEmpty ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "g=" );
				url.Append( TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(BackColor) );
				firstArg = false;
			}
						
			// Width
			if ( ! Width.IsEmpty ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "w=" );
				url.Append( ((int) Width.Value).ToString() );
				firstArg = false;
			}
						
			// HorizontalAlign
			if ( HorizontalAlign != HorizontalAlign.NotSet ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "a=" );
				switch ( HorizontalAlign ) {
					case HorizontalAlign.Right:  url.Append( "r" ); break;
					case HorizontalAlign.Center:  url.Append( "c" ); break;
					default: url.Append( "l" ); break;
				}
				firstArg = false;
			}
						
			// BackgroundMode
			if ( BackgroundMode != TextImagerBackgroundMode.NotSet ) {
				if ( ! firstArg ) url.Append( "&" );
				url.Append( "m=" );
				switch ( BackgroundMode ) {
					case TextImagerBackgroundMode.Solid:       url.Append( "s" ); break;
					case TextImagerBackgroundMode.Gradient:    url.Append( "g" ); break;
					case TextImagerBackgroundMode.Shadow:      url.Append( "h" ); break;
					case TextImagerBackgroundMode.Box:         url.Append( "x" ); break;
					case TextImagerBackgroundMode.TopRule:     url.Append( "t" ); break;
					case TextImagerBackgroundMode.BottomRule:  url.Append( "b" ); break;
					case TextImagerBackgroundMode.DoubleRule:  url.Append( "d" ); break;
					default: url.Append( "s" ); break;
				}
				firstArg = false;
			}
			
			return ( url.ToString() );
		}
				
	}
		
}
