using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.Controls {

	[Obsolete( "Use Ajax calendar-picker instead")]
	public class DatePicker : WebControl, INamingContainer {

		#region Private members

		protected TextBox dateTextBox = new TextBox();
		protected System.Web.UI.WebControls.ImageButton dateButton = new System.Web.UI.WebControls.ImageButton();
		protected System.Web.UI.WebControls.ImageButton clearButton = new System.Web.UI.WebControls.ImageButton();
		protected Calendar dateCalendar = new Calendar();
		protected Panel calendarDiv = new Panel();

		#endregion

		#region Properties

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "CssClass name applied to the dateTextBox" ), Localizable( true )]
		public string TextBoxCssClass {
			get {
				this.EnsureChildControls();
				return ( this.dateTextBox.CssClass );
			}
			set {
				this.EnsureChildControls();
				this.dateTextBox.CssClass = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "CssClass name applied to the Calendar" ), Localizable( true )]
		public string CalendarCssClass {
			get {
				this.EnsureChildControls();
				return ( this.dateCalendar.CssClass );
			}
			set {
				this.EnsureChildControls();
				this.dateCalendar.CssClass = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Localizable( true )]
		public string Text {
			get {
				this.EnsureChildControls();
				return ( this.dateTextBox.Text );
			}
			set {
				this.EnsureChildControls();
				DateTime dateValue = DateTime.MinValue;
				try { dateValue = DateTime.Parse( value ); } catch { }
				if ( dateValue <= DateTime.MinValue || dateValue >= DateTime.MaxValue ) {
					this.dateTextBox.Text = String.Empty;
				} else {
					this.dateTextBox.Text = dateValue.Date.ToString( this.dateFormatString );
				}
			}
		}

		[Bindable( true ), Category( "Layout" ), DefaultValue( "" ), Localizable( true )]
		public override Unit Width {
			get {
				this.EnsureChildControls();
				return ( this.dateTextBox.Width );
			}
			set {
				this.EnsureChildControls();
				this.dateTextBox.Width = value;
			}
		}

		[Bindable( true ), Category( "Layout" ), DefaultValue( "" ), Localizable( true )]
		public override Unit Height {
			get {
				this.EnsureChildControls();
				return ( this.dateTextBox.Height );
			}
			set {
				this.EnsureChildControls();
				this.dateTextBox.Height = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "URL of image to be shown" ), Localizable( true )]
		public string ButtonImageURL {
			get {
				this.EnsureChildControls();
				return ( this.dateButton.ImageUrl );
			}
			set {
				this.EnsureChildControls();
				this.dateButton.ImageUrl = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "Alignment of image" ), Localizable( true )]
		public ImageAlign ButtonImageAlign {
			get {
				this.EnsureChildControls();
				return ( this.dateButton.ImageAlign );
			}
			set {
				this.EnsureChildControls();
				this.dateButton.ImageAlign = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "URL of image to be shown" ), Localizable( true )]
		public string ClearButtonImageURL {
			get {
				this.EnsureChildControls();
				return ( this.clearButton.ImageUrl );
			}
			set {
				this.EnsureChildControls();
				this.clearButton.ImageUrl = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "Alignment of image" ), Localizable( true )]
		public ImageAlign ClearButtonImageAlign {
			get {
				this.EnsureChildControls();
				return ( this.clearButton.ImageAlign );
			}
			set {
				this.EnsureChildControls();
				this.clearButton.ImageAlign = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( false ), Description( "" ), Localizable( true )]
		public bool CalendarVisible {
			get {
				this.EnsureChildControls();
				return ( this.calendarDiv.Visible );
			}
			set {
				this.EnsureChildControls();
				this.calendarDiv.Visible = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( false ), Description( "" ), Localizable( true )]
		public bool ClearButtonVisible {
			get {
				this.EnsureChildControls();
				return ( this.clearButton.Visible );
			}
			set {
				this.EnsureChildControls();
				this.clearButton.Visible = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( false ), Description( "" ), Localizable( true )]
		public bool TextBoxReadOnly {
			get {
				this.EnsureChildControls();
				return ( this.dateTextBox.ReadOnly );
			}
			set {
				this.EnsureChildControls();
				this.dateTextBox.ReadOnly = value;
			}
		}

		[Bindable( true ), Category( "Appearance" ), DefaultValue( "" ), Description( "" ), Localizable( true )]
		public string DateDisplayFormat {
			get { return ( this.dateFormatString ); }
			set { this.dateFormatString = value; }
		}
		private string dateFormatString = "d";

		public DateTime SelectedDate {
			get {
				this.EnsureChildControls();
				return ( this.dateCalendar.SelectedDate.Date );
			}
			set {
				this.EnsureChildControls();
				this.dateCalendar.SelectedDate = value.Date;
				this.dateCalendar.VisibleDate = value.Date;
				if ( value <= DateTime.MinValue || value >= DateTime.MaxValue ) {
					this.dateTextBox.Text = String.Empty;
				} else {
					this.dateTextBox.Text = value.Date.ToString( dateFormatString );
				}
				this.OnCalendarSelectionChanged( this, new DateEventArgs( value ) );
			}
		}

		#endregion

		#region CalendarSelectionChanged Event
		public event DateEventHandler CalendarSelectionChanged {
			add { Events.AddHandler( CalendarSelectionChangedEventKey, value ); }
			remove { Events.RemoveHandler( CalendarSelectionChangedEventKey, value ); }
		}
		public virtual void OnCalendarSelectionChanged( Object sender, DateEventArgs args ) {
			DateEventHandler handler = (DateEventHandler) Events[CalendarSelectionChangedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
		private static readonly object CalendarSelectionChangedEventKey = new object();
		#endregion

		#region Methods

		protected override void CreateChildControls() {
			this.dateTextBox.AutoPostBack = true;
			this.dateTextBox.CausesValidation = false;
			this.dateTextBox.TextChanged += new EventHandler( this.dateTextBox_TextChanged );
			this.Controls.Add( this.dateTextBox );

			this.dateButton.CausesValidation = false;
			this.dateButton.Click += new ImageClickEventHandler( this.dateButton_Click );
			this.dateButton.ImageUrl = this.Page.ClientScript.GetWebResourceUrl(
					typeof( StarrTech.WebTools.Controls.DatePicker ), "StarrTech.WebTools.Resources.controls.calendar-button.gif"
			);
			this.dateButton.ImageAlign = ImageAlign.AbsMiddle;
			this.Controls.Add( this.dateButton );

			this.clearButton.CausesValidation = false;
			this.clearButton.Visible = false;
			this.clearButton.Click += new ImageClickEventHandler( this.clearButton_Click );
			this.clearButton.ImageUrl = this.Page.ClientScript.GetWebResourceUrl(
					typeof( StarrTech.WebTools.Controls.DatePicker ), "StarrTech.WebTools.Resources.controls.clear-calendar-button.gif"
			);
			this.clearButton.ImageAlign = ImageAlign.AbsMiddle;
			this.Controls.Add( this.clearButton );

			this.dateCalendar.Height = 1;
			this.dateCalendar.Width = 1;
			this.dateCalendar.SelectionChanged += new EventHandler( this.dateCalendar_SelectionChanged );
			this.calendarDiv.Controls.Add( this.dateCalendar );

			this.calendarDiv.Visible = false;
			//this.calendarDiv.Attributes.CssStyle.Add(HtmlTextWriterStyle.Position, "absolute");
			//this.calendarDiv.Attributes.CssStyle.Add(HtmlTextWriterStyle.ZIndex, "0");
			this.Controls.Add( this.calendarDiv );
		}

		protected override void RenderContents( HtmlTextWriter output ) {
			dateTextBox.RenderControl( output );
			dateButton.RenderControl( output );
			clearButton.RenderControl( output );
			calendarDiv.RenderControl( output );
		}

		protected void dateButton_Click( object sender, System.Web.UI.ImageClickEventArgs e ) {
			//dateCalendar.SelectedDate = dateTextBox.Text;
			//dateCalendar.VisibleDate = dateTextBox.Text;
			calendarDiv.Visible = !calendarDiv.Visible;
		}

		protected void clearButton_Click( object sender, System.Web.UI.ImageClickEventArgs e ) {
			ClearSelectedDate();
		}

		protected void dateTextBox_TextChanged( object sender, System.EventArgs e ) {
			if ( !calendarDiv.Visible == false ) {
				calendarDiv.Visible = false;
			}

			try {
				DateTime dt = DateTime.Parse( dateTextBox.Text );
				dateCalendar.SelectedDate = dt;
				dateCalendar.VisibleDate = dt;
			} catch {
				dateTextBox.Text = DateTime.Now.ToString( this.dateFormatString );
				dateCalendar.SelectedDate = DateTime.Now;
				dateCalendar.VisibleDate = DateTime.Now;
			}

		}

		protected void dateCalendar_SelectionChanged( object sender, System.EventArgs e ) {
			calendarDiv.Visible = false;
			dateTextBox.Text = dateCalendar.SelectedDate.ToString( this.dateFormatString );
			this.OnCalendarSelectionChanged( this, new DateEventArgs( dateCalendar.SelectedDate ) );
		}

		public void ClearSelectedDate() {
			dateCalendar.SelectedDate = DateTime.MinValue;
			dateTextBox.Text = String.Empty;
		}

		#endregion

	}

}

public delegate void DateEventHandler( object sender, DateEventArgs e );

public class DateEventArgs : System.EventArgs {

	public DateTime Date {
		get { return ( this.date ); }
		set { this.date = value; }
	}
	private DateTime date;

	public DateEventArgs( DateTime Date ) {
		this.date = Date;
	}

}
