using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
	

namespace StarrTech.WebTools.Controls {
	
	public class Xml : Control {

		#region Properties

		public DateTime RefreshDateTime {
			get { return ( refreshDateTime ); }
			set { refreshDateTime = value; }
		}
		private DateTime refreshDateTime = Cache.NoAbsoluteExpiration;
		
		public TimeSpan RefreshInterval {
			get { return ( refreshInterval ); }
			set { refreshInterval = value; }
		}
		private TimeSpan refreshInterval = Cache.NoSlidingExpiration; 
		
		public XPathDocument Document {
			get { return ( document ); }
			set { document = value; }
		}
		protected XPathDocument document = null;
		
		public string DocumentSource {
			set { 
				try {
					string key = HttpContext.Current.Server.MapPath( value );
					if ( HttpContext.Current.Cache[key] == null ) {
						Document = new XPathDocument( key );
						HttpContext.Current.Cache.Insert( key, Document, new CacheDependency( key ), RefreshDateTime, RefreshInterval );
					} else {
						Document = (XPathDocument) HttpContext.Current.Cache[key];
					}
				} catch { 
					Document = null; 
				}
			}
		}
		
		public string DocumentUrl {
			set { 
				try {
					string key = value;
					if ( HttpContext.Current.Cache[key] == null ) {
						Document = new XPathDocument( key );
						HttpContext.Current.Cache.Insert( key, Document, null, RefreshDateTime, RefreshInterval );
					}
					else Document = (XPathDocument) HttpContext.Current.Cache[key];
				} catch { 
					Document = null; 
				}
			}
		}
		
		public string DocumentContent {
			set { 
				try {
					UTF8Encoding enc = new UTF8Encoding();
					byte[] buff = enc.GetBytes( value ); 
					MemoryStream strm = new MemoryStream( buff );
					Document = new XPathDocument( strm );
				} catch { 
					Document = null; 
				}
			}
		}
				
		public XslCompiledTransform Transform {
			get { return ( transform ); }
			set { transform = value; }
		}
		protected XslCompiledTransform transform = null;

		private bool xslLoaded = false;

		public string TransformSource {
			set {
				if ( string.IsNullOrEmpty( value ) ) {
					xslLoaded = false;
					return;
				}
				xslLoaded = true;
				string key = HttpContext.Current.Server.MapPath( value );
				if ( HttpContext.Current.Cache[key] == null ) {
					Transform = new XslCompiledTransform();
					Transform.Load( key ); 
					HttpContext.Current.Cache.Insert( key, Transform, new CacheDependency( key ), RefreshDateTime, RefreshInterval );
				} else
					Transform = (XslCompiledTransform) HttpContext.Current.Cache[key];
			}
		}
		
		public string TransformUrl {
			set {
				if ( string.IsNullOrEmpty( value ) ) {
					xslLoaded = false;
					return;
				}
				xslLoaded = true;
				string key = value;
				try {
					if ( HttpContext.Current.Cache[key] == null ) {
						Transform = new XslCompiledTransform();
						Transform.Load( key ); 
						HttpContext.Current.Cache.Insert( key, Transform, null, RefreshDateTime, RefreshInterval );
					} else {
						Transform = (XslCompiledTransform) HttpContext.Current.Cache[key];
					}
				} catch { 
					Transform = null;
					xslLoaded = false;
				} 
			}
		}
		
		public string TransformContent {
			set { 
				try {
					UTF8Encoding enc = new UTF8Encoding();
					byte[] buff = enc.GetBytes( value ); 
					MemoryStream strm = new MemoryStream( buff );
					Transform = new XslCompiledTransform();
					Transform.Load( new XPathDocument( strm ) );
				} catch { 
					Document = null; 
				}
			}
		}

		public XsltArgumentList TransformArgumentList {
			get { return ( this.transformArgumentList ); }
			set { this.transformArgumentList = value; }
		}
		private XsltArgumentList transformArgumentList = null;

		#endregion
		
		#region Methods 

		private void LoadContentXml() {
			if ( Controls.Count <= 0 ) return;
			StringBuilder xml = new StringBuilder("");
			foreach ( Control cntl in Controls ) {
				xml.Append( ((LiteralControl) cntl).Text );
			}
			DocumentContent = xml.ToString();
			Controls.Clear();
		}

		protected override void CreateChildControls() {
			LoadContentXml();
			Controls.Add( Page.ParseControl( RenderAsText() ) );
			base.CreateChildControls();
		}
				
		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			base.Render( writer );
		}
				
		public string RenderAsText() {
			if ( Document == null ) return ( "" );
		if ( !xslLoaded ) {
				StringBuilder result = new StringBuilder("");
				HttpContext.Current.Trace.Write( "*** 1 " );
				XPathNavigator nav = Document.CreateNavigator();
				HttpContext.Current.Trace.Write( "*** 2 " );
				XPathNodeIterator iter = nav.Select( "//node()" );
				HttpContext.Current.Trace.Write( "*** 3 " );
				if ( iter.MoveNext() )
					result.Append( iter.Current.Value );
				HttpContext.Current.Trace.Write( "*** 4 " );
				return ( result.ToString() );
			} else {
				MemoryStream outstr = new MemoryStream();
				Transform.Transform( Document, TransformArgumentList, outstr );
				outstr.Position = 0;
				StreamReader rdr = new StreamReader( outstr );
				return ( rdr.ReadToEnd() );
			}
		}
		
		#endregion

	}
		

}


