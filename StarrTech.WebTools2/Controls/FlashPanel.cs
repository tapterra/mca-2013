using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Resources;

namespace StarrTech.WebTools.Controls {

	public class FlashPanel : Control {

		#region Public Properties

		public string FlashFile {
			get {
				string s = ViewState["FlashFile"].ToString();
				return ( string.IsNullOrEmpty( s ) ? string.Empty : s );
			}
			set { ViewState["FlashFile"] = value; }
		}

		public int Width {
			get {
				if ( ViewState["Width"] == null ) {
					return 100;
				} else {
					return Convert.ToInt32( ViewState["Width"] );
				}
			}
			set {
				if ( value > 0 ) {
					ViewState["Width"] = value;
				}
			}
		}

		public int Height {
			get {
				if ( ViewState["Height"] == null ) {
					return 100;
				} else {
					return Convert.ToInt32( ViewState["Height"] );
				}
			}
			set {
				if ( value > 0 ) {
					ViewState["Height"] = value;
				}
			}
		}

		public bool PlayImmediately {
			get {
				if ( ViewState["Play"] != null && Convert.ToBoolean( ViewState["Play"] ) == false ) {
					return false;
				} else {
					return true;
				}
			}
			set { ViewState["Play"] = value; }
		}

		public Color BackColor {
			get {
				if ( ViewState["BackColor"] == null ) {
					return Color.Empty;
				} else {
					return ( (Color) ( ViewState["BackColor"] ) );
				}
			}
			set { ViewState["BackColor"] = value; }
		}

		public bool Menu {
			get {
				if ( ViewState["Menu"] != null && Convert.ToBoolean( ViewState["Menu"] ) == false ) {
					return false;
				} else {
					return true;
				}
			}
			set { ViewState["Menu"] = value; }
		}

		public bool Loop {
			get {
				if ( ViewState["Loop"] != null && Convert.ToBoolean( ViewState["Loop"] ) == false ) {
					return false;
				} else {
					return true;
				}
			}
			set { ViewState["Loop"] = value; }
		}

		public FlashQualities Quality {
			get { return ( ViewState["Quality"] == null ? FlashQualities.AutoHigh : (FlashQualities) ( ViewState["Quality"] ) ); }
			set { ViewState["Quality"] = value; }
		}

		public FlashWModes WMode {
			get { return ( ViewState["WMode"] == null ? FlashWModes.Window : (FlashWModes) ( ViewState["WMode"] ) ); }
			set { ViewState["WMode"] = value; }
		}

		public FlashScales Scale {
			get { return ( ViewState["Scale"] == null ? FlashScales.NoScale : (FlashScales) ( ViewState["Scale"] ) ); }
			set { ViewState["Scale"] = value; }
		}

		public string FlashVars {
			get {
				object s = ViewState["FlashVars"];
				return ( s == null ? string.Empty : s.ToString() );
			}
			set { ViewState["FlashVars"] = value; }
		}

		public enum FlashQualities : byte {
			AutoHigh,
			Best,
			High,
			Medium,
			AutoLow,
			Low
		}

		// Window does not embed any window-related attributes in the object and embed tags. The background of the Flash content is opaque and uses the HTML background color. The HTML cannot render above or below the Flash content. This is the default setting.
		// Opaque Windowless sets the background of the Flash content to opaque, obscuring anything underneath the Flash content. Opaque Windowless lets HTML content appear above or on top of Flash content.
		// Transparent Windowless sets the background of the Flash content to transparent. This allows the HTML content to appear above and below the Flash content.

		public enum FlashWModes : byte {
			Window,
			Opaque,
			Transparent
		}

		// Default (Show All) shows the entire document in the specified area without distortion while maintaining the original aspect ratio of the SWF files. Borders can appear on two sides of the application.
		// No Border scales the document to fill the specified area and keeps the SWF file's original aspect ratio without distortion, cropping the SWF file if needed. 
		// Exact Fit displays the entire document in the specified area without preserving the original aspect ratio, which can cause distortion.
		// No Scale prevents the document from scaling when the Flash Player window is resized. 

		public enum FlashScales : byte {
			ShowAll,
			NoBorder,
			ExactFit,
			NoScale
		}

		protected bool IsIE {
			get {
				//return ( Context.Request.Browser.Browser.ToString().ToUpper() == "IE" );
				return ( Context.Request.Browser.Browser.ToUpper().IndexOf( "IE" ) >= 0 ); 
			}
		}

		#endregion

		protected override void OnPreRender( System.EventArgs e ) {
			base.OnPreRender( e );
			if ( this.IsIE ) {
				// Define the resource name and type.
				Type rstype = this.GetType();
				string rsname = "StarrTech.WebTools.Resources.script.flashpanel.js";
				// Register the client resource with the page.
				ClientScriptManager cs = this.Page.ClientScript;
				cs.RegisterClientScriptResource( rstype, rsname );
			}
		}

		protected override void Render( HtmlTextWriter output ) {
			if ( this.FlashFile.Trim() == "" || this.Width == 0 || this.Height == 0 ) {
				throw new ArgumentNullException( "FlashFile/Width/Height property is missing" );
			}
			if ( this.IsIE ) {
				RenderForIE( output );
			} else {
				RenderForAlt( output );
			}
		}

		/// <summary>
		/// Renders flash object tag for browsers besides IE
		/// </summary>
		/// <param name="output"></param>
		/// <remarks></remarks>
		private void RenderForAlt( HtmlTextWriter output ) {
			StringBuilder sb = new StringBuilder();
			sb.Append( "<object type='application/x-shockwave-flash'" );
			sb.AppendFormat( " id='{0}'", this.ClientID );
			sb.AppendFormat( " data='{0}'", Page.ResolveClientUrl( this.FlashFile ) );
			sb.AppendFormat( " width='{0}'", this.Width );
			sb.AppendFormat( " height='{0}'", this.Height.ToString() );
			if ( !this.BackColor.IsEmpty ) {
				sb.AppendFormat( " bgcolor='{0}'", Color2Hex( this.BackColor ) );
			}
			sb.AppendFormat( " play='{0}'", this.PlayImmediately.ToString().ToLower() );
			sb.AppendFormat( " loop='{0}'", this.Loop.ToString().ToLower() );
			sb.AppendFormat( " quality='{0}'", this.Quality.ToString() );
			sb.Append( " ></object>" ); // Safari chokes if you use a single tag: "<object ... />"
			output.Write( sb.ToString() );
		}

		/// <summary>
		/// Renders flash instantiation script for IE browsers
		/// </summary>
		private void RenderForIE( HtmlTextWriter output ) {
			//function CreateIEFlash( ID, Movie, Width, Height, BGC, Play, Loop, Qual, FlashVars, WMode, Scale ) {
			string script = string.Format(
				"CreateIEFlash( '{0}', '{1}', {2}, {3}, '{4}', {5}, '{6}', '{7}', '{8}', '{9}', '{10}' );",
				this.ClientID,
				this.Page.ResolveClientUrl( this.FlashFile ),
				this.Width,
				this.Height,
				this.BackColor.IsEmpty ? "" : Color2Hex( this.BackColor ),
				this.PlayImmediately.ToString().ToLower(),
				this.Loop.ToString().ToLower(),
				this.Quality.ToString().ToLower(), 
				this.FlashVars,
				this.WMode.ToString().ToLower(),
				this.Scale.ToString().ToLower()
			);
			output.Write( "<script language='JavaScript'>" );
			output.Write( script );
			output.Write( "</script>" );
		}

		/// <summary>
		/// converts a color object into a hex color value
		/// that can be used as an HTML color
		/// </summary>
		/// <param name="Clr">System.Drawing.Color</param>
		/// <returns>a hex value</returns>
		/// <remarks></remarks>
		public string Color2Hex( Color clr ) {
			string HexR = null;
			string HexB = null;
			string HexG = null;

			try {
				//Get Red Hex
				HexR = System.Convert.ToString( clr.R, 16 ).ToUpper();
				if ( HexR.Length < 2 ) {
					HexR = "0" + HexR;
				}

				//Get Green Hex
				HexG = System.Convert.ToString( clr.G, 16 ).ToUpper();
				if ( HexG.Length < 2 ) {
					HexG = "0" + HexG;
				}

				//Get Blue Hex
				HexB = System.Convert.ToString( clr.B, 16 ).ToUpper();
				if ( HexB.Length < 2 ) {
					HexB = "0" + HexB;
				}

			} catch {
				return "";
			}

			return ( "#" + HexR + HexG + HexB );
		}

	}

}
