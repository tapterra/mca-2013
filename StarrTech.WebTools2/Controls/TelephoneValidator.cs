using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.Controls {
	/// <summary>
	/// A RegularExpressionValidator subclass pre-configured to validate email-address fields.
	/// </summary>
	public class TelephoneValidator : RegularExpressionValidator {
		public TelephoneValidator() : base() {
			//this.ValidationExpression = @"((\(\d{3}\) )|(\d{3}[- \.]))\d{3}[- \.]\d{4}(\s(x\d+)?){0,1}$";
			//this.ValidationExpression = @"\d{3}-\d{3}-\d{4}$";
			this.ValidationExpression = @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}( x\d{0,})?";
		}
	}
}
