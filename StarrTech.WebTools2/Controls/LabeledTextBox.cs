using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.Controls {
	/// <summary>
	/// A structured TextBox control that uses a LabelFramer to display its label
	/// </summary>
	public class LabeledTextBox : TextBox {
		
		#region Properties
		/// <summary>
		/// The LabelFramer used to position and display the label
		/// </summary>
		public  LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();
		
		/// <summary>
		/// Color used to fill in the content area when the control is disabled
		/// </summary>
		public Color DisabledBackColor {
			get { return ( this.Frame.DisabledBackColor ); }
			set { this.Frame.DisabledBackColor = value; }
		}

		/// <summary>
		/// Color used for text, etc. in the content area when the control is disabled
		/// </summary>
		public Color DisabledForeColor {
			get { return ( this.Frame.DisabledForeColor ); }
			set { this.Frame.DisabledForeColor = value; }
		}

		/// <summary>
		/// The text displayed as the control's label
		/// </summary>
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}
		
		/// <summary>
		/// A TableStyle for the frame - exposes the backcolor, border, etc. styles
		/// </summary>
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}
		
		/// <summary>
		/// A TableItemStyle for the frame label
		/// </summary>
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}
		
		/// <summary>
		/// The TableItemStyle for the frame content (where the control is positioned)
		/// </summary>
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}
		
		/// <summary>
		/// Overridden to set up the frame
		/// </summary>
		public override Unit Width {
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}
		
		/// <summary>
		/// Client-side name of a button to "click" when Enter-key is pressed in this textbox
		/// </summary>
		public string DefaultButtonName {
			get { 
				object obj = ViewState["DefaultButtonName"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["DefaultButtonName"] = value; }
		}
		
		private const string kDefaultButtonScript = 
			"if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {{ document.all['{0}'].click();return false; }} else return true;";
		
		protected string DefaultButtonScript {
			get { return ( string.Format( kDefaultButtonScript, this.DefaultButtonName ) ); }
		}
		
		#endregion
		
		#region Constructor
		
		public LabeledTextBox() : base() {
		}	
		
		#endregion
		
		#region Methods

		protected override void OnPreRender( EventArgs e ) {
			base.OnPreRender( e );
			if ( Frame.FramerType == StarrTech.WebTools.Panels.FramerType.TopTitleFrame ) {
				Frame.Style["margin-bottom"] = "4px";
			}
			if ( this.DefaultButtonName.Length > 0 ) {
				this.Attributes["onkeydown"] = this.DefaultButtonScript;
			}
		}

		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			this.Frame.PreRender( writer );
			if ( this.Enabled ) {
				base.Render( writer );
			} else {
				this.Frame.RenderDisabled( writer, this.Font, this.Text );
			}
			this.Frame.PostRender( writer );
		}
		
		#endregion
		
	}
	
}
