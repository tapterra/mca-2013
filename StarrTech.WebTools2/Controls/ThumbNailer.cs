using System;
using System.Text;


namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// aspx-tag for ThumbNailer HttpHandler 
	/// </summary>
	public class ThumbNailer : System.Web.UI.WebControls.Image {

		#region Properties
		
		public string HandlerUrl {
			get {
				object obj = ViewState["HandlerUrl"];
				return ( obj == null ? "~/thumbnailer.ashx" : (string) obj );
			}
			set { ViewState["HandlerUrl"] = value; }
		}

		public override string ImageUrl {
			get { return ( CreateImagerUrl() ); }
		}

		public string SourceImageUrl {
			get {
				object obj = ViewState["SourceImageUrl"];
				return ( obj == null ? "" : (string) obj );
			}
			set { ViewState["SourceImageUrl"] = value; }
		}

		public double Scale {
			get {
				object obj = ViewState["Scale"];
				return ( obj == null ? (double) 0 : (double) obj );
			}
			set { ViewState["Scale"] = value; }
		}

		public int MaxWidth {
			get {
				object obj = ViewState["MaxWidth"];
				return ( obj == null ? (int) 0 : (int) obj );
			}
			set { ViewState["MaxWidth"] = value; }
		}

		public int MaxHeight {
			get {
				object obj = ViewState["MaxHeight"];
				return ( obj == null ? (int) 0 : (int) obj );
			}
			set { ViewState["MaxHeight"] = value; }
		}

		#endregion

		protected string CreateImagerUrl() {
			if ( string.IsNullOrEmpty( SourceImageUrl ) )
				throw new Exception( "SourceImageUrl must be specified." );
			StringBuilder url = new StringBuilder( 1024 );
			url.Append( HandlerUrl );
			url.AppendFormat( "?src={0}", SourceImageUrl );
			if ( Scale > 0 )
				url.AppendFormat( "&x={0}", Scale );
			if ( MaxWidth > 0 )
				url.AppendFormat( "&w={0}", MaxWidth );
			if ( MaxHeight > 0 )
				url.AppendFormat( "&h={0}", MaxHeight );
			return ( url.ToString() );
		}

	}

	/// <summary>
	/// HyperLink subclass using the ThumbNailer HttpHandler 
	/// </summary>
	public class ThumbNailButton : System.Web.UI.WebControls.HyperLink {

		#region Properties

		public string HandlerUrl {
			get {
				object obj = ViewState["HandlerUrl"];
				return ( obj == null ? "~/thumbnailer.ashx" : (string) obj );
			}
			set { ViewState["HandlerUrl"] = value; }
		}

		public override string ImageUrl {
			get { return ( CreateImagerUrl() ); }
		}

		public string SourceImageUrl {
			get {
				object obj = ViewState["SourceImageUrl"];
				return ( obj == null ? "" : (string) obj );
			}
			set {
				ViewState["SourceImageUrl"] = value;
				ImageUrl = CreateImagerUrl();
				NavigateUrl = value;
			}
		}

		public double Scale {
			get {
				object obj = ViewState["Scale"];
				return ( obj == null ? (double) 0 : (double) obj );
			}
			set { ViewState["Scale"] = value; }
		}

		public int MaxWidth {
			get {
				object obj = ViewState["MaxWidth"];
				return ( obj == null ? (int) 0 : (int) obj );
			}
			set { ViewState["MaxWidth"] = value; }
		}

		public int MaxHeight {
			get {
				object obj = ViewState["MaxHeight"];
				return ( obj == null ? (int) 0 : (int) obj );
			}
			set { ViewState["MaxHeight"] = value; }
		}

		#endregion

		protected string CreateImagerUrl() {
			if ( string.IsNullOrEmpty( SourceImageUrl ) )
				throw new Exception( "SourceImageUrl must be specified." );
			StringBuilder url = new StringBuilder( 1024 );
			url.Append( HandlerUrl );
			url.AppendFormat( "?src={0}", SourceImageUrl );
			if ( Scale > 0 )
				url.AppendFormat( "&x={0}", Scale );
			if ( MaxWidth > 0 )
				url.AppendFormat( "&w={0}", MaxWidth );
			if ( MaxHeight > 0 )
				url.AppendFormat( "&h={0}", MaxHeight );
			return ( url.ToString() );
		}

	}

}
