using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools;
using StarrTech.WebTools.Controls;

namespace StarrTech.WebTools.Controls {

	public class LoginPanel : Table {

		#region Properties

		#region Controls
		protected Label UserIDLbl;
		protected TextBox UserIDBox;

		protected Label UserPwLbl;
		protected TextBox UserPwBox;

		protected CheckBox SaveToClientXBox;
		protected System.Web.UI.WebControls.Button SubmitBtn;
		protected System.Web.UI.WebControls.Button IForgotBtn;
		#endregion


		#endregion

		#region Constructor

		public LoginPanel() : base() {
			// <table cellspacing="5" cellpadding="10" border="1" width="340" style="margin:40px; background-color: #dddddd">
			this.CellSpacing = 5;
			this.CellPadding = 01;
			this.BorderWidth = 1;
			this.Width = new Unit( "340px" );
			this.Style[HtmlTextWriterStyle.Margin] = "40px";
			this.BackColor = Color.LightGray; // #dddddd
		}

		#endregion

		#region Methods

		protected override void CreateChildControls() {
			Table table = new Table();
			#region User ID
			//        <tr>
			//          <td align="right">
			//            <asp:label runat="server" id="useridlbl" text="Email&nbsp;Address:" font-names="tahoma" font-size="8pt" font-bold="true" />
			//          </td>
			//          <td>
			//            <asp:textbox runat="server" id="UserIDBox" columns="20" font-names="tahoma" />
			//          </td>
			//        </tr>
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Right;
			this.UserIDLbl = new Label();
			this.UserIDLbl.Text = "Email&nbsp;Address:";
			this.UserIDLbl.Font.Name = "Tahoma";
			this.UserIDLbl.Font.Size = new FontUnit( "8pt" );
			this.UserIDLbl.Font.Bold = true;
			cell.Controls.Add( this.UserIDLbl );
			row.Cells.Add( cell );
			cell = new TableCell();
			this.UserIDBox = new TextBox();
			this.UserIDBox.Columns = 20;
			this.UserIDBox.Font.Name = "Tahoma";
			this.UserIDBox.Font.Size = new FontUnit( "8pt" );
			this.UserIDBox.Font.Bold = false;
			cell.Controls.Add( this.UserIDBox );
			row.Cells.Add( cell );
			table.Rows.Add( row );
			#endregion
			#region Password
			//        <tr>
			//          <td align="right">
			//            <asp:label runat="server" id="userpwlbl" text="Password:" font-names="tahoma" font-size="8pt" font-bold="true" />
			//          </td>
			//          <td>
			//            <asp:textbox runat="server" id="UserPWBox" columns="20" textmode="password" font-names="tahoma" />
			//          </td>
			//        </tr>
			row = new TableRow();
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Right;
			this.UserPwLbl = new Label();
			this.UserPwLbl.Text = "Password:";
			this.UserPwLbl.Font.Name = "Tahoma";
			this.UserPwLbl.Font.Size = new FontUnit( "8pt" );
			this.UserPwLbl.Font.Bold = true;
			cell.Controls.Add( this.UserPwLbl );
			row.Cells.Add( cell );
			cell = new TableCell();
			this.UserPwBox = new TextBox();
			this.UserPwBox.Columns = 20;
			this.UserPwBox.Font.Name = "Tahoma";
			this.UserPwBox.Font.Size = new FontUnit( "8pt" );
			this.UserPwBox.Font.Bold = false;
			this.UserPwBox.TextMode = TextBoxMode.Password;
			cell.Controls.Add( this.UserPwBox );
			row.Cells.Add( cell );
			table.Rows.Add( row );
			#endregion
			#region Save to computer xbox
			//        <tr>
			//          <td colspan="2" align="center">
			//            <asp:checkbox runat="server" id="SaveToClientXBox" text="Save this information on my computer" font-names="tahoma" font-size="8pt" font-bold="true" 
			//            visible="false" />
			//          </td>
			//        </tr>
			row = new TableRow();
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.HorizontalAlign = HorizontalAlign.Center;
			this.SaveToClientXBox = new CheckBox();
			this.SaveToClientXBox.Text = "Save this information on my computer";
			this.SaveToClientXBox.Font.Name = "Tahoma";
			this.SaveToClientXBox.Font.Size = new FontUnit( "8pt" );
			this.SaveToClientXBox.Font.Bold = true;
			cell.Controls.Add( this.SaveToClientXBox );
			row.Cells.Add( cell );
			table.Rows.Add( row );
			#endregion
			#region Buttons
			//        <tr>
			//          <td colspan="2" align="center">
			//            <asp:button runat="server" id="submitbtn" text="Sign In" forecolor="#006600" 
			//              onclick="DoSignIn" width="80" font-names="tahoma" font-size="10pt" font-bold="true" />
			//            <asp:button runat="server" id="iforgotbtn" text="I Forgot" forecolor="#990000" 
			//              onclick="DoIForgot" width="80" font-names="tahoma" font-size="10pt" font-bold="true" />
			//          </td>
			//        </tr>
			row = new TableRow();
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.HorizontalAlign = HorizontalAlign.Center;
			this.SubmitBtn = new System.Web.UI.WebControls.Button();
			this.SubmitBtn.Text = "Sign In";
			this.SubmitBtn.Width = new Unit( "80px" );
			this.SubmitBtn.Font.Name = "Tahoma";
			this.SubmitBtn.Font.Size = new FontUnit( "10pt" );
			this.SubmitBtn.Font.Bold = true;
			//this.SubmitBtn.Click += new EventHandler( this.DoSignIn ); 
			cell.Controls.Add( this.SubmitBtn );
			this.IForgotBtn = new System.Web.UI.WebControls.Button();
			this.IForgotBtn.Text = "Sign In";
			this.IForgotBtn.Width = new Unit( "80px" );
			this.IForgotBtn.Font.Name = "Tahoma";
			this.IForgotBtn.Font.Size = new FontUnit( "10pt" );
			this.IForgotBtn.Font.Bold = true;
			//this.IForgotBtn.Click += new EventHandler( this.DoIForgot ); 
			cell.Controls.Add( this.IForgotBtn );
			row.Cells.Add( cell );
			table.Rows.Add( row );
			#endregion

			cell = new TableCell();
			cell.Controls.Add( table );
			row = new TableRow();
			row.Cells.Add( cell );
			this.Rows.Add( row );
		}

		#endregion

	}

}

/*
//<table cellspacing="5" cellpadding="10" border="1" width="340" style="margin:40px; background-color: #dddddd">
//  <tr>
//    <td>
//      <table id="Table1" runat="server" cellspacing="0" cellpadding="5" border="0" width="100%" >
//        <tr>
//          <td align="right">
//            <asp:label runat="server" id="useridlbl" text="Email&nbsp;Address:" font-names="tahoma" font-size="8pt" font-bold="true" />
//          </td>
//          <td>
//            <asp:textbox runat="server" id="UserIDBox" columns="20" font-names="tahoma" />
//          </td>
//        </tr>
//        <tr>
//          <td align="right">
//            <asp:label runat="server" id="userpwlbl" text="Password:" font-names="tahoma" font-size="8pt" font-bold="true" />
//          </td>
//          <td>
//            <asp:textbox runat="server" id="UserPWBox" columns="20" textmode="password" font-names="tahoma" />
//          </td>
//        </tr>
//        <tr>
//          <td colspan="2" align="center">
//            <asp:checkbox runat="server" id="SaveToClientXBox" text="Save this information on my computer" font-names="tahoma" font-size="8pt" font-bold="true" 
//            visible="false" />
//          </td>
//        </tr>
//        <tr>
//          <td colspan="2" align="center">
//            <asp:button runat="server" id="submitbtn" text="Sign In" forecolor="#006600" 
//              onclick="DoSignIn" width="80" font-names="tahoma" font-size="10pt" font-bold="true" />
//            <asp:button runat="server" id="iforgotbtn" text="I Forgot" forecolor="#990000" 
//              onclick="DoIForgot" width="80" font-names="tahoma" font-size="10pt" font-bold="true" />
//          </td>
//        </tr>
//      </table>
//    </td>
//  </tr>
//</table>
 */
