using System;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.Controls {
	/// <summary>
	/// A RegularExpressionValidator subclass pre-configured to validate email-address fields.
	/// </summary>
	public class EmailValidator : RegularExpressionValidator {
		public EmailValidator() : base() {
			this.ValidationExpression = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*";
		}
	}
}
