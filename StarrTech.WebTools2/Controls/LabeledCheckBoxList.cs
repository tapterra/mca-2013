using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.Controls {

	// LabeledCheckBoxList - a labeled, scrolling checkbox list-box
	public class LabeledCheckBoxList : CheckBoxList {

		#region Properties
		
		/// <summary>
		/// 
		/// </summary>
		public  LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();
		
		/// <summary>
		/// 
		/// </summary>
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}
		
		
		/// <summary>
		/// Color used to fill in the content area when the control is disabled
		/// </summary>
		public Color DisabledBackColor {
			get { return ( this.Frame.DisabledBackColor ); }
			set { this.Frame.DisabledBackColor = value; }
		}

		/// <summary>
		/// Color used for text, etc. in the content area when the control is disabled
		/// </summary>
		public Color DisabledForeColor {
			get { return ( this.Frame.DisabledForeColor ); }
			set { this.Frame.DisabledForeColor = value; }
		}
		
		
		// TableStyle for the frame
		public TableStyle FrameStyle { 
			get { return ( Frame.FrameStyle ); }
		}
		
		
		// TableItemStyle for the frame label
		public TableItemStyle LabelStyle { 
			get { return ( Frame.LabelStyle ); }
		}
		
		
		// TableItemStyle for the frame content
		public TableItemStyle BodyStyle { 
			get { return ( Frame.BodyStyle ); }
		}
		
		
		// Width - override to setup the frame
		public override Unit Width { 
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}
		
		#endregion
		
		#region Methods
		
		public LabeledCheckBoxList() {
			this.CellPadding = 5;
			this.CellSpacing = 5;
			this.RepeatColumns = 1;
			this.RepeatDirection = RepeatDirection.Vertical;
			this.RepeatLayout = RepeatLayout.Flow;
			this.AutoPostBack = false;
			
			//this.Font.Name = SitePrefs.CheckPref( "formfont", "Verdana" );
			//this.Font.Size = SitePrefs.CheckFontUnit( "formfontsize", "8pt" );
			//this.Font.Bold = false;
			//this.Frame.LabelStyle.Font.Name = SitePrefs.CheckPref( "labelfont", "Verdana" );
			//this.Frame.LabelStyle.Font.Size = SitePrefs.CheckFontUnit( "labelfontsize", "8pt" );
			//this.Frame.LabelStyle.Font.Bold = true;
			//this.Frame.ScrollingContent = true;
			//this.Frame.FixedHeight = this.Height;
			this.Height = Unit.Empty;
			this.Style["margin"] = "0";
			this.Style["margin-bottom"] = "5px";
			this.Style["padding"] = "0";
			//this.Style["font-weight"] = "normal";
		}
				
		
		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			this.Frame.ScrollingContent = true;
			this.Frame.FixedHeight = this.Height;
			Frame.PreRender( writer );
			base.Render( writer );
			Frame.PostRender( writer );
		}
	
		
		#endregion
		
	}
	
}
