using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools;

namespace StarrTech.WebTools.Controls {

	public class TabStrip : Table, INamingContainer {

		#region Properties

		/// <summary>
		/// Comma-delimited list of tab labels
		/// </summary>
		public string TabLabels {
			get {
				object o = ViewState["TabLabels"];
				return ( o == null ? "" : (string) o );
			}
			set {
				string oldValue = (string) ViewState["TabLabels"];
				ViewState["TabLabels"] = value;
				if ( !string.IsNullOrEmpty( oldValue ) && oldValue != value ) {
					this.ChildControlsCreated = false;
				}
			}
		}
		protected ArrayList Tabs {
			get {
				return ( new ArrayList( this.TabLabels.Split( ',' ) ) );
			}
		}

		/// <summary>
		/// Index of the currently-active tab
		/// </summary>
		public int CurrentTabIndex {
			get {
				object o = ViewState["CurrentTabIndex"];
				if ( o == null ) {
					return ( 0 );
				}
				return ( (int) o );
			}
			set {
				ViewState["CurrentTabIndex"] = value;
			}
		}

		/// <summary>
		/// CSS Class for non-selected tabs
		/// </summary>
		public string TabCssClass {
			get {
				object o = ViewState["TabCssClass"];
				if ( o == null ) {
					return ( "TabCell" );
				}
				return ( (string) o );
			}
			set { ViewState["TabCssClass"] = value; }
		}

		/// <summary>
		/// CSS Class for the selected tab
		/// </summary>
		public string SelectedTabCssClass {
			get {
				object o = ViewState["SelectedTabCssClass"];
				if ( o == null ) {
					return ( "SelectedTabCell" );
				}
				return ( (string) o );
			}
			set { ViewState["SelectedTabCssClass"] = value; }
		}

		/// <summary>
		/// CSS Class for non-selected tabs during mouseover
		/// </summary>
		public string TabOverCssClass {
			get {
				object o = ViewState["TabOverCssClass"];
				if ( o == null ) {
					return ( "TabCell" );
				}
				return ( (string) o );
			}
			set { ViewState["TabOverCssClass"] = value; }
		}

		/// <summary>
		/// CSS Class for the selected tab during mouseover
		/// </summary>
		public string SelectedTabOverCssClass {
			get {
				object o = ViewState["SelectedTabOverCssClass"];
				if ( o == null ) {
					return ( "SelectedTabCell" );
				}
				return ( (string) o );
			}
			set { ViewState["SelectedTabOverCssClass"] = value; }
		}

		/// <summary>
		/// Defines whether or not form validation is triggered when a tab is clicked
		/// </summary>
		public bool CausesValidation {
			get {
				object o = ViewState["CausesValidation"];
				return ( o == null ? true : (bool) o );
			}
			set { ViewState["CausesValidation"] = value; }
		}

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}

		#endregion

		#region Constructors

		public TabStrip() {
			this.CellPadding = 0;
			this.CellSpacing = 1;
			this.BorderStyle = BorderStyle.None;
		}

		#endregion

		#region SelectionChanged Event
		/// <summary>
		/// Event raised when the current tab-index is changed.
		/// </summary>
		public event EventHandler<SelectionChangedEventArgs> SelectionChanged {
			add { Events.AddHandler( SelectionChangedEventKey, value ); }
			remove { Events.RemoveHandler( SelectionChangedEventKey, value ); }
		}
		protected virtual void OnSelectionChanged( SelectionChangedEventArgs args ) {
			EventHandler<SelectionChangedEventArgs> handler = (EventHandler<SelectionChangedEventArgs>) Events[SelectionChangedEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object SelectionChangedEventKey = new object();
		#endregion

		#region Methods

		protected override void CreateChildControls() {
			if ( !this.ChildControlsCreated ) {
				this.BuildTabs();
			}
			base.CreateChildControls();
		}

		protected void BuildTabs() {
			if ( !this.ChildControlsCreated ) {
				this.Rows.Clear();
				TableRow row = new TableRow();
				// Add a cell & TabCell for each element in Tabs
				for ( int tabIndex = 0 ; tabIndex < this.Tabs.Count ; tabIndex++ ) {
					TabCell btn = new TabCell();
					btn.ParentStrip = this;
					btn.ID = string.Format( "{0}_Tab{1}", this.ID, tabIndex );
					this.FormatTabCell( btn, tabIndex );
					btn.TabClicked += new EventHandler<TabClickedEventArgs>( TabClicked );
					row.Cells.Add( btn );
				}
				this.Rows.Add( row );
			}
		}

		public void UpdateTabs() {
			for ( int tabIndex = 0 ; tabIndex < this.Rows[0].Cells.Count ; tabIndex++ ) {
				TabCell cell = this.Rows[0].Cells[tabIndex] as TabCell;
				this.FormatTabCell( cell, tabIndex );
			}
		}

		protected void FormatTabCell( TabCell btn, int tabIndex ) {
			btn.Text = this.Tabs[tabIndex] as string;
			btn.CausesValidation = this.CausesValidation;
			bool isCurrentTab = tabIndex == this.CurrentTabIndex;
			btn.CssClass = isCurrentTab ? this.SelectedTabCssClass : this.TabCssClass;
			btn.Attributes["onmouseover"] = string.Format(
				"javascript:this.className='{0}';", isCurrentTab ? this.SelectedTabOverCssClass : this.TabOverCssClass
			);
			btn.Attributes["onmouseout"] = string.Format(
				"javascript:this.className='{0}';", isCurrentTab ? this.SelectedTabCssClass : this.TabCssClass
			);
			if ( !isCurrentTab ) {
				btn.TabNumber = tabIndex;
			} else {
				btn.TabNumber = -1;
				btn.CausesValidation = false;
			}
		}

		void TabClicked( object sender, TabClickedEventArgs e ) {
			this.Select( e.index );
		}

		public void Select( int newIndex ) {
			// Ensure the index is a valid value
			if ( newIndex < 0 || newIndex > Tabs.Count ) {
				newIndex = 0;
			}
			// Keep track of the old setting for the event-handlers
			int oldIndex = this.CurrentTabIndex;
			// Updates the current index. Must write directly to viewstate because the property is read-only
			ViewState["CurrentTabIndex"] = newIndex;
			// Refresh the UI
			this.UpdateTabs();
			// Fire the SelectionChanged event
			this.OnSelectionChanged( new SelectionChangedEventArgs( oldIndex, newIndex ) );
		}

		protected override void Render( HtmlTextWriter writer ) {
			foreach ( TabCell cell in this.Rows[0].Cells ) {
				if ( !this.Enabled ) {
					cell.Attributes["onmouseover"] = "";
					cell.Attributes["onmouseout"] = "";
				}
				cell.Enabled = this.Enabled;
			}
			base.Render( writer );
		}
		#endregion

	}

	/// <summary>
	/// Button class styled for use as a TabStrip tab
	/// </summary>
	public class TabCell : TableCell, IPostBackEventHandler {

		public TabStrip ParentStrip;
		public bool CausesValidation = true;
		public int TabNumber = -1;

		public TabCell() { }

		#region TabClicked Event
		/// <summary>
		/// Event raised when the current tab-index is changed.
		/// </summary>
		public event EventHandler<TabClickedEventArgs> TabClicked {
			add { Events.AddHandler( TabClickedEventKey, value ); }
			remove { Events.RemoveHandler( TabClickedEventKey, value ); }
		}
		protected virtual void OnTabClicked( TabClickedEventArgs args ) {
			EventHandler<TabClickedEventArgs> handler = (EventHandler<TabClickedEventArgs>) Events[TabClickedEventKey];
			if ( handler != null ) {
				handler( this, args );
			}
		}
		private static readonly object TabClickedEventKey = new object();
		#endregion

		protected override void Render( HtmlTextWriter writer ) {
			if ( this.TabNumber >= 0 && this.Enabled ) {
				PostBackOptions options = new PostBackOptions( this, this.TabNumber.ToString() );
				options.PerformValidation = this.CausesValidation;
				this.Attributes["onclick"] = this.Page.ClientScript.GetPostBackEventReference( options, true );
			}
			base.Render( writer );
		}

		public void RaisePostBackEvent( String arg ) {
			int tabIndex = Convert.ToInt32( arg );
			this.OnTabClicked( new TabClickedEventArgs( tabIndex ) );
		}

	}

	/// <summary>
	/// EventArgs class for the TabStrip SelectionChanged event
	/// </summary>
	public class SelectionChangedEventArgs : EventArgs {
		public int OldIndex;
		public int NewIndex;
		public SelectionChangedEventArgs( int Old, int New ) {
			this.OldIndex = Old;
			this.NewIndex = New;
		}
	}

	public class TabClickedEventArgs : EventArgs {
		public int index;
		public TabClickedEventArgs( int x ) {
			this.index = x;
		}
	}

}
