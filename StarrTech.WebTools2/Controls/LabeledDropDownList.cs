using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace StarrTech.WebTools.Controls {

	/// <summary>
	/// A labeled DropDownList control
	/// </summary>
	public class LabeledDropDownList : DropDownList {

		#region Properties
		
		private LabelFramer frame = new LabelFramer();
		public  LabelFramer Frame {
			get { return ( frame ); }
		}
		
		/// <summary>
		/// Color used to fill in the content area when the control is disabled
		/// </summary>
		public Color DisabledBackColor {
			get { return ( this.Frame.DisabledBackColor ); }
			set { this.Frame.DisabledBackColor = value; }
		}

		/// <summary>
		/// Color used for text, etc. in the content area when the control is disabled
		/// </summary>
		public Color DisabledForeColor {
			get { return ( this.Frame.DisabledForeColor ); }
			set { this.Frame.DisabledForeColor = value; }
		}
		
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}
		
		/// <summary>
		/// TableStyle for the frame
		/// </summary>
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}
		
		/// <summary>
		/// TableItemStyle for the frame label
		/// </summary>
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}
		
		/// <summary>
		/// TableItemStyle for the frame content
		/// </summary>
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}
					
		public override Unit Width {
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}

		public override string SelectedValue {
			get {
				EnsureChildControls();
				if ( SelectedItem != null ) {
					return ( SelectedItem.Value );
				}
				return ( "" );
			}
			set {
				EnsureChildControls();
				ListItem selitem = Items.FindByValue( value );
				if ( selitem != null ) {
					SelectedIndex = Items.IndexOf( selitem );
				} else {
					HttpContext.Current.Trace.Write( "Unknown value provided for SelectedValue: " + value );
				}
			}
		}
		
		public virtual string SelectedText {
			get { 
				EnsureChildControls();
				if ( SelectedItem != null ) {
					return ( SelectedItem.Text ); 
				}
				return ( "" );
			}
			set { 
				EnsureChildControls();
				ListItem selitem = Items.FindByText( value );
				if ( selitem != null ) SelectedIndex = Items.IndexOf( selitem );
				else HttpContext.Current.Trace.Write( "Unknown value provided for SelectedText: " + value );
			}
		}
					
		#endregion
		
		#region Methods
		
		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			if ( ! Enabled ) {
				Frame.Style["margin-bottom"] = "4px";
			}
			Frame.PreRender( writer );
			if ( Enabled ) {
				Style["margin-bottom"] = "4px";
				base.Render( writer );
			} else {
				Frame.RenderDisabled( writer, Font, SelectedText );
			}
			Frame.PostRender( writer );
		}
		
		#endregion

	}
	
}
