using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
	

namespace StarrTech.WebTools.Controls {

	[ParseChildrenAttribute(ChildrenAsProperties = false)]
	public class LabeledPanel : WebControl {
		
		#region Properties
		
		public  LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();

		/// <summary>
		/// Color used to fill in the content area when the control is disabled
		/// </summary>
		public Color DisabledBackColor {
			get { return ( this.Frame.DisabledBackColor ); }
			set { this.Frame.DisabledBackColor = value; }
		}

		/// <summary>
		/// Color used for text, etc. in the content area when the control is disabled
		/// </summary>
		public Color DisabledForeColor {
			get { return ( this.Frame.DisabledForeColor ); }
			set { this.Frame.DisabledForeColor = value; }
		}
		
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}
				
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}

		// LabelStyle - TableItemStyle for the frame label
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}
					
		// BodyStyle - TableItemStyle for the frame content
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}
					
		#endregion

		#region  Methods
		public LabeledPanel() {
			//BodyStyle.Font.Name = SitePrefs.GetPref("formfont");
			//BodyStyle.Font.Size = new FontUnit( SitePrefs.GetPref("formfontsize") );
			//BodyStyle.Font.Bold = false;
			//Frame.LabelStyle.Font.Name = SitePrefs.CheckPref("labelfont");
			//Frame.LabelStyle.Font.Size = new FontUnit( SitePrefs.CheckPref("labelfontsize") );
			//Frame.LabelStyle.Font.Bold = true;
			Frame.SetCellWidths( Width );
			Frame.LabelStyle.Wrap = false;
			//Frame.LabelMargin = 0;
			//Style["margin"] = "0px";
			//Style.Add( "margin-bottom", "4px" );
		}

		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			Frame.FrameStyle.Width = Width;
			Frame.PreRender( writer );
			RenderChildren( writer );
			Frame.PostRender( writer );
		}
				
		#endregion

	}
		
	

}
