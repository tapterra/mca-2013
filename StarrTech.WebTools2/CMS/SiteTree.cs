using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Text;
using System.Xml;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Represents a key/label tag from the SiteTrees section in web.config
	/// </summary>
	public class SiteTree {

		public string Key {
			get { return ( this.key ); }
			set { this.key = value; }
		}
		private string key;

		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label;

		public Tree Tree {
			get { return ( new Tree( this.Key ) ); }
		}

		public SiteTree( XmlNode node ) {
			XmlAttribute attr;
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "key" );
			if ( attr != null ) {
				this.Key = attr.Value;
			}
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "label" );
			if ( attr != null ) {
				this.Label = attr.Value;
			}
		}

		protected internal SiteTree( string Key, string Label ) {
			this.Key = Key;
			this.Label = Label;
		}

	}

	/// <summary>
	/// Custom collection class for handling a list of SiteTrees
	/// </summary>
	public class SiteTreesList : KeyedCollection<string, SiteTree> {

		public static SiteTreesList Current {
			get {
				SiteTreesList trees = (SiteTreesList) ConfigurationManager.GetSection( "starrtech.cms/SiteTrees" );
				if ( trees == null ) {
					// Generate the defaults...
					trees = new SiteTreesList();
					trees.Add( new SiteTree( ContentPublisher.MainNavTreeKey, "Main Menu" ) );
					trees.Add( new SiteTree( ContentPublisher.AlternateNavTreeKey, "Footer Menu" ) );
				}
				return ( trees ); 
			}
		}

		public SiteTreesList() : base() { }

		protected override string GetKeyForItem( SiteTree item ) {
			return item.Key;
		}

	}

	/// <summary>
	/// Config-file section handler for SiteTrees
	/// </summary>
	public class SiteTreesConfigSectionHandler : IConfigurationSectionHandler {

		object IConfigurationSectionHandler.Create( object parent, object configContext, XmlNode section ) {
			SiteTreesList trees = new SiteTreesList();
			XmlNodeList nodes = section.ChildNodes;
			if ( nodes == null || nodes.Count == 0 ) {
				// Generate the defaults...
				trees.Add( new SiteTree( ContentPublisher.MainNavTreeKey, "Main Menu" ) );
				trees.Add( new SiteTree( ContentPublisher.AlternateNavTreeKey, "Footer Menu" ) );
			} else {
				foreach ( XmlNode node in nodes ) {
					trees.Add( new SiteTree( node ) );
				}
			}
			return ( trees );
		}

	}

}
