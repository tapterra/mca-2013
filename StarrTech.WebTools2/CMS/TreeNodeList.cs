using System;
using System.Collections.ObjectModel;

namespace StarrTech.WebTools.CMS {
	
	public class TreeNodeList : KeyedCollection<int, TreeNode> {
		
		#region Properties
		
		/// <summary>
		/// The Tree or TreeNode this list is holding descendents for (as a BaseTreeNode).
		/// </summary>
		public BaseTreeNode Owner {
			get { return ( this.owner ); }
		}
		private BaseTreeNode owner = null;
		
		#endregion
		
		#region Constructor
		
		public TreeNodeList( BaseTreeNode owner ) : base() {
			this.owner = owner;
		}
		
		#endregion
		
		#region Public Methods

		protected override int GetKeyForItem( TreeNode Node ) {
			return Node.ID;
		}

		/// <summary>
		///		Add the given node to the collection
		/// </summary>
		/// <param name="node">Node to add</param>
		public new void Add( TreeNode NodeToAdd ) {
			if ( NodeToAdd == null ) return;
			NodeToAdd.ParentNode = this.Owner;
			NodeToAdd.TreeID = this.Owner.TreeID;
			if ( NodeToAdd.ID <= 0 ) NodeToAdd.Persist();
			base.Add( NodeToAdd );
		}
		
		/// <summary>
		///		Removes a given node from the collection
		/// </summary>
		/// <param name="node">The node to remove.</param>
		public new void Remove( TreeNode NodeToRemove ) {
			if ( NodeToRemove == null ) return;
			NodeToRemove.ParentNode = null;
			base.Remove( NodeToRemove );
		}
		
		/// <summary>
		///		Find the node at the start of the sibling chain (node.PrevSibID <= 0)
		/// </summary>
		/// <returns>The first node in the sibling chain</returns>
		public TreeNode FindLastNode() {
			foreach ( TreeNode node in this ) {
				if ( node.NextNodeID <= 0 ) return ( node );
			}
			return ( null );
		}
		
		/// <summary>
		///		Find a given node's previous sibling
		/// </summary>
		/// <param name="node">A node to test</param>
		/// <returns>The node immediately preceeding the given node in the sibling chain</returns>
		public TreeNode FindNextNode( TreeNode node ) {
			if ( node == null ) return ( null );
			if ( node.NextNodeID <= 0 ) return ( null );
			return ( this[node.NextNodeID] as TreeNode  );
		}
		
		/// <summary>
		///		Find a given node's next sibling
		/// </summary>
		/// <param name="node">A node to test</param>
		/// <returns>The node immediately following the given node in the sibling chain</returns>
		public TreeNode FindPrevNode( TreeNode node ) {
			if ( node == null ) return ( null );
			foreach ( TreeNode testnode in this ) {
				if ( testnode.NextNodeID == node.ID ) return ( testnode );
			}
			return ( null );
		}
		
		/// <summary>
		///		Find the node at the beginning of the sibling chain
		/// </summary>
		/// <returns>The last node in the sibling chain</returns>
		public TreeNode FindFirstNode() {
			foreach ( TreeNode node in this ) {
				if ( this.FindPrevNode( node ) == null ) return ( node );
			}
			return ( null );
		}
		
		/// <summary>
		/// Find a node anywhere in the descendent tree by it's PathKey
		/// </summary>
		/// <param name="key">PathKey value for the node to find.</param>
		/// <returns>The node with the matching key, or null if not found.</returns>
		public TreeNode FindNode( string key ) {
			// Search immediate children first
			foreach ( TreeNode node in this ) {
				if ( node.PathKey == key ) return ( node );
			}
			// Search all the descendents
			foreach ( TreeNode node in this ) {
				TreeNode found = node.Nodes.FindNode( key );
				if ( found != null ) return ( found );
			}
			// No joy...
			return ( null );
		}
		
		/// <summary>
		/// Find any node anywhere in the descendent tree with a matching TypeKey
		/// </summary>
		/// <param name="key">TypeKey value for the node to find.</param>
		/// <returns>The first node found with a matching TypeKey, or null if not found.</returns>
		public TreeNode FindNodeByTypeKey( string typeKey ) {
			// Search immediate children first
			foreach ( TreeNode node in this ) {
				if ( node.TypeKey == typeKey ) return ( node );
			}
			// Search all the descendents
			foreach ( TreeNode node in this ) {
				TreeNode found = node.Nodes.FindNodeByTypeKey( typeKey );
				if ( found != null ) return ( found );
			}
			return ( null );
		}
		
		/// <summary>
		/// Find a node anywhere in the descendent tree by it's ID
		/// </summary>
		/// <param name="id">Record ID value for the node to find.</param>
		/// <returns>The node with the matching ID, or null if not found.</returns>
		public TreeNode FindNode( int id ) {
			// Search immediate children first
			foreach ( TreeNode node in this ) {
				if ( node.ID == id ) return ( node );
			}
			// Search all the descendents
			foreach ( TreeNode node in this ) {
				TreeNode found = node.Nodes.FindNode( id );
				if ( found != null ) return ( found );
			}
			return ( null );
		}
		
		#endregion
		
	}
	
}
