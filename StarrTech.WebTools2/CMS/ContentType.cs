using System;
using System.Xml;

namespace StarrTech.WebTools.CMS {

	public class ContentType {

		#region Properties

		public string TypeKey {
			get { return ( this.typeKey ); }
			set { this.typeKey = value; }
		}
		private string typeKey = "";

		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label = "";

		public string Description {
			get { return ( this.description ); }
			set { this.description = value; }
		}
		private string description = "";

		public bool BaseSiteOnly {
			get { return ( this.baseSiteOnly ); }
			set { this.baseSiteOnly = value; }
		}
		private bool baseSiteOnly = false;

		public bool MustBeRoot {
			get { return ( this.mustBeRoot ); }
			set { this.mustBeRoot = value; }
		}
		private bool mustBeRoot = false;

		public bool CanDelete {
			get { return ( this.canDelete ); }
			set { this.canDelete = value; }
		}
		private bool canDelete = true;

		public bool CanEdit {
			get { return ( this.canEdit ); }
			set { this.canEdit = value; }
		}
		private bool canEdit = true;

		public bool CanAddChildren {
			get { return ( this.canAddChildren ); }
			set { this.canAddChildren = value; }
		}
		private bool canAddChildren = true;

		public bool CanBeOnlyOne {
			get { return ( this.canBeOnlyOne ); }
			set { this.canBeOnlyOne = value; }
		}
		private bool canBeOnlyOne = false;

		public bool CanMoveV {
			get { return ( this.canMoveV ); }
			set { this.canMoveV = value; }
		}
		private bool canMoveV = true;

		public bool CanMoveH {
			get { return ( this.canMoveH ); }
			set { this.canMoveH = value; }
		}
		private bool canMoveH = true;

		public bool AllowActivation {
			get { return ( this.allowActivation ); }
			set { this.allowActivation = value; }
		}
		private bool allowActivation = false;

		public bool ActivationRequired {
			get { return ( this.activationRequired ); }
			set { this.activationRequired = value; }
		}
		private bool activationRequired = false;

		public string PublishedAt {
			get { return ( this.publishedAt ); }
			set { this.publishedAt = value; }
		}
		private string publishedAt = "";

		public ContentTypeEditors EditorType {
			get { return ( this.editorType ); }
			set { this.editorType = value; }
		}
		private ContentTypeEditors editorType = ContentTypeEditors.contentform;

		public string EditorSrc {
			get { return ( this.editorSrc ); }
			set { this.editorSrc = value; }
		}
		private string editorSrc = "";

		public ContentItemPartsList Parts {
			get { return ( this.parts ); }
		}
		private ContentItemPartsList parts = new ContentItemPartsList();

		public ContentItemPartsList HomePageParts {
			get { return ( this.homePageParts ); }
		}
		private ContentItemPartsList homePageParts = new ContentItemPartsList();

		#endregion

		#region Constructors

		public ContentType( XmlNode node ) {
			XmlAttribute attr = (XmlAttribute) node.Attributes.GetNamedItem( "TypeKey" );
			if ( attr != null )
				this.TypeKey = attr.Value;
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "Label" );
			if ( attr != null )
				this.Label = attr.Value;
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "BaseSiteOnly" );
			if ( attr != null )
				this.BaseSiteOnly = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "MustBeRoot" );
			if ( attr != null )
				this.MustBeRoot = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanDelete" );
			if ( attr != null )
				this.CanDelete = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanEdit" );
			if ( attr != null )
				this.CanEdit = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanAddChildren" );
			if ( attr != null )
				this.CanAddChildren = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanBeOnlyOne" );
			if ( attr != null )
				this.CanBeOnlyOne = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanMoveV" );
			if ( attr != null )
				this.CanMoveV = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "CanMoveH" );
			if ( attr != null )
				this.CanMoveH = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "AllowActivation" );
			if ( attr != null )
				this.AllowActivation = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "ActivationRequired" );
			if ( attr != null )
				this.ActivationRequired = Convert.ToBoolean( attr.Value );
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "PublishedAt" );
			if ( attr != null )
				this.PublishedAt = attr.Value;
			XmlNode dNode = node.SelectSingleNode( "Description" );
			if ( dNode != null )
				this.Description = dNode.InnerText;
			dNode = node.SelectSingleNode( "HomePageItem" );
			if ( dNode != null ) {
				XmlNodeList partNodes = dNode.SelectNodes( "ContentItemParts/ContentItemPart" );
				this.HomePageParts.Clear();
				foreach ( XmlNode partNode in partNodes ) {
					this.HomePageParts.Add( new ContentItemPart( partNode ) );
				}
			}
			dNode = node.SelectSingleNode( "Editor" );
			if ( dNode != null ) {
				attr = (XmlAttribute) dNode.Attributes.GetNamedItem( "Type" );
				if ( attr != null )
					this.EditorType = (ContentTypeEditors) Enum.Parse( typeof( ContentTypeEditors ), attr.Value, true );
				attr = (XmlAttribute) dNode.Attributes.GetNamedItem( "Src" );
				if ( attr != null )
					this.EditorSrc = attr.Value;
				XmlNodeList partNodes = dNode.SelectNodes( "ContentItemParts/ContentItemPart" );
				this.Parts.Clear();
				foreach ( XmlNode partNode in partNodes ) {
					this.Parts.Add( new ContentItemPart( partNode ) );
				}
			}
		}

		#endregion

	}

}
