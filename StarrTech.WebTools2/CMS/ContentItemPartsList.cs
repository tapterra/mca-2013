using System;
using System.Collections.ObjectModel;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Custom collection class for handling the list of ContentItemParts in a ContentItemVersion
	/// </summary>
	// Assumptions:
	//	[] We want to key on part-number (not ID) so that we can iterate thru the parts in order, find the latest, etc.
	//	[] Once a part is created/persisted, we will never delete it (only change it's version's status)
	//	[] Part numbers will always be in sequence from 1 to N with N being the last part
	//	[] The part-number for the last part will always be this.Count.
	//	[] The next appropriate part-number for a new part will always be this.Count + 1
	public class ContentItemPartsList : KeyedCollection<string, ContentItemPart> {

		#region Properties
		
		/// <summary>
		/// The ContentItemVersion this list is holding Parts for.
		/// </summary>
		public ContentItemVersion Owner {
			get { return ( this.owner ); }
		}
		private ContentItemVersion owner = null;
		
		/// <summary>
		/// Returns the part-number of the most-recently created (last) part in the list
		/// </summary>
		public int MostRecentPartNo {
			get { return ( this.Count ); }
		}
		
		/// <summary>
		/// Returns the part-number to be assigned to a new (not yet added) part
		/// </summary>
		public int NextPartNo {
			get { return ( this.Count + 1 ); }
		}
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Use this one for the ContentType parts-list
		/// </summary>
		public ContentItemPartsList() : base() {
			this.owner = null;
		}
		
		public ContentItemPartsList( ContentItemVersion owner ) : base() {
			this.owner = owner;
		}
		
		#endregion

		protected override string GetKeyForItem( ContentItemPart part ) {
			return part.Name;
		}
		
	}
	
}
