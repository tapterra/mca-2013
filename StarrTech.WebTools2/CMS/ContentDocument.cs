using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using StarrTech.WebTools.Data;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Represents a binary file/document (Word, Excel, Jpeg, PDF, etc.) persisted in the CMS_DOCUMENTS sql table
	/// </summary>
	[SqlTable( "CMS_DOCUMENTS", SingularLabel = "Document", PluralLabel = "Documents", IsMarkedDeleted = true )]
	public class ContentDocument : BaseSqlPersistable {

		#region Properties

		#region Database Columns

		/// <summary>
		/// Date/time the file was uploaded
		/// </summary>
		[SqlColumn( "posted_dt", DbType.DateTime )]
		public DateTime PostedDT {
			get { return ( this.postedDT ); }
			set { this.postedDT = value; }
		}
		private DateTime postedDT = DateTime.Now;

		/// <summary>
		/// Flag indicating the document is approved for publication
		/// </summary>
		[SqlColumn( "is_approved", DbType.Boolean )]
		public bool IsApproved {
			get { return ( this.isApproved ); }
			set { this.isApproved = value; }
		}
		private bool isApproved = false;

		/// <summary>
		/// Content MIME type of the document
		/// </summary>
		[SqlColumn( "mime_type", DbType.String, Length = 50 )]
		public string MimeType {
			get { return ( this.mimeType ); }
			set { this.mimeType = value; }
		}
		private string mimeType = "";

		/// <summary>
		/// Name of the uploaded/downloaded file
		/// </summary>
		[SqlColumn( "file_name", DbType.String, Length = 200 )]
		public string FileName {
			get { return ( this.fileName ); }
			set { this.fileName = value; }
		}
		private string fileName = "";

		/// <summary>
		/// Published title of the document
		/// </summary>
		[SqlColumn( "title", DbType.String, Length = 250 )]
		public string Title {
			get { return ( this.title ); }
			set { this.title = value; }
		}
		private string title = "";

		/// <summary>
		/// Published description
		/// </summary>
		[SqlColumn( "description", DbType.String )]
		public string Description {
			get { return ( this.description ); }
			set { this.description = value; }
		}
		private string description = "";

		/// <summary>
		/// Actual content of the document (restored and persisted on demand)
		/// </summary>
		// For performance sake, we don't want to map this property to it's column in the database
		// We only want to load this data when it's needed by the HttpHandlers for display/download
		//[SqlColumn( "document_data", DbType.Binary )]
		protected internal virtual byte[] DocumentData {
			get {
				if ( this.documentData == null ) {
					this.RestoreDocumentData();
				}
				return ( this.documentData );
			}
			set {
				this.documentData = value;
				this.PersistDocumentData();
			}
		}
		private byte[] documentData = null;

		/// <summary>
		/// Number of bytes in document data
		/// </summary>
		[SqlColumn( "data_size", DbType.Int32 )]
		public int DataSize {
			get { return ( this.dataSize ); }
			set { this.dataSize = value; }
		}
		private int dataSize;

		/// <summary>
		/// Administrative comments
		/// </summary>
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		/// <summary>
		/// Node ID for the document-library folder this document is assigned to
		/// </summary>
		public virtual int FolderNodeID {
			get { return ( this.folderNodeID ); }
			set { this.folderNodeID = value; }
		}
		private int folderNodeID;

		#endregion

		#region Publishing

		/// <summary>
		/// Formatted string representation of the DataSize property
		/// </summary>
		public string DataSizeStr {
			get {
				if ( this.DataSize < 1024 ) {
					return ( string.Format( "{0:N0}B", this.DataSize ) );
				} else if ( this.DataSize < 1048576 ) {
					return ( string.Format( "{0:N0}K", this.DataSize / 1024 ) );
				} else {
					return ( string.Format( "{0:N0}M", this.DataSize / 1048576 ) );
				}
			}
		}

		/// <summary>
		/// Formatted string representation of the PostedDT property
		/// </summary>
		public string PostedDTStr {
			get { return ( this.PostedDT == DateTime.MinValue ? "" : this.PostedDT.ToString( "d" ) ); }
		}

		/// <summary>
		/// Returns an IMG tag for the eyeball glyph if the document is approved (yes, this is a hack)
		/// </summary>
		public string IsApprovedMark {
			get { return ( this.IsApproved ? "<img src='../images/eyeball.gif' />" : "" ); }
		}

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static string UrlFormat = "CMSDocument.axd?id={0}";

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static string DownloadUrlFormat = "CMSDocument.axd?id={0}&ax=1";

		/// <summary>
		/// Returns the URL required to download this document (via the CMSDocumentHandler).
		/// Note: Assumes that the following handler is installed in the &lt;httpHandlers&gt; section of web.config:
		/// &lt;add verb="GET" path="CMSDocument.axd" type="StarrTech.WebTools.CMS.CMSDocumentHandler, StarrTech.WebTools" /&gt;
		/// </summary>
		public virtual string DownloadUrl {
			get { return ( string.Format( DownloadUrlFormat, this.ID ) ); }
		}

		/// <summary>
		/// Returns the URL to the file-list icon resource appropriate for the given mime-type & filename (extention).
		/// </summary>
		/// <param name="MimeType">A mime-type value</param>
		/// <param name="FileName">A file name (for its extention)</param>
		/// <returns>The URL to use to display a 16x16 icon for the given file</returns>
		public static string FileTypeListIconImageResourceName( string MimeType, string FileName ) {
			// First, find the right resource icon-image file to use...
			string resName = "unknown.gif";
			if ( MimeType.StartsWith( "image/" ) ) {
				switch ( MimeType ) {
					case MimeTypes.ImageBmp:
						resName = "image_bmp.gif";
						break;
					case MimeTypes.ImageGif:
						resName = "image_gif.gif";
						break;
					case MimeTypes.ImagePng:
						resName = "image_jpeg.gif";
						break;
					case MimeTypes.ImageJpeg:
					case "image/pjpeg":
						resName = "image_jpeg.gif";
						break;
					default:
						resName = "image_bmp.gif";
						break;
				}
			} else if ( MimeType.StartsWith( "text/" ) ) {
				switch ( MimeType ) {
					case "text/html":
						resName = "text_html.gif";
						break;
					case "text/javascript":
						resName = "text_javascript.gif";
						break;
					default:
						resName = "text_plain.gif";
						break;
				}
			} else {
				switch ( MimeType ) {
					case "application/vnd.ms-access":
						resName = "ms-access.gif";
						break;
					case "application/vnd.ms-excel":
						resName = "ms-excel.gif";
						break;
					case "application/vnd.ms-powerpoint":
						resName = "ms-powerpoint.gif";
						break;
					case "application/vnd.ms-publisher":
						resName = "ms-publisher.gif";
						break;
					case "application/vnd.visio":
						resName = "ms-visio.gif";
						break;
					case "application/msword":
						resName = "ms-word.gif";
						break;
					case "folder":
						resName = "folder.gif";
						break;
					default:
						if ( FileName.EndsWith( ".pdf" ) ) {
							resName = "pdf.gif";
						} else if ( FileName.EndsWith( ".chm" ) ) {
							resName = "chm.gif";
						} else if ( FileName.EndsWith( ".psd" ) ) {
							resName = "photoshop.gif";
						} else if ( FileName.EndsWith( ".msi" ) ) {
							resName = "install.gif";
						} else if ( MimeType.IndexOf( "compressed" ) > 0 ) {
							resName = "zip.gif";
						} else if ( FileName.EndsWith( ".exe" ) ) {
							resName = "exe.gif";
						} else if ( FileName.EndsWith( ".dll" ) ) {
							resName = "exe.gif";
						} else {
							resName = "unknown.gif";
						}
						break;
				}
			}
			return ( "StarrTech.WebTools.Resources.docicons." + resName );
		}

		/// <summary>
		/// Generates a URL to the appropriate embedded-resource image for this document-type's list icon
		/// </summary>
		public string ListIconImageResourceName {
			get { return ( ContentDocument.FileTypeListIconImageResourceName( this.MimeType, this.FileName ) ); }
		}

		/// <summary>
		/// Returns true if the MimeType is that of an image/graphic.
		/// </summary>
		public virtual bool IsImage {
			get { return ( this.MimeType.StartsWith( "image" ) ); }
		}

		/// <summary>
		/// Comma-delimited list of acceptable mime-types for posted files (see HtmlInputFile.Accept).
		/// </summary>
		public virtual string AcceptedTypes {
			get { return ( this.acceptedTypes ); }
			set { this.acceptedTypes = value; }
		}
		private string acceptedTypes = "*/*";

		#endregion

		#endregion
		
		#region Constructors

		/// <summary>
		/// Create a new, empty instance
		/// </summary>
		public ContentDocument() : base() { }


		/// <summary>
		/// Restore a previously-persisted instance by ID
		/// </summary>
		/// <param name="ID">Database-record ID of the instance to restore</param>
		public ContentDocument( int ID ) : base( ID ) { }


		/// <summary>
		/// Instantiate from a typical form postback
		/// </summary>
		/// <param name="title">User-defined title for the document</param>
		/// <param name="description">User-defined description</param>
		/// <param name="comments">User-defined admin comments</param>
		/// <param name="postedFile">Uploaded file object</param>
		public ContentDocument( string title, string description, string comments, HttpPostedFile postedFile )
			: base() {
			this.Title = title;
			this.Description = description;
			this.Comments = comments;
			this.LoadPostedFile( postedFile );
		}


		#endregion

		#region Methods

		/// <summary>
		/// Save the document and insure that it's folder-node link is also persisted
		/// </summary>
		/// <returns>True if the record was successfully deleted.</returns>
		// LATER: add support for a document being assigned to more than one folder (?)
		public override bool Persist() {
			bool isNew = this.ID <= 0;
			if ( base.Persist() ) {
				if ( this.FolderNodeID > 0 ) {
					// Make sure the folder assignment is updated
					string sql;
					if ( isNew ) { // We've inserted a new record
						sql = "insert into CMS_NODE_DOCUMENTS ( tree_node_id, cms_document_id ) values ( @tree_node_id, @cms_document_id )";
					} else { // We've updated an existing document
						sql = "update CMS_NODE_DOCUMENTS set tree_node_id = @tree_node_id where cms_document_id = @cms_document_id ";
					}
					using ( SqlDatabase db = new SqlDatabase() ) {
						db.Execute( sql,
							db.NewParameter( "@tree_node_id", this.FolderNodeID ),
							db.NewParameter( "@cms_document_id", this.ID )
						);
					}
				}
				if ( this.documentData != null ) {
					// Update the document bits
					this.PersistDocumentData();
				}
				return ( true );
			}
			return ( false );
		}

		/// <summary>
		/// Delete the document and any folder-node links.
		/// </summary>
		/// <returns>True if the record was successfully deleted.</returns>
		public override bool Delete() {
			if ( base.Delete() ) {
				if ( this.FolderNodeID <= 0 ) { // No folder assigned ...
					return ( true );
				}
				using ( SqlDatabase db = new SqlDatabase() ) {
					string sql = "delete from CMS_NODE_DOCUMENTS where cms_document_id = @cms_document_id ";
					db.Execute( sql, db.NewParameter( "@cms_document_id", this.ID ) );
				}
				return ( true );
			}
			return ( false );
		}

		/// <summary>
		/// Restores the unmapped DocumentData property from the database
		/// </summary>
		protected void RestoreDocumentData() {
			if ( this.ID <= 0 ) {
				return; // Nothing to do if the instance hasn't been persisted yet
			}
			string sql = string.Format( "select document_data from {0} where id = @id", this.TableName );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@id", this.ID ) ) ) {
					while ( rdr.Read() ) {
						this.documentData = new byte[this.dataSize];
						rdr.GetBytes( 0, 0, this.documentData, 0, this.dataSize );
					}
				}
			}
		}

		/// <summary>
		/// Persists the unmapped DocumentData property to the database
		/// </summary>
		protected void PersistDocumentData() {
			if ( this.IsValidToPersist() ) {
				if ( this.ID <= 0 ) {
					this.Persist();
				}
				string sql = string.Format( "update {0} set document_data = @document_data where id = @id", this.TableName );
				using ( SqlDatabase db = new SqlDatabase() ) {
					db.Execute( sql, db.NewParameter( "@id", this.ID ), db.NewParameter( "@document_data", this.documentData ) );
				}
			}
		}

		/// <summary>
		/// Given an HttpPostedFile, update the related properties
		/// </summary>
		/// <param name="postedFile">An HttpPostedFile instance (as provided by HtmlInputFile.PostedFile).</param>
		public virtual void LoadPostedFile( HttpPostedFile postedFile ) {
			if ( postedFile.ContentLength <= 0 ) {
				return;
			}
			this.MimeType = postedFile.ContentType;
			this.DataSize = postedFile.ContentLength;
			this.FileName = postedFile.FileName.Substring( postedFile.FileName.LastIndexOf( "\\" ) + 1 );
			this.documentData = new byte[this.DataSize];
			int n = postedFile.InputStream.Read( this.documentData, 0, this.DataSize );
		}

		/// <summary>
		/// Given an HttpResponse instance, write the document to the response as a downloaded file.
		/// </summary>
		/// <param name="response">The HttpResponse instance to write to</param>
		public void DownloadDocument( HttpResponse Response ) {
			DownloadDocument( Response, true );
		}

		/// <summary>
		/// Write the document to the given HttpResponse, with an attachment-header if indicated,
		/// and scaled to a given percentage, maximum width, and/or max height (if the document is an image).
		/// </summary>
		/// <param name="response">The HttpResponse instance to write to</param>
		/// <param name="AsAttachment">A switch ti indicate the need for an attachment-header</param>
		public void DownloadDocument( HttpResponse Response, bool AsAttachment ) {
			Response.Clear();
			Response.ContentType = this.MimeType;
			if ( this.MimeType == "image/pjpeg" ) {
				Response.ContentType = "image/jpeg";
			}
			if ( AsAttachment ) {
				Response.AppendHeader( "Content-Disposition", string.Format( "attachment; filename={0}", this.FileName ) );
			}
			Response.AppendHeader( "Content-Length", this.DataSize.ToString() );
			Response.BinaryWrite( this.DocumentData );
			Response.Flush();
		}

		#endregion

		#region Static Search Members

		/// <summary>
		/// Node ID for the document-library folder we want to browse
		/// </summary>
		public static int SFolderNodeID {
			get { return ( sFolderNodeID ); }
			set { sFolderNodeID = value; }
		}
		private static int sFolderNodeID = -1;

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			SFolderNodeID = -1;
		}


		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder sql = new StringBuilder(
				@"select d.*, n.tree_node_id
				from CMS_DOCUMENTS as d left join CMS_NODE_DOCUMENTS as n on n.cms_document_id = d.id"
				);
			if ( SFolderNodeID > 0 ) {
				sql.Append( " where n.tree_node_id = @tree_node_id" );
			}
			sql.AppendFormat( " order by {0}", SortExpression );
			return ( sql.ToString() );
		}


		/// <summary>
		/// Execute the search based on the current values of the static search properties.
		/// </summary>
		/// <returns>A DataSet containing the results of the search.</returns>
		public static DataSet DoSearch() {
			string sql = ContentDocument.GetSearchSql();
			DataSet result;
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.GetDataSet( SqlDatabase.GetTableName( typeof( ContentDocument ) ), sql.ToString(),
					db.NewParameter( "@tree_node_id", SFolderNodeID )
				);
			}
			return ( result );
		}

		public static List<ContentDocument> DoSearchList() {
			using ( SqlDatabase db = new SqlDatabase() ) {
				return ( SqlDatabase.DoSearch<ContentDocument>( ContentDocument.GetSearchSql(), db.NewParameter( "@tree_node_id", SFolderNodeID ) ) );
			}
		}

		#endregion

	}

	/// <summary>
	/// Named constants representing the various common MIME types used for the response stream.
	/// </summary>
	public struct MimeTypes {
		public const string ImageGif = "image/gif";
		public const string ImageBmp = "image/bmp";
		public const string ImageJpeg = "image/jpeg";
		public const string ImagePJpeg = "image/pjpeg";
		public const string ImagePng = "image/png";
		public const string ImageXPng = "image/x-png";
		public const string ImageEmf = "image/emf";
		public const string ImageTiff = "image/tiff";
		public const string ImageExif = "image/exif";
		public const string ImageWmf = "image/wmf";
		public const string TextHtml = "text/html";
		public const string TextScript = "text/javascript";
		public const string TextXml = "text/xml";
		public const string TextComponent = "text/x-component";
	}

}
