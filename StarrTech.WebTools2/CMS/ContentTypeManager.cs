using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.Controls;

namespace StarrTech.WebTools.CMS {
	
	/// <summary>
	/// Static class for managing the CMS content types
	/// </summary>
	public static class ContentTypeManager {
		
		#region Properties
		
		/// <summary>
		/// Access to the known content-types for this app
		/// </summary>
		public static ContentTypesList ContentTypes {
			get { 
				if ( contentTypes == null ) {
					contentTypes = new ContentTypesList();
					ContentTypeManager.LoadContentTypes();
				}
				return ( contentTypes ); 
			}
		}
		private static ContentTypesList contentTypes = null;

		/// <summary>
		/// Access to the known section-properties for this app
		/// </summary>
		public static SectionPropertiesList SectionProperties {
			get {
				if ( sectionProperties == null ) {
					sectionProperties = new SectionPropertiesList();
					LoadSectionProperties();
				}
				return ( sectionProperties );
			}
		}
		private static SectionPropertiesList sectionProperties = null;

		// LATER: Replace this with something more structured/portable (?)
		public static int CurrentUserID {
			get {
				System.Web.SessionState.HttpSessionState session = HttpContext.Current.Session;
				object id = null;
				if ( session != null ) {
					id = HttpContext.Current.Session["CurrentUserID"];
				}
				return ( id == null ? -1 : (int) id );
			}
		}

		// TODO: Replace this with something that works
		public static string GetUserName( int UserID ) {
			return ( string.Format( "User Number {0}", UserID ) );
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Load and parse the application's ContentTypesXml file
		/// </summary>
		public static void LoadContentTypes() {
			contentTypes = (ContentTypesList) ConfigurationManager.GetSection( "starrtech.cms/ContentTypes" );
		}

		/// <summary>
		/// Load and parse the application's SectionProperties xml
		/// </summary>
		public static void LoadSectionProperties() {
			sectionProperties = (SectionPropertiesList) ConfigurationManager.GetSection( "starrtech.cms/SectionProperties" );
		}

		/// <summary>
		/// Factory method to generate a new ContentItem for a given ContentType
		/// </summary>
		/// <param name="type">ContentType instance to base the new ContentItem on</param>
		/// <returns>A new persisted ContentItem</returns>
		public static ContentItem NewContentItem( ContentType type ) {
			ContentItem item = new ContentItem( type.TypeKey, CurrentUserID );
			//item.Persist();
			return ( item );
		}

		#endregion

	}

	/// <summary>
	/// List of valid content-type editor types
	/// </summary>
	public enum ContentTypeEditors {
		contentgrid,
		contentform,
		ascx,
		none
	}

	/// <summary>
	/// Config-file section handler for contentTypes
	/// </summary>
	public class ContentTypesConfigSectionHandler : IConfigurationSectionHandler {

		object IConfigurationSectionHandler.Create( object parent, object configContext, XmlNode section ) {
			ContentTypesList contentTypes = new ContentTypesList();
			XmlNodeList nodes = section.ChildNodes;
			if ( nodes.Count == 0 ) {
				throw new Exception( "No Content Types defined for this application." );
			} else {
				foreach ( XmlNode node in nodes ) {
					contentTypes.Add( new ContentType( node ) );
				}
			}
			return ( contentTypes );
		}

	}

	/// <summary>
	/// Config-file section handler for section-properties
	/// </summary>
	public class SectionPropertiesConfigSectionHandler : IConfigurationSectionHandler {

		object IConfigurationSectionHandler.Create( object parent, object configContext, XmlNode section ) {
			SectionPropertiesList sectionProperties = new SectionPropertiesList();
			foreach ( XmlNode node in section.ChildNodes ) {
				sectionProperties.Add( new SectionProperty( node ) );
			}
			return ( sectionProperties );
		}

	}

}
