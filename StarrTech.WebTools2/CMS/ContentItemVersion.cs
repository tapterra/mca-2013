using System;
using System.Data;
using System.Xml;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;

namespace StarrTech.WebTools.CMS {
	/// <summary>
	/// Contains the collection of "parts" for one "version" of a content item.
	/// Represents a record from the CMS_VERSIONS table.
	/// </summary>
	[SqlTable( "CMS_VERSIONS", SingularLabel = "Content-Item Version", PluralLabel = "Content-Item Versions", IsMarkedDeleted = true )]
	public class ContentItemVersion : BaseSqlPersistable {
		
		#region Properties
		
		#region Database Columns

		[SqlColumn( "cms_item_id", DbType.Int32 )]
		public int ContentItemID {
			get { return ( this.contentItemID ); }
			set { this.contentItemID = value; }
		}
		private int contentItemID = -1;

		[SqlColumn( "version_number", DbType.Int32 )]
		public int VersionNumber {
			get { return ( this.versionNumber ); }
			set { this.versionNumber = value; }
		}
		private int versionNumber = -1;

		[SqlColumn( "createdby_user_id", DbType.Int32 )]
		public int CreatedByUserID {
			get { return ( this.createdByUserID ); }
			set { this.createdByUserID = value; }
		}
		private int createdByUserID = -1;
		
		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDT {
			get { return ( this.createdDT ); }
			set { this.createdDT = value; }
		}
		private DateTime createdDT = DateTime.Now;

		[SqlColumn( "last_revised_dt", DbType.DateTime )]
		public DateTime LastRevisedDT {
			get { return ( this.lastRevisedDT ); }
			set { this.lastRevisedDT = value; }
		}
		private DateTime lastRevisedDT = DateTime.Now;

		[SqlColumn( "status_code", DbType.Int32 )]
		protected internal int DBStatus {
			get { return ( (int) this.Status ); }
			set { this.Status = (ContentVersionStatusLevels) value; }
		}
		
		// TODO: Since we aren't using versioning yet, all versions are "active"
		// the hack below enforces this, ignoring what may be in the database
		public ContentVersionStatusLevels Status {
			get { return ( this.status ); }
			set { } //this.status = value; }
		}
		private ContentVersionStatusLevels status = ContentVersionStatusLevels.Active;

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";
		
		#endregion
		
		#region Parts
		
		/// <summary>
		/// Collection of ContentItemPart instances for this version
		/// </summary>
		public ContentItemPartsList Parts {
			get { 
				if ( this.parts == null ) {
					this.parts = new ContentItemPartsList( this );
				}
				return ( this.parts ); 
			}
		}
		private ContentItemPartsList parts = null;
		
		/// <summary>
		/// Returns the part-number of the last part (the largest part-number in use)
		/// </summary>
		public int MostRecentPartNo {
			get { return ( this.Parts.MostRecentPartNo ); }
		}
		
		#endregion
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Instantiate a new, empty version
		/// </summary>
		protected internal ContentItemVersion() {
			this.CreatedByUserID = ContentTypeManager.CurrentUserID;
			this.CreatedDT = DateTime.Now;
			this.LastRevisedDT = DateTime.Now;
			this.Status = ContentVersionStatusLevels.DraftRevision;
			this.VersionNumber = 1;
			// TODO: Create the list of new Parts based on the item's content-type
			
		}
		
		/// <summary>
		/// Instantiate a new version that's a content-specific copy of an existing version
		/// </summary>
		/// <param name="VersToCopy">The existing version instance to copy</param>
		protected internal ContentItemVersion( ContentItemVersion VersToCopy ) : this() {
			this.ID = -1;
			this.ContentItemID = VersToCopy.ContentItemID;
			this.VersionNumber = -1;
			this.CreatedDT = DateTime.Now;
			this.LastRevisedDT = DateTime.Now;
			this.Status = ContentVersionStatusLevels.DraftRevision;
			//this.Comments = VersToCopy.Comments;
			int count = VersToCopy.MostRecentPartNo;
			for ( int p = 1 ; p <= count ; p++ ) {
				this.AddPart( new ContentItemPart( VersToCopy.Parts[ p ] ) );
			}
		}
		
		/// <summary>
		/// Restore a version from the database by ID
		/// </summary>
		/// <param name="ID"></param>
		public ContentItemVersion( int ID ) : base( ID ) {
			// Load the parts for this version
			this.RestoreParts();
		}
		
		/// <summary>
		/// Given an item ID and version-number, restore the version from the database
		/// </summary>
		/// <param name="ItemID">Record ID for the ContentItem that owns this version</param>
		/// <param name="VersionNo">Version number for the item's version that we want to instantiate</param>
		public ContentItemVersion( int itemID, int versionNumber ) : base() {
			string sql = "select * from CMS_VERSIONS where cms_item_id = @cms_item_id and version_namber = @version_namber ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql,
					db.NewParameter( "@cms_item_id", itemID ),
					db.NewParameter( "@version_namber", versionNumber )
				);
			}
		}
		
		#endregion
		
		#region Methods
		
		#region SqlPersistable overrides
		
		/// <summary>
		/// Persist this instance to the database
		/// </summary>
		public override bool Persist() {
			StarrTech.WebTools.CMS.ContentPublisher.ResetContentItemCache( this.ContentItemID );
			if ( base.Persist() ) {
				this.PersistParts();
				return ( true );
			}
			return ( false );
		}
		
		/// <summary>
		/// Load this instance from the database
		/// </summary>
		public override void Restore() {
			base.Restore();
			this.RestoreParts();
		}
		
		/// <summary>
		/// We never want to delete a ContentItemVersion (?)
		/// </summary>
		/// <returns>Never happens</returns>
		public override bool Delete() {
			throw new InvalidOperationException( "Can't delete a ContentItemVersion!" );
		}
		
		#endregion
		
		#region Parts operations
		
		/// <summary>
		/// Persist this instance's parts
		/// </summary>
		private void PersistParts() {
			foreach ( ContentItemPart part in this.Parts ) {
				part.ContentVersionID = this.ID;
				part.Persist();
			}
		}
		
		/// <summary>
		/// Load the Parts collection for this instance from the database
		/// </summary>
		public void RestoreParts() {
			string sql = "select * from CMS_PARTS where cms_version_id = @cms_version_id order by part_number";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@cms_version_id", this.ID ) ) ) {
					ContentItemPart part = new ContentItemPart();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, part ) ) {
						this.AddPart( part );
						part = new ContentItemPart();
					}
				}
			}
		}
		
		/// <summary>
		/// Append a new ContentItemPart instance to the end of the list of parts.
		/// </summary>
		/// <param name="part">The new part to append.</param>
		public void AddPart( ContentItemPart part ) {
			part.ContentVersionID = this.ID;
			if ( part.PartNo <= 0 ) {
				part.PartNo = this.Parts.NextPartNo;
			}
			this.Parts.Add( part );
			//part.Persist();
		}
		
		/// <summary>
		/// Insert a new ContentItemPart instance before an existing part in the Parts list
		/// </summary>
		/// <param name="newPart">The new part to insert.</param>
		/// <param name="insertBeforePart">The existing part before which the new part should be inserted.</param>
		//public void InsertPart( ContentItemPart newPart, ContentItemPart insertBeforePart ) {
		//  newPart.PartNo = insertBeforePart.PartNo;
		//  int count = this.Parts.Count;
		//  for ( int p = newPart.PartNo ; p <= count ; p++ ) {
		//    ContentItemPart part = this.Parts[p];
		//    part.PartNo++;
		//    part.Persist();
		//  }
		//  this.AddPart( newPart );
		//}
		
		#endregion
		
		#region XML Rendering
		
		/// <summary>
		/// Returns this version (its parts) as an XML string
		/// </summary>
		/// <returns>An XML document as a string</returns>
		public string ToXml() {
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			XmlTextWriter writer = new XmlTextWriter( stream, System.Text.Encoding.Default );
			this.WriteXml( writer );
			stream.Position = 0;
			System.IO.StreamReader reader = new System.IO.StreamReader( stream );
			string xml = reader.ReadToEnd();
			writer.Close();
			stream.Close();
			return ( xml );
		}
		
		/// <summary>
		/// Generates this versions's XML to a writer (for the ToXml method).
		/// </summary>
		/// <param name="writer">an XmlTextWriter for catching the XML.</param>
		protected internal void WriteXml( XmlTextWriter writer ) {
			writer.WriteStartElement( "parts");
			// Write the parts
			foreach ( ContentItemPart part in this.Parts ) {
				part.WriteXml( writer );
			}
			writer.WriteEndElement(); // parts
		}
		
		#endregion
		
		#endregion
		
	}
	
}
