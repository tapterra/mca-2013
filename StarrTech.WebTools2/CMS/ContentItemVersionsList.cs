using System;
using System.Collections.ObjectModel;


namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Custom collection class for handling the list of ContentItemVersions in a ContentItem
	/// </summary>
	// Assumptions:
	//	[] We want to key on version-number (not ID) so that we can iterate thru the versions in order, find the latest, etc.
	//	[] Once a version is created/persisted, we will never delete it (only change it's status)
	//	[] Version numbers will always be in sequence from 1 to N with N being the most-recent
	//	[] The version-number for the most recent version will always be this.Count.
	//	[] The next appropriate version-number for a new version will always be this.Count + 1
	//TODO: Reimplement as a generic collection

	public class ContentItemVersionsList : KeyedCollection<int, ContentItemVersion> {
		
		#region Properties
		
		/// <summary>
		/// The ContentItem this list is holding versions for.
		/// </summary>
		public ContentItem Owner {
			get { return ( this.owner ); }
		}
		private ContentItem owner = null;
		
		/// <summary>
		/// Returns the version-number of the most-recently created version in the list
		/// </summary>
		public int MostRecentVersionNumber {
			// We won't be deleting individual versions, so ...
			get { return ( this.Count ); }
		}
		
		/// <summary>
		/// Returns the version-number to be assigned to a new (not yet added) version
		/// </summary>
		public int NextVersionNumber {
			get { return ( this.Count + 1 ); }
		}
		
		/// <summary>
		/// Return a reference to this item's "Active" version (null if no active version exists)
		/// "Active" means the version that is currently published on the website (status = Approved, there can be at most one)
		/// </summary>
		public ContentItemVersion CurrentActiveVersion {
			get {  
				int lastVersionNo = this.MostRecentVersionNumber;
				for ( int i = lastVersionNo ; i > 0 ; i-- ) {
					ContentItemVersion vers = this[i];
					if ( vers.Status == ContentVersionStatusLevels.Active ) {
						return ( vers );
					} else if ( vers.Status == ContentVersionStatusLevels.Archived ) {
						break; // We're beyond the possibility of finding an Active version
					}
				}
				return ( null ); // no active-version found - return null
			}
		}
		
		/// <summary>
		/// Return a reference to this item's "Current Revision" version (null if no current-revision version exists)
		/// The "Current Revision" version is the one in Draft/Revision/Awaiting Approval status - only one should exist
		/// This is the version that is editable in the Admin GUI.
		/// </summary>
		public ContentItemVersion CurrentRevisionVersion {
			get {  
				// If a "revision" version exists, it will always be the most recent version...
				ContentItemVersion vers = this[this.MostRecentVersionNumber];				
				if ( vers.Status == ContentVersionStatusLevels.DraftRevision || vers.Status == ContentVersionStatusLevels.AwaitingApproval ) {
					return ( vers );
				}
				return ( null ); // no revision-version found - return null
			}
		}
		
		#endregion
		
		#region Constructor
		
		public ContentItemVersionsList( ContentItem owner ) : base() {
			this.owner = owner;
		}
		
		#endregion

		protected override int GetKeyForItem( ContentItemVersion vers ) {
			return ( vers.VersionNumber );
		}
		
	}
	
}
