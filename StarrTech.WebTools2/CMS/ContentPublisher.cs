using System;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Xml;

using StarrTech.WebTools;

namespace StarrTech.WebTools.CMS {
	/// <summary>
	/// Static methods for accessing CMS content, tree-structure, etc.
	/// </summary>
	public static class ContentPublisher {

		/// <summary>
		/// Returns the preferences-based key string for the site's CMS tree
		/// </summary>
		public static string MainNavTreeKey {
			get { return SitePrefs.CheckPref( "BASE_SITE_TREE_KEY", "site" ); }
		}
		public static string AlternateNavTreeKey {
			get { return SitePrefs.CheckPref( "ALT_TREE_KEY", "alt" ); }
		}

		public const string TreeQKey = "tree";
		public const string NodeQKey = "nid";
		public const string CidQKey = "cid";

		/// <summary>
		/// Returms an XmlDocument containing the site' CMS tree
		/// </summary>
		public static XmlDocument SiteTreeXmlDoc() {
			return ( SiteTreeXmlDoc( ContentPublisher.MainNavTreeKey ) );
		}

		public static XmlDocument SiteTreeXmlDoc( string SiteTreeKey ) {
			// TODO: Use the cached XmlDocument created from the CMS Site Tree
			XmlDocument doc = HttpContext.Current.Cache[SiteTreeKey] as XmlDocument;
			if ( doc == null ) {
				// Need to (re)load the cache
				Tree siteTree = new Tree( SiteTreeKey );
				siteTree.Restore();
				doc = new XmlDocument();
				string treeXml = siteTree.ToXml( true );
				doc.Load( new StringReader( treeXml ) );
				// Insert the doc into the cache, with a trigger to expire every hour
				HttpContext.Current.Cache.Insert(
					SiteTreeKey, doc, null, DateTime.Now.AddHours( 1 ), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null
				);
			}
			return ( doc );
		}

		/// <summary>
		/// Given a tree path for a node in the site tree, return the node.
		/// </summary>
		/// <param name="treeKey">Key for the appropriate site tree.</param>
		/// <param name="treePath">A tree path ("foo/bar") for an existing node in the tree.</param>
		/// <returns>The XmlNode, or null if the path is invalid.</returns>
		public static XmlNode TreePathXmlNode( string treeKey, string treePath ) {
			string[] pathkeys = treePath.Split( '/' );
			XmlNode node = SiteTreeXmlDoc( treeKey );
			foreach ( string pathkey in pathkeys ) {
				if ( string.IsNullOrEmpty( pathkey ) ) {
					continue;
				}
				node = node.SelectSingleNode( string.Format( "//treenode[@key='{0}']", pathkey ) );
				if ( node == null ) {
					return ( null ); // no match
				}
			}
			// If we made it here, we've got a matching node for this full path
			return ( node );
		}

		/// <summary>
		/// Given a tree path for a node in the site tree, return the node ID.
		/// </summary>
		/// <param name="treeKey">Key for the appropriate site tree.</param>
		/// <param name="treePath">A tree path ("foo/bar") for an existing node in the tree.</param>
		/// <returns>The TreeNode.ID value for the node, or -1 if the path is invalid.</returns>
		public static int TreePathNodeID( string treeKey, string treePath ) {
			string val = ContentPublisher.TreePathNodeAttribute( treeKey, treePath, "id" );
			return ( Convert.ToInt32( val ) );
		}

		public static ContentType TreePathNodeContentType( string treeKey, string treePath ) {
			string typekey = ContentPublisher.TreePathNodeAttribute( treeKey, treePath, "typekey" );
			return ( ContentTypeManager.ContentTypes[typekey] );
		}

		public static string TreePathNodeAttribute( string treeKey, string treePath, string attribute ) {
			XmlNode node = TreePathXmlNode( treeKey, treePath );
			if ( node == null ) {
				return ( "" );
			}
			XmlAttributeCollection attribs = node.Attributes;
			if ( attribs.GetNamedItem( attribute ) == null ) {
				return ( "" );
			}
			return ( ( (XmlAttribute) attribs.GetNamedItem( attribute ) ).Value );
		}

		/// <summary>
		/// Given a tree path for a node in the site tree, return the ID for the default (rank=0) ContentItem attached to it.
		/// </summary>
		/// <param name="treeKey">Key for the appropriate site tree.</param>
		/// <param name="treePath">A tree path ("foo/bar") for an existing node in the tree.</param>
		/// <returns>The ContentItem.ID value for the node's content, or -1 if the path is invalid or an item is not found.</returns>
		public static int TreePathItemID( string treeKey, string treePath ) {
			XmlNode node = TreePathXmlNode( treeKey, treePath );
			if ( node == null ) {
				return ( -1 );
			}
			// If we made it here, we've got a matching node for this full path
			XmlAttributeCollection attribs = node.Attributes;
			// Make sure the node is marked as "visible"
			bool isVisible = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "isvisible" ) ).Value );
			if ( !isVisible ) {
				return ( -1 ); // not visible content
			}
			string cid = ( (XmlAttribute) attribs.GetNamedItem( "contentitemid" ) ).Value;
			return ( Convert.ToInt32( cid ) );
		}

		public static string TreePathInheritedAttribute( string treePath, string attribName ) {
			if ( treePath.StartsWith( "/" ) ) {
				treePath = treePath.Remove( 0, 1 );
			}
			string[] pathkeys = treePath.Split( '/' );
			string treeKey = pathkeys[0];
			if ( treeKey == treePath ) {
				return ( "" );
			}
			pathkeys = treePath.Remove( 0, treeKey.Length + 1 ).Split( '/' );
			XmlNode node = SiteTreeXmlDoc( treeKey );
			foreach ( string pathkey in pathkeys ) {
				if ( string.IsNullOrEmpty( pathkey ) ) {
					continue;
				}
				node = node.SelectSingleNode( string.Format( "//treenode[@key='{0}']", pathkey ) );
				if ( node == null ) {
					return ( null ); // no match
				}
			}
			// If we made it here, we've got a matching node for this full path
			return NodeInheritedAttribute( ref node, attribName );
		}

		public static string NodeInheritedAttribute( ref XmlNode node, string attribName ) {
			XmlAttribute attr = (XmlAttribute) node.Attributes.GetNamedItem( attribName );
			while ( attr == null ) {
				node = node.ParentNode;
				if ( node == null ) {
					return ( null );
				}
				attr = (XmlAttribute) node.Attributes.GetNamedItem( attribName );
			}
			// If we made it here, we've got a non-null attribute
			while ( attr.Value == "" || attr.Value == "auto" ) {
				node = node.ParentNode;
				if ( node == null || node.Name == "tree" ) {
					break;
				}
				attr = (XmlAttribute) node.Attributes.GetNamedItem( attribName );
			}
			return ( attr.Value );
		}

		/// <summary>
		/// Find the ContentItemID for a given publisher at a given path on a given tree
		/// </summary>
		/// <param name="treeKey">Key for the appropriate site tree.</param>
		/// <param name="treePath">A tree path ("foo/bar") for an existing node in the tree.</param>
		/// <param name="publisher">The "publisher" attribute for the specific content-type.</param>
		/// <returns>The ContentItem.ID value for the node's content, or -1 if the path is invalid or an item is not found.</returns>
		public static int PublishedAtNodeID( string treeKey, string publisher ) {
			XmlNode node = SiteTreeXmlDoc( treeKey );
			node = node.SelectSingleNode( string.Format( "//treenode[@publishedat='{0}']", publisher ) );
			if ( node == null ) {
				return ( -1 ); // no match
			}
			// If we made it here, we've got a matching node for this full path
			XmlAttributeCollection attribs = node.Attributes;
			string id = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
			return ( Convert.ToInt32( id ) );
		}

		/// <summary>
		/// Find the first node with a key matching the given pathKey, in the given tree, and return it's node ID
		/// </summary>
		public static int PathKeyTreeNodeID( string pathKey, string treeKey ) {
			XmlNode node = SiteTreeXmlDoc( treeKey );
			node = node.SelectSingleNode( string.Format( "//treenode[@key='{0}']", pathKey ) );
			if ( node == null ) {
				return ( -1 ); // no match
			}
			// Make sure the node is marked as "visible"
			XmlAttributeCollection attribs = node.Attributes;
			bool isVisible = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "isvisible" ) ).Value );
			if ( !isVisible ) {
				return ( -1 ); // not visible content
			}
			// If we made it here, we've got a matching node for this pathKey
			string id = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
			return ( Convert.ToInt32( id ) );
		}

		/// <summary>
		/// Given a TreeNodeID, return the node's tree-path (for use in a local URL).
		/// </summary>
		/// <param name="ContentItemID">ID for the specific tree-node.</param>
		/// <returns>The given node's treepath.</returns>
		public static string TreeNodePath( int TreeNodeID, string TreeKey ) {
			string path = "";
			// Find the referenced node in the tree's XmlDocument
			XmlNode node = GetNodeByID( TreeNodeID, TreeKey );
			while ( node != null && node.NodeType == XmlNodeType.Element ) {
				if ( node.Name == "tree" ) {
					break;
				}
				XmlAttributeCollection attribs = node.Attributes;
				string typeKey = ( (XmlAttribute) attribs.GetNamedItem( "typekey" ) ).Value;
				if ( typeKey == "homepage" ) {
					break;
				}
				string pathkey = ( (XmlAttribute) attribs.GetNamedItem( "key" ) ).Value;
				path = "/" + pathkey + path;
				if ( pathkey == TreeKey ) {
					break;
				}
				node = node.ParentNode;
			}
			return ( path );
		}

		public static XmlNode GetNodeByID( int TreeNodeID, string TreeKey ) {
			// Find the referenced node in the tree's XmlDocument
			XmlNode node = ContentPublisher.SiteTreeXmlDoc( TreeKey ).DocumentElement.SelectSingleNode(
				string.Format( @"//treenode[@id='{0}']", TreeNodeID )
			);
			return ( node );
		}

		/// <summary>
		/// Given a ContentItem ID, return the key used to cache the item.
		/// </summary>
		/// <param name="ContentItemID">ID of the ContentItem we want to publish/cache</param>
		/// <returns>The key used to reference the item in HttpContext.Current.Cache</returns>
		public static string ContentItemCacheKey( int ContentItemID ) {
			return ( string.Format( "ContentItem{0}", ContentItemID ) );
		}

		/// <summary>
		/// Given a ContentItem ID, load the ContentItem and return it's XML representation.
		/// </summary>
		/// <param name="ContentItemID">ID of the ContentItem we want to publish.</param>
		/// <returns>The XML for the active version of the specific ContentItem.</returns>
		public static XmlDocument GetContentXml( int ContentItemID ) {
			// Keep the items cached if possible...
			if ( ContentItemID <= 0 ) {
				return ( null );
			}
			string cachekey = ContentItemCacheKey( ContentItemID );
			XmlDocument doc = HttpContext.Current.Cache[cachekey] as XmlDocument;
			if ( doc == null ) {
				ContentItem item = new ContentItem( ContentItemID );
				doc = new XmlDocument();
				doc.Load( new StringReader( item.ToXml() ) );
				HttpContext.Current.Cache.Insert(
					cachekey, doc, null, DateTime.Now.AddHours( 1 ), TimeSpan.Zero, CacheItemPriority.AboveNormal, null
				);
			}
			return ( doc );
		}

		/// <summary>
		/// Given a ContentItemID and a content-part name, return the content
		/// </summary>
		public static string GetContentPartText( int ContentItemID, string PartName ) {
			string result = "";
			if ( ContentItemID > 0 ) {
				XmlDocument contentDoc = ContentPublisher.GetContentXml( ContentItemID );
				if ( contentDoc != null ) {
					XmlNode bodyNode = contentDoc.SelectSingleNode( string.Format( "/contentitem/parts/part[@name='{0}']/content", PartName ) );
					if ( bodyNode != null ) {
						result = bodyNode.InnerText;
					}
				}
			}
			return ( result );
		}

		public static string GetContentItemTitle( int ContentItemID ) {
			string result = "";
			if ( ContentItemID > 0 ) {
				XmlDocument contentDoc = ContentPublisher.GetContentXml( ContentItemID );
				if ( contentDoc != null ) {
					result = ( (XmlAttribute) contentDoc.FirstChild.Attributes.GetNamedItem( "title" ) ).Value;
				}
			}
			return ( result );
		}

		public static string GetNodeTitle( int TreeNodeID, string TreeKey ) {
			string result = "";
			if ( TreeNodeID > 0 ) {
				XmlNode node = ContentPublisher.GetNodeByID( TreeNodeID, TreeKey );
				if ( node != null ) {
					result = ( (XmlAttribute) node.Attributes.GetNamedItem( "title" ) ).Value;
				}
			}
			return ( result );
		}

		public static string GetRootNodeTitle( int TreeNodeID, string TreeKey ) {
			string result = "";
			XmlNode node = null;
			int parentID = TreeNodeID;
			while ( parentID > 0 ) {
				node = ContentPublisher.GetNodeByID( parentID, TreeKey );
				parentID = Convert.ToInt32( ( (XmlAttribute) node.Attributes.GetNamedItem( "parentid" ) ).Value );
			}
			if ( node != null ) {
				result = ( (XmlAttribute) node.Attributes.GetNamedItem( "title" ) ).Value;
			}
			return ( result );
		}

		public static string GetRootNodeTitle( string ContentTypeKey, string TreeKey ) {
			string result = "";
			XmlNode node = null;
			int parentID = ContentPublisher.GetContentTypeNodeID( TreeKey, ContentTypeKey );
			while ( parentID > 0 ) {
				node = ContentPublisher.GetNodeByID( parentID, TreeKey );
				parentID = Convert.ToInt32( ( (XmlAttribute) node.Attributes.GetNamedItem( "parentid" ) ).Value );
			}
			if ( node != null ) {
				result = ( (XmlAttribute) node.Attributes.GetNamedItem( "title" ) ).Value;
			}
			return ( result );
		}

		/// <summary>
		/// Given a tree-key, clear the tree's XML from the site cache.
		/// </summary>
		/// <param name="SiteTreeKey">Key for the tree to reset.</param>
		public static void ResetSiteTreeCache( string SiteTreeKey ) {
			HttpContext.Current.Cache.Remove( SiteTreeKey );
		}

		/// <summary>
		/// Given a content-item id, clear the content item from HttpContext.Current.Cache
		/// </summary>
		/// <param name="ContentItemID">ID of the ContentItem we are updating.</param>
		public static void ResetContentItemCache( int ContentItemID ) {
			string cachekey = ContentItemCacheKey( ContentItemID );
			HttpContext.Current.Cache.Remove( cachekey );
		}

		public static XmlNode GetContentTypeNode( string SiteTreeKey, string ContentTypeKey ) {
			return ( SiteTreeXmlDoc( SiteTreeKey ).SelectSingleNode( string.Format( "//treenode[@typekey='{0}']", ContentTypeKey ) ) );
		}

		public static int GetContentTypeContentItemID( string SiteTreeKey, string ContentTypeKey ) {
			XmlNode node = GetContentTypeNode( SiteTreeKey, ContentTypeKey );
			if ( node != null ) {
				string cid = ( (XmlAttribute) node.Attributes.GetNamedItem( "contentitemid" ) ).Value;
				return ( Convert.ToInt32( cid ) );
			}
			return ( -1 );  // No joy
		}

		public static int GetContentTypeNodeID( string SiteTreeKey, string ContentTypeKey ) {
			XmlNode node = ContentPublisher.GetContentTypeNode( SiteTreeKey, ContentTypeKey );
			if ( node != null ) {
				return ( Convert.ToInt32( ( (XmlAttribute) node.Attributes.GetNamedItem( "id" ) ).Value ) );
			}
			return ( -1 );  // No joy
		}

		/// <summary>
		/// For a given treekey, find the ID of the first TreeNode linked to the given ContentItem 
		/// </summary>
		public static int GetContentItemFirstNode( string TreeKey, int ContentItemID ) {
			XmlNode node = SiteTreeXmlDoc( TreeKey );
			node = node.SelectSingleNode( string.Format( "//treenode/items/contentitem[@id='{0}']", ContentItemID ) );
			if ( node == null ) {
				return ( -1 ); // no match
			}
			while ( node.Name.ToLower() != "treenode" ) {
				node = node.ParentNode;
			}
			XmlAttributeCollection attribs = node.Attributes;
			string id = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
			return ( Convert.ToInt32( id ) );
		}

	}

}
