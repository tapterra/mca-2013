using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.UI;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;
using System.Collections.Generic;
using System.Xml;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// ISqlPersistable implementation for CMS_TREE_NODES.
	/// </summary>
	[SqlTable( "CMS_TREE_NODES", SingularLabel = "Tree Node", PluralLabel = "Tree Nodes" )]
	public class TreeNode : BaseTreeNode {

		#region Properties

		#region Database Columns

		/// <summary>
		/// Record ID for the CMS_TREES record that owns this node.
		/// </summary>
		[SqlColumn( "tree_id", DbType.Int32 )]
		public override int TreeID {
			get { return ( this.treeID ); }
			set { this.treeID = value; }
		}
		private int treeID = -1;

		/// <summary>
		/// Record ID for the CMS_TREE_NODES record that is the "parent" of this node.
		/// </summary>
		[SqlColumn( "parent_node_id", DbType.Int32 )]
		public int ParentID {
			get {
				if ( this.ParentNode == null )
					return ( 0 );
				if ( this.ParentNode is TreeNode )
					return ( this.ParentNode.ID );
				return ( 0 );
			}
			set { }
		}

		/// <summary>
		/// User-defined key for this node
		/// </summary>
		[SqlColumn( "path_key", DbType.String, Length = 50 )]
		public string PathKey {
			get { return ( this.pathKey ); }
			set { this.pathKey = value; }
		}
		private string pathKey = "";

		/// <summary>
		/// User-defined title for this node
		/// </summary>
		[SqlColumn( "title", DbType.String, Length = 200 )]
		public string Title {
			get { return ( this.title ); }
			set { this.title = value; }
		}
		private string title = "";

		/// <summary>
		/// Sequential order for this node relative to it's siblings.
		/// </summary>
		[SqlColumn( "next_node_id", DbType.Int32 )]
		public int NextNodeID {
			get { return ( this.nextNodeID ); }
			set { this.nextNodeID = value; }
		}
		private int nextNodeID = -1;

		/// <summary>
		/// Administrative notes for this node.
		/// </summary>
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		/// <summary>
		/// Switch to determine if this node should be visible when the tree is published.
		/// </summary>
		[SqlColumn( "is_visible", DbType.Boolean )]
		public bool IsVisible {
			get { return ( this.isVisible ); }
			set { this.isVisible = value; }
		}
		private bool isVisible = true;

		/// <summary>
		/// Content-Type key for this node
		/// </summary>
		[SqlColumn( "type_key", DbType.String, Length = 50 )]
		public string TypeKey {
			get { return ( this.typeKey ); }
			set { this.typeKey = value; }
		}
		private string typeKey = "";

		/// <summary>
		/// Header-image key
		/// </summary>
		[SqlColumn( "header_image", DbType.String, Length = 250 )]
		public string HeaderImage {
			get { return ( this.headerImage ); }
			set { this.headerImage = value; }
		}
		private string headerImage = "";

		/// <summary>
		/// Corresponding section-ID value for records ported over from the ColdFusion CMS database
		/// </summary>
		[SqlColumn( "cf_section_id", DbType.Int32 )]
		public int CFSectionID {
			get { return ( this.cfSectionID ); }
			set { this.cfSectionID = value; }
		}
		private int cfSectionID = -1;

		/// <summary>
		/// Switch to determine if this node should be added to the menu as a menu item.
		/// </summary>
		[SqlColumn( "is_menuitem", DbType.Boolean )]
		public override bool IncludeAsMenuItem {
			get { return ( this.includeAsMenuItem ); }
			set { this.includeAsMenuItem = value; }
		}
		private bool includeAsMenuItem = false;

		/// <summary>
		/// Switch to determine if the children of this node should be added to the menu as a sub/popup menu.
		/// </summary>
		[SqlColumn( "is_subnode_menuitems", DbType.Boolean )]
		public override bool IncludeSubnodeMenuItems {
			get {
				// If ParentID == 0, this is a top-level node and it's subnodes must always appear on the main menu
				// return ( this.ParentID == 0 && this.includeAsMenuItem ? true : this.includeSubnodeMenuItems );
				return ( this.includeSubnodeMenuItems );
			}
			set { this.includeSubnodeMenuItems = value; }
		}
		private bool includeSubnodeMenuItems = false;

		#endregion

		#region UI State

		/// <summary>
		/// Switch to signify if the node's children are visible in the tree-editor UI.
		/// </summary>
		public bool IsExpanded {
			get { return ( this.isExpanded ); }
			set { this.isExpanded = value; }
		}
		private bool isExpanded = false;

		#endregion

		#region Content-Type Properties

		/// <summary>
		/// ContentType instance representing the valid content-type assigned to this node
		/// </summary>
		public ContentType ContentType {
			get {
				if ( string.IsNullOrEmpty( this.TypeKey ) )
					return ( null );
				//throw new ArgumentException( "Can't ask for a node's ContentType before it's TypeKey is assigned." );
				return ( ContentTypeManager.ContentTypes[this.TypeKey] );
			}
		}

		/// <summary>
		/// Switch to determine if this node can be deleted.
		/// </summary>
		public bool CanDelete {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanDelete );
				return ( true );
			}
		}

		/// <summary>
		/// Switch to determine if this node can be edited.
		/// </summary>
		public bool CanEdit {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanEdit );
				return ( true );
			}
		}

		/// <summary>
		/// Switch to determine if child-nodes can be added to this node.
		/// </summary>
		public bool CanAddChildren {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanAddChildren );
				return ( true );
			}
		}

		/// <summary>
		/// Switch to determine if this node can be moved up or down in the tree.
		/// </summary>
		public bool CanMoveV {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanMoveV );
				return ( true );
			}
		}

		/// <summary>
		/// Switch to determine if this node can be moved left or right in the tree.
		/// </summary>
		public bool CanMoveH {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanMoveH );
				return ( true );
			}
		}

		/// <summary>
		/// Switch to determine if this node-type is restricted to the root-level of the tree (like the Home Page)
		/// </summary>
		public bool MustBeRoot {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.MustBeRoot );
				return ( false );
			}
		}

		/// <summary>
		/// Switch to determine if there can be only one node in the tree with this assigned type (like the Home Page)
		/// </summary>
		public bool CanBeOnlyOne {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.CanBeOnlyOne );
				return ( false );
			}
		}

		/// <summary>
		/// For content-types with fixed publishers, returns the URL to the publisher
		/// </summary>
		public string PublishedAt {
			get {
				if ( this.ContentType != null )
					return ( this.ContentType.PublishedAt );
				return ( "" );
			}
		}

		#endregion

		#region Content-Item Properties

		/// <summary>
		/// Holds the list of ContentItems linked to this node
		/// </summary>
		public List<ContentItem> ContentItems {
			get {
				if ( this.contentItems == null ) {
					LoadContentItems();
				}
				return ( this.contentItems );
			}
		}
		private List<ContentItem> contentItems;

		private ContentItem DefaultItem {
			get {
				// Make sure this node's been persisted, and has a TypeKey before allowing this to be used
				if ( this.ID <= 0 ) {
					throw new ArgumentNullException( "TreeNode.ID", "Can't access the ContentItem for an unpersisted TreeNode." );
				}
				if ( string.IsNullOrEmpty( this.TypeKey ) ) { return ( null ); }
				if ( this.ContentItems.Count == 0 ) { return ( null ); }
				int index = 0;
                // Find active content item. Ignore approval flag
                while ( index < this.ContentItems.Count && !this.ContentItems[index].IsActive ) {
					index++;
				}
				if ( index >= this.ContentItems.Count ) { return ( null ); }
				return ( this.ContentItems[index] );
			}
		}

		/// <summary>
		/// Returns the "main/default" ContentItem for this TreeNode - if none exists yet, create one
		/// </summary>
		public ContentItem DefaultContentItem {
			get {
				// Do we have a default content-item yet?
				ContentItem item = this.DefaultItem;
				// ...if not, create a new one, persist and return it.
				if ( item == null ) {
					item = this.NewContentItem( 0 ); 
				}
				return ( item );
			}
		}

		/// <summary>
		/// Returns the ID number for the default (section-home) ContentItem linked to this node, or -1 if none
		/// </summary>
		public int DefaultContentItemID {
			get {
				ContentItem item = this.DefaultItem;
				return ( item == null ? -1 : item.ID );
			}
		}

		/// <summary>
		/// Returns the highest "rank" value currently used by CMS_NODE_ITEMS related to this node.
		/// </summary>
		public int HighestItemRank {
			get {
				this.CleanupItemRanks();
				int result = -1;
				using ( SqlDatabase db = new SqlDatabase() ) {
					string sql = "select top 1 rank from CMS_NODE_ITEMS where ( tree_node_id = @tree_node_id ) order by rank desc";
					object rank = db.ExecuteScalar( sql, db.NewParameter( "@tree_node_id", this.ID ) );
					if ( rank != null ) {
						result = (int) rank;
					}
				}
				return ( result );
			}
		}

		#endregion

		#endregion

		#region Constructors

		public TreeNode() : base() {}

		public TreeNode( int ID ) : base( ID ) {}

		public TreeNode( int treeID, int parentID, int nextNodeID, string pathKey, string title, bool isVisible, string comments, string typeKey )
			: base() {
			this.TreeID = treeID;
			//this.ParentID = parentID;
			this.NextNodeID = nextNodeID;
			this.PathKey = pathKey;
			this.Title = title;
			this.IsVisible = isVisible;
			this.Comments = comments;
			this.IsExpanded = true;
			this.TypeKey = typeKey;
		}

		#endregion

		#region Public Methods

		#region ContentItem Mgmt

		/// <summary>
		/// Loads the ContentItems member collection with the ContentItems linked to this node
		/// </summary>
		public void LoadContentItems() {
			this.contentItems = new List<ContentItem>();
			if ( this.ID > 0 ) {
				// Renumber the ranks...
				this.CleanupItemRanks();
				string qry =
					@"select i.* from CMS_NODE_ITEMS as ni left outer join CMS_ITEMS as i on i.id = ni.cms_item_id 
								where (i.is_deleted = 0) and (ni.tree_node_id = @tree_node_id) order by ni.rank"
				;
				using ( SqlDatabase db = new SqlDatabase() ) {
					using ( IDataReader rdr = db.SelectRecords( qry, db.NewParameter( "@tree_node_id", this.ID ) ) ) {
						ContentItem item = new ContentItem();
						while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
							this.contentItems.Add( item );
							item = new ContentItem();
						}
					}
				}
			}
		}

		/// <summary>
		/// Given an existing ContentItem, link it to this node at a specified rank (sequence).
		/// Deletes any previous link, and replaces the item if rank = 0.
		/// </summary>
		/// <param name="Item">The item to link.</param>
		/// <param name="Rank">Rank/sequence for the item link.</param>
		public void LinkContentItem( ContentItem Item, int Rank ) {
			// Make sure the item has been persisted...
			if ( Item.ID <= 0 ) {
				Item.Persist();
			}
			string sql = "";
			// Make sure this item isn't already linked to this node...
			sql = "delete CMS_NODE_ITEMS where tree_node_id = @tree_node_id and cms_item_id = @cms_item_id; ";
			if ( Rank > 0 ) {
				// Make a "hole" in the existing rank sequence...
				sql += "update CMS_NODE_ITEMS set rank = rank + 1 where tree_node_id = @tree_node_id and rank >= @rank; ";
			} else {
				// We're being asked for a new default-page for this node (?) so delete any existing one...
				sql += "delete CMS_NODE_ITEMS where tree_node_id = @tree_node_id and rank = 0; ";
			}
			// Create the CMS_NODE_ITEMS record linking the new item to this node
			sql += "insert into CMS_NODE_ITEMS ( tree_node_id, cms_item_id, page_name, title, description, rank ) VALUES ( @tree_node_id, @cms_item_id, @page_name, @title, @description, @rank )";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@tree_node_id", this.ID ),
					db.NewParameter( "@cms_item_id", Item.ID ),
					db.NewParameter( "@page_name", this.PathKey ),
					db.NewParameter( "@title", this.Title ),
					db.NewParameter( "@description", "" ),
					db.NewParameter( "@rank", Rank )
				);
			}
		}

		/// <summary>
		/// Generates a new ContentItem of the appropriate type for this node, 
		/// and creates a link for it at the specified rank (sequence).
		/// </summary>
		/// <param name="Rank">Rank/sequence for the new item link.</param>
		/// <returns>The new ContentItem instance.</returns>
		public ContentItem NewContentItem( int Rank ) {
			// No CMS_NODE_ITEMS record found (so no linked ContentItem) - create a new one
			ContentType type = this.TypeKey == "contentcatalog" ? ContentTypeManager.ContentTypes["contentitem"] : this.ContentType;
			ContentItem item = ContentTypeManager.NewContentItem( type );
			item.Rank = Rank;
			this.LinkContentItem( item, Rank );
			return ( item );
		}

		/// <summary>
		/// Generates a new ContentItem of the appropriate type for this node, 
		/// and creates a link for it at the end of the existing item-sequence.
		/// </summary>
		/// <returns>The new ContentItem instance.</returns>
		public ContentItem NewContentItem() {
			return ( this.NewContentItem( this.HighestItemRank + 1 ) );
		}

		/// <summary>
		/// Increments the rank of the linked item at the given rank, moving it down in the item sequence.
		/// </summary>
		/// <param name="ItemRank">Current rank of the item to move.</param>
		public void MoveItemDown( int ItemRank ) {
			//this.CleanupItemRanks();
			// We can't demote if we're already at the end
			if ( ItemRank >= this.HighestItemRank ) { return; }
			string sql = "";
			// Move me out of the way for a minute
			sql += "update CMS_NODE_ITEMS set rank = -1 where tree_node_id = @tree_node_id and rank = @rank; ";
			// Move the item below me up to my spot
			sql += "update CMS_NODE_ITEMS set rank = @rank where tree_node_id = @tree_node_id and rank = @rank + 1; ";
			// Replace me in the spot I just vacated
			sql += "update CMS_NODE_ITEMS set rank = @rank + 1 where tree_node_id = @tree_node_id and rank = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@tree_node_id", this.ID ),
					db.NewParameter( "@rank", ItemRank )
				);
			}
		}

		/// <summary>
		/// Decrements the rank of the linked item at the given rank, moving it up in the item sequence.
		/// </summary>
		/// <param name="ItemRank">Current rank of the item to move.</param>
		public void MoveItemUp( int ItemRank ) {
			//this.CleanupItemRanks();
			// We can't promote if we're already at the top
			if ( ItemRank <= 0 ) { return; }
			string sql = "";
			// Move me out of the way for a minute
			sql += "update CMS_NODE_ITEMS set rank = -1 where tree_node_id = @tree_node_id and rank = @rank; ";
			// Move the item above me down to my spot
			sql += "update CMS_NODE_ITEMS set rank = @rank where tree_node_id = @tree_node_id and rank = @rank - 1; ";
			// Replace me in the spot I just vacated
			sql += "update CMS_NODE_ITEMS set rank = @rank - 1 where tree_node_id = @tree_node_id and rank = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@tree_node_id", this.ID ),
					db.NewParameter( "@rank", ItemRank )
				);
			}
		}

		/// <summary>
		/// Sorts and renumbers the linked ContentItems
		/// </summary>
		public void CleanupItemRanks() {
			TreeNode.CleanupItemRanks( this.ID );
		}

		/// <summary>
		/// Sorts and renumbers the linked ContentItems
		/// </summary>
		public static void CleanupItemRanks( int nodeID ) {
			string sproc = "dbo.renumber_node_items";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sproc, CommandType.StoredProcedure, db.NewParameter( "@tree_node_id", nodeID ) );
			}
		}

		/// <summary>
		/// Steps through each child-node in this.Nodes, attaches all of its items to this node, then deletes the child-node
		/// </summary>
		public void PromoteChildItems() {
			foreach ( TreeNode childnode in this.Nodes ) {
				// If this node has any children, promote their items first
				if ( childnode.Nodes.Count > 0 ) {
					childnode.PromoteChildItems();
				}
				// Link the content-items for the child to this node
				int rank = this.HighestItemRank;
				childnode.contentItems = null;
				List<ContentItem> items = childnode.ContentItems;
				foreach ( ContentItem item in items ) {
					this.LinkContentItem( item, ++rank );
				}
			}
			// Delete the child nodes
			this.DeleteNodes();
		}

		#endregion

		#region XML Generation

		/// <summary>
		/// Returns this node & its descendents as an XML string
		/// </summary>
		/// <returns>An XML document as a string</returns>
		public string ToXml() {
			return ( this.ToXml( false ) );
		}
		
		public string ToXml( bool IncludeItems ) {
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			XmlTextWriter writer = new XmlTextWriter( stream, System.Text.Encoding.Default );
			this.WriteXml( writer, IncludeItems );
			stream.Position = 0;
			System.IO.StreamReader reader = new System.IO.StreamReader( stream );
			string xml = reader.ReadToEnd();
			writer.Close();
			stream.Close();
			return ( xml );
		}

		private const string NodeContentItemsQry =
			@"select i.* from CMS_NODE_ITEMS as ni left outer join CMS_ITEMS as i on i.id = ni.cms_item_id 
				where (i.is_deleted = 0) and (ni.tree_node_id = @tree_node_id) order by ni.rank"
		;

		/// <summary>
		/// Generates the node's XML to a writer (for the ToXml method).
		/// </summary>
		/// <param name="writer"></param>
		protected internal void WriteXml( XmlTextWriter writer, bool IncludeItems ) {
			writer.WriteStartElement( "treenode" );
			// Write the attributes
			writer.WriteAttributeString( "id", this.ID.ToString() );
			writer.WriteAttributeString( "key", this.PathKey );
			writer.WriteAttributeString( "title", this.Title );
			writer.WriteAttributeString( "parentid", this.ParentID.ToString() );
			writer.WriteAttributeString( "nextnodeid", this.NextNodeID.ToString() );
			writer.WriteAttributeString( "isvisible", this.IsVisible.ToString().ToLower() );
			writer.WriteAttributeString( "ismenuitem", this.IncludeAsMenuItem.ToString().ToLower() );
			writer.WriteAttributeString( "issubmenu", this.IncludeSubnodeMenuItems.ToString().ToLower() );
			writer.WriteAttributeString( "typekey", this.TypeKey.ToLower() );
			writer.WriteAttributeString( "isexpanded", this.IsExpanded.ToString().ToLower() );
			writer.WriteAttributeString( "candelete", this.CanDelete.ToString().ToLower() );
			writer.WriteAttributeString( "canedit", this.CanEdit.ToString().ToLower() );
			writer.WriteAttributeString( "canaddnodes", this.CanAddChildren.ToString().ToLower() );
			writer.WriteAttributeString( "canmovevert", this.CanMoveV.ToString().ToLower() );
			writer.WriteAttributeString( "canmovehorz", this.CanMoveH.ToString().ToLower() );
			writer.WriteAttributeString( "mustberoot", this.MustBeRoot.ToString().ToLower() );
			writer.WriteAttributeString( "canbeonlyone", this.CanBeOnlyOne.ToString().ToLower() );
			writer.WriteAttributeString( "headerimage", this.HeaderImage );
			writer.WriteAttributeString( "publishedat", this.PublishedAt );
			writer.WriteAttributeString( "contentitemid", this.DefaultContentItemID.ToString() );
			// Write the linked ContentItems
			if ( IncludeItems ) {
				writer.WriteStartElement( "items" );
//        string qry = @"select i.* from CMS_NODE_ITEMS as ni left outer join CMS_ITEMS as i on i.id = ni.cms_item_id 
//											 where (i.is_deleted = 0) and (ni.tree_node_id = @tree_node_id) order by ni.rank";
//        using ( SqlDatabase db = new SqlDatabase() ) {
//          using ( IDataReader rdr = db.SelectRecords( qry, db.NewParameter( "@tree_node_id", this.ID ) ) ) {
//            ContentItem item = new ContentItem();
//            while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
//              if ( item.IsVisible ) {
//                item.WriteXml( writer );
//              }
//              item = new ContentItem();
//            }
//          }
//        }
				foreach ( ContentItem item in this.ContentItems ) {
					if ( item.IsVisible ) {
						item.WriteXml( writer );
					}
				}
				writer.WriteEndElement();
			}
			// Write any children
			foreach ( TreeNode node in this ) {
				node.WriteXml( writer, IncludeItems );
			}
			writer.WriteEndElement(); // treenode
		}

		#endregion

		public override bool IsValidToDelete() {
			return ( this.CanDelete );
		}

		public void RestoreNodes( DataTable nodesTable ) {
			DataRow[] rows = nodesTable.Select( string.Format( "parent_node_id = {0}", this.ID ) );
			foreach ( DataRow row in rows ) {
				TreeNode node = new TreeNode();
				if ( SqlDatabase.InstantiateFromDataRow( row, node ) ) {
					this.Nodes.Add( node );
					node.RestoreNodes( nodesTable );
					node = new TreeNode();
				}
			}
		}

		#endregion

	}

}
