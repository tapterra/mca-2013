using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.CMS {

	public class ContentMenuNode {

		#region Properties

		public string TreeKey {
			get { return ( this.treeKey ); }
			set { this.treeKey = value; }
		}
		private string treeKey;

		public string ID {
			get { return ( this.id ); }
			set { this.id = value; }
		}
		private string id;

		public string Key {
			get { return ( this.key ); }
			set { this.key = value; }
		}
		private string key;

		public string Text {
			get { return ( this.title ); }
			set { this.title = value; }
		}
		private string title;

		public string TypeKey {
			get { return ( this.typeKey ); }
			set { this.typeKey = value; }
		}
		private string typeKey;

		public string PublishedAt {
			get { return ( this.publishedAt ); }
			set { this.publishedAt = value; }
		}
		private string publishedAt;

		public string ContentItemID {
			get { return ( this.contentItemID ); }
			set { this.contentItemID = value; }
		}
		private string contentItemID = "-1";

		public int ParentID {
			get { return ( this.parentID ); }
			set { this.parentID = value; }
		}
		public int parentID = 0;

		public ContentMenuNode Parent {
			get {
				if ( this.parent == null ) {
					if ( string.IsNullOrEmpty( this.TreeKey ) ) {
						return ( null );
					}
					this.parent = new ContentMenuNode( this.ParentID, this.TreeKey );
				}
				return ( this.parent ); 
			}
		}
		public ContentMenuNode parent;

		public bool IsVisible {
			get { return ( this.isVisible ); }
			set { this.isVisible = value; }
		}
		private bool isVisible = false;

		public bool IsMenuItem {
			get { return ( this.isMenuItem ); }
			set { this.isMenuItem = value; }
		}
		private bool isMenuItem = false;

		public bool IsSubMenu {
			get { return ( this.isSubMenu ); }
			set { this.isSubMenu = value; }
		}
		private bool isSubMenu = false;

		public bool IsLastItem {
			get { return ( this.isLastItem ); }
			set { this.isLastItem = value; }
		}
		private bool isLastItem = false;

		public string TreeNodePath {
			get { return ( this.treeNodePath ); }
			set { this.treeNodePath = value; }
		}
		private string treeNodePath;

		public string NavigateUrl {
			get { return ( this.navigateUrl ); }
			set { this.navigateUrl = value; }
		}
		private string navigateUrl;

		public string Permission {
			get { return ( this.permission ); }
			set { this.permission = value; }
		}
		private string permission;

		public int Level {
			get { return ( this.level ); }
			set { this.level = value; }
		}
		public int level = 0;

		/// <summary>
		/// ID to assign to the asp.net MenuItem for this node
		/// </summary>
		public string ControlID {
			get { return ( this.Key + "_" + this.ID ); }
		}

		public List<ContentMenuNode> SubNodes {
			get { return ( this.subNodes ); }
		}
		private List<ContentMenuNode> subNodes = new List<ContentMenuNode>();

		public List<ContentMenuNode> SiblingNodes {
		  get {
				if ( this.Parent != null ) {
					return ( this.Parent.subNodes );
				}
				return ( new List<ContentMenuNode>() );
		  }
		}

		public List<ContentMenuNode> Stories {
			get { return ( this.stories ); }
		}
		private List<ContentMenuNode> stories = new List<ContentMenuNode>();

		public List<ContentMenuNode> AdminSubNodes {
			get { return ( this.adminSubNodes ); }
		}
		private List<ContentMenuNode> adminSubNodes = new List<ContentMenuNode>();

		public List<ContentMenuNode> AdminSiblingNodes {
			get {
				if ( this.Parent != null ) {
					return ( this.Parent.AdminSubNodes );
				}
				return ( new List<ContentMenuNode>() );
			}
		}

		public List<ContentMenuNode> AdminStories {
			get { return ( this.adminStories ); }
		}
		private List<ContentMenuNode> adminStories = new List<ContentMenuNode>();

		public MenuItem MenuItem {
			get { return ( new MenuItem( this.Text, this.ControlID, "", this.NavigateUrl ) ); }
		}

		#endregion

		#region Constructors

		protected ContentMenuNode() { }

		public ContentMenuNode( int SectionNodeID, string SiteTreeKey ) : this( SectionNodeID, SiteTreeKey, false ) { }

		public ContentMenuNode( int SectionNodeID, string SiteTreeKey, bool IncludeStories ) {
			if ( SectionNodeID <= 0 ) {
				return;
			}
			if ( string.IsNullOrEmpty( SiteTreeKey ) ) {
				return;
			}
			XmlElement elem = ContentPublisher.GetNodeByID( SectionNodeID, SiteTreeKey ) as XmlElement;
			ContentMenuNode.FromXmlElement( this, elem, SiteTreeKey, Level );
			this.TreeKey = SiteTreeKey;
			if ( IncludeStories ) {
				this.LoadSectionStoryMenus( elem, SiteTreeKey );
			}
		}

		public ContentMenuNode( XmlElement MenuElement, string SiteTreeKey ) : this( MenuElement, SiteTreeKey, false ) {}

		public ContentMenuNode( XmlElement MenuElement, string SiteTreeKey, bool IncludeStories ) {
			if ( string.IsNullOrEmpty( SiteTreeKey ) ) {
				return;
			}
			ContentMenuNode.FromXmlElement( this, MenuElement, SiteTreeKey, Level );
			this.TreeKey = SiteTreeKey;
			if ( IncludeStories ) {
				this.LoadSectionStoryMenus( MenuElement, SiteTreeKey );
			}
		}

		#endregion

		#region Private Methods

		private void LoadSectionStoryMenus( XmlElement MenuElement, string SiteTreeKey ) {
			this.stories = new List<ContentMenuNode>();

			string sectionKey = this.TypeKey;
			string publishedAt = this.PublishedAt;

			string treepath = this.TreeNodePath; // ContentPublisher.TreeNodePath( Convert.ToInt32( this.ID ), SiteTreeKey );
			ContentMenuNode lastnode = null;
			XmlNodeList storyNodes = MenuElement.SelectNodes( "items/contentitem" );
			foreach ( XmlNode storyNode in storyNodes ) {
				ContentMenuNode mnode = new ContentMenuNode();
				XmlAttributeCollection attribs = storyNode.Attributes;
				mnode.ID = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
				mnode.Text = ( (XmlAttribute) attribs.GetNamedItem( "title" ) ).Value;
				mnode.TypeKey = ( (XmlAttribute) attribs.GetNamedItem( "typekey" ) ).Value;
				mnode.IsVisible = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "isvisible" ) ).Value );
				mnode.IsMenuItem = true;
				mnode.IsSubMenu = false;
				mnode.Key = sectionKey;
				mnode.PublishedAt = publishedAt;
				mnode.ContentItemID = mnode.ID;
				string filename = string.Format( "story{0}.aspx", mnode.ID );
				mnode.NavigateUrl = string.Format( "~/{0}/{1}", treepath, filename );
				if ( mnode.IsVisible ) {
					this.stories.Add( mnode );
					lastnode = mnode;
				}
				this.adminStories.Add( mnode );
			}
			if ( lastnode != null ) {
				lastnode.IsLastItem = true;
			}
		}

		private static ContentMenuNode FromXmlElement( ContentMenuNode mnode, XmlElement MenuElement, string SiteTreeKey, int Level ) {
			XmlAttributeCollection attribs = MenuElement.Attributes;
			mnode.IsVisible = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "isvisible" ) ).Value );
			mnode.IsMenuItem = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "ismenuitem" ) ).Value );
			mnode.IsSubMenu = Convert.ToBoolean( ( (XmlAttribute) attribs.GetNamedItem( "issubmenu" ) ).Value );
			mnode.ID = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
			mnode.Key = ( (XmlAttribute) attribs.GetNamedItem( "key" ) ).Value;
			mnode.Text = ( (XmlAttribute) attribs.GetNamedItem( "title" ) ).Value;
			mnode.TypeKey = ( (XmlAttribute) attribs.GetNamedItem( "typekey" ) ).Value;
			mnode.PublishedAt = ( (XmlAttribute) attribs.GetNamedItem( "publishedat" ) ).Value;
			mnode.ContentItemID = ( (XmlAttribute) attribs.GetNamedItem( "contentitemid" ) ).Value;
			int pid = 0;
			if ( Int32.TryParse( ( (XmlAttribute) attribs.GetNamedItem( "parentid" ) ).Value, out pid ) ) {
				mnode.ParentID = pid;
			}
			// Build the node's "path" from it's key values...
			string path = "";
			XmlNode xnode = MenuElement;
			while ( xnode.NodeType == XmlNodeType.Element ) {
				if ( mnode.TypeKey == "homepage" ) {
					break;
				}
				XmlAttributeCollection xnodeAttribs = xnode.Attributes;
				string pathkey = ( (XmlAttribute) xnodeAttribs.GetNamedItem( "key" ) ).Value;
				path = "/" + pathkey + path;
				if ( pathkey == SiteTreeKey ) {
					break;
				}
				xnode = xnode.ParentNode;
			}
			mnode.TreeNodePath = path;
			mnode.NavigateUrl = string.Format( "~{0}/default.aspx", path );

			mnode.Permission = "None";
			XmlAttribute permAtt = ( (XmlAttribute) attribs.GetNamedItem( "permission" ) );
			if ( permAtt != null ) {
				mnode.Permission = permAtt.Value;
			}
			ContentMenuNode lastnode = null;
			foreach ( XmlElement elem in MenuElement ) {
				if ( elem.Name != "treenode" ) {
					continue;
				}
				ContentMenuNode subnode = new ContentMenuNode();
				ContentMenuNode.FromXmlElement( subnode, elem, SiteTreeKey, Level + 1 );
				if ( subnode.IsVisible ) {
					mnode.SubNodes.Add( subnode );
					lastnode = subnode;
				}
				mnode.AdminSubNodes.Add( subnode );
			}
			if ( lastnode != null ) {
				lastnode.IsLastItem = true;
			}
			return ( mnode );
		}

		#endregion

		#region Public Static Methods

		public static List<ContentMenuNode> LoadTreeMenus() {
			return ( ContentMenuNode.LoadTreeMenus( ContentPublisher.MainNavTreeKey ) );
		}

		public static List<ContentMenuNode> LoadTreeMenus( string SiteTreeKey ) {
			return ( ContentMenuNode.LoadTreeMenus( SiteTreeKey, false ) );
		}

		public static List<ContentMenuNode> LoadTreeMenus( string SiteTreeKey, bool IncludeInvisible ) {
			List<ContentMenuNode> mainNodes = new List<ContentMenuNode>();
			XmlNode root = ContentPublisher.SiteTreeXmlDoc( SiteTreeKey ).DocumentElement;
			if ( root != null ) {
				foreach ( XmlElement elem in root ) {
					if ( elem.Name == "treenode" ) {
						//ContentMenuNode menuNode = new ContentMenuNode( elem, ContentPublisher.MainNavTreeKey );
						ContentMenuNode menuNode = new ContentMenuNode( elem, SiteTreeKey );
						if ( menuNode.IsVisible || IncludeInvisible ) {
							mainNodes.Add( menuNode );
						}
					}
				}
				ContentMenuNode lastnode = null;
				foreach ( ContentMenuNode menuNode in mainNodes ) {
					if ( menuNode.IsMenuItem ) {
							lastnode = menuNode;
						}
					}
				if ( lastnode != null ) {
					lastnode.IsLastItem = true;
				}
			}
			return ( mainNodes );
		}

		public static List<MenuItem> LoadTreeMenuItems( string SiteTreeKey ) {
			List<MenuItem> results = new List<MenuItem>();
			List<ContentMenuNode> nodes = ContentMenuNode.LoadTreeMenus( SiteTreeKey, false );
			foreach ( ContentMenuNode node in nodes ) {
				MenuItem item = node.MenuItem;
				LoadSubMenuItems( item, node );
				results.Add( item );
				}
			return ( results );
			}

		protected static void LoadSubMenuItems( MenuItem item, ContentMenuNode node ) {
			foreach ( ContentMenuNode subnode in node.SubNodes ) {
				MenuItem subitem = subnode.MenuItem ;
				item.ChildItems.Add( subitem );
				LoadSubMenuItems( subitem, subnode );
			}
		}

		#endregion

	}

	public class AuthorizeMenuEventArgs : EventArgs {
		public ContentMenuNode MenuNode;
		public bool IsAuthorized;

		public AuthorizeMenuEventArgs( ContentMenuNode menuNode ) {
			this.MenuNode = menuNode;
			this.IsAuthorized = true;
		}

	}

}
