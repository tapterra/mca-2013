using System;
using System.Web;


namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// HttpHandler for accessing documents (files) stored in the database (via ContentDocument).
	/// Note: To install, add the following tag to the &lt;httpHandlers&gt; section of web.config:
	/// &lt;add verb="GET" path="CMSDocument.axd" type="StarrTech.WebTools.CMS.DocumentHandler, StarrTech.WebTools2" /&gt;
	/// To generate a compatible URL, use ContentDocument.DownloadUrlFormat or ContentDocument.DownloadUrl
	/// </summary>
	public class DocumentHandler : IHttpHandler {

		#region IHttpHandler implementation

		/// <summary>
		/// This handler is marked as reusable and always return true.
		/// </summary>
		public bool IsReusable {
			get { return ( true ); }
		}

		/// <summary>
		/// Process the request
		/// </summary>
		/// <param name="context">The current HTTP context, from which we get to access the Request and the Response.</param>
		public virtual void ProcessRequest( HttpContext context ) {
			// Parse the query string 
			string idStr = context.Request.QueryString["id"];
			string attachStr = context.Request.QueryString["ax"];
			if ( !string.IsNullOrEmpty( idStr ) ) {
				try {
					// Determine the proper parameter values
					int id = 0;
					Int32.TryParse( idStr, out id );
					if ( id <= 0 ) { return; }
					// Load the document from the database
					ContentDocument doc = new ContentDocument( id );
					// Is this an attachment?
					bool attach = false;
					if ( attachStr == null ) {
						attach = !doc.IsImage;
					} else {
						attach = ( attachStr == "1" );
					}
					// Fetch the bits
					doc.DownloadDocument( context.Response, attach );
				} catch ( Exception e ) {
					string msg = e.Message;
				}
			}
		}

		#endregion

	}
}
