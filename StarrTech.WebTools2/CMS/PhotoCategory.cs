using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Web;
using System.Web.Caching;
using StarrTech.WebTools.Data;
using StarrTech.WebTools.CMS;
using System.Text;

namespace StarrTech.WebTools.CMS {

	[SqlTable( "PHOTO_CATEGORIES", SingularLabel = "Category", PluralLabel = "Categories", IsMarkedDeleted = true )]
	public class PhotoCategory : BaseSqlPersistable {

		#region Properties

		#region Database Columns

		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		[SqlColumn( "label", DbType.String, Length = 100 )]
		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label;

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		#endregion

		#region Constructors

		// Access the base constructors
		public PhotoCategory() : base() { }
		public PhotoCategory( int ID ) : base( ID ) { }

		#endregion

		#region Reordering Support

		/// <summary>
		/// Persists the instance to the database.
		/// Overrides BaseSqlPersistable.Persist to reset the item ranks when a new item is inserted.
		/// </summary>
		/// <returns>True if the item was sucessfuly persisted.</returns>
		public override bool Persist() {
			bool newItem = ( this.ID <= 0 );
			bool result = base.Persist();
			if ( newItem ) {
				PhotoCategory.ResetItemRanks();
			}
			return ( result );
		}

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public static int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_photo_categories";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public static int HighestSeq {
			get { return ( PhotoCategory.ResetItemRanks() - 1 ); }
		}

		/// <summary>
		/// Move this item up in the sequence-order for it's table
		/// </summary>
		public void MoveUp() {
			PhotoCategory.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( this.Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update PHOTO_CATEGORIES set seq = -1 where id = @id; ";
			// Move the item above me down to my spot
			sql += "update PHOTO_CATEGORIES set seq = @seq where seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update PHOTO_CATEGORIES set seq = @seq - 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		/// <summary>
		/// Move this item down in the sequence-order for it's table
		/// </summary>
		public void MoveDown() {
			// We can't demote if we're already at the end
			if ( this.Seq >= PhotoCategory.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update PHOTO_CATEGORIES set seq = -1 where id = @id; ";
			// Move the item below me up to my spot
			sql += "update PHOTO_CATEGORIES set seq = @seq where seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update PHOTO_CATEGORIES set seq = @seq + 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		#endregion

		#region Associated Photos

		public CategoryPhotos Photos {
			get {
				if ( this.photos == null ) {
					this.photos = new CategoryPhotos( this );
				}
				return ( this.photos );
			}
		}
		private CategoryPhotos photos;

		#endregion

		#region Static Search Members

		public static void NewSearch() { }

		public static new string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static new string sortExpression = "seq";

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			string sql = @"select * from	PHOTO_CATEGORIES where ( is_deleted = 0 ) order by seq";
			return ( sql );
		}

		public static List<PhotoCategory> DoSearchList() {
			return ( SqlDatabase.DoSearch<PhotoCategory>( PhotoCategory.GetSearchSql() ) );
		}

		#endregion

	}

	public class PhotoCategories : List<PhotoCategory> {

		public static string CacheKey = "PhotoCategories";

		public static PhotoCategories Current {
			get {
				PhotoCategories current = HttpContext.Current.Cache[CacheKey] as PhotoCategories;
				if ( current == null ) {
					current = new PhotoCategories();
					HttpContext.Current.Cache.Insert( CacheKey, current, null, DateTime.Now.AddHours( 1 ), Cache.NoSlidingExpiration );
				}
				return ( current );
			}
		}

		private PhotoCategories()
			: base() {
			this.RestoreItems();
		}

		public static void ReloadCurrent() {
			HttpContext.Current.Cache.Remove( CacheKey );
		}

		private void RestoreItems() {
			string sql = "select * from PHOTO_CATEGORIES where is_deleted = 0 order by seq";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					PhotoCategory item = new PhotoCategory();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new PhotoCategory();
					}
				}
			}
		}

	}

	public class CategoryPhotos : List<ImageDocument> {

		private PhotoCategory Owner = null;

		private CategoryPhotos() {}

		public CategoryPhotos( PhotoCategory owner ) {
			this.Owner = owner;
			this.RestoreItems();
		}

		public int FindIndexOf( int photoID ) {
			int index = 0;
			foreach ( ImageDocument photo in this ) {
				if ( photo.ID == photoID ) { return ( index ); }
				index++;
			}
			return ( -1 );
		}

		public void RestoreItems() {
			this.Clear();
			string sql = ImageDocument.GetSearchSql( this.Owner.ID, "", "seq" );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql.ToString() ) ) {
					ImageDocument item = new ImageDocument();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new ImageDocument();
					}
				}
			}
		}

		#region Reordering Support

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_category_photos";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure, db.NewParameter( "@category_id", this.Owner.ID ) );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public int HighestSeq {
			get { return ( this.ResetItemRanks() - 1 ); }
		}

		/// <summary>
		/// Move this item up in the sequence-order for it's table
		/// </summary>
		public void MoveUp( ImageDocument item, int seq ) {
			this.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( seq <= 0 ) { return; }
			string sql = "";
			// Swap seq values between item and it's immediate predecessor
			sql += "update PHOTO_CAT_LINKS set seq = -1 where cms_document_id = @cms_document_id and category_id = @category_id; ";
			sql += "update PHOTO_CAT_LINKS set seq = @seq where seq = @seq - 1 and category_id = @category_id; ";
			sql += "update PHOTO_CAT_LINKS set seq = @seq - 1 where cms_document_id = @cms_document_id and category_id = @category_id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@cms_document_id", item.ID ),
					db.NewParameter( "@category_id", this.Owner.ID ),
					db.NewParameter( "@seq", seq )
				);
			}
		}

		/// <summary>
		/// Move this item down in the sequence-order for it's table
		/// </summary>
		public void MoveDown( ImageDocument item, int seq ) {
			// We can't demote if we're already at the end
			if ( seq >= this.HighestSeq ) { return; }
			string sql = "";
			// Swap seq values between item and it's immediate predecessor
			sql += "update PHOTO_CAT_LINKS set seq = -1 where cms_document_id = @cms_document_id and category_id = @category_id; ";
			sql += "update PHOTO_CAT_LINKS set seq = @seq where seq = @seq + 1 and category_id = @category_id; ";
			sql += "update PHOTO_CAT_LINKS set seq = @seq + 1 where cms_document_id = @cms_document_id and category_id = @category_id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@cms_document_id", item.ID ),
					db.NewParameter( "@category_id", this.Owner.ID ),
					db.NewParameter( "@seq", seq )
				);
			}
		}

		#endregion

	}

}
