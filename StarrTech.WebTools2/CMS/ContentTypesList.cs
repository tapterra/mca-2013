using System;
using System.Collections;
using System.Collections.ObjectModel;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Custom collection class for handling the list of ContentTypes in a ContentItemVersion
	/// </summary>
	public class ContentTypesList : KeyedCollection<string, ContentType> {

		public ContentTypesList() : base() { }

		protected override string GetKeyForItem( ContentType item ) {
			return item.TypeKey;
		}
	}
	
}
