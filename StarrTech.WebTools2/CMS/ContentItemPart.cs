using System;
using System.Data;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;
using System.Xml;

namespace StarrTech.WebTools.CMS {
	
	public enum ContentPartTypes {
		PlainText = 10,
		HtmlText = 20,
    TinyMCE = 25,
    OptionCheckbox = 30,
		OptionListbox = 35,
		Image = 40,
		FileLink = 60
	}
	
	/// <summary>
	/// Low-level data elements for the versions of the CMS content items
	/// Represents a single record from the CMS_PARTS table.
	/// </summary>
	[SqlTable( "CMS_PARTS", SingularLabel = "Content-Item Part", PluralLabel = "Content-Item Parts", IsMarkedDeleted = true )]
	public class ContentItemPart : BaseSqlPersistable {
		
		#region Properties
		
		#region Database Columns

		[SqlColumn( "cms_version_id", DbType.Int32 )]
		public int ContentVersionID {
			get { return ( this.contentVersionID ); }
			set { this.contentVersionID = value; }
		}
		private int contentVersionID = -1;

		[SqlColumn( "part_number", DbType.Int32 )]
		public int PartNo {
			get { return ( this.partNo ); }
			set { this.partNo = value; }
		}
		private int partNo = -1;

		[SqlColumn( "name", DbType.String, Length = 50 )]
		public string Name {
			get { return ( this.name ); }
			set { this.name = value; }
		}
		private string name = "";

		[SqlColumn( "label", DbType.String, Length = 200 )]
		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label = "";

		[SqlColumn( "data_type_code", DbType.Int32 )]
		protected internal int DBDataTypeCode {
			get { return ( (int) this.DataTypeCode ); }
			set { this.DataTypeCode = (ContentPartTypes) value; }
		}
		
		public ContentPartTypes DataTypeCode {
			get { return ( this.dataTypeCode ); }
			set { this.dataTypeCode = value; }
		}
		private ContentPartTypes dataTypeCode = 0;

		[SqlColumn( "content", DbType.String )]
		public string Content {
			get { return ( this.content ); }
			set { this.content = value; }
		}
		private string content = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		public string Height {
			get { return ( this.height ); }
			set { this.height = value; }
		}
		private string height = "";

		public int Rows {
			get { return ( this.rows ); }
			set { this.rows = value; }
		}
		private int rows = 1;

		#endregion
		
		#endregion
		
		#region Constructors
		
		// do we ever want to instantiate a bare naked new ContentPart (?)
		public ContentItemPart() : base() {}

		protected internal ContentItemPart( XmlNode partNode ) : base() {
			// <ContentItemPart PartNo="1" Name="headline" Label="Headline" Type="PlainText" Rows="1" />
			XmlAttribute attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "PartNo" );
			if ( attr != null )
				this.PartNo = Convert.ToInt32( attr.Value );
			attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "Name" );
			if ( attr != null )
				this.Name = attr.Value;
			attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "Label" );
			if ( attr != null )
				this.Label = attr.Value;
			attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "Type" );
			if ( attr != null )
				this.DataTypeCode = (ContentPartTypes) Enum.Parse( typeof( ContentPartTypes ), attr.Value, true );
			attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "Height" );
			if ( attr != null )
				this.Height = attr.Value;
			int rows = 1;
			attr = (XmlAttribute) partNode.Attributes.GetNamedItem( "Rows" );
			if ( attr != null )
				Int32.TryParse( attr.Value, out rows );
			this.Rows = rows;
		}

		/// <summary>
		/// Instantiate a new part that's a content-specific copy of an existing part
		/// </summary>
		/// <param name="PartToCopy">The existing part instance to copy</param>
		protected internal ContentItemPart( ContentItemPart PartToCopy ) : this() {
			this.ID = -1;
			this.ContentVersionID = PartToCopy.ContentVersionID;
			this.PartNo = PartToCopy.PartNo;
			this.Name = PartToCopy.Name;
			this.Label = PartToCopy.Label;
			this.Content = PartToCopy.Content;
			this.DataTypeCode = PartToCopy.DataTypeCode;
			this.Comments = PartToCopy.Comments;
		}
		
		
		/// <summary>
		/// Given an existing Part ID, instantiate and restore the part.
		/// </summary>
		/// <param name="ID">Record ID for the part we want.</param>
		internal protected ContentItemPart( int ID ) : base( ID ) {}
		
		
		/// <summary>
		/// Given the Version ID and part-name, instantiate & restore the part.
		/// </summary>
		/// <param name="versionID">Record ID for the parent ContentVersion instance</param>
		/// <param name="name">Part Name for the part we want to instantiate</param>
		internal protected ContentItemPart( int versionID, string name ) : base() {
			string sql = "select * from CMS_PARTS where cms_version_id = @cms_version_id and name = @name ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql,
					db.NewParameter( "@cms_version_id", versionID ),
					db.NewParameter( "@name", name )
				);
			}
		}
		
		
		/// <summary>
		/// Given the Version ID and part-number, instantiate & restore the part.
		/// </summary>
		/// <param name="versionID">Record ID for the parent ContentVersion instance</param>
		/// <param name="partNo">Part Number for the part we want to instantiate</param>
		//internal protected ContentItemPart( int versionID, int partNo ) : base() {
		//  string sql = "select * from CMS_PARTS where cms_version_id = @cms_version_id and part_number = @part_number ";
		//  using ( SqlDatabase db = new SqlDatabase() ) {
		//    db.SelectInstance( this, sql,
		//      db.NewParameter( "@cms_version_id", versionID ),
		//      db.NewParameter( "@part_number", partNo )
		//    );
		//  }
		//}
		
		
		#endregion
		
		#region Methods
		/// <summary>
		/// We never want to delete a ContentItemPart
		/// </summary>
		/// <returns>Never happens</returns>
		//public override bool Delete() {
		//  throw new InvalidOperationException( "Can't delete a ContentItemPart!" );
		//}
		
		
		#region XML Rendering
		
		/// <summary>
		/// Returns this part as an XML string
		/// </summary>
		/// <returns>An XML document as a string</returns>
		public string ToXml() {
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			XmlTextWriter writer = new XmlTextWriter( stream, System.Text.Encoding.Default );
			this.WriteXml( writer );
			stream.Position = 0;
			System.IO.StreamReader reader = new System.IO.StreamReader( stream );
			string xml = reader.ReadToEnd();
			writer.Close();
			stream.Close();
			return ( xml );
		}
		
		
		/// <summary>
		/// Generates this part's XML to a writer (for the ToXml method).
		/// </summary>
		/// <param name="writer">an XmlTextWriter for catching the XML.</param>
		protected internal void WriteXml( XmlTextWriter writer ) {
			writer.WriteStartElement( "part");
			// Write the attributes
			writer.WriteAttributeString( "id", this.ID.ToString() );
			writer.WriteAttributeString( "partno", this.PartNo.ToString() );
			writer.WriteAttributeString( "name", this.Name );
			writer.WriteAttributeString( "label", this.Label );
			writer.WriteAttributeString( "typecode", this.DataTypeCode.ToString() );
			writer.WriteStartElement( "content" );
			writer.WriteCData( this.Content );
			writer.WriteEndElement(); //content
			writer.WriteEndElement(); // part
		}
		
		
		#endregion
		
		#endregion
		
	}
	
	
}
