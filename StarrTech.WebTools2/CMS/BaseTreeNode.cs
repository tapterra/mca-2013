using System;
using System.Collections;
using System.Data;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;

namespace StarrTech.WebTools.CMS {
	
	/// <summary>
	/// Summary description for BaseTreeNode.
	/// </summary>
	public abstract class BaseTreeNode : BaseSqlPersistable, IEnumerable {
		
		#region Properties
		public BaseTreeNode ParentNode {
			get { return ( this.parentNode ); }
			set { this.parentNode = value; }
		}
		private BaseTreeNode parentNode = null;
		
		/// <summary>
		/// The list of direct children of this node.
		/// </summary>
		public TreeNodeList Nodes {
			get {
				if ( this.nodes == null ) {
					this.nodes = new TreeNodeList( this );
				}
				return ( nodes ); 
			}
		}
		private TreeNodeList nodes = null;
		
		public Tree Tree {
			get { 
				if ( this.ParentNode == null ) {
					return ( this as Tree ); 
				} else {
					return ( this.ParentNode.Tree ); 
				}
			}
		}
		
		/// <summary>
		/// Record ID for the CMS_TREES record at the root of this tree/node
		/// </summary>
		public abstract int TreeID { 
			get; set; 
		}
		
		/// <summary>
		/// Returns the number of direct children in the nodes list
		/// </summary>
		public int Count {
			get { return ( this.Nodes.Count ); }
		}

		/// <summary>
		/// Switch to determine if this node should be added to the menu as a menu item.
		/// </summary>
		public abstract bool IncludeAsMenuItem {
			get;
			set;
		}

		/// <summary>
		/// Switch to determine if the children of this node should be added to the menu as a sub/popup menu.
		/// </summary>
		public abstract bool IncludeSubnodeMenuItems {
			get;
			set;
		}

		#endregion
		
		#region Constructors
		// Access the base constructors
		public BaseTreeNode() : base() {}
		public BaseTreeNode( int ID ) : base( ID ) {}
		
		#endregion
		
		#region Adding, rearranging and removing children
		/// <summary>
		/// Add a node to the end of the list of children
		/// </summary>
		/// <param name="NodeToAdd">The node to add.</param>
		public void AddNode( TreeNode NodeToAdd ) {
			// Validate...
			if ( NodeToAdd == null ) {
				throw new ArgumentException( "NodeToAdd must not be null.", "NodeToAdd" );
			}
			// Make sure our new node has a valid ID
			if ( NodeToAdd.ID <= 0 ) NodeToAdd.Persist();
			// Find the node currently at the end of the chain
			TreeNode lastNode = this.Nodes.FindLastNode();
			// If lastNode is null, then NodeToAdd is our first child node
			if ( lastNode != null ) {
				lastNode.NextNodeID = NodeToAdd.ID;
				lastNode.Persist();
			}
			// Add our new node to the list
			this.Nodes.Add( NodeToAdd );
			NodeToAdd.NextNodeID = 0; // we're getting added to the end of the list here
			NodeToAdd.Persist();
		}
		
		/// <summary>
		/// Create a new child node (given a key and title) and add it to the end of this node's list of children.
		/// </summary>
		/// <param name="NodeKey">Path-key string for the new node.</param>
		/// <param name="NodeTitle">Title string for the new node.</param>
		/// <returns></returns>
		public TreeNode AddNode( string NodeKey, string NodeTitle ) {
			int parentID = ( this is Tree ? 0 : this.ID );
			TreeNode node = new TreeNode( this.TreeID, parentID, 0, NodeKey, NodeTitle, true, "", "" );
			this.AddNode( node );
			return ( node );
		}
		
		/// <summary>
		/// Insert a node before an existing node in the list of children.
		/// </summary>
		/// <param name="NodeToAdd">The node to insert into the list of children</param>
		/// <param name="BeforeNode">A child node before which the NodeToAdd should be positioned.</param>
		public void InsertNodeBefore( TreeNode NodeToAdd, TreeNode BeforeNode ) {
			// Validate...
			if ( NodeToAdd == null ) {
				throw new ArgumentException( "NodeToAdd must not be null.", "NodeToAdd" );
			}
			if ( BeforeNode == null ) {
				throw new ArgumentException( "BeforeNode must not be null.", "BeforeNode" );
			}
			if ( ! this.Nodes.Contains( BeforeNode ) ) {
				BaseTreeNode parent = this.FindNode( BeforeNode.ParentID );
				if ( parent != null ) {
					parent.InsertNodeBefore( NodeToAdd, BeforeNode );
					return;
				} else {
					throw new ArgumentException( "BeforeNode must be a descendent of this node.", "NodeToMove" );
				}
			}
			// Add our new node to the list
			this.Nodes.Add( NodeToAdd );
			// Find BeforeNode's previous-sibling node
			TreeNode prevNode = this.nodes.FindPrevNode( BeforeNode );
			if ( prevNode != null ) {
				prevNode.NextNodeID = NodeToAdd.ID;
				prevNode.Persist();
			}
			NodeToAdd.NextNodeID = BeforeNode.ID;
			NodeToAdd.Persist();
		}
		
		/// <summary>
		/// Insert a node after an existing node in the list of children.
		/// </summary>
		/// <param name="NodeToAdd">The node to insert into the list of children</param>
		/// <param name="AfterNode">A child node after which the NodeToAdd should be positioned.</param>
		public void InsertNodeAfter( TreeNode NodeToAdd, TreeNode AfterNode ) {
			// Validate...
			if ( NodeToAdd == null ) {
				throw new ArgumentException( "NodeToAdd must not be null.", "NodeToAdd" );
			}
			if ( AfterNode == null ) {
				throw new ArgumentException( "AfterNode must not be null.", "BeforeNode" );
			}
			if ( ! this.Nodes.Contains( AfterNode ) ) {
				BaseTreeNode parent = this.FindNode( AfterNode.ParentID );
				if ( parent != null ) {
					parent.InsertNodeAfter( NodeToAdd, AfterNode );
					return;
				} else {
					throw new ArgumentException( "AfterNode must be a descendent of this node.", "AfterNode" );
				}
			}
			// Add our new node to the list
			this.Nodes.Add( NodeToAdd );
			// Find AfterNode's next-sibling node
			TreeNode nextNode = this.nodes.FindNextNode( AfterNode );
			if ( nextNode != null ) {
				NodeToAdd.NextNodeID = nextNode.ID;
				NodeToAdd.Persist();
			} else if ( NodeToAdd.ID <= 0 ) {
				NodeToAdd.Persist();
			}
			AfterNode.NextNodeID = NodeToAdd.ID;
			AfterNode.Persist();
		}
		
		/// <summary>
		/// Move a child node up one notch in the list of children 
		/// </summary>
		/// <param name="NodeToMove">The node to move</param>
		public void MoveNodeUp( TreeNode NodeToMove ) {
			// Validate...
			if ( NodeToMove == null ) {
				throw new ArgumentException( "NodeToMove must not be null.", "NodeToMove" );
			}
			if ( ! this.Nodes.Contains( NodeToMove ) ) {
				BaseTreeNode parent = this.FindNode( NodeToMove.ParentID );
				if ( parent != null ) {
					parent.MoveNodeUp( NodeToMove );
					return;
				} else {
					throw new ArgumentException( "NodeToMove must be a descendent of this node.", "NodeToMove" );
				}
			}
			// Find the 2 nodes preceeding our node in the list
			TreeNode prevNode1 = this.Nodes.FindPrevNode( NodeToMove );
			// if prevNode1 is null, this node is already the first in the list
			if ( prevNode1 == null ) return;
			TreeNode prevNode2 = this.Nodes.FindPrevNode( prevNode1 );
			prevNode1.NextNodeID = NodeToMove.NextNodeID;
			prevNode1.Persist();
			NodeToMove.NextNodeID = prevNode1.ID;
			NodeToMove.Persist();
			if ( prevNode2 != null ) {
				prevNode2.NextNodeID = NodeToMove.ID;
				prevNode2.Persist();
			}
		}
		
		/// <summary>
		/// Move a child node down one notch in the list of children
		/// </summary>
		/// <param name="NodeToMove">The node to move</param>
		public void MoveNodeDown( TreeNode NodeToMove ) {
			// Validate...
			if ( NodeToMove == null ) {
				throw new ArgumentException( "NodeToMove must not be null.", "NodeToMove" );
			}
			if ( ! this.Nodes.Contains( NodeToMove ) ) {
				BaseTreeNode parent = this.FindNode( NodeToMove.ParentID );
				if ( parent != null ) {
					parent.MoveNodeDown( NodeToMove );
					return;
				} else {
					throw new ArgumentException( "NodeToMove must be a descendent of this node.", "NodeToMove" );
				}
			}
			// Find the nodes preceeding and following our node in the list
			TreeNode nextNode = this.Nodes.FindNextNode( NodeToMove );
			TreeNode prevNode = this.Nodes.FindPrevNode( NodeToMove );
			// if nextNode is null, this node is already the last in the list
			if ( nextNode == null ) return;
			if ( prevNode != null ) {
				prevNode.NextNodeID = NodeToMove.NextNodeID;
				prevNode.Persist();
			}
			NodeToMove.NextNodeID = nextNode.NextNodeID;
			NodeToMove.Persist();
			nextNode.NextNodeID = NodeToMove.ID;
			nextNode.Persist();
		}
		
		public void MoveNodeLeft( TreeNode NodeToMove ) {
			// Validate...
			if ( NodeToMove == null ) {
				throw new ArgumentException( "NodeToMove must not be null.", "NodeToMove" );
			}
			if ( ! this.Nodes.Contains( NodeToMove ) ) {
				BaseTreeNode parent = this.FindNode( NodeToMove.ParentID );
				if ( parent != null ) {
					parent.MoveNodeLeft( NodeToMove );
					return;
				} else {
					throw new ArgumentException( "NodeToMove must be a descendent of this node.", "NodeToMove" );
				}
			}
			// Remove the node from my collection of nodes...
			this.RemoveNode( NodeToMove );
			// And insert it into my parent's, right after me
			TreeNode nextNode = this.ParentNode.Nodes.FindNextNode( this as TreeNode );
			if ( nextNode == null ) {
				this.ParentNode.AddNode( NodeToMove );
			} else {
				this.ParentNode.InsertNodeBefore( NodeToMove, nextNode );
			}
		}
		
		public void MoveNodeRight( TreeNode NodeToMove ) {
			// Validate...
			if ( NodeToMove == null ) {
				throw new ArgumentException( "NodeToMove must not be null.", "NodeToMove" );
			}
			if ( ! this.Nodes.Contains( NodeToMove ) ) {
				BaseTreeNode parent = this.FindNode( NodeToMove.ParentID );
				if ( parent != null ) {
					parent.MoveNodeRight( NodeToMove );
					return;
				} else {
					throw new ArgumentException( "NodeToMove must be a descendent of this node.", "NodeToMove" );
				}
			}
			// Find the node's immediately-preceeding sibling
			TreeNode prevNode = this.Nodes.FindPrevNode( NodeToMove );
			// If this node's at the top of it's sibling list, it can't go right!
			if ( prevNode == null ) return;
			// Remove the node from my collection of nodes...
			this.RemoveNode( NodeToMove );
			// And give it to it's most immediately-preceeding sibling...
			prevNode.AddNode( NodeToMove );
		}
		
		/// <summary>
		/// Remove a child node from the list of children (without deleting it from the database).
		/// </summary>
		/// <param name="NodeToRemove">The existing child node to remove from this list.</param>
		public void RemoveNode( TreeNode NodeToRemove ) {
			// Validate...
			if ( NodeToRemove == null ) {
				throw new ArgumentException( "NodeToRemove must not be null.", "NodeToRemove" );
			}
			if ( ! this.Nodes.Contains( NodeToRemove ) ) {
				BaseTreeNode parent = this.FindNode( NodeToRemove.ParentID );
				if ( parent != null ) {
					parent.RemoveNode( NodeToRemove );
					return;
				} else {
					throw new ArgumentException( "NodeToRemove must be a child of this node.", "NodeToRemove" );
				}
			}
			// If this node is in the middle of the chain, we need to "join" the nodes before and after
			TreeNode prevNode = this.Nodes.FindPrevNode( NodeToRemove );
			// if prevNode is null, this node is already the first in the list
			if ( prevNode != null ) {
				prevNode.NextNodeID = NodeToRemove.NextNodeID;
				prevNode.Persist();
			}
			this.Nodes.Remove( NodeToRemove );
			NodeToRemove.Persist();
		}
		
		/// <summary>
		/// Removes a child node from the list and deletes it (and all it's descendents) from the database.
		/// </summary>
		/// <param name="NodeToDelete">The child node to delete.</param>
		public void DeleteNode( TreeNode NodeToDelete ) {
			if ( NodeToDelete == null ) {
				throw new ArgumentException( "NodeToDelete must not be null.", "NodeToDelete" );
			}
			BaseTreeNode parent;
			if ( ! this.Nodes.Contains( NodeToDelete ) ) {
				parent = this.FindNode( NodeToDelete.ParentID );
			} else {
				parent = this;
			}
			if ( parent != null ) {
				parent.RemoveNode( NodeToDelete );
			} else {
				throw new ArgumentException( "NodeToDelete must be a child of this node.", "NodeToDelete" );
			}
			NodeToDelete.Delete();
		}
		
		/// <summary>
		/// Removes all child nodes and deletes them from the database
		/// </summary>
		public void DeleteNodes() {
			foreach ( TreeNode node in this ) {
				this.RemoveNode( node );
				node.Delete();
			}
		}
		
		/// <summary>
		/// Given the path-key for any child node, find and return a reference to the matching node
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public TreeNode FindNode( string key ) {
			return ( this.Nodes.FindNode( key ) );
		}
		public TreeNode FindNode( int id ) {
			return ( this.Nodes.FindNode( id ) );
		}
		
		public TreeNode FindNodeByTypeKey( string key ) {
			return ( this.Nodes.FindNodeByTypeKey( key ) );
		}

		#endregion
		
		#region BaseSqlPersistable overrides
		
		/// <summary>
		/// Delete this node and all it's descendents from the database.
		/// </summary>
		public override bool Delete() {
			if ( this.IsValidToDelete() ) {
				this.DeleteNodes();
			}
			return ( base.Delete() );
		}
		
		#endregion
		
		#region IEnumerable Members
		/// <summary>
		/// Generates the enumerator for this instance (for use in a foreach block)
		/// </summary>
		/// <returns>An enumerator</returns>
		public IEnumerator GetEnumerator() {
			return new TreeNodeEnumerator( this.Nodes );
		}
		#endregion
		
	}

	/// <summary>
	/// Enumerator for BaseTreeNode
	/// </summary>
	public class TreeNodeEnumerator : IEnumerator {
		private TreeNode currentNode;
		private TreeNodeList list;

		public TreeNodeEnumerator( TreeNodeList list ) {
			this.list = list;
		}

		public void Reset() {
			this.currentNode = null;
		}

		public object Current {
			get {
				return ( this.currentNode );
			}
		}

		public bool MoveNext() {
			if ( this.currentNode == null ) {
				this.currentNode = this.list.FindFirstNode();
			} else {
				this.currentNode = this.list.FindNextNode( this.currentNode );
			}
			return ( this.currentNode != null );
		}

	}
	
}
