using System;
using System.Data;
using StarrTech.WebTools;
using StarrTech.WebTools.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace StarrTech.WebTools.CMS {
	
	public enum ContentVersionStatusLevels {
		DraftRevision = 0, 
		AwaitingApproval = 10, 
		Active = 20, 
		Archived = 90
	}
	
	/// <summary>
	/// Top-level element of content from the CMS.
	/// Represents a record from the CMS_ITEMS database table.
	/// </summary>
	[SqlTable( "CMS_ITEMS", SingularLabel = "Story", PluralLabel = "Stories", IsMarkedDeleted = true )]
	public class ContentItem : BaseSqlPersistable {
		
		#region Properties
		
		#region Database Columns

		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDT {
			get { return ( this.createdDT ); }
			set { this.createdDT = value; }
		}
		private DateTime createdDT = DateTime.Now;

		[SqlColumn( "createdby_user_id", DbType.Int32 )]
		public int CreatedByUserID {
			get { return ( this.createdByUserID ); }
			set { this.createdByUserID = value; }
		}
		private int createdByUserID = -1;

		[SqlColumn( "type_key", DbType.String, Length = 50 )]
		public string TypeKey {
			get { return ( this.typeKey ); }
			set { this.typeKey = value; }
		}
		private string typeKey = "";

		[SqlColumn( "is_approved", DbType.Boolean )]
		public bool IsApproved {
			get { return ( this.isApproved ); }
			set { this.isApproved = value; }
		}
		private bool isApproved = true;

		[SqlColumn( "active_date", DbType.DateTime )]
		public DateTime ActiveDate {
			get { return ( this.activeDate ); }
			set { this.activeDate = value.Date; }
		}
		private DateTime activeDate = DateTime.MinValue;

		[SqlColumn( "expire_date", DbType.DateTime )]
		public DateTime ExpireDate {
			get { return ( this.expireDate ); }
			set { this.expireDate = value.Date; }
		}
		private DateTime expireDate = DateTime.MaxValue;

		[SqlColumn( "title", DbType.String, Length = 200 )]
		public string Title {
			get { return ( this.title ); }
			set { this.title = value; }
		}
		private string title = "";
		
		[SqlColumn( "description", DbType.String )]
		public string Description {
			get { return ( this.description ); }
			set { this.description = value; }
		}
		private string description = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		[SqlColumn( "keywords", DbType.String )]
		public string Keywords {
			get { return ( this.keywords ); }
			set { this.keywords = value; }
		}
		private string keywords = "";

		#endregion
		
		#region Versions
		
		/// <summary>
		/// Collection of ContentItemVersion instances for this item 
		/// </summary>
		protected internal ContentItemVersionsList Versions {
			get {
				if ( this.versions == null ) {
					this.versions = new ContentItemVersionsList( this );
				}
				if ( this.versions.Count == 0 ) {
					this.RestoreVersions();
				}
				return ( this.versions ); 
			}
		}
		private ContentItemVersionsList versions = null;
		
		/// <summary>
		/// Return a reference to this item's "Active" version (null if no active version exists)
		/// "Active" means the version that is currently published on the website (status = Approved, there can be only zero or one)
		/// </summary>
		public ContentItemVersion CurrentActiveVersion {
			get { return ( this.Versions.CurrentActiveVersion ); }
		}
		
		/// <summary>
		/// Return a reference to this item's "Current Revision" version (null if no current-revision version exists)
		/// The "Current Revision" version is the one in Draft/Revision/Awaiting Approval status - only one should exist
		/// This is the version that is editable in the Admin GUI.
		/// </summary>
		public ContentItemVersion CurrentRevisionVersion {
			get { return ( this.Versions.CurrentRevisionVersion ); }
		}
		
		/// <summary>
		/// Current Status property - value indicating the status of the item's most-recent or most-relevent version
		/// </summary>
		// ... this will have a different meaning when in the admin (revision more significant than active) 
		// ... than it does when publishing (active is the only relevent status)
		// ... maybe we want an "IsActive" and an "IsInRevision" also/instead?
		public ContentVersionStatusLevels CurrentStatus {
			get { return ( this.Versions[this.Versions.MostRecentVersionNumber].Status ); }
		}
		
		#endregion

		/// <summary>
		/// Sequence ranking for catalog items
		/// </summary>
		public int Rank {
			get { return ( this.rank ); }
			set { this.rank = value; }
		}
		private int rank = -1;

		/// <summary>
		/// Returns a ContentType instance for this item's content type
		/// </summary>
		public ContentType ContentType {
			get {
				if ( !string.IsNullOrEmpty( this.TypeKey ) ) {
					if ( ContentTypeManager.ContentTypes.Contains( this.TypeKey ) ) {
						return ( ContentTypeManager.ContentTypes[this.TypeKey] );
					}
				}
				return ( null );
			}
		}

		/// <summary>
		/// A descriptive label for the item's content-type
		/// </summary>
		public string ContentTypeLabel {
			get {
				if ( !string.IsNullOrEmpty( this.TypeKey ) ) {
					if ( ContentTypeManager.ContentTypes.Contains( this.TypeKey ) ) {
						return ( ContentTypeManager.ContentTypes[this.TypeKey].Label );
					}
				}
				return ( "" );
			}
		}

		/// <summary>
		/// Returns true if the story is active & not expired on today's date
		/// </summary>
		public bool IsActive {
			get { return ( ( this.ActiveDate <= DateTime.Today ) && ( DateTime.Today < this.ExpireDate ) ); }
		}

		/// <summary>
		/// Returns true if the story is both approved and active
		/// </summary>
		public bool IsVisible {
			get { return ( this.IsApproved && this.IsActive ); }
		}

		/// <summary>
		/// Returns an IMG tag for the eyeball glyph if the story is approved/active/visible (yes, this is a hack)
		/// </summary>
		public string IsApprovedMark {
			get { return ( this.IsApproved ? "<img src='../images/eyeball.gif' />" : "" ); }
		}

		public string IsActiveMark {
			get { return ( this.IsActive ? "<img src='../images/eyeball.gif' />" : "" ); }
		}

		public string IsVisibleMark {
			get { return ( this.IsVisible ? "<img src='../images/eyeball.gif' />" : "" ); }
		}

		/// <summary>
		/// Active-Date as a string value, for working with UI controls.
		/// Empty (or invalid) strings == DateTime.MinValue
		/// </summary>
		public String ActiveDateStr {
			get {
				if ( this.activeDate == DateTime.MinValue ) {
					return ( "" );
				}
				return ( this.activeDate.ToString( "MM/dd/yyyy" ) );
			}
			set {
				if ( !DateTime.TryParse( value, out this.activeDate ) ) {
					this.activeDate = DateTime.MinValue.Date;
				}
			}
		}

		/// <summary>
		/// Expire-Date as a string value, for working with UI controls.
		/// Empty (or invalid) strings == DateTime.MaxValue
		/// </summary>
		public String ExpireDateStr {
			get {
				if ( this.expireDate >= DateTime.MaxValue.Date ) {
					return ( "" );
				}
				return ( this.expireDate.ToString( "MM/dd/yyyy" ) );
			}
			set {
				if ( !DateTime.TryParse( value, out this.expireDate ) ) {
					this.expireDate = DateTime.MaxValue.Date;
				}
			}
		}

		#endregion
		
		#region Constructor
		
		/// <summary>
		/// Create a new, as yet unpersisted instance
		/// </summary>
		public ContentItem( string typeKey, int currentUserID ) : base() {
			this.createdByUserID = currentUserID;
			this.TypeKey = typeKey;
			this.CreateInitialVersion();
		}
		
		/// <summary>
		/// Restore a previously-persisted instance by ID
		/// </summary>
		/// <param name="ID">Database-record ID of the instance to restore</param>
		public ContentItem( int ID ) : base( ID ) {}

		protected internal ContentItem() : base() { }

		#endregion
		
		#region Methods
		
		#region SqlPersistable overrides
		
		/// <summary>
		/// Persist this instance to the database
		/// </summary>
		public override bool Persist() {
			if ( base.Persist() ) {
				this.PersistVersions();
				ContentPublisher.ResetContentItemCache( this.ID );
				ContentPublisher.ResetSiteTreeCache( ContentPublisher.MainNavTreeKey );
				ContentPublisher.ResetSiteTreeCache( ContentPublisher.AlternateNavTreeKey );
				return ( true );
			}
			return ( false );
		}
		
		/// <summary>
		/// Load this instance from the database
		/// </summary>
		public override void Restore() {
			base.Restore();
			this.RestoreVersions();
		}

		public override bool Delete() {
			if ( base.Delete() ) {
				ContentPublisher.ResetContentItemCache( this.ID );
				ContentPublisher.ResetSiteTreeCache( ContentPublisher.MainNavTreeKey );
				ContentPublisher.ResetSiteTreeCache( ContentPublisher.AlternateNavTreeKey );
				return ( true );
			}
			return ( false );
		}
		#endregion
		
		#region Versions Operations
		
		/// <summary>
		/// Persist this instance's versions
		/// </summary>
		public void PersistVersions() {
			foreach ( ContentItemVersion version in this.Versions ) {
				version.ContentItemID = this.ID; 
				version.Persist();
			}
		}
		
		/// <summary>
		/// Load all the item's related ContentItemVersions (for use by the admin function)
		/// </summary>
		public void RestoreVersions() {
			this.versions = new ContentItemVersionsList( this );
			string sql = "select * from CMS_VERSIONS where cms_item_id = @cms_item_id order by version_number";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@cms_item_id", this.ID ) ) ) {
					ContentItemVersion version = new ContentItemVersion();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, version ) ) {
						version.RestoreParts(); // do we need to do all this right now?
						//this.Versions[version.VersionNumber] = version;
						this.versions.Add( version );
						version = new ContentItemVersion();
					}
				}
			}
		}
		
		/// <summary>
		/// Method to create the first draft version for a new ContentItem
		/// </summary>
		protected ContentItemVersion CreateInitialVersion() {
			ContentItemVersion vers = new ContentItemVersion();
			// Create the specific sequence of Parts needed for this item's ContentType
			// Use the parts-list in the item's ContentItem as a template...
			ContentType type = ContentTypeManager.ContentTypes[ this.TypeKey ];
			// Make a copy of each part in the ContentType for this version
			foreach ( ContentItemPart part in type.Parts ) {
				vers.Parts.Add( new ContentItemPart( part ) );
			}
			this.Versions.Clear(); // being anal again
			this.AddVersion( vers );
			return ( vers );
		}
		
		/// <summary>
		/// Called whenever this item needs to be edited, by creating a new "revision" version (if needed) 
		/// as a copy of the most recent version
		/// </summary>
		public ContentItemVersion CreateNewRevisionVersion() {
			// If we have no versions yet (should never be true, but what the heck?) create one...
			if ( this.Versions.Count == 0 ) {
				return ( this.CreateInitialVersion() );
			}
			// See if we already have an outstanding version under revision ...
			ContentItemVersion vers = this.CurrentRevisionVersion;
			if ( vers != null ) return ( vers );  // We already have a revision-version - let it stand
			// If we made it this far, we simply want to make a copy of the most-recent version as our new revision..
			vers = this.Versions[ this.Versions.MostRecentVersionNumber ];
			// We have our most-recent version - make a copy of it as the new revision
			vers = new ContentItemVersion( vers );
			this.AddVersion( vers );
			return ( vers );
		}
		
		/// <summary>
		/// Append a new ContentItemVersion instance to the end of the list of versions.
		/// </summary>
		/// <param name="version">The new version to append.</param>
		protected void AddVersion( ContentItemVersion version ) {
			//if ( this.ID <= 0 ) this.Persist();
			version.ContentItemID = this.ID;
			if ( version.VersionNumber <= 0 ) {
				version.VersionNumber = this.Versions.NextVersionNumber;
			}
			this.Versions.Add( version );
			//version.Persist();
		}
		
		#endregion
		
		#region XML Rendering
		
		/// <summary>
		/// Returns this part as an XML string
		/// </summary>
		/// <returns>An XML document as a string</returns>
		public string ToXml() {
			if ( this.CurrentActiveVersion == null ) {
				return ( "" );
			}
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			XmlTextWriter writer = new XmlTextWriter( stream, System.Text.Encoding.Default );
			this.WriteXml( writer );
			writer.Flush();
			stream.Position = 0;
			System.IO.StreamReader reader = new System.IO.StreamReader( stream );
			string xml = reader.ReadToEnd();
			writer.Close();
			stream.Close();
			return ( xml );
		}
		
		/// <summary>
		/// Generates this item's XML to a writer (for the ToXml method).
		/// </summary>
		/// <param name="writer">an XmlTextWriter for catching the XML.</param>
		protected internal void WriteXml( XmlTextWriter writer ) {
			if ( this.CurrentActiveVersion == null ) {
				return;
			}
			writer.WriteStartElement( "contentitem");
			// Write the attributes
			writer.WriteAttributeString( "id", this.ID.ToString() );
			writer.WriteAttributeString( "typekey", this.TypeKey );
			writer.WriteAttributeString( "title", this.Title );
			// We probably shouldn't even load it if it's not visible...
			writer.WriteAttributeString( "isvisible", this.IsVisible.ToString() );
			this.CurrentActiveVersion.WriteXml( writer );
			writer.WriteEndElement(); // contentitem
		}
		
		#endregion
		
		#endregion

		#region Static Search Members

		/// <summary>
		/// Node ID for the site-tree node we want to browse
		/// </summary>
		public static int SNodeID {
			get { return ( sNodeID ); }
			set { sNodeID = value; }
		}
		private static int sNodeID = -1;

		public static string STitle {
			get { return ( sTitle ); }
			set { sTitle = value; }
		}
		private static string sTitle = "";

		public static int SCreatedByUserID {
			get { return ( sCreatedByUserID ); }
			set { sCreatedByUserID = value; }
		}
		private static int sCreatedByUserID = -1;

		public static DateTime SCreatedStart {
			get { return ( sCreatedStart ); }
			set { sCreatedStart = value; }
		}
		private static DateTime sCreatedStart = DateTime.MinValue;

		public static DateTime SCreatedEnd {
			get { return ( sCreatedEnd ); }
			set { sCreatedEnd = value; }
		}
		private static DateTime sCreatedEnd = DateTime.MaxValue;
		
		public static int SApprovals {
			get { return ( sApprovals ); }
			set { sApprovals = value; }
		}
		private static int sApprovals = 0;
		// -1 == find unapproved
		//  1 == find approved
		//  0 == ignore


		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			//SNodeID = -1;
			STitle = "";
			SCreatedByUserID = -1;
			SCreatedStart = DateTime.MinValue;
			SCreatedEnd = DateTime.MaxValue;
			SApprovals = 0;
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder sql = new StringBuilder();
			if ( SNodeID > 0 ) {
				sql.Append( "select c.*, n.rank from CMS_ITEMS as c left join CMS_NODE_ITEMS as n on n.cms_item_id = c.id" );
				sql.AppendFormat( " where n.tree_node_id = {0}", SNodeID );
			} else {
				sql.Append( "select c.* from CMS_ITEMS as c  where c.type_key = 'contentitem'" );
				string delim = " and ";
				AddSearchExpr( sql, "c.created_dt", SCreatedStart, SCreatedEnd, ref delim );
				switch ( SApprovals ) {
					case -1: // find unapproved
						AddSearchExpr( sql, "c.is_approved", false, ref delim );
						break;
					case 1: // find approved
						AddSearchExpr( sql, "c.is_approved", true, ref delim );
						break;
				}
				AddSearchExpr( sql, "c.title", STitle, ref delim );
			}
			if ( !string.IsNullOrEmpty( SortExpression ) ) {
				sql.AppendFormat( " order by {0}", SortExpression );
			}
			return ( sql.ToString() );
		}

		public static List<ContentItem> DoSearchList() {
			string sql = GetSearchSql();
			List<ContentItem> results = new List<ContentItem>();
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					ContentItem item = new ContentItem();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						if ( SNodeID > 0 ) {
							item.rank = rdr.GetInt32( rdr.GetOrdinal( "rank" ) );
						}
						results.Add( item );
						item = new ContentItem();
					}
				}
			}
			return ( results );
		}

		#endregion

	}
	
}
