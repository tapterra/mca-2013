using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web;

using StarrTech.WebTools.Data;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// Represents a "photo" content-document, intended for use in a Photo Gallery application
	/// </summary>
	[SqlTable( "CMS_DOCUMENTS", SingularLabel = "Photo", PluralLabel = "Photos", IsMarkedDeleted = true )]
	public class ImageDocument : ContentDocument {

		#region Properties

		/// <summary>
		/// Photo galleries don't use folders, so this property is always equal to -1
		/// </summary>
		public override int FolderNodeID {
			get { return ( -1 ); }
			set { }
		}

		/// <summary>
		/// IDs for the photo-categories this photo is assigned to
		/// </summary>
		public List<int> CategoryIDs {
			get { return ( categoryIDs ); }
		}
		private List<int> categoryIDs = new List<int>();

		/// <summary>
		/// Displayed label for the photo (alias for ContentDocument.Title)
		/// </summary>
		public string Caption {
			get { return ( this.Title ); }
			set { this.Title = value; }
		}

		/// <summary>
		/// Comma-delimited list of acceptable mime-types for posted files (see HtmlInputFile.Accept).
		/// </summary>
		public override string AcceptedTypes {
			get { return ( "image/*" ); }
			set { }
		}

		#region Publishing

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static new string UrlFormat = "ImageDocument.axd?id={0}&sc={1}&mw={2}&mh={3}&ax={4}";

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static new string DownloadUrlFormat = "ImageDocument.axd?id={0}&ax=1";

		/// <summary>
		/// String.Format template for this (image) document's display URL.
		/// </summary>
		public static string ImageUrlFormat = "ImageDocument.axd?id={0}";

		/// <summary>
		/// String.Format template (with scaling) for this document's thumbnail display URL.
		/// </summary>
		public static string ThumbnailUrlFormat = "ImageDocument.axd?id={0}&mw={1}&mh={1}";

		/// <summary>
		/// Returns the URL required to download this document (via the CMSDocumentHandler).
		/// Note: Assumes that the following handler is installed in the &lt;httpHandlers&gt; section of web.config:
		/// &lt;add verb="GET" path="ImageDocument.axd" type="StarrTech.WebTools.CMS.ImageDocumentHandler, StarrTech.WebTools" /&gt;
		/// </summary>
		public override string DownloadUrl {
			get { return ( string.Format( ImageDocument.DownloadUrlFormat, this.ID ) ); }
		}

		/// <summary>
		/// Returns the URL used to display a scaled thumbnail version of this (image) document
		/// </summary>
		public string ThumbnailUrl {
			get { return ( string.Format( ImageDocument.ThumbnailUrlFormat, this.ID, "100" ) + "&ib=1" ); }
		}

		public string GridThumbUrl {
			get { return ( string.Format( ImageDocument.ThumbnailUrlFormat, this.ID, "32" ) ); }
		}

		/// <summary>
		/// Returns the URL used to display a full-scale version of this (image) document
		/// </summary>
		public string ImageUrl {
			get { return ( string.Format( ImageDocument.ImageUrlFormat, this.ID ) ); }
		}

		/// <summary>
		/// Generates an appropriate ImageFormat object for this document's Mime Type (null for non-image types)
		/// </summary>
		public ImageFormat ImageFormat {
			get {
				switch ( this.MimeType ) {
					case MimeTypes.ImageBmp:
						return ( ImageFormat.Bmp );
					case MimeTypes.ImageGif:
						return ( ImageFormat.Gif );
					case MimeTypes.ImageJpeg:
					case "image/pjpeg":
						return ( ImageFormat.Jpeg );
					case MimeTypes.ImagePng:
						return ( ImageFormat.Png );
					case MimeTypes.ImageEmf:
						return ( ImageFormat.Emf );
					case MimeTypes.ImageTiff:
						return ( ImageFormat.Tiff );
					case MimeTypes.ImageExif:
						return ( ImageFormat.Exif );
					case MimeTypes.ImageWmf:
						return ( ImageFormat.Wmf );
					default:
						return ( null );
				}
			}
		}

		/// <summary>
		/// Returns true if the MimeType is that of an image/graphic.
		/// </summary>
		public override bool IsImage {
			get { return ( this.ImageFormat != null ); }
		}

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Create a new, empty instance
		/// </summary>
		public ImageDocument() : base() { }

		/// <summary>
		/// Restore a previously-persisted instance by ID
		/// </summary>
		/// <param name="ID">Database-record ID of the instance to restore</param>
		public ImageDocument( int ID ) : base( ID ) { }

		/// <summary>
		/// Instantiate from a typical form postback
		/// </summary>
		/// <param name="title">User-defined caption for the document</param>
		/// <param name="description">User-defined description</param>
		/// <param name="comments">User-defined admin comments</param>
		/// <param name="postedFile">Uploaded file object</param>
		public ImageDocument( string title, string description, string comments, HttpPostedFile postedFile )
			: base() {
			this.Title = title;
			this.Description = description;
			this.Comments = comments;
			this.LoadPostedFile( postedFile );
		}

		#endregion

		#region Publishing methods

		/// <summary>
		/// Write the document to the given HttpResponse, with an attachment-header if indicated,
		/// and scaled to a given percentage, maximum width, and/or max height (if the document is an image).
		/// </summary>
		/// <param name="response">The HttpResponse instance to write to</param>
		/// <param name="AsAttachment">A switch ti indicate the need for an attachment-header</param>
		/// <param name="ScalePct">An integer percentage-value to scale the image by.</param>
		/// <param name="MaxWidth">An integer giving the maximum width in pixels desired (after scaling).</param>
		/// <param name="MaxHeight">An integer giving the maximum height in pixels desired (after scaling).</param>
		/// <param name="Border">A boolean that specifies the need for a snapshot-border around the thumbnail.</param>
		public void DownloadDocument( HttpResponse Response, bool AsAttachment, int ScalePct, int MaxWidth, int MaxHeight ) {
			this.DownloadDocument( Response, AsAttachment, ScalePct, MaxWidth, MaxHeight, false );
		}
		public void DownloadDocument( HttpResponse Response, bool AsAttachment, int ScalePct, int MaxWidth, int MaxHeight, bool Border ) {
			Response.Clear();
			Response.ContentType = this.MimeType;
			if ( this.MimeType == "image/pjpeg" ) {
				Response.ContentType = "image/jpeg";
			}
			if ( ( ScalePct == 0 || ScalePct == 100 ) && MaxWidth == 0 && MaxHeight == 0 && !Border ) {
				// Send the document as-is
				if ( AsAttachment ) {
					Response.AppendHeader( "Content-Disposition", string.Format( "attachment; filename={0}", this.FileName ) );
				}
				Response.AppendHeader( "Content-Length", this.DataSize.ToString() );
				Response.BinaryWrite( this.DocumentData );
			} else {
				// Redraw the image
				this.DrawThumbnail( Response, ScalePct, MaxWidth, MaxHeight, Border );
			}
			Response.Flush();
		}
		
		/// <summary>
		/// Scales the image document into a thumbnail
		/// </summary>
		/// <param name="ScalePct">An integer percentage-value to scale the image by.</param>
		/// <param name="MaxWidth">An integer giving the maximum width in pixels desired (after scaling).</param>
		/// <param name="MaxHeight">An integer giving the maximum height in pixels desired (after scaling).</param>
		/// <param name="Border">A boolean that specifies the need for a snapshot-border around the thumbnail.</param>
		public void DrawThumbnail( HttpResponse Response, int ScalePct, int MaxWidth, int MaxHeight, bool Border ) {
			// This only works for image documents
			if ( this.ImageFormat == null ) { return; }
			double Scale = ( ScalePct == 0 ? 100 : ScalePct ) / 100D;
			// Load the main image
			using ( MemoryStream rawStream = new MemoryStream() ) {
				rawStream.Write( this.DocumentData, 0, this.DataSize );
				using ( Image pic = Image.FromStream( rawStream ) ) {
					// Compute the new dimensions
					int ht = Convert.ToInt32( pic.Height * Scale );
					int wd = Convert.ToInt32( pic.Width * Scale );
					if ( MaxWidth > 0 ) {
						//if ( wd > MaxWidth ) {
						double wScale = Convert.ToDouble( MaxWidth ) / Convert.ToDouble( wd );
						wd = Convert.ToInt32( wd * wScale );
						ht = Convert.ToInt32( ht * wScale );
						//}
					}
					if ( MaxHeight > 0 ) {
						//if ( ht > MaxHeight ) {
						double hScale = Convert.ToDouble( MaxHeight ) / Convert.ToDouble( ht );
						wd = Convert.ToInt32( wd * hScale );
						ht = Convert.ToInt32( ht * hScale );
						//}
					}
					if ( MaxWidth > 0 && MaxHeight > 0 ) {
						double wScale = Convert.ToDouble( MaxWidth ) / Convert.ToDouble( wd );
						double hScale = Convert.ToDouble( MaxHeight ) / Convert.ToDouble( ht );
						// take the smallest scale percentage that will fit in canvas
						double scale = wScale > hScale ? hScale : wScale;
						wd = Convert.ToInt32( wd * scale );
						ht = Convert.ToInt32( ht * scale );
					}
					// Set up the canvas
					using ( Bitmap bmp = new Bitmap( wd, ht ) ) {
						using ( Graphics graf = Graphics.FromImage( bmp ) ) {
							graf.Clear( Color.White );
							if ( Border ) {
								graf.DrawRectangle( new Pen( Color.LightGray, 1 ), 0, 0, wd - 1, ht - 1 );
							}
							graf.SmoothingMode = SmoothingMode.HighQuality;
							graf.InterpolationMode = InterpolationMode.HighQualityBicubic;
							graf.PixelOffsetMode = PixelOffsetMode.HighQuality;
							// Draw the thumbnail
							Rectangle destRect;
							if ( Border ) {
								destRect = new Rectangle( 2, 2, wd - 4, ht - 4 );
							} else {
								destRect = new Rectangle( 0, 0, wd, ht );
							}
							Rectangle srcRect = new Rectangle( 0, 0, pic.Width, pic.Height );
							graf.DrawImage( pic, destRect, srcRect, GraphicsUnit.Pixel );
						}
						// Write the image & clean up
						bmp.Save( Response.OutputStream, this.ImageFormat );
					}
				}
			}
			Response.Flush();
			//Response.End();
		}

		/// <summary>
		/// Scales the image document into a thumbnail and draws it inside the "photo album" background
		/// </summary>
		/// <param name="ScalePct">An integer percentage-value to scale the image by.</param>
		/// <param name="MaxWidth">An integer giving the maximum width in pixels desired (after scaling).</param>
		/// <param name="MaxHeight">An integer giving the maximum height in pixels desired (after scaling).</param>
		public void DrawAlbumThumb( HttpResponse Response ) {
			// This only works for image documents
			if ( this.ImageFormat == null ) { return; }
			// Dimensions of the album background (overall size of our output image)
			const int bgWd = 136;
			const int bgHt = 106;
			// Maximum dimensions of our thumbnail overlay (including border)
			const int maxWd = 100;
			const int maxHt = 75;
			// Load the main image
			using ( MemoryStream rawStream = new MemoryStream() ) {
				rawStream.Write( this.DocumentData, 0, this.DataSize );
				using ( Image pic = Image.FromStream( rawStream ) ) {
					// Compute the new dimensions
					int ht = pic.Height;
					int wd = pic.Width;
					if ( wd > maxWd ) {
						double wScale = Convert.ToDouble( maxWd ) / Convert.ToDouble( wd );
						wd = Convert.ToInt32( wd * wScale );
						ht = Convert.ToInt32( ht * wScale );
					}
					if ( ht > maxHt ) {
						double hScale = Convert.ToDouble( maxHt ) / Convert.ToDouble( ht );
						wd = Convert.ToInt32( wd * hScale );
						ht = Convert.ToInt32( ht * hScale );
					}
					// Set up the canvas
					using ( Bitmap bmp = new Bitmap( bgWd, bgHt ) ) {
						using ( Graphics graf = Graphics.FromImage( bmp ) ) {
							graf.SmoothingMode = SmoothingMode.HighQuality;
							graf.InterpolationMode = InterpolationMode.HighQualityBicubic;
							graf.PixelOffsetMode = PixelOffsetMode.HighQuality;
							graf.Clear( Color.White );
							// Draw the background-image
							string bgFile = SitePrefs.CheckPref( "ALBUM_BACKGROUND_IMAGE_URL", "~/images/album-background.gif" );
							using ( Image bgPic = System.Drawing.Image.FromFile( HttpContext.Current.Server.MapPath( bgFile ) ) ) {
								Rectangle bgRect = new Rectangle( 0, 0, bgWd, bgHt );
								graf.DrawImage( bgPic, bgRect, bgRect, GraphicsUnit.Pixel );
							}
							// Draw the photo-frame
							Rectangle destRect = new Rectangle( 0, 0, wd - 1, ht - 1 );
							destRect.Offset( ( bgWd + 4 - wd ) / 2, ( bgHt - 6 - ht ) / 2 );
							graf.FillRectangle( new SolidBrush( Color.White ), destRect );
							//graf.DrawRectangle( new Pen( Color.Black, 1 ), destRect );
							// Draw the thumbnail
							destRect.Inflate( -2, -2 );
							Rectangle srcRect = new Rectangle( 0, 0, pic.Width, pic.Height );
							graf.DrawImage( pic, destRect, srcRect, GraphicsUnit.Pixel );
						}
						// Write the image & clean up
						bmp.Save( Response.OutputStream, this.ImageFormat );
					}
				}
			}
			Response.Flush();
			Response.End();
		}

		#endregion

		#region SqlPersistable Overrides

		/// <summary>
		/// Verify that it's OK to persist this User
		/// Overridden to verify that the document is a valid image
		/// </summary>
		/// <returns>True if it's OK to persist</returns>
		public override bool IsValidToPersist() {
			this.ValidationMessages.Clear();
			bool isValid = this.MimeType.StartsWith( "image/" );
			if ( !isValid ) {
				this.ValidationMessages.Add( "The file you selected to upload is not a valid image type." );
				if ( this.ID > 0 ) {
					this.Restore();
				}
				return ( false );
			}
			return ( base.IsValidToPersist() );
		}

		/// <summary>
		/// Saves this instance to the database
		/// Overridden to handle persisting the category-assignment records
		/// </summary>
		/// <returns>True if the record was persisted successfully</returns>
		public override bool Persist() {
			if ( this.IsValidToPersist() ) {
				if ( base.Persist() ) {
					this.PersistCategoryIDs();
					return ( true );
				}
				return ( false );
			}
			return ( false );
		}

		/// <summary>
		/// Restores this instance from it's database record.
		/// Overridden to restore the Category assignments.
		/// </summary>
		public override void Restore() {
			base.Restore();
			this.RestoreCategoryIDs();
		}

		/// <summary>
		/// Removes the record for this instance from the database.
		/// Overridden to clean up the Category assignments.
		/// </summary>
		/// <returns>True if the record was successfully deleted.</returns>
		public override bool Delete() {
			if ( this.IsValidToDelete() ) {
				// Delete any existing Category assignments first
				this.DeleteCategoryIDs();
				return ( base.Delete() );
			}
			return ( false );
		}

		#endregion

		#region Private methods for working with Category-assignments

		/// <summary>
		/// Save the CategoryIDs list for this photo to the database
		/// </summary>
		protected void PersistCategoryIDs() {
			if ( this.ID <= 0 ) { // Make sure we have a valid ID
				return;
			}
			// We can't just delete & recreate the links because we don't want to destroy the category-link sequence numbers
			// First, we need to see what category-IDs we were previously linked to...
			List<int> oldCatIDs = this.LoadCategoryIDs();
			using ( SqlDatabase db = new SqlDatabase() ) {
				// Step over each of our current links and see if we need to add any to the database...
				string sql = "insert into PHOTO_CAT_LINKS ( cms_document_id, category_id, seq ) values ( @cms_document_id, @category_id, 999 ) ";
				// For new links, the sequence number will be set to 999, so they'll be added to the end of the list
				foreach ( int catID in this.CategoryIDs ) {
					if ( !oldCatIDs.Contains( catID ) ) {
						// we've got a new category...
						db.Execute( sql, db.NewParameter( "@cms_document_id", this.ID ), db.NewParameter( "@category_id", catID ) );
					}
				}
				// Step over each of our old links to see if they need to be deleted from the database...
				sql = "delete from PHOTO_CAT_LINKS where cms_document_id = @cms_document_id and category_id = @category_id";
				foreach ( int catID in oldCatIDs ) {
					if ( !this.CategoryIDs.Contains( catID ) ) {
						// we've dropped this category...
						db.Execute( sql, db.NewParameter( "@cms_document_id", this.ID ), db.NewParameter( "@category_id", catID ) );
					}
				}
			}
		}

		/// <summary>
		/// Load the CategoryIDs list for this photo's Category assignments
		/// </summary>
		protected void RestoreCategoryIDs() {
			this.categoryIDs = LoadCategoryIDs();
		}

		private List<int> LoadCategoryIDs() {
			List<int> results = new List<int>();
			if ( this.ID > 0 ) {  // Make sure we have a valid ID
				string sql = "select category_id from PHOTO_CAT_LINKS where cms_document_id = @cms_document_id ";
				using ( SqlDatabase db = new SqlDatabase() ) {
					using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@cms_document_id", this.ID ) ) ) {
						while ( rdr.Read() ) {
							results.Add( rdr.GetInt32( 0 ) );
						}
					}
				}
			}
			return ( results );
		}

		/// <summary>
		/// Delete all the Category links for this photo
		/// Overloaded to allow the records to be deleted without clearing the CategoryIDs collection.
		/// </summary>
		/// <param name="ClearList">Pass false to delete the records without clearing the CategoryIDs</param>
		protected void DeleteCategoryIDs() {
			this.DeleteCategoryIDs( true );
		}
		protected void DeleteCategoryIDs( bool ClearList ) {
			if ( this.ID <= 0 ) { return; }// Make sure we have a valid ID
			string sql = "delete from PHOTO_CAT_LINKS where cms_document_id = @cms_document_id ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql, db.NewParameter( "@cms_document_id", this.ID ) );
			}
			if ( ClearList ) {
				this.CategoryIDs.Clear();
			}
		}

		#endregion

		#region Static Search Members

		/// <summary>
		/// Photo Category ID
		/// </summary>
		public static int SCategoryID {
			get { return ( sCategoryID ); }
			set { sCategoryID = value; }
		}
		private static int sCategoryID = -1;

		/// <summary>
		/// Photo Caption substring
		/// </summary>
		public static string SCaption {
			get { return ( ImageDocument.sCaption ); }
			set { ImageDocument.sCaption = value; }
		}
		private static string sCaption = "";

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static new void NewSearch() {
			ImageDocument.SCategoryID = -1;
			ImageDocument.SCaption = "";
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static new string GetSearchSql() {
			return ( ImageDocument.GetSearchSql( ImageDocument.SCategoryID, ImageDocument.SCaption ) );
		}
		public static string GetSearchSql( int categoryID, string caption ) {
			if ( string.IsNullOrEmpty( ImageDocument.SortExpression ) ) {
				ImageDocument.SortExpression = "title";
			}
			return ( ImageDocument.GetSearchSql( categoryID, caption, ImageDocument.SortExpression ) );
		}
		public static string GetSearchSql( int categoryID, string caption, string sortExpr ) {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "title", caption, ref delim );
			if ( categoryID > 0 ) {
				AddSearchExpr( whereBuilder, "category_id", categoryID, ref delim );
			}
			// No deleted records
			AddSearchExpr( whereBuilder, "is_deleted", 0, ref delim );
			// Limit the results to image mimetypes
			AddSearchExpr( whereBuilder, "mime_type", "image", ref delim );

			StringBuilder sql = new StringBuilder();
			sql.AppendFormat( "select {0}", SqlDatabase.GetFieldList( typeof( ImageDocument ) ) );
			sql.Append( " from CMS_DOCUMENTS" );
			if ( categoryID > 0 ) {
				sql.Append( " left join PHOTO_CAT_LINKS on cms_document_id = id" );
			} else {
				if ( sortExpr == "seq" ) {
					sortExpr = "";
				}
			}
			if ( whereBuilder.Length != 0 ) {
				sql.AppendFormat( " where ( {0} )", whereBuilder );
			}
			if ( sortExpr != "" ) {
				sql.AppendFormat( " order by {0}", sortExpr );
			}
			return ( sql.ToString() );
		}

		/// <summary>
		/// Execute the search based on the current values of the static search properties.
		/// </summary>
		public static new List<ImageDocument> DoSearchList() {
			using ( SqlDatabase db = new SqlDatabase() ) {
				return ( SqlDatabase.DoSearch<ImageDocument>( ImageDocument.GetSearchSql() ) );
			}
		}

		#endregion

	}

}
