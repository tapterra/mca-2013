using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// HttpHandler for accessing image documents (photos) stored in the database (via ImageDocument).
	/// Note: To install, add the following tag to the &lt;httpHandlers&gt; section of web.config:
	/// &lt;add verb="GET" path="ImageDocument.axd" type="StarrTech.WebTools.CMS.ImageDocumentHandler, StarrTech.WebTools2" /&gt;
	/// To generate a compatible URL, use ImageDocument.DownloadUrlFormat or ImageDocument.DownloadUrl
	/// </summary>
	public class ImageDocumentHandler : IHttpHandler {

		public bool IsReusable {
			get { return ( false ); }
		}

		/// <summary>
		/// Process the request
		/// </summary>
		/// <param name="context">The current HTTP context, from which we get to access the Request and the Response.</param>
		public virtual void ProcessRequest( HttpContext context ) {
			// Parse the query string 
			string idStr = context.Request.QueryString["id"];
			string scaleStr = context.Request.QueryString["sc"];
			string widthStr = context.Request.QueryString["mw"];
			string heightStr = context.Request.QueryString["mh"];
			string attachStr = context.Request.QueryString["ax"];
			string borderStr = context.Request.QueryString["ib"];
			if ( !string.IsNullOrEmpty( idStr ) ) {
				// Determine the proper parameter values
				int id = 0;
				Int32.TryParse( idStr, out id );
				if ( id <= 0 ) { return; }
				int scale = 0;
				Int32.TryParse( scaleStr, out scale );
				int width = 0;
				Int32.TryParse( widthStr, out width );
				int height = 0;
				Int32.TryParse( heightStr, out height );
				bool border = false;
				if ( !string.IsNullOrEmpty( borderStr ) ) {
					border = ( borderStr == "1" );
				}
				// Load the photo from the database
				ImageDocument photo = new ImageDocument( id );
				// Is this an attachment?
				bool attach = false;
				if ( string.IsNullOrEmpty( attachStr ) ) {
					attach = !photo.IsImage;
				} else {
					attach = ( attachStr == "1" );
				}
				// Fetch the bits
				photo.DownloadDocument( context.Response, attach, scale, width, height, border );
			}
		}

	}

}
