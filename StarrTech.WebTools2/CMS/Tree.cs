using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;
using System.Xml;

namespace StarrTech.WebTools.CMS {

	/// <summary>
	/// An ISqlPersistable implementation of the CMS Tree (CMS_TREES)
	/// </summary>
	[SqlTable( "CMS_TREES", SingularLabel = "Tree", PluralLabel = "Trees" )]
	public class Tree : BaseTreeNode {

		#region Properties
		
		/// <summary>
		/// User-defined key for this tree
		/// </summary>
		[SqlColumn( "tree_key", DbType.String, Length = 50 )]
		public string TreeKey {
			get { return ( this.treekey ); }
			set { this.treekey = value; }
		}
		private string treekey = "";
		
		public override int TreeID {
			get { return ( this.ID ); }
			set { this.ID = value; }
		}

		/// <summary>
		/// Switch to determine if this node should be added to the menu as a menu item.
		/// Always true
		/// </summary>
		public override bool IncludeAsMenuItem {
			get { return ( true ); }
			set { }
		}

		/// <summary>
		/// Switch to determine if the children of this node should be added to the menu as a sub/popup menu.
		/// Always true
		/// </summary>
		public override bool IncludeSubnodeMenuItems {
			get { return ( true ); }
			set { }
		}
		
		#endregion
		
		#region Constructors
		
		// Access the base constructors
		public Tree() : base() {}
		public Tree( int ID ) : base( ID ) {}
		
		/// <summary>
		/// Restore a persisted instance by key.
		/// </summary>
		/// <param name="key">"tree_key" value for the tree to restore.</param>
		public Tree( string key ) : base() {
			this.TreeKey = key;
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, 
					"select * from CMS_TREES where tree_key = @tree_key ",
					db.NewParameter( "@tree_key", key )	
				);
			}
			this.RestoreNodes();
		}
		
		#endregion
		
		#region Public Methods
		
		public string ToXml() {
			return ( this.ToXml( false ) );
		}
		public string ToXml( bool IncludeItems ) {
			HttpContext.Current.Trace.Warn( ">>> Tree.ToXml" );
			string xml = "";
			using ( System.IO.MemoryStream stream = new System.IO.MemoryStream() ) {
				using ( XmlTextWriter writer = new XmlTextWriter( stream, System.Text.Encoding.Default ) ) {
					writer.WriteStartElement( "tree", "" );
					writer.WriteAttributeString( "id", this.ID.ToString() );
					writer.WriteAttributeString( "key", this.TreeKey );
					// Write out all the child nodes
					foreach ( TreeNode node in this ) {
						node.WriteXml( writer, IncludeItems );
					}
					writer.WriteEndElement();
					writer.Flush();
					// Writing the XML is done, read it back out
					stream.Position = 0;
					System.IO.StreamReader reader = new System.IO.StreamReader( stream );
					xml = reader.ReadToEnd();
				}
			}
			HttpContext.Current.Trace.Warn( "<<< Tree.ToXml" );
			return ( xml );
		}
		
		#endregion
		
		#region Protected Methods
		
		// Query the database and retrieve all nodes for this tree
		protected void RestoreNodes() {
			HttpContext.Current.Trace.Warn( ">>> Tree.RestoreNodes" );
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			string sql = "select * from CMS_TREE_NODES where tree_id = @tree_id ";
			DataTable nodesTable;
			using ( SqlDatabase db = new SqlDatabase() ) {
				nodesTable = db.GetDataTable( sql, db.NewParameter( "@tree_id", this.ID ) );
			}
			//DataRow[] rows = nodesTable.Select( string.Format( "parent_node_id = {0}", 0 ) );
			DataRow[] rows = nodesTable.Select( "parent_node_id = 0" );
			foreach ( DataRow row in rows ) {
				TreeNode node = new TreeNode();
				if ( SqlDatabase.InstantiateFromDataRow( row, node ) ) {
					this.Nodes.Add( node );
					node.RestoreNodes( nodesTable );
					node = new TreeNode();
				}
			}
			HttpContext.Current.Trace.Warn( "<<< Tree.RestoreNodes" );
		}
		
		#endregion

	}

}
