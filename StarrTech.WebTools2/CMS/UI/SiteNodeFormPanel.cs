using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.TreeUI;

namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Editing panel for nodes on the CMS SiteEditor.
	/// </summary>
	public class SiteNodeFormPanel : NodeFormPanel {

		#region  Properties

		public override string PathKey {
			get { return ( this.PathKeyBox.Text.ToLower() ); }
			set { this.PathKeyBox.Text = value.ToLower(); }
		}

		public override string Title {
			get { return ( this.TitleBox.Text ); }
			set { this.TitleBox.Text = value; }
		}

		public override string Comments {
			get { return ( this.CommentsBox.Text ); }
			set { this.CommentsBox.Text = value; }
		}

		public override string Description {
			get { return ( this.DescriptionBox.Text ); }
			set { this.DescriptionBox.Text = value; }
		}

		#region Permissions Properties

		public bool CanAddContent {
			get {
				object obj = this.ViewState["CanAddContent"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanAddContent"] = value; }
		}

		public bool CanEditContent {
			get {
				object obj = this.ViewState["CanEditContent"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanEditContent"] = value; }
		}

		public bool CanDeleteContent {
			get {
				object obj = this.ViewState["CanDeleteContent"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanDeleteContent"] = value; }
		}

		public bool CanApproveContent {
			get {
				object obj = this.ViewState["CanApproveContent"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanApproveContent"] = value; }
		}

		#endregion

		#region Components

		public LabeledTextBox PathKeyBox {
			get {
				this.EnsureChildControls();
				return ( this.pathKeyBox );
			}
		}
		private LabeledTextBox pathKeyBox;

		private RequiredFieldValidator pathKeyBoxReqValidator;
		private RegularExpressionValidator pathKeyBoxRegexValidator;

		public LabeledTextBox TitleBox {
			get {
				this.EnsureChildControls();
				return ( this.titleBox );
			}
		}
		private LabeledTextBox titleBox;

		private RequiredFieldValidator titleBoxReqValidator;

		public LabeledTextBox CommentsBox {
			get {
				this.EnsureChildControls();
				return ( this.commentsBox );
			}
		}
		private LabeledTextBox commentsBox;

		public CheckBox IsVisibleXBox {
			get {
				this.EnsureChildControls();
				return ( this.isVisibleXBox );
			}
		}
		private CheckBox isVisibleXBox;

		public LabeledDropDownList ContentTypeListBox {
			get {
				this.EnsureChildControls();
				return ( this.contentTypeListBox );
			}
		}
		private LabeledDropDownList contentTypeListBox;

		public LabeledTextBox DescriptionBox {
			get {
				this.EnsureChildControls();
				return ( this.descriptionBox );
			}
		}
		private LabeledTextBox descriptionBox;

		public CheckBox ItemOnMainMenuXBox {
			get {
				this.EnsureChildControls();
				return ( this.itemOnMainMenuXBox );
			}
		}
		private CheckBox itemOnMainMenuXBox;

		public CheckBox SubnodesOnMainMenuXBox {
			get {
				this.EnsureChildControls();
				return ( this.subnodesOnMainMenuXBox );
			}
		}
		private CheckBox subnodesOnMainMenuXBox;

		#endregion

		#endregion

		#region Constructors

		public SiteNodeFormPanel()
			: base() {
			this.CellSpacing = 0;
			this.CellPadding = 3;
		}

		#endregion

		#region Methods

		protected override void CreateChildControls() {

			#region FormController
			this.formController = new NodeFormController();
			this.formController.Width = new Unit( "100%" );
			this.formController.ButtonClick += new CommandEventHandler( this.NodeFormCommands );
			#endregion

			#region PathKeyBox
			this.pathKeyBox = new LabeledTextBox();
			this.pathKeyBox.Label = "Path Key";
			this.pathKeyBox.Width = new Unit( "100%" );
			this.pathKeyBox.ID = "NodeFormPanel_PathKeyBox"; // Required for validators
			this.pathKeyBoxReqValidator = new RequiredFieldValidator();
			this.pathKeyBoxReqValidator.ControlToValidate = this.pathKeyBox.ID;
			this.pathKeyBoxReqValidator.Display = ValidatorDisplay.None;
			this.pathKeyBoxReqValidator.ErrorMessage = "Please enter a Path Key value for this section.";

			this.pathKeyBoxRegexValidator = new RegularExpressionValidator();
			this.pathKeyBoxRegexValidator.ControlToValidate = this.pathKeyBox.ID;
			this.pathKeyBoxRegexValidator.Display = ValidatorDisplay.None;
			this.pathKeyBoxRegexValidator.ErrorMessage = "The Path Key value must contain only letters and numbers.";
			this.pathKeyBoxRegexValidator.ValidationExpression = "[0-9a-zA-Z]+";
			#endregion

			#region TitleBox
			this.titleBox = new LabeledTextBox();
			this.titleBox.Label = "Section Title";
			this.titleBox.Width = new Unit( "100%" );
			this.titleBox.ID = "NodeFormPanel_TitleBox"; // Required for validators

			this.titleBoxReqValidator = new RequiredFieldValidator();
			this.titleBoxReqValidator.ControlToValidate = this.titleBox.ID;
			this.titleBoxReqValidator.Display = ValidatorDisplay.None;
			this.titleBoxReqValidator.ErrorMessage = "Please enter a title for this section.";
			#endregion

			#region CommentsBox
			this.commentsBox = new LabeledTextBox();
			this.commentsBox.Label = "Comments";
			this.commentsBox.Width = new Unit( "100%" );
			this.commentsBox.TextMode = TextBoxMode.MultiLine;
			this.commentsBox.Rows = 4;
			this.commentsBox.Wrap = true;
			#endregion

			#region IsVisibleXBox
			this.isVisibleXBox = new CheckBox();
			this.isVisibleXBox.Text = "Approved?";
			this.isVisibleXBox.CssClass = "xbox";
			#endregion

			#region ContentTypeListBox
			this.contentTypeListBox = new LabeledDropDownList();
			this.contentTypeListBox.Label = "Section/Content Type";
			this.contentTypeListBox.Width = new Unit( "100%" );
			#endregion

			#region ItemOnMainMenuXBox
			this.itemOnMainMenuXBox = new CheckBox();
			this.itemOnMainMenuXBox.Text = "Appears&nbsp;on&nbsp;Main&nbsp;Menu?";
			this.itemOnMainMenuXBox.CssClass = "xbox";
			#endregion

			#region SubnodesOnMainMenuXBox
			this.subnodesOnMainMenuXBox = new CheckBox();
			this.subnodesOnMainMenuXBox.Text = "Subsections Appear on Main Menu?";
			this.subnodesOnMainMenuXBox.CssClass = "xbox";
			#endregion

			#region ContentPanel
			this.contentPanel = new Panel();
			this.contentPanel.Width = new Unit( "100%" );
			this.contentPanel.Height = new Unit( "100%" );
			this.contentPanel.BackColor = Color.White;
			this.contentPanel.BorderColor = Color.LightGray;
			this.contentPanel.BorderStyle = BorderStyle.Solid;
			this.contentPanel.BorderWidth = 1;
			this.contentPanel.Style["padding"] = "10px";
			#endregion

			#region DescriptionBox
			this.descriptionBox = new LabeledTextBox();
			this.descriptionBox.Label = "Description";
			this.descriptionBox.Width = new Unit( "100%" );
			this.descriptionBox.TextMode = TextBoxMode.MultiLine;
			this.descriptionBox.Rows = 4;
			this.descriptionBox.Wrap = true;
			this.descriptionBox.Enabled = false;
			this.descriptionBox.Frame.FrameStyle.CellPadding = 10;
			#endregion

			#region Stack it up...
			this.Rows.Clear();
			this.mainFormTable = new Table();
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.ColumnSpan = 3;
			cell.Controls.Add( this.formController );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.pathKeyBox );
			cell.Controls.Add( this.pathKeyBoxReqValidator );
			cell.Controls.Add( this.pathKeyBoxRegexValidator );
			cell.Width = new Unit( "25%" );
			row.Cells.Add( cell );
			cell = new TableCell();
			cell.Width = new Unit( "65%" );
			cell.Controls.Add( this.titleBox );
			cell.Controls.Add( this.titleBoxReqValidator );
			row.Cells.Add( cell );
			cell = new TableCell();
			cell.Width = new Unit( "15%" );
			cell.HorizontalAlign = HorizontalAlign.Left;
			cell.VerticalAlign = VerticalAlign.Bottom;
			cell.Style[HtmlTextWriterStyle.PaddingBottom] = "6px";
			cell.Controls.Add( this.isVisibleXBox );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.HorizontalAlign = HorizontalAlign.Left;
			cell.VerticalAlign = VerticalAlign.Bottom;
			cell.Controls.Add( this.itemOnMainMenuXBox );
			row.Cells.Add( cell );
			cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.HorizontalAlign = HorizontalAlign.Left;
			cell.VerticalAlign = VerticalAlign.Bottom;
			cell.Controls.Add( this.subnodesOnMainMenuXBox );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.ColumnSpan = 3;
			cell.Controls.Add( this.commentsBox );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.contentTypeListBox );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.ColumnSpan = 3;
			cell.Controls.Add( this.descriptionBox );
			row.Cells.Add( cell );
			this.mainFormTable.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.mainFormTable );
			row.Cells.Add( cell );
			this.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.contentPanel );
			row.Cells.Add( cell );
			this.Rows.Add( row );

			#endregion
		}

		protected override void OnLoad( EventArgs e ) {
			base.OnLoad( e );
			this.PathKeyBox.Focus();
		}

		#endregion

	}

}
