using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml;

using StarrTech.WebTools.Controls;

namespace StarrTech.WebTools.CMS.UI {
	
	public class BreadCrumbPanel : Panel {

		#region Properties

		public string TreeKey {
			get {
				object obj = this.ViewState["TreeKey"];
				if ( obj != null ) {
					return ( (string) obj );
				}
				string key = this.Page.Request.QueryString[ContentPublisher.TreeQKey];
				if ( !string.IsNullOrEmpty( key ) ) {
					string[] keys = key.Split( ',' );
					return ( keys[0] );
				}
				return ( "" );
			}
			set { this.ViewState["TreeKey"] = value; }
		}

		public int SectionNodeID {
			get {
				object obj = this.ViewState["SectionNodeID"];
				if ( obj != null ) {
					return ( (int) obj );
				}
				string id = this.Page.Request.QueryString[ContentPublisher.NodeQKey];
				if ( !string.IsNullOrEmpty( id ) ) {
					string[] ids = id.Split( ',' );
					return ( Convert.ToInt32( ids[0] ) );
				}
				return ( -1 );
			}
			set { this.ViewState["SectionNodeID"] = value; }
		}

		public string Separator {
			get { return ( this.separator ); }
			set { this.separator = value; }
		}
		private string separator = " : ";

		public bool AutoGenerateSiteTreeLinks {
			get { return ( this.autoGenerateSiteTreeLinks ); }
			set { this.autoGenerateSiteTreeLinks = value; }
		}
		private bool autoGenerateSiteTreeLinks = true;

		public int MaxTitleLength {
			get {
				object obj = this.ViewState["MaxTitleLength"];
				return ( obj == null ? 50 : (int) obj );
			}
			set { this.ViewState["MaxTitleLength"] = value; }
		}
		
		private int partNo = 0;

		public string ItemCssClass {
			get { return ( this.itemCssClass ); }
			set { this.itemCssClass = value; }
		}
		private string itemCssClass = "";

		public string PageItemLabel {
			get { return ( this.pageItemLabel ); }
			set { this.pageItemLabel = value; }
		}
		private string pageItemLabel = "";

		#endregion

		public void Clear() {
			this.Controls.Clear();
			this.partNo = 0;
		}

		private string TrimTitle( string title ) {
			if ( title.Length > this.MaxTitleLength ) {
				int len = this.MaxTitleLength - 3;
				while ( title[len] != ' ' ) {
					len -= 1;
				}
				return ( title.Substring( 0, len ) + "..." );
			}
			return ( title );
		}

		public void AddLink( string label, string url ) {
			HyperLink partLink = new HyperLink();
			partLink.Text = this.TrimTitle( label );
			partLink.NavigateUrl = url;
			partLink.CssClass = this.ItemCssClass;
			if ( this.partNo++ > 0 ) {
				this.Controls.Add( new LiteralControl( this.Separator ) );
			}
			this.Controls.Add( partLink );
		}

		protected void GenerateSiteTreeLinks( string UserPath ) {
			string[] pathParts = UserPath.Split( '/' );
			string siteTreeKey = pathParts[0];
			if ( pathParts.Length < 2 ) {
				return;
			}
			string partPath = "~";
			string xpath = "";
			XmlNode partNode = StarrTech.WebTools.CMS.ContentPublisher.SiteTreeXmlDoc( siteTreeKey ).DocumentElement;
			this.Visible = false;
			foreach ( string part in pathParts ) {
				if ( part == siteTreeKey ) {
					this.AddLink( "Home", "~/default.aspx" );
					continue;
				}
				this.Visible = true;
				partPath += "/" + part;
				xpath = string.Format( "treenode[@key='{0}']", part );
				partNode = partNode.SelectSingleNode( xpath );
				XmlAttributeCollection attribs = partNode.Attributes;
				string label = ( (XmlAttribute) attribs.GetNamedItem( "title" ) ).Value;
				string url = "";
				if ( this.partNo < pathParts.Length - 1 + (string.IsNullOrEmpty(this.PageItemLabel) ? 0 : 1)  ) {
					url = this.ResolveUrl( partPath.Replace( "~", "~/" + siteTreeKey ) + "/default.aspx" );
				}
				this.AddLink( label, url );
			}
			if ( !string.IsNullOrEmpty( this.PageItemLabel ) ) {
				this.AddLink( this.PageItemLabel, "" );				
			}
		}
		
		protected override void OnLoad(EventArgs e) {
			if ( this.AutoGenerateSiteTreeLinks ) {
				// Generate the menu from our location in the site tree
				string treepath = StarrTech.WebTools.CMS.ContentPublisher.TreeNodePath( this.SectionNodeID, this.TreeKey );
				this.GenerateSiteTreeLinks( this.TreeKey + treepath );
			}
			base.OnLoad( e );
		}

	}

}
