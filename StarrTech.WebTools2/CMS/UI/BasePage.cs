using System;
using System.Collections.Generic;
using System.Text;

namespace StarrTech.WebTools.CMS.UI {

	public class BasePage : System.Web.UI.Page {

		public virtual string TreeKey {
			get {
				string key = this.Request.QueryString[ContentPublisher.TreeQKey];
				if ( !string.IsNullOrEmpty( key ) ) {
					string[] keys = key.Split( ',' );
					return ( keys[0] );
				}
				return ( "" );
			}
		}

		public virtual int SectionNodeID {
			get {
				string id = this.Request.QueryString[ContentPublisher.NodeQKey];
				if ( !string.IsNullOrEmpty( id ) ) {
					string[] ids = id.Split( ',' );
					return ( Convert.ToInt32( ids[0] ) );
				}
				return ( -1 );
			}
		}

		public virtual int ContentItemID {
			get {
				string id = this.Request.QueryString[ContentPublisher.CidQKey];
				if ( !string.IsNullOrEmpty( id ) ) {
					string[] ids = id.Split( ',' );
					int index = ids.Length - 1;
					return ( Convert.ToInt32( ids[index] ) );
				}
				return ( -1 );
			}
		}

		public virtual ContentMenuNode ContentMenuNode {
			get {
				if ( this.contentMenuNode == null ) {
					this.contentMenuNode = new ContentMenuNode( this.SectionNodeID, this.TreeKey, true );
				}
				return ( this.contentMenuNode );
			}
		}
		private ContentMenuNode contentMenuNode;

		public bool IsSafari {
			get { return ( this.isSafari ); }
			set { this.isSafari = value; }
		}
		private bool isSafari = false;

		protected override void OnPreInit( EventArgs e ) {
			string uAgent = this.Request.ServerVariables["http_user_agent"];
			if ( !string.IsNullOrEmpty( uAgent ) ) {
				this.IsSafari = uAgent.ToLower().Contains( "safari" );
				if ( this.IsSafari ) {
					this.Page.ClientTarget = "uplevel";
				}
			}
			base.OnPreInit( e );
		}

	}

}
