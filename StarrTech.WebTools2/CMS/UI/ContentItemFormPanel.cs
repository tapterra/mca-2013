using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.DBUI;
using StarrTech.WebTools.Panels;

namespace StarrTech.WebTools.CMS.UI {

	public class ContentItemFormPanel : Table, INamingContainer {

		#region Properties

		/// <summary>
		/// The ContentItem instance we are editing
		/// </summary>
		public ContentItem ContentItem {
			get {
				if ( this.contentItem == null ) {
					this.contentItem = new ContentItem( this.ContentItemID );
				}
				return ( this.contentItem );
			}
			set {
				this.LoadContentEditor( value );
			}
		}
		private ContentItem contentItem;

		public int NodeID {
			get {
				object id = ViewState["NodeID"];
				return ( id == null ? -1 : (int) id );
			}
			set { ViewState["NodeID"] = value; }
		}

		public int ContentItemID {
			get {
				object id = ViewState["ContentItemID"];
				return ( id == null ? -1 : (int) id );
			}
			set {
				ViewState["ContentItemID"] = value;
				if ( this.IncludeNodePicker ) {
					this.NodePicker.ContentItemID = value;
				}
			}
		}

		public bool IncludeNodePicker {
			get {
				object obj = ViewState["IncludeNodePicker"];
				bool result = obj == null ? false : (bool) obj;
				return ( this.NeverUseNodePicker ? false : result );
			}
			set { ViewState["IncludeNodePicker"] = value; }
		}

		/// <summary>
		/// Overrides IncludeNodePicker (which can be reset by ContentitemsGrid) for applications with fixed trees (like Tetris)
		/// </summary>
		public bool NeverUseNodePicker {
			get {
				object obj = ViewState["NeverUseNodePicker"];
				return ( obj == null ? false : (bool) obj );
			}
			set { ViewState["NeverUseNodePicker"] = value; }
		}

		#region Controls

		public LabeledTextBox TitleBox {
			get {
				this.EnsureChildControls();
				return ( this.titleBox );
			}
		}
		protected LabeledTextBox titleBox;
		protected RequiredFieldValidator titleReqVal;

		public LabeledDropDownList ContentTypeListBox {
			get {
				this.EnsureChildControls();
				return ( this.contentTypeListBox );
			}
		}
		protected LabeledDropDownList contentTypeListBox;

		public CheckBox IsApprovedXBox {
			get {
				this.EnsureChildControls();
				return ( this.isApprovedXBox );
			}
		}
		protected CheckBox isApprovedXBox;

		public LabeledTextBox CreatedDTBox {
			get {
				this.EnsureChildControls();
				return ( this.createdDTBox );
			}
		}
		protected LabeledTextBox createdDTBox;

		public LabeledTextBox ActiveDateBox {
			get {
				this.EnsureChildControls();
				return ( this.activeDateBox );
			}
		}
		protected LabeledTextBox activeDateBox;
		protected CompareValidator activeDateCompVal;
		protected RequiredFieldValidator activeDateReqVal;

		public LabeledTextBox ExpireDateBox {
			get {
				this.EnsureChildControls();
				return ( this.expireDateBox );
			}
		}
		protected LabeledTextBox expireDateBox;
		protected CompareValidator expireDateCompVal;
		protected RequiredFieldValidator expireDateReqVal;

		protected CompareValidator actexpDateCompVal;

		public LabeledTextBox CommentsBox {
			get {
				this.EnsureChildControls();
				return ( this.commentsBox );
			}
		}
		protected LabeledTextBox commentsBox;

		public Panel ContentPanel {
			get {
				this.EnsureChildControls();
				return ( this.contentPanel );
			}
		}
		protected Panel contentPanel;

		public SiteTreeNodePicker NodePicker {
			get {
				this.EnsureChildControls();
				return ( this.nodePicker );
			}
		}
		protected SiteTreeNodePicker nodePicker;

		public LabeledTextBox LinkBox {
			get {
				this.EnsureChildControls();
				return ( this.linkBox );
			}
		}
		protected LabeledTextBox linkBox;

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}

		#endregion

		#region UI Data-Access
		public string Title {
			get { return ( this.TitleBox.Text ); }
			set { this.TitleBox.Text = value; }
		}

		public string ContentTypeKey {
			get {
				this.EnsureChildControls();
				return ( this.ContentTypeListBox.SelectedValue );
			}
			set {
				this.EnsureChildControls();
				this.ContentTypeListBox.SelectedValue = value;
			}
		}

		public bool IsApproved {
			get { return ( this.IsApprovedXBox.Checked ); }
			set { this.IsApprovedXBox.Checked = value; }
		}

		public int CreatedByUserID {
			get {
				object id = ViewState["CreatedByUserID"];
				return ( id == null ? this.CurrentUserID : (int) id );
			}
			set { ViewState["CreatedByUserID"] = value; }
		}

		public int CurrentUserID {
			get {
				object id = ViewState["CurrentUserID"];
				return ( id == null ? ContentTypeManager.CurrentUserID : (int) id );
			}
			set {
				ViewState["CurrentUserID"] = value;
			}
		}

		public string CreatedBy {
			set {
				//this.CreatedByBox.Text = value; 
			}
		}

		public string CreatedDT {
			get { return ( this.CreatedDTBox.Text ); }
			set { this.CreatedDTBox.Text = value; }
		}

		public string ActiveDate {
			get { return ( this.ActiveDateBox.Text ); }
			set { this.ActiveDateBox.Text = value; }
		}

		public string ExpireDate {
			get { return ( this.ExpireDateBox.Text ); }
			set { this.ExpireDateBox.Text = value; }
		}

		public string Comments {
			get { return ( this.CommentsBox.Text ); }
			set { this.CommentsBox.Text = value; }
		}

		public string Link {
			set { this.LinkBox.Text = value; }
		}

		#endregion

		#endregion

		#region Constructors

		public ContentItemFormPanel()
			: base() {
			this.CellSpacing = 4;
			this.CellPadding = 0;
			this.BorderStyle = BorderStyle.None;
			this.Width = new Unit( "100%" );
		}

		public ContentItemFormPanel( ContentType type, bool includeNodePicker ) {
			this.IncludeNodePicker = includeNodePicker;
			this.CreateContentEditor( type );
		}

		#endregion

		#region Methods

		protected override void CreateChildControls() {

			#region TitleBox
			this.titleBox = new LabeledTextBox();
			this.titleBox.Label = "Story Title";
			this.titleBox.Width = new Unit( "100%" );
			this.titleBox.ID = "ContentItemFormPanel_TitleBox"; // Required for validators
			this.titleBox.Frame.LabelMark = "*";

			this.titleReqVal = new RequiredFieldValidator();
			this.titleReqVal.ControlToValidate = this.titleBox.ID;
			this.titleReqVal.Display = ValidatorDisplay.None;
			this.titleReqVal.ErrorMessage = "Please enter a title for this story.";
			this.titleReqVal.SetFocusOnError = true;
			#endregion

			#region ContentTypeListBox
			this.contentTypeListBox = new LabeledDropDownList();
			this.contentTypeListBox.Label = "Content Type";
			this.contentTypeListBox.Width = new Unit( "100%" );
			this.contentTypeListBox.Enabled = false; // Can't make changes to this here for this release
			#endregion

			#region IsApprovedXBox
			this.isApprovedXBox = new CheckBox();
			this.isApprovedXBox.Text = "Approved?";
			this.isApprovedXBox.CssClass = "xbox";
			#endregion

			#region CreatedDTBox
			this.createdDTBox = new LabeledTextBox();
			this.createdDTBox.Label = "Created On";
			this.createdDTBox.Width = new Unit( "100%" );
			this.createdDTBox.Enabled = false;
			#endregion

			#region ActiveDateBox
			this.activeDateBox = new LabeledTextBox();
			this.activeDateBox.Width = new Unit( "100%" );
			this.activeDateBox.Label = "Active On";
			this.activeDateBox.ID = "ContentItemFormPanel_ActiveDateBox"; // Required for validators

			this.activeDateCompVal = new CompareValidator();
			this.activeDateCompVal.ControlToValidate = this.ActiveDateBox.ID;
			this.activeDateCompVal.Operator = ValidationCompareOperator.DataTypeCheck;
			this.activeDateCompVal.Type = ValidationDataType.Date;
			this.activeDateCompVal.Display = ValidatorDisplay.None;
			this.activeDateCompVal.ErrorMessage = "Please enter a valid activation date for this story.";
			this.activeDateCompVal.SetFocusOnError = true;

			this.activeDateReqVal = new RequiredFieldValidator();
			this.activeDateReqVal.ControlToValidate = this.ActiveDateBox.ID;
			this.activeDateReqVal.Display = ValidatorDisplay.None;
			this.activeDateReqVal.ErrorMessage = "Please enter the Active On date for this story.";
			this.activeDateReqVal.SetFocusOnError = true;
			#endregion

			#region ExpireDateBox
			this.expireDateBox = new LabeledTextBox();
			this.expireDateBox.Width = new Unit( "100%" );
			this.expireDateBox.Label = "Expires On";
			this.expireDateBox.ID = "ContentItemFormPanel_ExpireDateBox"; // Required for validators

			this.expireDateCompVal = new CompareValidator();
			this.expireDateCompVal.ControlToValidate = this.expireDateBox.ID;
			this.expireDateCompVal.Operator = ValidationCompareOperator.DataTypeCheck;
			this.expireDateCompVal.Type = ValidationDataType.Date;
			this.expireDateCompVal.Display = ValidatorDisplay.None;
			this.expireDateCompVal.ErrorMessage = "Please enter a valid expiration date for this story.";
			this.expireDateCompVal.SetFocusOnError = true;

			this.expireDateReqVal = new RequiredFieldValidator();
			this.expireDateReqVal.ControlToValidate = this.ExpireDateBox.ID;
			this.expireDateReqVal.Display = ValidatorDisplay.None;
			this.expireDateReqVal.ErrorMessage = "Please enter the Expires On date for this story.";
			this.expireDateReqVal.SetFocusOnError = true;

			this.actexpDateCompVal = new CompareValidator();
			this.actexpDateCompVal.ControlToValidate = this.expireDateBox.ID;
			this.actexpDateCompVal.ControlToCompare = this.activeDateBox.ID;
			this.actexpDateCompVal.Operator = ValidationCompareOperator.GreaterThan;
			this.actexpDateCompVal.Type = ValidationDataType.Date;
			this.actexpDateCompVal.Display = ValidatorDisplay.None;
			this.actexpDateCompVal.ErrorMessage = "Active Date must be earlier than Expire Date.";
			this.actexpDateCompVal.SetFocusOnError = true;
			#endregion

			#region LinkBox
			this.linkBox = new LabeledTextBox();
			this.linkBox.Label = "Story Link";
			this.linkBox.Enabled = false;
			#endregion

			#region CommentsBox
			this.commentsBox = new LabeledTextBox();
			this.commentsBox.Label = "Comments";
			this.commentsBox.Width = new Unit( "100%" );
			this.commentsBox.TextMode = TextBoxMode.MultiLine;
			this.commentsBox.Rows = 4;
			this.commentsBox.Wrap = true;
			#endregion

			#region ContentPanel
			this.contentPanel = new Panel();
			this.contentPanel.Width = new Unit( "100%" );
			#endregion

			#region NodePicker
			this.nodePicker = new SiteTreeNodePicker();
			if ( this.IncludeNodePicker ) {
				this.nodePicker.Label = "Section Links";
				this.nodePicker.Width = new Unit( "100%" );
				this.nodePicker.Frame.FramerType = FramerType.TopTitleFrame;
				this.nodePicker.Frame.FixedWidth = new Unit( "240px" ); //this.nodePicker.Width;
				this.nodePicker.Frame.FixedHeight = new Unit( "177px" );
				this.nodePicker.Frame.ScrollingContent = true;
			}
			#endregion

			#region Stack it up...
			this.Rows.Clear();
			this.Style["table-layout"] = "fixed"; // (?)

			TableRow row = new TableRow();

			TableCell cell = new TableCell();
			cell.ColumnSpan = 3;
			cell.Controls.Add( this.titleBox );
			cell.Controls.Add( this.titleReqVal );
			cell.Width = new Unit( this.IncludeNodePicker ? "430px" : "100%" );
			row.Cells.Add( cell );

			if ( this.IncludeNodePicker ) {
				cell = new TableCell();
				cell.VerticalAlign = VerticalAlign.Top;
				cell.Style["padding-left"] = "0px";
				cell.Style["padding-top"] = "0px";
				cell.Controls.Add( this.nodePicker );
				cell.RowSpan = 4;
				row.Cells.Add( cell );
			}

			this.Rows.Add( row );
			row = new TableRow();

			cell = new TableCell();
			cell.Controls.Add( this.contentTypeListBox );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.createdDTBox );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.linkBox );
			row.Cells.Add( cell );

			this.Rows.Add( row );
			row = new TableRow();

			cell = new TableCell();
			cell.Controls.Add( this.activeDateBox );
			cell.Controls.Add( this.activeDateCompVal );
			cell.Controls.Add( this.activeDateReqVal );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.expireDateBox );
			cell.Controls.Add( this.expireDateCompVal );
			cell.Controls.Add( this.expireDateReqVal );
			cell.Controls.Add( this.actexpDateCompVal );
			row.Cells.Add( cell );

			cell = new TableCell();
			//cell.Width = new Unit( "90px" );
			cell.VerticalAlign = VerticalAlign.Bottom;
			cell.Style["padding-bottom"] = "3px";
			cell.Style["padding-left"] = "3px";
			cell.Controls.Add( this.isApprovedXBox );
			row.Cells.Add( cell );

			this.Rows.Add( row );
			row = new TableRow();

			cell = new TableCell();
			cell.ColumnSpan = 3;
			cell.Controls.Add( this.commentsBox );
			row.Cells.Add( cell );
			this.Rows.Add( row );

			row = new TableRow();
			cell = new TableCell();
			cell.ColumnSpan = this.IncludeNodePicker ? 4 : 3;
			cell.Controls.Add( this.contentPanel );
			row.Cells.Add( cell );

			this.Rows.Add( row );

			#endregion

		}

		#region Content-Panel UI Creation

		protected override void OnInit( EventArgs args ) {
			// Build the custom content-editors as defined by ContentTypes.config
			//this.SetupContentEditors();
			
			// Build the custom content editor for the current content-type
			if ( this.contentItem != null ) {
				this.CreateContentEditor( this.contentItem.ContentType );
			}
			base.OnInit( args );
		}

		/// <summary>
		/// Build the UI controls for the custom content-editors as defined by ContentTypes.config
		/// </summary>
		//protected void SetupContentEditors() {
		//  this.EnsureChildControls();
		//  foreach ( ContentType type in ContentTypeManager.ContentTypes ) {
		//    if ( type.Parts.Count > 0 ) {
		//      // Create the editor for this type and add it to the ContentPanel
		//      this.CreateContentEditor( type );
		//    }
		//  }
		//}

		/// <summary>
		/// For the given ContentType, create an invisible editor-form and add it to the ContentPanel
		/// </summary>
		protected Control CreateContentEditor( ContentType type ) {
			// Build the editor based on the type's ContentItemParts
			Panel editor = ContentItemFormPanel.BuildContentEditorPanel( type );
			editor.ID = type.TypeKey + "_editor";
			editor.Width = new Unit( "100%" );
			this.ContentPanel.Controls.Clear();
			this.ContentPanel.Controls.Add( editor );
			return ( editor );
		}

		internal static Panel BuildContentEditorPanel( ContentType type ) {
			Panel editor = new Panel();
			// Create and initialize a part-editor control for each ContentItemPart
			foreach ( ContentItemPart part in type.Parts ) {
				ContentPartTypes parttype = (ContentPartTypes) part.DataTypeCode;
				Control partEditor = null;
				switch ( parttype ) {
					case ContentPartTypes.PlainText:
						LabeledTextBox tbox = new LabeledTextBox();
						tbox.Label = part.Label;
						tbox.Width = new Unit( "100%" );
						tbox.Frame.Style["margin-top"] = "10px";
						tbox.Frame.Style["margin-bottom"] = "20px";
						if ( part.Rows > 1 ) {
							tbox.Rows = part.Rows;
							tbox.TextMode = TextBoxMode.MultiLine;
						}
						if ( !string.IsNullOrEmpty( part.Height ) ) {
							tbox.Height = new Unit( part.Height );
						}
						tbox.TabIndex = (short) part.PartNo;
						partEditor = tbox;
						break;
					case ContentPartTypes.HtmlText:
						HtmlBox hbox = new HtmlBox();
						hbox.Label = part.Label;
						hbox.Width = new Unit( "100%" );
						hbox.Height = new Unit( string.IsNullOrEmpty( part.Height ) ? "400px" : part.Height );
						hbox.Frame.Style["margin-top"] = "6px";
						hbox.Frame.Style["margin-bottom"] = "20px";
						hbox.TabIndex = (short) part.PartNo;
						partEditor = hbox;
						break;
					case ContentPartTypes.TinyMCE:
						HtmlTextBox htbox = new HtmlTextBox();
						htbox.Label = part.Label;
						htbox.Width = new Unit( "100%" );
						htbox.Height = new Unit( string.IsNullOrEmpty( part.Height ) ? "400px" : part.Height );
						htbox.Frame.Style["margin-top"] = "6px";
						htbox.Frame.Style["margin-bottom"] = "20px";
						htbox.TabIndex = (short) part.PartNo;
						partEditor = htbox;
						break;
					case ContentPartTypes.OptionCheckbox:
						CheckBox xbox = new CheckBox();
						xbox.Text = part.Label;
						xbox.Width = new Unit( "100%" );
						xbox.CssClass = "xbox";
						xbox.Style["margin-top"] = "0px";
						xbox.Style["margin-bottom"] = "6px";
						xbox.TabIndex = (short) part.PartNo;
						partEditor = xbox;
						break;
					case ContentPartTypes.OptionListbox:
						// LATER: Implement support for ContentPartTypes.OptionListbox
						break;
					case ContentPartTypes.Image:
						// LATER: Implement support for ContentPartTypes.Image
						break;
					case ContentPartTypes.FileLink:
						// LATER: Implement support for ContentPartTypes.Document
						break;
				}
				if ( partEditor != null ) {
					// Add the part-editor control to the type editor
					partEditor.ID = type.TypeKey + "_" + part.Name;
					editor.Controls.Add( partEditor );
				}
			}
			return ( editor );
		}

		#endregion

		#region Content-Panel Data Loading

		/// <summary>
		/// Populate the custom content-part editor controls based on the ContentItem being edited
		/// </summary>
		public void InitContentEditor() {
			this.ContentTypeListBox.Items.Clear();
			if ( this.ContentItem.ID > 0 ) {
				// We're editing an existing node ...
				// Hide all the content-type editors
				foreach ( Control ctl in this.contentPanel.Controls ) {
					ctl.Visible = false;
				}
				// ...then set up the editor we want to use
				ContentType type = ContentTypeManager.ContentTypes[this.ContentItem.TypeKey];
				// Set up the content-type listbox and description for our content type
				this.ContentTypeListBox.Items.Add( new ListItem( type.Label, type.TypeKey ) );
				this.ContentTypeListBox.SelectedValue = type.TypeKey;
				this.LoadContentEditor();
			} else {
				// We're adding a new node...
				this.ContentTypeListBox.Enabled = true;
				//formPanel.Description = string.Format( "Please select a {0} above and click Save.", formPanel.ContentTypeListBox.Label );
				// Set up the ContentTypeListBox with valid values for this node
				foreach ( ContentType type in ContentTypeManager.ContentTypes ) {
					// No custom types...
					if ( type.EditorType != ContentTypeEditors.contentform ) {
						continue;
					}
					this.ContentTypeListBox.Items.Add( new ListItem( type.Label, type.TypeKey ) );
				}
			}
			this.ContentTypeListBox.Enabled = false;
		}

		public void LoadContentEditor( ContentItem Item ) {
			this.ContentItemID = Item.ID;
			this.contentItem = Item;
			this.Title = Item.Title;
			this.Comments = Item.Comments;
			this.CreatedBy = ContentTypeManager.GetUserName( Item.CreatedByUserID );
			this.IsApproved = Item.IsApproved;
			this.CreatedDT = Item.CreatedDT.ToString( "MM/dd/yyyy HH:mm" );
			this.ActiveDate = Item.ActiveDateStr;
			this.ExpireDate = Item.ExpireDateStr;
			this.ContentTypeListBox.Items.Clear();
			this.ContentTypeListBox.Items.Add( new ListItem( Item.ContentType.Label, Item.ContentType.TypeKey ) );
			this.ContentTypeKey = Item.TypeKey;
			if ( this.IncludeNodePicker ) {
				this.NodePicker.ContentItemID = Item.ID;
			}
			this.Link = this.ResolveUrl( string.Format( "~/story{0}.aspx", Item.ID ) );
			this.LoadContentEditor();
			// Configure the common elements
			if ( Item.ContentType.AllowActivation ) {
				this.ActiveDateBox.Enabled = this.ExpireDateBox.Enabled = true;
				this.ActiveDateBox.Frame.LabelMark = this.ExpireDateBox.Frame.LabelMark = Item.ContentType.ActivationRequired ? "*" : "";
				this.activeDateReqVal.Visible = this.expireDateReqVal.Visible = Item.ContentType.ActivationRequired;
			} else {
				this.ActiveDateBox.Enabled = this.ExpireDateBox.Enabled = false;
				this.ActiveDateBox.Text = this.ExpireDateBox.Text = "n/a";
				this.activeDateCompVal.Visible = this.expireDateCompVal.Visible = this.actexpDateCompVal.Visible = false;
			}
			this.TitleBox.Focus();
		}

		protected void LoadContentEditor() {
			//ContentType type = ContentTypeManager.ContentTypes[this.ContentItem.TypeKey];
			// Hide all the content-editors
			//foreach ( Control ctl in this.ContentPanel.Controls ) {
			//  ctl.Visible = false;
			//}
			ContentType type = this.contentItem.ContentType;
			Control editor = this.ContentPanel.FindControl( type.TypeKey + "_editor" );
			if ( editor == null ) {
				editor = this.CreateContentEditor( type );
			}
			// Show the editor panel
			editor.Visible = true;
			// If it needs initialization, do it...
			IAdminDataGridContentEditor editorEx = editor as IAdminDataGridContentEditor;
			if ( editorEx != null ) {
				// Make sure the grid is up-to-date...
				editorEx.DataGrid.InitPanel();
				// We already have a ValidationSummary, so the editor doesn't need one...
				editorEx.DataGrid.MessagePanel.ValidationMessage.Visible = false;
				// Pass it the current node ID 
				editorEx.NodeID = NodeID;
			}
			// Get the node's ContentItem 
			if ( this.ContentItem != null ) {
				// Load the node's ContentItem Parts values into the editor's part-controls
				ContentItemVersion vers = this.ContentItem.Versions[this.ContentItem.Versions.MostRecentVersionNumber];
				// If the ContentType definition has changed since this Item was persisted, we won't be able to init the 
				// new part editors - need to do it here...
				foreach ( Control partEditor in editor.Controls ) {
					if ( partEditor is LabeledTextBox ) {
						( partEditor as LabeledTextBox ).Text = "";
					} else if ( partEditor is HtmlBox ) {
						( partEditor as HtmlBox ).Text = "";
					} else if ( partEditor is HtmlTextBox ) {
						( partEditor as HtmlTextBox ).Text = "";
					} else if ( partEditor is CheckBox ) {
						( partEditor as CheckBox ).Checked = false;
					}
				}
				// Copy the value of each ContentItemPart to it's corresponding part-editor control
				foreach ( ContentItemPart part in vers.Parts ) {
					ContentPartTypes parttype = (ContentPartTypes) part.DataTypeCode;
					Control partEditor = editor.FindControl( type.TypeKey + "_" + part.Name );
					if ( partEditor != null ) {
						switch ( parttype ) {
							case ContentPartTypes.PlainText:
								LabeledTextBox tbox = partEditor as LabeledTextBox;
								tbox.Text = part.Content;
								break;
							case ContentPartTypes.HtmlText:
								HtmlBox hbox = partEditor as HtmlBox;
								hbox.Text = part.Content;
								break;
							case ContentPartTypes.TinyMCE:
								HtmlTextBox htbox = partEditor as HtmlTextBox;
								htbox.Text = part.Content;
								break;
							case ContentPartTypes.OptionCheckbox:
								CheckBox xbox = partEditor as CheckBox;
								xbox.Checked = part.Content.ToLower().Equals( "true" );
								break;
						}
					}
				}
			}
		}

		#endregion

		#region Content-Panel Data Reading

		public ContentItem ReadContentItem() {
			return ( this.ReadContentItem( this.ContentItemID ) );
		}

		public ContentItem ReadContentItem( int ContentItemID ) {
			this.contentItem = new ContentItem( ContentItemID );
			this.contentItem.Title = this.Title;
			this.contentItem.Comments = this.Comments;
			this.contentItem.IsApproved = this.IsApproved;
			this.contentItem.TypeKey = this.ContentTypeKey;
			this.contentItem.CreatedByUserID = this.CreatedByUserID;
			this.contentItem.ActiveDateStr = this.ActiveDate;
			this.contentItem.ExpireDateStr = this.ExpireDate;
			// Deconstruct the content-panel part editor values
			this.ReadContentEditor();
			return ( this.contentItem );
		}

		protected void ReadContentEditor() {
			// Panel contentPanel, ContentItem item 
			ContentType type = this.contentItem.ContentType; 
			Panel editor = this.ContentPanel.FindControl( type.TypeKey + "_editor" ) as Panel;
			if ( editor != null ) {
				if ( this.ContentItem != null ) {
					// Load the editor's part-controls values into the node's ContentItem Parts 
					ContentItemVersion vers = this.ContentItem.CurrentActiveVersion;
					if ( vers == null ) {
						vers = this.ContentItem.CreateNewRevisionVersion();
					}
					// Verify that the ContentItem & ContentType match up
					if ( type.Parts.Count != vers.Parts.Count ) {
						//throw new Exception( "type.Parts.Count != vers.Parts.Count!" );
						// Our ContentType definition has changed - clear & rebuild the parts
						foreach ( ContentItemPart part in vers.Parts ) {
							part.Delete();
						}
						vers.Parts.Clear();
						foreach ( ContentItemPart part in type.Parts ) {
							vers.Parts.Add( new ContentItemPart( part ) );
						}
					}
					// Copy the value of each part-editor control to it's matching ContentItem part
					foreach ( ContentItemPart part in vers.Parts ) {
						// Find the part-editor control for this part
						Control partEditor = editor.FindControl( type.TypeKey + "_" + part.Name );
						ContentPartTypes parttype = (ContentPartTypes) part.DataTypeCode;
						switch ( parttype ) {
							case ContentPartTypes.PlainText:
								LabeledTextBox tbox = partEditor as LabeledTextBox;
								part.Content = tbox.Text;
								break;
							case ContentPartTypes.HtmlText:
								HtmlBox hbox = partEditor as HtmlBox;
								part.Content = hbox.Text;
								break;
							case ContentPartTypes.TinyMCE:
								HtmlTextBox htbox = partEditor as HtmlTextBox;
								part.Content = htbox.Text;
								break;
							case ContentPartTypes.OptionCheckbox:
								CheckBox xbox = partEditor as CheckBox;
								part.Content = xbox.Checked ? "true" : "false";
								break;
						}
					}
				}
			}
		}

		#endregion

		#endregion

	}

}
