using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

using StarrTech.WebTools.CMS;

namespace StarrTech.WebTools.CMS.UI {
	
	public class SiteMap : TreeView {

		public string RootLabel {
			get {
				object obj = ViewState["RootLabel"];
				return ( obj == null ? rootLabel : (string) obj );
			}
			set { ViewState["RootLabel"] = value; }
		}
		private const string rootLabel = "Site Home Page";

		public string TreeKeys {
			get {
				object obj = ViewState["TreeKeys"];
				return ( obj == null ? ContentPublisher.MainNavTreeKey : (string) obj );
			}
			set { ViewState["TreeKeys"] = value; }
		}

		#region AuthorizeMenuItem
		public event EventHandler<AuthorizeMenuEventArgs> AuthorizeMenuItem {
			add { Events.AddHandler( AuthorizeMenuItemEventKey, value ); }
			remove { Events.RemoveHandler( AuthorizeMenuItemEventKey, value ); }
		}
		protected virtual void OnAuthorizeMenuItem( Object sender, AuthorizeMenuEventArgs args ) {
			this.EnsureChildControls();
			EventHandler<AuthorizeMenuEventArgs> handler = (EventHandler<AuthorizeMenuEventArgs>) Events[AuthorizeMenuItemEventKey];
			if ( handler != null ) {
				handler( sender, args );
			}
		}
		private static readonly object AuthorizeMenuItemEventKey = new object();
		#endregion

		protected override void OnLoad( EventArgs e ) {
			if ( !this.Page.IsPostBack ) {
				this.BuildTreeNodes();
			}
			base.OnLoad( e );
		}

		protected void BuildTreeNodes() {
			System.Web.UI.WebControls.TreeNode rootNode = new System.Web.UI.WebControls.TreeNode( RootLabel );
			rootNode.NavigateUrl = "~/default.aspx";
			this.Nodes.Add( rootNode );
			string[] keys = this.TreeKeys.Split( ',' );
			foreach ( string key in keys ) {
				this.LoadSiteTree( rootNode, key );
			}
		}

		protected bool AuthorizeMenuNode( ContentMenuNode menuNode ) {
			AuthorizeMenuEventArgs args = new AuthorizeMenuEventArgs( menuNode );
			this.OnAuthorizeMenuItem( this, args );
			return ( args.IsAuthorized );
		}

		protected void LoadSiteTree( System.Web.UI.WebControls.TreeNode rootNode, string SiteTreeKey ) {
			List<ContentMenuNode> menuNodes = ContentMenuNode.LoadTreeMenus( SiteTreeKey );
			foreach ( ContentMenuNode menuNode in menuNodes ) {
				if ( this.AuthorizeMenuNode( menuNode ) ) {
					BuildTreeNode( SiteTreeKey, rootNode, menuNode );
				}
			}
		}

		protected void BuildTreeNode( string siteTreeKey, System.Web.UI.WebControls.TreeNode ParentTreeNode, ContentMenuNode menuNode ) {
			if ( menuNode.TypeKey == "homepage" ) {
				return;
			}
			System.Web.UI.WebControls.TreeNode treeNode = new System.Web.UI.WebControls.TreeNode(
				menuNode.Text, menuNode.ControlID, "", menuNode.NavigateUrl, ""
			);
			ParentTreeNode.ChildNodes.Add( treeNode );
			foreach ( ContentMenuNode subnode in menuNode.SubNodes ) {
				BuildTreeNode( siteTreeKey, treeNode, subnode );
			}
		}

	}

}
