using System;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using StarrTech.WebTools.TreeUI;

namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// TreePanel customized for use by the CMS SiteEditor control.
	/// </summary>
	public class TreeEditorGrid : TreePanel {
		
		#region Properties
		
		/// <summary>
		/// Object responsible for rendering our UI features.
		/// Overridden to insure that we use a TreeEditorGridRenderer.
		/// </summary>
		public override BaseTreeRenderer Renderer {
			get { 
				if ( this.renderer == null ) {
					this.renderer = new CMS.UI.TreeEditorGridRenderer( this );
				}
				return ( this.renderer ); 
			}
		}
		protected internal BaseTreeRenderer renderer = null;
		
		/// <summary>
		/// Key string for the CMS.Tree instance being edited
		/// </summary>
		public string TreeKey {
			get { 
				return ( this.treeKey ); 
			}
			set { 
				string oldKey = this.treeKey;
				this.treeKey = value;
				if ( oldKey != value ) {
					this.dataTree = null;
					if ( oldKey != null ) this.SelectedNodeKey = null;
					this.ReloadDataTree();
				}
			}
		}
		private string treeKey;
		
		/// <summary>
		/// CMS.Tree being edited
		/// </summary>
		public Tree DataTree {
			get { 
				if ( this.dataTree == null ) {
					this.ReloadDataTree();
				}
				return ( this.dataTree ); 
			}
			set { this.dataTree = value; }
		}
		private Tree dataTree = null;
		
		#endregion
		
		#region Events

		#region OnNodeEdit
		/// <summary>
		/// Event fired when the panel's currently-selected node is clicked a second time (to edit).
		/// </summary>
		public event TreePanelNodeClickEventHandler NodeEdit {
			add { Events.AddHandler( NodeEditEventKey, value ); }
			remove { Events.RemoveHandler( NodeEditEventKey, value ); }
		}
		public virtual void OnNodeEdit( Object sender, TreePanelNodeClickEventArgs args ) {
			TreePanelNodeClickEventHandler handler = (TreePanelNodeClickEventHandler) Events[NodeEditEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object NodeEditEventKey = new object();
		#endregion

		#endregion

		#region Constructor
		/// <summary>
		/// Default constructor to set up the various properties the way we want them.
		/// </summary>
		public TreeEditorGrid() : base() {
			this.TreeOutputStyle = TreeOutputStyle.Tabular;
			this.Width = new Unit( "100%" );
			this.TableBorder = 0;
			this.TableCellSpacing = 0;
			this.TableCellPadding = 4;
			this.NodeDisplayStyle = NodeDisplayStyle.Standard;
			this.Scrolling = false;
			this.ForceInheritedChecks = false;
			this.TableHeaderHorizAlign = HorizontalAlign.Center;
			this.DataBackColor = Color.FromName( "#FFFFFF" );
			this.AltDataBackColor = Color.FromName( "#F0F0F0" );
			this.SelectedDataBackColor = Color.Black; //FromName( "#ffe0e0" );
			this.SelectedDataForeColor = Color.White; 
			this.TableBackColor = Color.FromName( "#CCCCCC" );
			this.EnableViewState = true;
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Generate an object containing the panel's current state
		/// </summary>
		/// <returns>The panel's current state</returns>
		protected override object SaveViewState() {
			object[] state = new object[2];
			object baseState = base.SaveViewState();
			state[0] = baseState;
			state[1] = this.TreeKey;
			return ( state );
		}
		
		/// <summary>
		/// Load the TreePanel from the viewstate XML
		/// </summary>
		/// <param name="savedState">the statebag object</param>
		protected override void LoadViewState( object savedState ) {
			if ( savedState != null ) {
				object[] state = (object[]) savedState;
				if ( state[0] != null ) base.LoadViewState( state[0] );
				if ( state[1] != null ) this.TreeKey = (string) state[1];
			}
		}
		
		/// <summary>
		/// Causes the CMS.Tree instance being edited to be reloaded from the database (and the TreeNodeControls to be rebuilt)
		/// </summary>
		public void ReloadDataTree() {
			HttpContext.Current.Trace.Warn( ">>> TreeEdtorGrid.ReloadDataTree" );
			// Load the data-tree from the database
			if ( this.dataTree == null ) {
				this.dataTree = new Tree( this.TreeKey );
				if ( this.dataTree.ID <= 0 ) {
					this.dataTree.Persist();
				}
				this.ReloadControlTree();
			}
			HttpContext.Current.Trace.Warn( "<<< TreeEdtorGrid.ReloadDataTree" );
		}
		
		/// <summary>
		/// Assigns a new tree-key string and reloads the panel's nodes.
		/// </summary>
		/// <param name="tree">Tree-key string for the new CMS.Tree instance to be loaded</param>
		public void ReloadControlTree() {
			HttpContext.Current.Trace.Warn( ">>> TreeEdtorGrid.ReloadControlTree" );
			// Clear the old stuff
			this.Controls.Clear();
			// Rebuild the UI-tree...
			this.FromXml( this.DataTree.ToXml() );
			HttpContext.Current.Trace.Warn( "<<< TreeEdtorGrid.ReloadControlTree" );
		}
		
		/// <summary>
		/// Identifies and dispatches the various click post-back events
		/// </summary>
		/// <param name="eventArgument">String passed from the client to identify how we got here.</param>
		public override void RaisePostBackEvent( String eventArgument ) {
			// Find out how we got here...
			// True when the node's select-link is clicked
			bool selectLinkClicked = false;
			if ( eventArgument.IndexOf( ":select" ) > -1 ) {
				eventArgument = eventArgument.Replace( ":select", "" );
				selectLinkClicked = true;
			}
			// True when the node's edit-link is clicked
			bool editLinkClicked = false;
			if ( eventArgument.IndexOf( ":edit" ) > -1 ) {
				eventArgument = eventArgument.Replace( ":edit", "" );
				editLinkClicked = true;
			}
			// Get a ref to the node-control that was clicked...
			TreeNodeControl clickedNode = (TreeNodeControl) this.Page.FindControl( eventArgument );
			if ( selectLinkClicked ) {
				// Here, the select-link was clicked ...
				// We want to identify the node clicked as the new "selected" node for the panel
				this.SelectedNodeKey = clickedNode.Key;
				// ... and fire the event...
				this.OnNodeSelectedChanged( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			} else if ( editLinkClicked ) {
				// Here, the edit-link was clicked ...
				// We want to identify the node clicked as the new "selected" node for the panel
				this.SelectedNodeKey = clickedNode.Key;
				// ... and fire the event...
				this.OnNodeEdit( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			} else {
				// Here, a node's expand/collapse glyph was clicked...
				// We want to change it's IsExpanded state...
				clickedNode.IsExpanded = ! clickedNode.IsExpanded;
				// Fire the event ...
				this.OnNodeExpandedChanged( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			}
		}
		
		#endregion
	}
	
	/// <summary>
	/// Delegate type used for our custom node-clicked events
	/// </summary>
	public delegate void TreePanelNodeEditEventHandler(object sender, TreePanelNodeEditEventArgs e);

	/// <summary>
	/// EventArgs for creating/editing nodes
	/// </summary>
	public class TreePanelNodeEditEventArgs : System.EventArgs {
		public TreeNode Node {
			get { return ( this.node ); }
		}
		private TreeNode node;
		
		public string CommandName {
			get { return ( this.commandName ); }
		}
		private string commandName;
		
		public TreePanelNodeEditEventArgs( TreeNode n, string cmd ) {
			this.node = n;
			this.commandName = cmd;
		}
		
	}
	
}

