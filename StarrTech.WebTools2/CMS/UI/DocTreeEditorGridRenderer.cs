using System;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools.TreeUI;


namespace StarrTech.WebTools.CMS.UI {
	
	/// <summary>
	/// Custom TreeRenderer for DocTreeEditorGrid.
	/// </summary>
	public class DocTreeEditorGridRenderer : TreeEditorGridRenderer {
		
		/// <summary>
		/// The image that appears on expanded nodes that contain children
		/// </summary>
		public string ExpandedImage {
			get { return ( this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.expandedImage ) ); }
			set { this.expandedImage = value; }
		}
		private string expandedImage = "StarrTech.WebTools.Resources.treeui.minus.gif";
		
		/// <summary>
		/// The image that appears on collapsed nodes that contain children
		/// </summary>
		public string CollapsedImage {
			get { return ( this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.collapsedImage ) ); }
			set { this.collapsedImage = value; }
		}
		private string collapsedImage = "StarrTech.WebTools.Resources.treeui.plus.gif";
		
		/// <summary>
		/// The image that appears on nodes without children
		/// </summary>
		public string NullImage {
			get { return ( this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.nullImage ) ); }
			set { this.nullImage = value; }
		}
		private string nullImage = "StarrTech.WebTools.Resources.treeui.null.gif";
		
		/// <summary>
		/// The image that appears on expanded nodes that contain children
		/// </summary>
		public string OpenFolderImage {
			get { return ( this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.openFolderImage ) ); }
			set { this.openFolderImage = value; }
		}
		private string openFolderImage = "StarrTech.WebTools.Resources.treeui.openfolder.gif";
		
		/// <summary>
		/// The image that appears on collapsed nodes that contain children
		/// </summary>
		public string ClosedFolderImage {
			get { return ( this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.closedFolderImage ) ); }
			set { this.closedFolderImage = value; }
		}
		private string closedFolderImage = "StarrTech.WebTools.Resources.treeui.closedfolder.gif";
		
		/// <summary>
		/// Exposes the base constructor
		/// </summary>
		/// <param name="treePanel">The TreeEditorGrid instance that is using this instance for rendering</param>
		public DocTreeEditorGridRenderer( TreeEditorGrid treePanel ) : base( treePanel ) {}
		
		/// <summary>
		/// Called as the first step of rendering each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer ) {
			this.rowIndex++;
			bool isCurrent = node.Key == this.TreePanel.SelectedNodeKey;
			string link = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID + ( isCurrent ? ":edit" : ":select" ) );
			string bgColor = ColorTranslator.ToHtml( isCurrent ? this.TreePanel.SelectedDataBackColor :
				( this.rowIndex % 2 != 0 ? this.TreePanel.DataBackColor : this.TreePanel.AltDataBackColor ) 
			);
			string rowHilightColor = ColorTranslator.ToHtml( this.TreePanel.RowHilightedBackColor );
			string fgColor = ColorTranslator.ToHtml( isCurrent ? this.TreePanel.SelectedDataForeColor : this.TreePanel.DataForeColor );
			writer.WriteBeginTag( "tr" );
			writer.WriteAttribute( "style", string.Format( "color:{0}; background-color:{1};", fgColor, bgColor ) );
			writer.WriteAttribute( "onmouseover", 
				string.Format( "javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';", 
				isCurrent ? bgColor : rowHilightColor 
			) );
			writer.WriteAttribute( "onmouseout", 
				string.Format("javascript:this.style.cursor='auto';this.style.backgroundColor='{0}';", bgColor ) );
			writer.WriteAttribute( "onclick", link + ";" );
			writer.Write( HtmlTextWriter.TagRightChar );
			writer.WriteBeginTag( "td" );
			writer.WriteAttribute( "style", "vertical-align:top; padding-left:" + ((node.Indent * IndentPixels) + 4) + "px;" );
			writer.WriteAttribute( "nowrap", "true" );
			writer.Write( HtmlTextWriter.TagRightChar );
		}
		
		/// <summary>
		/// Called when rendering the outline (expand/collapse) glyph for each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer ) {
			string imgTag = "<img src='{0}' border='0' align='top' style='margin-right:12px;' onclick=\"{1}\" />";
			if ( node.IsFolder ) {
				string script = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID ) + ";window.event.cancelBubble = true;";
				writer.Write( string.Format( imgTag, ( node.IsExpanded ? this.ExpandedImage : this.CollapsedImage ), script ) );
			} else {
				writer.Write( string.Format( imgTag, this.NullImage, "" ) );
			}
		}
		
		/// <summary>
		/// Called when rendering the text/details for each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderNodeControlText( TreeNodeControl node, HtmlTextWriter writer ) {
			string imgTag = "<img src='{0}' border='0' align='top' style='margin-right:8px;' />";
			writer.Write( string.Format( imgTag, ( node.HasControls() ? node.IsExpanded ? this.OpenFolderImage : this.ClosedFolderImage : this.ClosedFolderImage ) ) );
			writer.WriteBeginTag( "span" );
			writer.WriteAttribute( "class", this.TreePanel.CssClass );
			writer.Write( HtmlTextWriter.TagRightChar );
			if ( node.Key == this.TreePanel.SelectedNodeKey ) {
				writer.Write( string.Format( "<b>{0}</b>", node.Title ) );
			} else {
				writer.Write( node.Title );
			}
			writer.WriteEndTag( "span" );
			writer.WriteEndTag( "td" );
		}
		
	}
	
}
