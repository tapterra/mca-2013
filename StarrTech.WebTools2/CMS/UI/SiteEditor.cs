using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.TreeUI;
using StarrTech.WebTools.DBUI;

namespace StarrTech.WebTools.CMS.UI {
	
	/// <summary>
	/// UI component for administration of the StarrTech CMS.
	/// </summary>
	public class SiteEditor : BaseTreeEditor {

		#region Permissions Properties

		public bool CanAddNodes {
			get {
				object obj = this.ViewState["CanAddNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanAddNodes"] = value; }
		}

		public bool CanEditNodes {
			get {
				object obj = this.ViewState["CanEditNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanEditNodes"] = value; }
		}

		public bool CanMoveNodes {
			get {
				object obj = this.ViewState["CanMoveNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanMoveNodes"] = value; }
		}

		public bool CanDeleteNodes {
			get {
				object obj = this.ViewState["CanDeleteNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanDeleteNodes"] = value; }
		}

		public bool CanApproveNodes {
			get {
				object obj = this.ViewState["CanApproveNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanApproveNodes"] = value; }
		}

		public bool CanAddItems {
			get {
				object obj = this.ViewState["CanAddItems"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanAddItems"] = value; }
		}

		public bool CanEditItems {
			get {
				object obj = this.ViewState["CanEditItems"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanEditItems"] = value; }
		}

		public bool CanDeleteItems {
			get {
				object obj = this.ViewState["CanDeleteItems"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanDeleteItems"] = value; }
		}

		public bool CanApproveItems {
			get {
				object obj = this.ViewState["CanApproveItems"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanApproveItems"] = value; }
		}

		#endregion

		#region Constructors

		public SiteEditor() : base() {}
		
		#endregion
		
		#region Methods 
		
		protected override void CreateChildControls() {
			// Build the controls
			this.nodePanel = new SiteNodeFormPanel();
			// HACK: The following line looks useless, but without it, for some reason the event-handlers 
			// for the Save & Cancel buttons on the form don't get triggered ?
			// The button is being assigned a unique ID, so don't bother checking that...
			this.nodePanel.FormController.SaveButton.Text = this.NodePanel.FormController.SaveButton.Text;
			this.nodePanel.Width = new Unit( "100%" );
			this.nodePanel.Visible = false;
			
			// Build the TreePanel
			this.treePanel = new TreeEditorPanel();
			this.treePanel.Width = new Unit( "100%" );
			this.treePanel.Visible = true;
			
			// ValidationMessage
			this.validationMessage = new ValidationSummary();
			this.validationMessage.DisplayMode = ValidationSummaryDisplayMode.BulletList;
			this.validationMessage.ShowSummary = false;
			this.validationMessage.ShowMessageBox = true;
			this.validationMessage.Font.Bold = true;
			this.validationMessage.Font.Name = "Verdana";
			this.validationMessage.Font.Size = new FontUnit( "10px" );
			this.validationMessage.HeaderText = "Please correct the following errors before proceeding:";
			this.validationMessage.BorderStyle = BorderStyle.Solid;
			this.validationMessage.BorderWidth = new Unit( "1px" );
			this.validationMessage.BorderColor = Color.FromName( "#993333" );
			this.validationMessage.BackColor = Color.FromName( "#FFEEEE" );
			this.validationMessage.Width = new Unit( "100%" );
			this.validationMessage.Style["margin"] = "10px";
			this.validationMessage.Style["padding"] = "10px";
			
			// Put 'em together
			this.Controls.Clear();
			this.Controls.Add( this.nodePanel );
			this.Controls.Add( this.treePanel );
			this.Controls.Add( this.validationMessage );
			// Wire in the event handlers
			this.NodePanel.SaveForm += new EventHandler( this.SaveFormNode );
			this.NodePanel.CloseForm += new EventHandler( this.CancelForm );
			this.TreePanel.Grid.NodeEdit += new TreePanelNodeClickEventHandler( this.SetupNodeFormPanel );
			this.TreePanel.NodeEdit += new TreePanelNodeEditEventHandler( this.SetupNodeFormPanel );
		}

		protected internal override void SetupNodeFormPanel( Object sender, TreePanelNodeClickEventArgs args ) {
			this.SetupNodeFormPanel( this.TreePanel.CurrentNode, TreeEditorCommandNames.NodeEdit );
		}
		protected internal override void SetupNodeFormPanel( Object sender, TreePanelNodeEditEventArgs args ) {
			this.SetupNodeFormPanel( args.Node, args.CommandName );
		}
		protected internal override void SetupNodeFormPanel( TreeNode CurrentNode, string CommandName ) {	
			this.TreePanel.Visible = false;
			this.NodePanel.Visible = true;
			SiteNodeFormPanel formPanel = this.NodePanel as SiteNodeFormPanel;
			if ( CommandName == TreeEditorCommandNames.NodeEdit ) {
				// We're editing an existing node ...
				// Set up the node-properties area for the current node
				formPanel.FormController.NodeID = CurrentNode.ID;
				formPanel.FormController.ParentNodeID = CurrentNode.ParentID;
				formPanel.PathKey = CurrentNode.PathKey;
				formPanel.Title = CurrentNode.Title;
				formPanel.Comments = CurrentNode.Comments;
				formPanel.IsVisibleXBox.Checked = CurrentNode.IsVisible;
				formPanel.ContentTypeListBox.Enabled = false;
				if ( CurrentNode.ParentNode.IncludeSubnodeMenuItems ) {
					formPanel.ItemOnMainMenuXBox.Checked = CurrentNode.IncludeAsMenuItem;
					formPanel.SubnodesOnMainMenuXBox.Checked = CurrentNode.IncludeSubnodeMenuItems; // || CurrentNode.ParentID == 0;
				} else {
					formPanel.ItemOnMainMenuXBox.Checked = true;
					formPanel.SubnodesOnMainMenuXBox.Checked = false;
				}
				formPanel.ContentPanel.Visible = true;
				formPanel.ID = "NodePanel";
				formPanel.Height = new Unit( "100%" );
				// Hide all the content-type editors
				ContentType type = CurrentNode.ContentType;
				string homeEditorID = this.GetContentTypeHomeEditorID( type );
				foreach ( Control ctl in formPanel.ContentPanel.Controls ) {
					ctl.Visible = ( ctl.ID == homeEditorID );
				}
				// ...then set up the editor we want to use
				this.LoadCustomContentEditor( formPanel.ContentPanel, CurrentNode );
				// Set up the content-type listbox and description for our content type
				formPanel.ContentTypeListBox.Items.Add( new ListItem( type.Label, type.TypeKey ) );
				formPanel.ContentTypeListBox.SelectedValue = CurrentNode.TypeKey;
				formPanel.Description = string.Format( "<div style='padding:4px'>{0}</div>", type.Description );
				if ( type.EditorType == ContentTypeEditors.none ) {
					formPanel.ContentPanel.Visible = false;
				}
				formPanel.DescriptionBox.Visible = true;
			} else {
				// We're adding a new node...
				formPanel.FormController.NodeID = -1;
				formPanel.IsVisibleXBox.Checked = false;
				formPanel.ContentTypeListBox.Visible = true;
				formPanel.ContentTypeListBox.Enabled = true;
				formPanel.ItemOnMainMenuXBox.Checked = false;
				formPanel.SubnodesOnMainMenuXBox.Checked = false;
				formPanel.DescriptionBox.Visible = true;
				formPanel.Description = string.Format( "Please select a {0} above and click Save.", formPanel.ContentTypeListBox.Label );
				// Set up the ContentTypeListBox with valid values for this node
				formPanel.ContentTypeListBox.Items.Clear();
				foreach ( ContentType type in ContentTypeManager.ContentTypes ) {
					// We can't add any child-nodes if the current-item content-type is MustBeRoot...
					if ( CommandName == TreeEditorCommandNames.NodeInsertChild && type.MustBeRoot ) {
						continue;
					}
					// If we're not editing the base-site tree, skip any types marked as BaseSiteOnly...
					if ( this.TreeKey != ContentPublisher.MainNavTreeKey && type.BaseSiteOnly ) {
						continue;
					}
					if ( type.CanBeOnlyOne ) {
						// If this type is already in use, skip it
						if ( this.DataTree.FindNodeByTypeKey( type.TypeKey ) != null ) {
							continue;
						}
					}
					formPanel.ContentTypeListBox.Items.Add( new ListItem( type.Label, type.TypeKey ) );
				}
				// Finish setting up the panel
				switch ( CommandName ) {
					case TreeEditorCommandNames.NodeInsertChild:
						// if we're inserting a new child node, the parentID is the ID of the currently-selected node
						formPanel.FormController.ParentNodeID = CurrentNode.ID;
						break;
					case TreeEditorCommandNames.NodeInsertNext:
						// if we're inserting a new sibling, the parentID is the ID of the parent of the currently-selected node
						// (...unless there is no currently-selected node, in which case the parentID is zero).
						formPanel.FormController.ParentNodeID = ( CurrentNode != null ? CurrentNode.ParentID : 0 );
						break;
				}
				formPanel.PathKey = "";
				formPanel.Title = "";
				formPanel.Comments = "";
				formPanel.ContentPanel.Visible = false;
			}
		}

		protected internal string GetContentTypeEditorID( ContentType type ) {
			return ( type.TypeKey + "_editbox" );
		}

		protected internal string GetContentTypeHomeEditorID( ContentType type ) {
			return ( type.TypeKey + "_home_editbox" );
		}

		/// <summary>
		/// Load the editor panel's controls with the content-item part values
		/// </summary>
		/// <param name="ContentPanel">Panel containing the content-type editors in the SiteEditor</param>
		/// <param name="CurrentNode">TreeNode to be edited (from which the ContentItem is found)</param>
		protected void LoadCustomContentEditor( Panel contentPanel, TreeNode currentNode ) {
			ContentType nodeType = ContentTypeManager.ContentTypes[currentNode.TypeKey];
			Control editor = contentPanel.FindControl( this.GetContentTypeEditorID( nodeType ) );
			if ( editor != null ) {
				// Show the editor panel
				editor.Visible = true;
				// If it needs initialization, do it...
				IAdminDataGridContentEditor editorEx = editor as IAdminDataGridContentEditor;
				if ( editorEx != null ) {
					// Make sure the grid is up-to-date...
					editorEx.DataGrid.InitPanel();
					// We already have a ValidationSummary, so the editor don't need one...
					editorEx.DataGrid.MessagePanel.ValidationMessage.Visible = false;
					// Pass it the current node ID 
					editorEx.NodeID = currentNode.ID;
				}
				switch ( nodeType.EditorType ) {
					case ContentTypeEditors.ascx:
						// we're done
						break;
					case ContentTypeEditors.contentgrid:
						ContentItemsGrid grid = editor as ContentItemsGrid;
						grid.NodeID = currentNode.ID;
						grid.AdminGrid.BindGrid();
						break;
					case ContentTypeEditors.contentform:
						ContentItemFormPanel form = editor as ContentItemFormPanel;
						form.NodeID = currentNode.ID;
						if ( form != null ) {
							form.LoadContentEditor( currentNode.DefaultContentItem );
						}
						break;
					default:
						break;
				}
			}
		}

		/// <summary>
		/// Called during page Init to load/build/instantiate the various content-type editor panels used by the site 
		/// </summary>
		protected void SetupCustomContentEditors( Page page, Panel contentPanel ) {
			this.SetupCustomContentEditors( page, contentPanel, true );
		}
		protected void SetupCustomContentEditors( Page page, Panel contentPanel, bool allowAscx ) {
			contentPanel.Controls.Clear();
			foreach ( ContentType type in ContentTypeManager.ContentTypes ) {
				// Based on this ContentType's EditorType, create an editor and add it to the ContentPanel
				Control editor;
				//if ( type.HomePageParts.Count > 0 ) {
				//  editor = ContentItemFormPanel.BuildContentEditorPanel( type, type.HomePageParts );
				//  editor.ID = GetContentTypeHomeEditorID( type );
				//  contentPanel.Controls.Add( editor );
				//}
				switch ( type.EditorType ) {
					case ContentTypeEditors.none:
						continue;
					case ContentTypeEditors.ascx:
						if ( !allowAscx ) { continue; }
						// This is a user-control editor - load it
						string EditorSrc = type.EditorSrc;
						editor = page.LoadControl( EditorSrc );
						break;
					case ContentTypeEditors.contentgrid:
						if ( !allowAscx ) { continue; }
						ContentItemsGrid ed = new ContentItemsGrid();
						ed.IsSingleNodeMode = true;
						editor = ed;
						break;
					case ContentTypeEditors.contentform:
						// This is a dynamic content-type - build the editor based on the type's ContentItemParts
						ContentItemFormPanel form = new ContentItemFormPanel( type, false );
						form.IncludeNodePicker = false;
						editor = form;
						break;
					default:
						continue;
				}
				editor.ID = this.GetContentTypeEditorID( type );
				HttpContext.Current.Trace.Warn( "CMS", ">>> SiteEditor.SetupCustomContentEditors: contentPanel.Controls.Add( editor )" );
				contentPanel.Controls.Add( editor );
				HttpContext.Current.Trace.Warn( "CMS", "<<< SiteEditor.SetupCustomContentEditors: contentPanel.Controls.Add( editor )" );
			}
		}

		protected internal void ReadNodeForm( TreeNode currentNode ) {
			HttpContext.Current.Trace.Warn( ">>> SiteEditor.ReadFormNode" );
			SiteNodeFormPanel formPanel = this.NodePanel as SiteNodeFormPanel;
			currentNode.PathKey = formPanel.PathKey;
			currentNode.Title = formPanel.Title;
			currentNode.Comments = formPanel.Comments;
			currentNode.TreeID = this.DataTree.ID;
			currentNode.IsVisible = formPanel.IsVisibleXBox.Checked;
			currentNode.TypeKey = formPanel.ContentTypeListBox.SelectedValue;
			currentNode.IncludeAsMenuItem = formPanel.ItemOnMainMenuXBox.Checked;
			currentNode.IncludeSubnodeMenuItems = formPanel.SubnodesOnMainMenuXBox.Checked;
			HttpContext.Current.Trace.Warn( "<<< SiteEditor.ReadFormNode" );
		}
		
		/// <summary>
		/// Event handler for the Node Panel's Save button.
		/// </summary>
		protected internal void SaveFormNode( Object sender, EventArgs args ) {
			HttpContext.Current.Trace.Warn( ">>> SiteEditor.SaveFormNode" );
			int nodeID = this.NodePanel.FormController.NodeID;
			this.Page.Validate();
			if ( ! this.Page.IsValid ) return;
			if ( nodeID <= 0 ) {
				// We're inserting a new node...
				TreeNode newNode = new TreeNode();
				this.ReadNodeForm( newNode );
				int parentID = this.NodePanel.FormController.ParentNodeID;
				TreeNodeControl newControl = null;
				TreeNodeControl currentNodeControl = this.TreePanel.Grid.SelectedNode;
				TreeNode currentDataNode = ( currentNodeControl != null ? this.DataTree.FindNode( Convert.ToInt32(currentNodeControl.Key) ) : null );
				if ( parentID == 0 ) {
					if ( currentNodeControl != null ) {
						// Insert it into the DataTree
						this.DataTree.InsertNodeAfter( newNode, currentDataNode );
						// and into the UI...
						newControl = this.TreePanel.Grid.AddNodeAfter( newNode.Title, newNode.ID.ToString(), currentNodeControl );
					} else {
						// Insert it into the DataTree
						this.DataTree.AddNode( newNode );
						// and into the UI...
						newControl = this.TreePanel.Grid.AddNode( newNode.Title, newNode.ID.ToString() );
					}
				} else {
					// Insert it into the DataTree
					// Is this an InsertNext or InsertParent?
					TreeNodeControl parentControl = this.TreePanel.Grid.FindTreeNodeControl( parentID.ToString() );
					TreeNode parentNode = this.DataTree.FindNode( parentID );
					if ( ( currentNodeControl != null ) && ( parentID == Convert.ToInt32(currentNodeControl.Key) ) ) {
						// If its an InsertChild, the parentID we got will equal the ID of the currently-selected node
						if ( parentNode != null ) {
							parentNode.AddNode( newNode );
							newControl = parentControl.AddNode( newNode.Title, newNode.ID.ToString() );
							parentControl.IsExpanded = true;
						}
					} else {
						// We're gonna insert the new node following the currently-selected node
						parentNode.InsertNodeAfter( newNode, currentDataNode );
						newControl = parentControl.AddNodeAfter( newNode.Title, newNode.ID.ToString(), false, currentNodeControl );
					}
				}
				if ( newControl != null ) {
					newControl["key"] = newNode.PathKey;
					this.TreePanel.Grid.SelectedNodeKey = newControl.Key;
				}
				this.SetupNodeFormPanel( newNode, TreeEditorCommandNames.NodeEdit );
			} else {
				// We're saving an existing node - replacing the SelectedNode
				TreeNode node = this.TreePanel.CurrentNode;
				this.ReadNodeForm( node );
				node.Persist();
				this.TreePanel.Grid.SelectedNode["key"] = node.PathKey;
				this.TreePanel.Grid.SelectedNode.Title = node.Title;
				this.TreePanel.Grid.SelectedNode.Attributes["ismenuitem"] = node.IncludeAsMenuItem.ToString().ToLower();
				this.TreePanel.Grid.SelectedNode.Attributes["issubmenu"] = node.IncludeSubnodeMenuItems.ToString().ToLower();
				this.TreePanel.Grid.SelectedNode.Attributes["isvisible"] = node.IsVisible.ToString().ToLower();
				this.SaveCustomContentItem();
				this.SetupTreeGridPanel();
			}
			// Since we've just made a change to the site tree, we need to clear it from the ContentPublisher's cache 
			// so that it shows up in the menus...
			ContentPublisher.ResetSiteTreeCache( this.TreeKey );
			HttpContext.Current.Trace.Warn( "<<< SiteEditor.SaveFormNode" );
		}

		private void SaveCustomContentItem() {
			TreeNode currentNode = this.TreePanel.CurrentNode;
			// What's the current ContentType? ...
			ContentType nodeType = ContentTypeManager.ContentTypes[currentNode.TypeKey];
			ContentTypeEditors editorType = nodeType.EditorType;
			if ( editorType == ContentTypeEditors.contentform ) {
				Control editor = this.NodePanel.ContentPanel.FindControl( this.GetContentTypeEditorID( nodeType ) );
				ContentItemFormPanel form = editor as ContentItemFormPanel;
				if ( form != null ) {
					form.ReadContentItem( currentNode.DefaultContentItemID ).Persist();
				}
			}
		}
		
		protected override void OnInit( EventArgs args ) {
			HttpContext.Current.Trace.Warn( "CMS", ">>> SiteEditor.OnInit" );
			// Build the custom content-editors as defined by ContentTypes.config
			this.SetupCustomContentEditors( this.Page, this.NodePanel.ContentPanel );
			// Tie into the AdminDataGrid-based editors' form events
			foreach ( Control editor in this.NodePanel.ContentPanel.Controls ) {
				if ( editor is IAdminDataGridContentEditor ) {
					IAdminDataGridContentEditor gridEditor = editor as IAdminDataGridContentEditor;
					gridEditor.DataGrid.FormControlsInit += new EventHandler( SubGridFormControlsInit );
					gridEditor.DataGrid.FormClosing += new EventHandler( SubGridFormClosing );
				}
			}
			base.OnInit( args );
			HttpContext.Current.Trace.Warn("CMS", "<<< SiteEditor.OnInit" );
		}
		
		private void SubGridFormControlsInit( object sender, EventArgs e ) {
			// this.NodePanel.FormController.Enabled = false;
			this.NodePanel.MainFormTable.Visible = false;
		}
		
		private void SubGridFormClosing( object sender, EventArgs e ) {
			// this.NodePanel.FormController.Enabled = true;
			this.NodePanel.MainFormTable.Visible = true;
		}
		
		#endregion
		
	}
	
	/// <summary>
	/// Interface to be implemented by any content editors using AdminDataGrid
	/// </summary>
	public interface IAdminDataGridContentEditor {
		
		/// <summary>
		/// Allows our grid access to the tree node it is attached to
		/// </summary>
		int NodeID { get; set; }
		
		/// <summary>
		/// Provides the parent tree-editor with access to our grid
		/// </summary>
		AdminDataGrid DataGrid { get; }

	}
	
}
