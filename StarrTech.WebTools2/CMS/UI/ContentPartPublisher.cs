using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.CMS.UI {

	public class ContentPartPublisher : Literal {

		/// <summary>
		/// ID of the ContentItem for this placeholder's page
		/// </summary>
		public int ContentItemID {
			get {
				object obj = ViewState["ContentItemID"];
				if ( obj == null ) {
					if ( this.Page is BasePage ) {
						ViewState["ContentItemID"] = obj = ( this.Page as BasePage ).ContentItemID;
					} else {
						string id = this.Page.Request.QueryString[ContentPublisher.CidQKey];
						if ( !string.IsNullOrEmpty( id ) ) {
							string[] ids = id.Split( ',' );
							int index = ids.Length - 1;
							ViewState["ContentItemID"] = obj = Convert.ToInt32( ids[index] );
						}
					}
				}
				return ( (int) obj );
			}
			set { ViewState["ContentItemID"] = value; }
		}

		/// <summary>
		/// ContentType Part Name for the part this placeholder is to display
		/// </summary>
		public string PartName {
			get {
				object obj = ViewState["PartName"];
				return ( obj == null ? "" : (string) obj );
			}
			set { ViewState["PartName"] = value; }
		}

		/// <summary>
		/// Text to be published for this part
		/// </summary>
		public string PartText {
			get {
				if ( string.IsNullOrEmpty( this.partText ) ) {
					this.partText = ContentPublisher.GetContentPartText( this.ContentItemID, this.PartName ).Trim();
				}
				return ( this.partText );
			}
		}
		private string partText;

		protected override void Render( HtmlTextWriter writer ) {
			writer.Write( this.PartText );
			base.Render( writer );
		}

	}
}
