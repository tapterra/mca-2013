using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using StarrTech.WebTools.TreeUI;

namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Header and command buttons for the NodeFormPanel
	/// </summary>
	public class NodeFormController : Table {
		
		#region Properties 
		
		/// <summary>
		/// The ID of the node we're editing
		/// </summary>
		public int NodeID {
			get { 
				object id = ViewState["NodeID"];
				return ( id == null ? -1 : (int) id ); 
			}
			set { ViewState["NodeID"] = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public override bool Enabled {
			//get { 
			//  return ( base.Enabled );
			//}
			set {
				//base.Enabled = value;
				this.SaveButton.Enabled = value;
				this.CancelButton.Enabled = value;
			}
		}
		
		/// <summary>
		/// The ID of the parent-node of the node we're editing
		/// (needed when inserting new nodes)
		/// </summary>
		public int ParentNodeID {
			get { 
				object id = ViewState["ParentNodeID"];
				return ( id == null ? -1 : (int) id ); 
			}
			set { ViewState["ParentNodeID"] = value; }
		}
				
		/// <summary>
		/// Exposes the Text property of the Header label
		/// </summary>
		public string Header {
			get { 
				this.EnsureChildControls();
				return ( this.HeaderLabel.Text ); 
			}
			set { 
				this.EnsureChildControls();
				this.HeaderLabel.Text = value.Replace(" ", "&nbsp;" );
			}
		}
		
		/// <summary>
		/// Label control for displaying the header.
		/// </summary>
		public Label HeaderLabel {
			get {
				this.EnsureChildControls();
				return ( this.headerLabel );
			}
			set { this.headerLabel = value; }
		}
		private Label headerLabel;
		
		/// <summary>
		/// Button to save the current node and return to the tree-editor.
		/// </summary>
		public System.Web.UI.WebControls.Button SaveButton {
			get {
				this.EnsureChildControls();
				return ( this.saveButton );
			}
			set { this.saveButton = value; }
		}
		private System.Web.UI.WebControls.Button saveButton;
		
		/// <summary>
		/// Button to cancel all changes to the current node and return to the tree-editor.
		/// </summary>
		public System.Web.UI.WebControls.Button CancelButton {
			get {
				this.EnsureChildControls();
				return ( this.cancelButton );
			}
			set { this.cancelButton = value; }
		}
		private System.Web.UI.WebControls.Button cancelButton;
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion
		
		#region Events 
		
		/// <summary>
		/// Raised when any of the button controls are clicked.
		/// </summary>
		public event CommandEventHandler ButtonClick {
			add { Events.AddHandler( ButtonClickEventKey, value ); }
			remove { Events.RemoveHandler( ButtonClickEventKey, value ); }
		}
		protected virtual void OnButtonClick( Object sender, CommandEventArgs args ) {
			CommandEventHandler btnClickHandler = (CommandEventHandler) Events[ButtonClickEventKey];
			if ( btnClickHandler != null ) btnClickHandler( sender, args );
		}
		private static readonly object ButtonClickEventKey = new object();
		
		#endregion
		
		#region Constructors
		
		public NodeFormController() :base() {
			this.CellSpacing = 0;
			this.CellPadding = 0;
			this.BorderStyle = BorderStyle.None;
			this.Width = new Unit( "100%" );
		}
		
		#endregion
		
		#region Methods
		
		protected override void CreateChildControls() {
			
			#region HeaderLabel
			this.headerLabel = new Label();
			this.headerLabel.Font.Name = "Tahoma";
			this.headerLabel.Font.Size = new FontUnit( "11pt" );
			this.headerLabel.Font.Bold = true;
			this.headerLabel.Style["width"] = "100%";
			this.headerLabel.Text = "Section Editor";
			this.headerLabel.ID = string.Format( "{0}_headerLabel", this.ID );
			#endregion
			
			#region SaveButton
			this.saveButton = new System.Web.UI.WebControls.Button();
			this.saveButton.Text = "Save";
			this.saveButton.ToolTip = "Save changes and return to the site outline";
			this.saveButton.Height = new Unit( "24" );
			this.saveButton.Width = new Unit( "50" );
			this.saveButton.Font.Name = "Tahoma";
			this.saveButton.Font.Size = new FontUnit( "8" );
			this.saveButton.Font.Bold = true;
			this.saveButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeFormSave );
			this.saveButton.CommandName = TreeEditorCommandNames.NodeFormSave;
			this.saveButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.saveButton.CausesValidation = true;
			#endregion
			
			#region CancelButton
			this.cancelButton = new System.Web.UI.WebControls.Button();
			this.cancelButton.Text = "Cancel";
			this.cancelButton.ToolTip = "Discard all changes and return to the site outline";
			this.cancelButton.Height = new Unit( "24" );
			this.cancelButton.Width = new Unit( "50" );
			this.cancelButton.Font.Name = "Tahoma";
			this.cancelButton.Font.Size = new FontUnit( "8" );
			this.cancelButton.Font.Bold = true;
			this.cancelButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeFormCancel );
			this.cancelButton.CommandName = TreeEditorCommandNames.NodeFormCancel;
			this.cancelButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.cancelButton.CausesValidation = false;
			#endregion
			
			#region Put it all together
			this.Rows.Clear();
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.Controls.Add( this.HeaderLabel );
			row.Cells.Add( cell );
			Unit buttonCellWd = new Unit( "52" );
			cell = new TableCell();
			cell.Width = buttonCellWd;
			cell.Controls.Add( this.SaveButton );
			row.Cells.Add( cell );
			cell = new TableCell();
			cell.Width = buttonCellWd;
			cell.Controls.Add( this.CancelButton );
			row.Cells.Add( cell );
			this.Rows.Add( row );
			#endregion

		}
		
		#endregion
		
	}
		
}
