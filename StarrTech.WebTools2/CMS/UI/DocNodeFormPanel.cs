using System;
using System.Collections;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.TreeUI;


namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Summary description for DocNodeFormPanel.
	/// </summary>
	public class DocNodeFormPanel : NodeFormPanel {
		
		#region  Properties 
		
		public override string PathKey {
			get { return ( "" ); }
			set {  }
		}
		
		
		public override string Title {
			get { return ( this.TitleBox.Text ); }
			set { this.TitleBox.Text = value; }
		}
		
		
		public override string Comments {
			get { return ( this.CommentsBox.Text ); }
			set { this.CommentsBox.Text = value; }
		}
		
		
		public override string Description {
			get { return ( "" ); }
			set {  }
		}
		
		
		#region Components
		
		public LabeledTextBox TitleBox {
			get {
				this.EnsureChildControls();
				return ( this.titleBox );
			}
		}
		private LabeledTextBox titleBox;
		private RequiredFieldValidator titleBoxReqValidator;
		
		public LabeledTextBox CommentsBox {
			get {
				this.EnsureChildControls();
				return ( this.commentsBox );
			}
		}
		private LabeledTextBox commentsBox;
		
		#endregion
		
		#endregion
		
		#region Constructors 
		public DocNodeFormPanel() : base() {
			this.BackColor = Color.FromName( "#EEEEEE" );
			this.CellSpacing = 0;
			this.CellPadding = 3;
			this.Width = new Unit( "100%" );
			this.Height = new Unit( "100%" );
		}
		
		
		#endregion
		
		#region Methods 
		protected override void CreateChildControls() {
			
			#region FormController
			this.formController = new NodeFormController();
			this.formController.Width = new Unit( "100%" );
			this.formController.HeaderLabel.Text = "Document Library";
			this.formController.ButtonClick += new CommandEventHandler( this.NodeFormCommands );
			#endregion
			
			#region TitleBox 
			this.titleBox = new LabeledTextBox();
			this.titleBox.Label = "Folder Title";
			this.titleBox.Width = new Unit( "100%" );
			this.titleBox.ID = "NodeFormPanel_TitleBox"; // Required for validators
			
			this.titleBoxReqValidator = new RequiredFieldValidator();
			this.titleBoxReqValidator.ControlToValidate = this.titleBox.ID;
			this.titleBoxReqValidator.Display = ValidatorDisplay.None;
			this.titleBoxReqValidator.ErrorMessage = "Please enter a Title for this directory.";
			#endregion
			
			#region CommentsBox
			this.commentsBox = new LabeledTextBox();
			this.commentsBox.Label = "Comments";
			this.commentsBox.Width = new Unit( "100%" );
			this.commentsBox.TextMode = TextBoxMode.MultiLine;
			this.commentsBox.Rows = 4;
			this.commentsBox.Wrap = true;
			#endregion
			
			#region ContentPanel
			this.contentPanel = new Panel();
			this.contentPanel.Style["padding"] = "6px";
			this.contentPanel.Width = new Unit( "100%" );
			this.contentPanel.Height = new Unit( "100%" );
			this.contentPanel.BackColor = Color.White;
			this.contentPanel.BorderStyle = BorderStyle.Solid;
			this.contentPanel.BorderWidth = 1;
			this.contentPanel.BorderColor = Color.Gray;
			#endregion
			
			#region Stack it up...
			this.Rows.Clear();
			
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.ColumnSpan = 2;
			cell.Controls.Add( this.formController );
			row.Cells.Add( cell );
			this.Rows.Add( row );
			
			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.titleBox );
			cell.Controls.Add( this.titleBoxReqValidator );
			row.Cells.Add( cell );
			this.Rows.Add( row );
			
			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.commentsBox );
			row.Cells.Add( cell );
			this.Rows.Add( row );
			
			row = new TableRow();
			cell = new TableCell();
			cell.Controls.Add( this.contentPanel );
			row.Cells.Add( cell );
			this.Rows.Add( row );
			
			#endregion
		}
		
		#endregion
		
	}
}
