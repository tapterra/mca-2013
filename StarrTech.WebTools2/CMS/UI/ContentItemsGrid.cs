using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.DBUI;
using System.Drawing;
using StarrTech.WebTools.Data;

namespace StarrTech.WebTools.CMS.UI {

	public class ContentItemsGrid : Panel, IAdminDataGridContentEditor {

		#region Properties

		public int NodeID {
			get {
				object id = ViewState["NodeID"];
				return ( id == null ? -1 : (int) id );
			}
			set {
				ViewState["NodeID"] = value;
				//this.IsSingleNodeMode = ( value > 0 );
			}
		}

		public string ContentTypeKey {
			get {
				object obj = ViewState["ContentTypeKey"];
				return ( obj == null ? "contentitem" : (string) obj );
			}
			set {
				ViewState["ContentType"] = value;
				this.ConfigureGrid();
			}
		}

		public bool IsSingleNodeMode {
			get {
				object obj = ViewState["IsSingleNodeMode"];
				return ( obj == null ? false : (bool) obj );
			}
			set {
				ViewState["IsSingleNodeMode"] = value;
				this.ConfigureGrid();
			}
		}

		public int CurrentUserID {
			get {
				return ( ContentTypeManager.CurrentUserID );
			}
		}

		AdminDataGrid IAdminDataGridContentEditor.DataGrid {
			get {
				this.EnsureChildControls();
				return ( this.AdminGrid );
			}
		}

		public override Unit Width {
			get {
				this.EnsureChildControls();
				return ( AdminGrid.Width );
			}
			set {
				this.EnsureChildControls();
				AdminGrid.Width = value;
			}
		}

		public AdminDataGrid AdminGrid;

		public ContentItemFormPanel ItemPanel;

		public Table SearchPanel;

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}

		public bool CanEditRecords {
			get { return ( this.AdminGrid.CanEditRecord ); }
			set { this.AdminGrid.CanEditRecord = value; }
		}

		public bool CanAddRecords {
			get { return ( this.AdminGrid.CanAddRecord ); }
			set { this.AdminGrid.CanAddRecord = value; }
		}

		public bool CanDeleteRecords {
			get { return ( this.AdminGrid.CanDeleteRecord ); }
			set { this.AdminGrid.CanDeleteRecord = value; }
		}

		protected LabeledTextBox STitleBox;
		protected LabeledTextBox SCreatedStartDTBox;
		protected LabeledTextBox SCreatedEndDTBox;
		protected LabeledDropDownList SIsApprovedLBox;

		public string EyeballGlyphTag {
			get { return ( "<img src='../images/eyeball.gif' />" ); }
		}

		#endregion

		#region Constructors

		public ContentItemsGrid() {
			this.NodeID = -1;
		}
		
		public ContentItemsGrid( string ContentTypeKey ) {
			this.NodeID = -1;
			this.ContentTypeKey = ContentTypeKey;
		}

		#endregion

		#region Methods

		protected void ConfigureGrid() {
			this.EnsureChildControls();
			this.AdminGrid.SearchController.Visible = true;
			this.AdminGrid.SearchController.NewButton.Visible = this.CanAddRecords;
			this.AdminGrid.BorderStyle = BorderStyle.None;
			if ( this.IsSingleNodeMode ) {
				this.ItemPanel.IncludeNodePicker = false;
				this.AdminGrid.BackColor = Color.White;
				this.AdminGrid.AllowUserSorting = false;
				this.AdminGrid.IncludeReorderControls = true;
				this.AdminGrid.GridController.SortExpression = "n.rank";
				this.AdminGrid.Grid.PageSize = 1000;
				this.AdminGrid.GridPageController.Visible = false;
				this.SearchPanel.Visible = false;
				this.AdminGrid.SearchController.SearchButton.Visible = false;
				this.AdminGrid.SearchController.ShowAllButton.Visible = false;
			} else {
				this.ItemPanel.IncludeNodePicker = true;
				this.AdminGrid.BackColor = Color.White;
				this.AdminGrid.AllowUserSorting = true;
				this.AdminGrid.IncludeReorderControls = false;
				this.AdminGrid.GridController.SortExpression = "c.title";
				this.AdminGrid.Grid.PageSize = 20;
				this.AdminGrid.GridPageController.Visible = true;
				this.SearchPanel.Visible = true;
				this.AdminGrid.SearchController.Visible = true;
				this.AdminGrid.SearchController.SearchButton.Visible = true;
				this.AdminGrid.SearchController.ShowAllButton.Visible = true;
			}
		}

		void SearchControlsInit( object sender, EventArgs e ) {
			// TODO: Make this actually work...
			//this.SCreatedByLBox.Items.Add( new ListItem( "-- Any --", "-1" ) );
		}

		public void ReadSearchControls( Object sender, EventArgs args ) {
			ContentItem.SNodeID = this.NodeID;
			if ( this.IsSingleNodeMode ) {
				TreeNode.CleanupItemRanks( this.NodeID );
				this.AdminGrid.GridController.SortExpression = "n.rank";
			} else {
				ContentItem.SApprovals = Convert.ToInt32( this.SIsApprovedLBox.SelectedValue );
				ContentItem.SCreatedByUserID = -1; //Convert.ToInt32( this.SCreatedByLBox.SelectedValue );
				try {
					ContentItem.SCreatedEnd = DateTime.Parse( this.SCreatedEndDTBox.Text );
				} catch {
					ContentItem.SCreatedEnd = DateTime.MaxValue;
					this.SCreatedEndDTBox.Text = "";
				}
				try {
					ContentItem.SCreatedStart = DateTime.Parse( this.SCreatedStartDTBox.Text );
				} catch {
					ContentItem.SCreatedStart = DateTime.MinValue;
					this.SCreatedStartDTBox.Text = "";
				}
				ContentItem.STitle = this.STitleBox.Text;
			}
		}

		public void InitFormControls( Object sender, EventArgs args ) {
			this.ItemPanel.ContentItemID = this.AdminGrid.FormController.RecordID;
			if ( this.ItemPanel.ContentItemID > 0 ) {
				// This is an existing ContentItem
				this.ItemPanel.ContentItem = new ContentItem( this.AdminGrid.FormController.RecordID );
			} else {
				// This is a brand-spankin new ContentItem...
				if ( this.IsSingleNodeMode ) {
					// Get a ref to our parent node...
					TreeNode node = new TreeNode( this.NodeID );
					//this.ItemPanel.ContentItem = node.NewContentItem();
					//this.AdminGrid.FormController.RecordID = this.ItemPanel.ContentItem.ID;
					ContentType type = node.TypeKey == "contentcatalog" ? ContentTypeManager.ContentTypes["contentitem"] : node.ContentType;
					ContentItem item = ContentTypeManager.NewContentItem( type );
					item.Rank = node.HighestItemRank + 1;
					this.ItemPanel.ContentItem = item;
					this.AdminGrid.FormController.RecordID = -1;
					// 
					if ( this.ItemPanel.ContentItem.Rank == 0 ) {
						this.ItemPanel.Title = string.Format( "{0} Home Page", node.Title );
					} else {
						this.ItemPanel.Title = string.Format( "{0} Page {1}", node.Title, this.ItemPanel.ContentItem.Rank );
					}
					this.ItemPanel.ContentTypeKey = node.TypeKey;
				} else {
					this.ItemPanel.ContentItem = new ContentItem( "contentitem", ContentTypeManager.CurrentUserID );
					this.AdminGrid.FormController.RecordID = this.ItemPanel.ContentItem.ID;
					this.ItemPanel.ContentTypeKey = this.ItemPanel.ContentItem.TypeKey;
				}
			}
			this.ItemPanel.InitContentEditor();
		}

		private static string IDList( List<int> IDs ) {
			string ids = "";
			foreach ( int id in IDs ) {
				ids += string.Format( "{0}, ", id );
			}
			return ( ids + "-1" );
		}

		public void ReadFormControls( Object sender, EventArgs args ) {
			ContentItem item = this.ItemPanel.ReadContentItem( this.AdminGrid.FormController.RecordID );
			this.AdminGrid.DBItem = item;
		}

		protected void CurrentItemSaved( Object sender, EventArgs args ) {
			ContentItem item = this.AdminGrid.DBItem as ContentItem;
			if ( this.IsSingleNodeMode ) {
				// Create a single link for the current NodeID
				TreeNode node = new TreeNode( this.NodeID );
				string sql = "select rank from CMS_NODE_ITEMS where cms_item_id = @cms_item_id and tree_node_id = @tree_node_id; ";
				int rank = -1;
				using ( SqlDatabase db = new SqlDatabase() ) {
					object obj = db.ExecuteScalar( sql, db.NewParameter( "@cms_item_id", item.ID ), db.NewParameter( "@tree_node_id", node.ID ) );
					if ( obj != null ) {
						rank = (int) obj;
					}
				}
				if ( rank == -1 ) {
					node.LinkContentItem( item, node.HighestItemRank + 1 );
				}
			} else {
				this.UpdateNodeLinks( item );
			}
		}

		protected void UpdateNodeLinks( ContentItem item ) {
			// Read the NodePicker checkboxes and update the CMS-NODE_ITEMS links for this item
			if ( ( !this.IsSingleNodeMode ) && this.ItemPanel.IncludeNodePicker ) {
				List<int> currentCheckedNodeIDs = this.ItemPanel.NodePicker.GetCheckedNodeIDs();
				this.ItemPanel.NodePicker.ContentItemID = item.ID;
				this.ItemPanel.NodePicker.LoadCheckedNodeIDs();
				List<int> oldCheckedNodeIDs = this.ItemPanel.NodePicker.CheckedNodeIDs;
				// We don't just want to wipe the existing link records and recreate them, for fear of losing the ranks, etc.
				// First, delete any CMS_NODE_ITEMS records that are in oldCheckedNodeIDs and not in currentCheckedNodeIDs
				List<int> nodeIDsToDelete = new List<int>();
				foreach ( int id in oldCheckedNodeIDs ) {
					if ( !currentCheckedNodeIDs.Contains( id ) ) {
						nodeIDsToDelete.Add( id );
					}
				}
				if ( nodeIDsToDelete.Count > 0 ) {
					string sql = string.Format(
						"delete from CMS_NODE_ITEMS where cms_item_id = @cms_item_id and tree_node_id in ( {0} )",
						IDList( nodeIDsToDelete )
					);
					using ( SqlDatabase db = new SqlDatabase() ) {
						db.Execute( sql, db.NewParameter( "@cms_item_id", item.ID ) );
					}
				}
				// Second, drop any currentCheckedNodeIDs items that are already in oldCheckedNodeIDs 
				List<int> nodeIDsToAdd = new List<int>();
				foreach ( int id in currentCheckedNodeIDs ) {
					if ( !oldCheckedNodeIDs.Contains( id ) ) {
						nodeIDsToAdd.Add( id );
					}
				}
				// Finally, add records to CMS_NODE_ITEMS for all the remaining currentCheckedNodeIDs
				foreach ( int id in nodeIDsToAdd ) {
					TreeNode node = new TreeNode( id );
					node.LinkContentItem( item, node.HighestItemRank + 1 );
				}
			}
		}

		protected void AdminGridReorderDown( object sender, DataGridClickEventArgs e ) {
			if ( this.IsSingleNodeMode ) {
				// Get a ref to our parent node...
				TreeNode node = new TreeNode( this.NodeID );
				node.MoveItemDown( e.RecordSeq );
				this.AdminGrid.BindGrid();
			}
		}

		protected void AdminGridReorderUp( object sender, DataGridClickEventArgs e ) {
			if ( this.IsSingleNodeMode ) {
				// Get a ref to our parent node...
				TreeNode node = new TreeNode( this.NodeID );
				node.MoveItemUp( e.RecordSeq );
				this.AdminGrid.BindGrid();
			}
		}

		protected override void CreateChildControls() {
			this.Controls.Clear();
			// Set up the AdminGrid...
			this.BuildAdminGrid();
			// Add the ItemPanel
			this.ItemPanel = new ContentItemFormPanel( ContentTypeManager.ContentTypes[this.ContentTypeKey], !this.IsSingleNodeMode );
			this.ItemPanel.IncludeNodePicker = !this.IsSingleNodeMode;
			this.AdminGrid.FormControlsPanel.Controls.Add( this.ItemPanel );
			this.AdminGrid.SearchControlsRead += new EventHandler( this.ReadSearchControls );
			this.AdminGrid.SearchControlsInit += new EventHandler( this.SearchControlsInit );
			this.AdminGrid.SearchControlsClear += new EventHandler( SearchControlsClear );
			this.AdminGrid.FormControlsInit += new EventHandler( this.InitFormControls );
			this.AdminGrid.FormControlsRead += new EventHandler( this.ReadFormControls );
			this.AdminGrid.FormItemPersisted += new EventHandler( this.CurrentItemSaved );
			this.AdminGrid.ReorderUp += new EventHandler<DataGridClickEventArgs>( AdminGridReorderUp );
			this.AdminGrid.ReorderDown += new EventHandler<DataGridClickEventArgs>( AdminGridReorderDown );
			this.AdminGrid.IncludeGridStatusMessage = true;
			// Add the SearchPanel
			this.BuildSearchPanel();
			this.AdminGrid.SearchPanel.Controls.Add( this.SearchPanel );
			this.Controls.Add( this.AdminGrid );
		}

		void SearchControlsClear( object sender, EventArgs e ) {
			this.SIsApprovedLBox.SelectedIndex = 0;
			//this.SCreatedByLBox.SelectedIndex = 0;
			this.SCreatedEndDTBox.Text = "";
			this.SCreatedStartDTBox.Text = "";
			this.STitleBox.Text = "";
		}

		protected void BuildAdminGrid() {
			// Set up the grid
			this.AdminGrid = new AdminDataGrid();
			this.AdminGrid.Width = new Unit( "100%" );
			this.AdminGrid.BorderStyle = BorderStyle.None;
			// Title column
			BoundColumn col = new BoundColumn();
			col.HeaderText = "Title";
			col.DataField = "Title";
			col.SortExpression = "c.title";
			this.AdminGrid.GridColumns.Add( col );
			// Rank column
			col = new BoundColumn();
			col.HeaderText = "Seq";
			col.DataField = "Rank";
			col.SortExpression = "n.rank";
			col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			col.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
			col.Visible = false;
			this.AdminGrid.GridColumns.Add( col );
			// Title column
			col = new BoundColumn();
			col.HeaderText = "Type";
			col.DataField = "ContentTypeLabel";
			col.SortExpression = "c.type_key";
			this.AdminGrid.GridColumns.Add( col );
			// ActiveDate column
			col = new BoundColumn();
			col.HeaderText = "Active On";
			col.DataField = "ActiveDateStr";
			col.SortExpression = "c.active_date";
			col.ItemStyle.Width = new Unit( "80px" );
			col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
			this.AdminGrid.GridColumns.Add( col );
			// ExpireDate column
			col = new BoundColumn();
			col.HeaderText = "Expires On";
			col.DataField = "ExpireDateStr";
			col.SortExpression = "c.expire_date";
			col.ItemStyle.Width = new Unit( "80px" );
			col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
			this.AdminGrid.GridColumns.Add( col );
			// IsApprovedMark column
			col = new BoundColumn();
			col.HeaderText = this.EyeballGlyphTag;//"&nbsp;";
			col.DataField = "IsVisibleMark";
			col.SortExpression = "c.is_approved";
			col.ItemStyle.Width = new Unit( "20px" );
			col.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
			col.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
			this.AdminGrid.GridColumns.Add( col );
		}

		protected void BuildSearchPanel() {
			this.SearchPanel = new Table();
			this.SearchPanel.BorderStyle = BorderStyle.None;
			this.SearchPanel.CellPadding = 0;
			this.SearchPanel.CellSpacing = 4;
			this.SearchPanel.Width = new Unit( "100%" );

			TableRow row = new TableRow();
			System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
			cell.ColumnSpan = 2;
			this.STitleBox = new LabeledTextBox();
			this.STitleBox.Label = "Page Title";
			cell.Controls.Add( this.STitleBox );
			row.Cells.Add( cell );

			//cell = new System.Web.UI.WebControls.TableCell();
			//this.SCreatedByLBox = new LabeledDropDownList();
			//this.SCreatedByLBox.Label = "Created By";
			//cell.Controls.Add( this.SCreatedByLBox );
			//row.Cells.Add( cell );
			cell = new System.Web.UI.WebControls.TableCell();
			cell.Width = new Unit( "30%" );
			this.SIsApprovedLBox = new LabeledDropDownList();
			this.SIsApprovedLBox.Label = "Approved?";
			this.SIsApprovedLBox.Items.Add( new ListItem( "-- Any --", "0" ) );
			this.SIsApprovedLBox.Items.Add( new ListItem( "Not Approved", "-1" ) );
			this.SIsApprovedLBox.Items.Add( new ListItem( "Approved", "1" ) );
			cell.Controls.Add( this.SIsApprovedLBox );
			row.Cells.Add( cell );
			this.SearchPanel.Rows.Add( row );

			row = new TableRow();
			cell = new System.Web.UI.WebControls.TableCell();
			cell.Width = new Unit( "35%" );
			this.SCreatedStartDTBox = new LabeledTextBox();
			this.SCreatedStartDTBox.Label = "Start Date";
			this.SCreatedStartDTBox.ID = "SCreatedStartDTBox";
			cell.Controls.Add( this.SCreatedStartDTBox );
			CompareValidator SCreatedStartDTCompVal = new CompareValidator();
			SCreatedStartDTCompVal.EnableClientScript = true;
			SCreatedStartDTCompVal.Display = ValidatorDisplay.None;
			SCreatedStartDTCompVal.ControlToValidate = "SCreatedStartDTBox";
			SCreatedStartDTCompVal.Type = ValidationDataType.Date;
			SCreatedStartDTCompVal.Operator = ValidationCompareOperator.DataTypeCheck;
			SCreatedStartDTCompVal.ErrorMessage = "Please enter a valid date value for Start Date.";
			cell.Controls.Add( SCreatedStartDTCompVal );
			row.Cells.Add( cell );

			cell = new System.Web.UI.WebControls.TableCell();
			cell.Width = new Unit( "35%" );
			this.SCreatedEndDTBox = new LabeledTextBox();
			this.SCreatedEndDTBox.Label = "End Date";
			this.SCreatedEndDTBox.ID = "SCreatedEndDTBox";
			cell.Controls.Add( this.SCreatedEndDTBox );
			CompareValidator SCreatedEndDTCompVal = new CompareValidator();
			SCreatedEndDTCompVal.EnableClientScript = true;
			SCreatedEndDTCompVal.Display = ValidatorDisplay.None;
			SCreatedEndDTCompVal.ControlToValidate = "SCreatedEndDTBox";
			SCreatedEndDTCompVal.Type = ValidationDataType.Date;
			SCreatedEndDTCompVal.Operator = ValidationCompareOperator.DataTypeCheck;
			SCreatedEndDTCompVal.ErrorMessage = "Please enter a valid date value for End Date.";
			cell.Controls.Add( SCreatedEndDTCompVal );
			row.Cells.Add( cell );

			this.SearchPanel.Rows.Add( row );
		}

		public void InitEditor() {
			this.EnsureChildControls();
			// Let it know which BaseSqlPersistable class it is working with for this page
			this.AdminGrid.DBItemType = typeof( StarrTech.WebTools.CMS.ContentItem );
			// Start things rolling
			if ( !this.Page.IsPostBack ) {
				this.AdminGrid.GridController.SortExpression = this.IsSingleNodeMode ? "n.rank" : "c.title";
				this.AdminGrid.GridController.SortOrder = "asc";
				this.AdminGrid.InitPanel();
				this.ConfigureGrid();
			}
		}

		protected override void OnLoad( EventArgs e ) {
			this.AdminGrid.BorderStyle = BorderStyle.None;
			this.InitEditor();
			base.OnLoad( e );
		}

		#region IAdminDataGridContentEditor

		private void SubGridFormControlsInit( object sender, EventArgs e ) {
			//this.NodePanel.FormController.Enabled = false;
		}

		private void SubGridFormClosing( object sender, EventArgs e ) {
			//this.NodePanel.FormController.Enabled = true;
		}

		#endregion

		#endregion

	}

}
