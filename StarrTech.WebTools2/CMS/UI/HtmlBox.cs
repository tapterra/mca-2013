using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.Controls;

//using FreeTextBoxControls;

namespace StarrTech.WebTools.CMS.UI {
//  /// <summary>
//  /// A subclass of the FreeTextBox component for use in the CMS
//  /// </summary>
  public class HtmlBox : HtmlTextBox {

//    #region Properties
//    /// <summary>
//    /// The LabelFramer used to position and display the label
//    /// </summary>
//    public LabelFramer Frame {
//      get { return ( frame ); }
//    }

//    private LabelFramer frame = new LabelFramer();

//    /// <summary>
//    /// The text displayed as the control's label
//    /// </summary>
//    public string Label {
//      get { return ( Frame.Label ); }
//      set { Frame.Label = value; }
//    }

//    /// <summary>
//    /// A TableStyle for the frame - exposes the backcolor, border, etc. styles
//    /// </summary>
//    public TableStyle FrameStyle {
//      get { return ( Frame.FrameStyle ); }
//    }

//    /// <summary>
//    /// A TableItemStyle for the frame label
//    /// </summary>
//    public TableItemStyle LabelStyle {
//      get { return ( Frame.LabelStyle ); }
//    }

//    /// <summary>
//    /// The TableItemStyle for the frame content (where the control is positioned)
//    /// </summary>
//    public TableItemStyle BodyStyle {
//      get { return ( Frame.BodyStyle ); }
//    }

//    // Override the setter method so the Frame can be adjusted
//    public new Unit Width {
//      get { return ( base.Width ); }
//      set {
//        base.Width = value;
//        Frame.SetCellWidths( value );
//      }
//    }

//    public bool Enabled {
//      get {
//        object obj = ViewState["Enabled"];
//        return ( obj == null ? true : (bool) obj );
//      }
//      set { ViewState["Enabled"] = value; }
//    }

//    /// <summary>
//    /// Color used to fill in the content area when the control is disabled
//    /// </summary>
//    public Color DisabledBackColor {
//      get { return ( this.Frame.DisabledBackColor ); }
//      set { this.Frame.DisabledBackColor = value; }
//    }

//    /// <summary>
//    /// Color used for text, etc. in the content area when the control is disabled
//    /// </summary>
//    public Color DisabledForeColor {
//      get { return ( this.Frame.DisabledForeColor ); }
//      set { this.Frame.DisabledForeColor = value; }
//    }

//    /// <summary>
//    /// Option for selecting a predefined configuration
//    /// </summary>
//    public HtmlTextBoxConfigModes ConfigMode {
//      get {
//        object obj = ViewState["ConfigMode"];
//        return ( obj == null ? HtmlTextBoxConfigModes.FullFeatured : (HtmlTextBoxConfigModes) obj );
//      }
//      set { ViewState["ConfigMode"] = value; }
//    }

//    public bool IncludeColorToolbar {
//      get {
//        object obj = ViewState["IncludeColorToolbar"];
//        return ( obj == null ? false : (bool) obj );
//      }
//      set { ViewState["IncludeColorToolbar"] = value; }
//    }

//    public string Html {
//      get {
//        string html = this.Text;
//        html = this.Page.Server.HtmlEncode( html );
//        return ( html );
//      }
//    }

//    #endregion

//    #region Constructor

//    public HtmlBox()
//      : base() {
//      // Set up some default properties
//      Frame.FramerType = StarrTech.WebTools.Panels.FramerType.TopTitleFrame;
//      Frame.LabelStyle.Wrap = false;
//      Frame.LabelMargin = 2;
//      Frame.Style["margin-bottom"] = "6px";
//      Frame.LabelStyle.Font.Name = "Verdana";
//      Frame.LabelStyle.Font.Size = new FontUnit( "8pt" );
//      Frame.LabelStyle.Font.Bold = true;

//      this.ConfigMode = (HtmlTextBoxConfigModes) Enum.Parse( typeof( HtmlTextBoxConfigModes ),
//      SitePrefs.CheckPref( "HTMLTEXTBOX_CONFIG_MODE", "FullFeatured" ), true );
//      this.DesignModeBodyTagCssClass = SitePrefs.CheckPref( "WYSIWYG_BODYTAG_CSSCLASS", "bodyContent" );
//      this.DesignModeCss = SitePrefs.CheckPref( "WYSIWYG_STYLESHEET", "App_Themes/_admin/wysiwyg.css" );
//      this.AutoParseStyles = true;

//      this.ToolbarImagesLocation = ResourceLocation.InternalResource;
//      this.JavaScriptLocation = ResourceLocation.InternalResource;
//      this.ButtonImagesLocation = ResourceLocation.InternalResource;
//      this.ToolbarImagesLocation = ResourceLocation.InternalResource;
//      this.ButtonImagesLocation = ResourceLocation.InternalResource;

//      this.RemoveServerNameFromUrls = true;
//      this.SupportFolder = "FtbWebResource.axd";

//      this.ShowTagPath = false;
//      this.ConvertHtmlSymbolsToHtmlCodes = true;
//      this.AutoGenerateToolbarsFromString = false;
//      this.UpdateToolbar = true;
//      this.StripAllScripting = false;
//      this.FormatHtmlTagsToXhtml = false;

//      this.BreakMode = BreakMode.Paragraph;

//      this.Width = new Unit( "100%" );
//      this.TabIndex = 1;
//    }

//    #endregion

//    #region Methods

//    protected void AddFormat1Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new Bold() );
//      bar.Items.Add( new Italic() );
//      bar.Items.Add( new Underline() );
//      bar.Items.Add( new StrikeThrough() );
//      bar.Items.Add( new SuperScript() );
//      bar.Items.Add( new SubScript() );
//      bar.Items.Add( new RemoveFormat() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddFormat2Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new JustifyLeft() );
//      bar.Items.Add( new JustifyRight() );
//      bar.Items.Add( new JustifyCenter() );
//      bar.Items.Add( new JustifyFull() );

//      bar.Items.Add( new ToolbarSeparator() );
//      FreeTextBoxControls.BulletedList bull = new FreeTextBoxControls.BulletedList();
//      bull.Title = "Bulleted List"; // correcting a typo in the base class;
//      bar.Items.Add( bull );
//      bar.Items.Add( new NumberedList() );
//      bar.Items.Add( new Indent() );
//      bar.Items.Add( new Outdent() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddFormat3Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new ParagraphMenu() );
//      FontFacesMenu fontMenu = new FontFacesMenu();
//      bar.Items.Add( fontMenu );
//      FontSizesMenu sizeMenu = new FontSizesMenu();
//      bar.Items.Add( sizeMenu );
//      bar.Items.Add( new SymbolsMenu() );
//      StylesMenu stylesMenu = new StylesMenu();
//      //stylesMenu.Items.Add( new ToolbarListItem( "Headline", "h1" ) );
//      //...
//      bar.Items.Add( stylesMenu );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddLinkCommandsToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new CreateLink() );
//      bar.Items.Add( new Unlink() );
//      bar.Items.Add( new InsertImageFromGallery() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddEditCommands1Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new SelectAll() );
//      bar.Items.Add( new Undo() );
//      bar.Items.Add( new Redo() );
//      bar.Items.Add( new Cut() );
//      bar.Items.Add( new Copy() );
//      bar.Items.Add( new Paste() );
//      bar.Items.Add( new Delete() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddEditCommands2Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new InsertRule() );
//      bar.Items.Add( new InsertDate() );
//      bar.Items.Add( new InsertTime() );
//      bar.Items.Add( new InsertDiv() );
//      bar.Items.Add( new EditStyle() );
//      bar.Items.Add( new WordClean() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddColorCommandsToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new FontForeColorsMenu() );
//      bar.Items.Add( new FontBackColorsMenu() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddTableToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new InsertTable() );
//      bar.Items.Add( new EditTable() );
//      bar.Items.Add( new InsertTableRowAfter() );
//      bar.Items.Add( new InsertTableRowBefore() );
//      bar.Items.Add( new DeleteTableRow() );
//      bar.Items.Add( new ToolbarSeparator() );
//      bar.Items.Add( new InsertTableColumnAfter() );
//      bar.Items.Add( new InsertTableColumnBefore() );
//      bar.Items.Add( new DeleteTableColumn() );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddPrintToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new Print() );
//      bar.Items.Add( new Preview() );
//      this.Toolbars.Add( bar );
//    }

//    protected void AddFlashFormatToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new Bold() );
//      bar.Items.Add( new Italic() );
//      bar.Items.Add( new Underline() );
//      bar.Items.Add( new RemoveFormat() );
//      this.Toolbars.Add( bar );
//    }

//    protected void AddFlashFormat2Toolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new SymbolsMenu() );
//      StylesMenu stylesMenu = new StylesMenu();
//      bar.Items.Add( stylesMenu );
//      this.Toolbars.Add( bar );
//    }
//    protected void AddFlashLinkCommandsToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new CreateLink() );
//      bar.Items.Add( new Unlink() );
//      this.Toolbars.Add( bar );
//    }

//    protected void AddFlashEditCommandsToolbar() {
//      Toolbar bar = new Toolbar();
//      bar.Items.Add( new WordClean() );
//      this.Toolbars.Add( bar );
//    }

//    public void ConfigureOptions() {
//      this.ImageGalleryPath = SitePrefs.CheckPref( "ImageGalleryPath", "~/images/gallery" );
//      this.ToolbarStyleConfiguration = ToolbarStyleConfiguration.OfficeXP;
//      this.Toolbars.Clear();
//      switch ( this.ConfigMode ) {
//        case HtmlTextBoxConfigModes.Simple:
//          this.EnableHtmlMode = false;
//          this.ShowTagPath = false;
//          this.AddFormat1Toolbar();
//          this.AddFormat2Toolbar();
//          if ( this.IncludeColorToolbar ) {
//            this.AddColorCommandsToolbar();
//          }
//          break;
//        case HtmlTextBoxConfigModes.Catalog:
//          this.EnableHtmlMode = true;
//          this.ShowTagPath = false;
//          this.AddFormat1Toolbar();
//          this.AddFormat2Toolbar();
//          this.AddFormat3Toolbar();
//          if ( this.IncludeColorToolbar ) {
//            this.AddColorCommandsToolbar();
//          }
//          break;
//        case HtmlTextBoxConfigModes.FullFeatured:
//          this.EnableHtmlMode = true;
//          this.ShowTagPath = true;
//          this.AddFormat1Toolbar();
//          this.AddFormat2Toolbar();
//          this.AddLinkCommandsToolbar();
//          this.AddEditCommands1Toolbar();
//          this.AddFormat3Toolbar();
//          if ( this.IncludeColorToolbar ) {
//            this.AddColorCommandsToolbar();
//          }
//          this.AddEditCommands2Toolbar();
//          this.AddTableToolbar();
//          this.AddPrintToolbar();
//          break;
//        case HtmlTextBoxConfigModes.Flash:
//          this.EnableHtmlMode = true;
//          this.ShowTagPath = true;
//          this.AddFlashFormatToolbar();
//          this.AddFlashFormat2Toolbar();
//          this.AddFlashLinkCommandsToolbar();
//          this.AddFlashEditCommandsToolbar();
//          break;
//      }
//    }

//    protected override void OnLoad( EventArgs e ) {
//      //if ( !this.Page.IsPostBack ) {
//        this.ConfigureOptions();
//      //}
//      base.OnLoad( e );
//    }

//    protected override void Render( HtmlTextWriter writer ) {
//      EnsureChildControls();
//      this.Frame.PreRender( writer );
//      if ( this.Enabled ) {
//        base.Render( writer );
//      } else {
//        this.Frame.RenderDisabled( writer, null, this.Text );
//      }
//      this.Frame.PostRender( writer );
//    }

//    #endregion

  }
}
