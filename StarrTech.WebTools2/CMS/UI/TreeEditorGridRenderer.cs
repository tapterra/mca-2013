using System;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools.TreeUI;


namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Custom TreeRenderer for the CMS TreeEditorGrid
	/// </summary>
	public class TreeEditorGridRenderer : TabularTreeRenderer {

		public string EyeballGlyphTag {
			get {
				if ( string.IsNullOrEmpty( this.eyeballGlyphTag ) ) {
					string url = this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), "StarrTech.WebTools.Resources.treeui.eyeball.gif" );
					this.eyeballGlyphTag = string.Format( "<img src='{0}' />", url );
				}
				return ( this.eyeballGlyphTag );
			}
		}
		private string eyeballGlyphTag;

		public string MenuGlyphTag {
			get {
				if ( string.IsNullOrEmpty( this.menuGlyphTag ) ) {
					string url = this.TreePanel.Page.ClientScript.GetWebResourceUrl( this.GetType(), "StarrTech.WebTools.Resources.treeui.menu.gif" );
					this.menuGlyphTag = string.Format( "<img src='{0}' />", url );
				}
				return ( this.menuGlyphTag );
			}
		}
		private string menuGlyphTag;

		/// <summary>
		/// Exposes the base constructor
		/// </summary>
		/// <param name="treePanel">The TreeEditorGrid instance that is using this instance for rendering</param>
		public TreeEditorGridRenderer( TreeEditorGrid treePanel ) : base( treePanel ) { }

		/// <summary>
		/// Called as the first step of rendering each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer ) {
			this.rowIndex++;
			bool isCurrent = node.Key == this.TreePanel.SelectedNodeKey;
			string link = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID + ( isCurrent ? ":edit" : ":select" ) );
			string bgColor = ColorTranslator.ToHtml( isCurrent ? this.TreePanel.SelectedDataBackColor :
				( this.rowIndex % 2 != 0 ? this.TreePanel.DataBackColor : this.TreePanel.AltDataBackColor )
			);
			string rowHilightColor = ColorTranslator.ToHtml( this.TreePanel.RowHilightedBackColor );
			string fgColor = ColorTranslator.ToHtml( isCurrent ? this.TreePanel.SelectedDataForeColor : this.TreePanel.DataForeColor );
			writer.WriteBeginTag( "tr" );
			writer.WriteAttribute( "style", string.Format( "color:{0}; background-color:{1};", fgColor, bgColor ) );
			writer.WriteAttribute( "onmouseover",
				string.Format( "javascript:this.style.cursor='hand';this.style.backgroundColor='{0}';", isCurrent ? bgColor : rowHilightColor ) );
			writer.WriteAttribute( "onmouseout",
				string.Format( "javascript:this.style.cursor='auto';this.style.backgroundColor='{0}';", bgColor ) );
			writer.WriteAttribute( "onclick", link + ";" );
			writer.Write( HtmlTextWriter.TagRightChar );
			writer.WriteBeginTag( "td" );
			writer.WriteAttribute( "style", "vertical-align:top; padding-left:" + ( ( node.Indent * IndentPixels ) + 4 ) + "px;" );
			writer.WriteAttribute( "nowrap", "true" );
			writer.WriteAttribute( "width", "150" );
			writer.Write( HtmlTextWriter.TagRightChar );
		}

		/// <summary>
		/// Called as the last step of rendering each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderNodeControlEnd( TreeNodeControl node, HtmlTextWriter writer ) {
			// Render the "menu" cell
			writer.WriteBeginTag( "td" );
			writer.WriteAttribute( "width", "20" );
			writer.Write( HtmlTextWriter.TagRightChar );
			bool isMenuItem = node.Attributes["ismenuitem"] == "true";
			bool isVisible = node.Attributes["isvisible"] == "true";
			writer.Write( isVisible && node.IsVisibleMenuItem() ? this.MenuGlyphTag : "&nbsp;" );
			writer.WriteEndTag( "td" );

			// Render the "visible" cell
			writer.WriteBeginTag( "td" );
			writer.WriteAttribute( "width", "20" );
			writer.Write( HtmlTextWriter.TagRightChar );
			writer.Write( isVisible ? this.EyeballGlyphTag : "&nbsp;" );
			writer.WriteEndTag( "td" );

			writer.WriteEndTag( "tr" );
		}

		/// <summary>
		/// Called when rendering the outline (expand/collapse) glyph for each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer ) {
			string imgTag = "<img src='{0}' border='0' align='top' style='margin-right:12px;' onclick=\"{1}\", tooltip='{2}' />";
			if ( node.IsFolder ) {
				string script = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID ) + ";window.event.cancelBubble = true;";
				string imgSrc = node.IsExpanded ? this.TreePanel.ExpandedImage : this.TreePanel.CollapsedImage;
				string tip = node.IsExpanded ? "Click to collapse" : "Click to expand";
				writer.Write( string.Format( imgTag, imgSrc, script, tip ) );
			} else {
				writer.Write( string.Format( imgTag, this.TreePanel.NullImage, "", "nothing" ) );
			}
		}

		/// <summary>
		/// Called when rendering the text/details for each node.
		/// </summary>
		/// <param name="node">Node being rendered</param>
		/// <param name="writer">The HtmlTextWriter for the current page response</param>
		public override void RenderNodeControlText( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.WriteBeginTag( "a" );
			writer.WriteAttribute( "name", node.Key );
			writer.WriteAttribute( "class", this.TreePanel.CssClass );
			writer.Write( HtmlTextWriter.TagRightChar );
			//writer.Write( string.Format( "<b>/{0}</b>", node.Attributes["key"] ) );
			if ( node.Key == this.TreePanel.SelectedNodeKey ) {
				writer.Write( string.Format( "<b>{0}</b>", node.Attributes["key"] ) );
			} else {
				writer.Write( node.Attributes["key"] );
			}
			writer.WriteEndTag( "a" );
			writer.WriteEndTag( "td" );

			writer.WriteBeginTag( "td" );
			writer.WriteAttribute( "style", "vertical-align:top; padding-left:" + ( ( node.Indent * IndentPixels ) + 4 ) + "px;" );
			writer.Write( HtmlTextWriter.TagRightChar );
			writer.WriteBeginTag( "span" );
			writer.WriteAttribute( "class", this.TreePanel.CssClass );
			writer.Write( HtmlTextWriter.TagRightChar );
			if ( node.Key == this.TreePanel.SelectedNodeKey ) {
				writer.Write( string.Format( "<b>{0}</b>", node.Title ) );
			} else {
				writer.Write( node.Title );
			}
			writer.WriteEndTag( "span" );
			writer.WriteEndTag( "td" );
		}

	}

}
