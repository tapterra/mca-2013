using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.Data;

using System.Data;

namespace StarrTech.WebTools.CMS.UI {

	public class SiteTreeNodePicker : TreeView {

		#region Properties
		/// <summary>
		/// The LabelFramer used to position and display the label
		/// </summary>
		public LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();

		/// <summary>
		/// The text displayed as the control's label
		/// </summary>
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}

		/// <summary>
		/// A TableStyle for the frame - exposes the backcolor, border, etc. styles
		/// </summary>
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}

		/// <summary>
		/// A TableItemStyle for the frame label
		/// </summary>
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}

		/// <summary>
		/// The TableItemStyle for the frame content (where the control is positioned)
		/// </summary>
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}

		/// <summary>
		/// Overridden to set up the frame
		/// </summary>
		public override Unit Width {
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}

		/// <summary>
		/// ID for the Content-Item we're configuring
		/// </summary>
		public int ContentItemID {
			get { return ( this.contentItemID ); }
			set { 
				this.contentItemID = value;
				this.ResetNodeChecks();
			}
		}
		private int contentItemID = -1;

		/// <summary>
		/// Returns the list of node-IDs for the nodes that are checked
		/// </summary>
		public List<int> CheckedNodeIDs;
		
		#endregion
		
		#region Constructor

		public SiteTreeNodePicker(): base() {
			// Define some defaults
			//this.ImageSet = TreeViewImageSet.Simple2;
			this.NodeWrap = false;
			this.ExpandDepth = 0;
			this.Width = new Unit( "300" );
			this.Frame.LabelMargin = 0;
			this.NodeIndent = 6;
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Given the current ContentItemID, load a list of linked TreeNode IDs from CMS_NODE_ITEMS
		/// </summary>
		public void LoadCheckedNodeIDs() {
			this.CheckedNodeIDs = new List<int>();
			if ( this.ContentItemID > 0 ) {
				using ( SqlDatabase db = new SqlDatabase() ) {
					string sql = "select tree_node_id from CMS_NODE_ITEMS where ( cms_item_id = @cms_item_id )";
					using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@cms_item_id", this.ContentItemID ) ) ) {
					while ( rdr.Read() ) {
						this.CheckedNodeIDs.Add( rdr.GetInt32( 0 ) );
					}
				}
			}
		}
		}

		public List<int> GetCheckedNodeIDs() {
			List<int> result = new List<int>();
			foreach ( System.Web.UI.WebControls.TreeNode node in this.CheckedNodes ) {
				result.Add( Convert.ToInt32( node.Value ) );
			}
			return ( result );
		}

		public void ResetNodeChecks() {
			this.LoadCheckedNodeIDs();
			foreach ( System.Web.UI.WebControls.TreeNode node in this.Nodes ) {
				ResetNodeChecks( node );
			}
		}

		private void ResetNodeChecks( System.Web.UI.WebControls.TreeNode node ) {
			foreach ( System.Web.UI.WebControls.TreeNode child in node.ChildNodes ) {
				int nodeID = Convert.ToInt32( child.Value );
				child.Checked = ( this.CheckedNodeIDs.Contains( nodeID ) );
				ResetNodeChecks( child );
			}
		}

		protected void BuildTreeNode( System.Web.UI.WebControls.TreeNode ParentTreeNode, string siteTreeKey, ContentMenuNode menuNode ) {
			System.Web.UI.WebControls.TreeNode treeNode = new System.Web.UI.WebControls.TreeNode( menuNode.Text, menuNode.ID );
			int nodeID = Convert.ToInt32( menuNode.ID );
			treeNode.SelectAction = TreeNodeSelectAction.None;
			if ( menuNode.TypeKey == "contentcatalog" ) {
			treeNode.ShowCheckBox = true;
			treeNode.Checked = ( this.CheckedNodeIDs.Contains( nodeID ) );
			} else {
				treeNode.ShowCheckBox = false;
			}
			ParentTreeNode.ChildNodes.Add( treeNode );
			foreach ( ContentMenuNode subMenu in menuNode.AdminSubNodes ) {
				BuildTreeNode( treeNode, siteTreeKey, subMenu );
			}
		}

		protected void BuildTreeNodes( SiteTree tree ) {
			List<ContentMenuNode> mainNodes = ContentMenuNode.LoadTreeMenus( tree.Key, true );
				this.LoadCheckedNodeIDs();
			System.Web.UI.WebControls.TreeNode treeRoot = new System.Web.UI.WebControls.TreeNode( tree.Label, tree.Key );
				treeRoot.SelectAction = TreeNodeSelectAction.None;
				this.Nodes.Add( treeRoot );
			foreach ( ContentMenuNode node in mainNodes ) {
				BuildTreeNode( treeRoot, tree.Key, node );
						}
					}

		protected void BuildTrees() {
			SiteTreesList trees = SiteTreesList.Current;
			foreach ( SiteTree tree in trees ) {
				this.BuildTreeNodes( tree );
			}
		}

		protected override void OnInit( EventArgs e ) {
			base.OnInit( e );
			this.BuildTrees();
		}

		protected override void OnPreRender( EventArgs e ) {
			base.OnPreRender( e );
			if ( Frame.FramerType == StarrTech.WebTools.Panels.FramerType.TopTitleFrame ) {
				Frame.Style["margin-bottom"] = "4px";
			}
		}

		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			this.Frame.PreRender( writer );
			base.Render( writer );
			this.Frame.PostRender( writer );
		}

		#endregion
		
	}

}
