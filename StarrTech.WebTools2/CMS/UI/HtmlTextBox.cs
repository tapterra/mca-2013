using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.Controls;
using System.Collections.Specialized;
using System.Text;

namespace StarrTech.WebTools.CMS.UI {

	public enum HtmlTextBoxConfigModes {
		Simple,
		Catalog,
		FullFeatured,
		Flash
	}

	public enum HtmlTextBoxResizeModes {
		NotResizeable,
		Resizeable,
		VertResizable
	}
	/// <summary>
	/// A subclass of the TextBox component masquarading as a TinyMCE WYSIWYG HTML editor
	/// </summary>
	public class HtmlTextBox : TextBox {
		// CSS Class Name used to trigger the TinyMCE javascript
		public const string TinyMCETriggerClass = "MakeMeAnHtmlEditor";

		#region Properties

		public override string Text {
			get { return ( base.Text ); }
			set { base.Text = value; }
		}

		#region Appearance

		/// <summary>
		/// The LabelFramer used to position and display the label
		/// </summary>
		public LabelFramer Frame {
			get { return ( frame ); }
		}
		private LabelFramer frame = new LabelFramer();

		/// <summary>
		/// The text displayed as the control's label
		/// </summary>
		public string Label {
			get { return ( Frame.Label ); }
			set { Frame.Label = value; }
		}

		/// <summary>
		/// A TableStyle for the frame - exposes the backcolor, border, etc. styles
		/// </summary>
		public TableStyle FrameStyle {
			get { return ( Frame.FrameStyle ); }
		}

		/// <summary>
		/// A TableItemStyle for the frame label
		/// </summary>
		public TableItemStyle LabelStyle {
			get { return ( Frame.LabelStyle ); }
		}

		/// <summary>
		/// The TableItemStyle for the frame content (where the control is positioned)
		/// </summary>
		public TableItemStyle BodyStyle {
			get { return ( Frame.BodyStyle ); }
		}

		// Override the setter method so the Frame can be adjusted
		public override Unit Width {
			get { return ( base.Width ); }
			set {
				base.Width = value;
				Frame.SetCellWidths( value );
			}
		}

		#endregion

		#region TinyMCE Configuration

		/// <summary>
		/// URL to the main TinyMCE jscript file (~/tiny_mce/tiny_mce.js)
		/// </summary>
		public string IncludeScriptUrl {
			get {
				if ( string.IsNullOrEmpty( this.includeScriptUrl ) ) {
					this.includeScriptUrl = SitePrefs.CheckPref( "TinyMceIncludeScriptUrl", "~/tiny_mce/tiny_mce.js" );
				}
				return ( this.Page.ResolveUrl( this.includeScriptUrl ) );
			}
		}
		private string includeScriptUrl = "";

		/// <summary>
		/// 
		/// </summary>
		public string ContentCssUrl {
			get {
				if ( string.IsNullOrEmpty( this.contentCssUrl ) ) {
					this.contentCssUrl = SitePrefs.CheckPref( "TinyMceContentCssUrl", "~/App_Themes/default/wysiwyg.css" );
				}
				return ( this.Page.ResolveUrl( this.contentCssUrl ) );
			}
		}
		private string contentCssUrl = "";

		/// <summary>
		/// Collection of name-value pairs for the tinyMCE.init() configuration array
		/// </summary>
		public StringDictionary ConfigAttributes {
			get { return ( this.configAttributes ); }
		}
		private StringDictionary configAttributes = new StringDictionary();

		/// <summary>
		/// Option for selecting a predefined configuration
		/// </summary>
		public HtmlTextBoxConfigModes ConfigMode {
			get {
				object obj = ViewState["ConfigMode"];
				return ( obj == null ? HtmlTextBoxConfigModes.FullFeatured : (HtmlTextBoxConfigModes) obj );
			}
			set { ViewState["ConfigMode"] = value; }
		}

		/// <summary>
		/// Options for allowing the user to resize the editor
		/// </summary>
		public HtmlTextBoxResizeModes ResizeMode {
			get {
				object obj = ViewState["ResizeMode"];
				return ( obj == null ? HtmlTextBoxResizeModes.NotResizeable : (HtmlTextBoxResizeModes) obj );
			}
			set { ViewState["ResizeMode"] = value; }
		}

		#endregion

		#endregion

		#region Constructor

		public HtmlTextBox()
			: base() {
			this.ConfigMode = (HtmlTextBoxConfigModes) Enum.Parse( typeof( HtmlTextBoxConfigModes ),
					SitePrefs.CheckPref( "HTMLTEXTBOX_CONFIG_MODE", "FullFeatured" ), true );
			this.TextMode = TextBoxMode.MultiLine;
			// Set up some default properties
			Frame.LabelStyle.Font.Name = SitePrefs.CheckPref( "labelfont", "Verdana" );
			Frame.LabelStyle.Font.Size = SitePrefs.CheckFontUnit( "labelfontsize", "8pt" );
			Frame.LabelStyle.Font.Bold = true;

			Frame.FramerType = StarrTech.WebTools.Panels.FramerType.TopTitleFrame;
			Frame.LabelStyle.Wrap = false;
			Frame.LabelMargin = 2;
			//Frame.Style["margin-bottom"] = "6px";
			//Frame.BodyStyle.BorderColor = Color.DarkGray;
			//Frame.BodyStyle.BorderStyle = BorderStyle.Solid;
			//Frame.BodyStyle.BorderWidth = 1;
			//Frame.BodyStyle.BackColor = Color.White;

			//this.Width = new Unit( "100%" );
			//this.Height = new Unit( "400" );

			// TinyMCE Trigger ...
			this.CssClass = TinyMCETriggerClass;
			this.ConfigAttributes.Add( "mode", "textareas" );
			this.ConfigAttributes.Add( "editor_selector", TinyMCETriggerClass );
		}

		#endregion

		#region Methods

		protected string GetConfigScript() {
			switch ( this.ConfigMode ) {
				case HtmlTextBoxConfigModes.Simple:
					this.ConfigAttributes.Add( "theme", "simple" );
					break;
				case HtmlTextBoxConfigModes.Catalog:
					//this.ConfigAttributes.Add( "theme", "simple" );
					this.ConfigAttributes.Add( "theme", "advanced" );
					this.ConfigAttributes.Add( "theme_advanced_buttons1", 
						"bold,italic,underline,strikethrough,separator,undo,redo,separator,cleanup,separator,bullist, numlist,separator,code" 
					);
					// Hide the second & third row of buttons
					this.ConfigAttributes.Add( "theme_advanced_buttons2", "" ); 
					this.ConfigAttributes.Add( "theme_advanced_buttons3", "" );
					//this.ConfigAttributes.Add( "theme_advanced_toolbar_location", "top" );
					//this.ConfigAttributes.Add( "theme_advanced_toolbar_align", "left" );
					this.ConfigAttributes.Add( "theme_advanced_resizing", "true" );
					this.ConfigAttributes.Add( "theme_advanced_resize_horizontal", "false" );
					this.ConfigAttributes.Add( "apply_source_formatting", "true" );
					break;
				case HtmlTextBoxConfigModes.FullFeatured:
					this.ConfigAttributes.Add( "theme", "advanced" );
					this.ConfigAttributes.Add( "plugins", 
						"table,advhr,advimage,advlink,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu,paste,fullscreen,noneditable" 
					);
					this.ConfigAttributes.Add( "theme_advanced_buttons1_add", "fontselect,fontsizeselect,separator,fullscreen" );
					this.ConfigAttributes.Add( "theme_advanced_buttons2_add", 
						"separator,insertdate,inserttime,preview,separator,forecolor,backcolor" 
					);
					this.ConfigAttributes.Add( "theme_advanced_buttons2_add_before", 
						"cut,copy,paste,pastetext,pasteword,separator,search,replace,separator" 
					);
					this.ConfigAttributes.Add( "theme_advanced_buttons3_add_before", "tablecontrols,separator" );
					this.ConfigAttributes.Add( "theme_advanced_buttons3_add", "iespell,flash,advhr,separator,print,separator,ltr,rtl" );
					this.ConfigAttributes.Add( "theme_advanced_toolbar_location", "top" );
					this.ConfigAttributes.Add( "theme_advanced_toolbar_align", "left" );
					this.ConfigAttributes.Add( "theme_advanced_path_location", "bottom" );
					this.ConfigAttributes.Add( "plugin_insertdate_dateFormat", "%Y-%m-%d" );
					this.ConfigAttributes.Add( "plugin_insertdate_timeFormat", "%H:%M:%S" );
					this.ConfigAttributes.Add( "extended_valid_elements", 
						"hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]" 
					);
					this.ConfigAttributes.Add( "apply_source_formatting", "true" );
					switch ( this.ResizeMode ) {
						case HtmlTextBoxResizeModes.Resizeable:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "true" );
							this.ConfigAttributes.Add( "theme_advanced_resize_horizontal", "true" );
							break;
						case HtmlTextBoxResizeModes.VertResizable:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "true" );
							this.ConfigAttributes.Add( "theme_advanced_resize_horizontal", "false" );
							break;
						default:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "false" );
							break;
					}
					this.ConfigAttributes.Add( "content_css", this.Page.ResolveUrl( this.ContentCssUrl ) );
					//this.ConfigAttributes.Add( "external_link_list_url", "example_link_list.js" );
					//this.ConfigAttributes.Add( "external_image_list_url", "example_image_list.js" );
					//this.ConfigAttributes.Add( "flash_external_list_url", "example_flash_list.js" );
					//this.ConfigAttributes.Add( "file_browser_callback", "fileBrowserCallBack" );
					break;
				case HtmlTextBoxConfigModes.Flash:
					this.ConfigAttributes.Add( "theme", "advanced" );
					this.ConfigAttributes.Add( "plugins", "advlink,preview,searchreplace,contextmenu,paste" );
					this.ConfigAttributes.Add( "theme_advanced_buttons1", "bold,italic,underline,removeformat,separator,styleselect,separator,preview" );
					this.ConfigAttributes.Add( "theme_advanced_buttons2", "undo,redo,separator,cut,copy,paste,pastetext,pasteword,separator,search,replace,separator,code,cleanup" );
					this.ConfigAttributes.Add( "theme_advanced_buttons3", "link,unlink" );
					this.ConfigAttributes.Add( "theme_advanced_toolbar_location", "top" );
					this.ConfigAttributes.Add( "theme_advanced_toolbar_align", "left" );
					this.ConfigAttributes.Add( "theme_advanced_path_location", "bottom" );
					this.ConfigAttributes.Add( "force_p_newlines", "false" );
					this.ConfigAttributes.Add( "force_br_newlines", "true" );
					this.ConfigAttributes.Add( "entity_encoding", "numeric" );
					this.ConfigAttributes.Add( "entities", "8226,bull,160,nbsp,38,amp,34,quot,169,copy,174,reg,8482,trade,183,middot,8242,prime,8243,Prime,60,lt,62,gt,8211,ndash,8212,mdash" );
					this.ConfigAttributes.Add( "cleanup", "true" );
					this.ConfigAttributes.Add( "invalid_elements", "p" );
					this.ConfigAttributes.Add( "extended_valid_elements", "b/strong,i/em" );
					switch ( this.ResizeMode ) {
						case HtmlTextBoxResizeModes.Resizeable:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "true" );
							this.ConfigAttributes.Add( "theme_advanced_resize_horizontal", "true" );
							break;
						case HtmlTextBoxResizeModes.VertResizable:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "true" );
							this.ConfigAttributes.Add( "theme_advanced_resize_horizontal", "false" );
							break;
						default:
							this.ConfigAttributes.Add( "theme_advanced_resizing", "false" );
							break;
					}
					this.ConfigAttributes.Add( "content_css", this.Page.ResolveUrl( this.ContentCssUrl ) );
					break;
			}
			StringBuilder initScript = new StringBuilder( "tinyMCE.init({ " );
			initScript.AppendFormat( "mode: 'textareas', editor_selector : '{0}'", TinyMCETriggerClass );
			// Append the ConfigAttribute items...
			foreach ( DictionaryEntry item in this.ConfigAttributes ) {
				string val = item.Value.ToString();
				string fmt = ( val == "true" || val == "false" ) ? ", {0} : {1}" : ", {0} : '{1}'";
				initScript.AppendFormat( fmt, item.Key, item.Value );
			}
			initScript.Append( " });" );
			return ( initScript.ToString() );
		}

		protected void ConfigureScript() {
			ClientScriptManager cs = this.Page.ClientScript;
			// <script language="javascript" type="text/javascript" src="/tiny_mce/tiny_mce.js"></script>
			string includeKey = "TinyMceIncludeKey";
			if ( !cs.IsClientScriptIncludeRegistered( this.GetType(), includeKey ) ) {
				cs.RegisterClientScriptInclude( this.GetType(), includeKey, this.IncludeScriptUrl );
			}
			string initKey = "TinyMceInitScriptKey";
			string initScript = this.GetConfigScript();
			if ( !cs.IsClientScriptBlockRegistered( this.GetType(), initKey ) ) {
				cs.RegisterClientScriptBlock( this.GetType(), initKey, initScript, true );
			}
		}

		protected override void OnLoad( EventArgs e ) {
			this.ConfigureScript();
			base.OnLoad( e );
		}

		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			this.Frame.PreRender( writer );
			base.Render( writer );
			this.Frame.PostRender( writer );
		}

		#endregion

	}

}

/*
	<script language="javascript" type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			theme : "advanced",
			plugins : "table,save,advhr,advimage,advlink,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,-emotions,fullpage",
			theme_advanced_buttons1_add_before : "save,newdocument,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
			theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "tablecontrols,separator",
			theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen,fullpage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path_location : "bottom",
			content_css : "example_full.css",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
			external_link_list_url : "example_link_list.js",
			external_image_list_url : "example_image_list.js",
			flash_external_list_url : "example_flash_list.js",
			file_browser_callback : "fileBrowserCallBack",
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			apply_source_formatting : true
		});

		function fileBrowserCallBack(field_name, url, type, win) {
			// This is where you insert your custom filebrowser logic
			alert("Example of filebrowser callback: field_name: " + field_name + ", url: " + url + ", type: " + type);

			// Insert new URL, this would normaly be done in a popup
			win.document.forms[0].elements[field_name].value = "someurl.htm";
		}
	</script>
*/
