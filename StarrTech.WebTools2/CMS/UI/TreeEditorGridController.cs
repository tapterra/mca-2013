using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.TreeUI;

namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Button-panel and controller logic for the TreeEditorGrid
	/// </summary>
	public class TreeEditorGridController : Table {
		
		#region Properties 
		
		public string HeaderText {
			get { return ( this.HeaderLabel.Text ); }
			set { this.HeaderLabel.Text = value; }
		}
				
		public string CurrentTreeKey {
			get { return ( this.TreeKeyListBox.SelectedValue ); }
			set { this.TreeKeyListBox.SelectedValue = value; }
		}

		#region Permissions Properties

		public bool CanAddNodes {
			get {
				object obj = this.ViewState["CanAddNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanAddNodes"] = value; }
		}

		public bool CanEditNodes {
			get {
				object obj = this.ViewState["CanEditNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanEditNodes"] = value; }
		}

		public bool CanMoveNodes {
			get {
				object obj = this.ViewState["CanMoveNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanMoveNodes"] = value; }
		}

		public bool CanDeleteNodes {
			get {
				object obj = this.ViewState["CanDeleteNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanDeleteNodes"] = value; }
		}

		public bool CanApproveNodes {
			get {
				object obj = this.ViewState["CanApproveNodes"];
				return ( obj == null ? false : (bool) obj );
			}
			set { this.ViewState["CanApproveNodes"] = value; }
		}

		#endregion

		#region Components

		/// <summary>
		/// Label control for displaying the header.
		/// </summary>
		public Label HeaderLabel {
			get {
				this.EnsureChildControls();
				return ( headerLabel );
			}
		}
		private Label headerLabel;

		public DropDownList TreeKeyListBox {
			get {
				this.EnsureChildControls();
				return ( this.treeKeyListBox );
			}
		}
		private DropDownList treeKeyListBox;
		
		/// <summary>
		/// Button to expand all nodes in the grid.
		/// </summary>
		public System.Web.UI.WebControls.Button ExpandAllButton {
			get {
				this.EnsureChildControls();
				return ( this.expandAllButton );
			}
		}
		private System.Web.UI.WebControls.Button expandAllButton;
		
		/// <summary>
		/// Button to collapse all nodes in the grid.
		/// </summary>
		public System.Web.UI.WebControls.Button CollapseAllButton {
			get {
				this.EnsureChildControls();
				return ( this.collapseAllButton );
			}
		}
		private System.Web.UI.WebControls.Button collapseAllButton;
		
		/// <summary>
		/// Button to move the selected node up.
		/// </summary>
		public System.Web.UI.WebControls.Button MoveUpButton {
			get {
				this.EnsureChildControls();
				return ( this.moveUpButton );
			}
		}
		private System.Web.UI.WebControls.Button moveUpButton;
		
		/// <summary>
		/// Button to move the selected node down.
		/// </summary>
		public System.Web.UI.WebControls.Button MoveDownButton {
			get {
				this.EnsureChildControls();
				return ( this.moveDownButton );
			}
		}
		private System.Web.UI.WebControls.Button moveDownButton;
		
		/// <summary>
		/// Button to move the selected node left.
		/// </summary>
		public System.Web.UI.WebControls.Button MoveLeftButton {
			get {
				this.EnsureChildControls();
				return ( this.moveLeftButton );
			}
		}
		private System.Web.UI.WebControls.Button moveLeftButton;
		
		/// <summary>
		/// Button to move the selected node right.
		/// </summary>
		public System.Web.UI.WebControls.Button MoveRightButton {
			get {
				this.EnsureChildControls();
				return ( this.moveRightButton );
			}
		}
		private System.Web.UI.WebControls.Button moveRightButton;
		
		/// <summary>
		/// Button to delete the selected node.
		/// </summary>
		public System.Web.UI.WebControls.Button DeleteButton {
			get {
				this.EnsureChildControls();
				return ( this.deleteButton );
			}
		}
		private System.Web.UI.WebControls.Button deleteButton;
		
		/// <summary>
		/// Button to edit the selected node.
		/// </summary>
		public System.Web.UI.WebControls.Button EditButton {
			get {
				this.EnsureChildControls();
				return ( this.editButton );
			}
		}
		private System.Web.UI.WebControls.Button editButton;
		
		/// <summary>
		/// Button to insert a new node following the selected node.
		/// </summary>
		public System.Web.UI.WebControls.Button InsertNextButton {
			get {
				this.EnsureChildControls();
				return ( this.insertNextButton );
			}
		}
		private System.Web.UI.WebControls.Button insertNextButton;

		/// <summary>
		/// Button to insert a new node as a child of the selected node.
		/// </summary>
		public System.Web.UI.WebControls.Button InsertChildButton {
			get {
				this.EnsureChildControls();
				return ( this.insertChildButton );
			}
		}
		private System.Web.UI.WebControls.Button insertChildButton;

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion
		
		#endregion
		
		#region Events 
		
		/// <summary>
		/// Event raised when one of the buttons are clicked.
		/// </summary>
		public event CommandEventHandler ButtonClick {
			add { Events.AddHandler( ButtonClickEventKey, value ); }
			remove { Events.RemoveHandler( ButtonClickEventKey, value ); }
		}
		protected virtual void OnButtonClick( Object sender, CommandEventArgs args ) {
			CommandEventHandler handler = (CommandEventHandler) Events[ButtonClickEventKey];
			if ( handler != null ) {
				handler( sender, args );
			}
		}
		private static readonly object ButtonClickEventKey = new object();
		
		#endregion
		
		#region Constructor 
		
		public TreeEditorGridController() : base() {
			this.CellSpacing = 2;
			this.CellPadding = 0;
			this.BorderStyle = BorderStyle.None;
			this.Width = new Unit( "100%" );
			this.BackColor = Color.LightGray;
		}
		
		#endregion
		
		#region Methods 
		
		protected override void CreateChildControls() {
			this.ID = this.Parent.ID + "_gridcontroller";
			
			#region HeaderLabel
			this.headerLabel = new Label();
			this.headerLabel.Font.Name = "Tahoma";
			this.headerLabel.Font.Size = new FontUnit( "11pt" );
			this.headerLabel.Font.Bold = true;
			this.headerLabel.Style["padding-left"] = "3pt";
			this.headerLabel.Text = "Menu&nbsp;Tree:&nbsp;";
			#endregion

			#region treeKeyListBox
			this.treeKeyListBox = new DropDownList();
			this.treeKeyListBox.ID = string.Format( "{0}_ddl", this.ID );
			this.treeKeyListBox.Font.Name = "Tahoma";
			this.treeKeyListBox.Font.Size = new FontUnit( "11pt" );
			this.treeKeyListBox.Font.Bold = false;
			this.treeKeyListBox.Style["width"] = "100%";
			this.treeKeyListBox.EnableViewState = true;
			this.treeKeyListBox.AutoPostBack = true;
			#endregion
			
			#region ExpandAllButton
			this.expandAllButton = new System.Web.UI.WebControls.Button();
			this.expandAllButton.EnableTheming = false;
			this.expandAllButton.Height = new Unit( "30" );
			this.expandAllButton.Width = new Unit( "30" );
			this.expandAllButton.ToolTip = "Expand All";
			this.expandAllButton.Text = ",";
			this.expandAllButton.Font.Name = "WingDings 3";
			this.expandAllButton.Font.Size = new FontUnit( "14pt" );
			this.expandAllButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.GridExpandAll );
			this.expandAllButton.CommandName = TreeEditorCommandNames.GridExpandAll;
			this.expandAllButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.expandAllButton.CausesValidation = false;
			#endregion
			
			#region CollapseAllButton
			this.collapseAllButton = new System.Web.UI.WebControls.Button();
			this.collapseAllButton.EnableTheming = false;
			this.collapseAllButton.Height = new Unit( "30" );
			this.collapseAllButton.Width = new Unit( "30" );
			this.collapseAllButton.ToolTip = "Collapse All";
			this.collapseAllButton.Text = "+";
			this.collapseAllButton.Font.Name = "WingDings 3";
			this.collapseAllButton.Font.Size = new FontUnit( "14pt" );
			this.collapseAllButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.GridCollapseAll );
			this.collapseAllButton.CommandName = TreeEditorCommandNames.GridCollapseAll;
			this.collapseAllButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.collapseAllButton.CausesValidation = false;
			#endregion
			
			#region MoveUpButton
			this.moveUpButton = new System.Web.UI.WebControls.Button();
			this.moveUpButton.EnableTheming = false;
			this.moveUpButton.Height = new Unit( "30" );
			this.moveUpButton.Width = new Unit( "30" );
			this.moveUpButton.ToolTip = "Move Up";
			this.moveUpButton.Text = "�";
			this.moveUpButton.Font.Name = "WingDings 3";
			this.moveUpButton.Font.Size = new FontUnit( "14pt" );
			this.moveUpButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeMoveUp );
			this.moveUpButton.CommandName = TreeEditorCommandNames.NodeMoveUp;
			this.moveUpButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.moveUpButton.Enabled = this.CanMoveNodes;
			this.moveUpButton.CausesValidation = false;
			#endregion
			
			#region MoveDownButton
			this.moveDownButton = new System.Web.UI.WebControls.Button();
			this.moveDownButton.EnableTheming = false;
			this.moveDownButton.Height = new Unit( "30" );
			this.moveDownButton.Width = new Unit( "30" );
			this.moveDownButton.ToolTip = "Move Down";
			this.moveDownButton.Text = "�";
			this.moveDownButton.Font.Name = "WingDings 3";
			this.moveDownButton.Font.Size = new FontUnit( "14pt" );
			this.moveDownButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeMoveDown );
			this.moveDownButton.CommandName = TreeEditorCommandNames.NodeMoveDown;
			this.moveDownButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.moveDownButton.Enabled = this.CanMoveNodes;
			this.moveDownButton.CausesValidation = false;
			#endregion
			
			#region MoveLeftButton
			this.moveLeftButton = new System.Web.UI.WebControls.Button();
			this.moveLeftButton.EnableTheming = false;
			this.moveLeftButton.Height = new Unit( "30" );
			this.moveLeftButton.Width = new Unit( "30" );
			this.moveLeftButton.ToolTip = "Move Left";
			this.moveLeftButton.Text = "�";
			this.moveLeftButton.Font.Name = "WingDings 3";
			this.moveLeftButton.Font.Size = new FontUnit( "14pt" );
			this.moveLeftButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeMoveLeft );
			this.moveLeftButton.CommandName = TreeEditorCommandNames.NodeMoveLeft;
			this.moveLeftButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.moveLeftButton.Enabled = this.CanMoveNodes;
			this.moveLeftButton.CausesValidation = false;
			#endregion
			
			#region MoveRightButton
			this.moveRightButton = new System.Web.UI.WebControls.Button();
			this.moveRightButton.EnableTheming = false;
			this.moveRightButton.Height = new Unit( "30" );
			this.moveRightButton.Width = new Unit( "30" );
			this.moveRightButton.ToolTip = "Move Right";
			this.moveRightButton.Text = "�";
			this.moveRightButton.Font.Name = "WingDings 3";
			this.moveRightButton.Font.Size = new FontUnit( "14pt" );
			this.moveRightButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeMoveRight );
			this.moveRightButton.CommandName = TreeEditorCommandNames.NodeMoveRight;
			this.moveRightButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.moveRightButton.Enabled = this.CanMoveNodes;
			this.moveRightButton.CausesValidation = false;
			#endregion
			
			#region DeleteButton
			this.deleteButton = new System.Web.UI.WebControls.Button();
			this.deleteButton.EnableTheming = false;
			this.deleteButton.Height = new Unit( "30" );
			this.deleteButton.Width = new Unit( "30" );
			this.deleteButton.ToolTip = "Delete";
			this.deleteButton.Text = "r";
			this.deleteButton.Font.Name = "WebDings";
			this.deleteButton.Font.Size = new FontUnit( "14pt" );
			this.deleteButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeDelete );
			this.deleteButton.CommandName = TreeEditorCommandNames.NodeDelete;
			this.deleteButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.deleteButton.OnClientClick = "return confirm( 'Are you sure you want to delete the selected section (and all subsections)?' );";
			this.deleteButton.Enabled = this.CanDeleteNodes;
			this.deleteButton.CausesValidation = false;
			#endregion
			
			#region InsertNextButton
			this.insertNextButton = new System.Web.UI.WebControls.Button();
			this.insertNextButton.EnableTheming = false;
			this.insertNextButton.Height = new Unit( "30" );
			this.insertNextButton.Width = new Unit( "30" );
			this.insertNextButton.ToolTip = "Insert After";
			this.insertNextButton.Text = "B";
			this.insertNextButton.Font.Name = "WingDings 3";
			this.insertNextButton.Font.Size = new FontUnit( "14pt" );
			this.insertNextButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeInsertNext );
			this.insertNextButton.CommandName = TreeEditorCommandNames.NodeInsertNext;
			this.insertNextButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.insertNextButton.Enabled = this.CanAddNodes;
			this.insertNextButton.CausesValidation = false;
			#endregion
			
			#region InsertChildButton
			this.insertChildButton = new System.Web.UI.WebControls.Button();
			this.insertChildButton.EnableTheming = false;
			this.insertChildButton.Height = new Unit( "30" );
			this.insertChildButton.Width = new Unit( "30" );
			this.insertChildButton.ToolTip = "Insert New Child";
			this.insertChildButton.Text = "C";
			this.insertChildButton.Font.Name = "WingDings 3";
			this.insertChildButton.Font.Size = new FontUnit( "14pt" );
			this.insertChildButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeInsertChild );
			this.insertChildButton.CommandName = TreeEditorCommandNames.NodeInsertChild;
			this.insertChildButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.insertChildButton.Enabled = this.CanAddNodes;
			this.insertChildButton.CausesValidation = false;
			#endregion
			
			#region EditButton
			this.editButton = new System.Web.UI.WebControls.Button();
			this.editButton.EnableTheming = false;
			this.editButton.Height = new Unit( "30" );
			this.editButton.Width = new Unit( "30" );
			this.editButton.ToolTip = "Edit";
			this.editButton.Text = "$";
			this.editButton.Font.Name = "WingDings 2";
			this.editButton.Font.Size = new FontUnit( "14pt" );
			this.editButton.ID = string.Format( "{0}_{1}_btn", this.ID, TreeEditorCommandNames.NodeEdit );
			this.editButton.CommandName = TreeEditorCommandNames.NodeEdit;
			this.editButton.Command += new CommandEventHandler( this.OnButtonClick );
			this.editButton.Enabled = this.CanEditNodes;
			this.editButton.CausesValidation = false;
			#endregion
			
			// Put it all together now...
			this.Rows.Clear();
			TableRow row = new TableRow();

			TableCell cell = new TableCell();
			//cell.Width = new Unit( "25%" );
			cell.Controls.Add( this.HeaderLabel );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Width = new Unit( "50%" );
			cell.Controls.Add( this.TreeKeyListBox );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.ExpandAllButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.CollapseAllButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.MoveUpButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.MoveDownButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.MoveLeftButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.MoveRightButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.DeleteButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.InsertNextButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.InsertChildButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( this.EditButton );
			row.Cells.Add( cell );

			cell = new TableCell();
			cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
			row.Cells.Add( cell );
			
			this.Rows.Add( row );
		}
		
		#endregion

	}
	
}
