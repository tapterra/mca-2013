using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Abstract base class for NodeFormPanel.
	/// </summary>
	public abstract class NodeFormPanel : Table {
		
		#region  Properties 
		
		public abstract string PathKey { get; set; }
		public abstract string Title { get; set; }
		public abstract string Comments { get; set; }
		public abstract string Description { get; set; }
		
		public NodeFormController FormController {
			get {
				this.EnsureChildControls();
				return ( this.formController );
			}
		}
		protected NodeFormController formController;

		public Table MainFormTable {
			get {
				this.EnsureChildControls();
				return ( this.mainFormTable );
			}
		}
		protected Table mainFormTable;

		public Panel ContentPanel {
			get {
				this.EnsureChildControls();
				return ( this.contentPanel );
			}
		}
		protected Panel contentPanel;

		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion
		
		#region Events 
		/// <summary>
		/// Raised when the contents of the form are to be persisted.
		/// </summary>
		public event EventHandler SaveForm {
			add { Events.AddHandler( SaveFormEventKey, value ); }
			remove { Events.RemoveHandler( SaveFormEventKey, value ); }
		}
		protected virtual void OnSaveForm( Object sender, EventArgs args ) {
			EventHandler btnClickHandler = (EventHandler) Events[SaveFormEventKey];
			if ( btnClickHandler != null ) btnClickHandler( sender, args );
		}
		private static readonly object SaveFormEventKey = new object();
		
		/// <summary>
		/// Raised when the form is to be closed.
		/// </summary>
		public event EventHandler CloseForm {
			add { Events.AddHandler( CloseFormEventKey, value ); }
			remove { Events.RemoveHandler( CloseFormEventKey, value ); }
		}
		protected virtual void OnCloseForm( Object sender, EventArgs args ) {
			EventHandler btnClickHandler = (EventHandler) Events[CloseFormEventKey];
			if ( btnClickHandler != null ) btnClickHandler( sender, args );
		}
		private static readonly object CloseFormEventKey = new object();
		
		#endregion
		
		#region Methods 
		
		protected internal void NodeFormCommands( Object sender, CommandEventArgs args ) {
			switch( args.CommandName ) {
				case TreeEditorCommandNames.NodeFormSave:
					this.OnSaveForm( this, new EventArgs() );
					break;
				case TreeEditorCommandNames.NodeFormCancel:
					this.OnCloseForm( this, new EventArgs() );
					break;
			}
		}
		
		#endregion
		
	}
	
}
