using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.TreeUI;
using StarrTech.WebTools.DBUI;


namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Tree editor for the document-library folders
	/// </summary>
	public class DocLibEditor : BaseTreeEditor {
		
		protected IAdminDataGridContentEditor documentGrid;
		
		#region Constructor
		
		public DocLibEditor() : base() {
			this.BorderColor = Color.Black;
			this.BorderStyle = BorderStyle.Solid;
			this.BorderWidth = 1;
			this.Width = new Unit( "100%" );
			this.BackColor = Color.White;
			this.Style["padding"] = "1px";
			this.TreePanel.Controller.HeaderText = "Document&nbsp;Library:&nbsp;";
		}
		
		#endregion
		
		#region Methods 
		
		protected override void CreateChildControls() {
			// Build the controls
			this.nodePanel = new DocNodeFormPanel();
			// HACK: The following line looks useless, but without it, for some reason the event-handlers 
			// for the Save & Cancel buttons on the form don't get triggered ?
			// The button is being assigned a unique ID, so don't bother checking that...
			this.nodePanel.FormController.SaveButton.Text = this.NodePanel.FormController.SaveButton.Text;
			this.nodePanel.Width = new Unit( "100%" );
			this.nodePanel.Visible = false;
			
			// Build the TreePanel
			this.treePanel = new TreeEditorPanel();
			this.treePanel.Width = new Unit( "100%" );
			this.treePanel.Visible = true;
			
			// ValidationMessage
			this.validationMessage = new ValidationSummary();
			this.validationMessage.DisplayMode = ValidationSummaryDisplayMode.BulletList;
			this.validationMessage.ShowSummary = false;
			this.validationMessage.ShowMessageBox = true;
			this.validationMessage.Font.Bold = true;
			this.validationMessage.Font.Name = "Verdana";
			this.validationMessage.Font.Size = new FontUnit( "10px" );
			this.validationMessage.HeaderText = "Please correct the following errors before proceeding:";
			this.validationMessage.BorderStyle = BorderStyle.Solid;
			this.validationMessage.BorderWidth = new Unit( "1px" );
			this.validationMessage.BorderColor = Color.FromName( "#993333" );
			this.validationMessage.BackColor = Color.FromName( "#FFEEEE" );
			this.validationMessage.Width = new Unit( "500px" );
			this.validationMessage.Style["margin"] = "10px";
			this.validationMessage.Style["padding"] = "10px";
			
			// Put 'em together
			this.Controls.Clear();
			this.Controls.Add( this.nodePanel );
			this.Controls.Add( this.treePanel );
			this.Controls.Add( this.validationMessage );
			
			// Wire in the event handlers
			this.NodePanel.SaveForm += new EventHandler( this.SaveFormNode );
			this.NodePanel.CloseForm += new EventHandler( this.CancelForm );
			this.TreePanel.Grid.NodeEdit += new TreePanelNodeClickEventHandler( this.SetupNodeFormPanel );
			this.TreePanel.NodeEdit += new TreePanelNodeEditEventHandler( this.SetupNodeFormPanel );
			this.TreePanel.Grid.renderer = new DocTreeEditorGridRenderer( this.TreePanel.Grid );
		}
		
		/// <summary>
		/// Event handler for the Node Panel's Save button.
		/// </summary>
		protected internal void SaveFormNode( Object sender, EventArgs args ) {
			//this.TreePanel.Grid.ReloadControlTree();
			int nodeID = this.NodePanel.FormController.NodeID;
			/////this.TreePanel.Grid.SelectedNodeKey = nodeID.ToString();
			DocNodeFormPanel formPanel = this.NodePanel as DocNodeFormPanel;
			this.Page.Validate();
			if ( ! this.Page.IsValid ) return;
			if ( nodeID <= 0 ) {
				// We're inserting a new node...
				TreeNode newNode = new TreeNode();
				newNode.PathKey = formPanel.PathKey;
				newNode.Title = formPanel.Title;
				newNode.Comments = formPanel.Comments;
				newNode.TreeID = this.DataTree.ID;
				int parentID = formPanel.FormController.ParentNodeID;
				TreeNodeControl newControl = null;
				TreeNodeControl currentNodeControl = this.TreePanel.Grid.SelectedNode;
				TreeNode currentDataNode = ( currentNodeControl != null ? this.DataTree.FindNode( Convert.ToInt32(currentNodeControl.Key) ) : null );
				if ( parentID == 0 ) {
					if ( currentNodeControl != null ) {
						// Insert it into the DataTree
						this.DataTree.InsertNodeAfter( newNode, currentDataNode );
						// and into the UI...
						newControl = this.TreePanel.Grid.AddNodeAfter( newNode.Title, newNode.ID.ToString(), currentNodeControl );
					} else {
						// Insert it into the DataTree
						this.DataTree.AddNode( newNode );
						// and into the UI...
						newControl = this.TreePanel.Grid.AddNode( newNode.Title, newNode.ID.ToString() );
					}
				} else {
					// Insert it into the DataTree
					// Is this an InsertNext or InsertParent?
					TreeNodeControl parentControl = this.TreePanel.Grid.FindTreeNodeControl( parentID.ToString() );
					TreeNode parentNode = this.DataTree.FindNode( parentID );
					if ( ( currentNodeControl != null ) && ( parentID == Convert.ToInt32(currentNodeControl.Key) ) ) {
						// If its an InsertChild, the parentID we got will equal the ID of the currently-selected node
						if ( parentNode != null ) {
							parentNode.AddNode( newNode );
							newControl = parentControl.AddNode( newNode.Title, newNode.ID.ToString() );
							parentControl.IsExpanded = true;
						}
					} else {
						// We're gonna insert the new node following the currently-selected node
						parentNode.InsertNodeAfter( newNode, currentDataNode );
						newControl = parentControl.AddNodeAfter( newNode.Title, newNode.ID.ToString(), false, currentNodeControl );
					}
				}
				if ( newControl != null ) {
					newControl["key"] = newNode.PathKey;
					this.TreePanel.Grid.SelectedNodeKey = newControl.Key;
				}
				this.SetupNodeFormPanel( newNode, TreeEditorCommandNames.NodeEdit );
			} else {
				// we're saving an existing node - replacing the SelectedNode
				TreeNode node = this.TreePanel.CurrentNode;
				node.PathKey = formPanel.PathKey;
				node.Title = formPanel.Title;
				node.Comments = formPanel.Comments;
				node.Persist();
				this.TreePanel.Grid.SelectedNode["key"] = node.PathKey;
				this.TreePanel.Grid.SelectedNode.Title = node.Title;
				this.SetupTreeGridPanel();
			}
		}
		
		/// <summary>
		/// Override to load the AdminDataGrid user control
		/// </summary>
		protected override void OnInit( EventArgs args ) {
			// Load the Document Editor Grid
			Control editor = this.Page.LoadControl( "~/usercontrols/DocsGrid.ascx" );
			editor.ID = this.ID + "_dox_editor";
			this.NodePanel.ContentPanel.Controls.Add( editor );
			base.OnInit( args );
			this.documentGrid = editor as IAdminDataGridContentEditor;
			this.documentGrid.DataGrid.FormControlsInit += new EventHandler( DocsGridFormControlsInit );
			this.documentGrid.DataGrid.FormClosing += new EventHandler( DocsGridFormClosing );
		}
		
		protected internal override void SetupNodeFormPanel( TreeNode CurrentNode, string CommandName ) {
			base.SetupNodeFormPanel( CurrentNode, CommandName );
			// Let our grid know which node he's working for...
			this.documentGrid.NodeID = this.NodePanel.FormController.NodeID;
			// Refresh the grid...
			//this.documentGrid.DataGrid.BindGrid();
			this.documentGrid.DataGrid.InitPanel();
			// We already have a ValidationSummary, so the editor won't need one...
			this.documentGrid.DataGrid.MessagePanel.ValidationMessage.Visible = false;
		}
		
		private void DocsGridFormControlsInit( object sender, EventArgs e ) {
			this.NodePanel.FormController.Enabled = false;
		}
		
		private void DocsGridFormClosing( object sender, EventArgs e ) {
			this.NodePanel.FormController.Enabled = true;
		}
		
		#endregion
		
	}
	
	/// <summary>
	/// Tree grid for use by the DocLibEditor
	/// </summary>
	public class DocTreeEditorGrid : TreeEditorGrid {
			
		public DocTreeEditorGrid() : base() {}
		
			
		public override BaseTreeRenderer Renderer {
			get { 
				if ( this.renderer == null ) {
					this.renderer = new CMS.UI.DocTreeEditorGridRenderer( this );
				}
				return ( this.renderer ); 
			}
		}

			
	}
	
}
