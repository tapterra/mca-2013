using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools.TreeUI;
using System.Web;

namespace StarrTech.WebTools.CMS.UI {

	/// <summary>
	/// Summary description for BaseTreeEditor.
	/// </summary>
	public class BaseTreeEditor : Panel {
		
		#region Properties

		#region Appearance

		public override Unit Width {
			get {
				return base.Width;
			}
			set {
				base.Width = value;
				this.EnsureChildControls();
				this.NodePanel.Width = value;
				this.TreePanel.Width = value;
			}
		}
		public string TreeGridCssClass {
			get { return ( this.TreePanel.Grid.CssClass ); }
			set { this.TreePanel.Grid.CssClass = value; }
		}

		public string Header1CssClass {
			get {
				object obj = ViewState["Header1CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header1CssClass"] = value; }
		}

		public string Header2CssClass {
			get {
				object obj = ViewState["Header2CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header2CssClass"] = value; }
		}

		public string Header3CssClass {
			get {
				object obj = ViewState["Header3CssClass"];
				return ( obj == null ? "" : obj.ToString() );
			}
			set { ViewState["Header3CssClass"] = value; }
		}

		public string TreeNodeCssClass {
			get { return ( this.TreePanel.Grid.CssClass ); }
			set { this.TreePanel.Grid.CssClass = value; }
		}

		#region Grid formatting properties
		/// <summary>
		/// The horizontal align of the header table
		/// </summary>
		public HorizontalAlign GridHeaderHorizAlign {
			get { return ( this.TreePanel.Grid.TableHeaderHorizAlign ); }
			set { this.TreePanel.Grid.TableHeaderHorizAlign = value; }
		}

		/// <summary>
		/// The back color for the table
		/// </summary>
		public Color GridBackColor {
			get { return ( this.TreePanel.Grid.TableBackColor ); }
			set { this.TreePanel.Grid.TableBackColor = value; }
		}

		/// <summary>
		/// The cellpadding of the table
		/// </summary>
		public int GridCellPadding {
			get { return ( this.TreePanel.Grid.TableCellPadding ); }
			set { this.TreePanel.Grid.TableCellPadding = value; }
		}

		/// <summary>
		/// The cellspacing of the table
		/// </summary>
		public int GridCellSpacing {
			get { return ( this.TreePanel.Grid.TableCellSpacing ); }
			set { this.TreePanel.Grid.TableCellSpacing = value; }
		}

		/// <summary>
		/// The border size of the table
		/// </summary>
		public int GridBorder {
			get { return ( this.TreePanel.Grid.TableBorder ); }
			set { this.TreePanel.Grid.TableBorder = value; }
		}

		/// <summary>
		/// The back color for header text
		/// </summary>
		public Color GridHeaderBackColor {
			get { return ( this.TreePanel.Grid.HeaderBackColor ); }
			set { this.TreePanel.Grid.HeaderBackColor = value; }
		}

		/// <summary>
		/// The fore color for header text
		/// </summary>
		public Color GridHeaderForeColor {
			get { return ( this.TreePanel.Grid.HeaderForeColor ); }
			set { this.TreePanel.Grid.HeaderForeColor = value; }
		}

		/// <summary>
		/// The foreground color for node text
		/// </summary>
		public Color GridRowForeColor {
			get { return ( this.TreePanel.Grid.DataForeColor ); }
			set { this.TreePanel.Grid.DataForeColor = value; }
		}

		/// <summary>
		/// The background color for each node row
		/// </summary>
		public Color GridRowBackColor {
			get { return ( this.TreePanel.Grid.DataBackColor ); }
			set { this.TreePanel.Grid.DataBackColor = value; }
		}

		/// <summary>
		/// The background color for each alternating node row
		/// </summary>
		public Color GridAltRowBackColor {
			get { return ( this.TreePanel.Grid.AltDataBackColor ); }
			set { this.TreePanel.Grid.AltDataBackColor = value; }
		}

		/// <summary>
		/// The background color for the panel's selected node row
		/// </summary>
		public Color GridSelectedRowBackColor {
			get { return ( this.TreePanel.Grid.SelectedDataBackColor ); }
			set { this.TreePanel.Grid.SelectedDataBackColor = value; }
		}

		/// <summary>
		/// The foreground color for the panel's selected node row
		/// </summary>
		public Color GridSelectedRowForeColor {
			get { return ( this.TreePanel.Grid.SelectedDataForeColor ); }
			set { this.TreePanel.Grid.SelectedDataForeColor = value; }
		}

		/// <summary>
		/// The background color for the panel's selected node row
		/// </summary>
		public Color GridHilightedRowBackColor {
			get { return ( this.TreePanel.Grid.RowHilightedBackColor ); }
			set { this.TreePanel.Grid.RowHilightedBackColor = value; }
		}

		#endregion
		
		#endregion

		public string TreeKey {
			get { return ( this.TreePanel.TreeKey ); }
			set { this.TreePanel.TreeKey = value; }
		}
		
		public Tree DataTree {
			get { return ( this.TreePanel.DataTree ); }
		}
		
		public TreeEditorPanel TreePanel {
			get { 
				this.EnsureChildControls();
				return ( this.treePanel ); 
			}
		}
		protected TreeEditorPanel treePanel;
		
		public NodeFormPanel NodePanel {
			get { 
				this.EnsureChildControls();
				return ( this.nodePanel ); 
			}
		}
		protected NodeFormPanel nodePanel;

		public ValidationSummary ValidationMessage {
			get {
				EnsureChildControls();
				return ( validationMessage );
			}
		}
		protected ValidationSummary validationMessage;
		
		public override ControlCollection Controls {
			get {
				this.EnsureChildControls();
				return ( base.Controls );
			}
		}
		
		#endregion
		
		#region Methods 
		
		protected void SetupTreeGridPanel() {
			HttpContext.Current.Trace.Warn( ">>> BaseTreeEditor.SetupTreeGridPanel" );
			this.TreePanel.Visible = true;
			this.NodePanel.Visible = false;
			this.TreePanel.SetupButtons();
			this.NodePanel.FormController.Enabled = true;
			HttpContext.Current.Trace.Warn( "<<< BaseTreeEditor.SetupTreeGridPanel" );
		}

		protected internal virtual void SetupNodeFormPanel( Object sender, TreePanelNodeClickEventArgs args ) {
			this.SetupNodeFormPanel( this.TreePanel.CurrentNode, TreeEditorCommandNames.NodeEdit );
		}

		protected internal virtual void SetupNodeFormPanel( Object sender, TreePanelNodeEditEventArgs args ) {
			this.SetupNodeFormPanel( args.Node, args.CommandName ); 
		}
		
		protected internal virtual void SetupNodeFormPanel( TreeNode CurrentNode, string CommandName ) {
			this.TreePanel.Visible = false;
			this.NodePanel.Visible = true;
			this.NodePanel.FormController.Enabled = true;
			if ( CommandName == TreeEditorCommandNames.NodeEdit ) {
				// We're editing an existing node ...
				// Set up the node-properties area for the current node
				this.NodePanel.FormController.NodeID = CurrentNode.ID;
				this.NodePanel.FormController.ParentNodeID = CurrentNode.ParentID;
				this.NodePanel.PathKey = CurrentNode.PathKey;
				this.NodePanel.Title = CurrentNode.Title;
				this.NodePanel.Comments = CurrentNode.Comments;
				this.NodePanel.ContentPanel.Visible = true;
			} else {
				// We're adding a new node...
				this.NodePanel.FormController.NodeID = -1;
				// Finish setting up the panel
				switch( CommandName ) {
					case TreeEditorCommandNames.NodeInsertChild:
						// if we're inserting a new child node, the parentID is the ID of the currently-selected node
						this.NodePanel.FormController.ParentNodeID = CurrentNode.ID;
						break;
					case TreeEditorCommandNames.NodeInsertNext:
						// if we're inserting a new sibling, the parentID is the ID of the parent of the currently-selected node
						// (...unless there is no currently-selected node, in which case the parentID is zero).
						this.NodePanel.FormController.ParentNodeID = ( CurrentNode != null ? CurrentNode.ParentID : 0 );
						break;
				}
				this.NodePanel.PathKey = "";
				this.NodePanel.Title = "";
				this.NodePanel.Comments = "";
				this.NodePanel.ContentPanel.Visible = false;
			}
		}
		
		protected internal void CancelForm( Object sender, EventArgs args ) {
			this.SetupTreeGridPanel();
		}
		
		#endregion

	}
	
	/// <summary>
	/// Named constants for the commanders used by the TreeEditor components
	/// </summary>
	public class TreeEditorCommandNames {
		public const string GridExpandAll = "gridexpandall";
		public const string GridCollapseAll = "gridcollapseall";
		public const string NodeMoveUp = "nodemoveup";
		public const string NodeMoveDown = "nodemovedown";
		public const string NodeMoveLeft = "nodemoveleft";
		public const string NodeMoveRight = "nodemoveright";
		public const string NodeEdit = "nodeedit";
		public const string NodeDelete = "nodedelete";
		public const string NodeInsertNext = "nodeinsertnext";
		public const string NodeInsertChild = "nodeinsertchild";
		public const string NodeFormSave = "nodeformsave";
		public const string NodeFormCancel = "nodeformcancel";
		public const string NodeRollup = "noderollup";
	}
	
}
