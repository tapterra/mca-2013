using System;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using StarrTech.WebTools.TreeUI;


namespace StarrTech.WebTools.CMS.UI {
	/// <summary>
	/// Panel tying a TreeEditorGrid and TreeEditorGridController together.
	/// </summary>
	public class TreeEditorPanel : Panel {
		
		#region Properties
		
		public string TreeKey {
			get { return ( this.Grid.TreeKey ); }
			set { this.Grid.TreeKey = value; }
		}
		
		public Tree DataTree {
			get { return ( this.Grid.DataTree ); }
		}
		
		public TreeNode CurrentNode {
			get {
				TreeNodeControl currentNodeControl = this.Grid.SelectedNode;
				if ( currentNodeControl == null ) return ( null );
				return ( this.DataTree.FindNode( Convert.ToInt32( this.Grid.SelectedNodeKey ) ) );
			}
			set { this.Grid.SelectedNodeKey = value.ID.ToString(); }
		}
		
		#region Components

		public virtual TreeEditorGrid Grid {
			get {
				this.EnsureChildControls();
				return ( this.grid );
			}
		}
		private TreeEditorGrid grid;
		
		public TreeEditorGridController Controller {
			get {
				this.EnsureChildControls();
				return ( this.controller );
			}
		}
		private TreeEditorGridController controller;
		
		#endregion
		
		#endregion
		
		#region Events
		
		/// <summary>
		/// Event fired when one of the controller's edit/insert buttons are clicked.
		/// </summary>
		public event TreePanelNodeEditEventHandler NodeEdit {
			add { Events.AddHandler( NodeEditEventKey, value ); }
			remove { Events.RemoveHandler( NodeEditEventKey, value ); }
		}
		public virtual void OnNodeEdit( Object sender, TreePanelNodeEditEventArgs args ) {
			TreePanelNodeEditEventHandler handler = (TreePanelNodeEditEventHandler) Events[NodeEditEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object NodeEditEventKey = new object();
		
		#endregion
		
		#region Methods
		
		protected TreeNode GetControlDataNode( TreeNodeControl nodeControl ) {
			if ( nodeControl == null ) return ( null );
			return ( this.DataTree.FindNode( Convert.ToInt32( nodeControl.Key ) ) );
		}
		
		protected override void CreateChildControls() {
			
			#region Controller
			this.controller = new TreeEditorGridController();
			this.controller.Width = new Unit( "100%" );
			this.controller.ButtonClick += new CommandEventHandler( this.TreeCommands );
			//this.controller.TreeKeyChange +=new EventHandler( this.ControllerTreeKeyChange );
			#endregion
			
			#region Grid
			this.grid = new TreeEditorGrid();
			this.grid.Width = new Unit( "100%" );
			this.grid.NodeSelectedChanged += new TreePanelNodeClickEventHandler( this.NodeSelectedChanged );
			this.grid.NodeExpandedChanged += new TreePanelNodeClickEventHandler( this.NodeExpandedChanged );
			#endregion
			
			// Stack it up...
			this.Controls.Clear();
			this.Controls.Add( this.controller );
			this.Controls.Add( this.grid );
			this.SetupButtons();
		}
		
		protected void ControllerTreeKeyChange(object sender, EventArgs e) {
			this.TreeKey = this.Controller.CurrentTreeKey;
		}
		
		protected internal void NodeSelectedChanged( Object sender, TreePanelNodeClickEventArgs args ) {
			this.SetupButtons();
		}
		
		protected internal void NodeExpandedChanged( Object sender, TreePanelNodeClickEventArgs args ) {
			this.SetupButtons();
		}
		
		protected void TreeCommands( Object sender, CommandEventArgs args ) {
			string cmd = args.CommandName;
			TreeNode currentNode = this.CurrentNode;
			// These commands don't require a current-node...
			switch( cmd ) {
				case TreeEditorCommandNames.GridExpandAll:
					this.Grid.ExpandAll();
					break;
				case TreeEditorCommandNames.GridCollapseAll:
					this.Grid.CollapseAll();
					break;
				case TreeEditorCommandNames.NodeInsertNext:
					this.OnNodeEdit( this, new TreePanelNodeEditEventArgs( currentNode, cmd ) );
					break;
				default:
					//... but all the rest of these do...
					if ( currentNode == null ) return;
					switch( cmd ) {
						case TreeEditorCommandNames.NodeInsertChild:
							this.OnNodeEdit( this, new TreePanelNodeEditEventArgs( currentNode, cmd ) );
							break;
						case TreeEditorCommandNames.NodeRollup:
							this.NodeRollup( currentNode );
							break;
						case TreeEditorCommandNames.NodeEdit:
							this.OnNodeEdit( this, new TreePanelNodeEditEventArgs( currentNode, cmd ) );
							break;
						case TreeEditorCommandNames.NodeDelete:
							this.Grid.RemoveNode( this.Grid.SelectedNode );
							this.DataTree.DeleteNode( currentNode );
							break;
						case TreeEditorCommandNames.NodeMoveUp:
							this.DataTree.MoveNodeUp( currentNode );
							this.Grid.MoveNodeUp( this.Grid.SelectedNode );
							break;
						case TreeEditorCommandNames.NodeMoveDown:
							this.DataTree.MoveNodeDown( currentNode );
							this.Grid.MoveNodeDown( this.Grid.SelectedNode );
							break;
						case TreeEditorCommandNames.NodeMoveLeft:
							this.DataTree.MoveNodeLeft( currentNode );
							this.Grid.MoveNodeLeft( this.Grid.SelectedNode );
							break;
						case TreeEditorCommandNames.NodeMoveRight:
							this.DataTree.MoveNodeRight( currentNode );
							this.Grid.MoveNodeRight( this.Grid.SelectedNode );
							break;
					}
					// Flush the tree from the ContentPublisher's cache...
					ContentPublisher.ResetSiteTreeCache( this.TreeKey );
					break;
			}
			this.SetupButtons();
		}

		private void NodeRollup( TreeNode currentNode ) {
			currentNode.PromoteChildItems();
			//this.Grid.ReloadControlTree();
			this.Grid.SelectedNode.Controls.Clear();
		}
		
		public void SetupButtons() {
			// This button is always enabled (if no node is selected, it will add a new top-level node)
			this.Controller.InsertNextButton.Enabled = true;
			TreeNodeControl currentNodeCtrl = this.Grid.SelectedNode;
			TreeNode currentNode = this.GetControlDataNode( currentNodeCtrl );
			if ( currentNodeCtrl == null ) {
				// If there is no selected-node, disable all the node-specific commands
				this.Controller.EditButton.Enabled = false;
				this.Controller.DeleteButton.Enabled = false;
				this.Controller.InsertChildButton.Enabled = false;
				this.Controller.MoveLeftButton.Enabled = false;
				this.Controller.MoveRightButton.Enabled = false;
				this.Controller.MoveUpButton.Enabled = false;
				this.Controller.MoveDownButton.Enabled = false;
			} else { // We have a selected node...
				this.Controller.EditButton.Enabled = currentNode.CanEdit; 
				this.Controller.DeleteButton.Enabled = currentNode.CanDelete; 
				this.Controller.InsertChildButton.Enabled = currentNode.CanAddChildren; 
				if ( currentNodeCtrl.Parent is TreeNodeControl ) {
					TreeNode parentNode = this.GetControlDataNode( currentNodeCtrl.Parent as TreeNodeControl );
					this.Controller.InsertNextButton.Enabled = parentNode.CanAddChildren;
				}
				TreeNodeControl nextNodeCtrl = currentNodeCtrl.NextSibling();
				TreeNodeControl prevNodeCtrl = currentNodeCtrl.PrevSibling();
				int currentNodeCtrlIndex = currentNodeCtrl.Parent.Controls.IndexOf( currentNodeCtrl );
				
				this.Controller.MoveLeftButton.Enabled = currentNode.CanMoveH && ( currentNodeCtrl.Parent is TreeNodeControl ); 
				this.Controller.MoveRightButton.Enabled = currentNode.CanMoveH && ( currentNodeCtrlIndex > 0 ) && ( ! currentNode.MustBeRoot );
				if ( prevNodeCtrl != null ) {
					TreeNode prevNode = this.GetControlDataNode( prevNodeCtrl );
					this.Controller.MoveRightButton.Enabled = this.Controller.MoveRightButton.Enabled && ( prevNode.CanAddChildren );
				}
				this.Controller.MoveUpButton.Enabled = currentNode.CanMoveV && ( currentNodeCtrlIndex > 0 );
				this.Controller.MoveDownButton.Enabled = currentNode.CanMoveV && ( nextNodeCtrl != null );
			}
		}
		
		protected override void OnLoad(EventArgs e) {
			base.OnLoad (e);
			this.TreeKey = this.Controller.CurrentTreeKey;
		}
		
		#endregion
	}

}
