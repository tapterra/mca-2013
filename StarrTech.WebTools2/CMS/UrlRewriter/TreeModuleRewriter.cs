using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

using StarrTech.WebTools.CMS;


namespace StarrTech.WebTools.CMS.UrlRewriter {

	public class TreeModuleRewriter : BaseModuleRewriter {

		/// <summary>
		/// This method is called during the module's BeginRequest event.
		/// </summary>
		/// <param name="requestedUrl">The URL being requested (includes path and querystring).</param>
		/// <param name="app">The HttpApplication instance.</param>
		protected override void Rewrite( string requestedUrl, System.Web.HttpApplication app ) {
			// We never rewrite a parameterized URL...
			if ( app.Request.QueryString.Count > 0 ) { return; }
			// We only want to mess with aspx files...
			requestedUrl = requestedUrl.ToLower();
			string ext = Path.HasExtension( requestedUrl ) ? Path.GetExtension( requestedUrl ) : "";
			if ( !ext.Equals( ".aspx" ) ) { return; }
			// Leave the admin pages alone (not needed, but faster?)
			if ( requestedUrl.IndexOf( "/admin" ) >= 0 ) { return; }
			// Determine the new URL
			string redirectedUrl = ResolveRequestedUrl( requestedUrl, app );
			if ( redirectedUrl != requestedUrl ) {
				RewriterUtils.RewriteUrl( app.Context, redirectedUrl );
			}
		}

		/// <summary>
		/// Parses the incoming URL to determine the site tree & physical page
		/// </summary>
		private string ResolveRequestedUrl( string requestedUrl, System.Web.HttpApplication app ) {
			string nodeID = "";
			string treeKey = "";

			// Start with what we have...
			string redirectedUrl = requestedUrl.ToLower();									// = "/website/treekey/nodeX/nodeY/default.aspx"
			string appPath = app.Context.Request.ApplicationPath.ToLower();	// = "/website"
			string inUrl = redirectedUrl.Replace( appPath + "/", "/" );			// = "/treekey/nodeX/nodeY/default.aspx"

			string fileName = inUrl.Substring( inUrl.LastIndexOf( '/' ) );	// = "/default.aspx"
			string treePath = inUrl.Replace( fileName, "" );								// = "/treekey/nodeX/nodeY"
			string contentPath = treePath;

			if ( contentPath.Length == 0 && fileName == "/default.aspx" ) {
				// This must be the home page?
				treeKey = ContentPublisher.MainNavTreeKey;
				nodeID = ContentPublisher.GetContentTypeNodeID( treeKey, "homepage" ).ToString();
			} else if ( contentPath.Length > 0 ) {
				int contentStart = contentPath.IndexOf( "/", 1 );
				if ( contentStart < 0 ) {
					treeKey = contentPath.Substring( 1 );												// = "treekey"
					contentPath = "";																						// = "/nodeX/nodeY"
				} else {
					treeKey = contentPath.Substring( 1, contentStart - 1 );			// = "treekey"
					contentPath = contentPath.Substring( contentStart );				// = "/nodeX/nodeY"
				}
				nodeID = ContentPublisher.TreePathInheritedAttribute( treePath, "id" );
			} else {
				treeKey = ContentPublisher.MainNavTreeKey;
			}
			redirectedUrl = redirectedUrl.Replace( string.Format( "/{0}/", treeKey ), "/" );
			// Step through all the ContentTypes and see if any of them have a "publishedat" URL
			// equal to the incoming URL. If so, pass the URL along unchanged.
			foreach ( ContentType type in ContentTypeManager.ContentTypes ) {
				string typeUrl = RewriterUtils.ResolveUrl( appPath, type.PublishedAt.ToLower() );
				if ( typeUrl == redirectedUrl ) {
					//if ( type.EditorType == ContentTypeEditors.none ) {
					//  return ( typeUrl );
					//}
					nodeID = ContentPublisher.PublishedAtNodeID( treeKey, type.PublishedAt.ToLower() ).ToString();
					redirectedUrl = this.BuildStoryUrl( redirectedUrl, treeKey, nodeID, -1 );
					return ( redirectedUrl );
				}
			}
			// If we made it here, we've (potentially) got a ContentItem request ... 
			if ( fileName == "/login.aspx" ) {
				redirectedUrl = appPath + fileName;
			} else if ( fileName.StartsWith( "/story" ) ) {
				// If the filename is something like "storyxxx.aspx", then this is a content-item and xxx is the item ID 
				string treepath = inUrl.Replace( fileName, "" );
				int cid = -1;
				if ( Int32.TryParse( fileName.Replace( "/story", "" ).Replace( ".aspx", "" ), out cid ) ) {
					ContentItem itm = new ContentItem( cid );
					if ( !itm.IsVisible ) {
						return ( redirectedUrl );
					}
					treeKey = ContentPublisher.MainNavTreeKey;
					int nid = ContentPublisher.GetContentItemFirstNode( treeKey, itm.ID );
					if ( nid <= 0 ) {
						treeKey = ContentPublisher.AlternateNavTreeKey;
						nid = ContentPublisher.GetContentItemFirstNode( treeKey, itm.ID );
					}
					redirectedUrl = this.BuildStoryUrl( itm.ContentType.PublishedAt, treeKey, nid.ToString(), cid );
				}
			} else if ( fileName == "/default.aspx" ) {
				// If the requested folder-path (requestedUrl minus appPath minus "/default.aspx" is a valid site-tree path, 
				treePath = inUrl.Replace( fileName, "" ).Replace( treeKey + "/", "" );
				string pageFile = ContentPublisher.TreePathNodeContentType( treeKey, treePath ).PublishedAt;
				int cid = ContentPublisher.TreePathItemID( treeKey, treePath );
				if ( cid > 0 ) {
					redirectedUrl = this.BuildStoryUrl( pageFile, treeKey, nodeID, cid );
				} else {
					redirectedUrl = this.BuildStoryUrl( pageFile, treeKey, nodeID, cid );
				}
			}
			return ( redirectedUrl ); // we're done
		}

		private string BuildStoryUrl( string pageFile, string treeKey, string nodeID, int contentID ) {
			return ( string.Format( "{0}?{1}={2}&{3}={4}&{5}={6}", pageFile,
				ContentPublisher.TreeQKey, treeKey,
				ContentPublisher.NodeQKey, nodeID,
				ContentPublisher.CidQKey, contentID
			) );
		}
		
	}
	
}
