using System;
using System.Web;

namespace StarrTech.WebTools.CMS.UrlRewriter {
	
	/// <summary>
	/// Static helper methods for rewriting URLs (usually via HttpModule/HttpHandler).
	/// </summary>
	public class RewriterUtils {
		
		/// <summary>
		/// Rewrite's a URL using <b>HttpContext.RewriteUrl()</b>.
		/// </summary>
		/// <param name="context">The HttpContext object to rewrite the URL to.</param>
		/// <param name="newUrl">The URL to rewrite to.</param>
		public static void RewriteUrl( HttpContext context, string newUrl ) {
			string x, y;
			RewriteUrl( context, newUrl, out x, out y );
		}
		
		/// <summary>
		/// Rewrite's a URL using <b>HttpContext.RewriteUrl()</b>.
		/// </summary>
		/// <param name="context">The HttpContext object to rewrite the URL to.</param>
		/// <param name="newUrl">The URL to rewrite to.</param>
		/// <param name="newUrlLessQString">Returns the value of newUrl stripped of the querystring.</param>
		/// <param name="filePath">Returns the physical file path to the requested page.</param>
		public static void RewriteUrl( HttpContext context, string newUrl, out string newUrlLessQString, out string filePath ) {
			// See if we need to add any extra querystring information
			if ( context.Request.QueryString.Count > 0 ) {
				newUrl += ( newUrl.IndexOf('?') >= 0 ? "&" : "?" ) + context.Request.QueryString.ToString();
			}
			// First strip the querystring, if any
			string queryString = String.Empty;
			newUrlLessQString = newUrl;
			if ( newUrl.IndexOf( '?' ) > 0 ) {
				newUrlLessQString = newUrl.Substring( 0, newUrl.IndexOf( '?' ) );
				queryString = newUrl.Substring( newUrl.IndexOf( '?' ) + 1 );
			}
			// Get the file's physical path
			filePath = string.Empty;
			filePath = context.Server.MapPath( newUrlLessQString );
			// Send the new URL to the HttpContext
			context.RewritePath( newUrlLessQString, String.Empty, queryString );
		}
		
		/// <summary>
		/// Converts a URL into one that is usable on the requesting client.
		/// </summary>
		/// <remarks>Converts the "~" prefix to the requesting application path. 
		/// Use this when the <b>Control.ResolveUrl()</b> method isn't available.</remarks>
		/// <param name="appPath">The application path.</param>
		/// <param name="url">The URL (which might contain ~).</param>
		/// <returns>
		/// A resolved URL. If the input parameter <b>url</b> contains ~, it is replaced with the
		/// value of the <b>appPath</b> parameter.
		/// </returns>
		public static string ResolveUrl( string appPath, string url ) {
			if ( url.Length == 0 || url[0] != '~' ) {
				return ( url );		// there is no ~ in the first character position, just return the url
			} else {
				if ( url.Length == 1 ) {
					return ( appPath );  // there is just the ~ in the URL, return the appPath
				}
				if ( url[1] == '/' || url[1] == '\\' ) {
					// url looks like ~/ or ~\
					if ( appPath.Length > 1 ) {
						return ( appPath + "/" + url.Substring( 2 ) );
					} else {
						return ( "/" + url.Substring( 2 ) );
					}
				} else {
					// url looks like ~something
					if ( appPath.Length > 1 ) {
						return ( appPath + "/" + url.Substring( 1 ) );
					} else {
						return ( appPath + url.Substring( 1 ) );
					}
				}
			}
		}

	}
	
}
