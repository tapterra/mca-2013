using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Collections.ObjectModel;

namespace StarrTech.WebTools.CMS {

	public class SectionProperty {

		#region Properties

		public string Key {
			get { return ( this.key ); }
			set { this.key = value; }
		}
		private string key = "";

		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label = "";

		public SectionPropertyTypes Type {
			get { return ( this.type ); }
			set { this.type = value; }
		}
		private SectionPropertyTypes type;

		public SectionPropertyOptionsList Options {
			get { return ( this.options ); }
		}
		private SectionPropertyOptionsList options = new SectionPropertyOptionsList();

		#endregion

		#region Constructors

		public SectionProperty( XmlNode node ) {
			XmlAttribute attr;
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "key" );
			if ( attr != null ) {
				this.Key = attr.Value;
			}
			attr = (XmlAttribute) node.Attributes.GetNamedItem( "label" );
			if ( attr != null ) {
				this.Label = attr.Value;
			}
			XmlNodeList optNodes = node.SelectNodes( "options/option" );
			this.Options.Clear();
			foreach ( XmlNode optionNode in optNodes ) {
				string key = "";
				string text = "";
				string value = "";
				attr = (XmlAttribute) optionNode.Attributes.GetNamedItem( "key" );
				if ( attr != null ) {
					key = attr.Value;
				}
				attr = (XmlAttribute) optionNode.Attributes.GetNamedItem( "text" );
				if ( attr != null ) {
					text = attr.Value;
				}
				attr = (XmlAttribute) optionNode.Attributes.GetNamedItem( "value" );
				if ( attr != null ) {
					value = attr.Value;
				}
				this.Options.Add( new SectionPropertyOption( key, text, value ) );
			}
		}

		#endregion

	}

	public class SectionPropertiesList : KeyedCollection<string, SectionProperty> {
		
		public SectionPropertiesList() : base() { }
		
		protected override string GetKeyForItem( SectionProperty item ) {
			return item.Key;
		}

	}

	public enum SectionPropertyTypes {
		list,
		checkbox,
		textbox
	}

	public class SectionPropertyOption {
		
		public string Key {
			get { return ( this.key ); }
			set { this.key = value; }
		}
		
		private string key = "";

		public string Text {
			get { return ( this.text ); }
			set { this.text = value; }
		}
		private string text = "";

		public string Value {
			get { return ( this.value ); }
			set { this.value = value; }
		}
		private string value = "";

		public SectionPropertyOption( string key, string text, string value ) {
			this.Key = key;
			this.Text = text;
			this.Value = value;
		}

	}

	public class SectionPropertyOptionsList : KeyedCollection<string, SectionPropertyOption> {
		
		public SectionPropertyOptionsList() : base() { }
		
		protected override string GetKeyForItem( SectionPropertyOption item ) {
			return item.Key;
		}

	}

}
