using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Web;
	

namespace StarrTech.WebTools.Imaging {

	/// <summary>
	/// Builds graphical headlines via IHttpHandler
	/// </summary>
	public class TextImager : IHttpHandler {
		
		#region Properties 
		
		bool IHttpHandler.IsReusable {
			get { return ( false ); }
		}
				
		public int origin = 0;
		public StringFormat stringfmt = new StringFormat( StringFormatFlags.NoWrap );
		public Color tcolor = Color.Black;
		public Color bgcolor = Color.White;
		public Color bordercolor = Color.Black;
		public int bordersize = 0;
		public TextImagerBackgroundMode bgmode = TextImagerBackgroundMode.Solid;
		public int wd = 0;
		public string text = "  ";
		public string fontName = "Verdana";
		public int fontSize = 18;
		public FontStyle fontStyle = FontStyle.Regular;
				
		private Font tfont;
		private Image bmp;
		private Graphics graf;
		private Pen pen;
		private LinearGradientBrush gbrush;
		private SolidBrush tbrush;
				
		#endregion
		
		#region Methods 
		
		private void ParseQuery(HttpRequest request) {
			string q = "";
			// Get the font size        [s]
			q = request["s"];
			if ( ( q != null ) && ( q != "" ) )
				fontSize = Convert.ToInt32( q );
						
			// Get the font name        [f]
			q = request["f"];
			if ( ( q != null ) && ( q != "" ) )
				fontName = q;
						
			// Get the font style       [z=biu]
			q = request["z"];
			if ( ( q != null ) && ( q != "" ) ) {
				if ( q.IndexOf( "b" ) >= 0 ) fontStyle |= FontStyle.Bold;
				if ( q.IndexOf( "i" ) >= 0 ) fontStyle |= FontStyle.Italic;
				if ( q.IndexOf( "u" ) >= 0 ) fontStyle |= FontStyle.Underline;
			}
						
			// Get the alignment        [a=l|c|r]
			q = request["a"];
			if ( ( q != null ) && ( q != "" ) )
				switch ( q.ToUpper() ) {
					case ( "C" ):
						stringfmt.Alignment = StringAlignment.Center;
						break;
					case ( "R" ):
						stringfmt.Alignment = StringAlignment.Far;
						origin = -3;
						break;
					default:
						stringfmt.Alignment = StringAlignment.Near;
						break;
				}
							
						
			// Get the text color       [c]
			q = request["c"];
			if ( ( q != null ) && ( q != "" ) )
				tcolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
						
			// Get the background color [g]
			q = request["g"];
			if ( ( q != null ) && ( q != "" ) )
				bgcolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
						
			// Get the border color     [b]
			q = request["b"];
			if ( ( q != null ) && ( q != "" ) )
				bordercolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
						
			// Get the background mode  [m=s|g|h|x|t|b|d]
			q = request["m"];
			if ( ( q != null ) && ( q != "" ) )
				switch ( q.ToUpper() ) {
					case ( "S" ):
						bgmode = TextImagerBackgroundMode.Solid;
						break;
					case ( "G" ):
						bgmode = TextImagerBackgroundMode.Gradient;
						break;
					case ( "H" ):
						bgmode = TextImagerBackgroundMode.Shadow;
						break;
					case ( "X" ):
						bgmode = TextImagerBackgroundMode.Box;
						break;
					case ( "T" ):
						bgmode = TextImagerBackgroundMode.TopRule;
						break;
					case ( "B" ):
						bgmode = TextImagerBackgroundMode.BottomRule;
						break;
					case ( "D" ):
						bgmode = TextImagerBackgroundMode.DoubleRule;
						break;
					default:
						bgmode = TextImagerBackgroundMode.NotSet;
						break;
				}
							
						
			// Get the border width     [d]
			q = request["d"];
			if ( ( q != null ) && ( q != "" ) )
				bordersize = Convert.ToInt32( q );
						
			// Get the text-box width   [w]
			q = request["w"];
			if ( ( q != null ) && ( q != "" ) )
				wd = Convert.ToInt32( q );
						
			// Get the text             [t]
			q = request["t"];
			if ( ( q != null ) && ( q != "" ) )
				text = q;
						
		}

		private void Draw(HttpResponse response) {
			try {
				// Set up the graf
				tfont = new Font( fontName, fontSize, fontStyle );
				bmp = new Bitmap( 1, 1, PixelFormat.Format32bppArgb );
				graf = Graphics.FromImage( bmp );
				text = text.Replace( "'", "�" );
				int ht = Convert.ToInt32( graf.MeasureString(text, tfont).Height ) + 2;
				int textwd = Convert.ToInt32( graf.MeasureString(text, tfont).Width ) + 2;
				wd = wd > textwd ? wd : textwd;
				bmp.Dispose();
				bmp = new Bitmap( wd, ht, PixelFormat.Format32bppRgb );
				graf = Graphics.FromImage( bmp );
							
				graf.SmoothingMode = SmoothingMode.HighQuality;
				graf.TextRenderingHint = TextRenderingHint.AntiAlias;
				graf.TextContrast = 6;
							
				// Draw the background effects 
				pen = new Pen( bordercolor, bordersize );
				int bdroff = bordersize / 2;
				switch ( bgmode ) {
					case TextImagerBackgroundMode.NotSet:
					case TextImagerBackgroundMode.Solid:
						graf.Clear( bgcolor );
						break;
					case TextImagerBackgroundMode.Gradient:
						graf.Clear( Color.White );
						Color gradStart = Color.FromArgb( 64, bgcolor.R, bgcolor.G, bgcolor.B );
						Color gradEnd = Color.White;
					switch ( stringfmt.Alignment ) {
						case ( StringAlignment.Center ):
							int halfwd = wd / 2;
							gbrush = new LinearGradientBrush( new Point(0,0), new Point(halfwd, ht + bordersize), gradEnd, gradStart );
							graf.FillRectangle( gbrush, 0, 0, halfwd, ht + bordersize );
							gbrush.Dispose();
							gbrush = new LinearGradientBrush( new Point(halfwd, 0), new Point(wd + bordersize, ht + bordersize), gradStart, gradEnd );
							graf.FillRectangle( gbrush, halfwd, 0, wd + bordersize, ht + bordersize );
							break;
						case ( StringAlignment.Far ):
							gbrush = new LinearGradientBrush( new Point(0,0), new Point(wd + bordersize, ht + bordersize), gradEnd, gradStart );
							graf.FillRectangle( gbrush, 0, 0, wd + bordersize, ht + bordersize );
							break;
						default:
							gbrush = new LinearGradientBrush( new Point(0,0), new Point(wd + bordersize, ht + bordersize), gradStart, gradEnd );
							graf.FillRectangle( gbrush, 0, 0, wd + bordersize, ht + bordersize );
							break;
					}
						break;
					case TextImagerBackgroundMode.Shadow:
						graf.Clear( Color.White );
						break;
					case TextImagerBackgroundMode.Box:
						graf.Clear( bgcolor );
						graf.DrawRectangle( pen, bdroff, bdroff, wd, ht + 4 );
						break;
					case TextImagerBackgroundMode.TopRule:
						graf.Clear( bgcolor );
						graf.DrawLine( pen, 0, bdroff, wd + bordersize, bdroff );
						break;
					case TextImagerBackgroundMode.BottomRule:
						graf.Clear( bgcolor );
						graf.DrawLine( pen, 0, ht + bdroff + 4, wd + bordersize, ht + bdroff + 4 );
						break;
					case TextImagerBackgroundMode.DoubleRule:
						graf.Clear( bgcolor );
						graf.DrawLine( pen, 0, bdroff, wd + bordersize, bdroff );
						graf.DrawLine( pen, 0, ht + bdroff + 4, wd + bordersize, ht + bdroff + 4 );
						break;
				}
				pen.Dispose();
				// Draw the string
				switch ( bgmode ) {
					case TextImagerBackgroundMode.Shadow:
						tbrush = new SolidBrush( bgcolor );
						origin = 0;
						graf.DrawString( text, tfont, tbrush, new RectangleF( origin + bordersize + 2, bordersize + 2, wd - bordersize, ht - bordersize ), stringfmt );
						break;
					case TextImagerBackgroundMode.Gradient:
						tbrush = new SolidBrush( Color.White );
						graf.DrawString( text, tfont, tbrush, new RectangleF( 4, 4, wd - bordersize, ht - bordersize ), stringfmt );
						tbrush.Dispose();
						tbrush = new SolidBrush( tcolor );
						graf.DrawString( text, tfont, tbrush, new RectangleF( 3, 3, wd - bordersize, ht - bordersize ), stringfmt );
						break;
					default:
						tbrush = new SolidBrush( tcolor );
						graf.DrawString( text, tfont, tbrush, new RectangleF( -3, 0, wd - bordersize, ht - bordersize ), stringfmt );
						break;
				}
							
				// Send the image & clean up
				response.ContentType = "image/jpeg";
				bmp.Save( response.OutputStream, ImageFormat.Jpeg );
				response.End();
			}
			catch {}
			finally {
				if ( tfont != null ) tfont.Dispose();
				if ( bmp != null ) bmp.Dispose(); 
				if ( graf != null ) graf.Dispose(); 
				if ( pen != null ) pen.Dispose(); 
				if ( gbrush != null ) gbrush.Dispose();
				if ( tbrush != null ) tbrush.Dispose();
			}
		}
		
		void IHttpHandler.ProcessRequest( HttpContext context ) {
			ParseQuery( context.Request );
			Draw( context.Response );
		}
				
		#endregion
		
	}
	
	public enum TextImagerBackgroundMode {
		NotSet,
		Solid,      // s
		Gradient,   // g
		Shadow,     // h
		Box,        // x
		TopRule,    // t
		BottomRule, // b
		DoubleRule  // d
	}

}
