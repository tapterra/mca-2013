using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Web;
	

namespace StarrTech.WebTools.Imaging {

	/// <summary>
	/// Builds graphical labeled buttons (as an IHttpHandler)
	/// </summary>
	public class ButtonImager : IHttpHandler {
		
		#region Properties
		
		/// <summary>
		/// Always returns false
		/// </summary>
		bool IHttpHandler.IsReusable {
			get { return ( false ); }
		}
		
		private StringFormat stringfmt = new StringFormat( StringFormatFlags.NoWrap );
		private Color tcolor = Color.White;
		private Color frcolor = Color.FromName("darkblue");
		private Color bgcolor = Color.White;
		private int wd = 100;
		private int ht = 26;
		private string text = "For Rent";
		private string fontName = "Tahoma";
		private int fontSize = 9;
		private FontStyle fontStyle = FontStyle.Bold;
		private int cornersz = -1;
		
		private Font tfont;
		private Bitmap bmp;
		private Graphics graf;
		private SolidBrush framebrush;
		private GraphicsPath framepath;
		private SolidBrush tbrush;
		
		#endregion
		
		#region Methods 
		
		/// <summary>
		/// Parse up the query-string to get our property values
		/// </summary>
		/// <param name="request">The current HTTP Request</param>
		protected void ParseQuery(HttpRequest request) {
			// Get the font size        [s]
			string q = request["s"];
			if ( ( q != null ) && ( q != "" ) )
				fontSize = Convert.ToInt32( q );
						
			// Get the font name        [f]
			q = request["f"];
			if ( ( q != null ) && ( q != "" ) )
				fontName = q;
						
			// Get the font style       [z=biu]
			q = request["z"];
			if ( ( q != null ) && ( q != "" ) ) {
				fontStyle = FontStyle.Regular;
				if ( q.IndexOf( "b" ) >= 0 ) fontStyle |= FontStyle.Bold;
				if ( q.IndexOf( "i" ) >= 0 ) fontStyle |= FontStyle.Italic;
				if ( q.IndexOf( "u" ) >= 0 ) fontStyle |= FontStyle.Underline;
			}
						
			// Get the text color       [c]
			q = request["c"];
			if ( ( q != null ) && ( q != "" ) ) {
				tcolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
			}
						
			// Get the frame color      [g]
			q = request["g"];
			if ( ( q != null ) && ( q != "" ) ) {
				frcolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
			}
						
			// Get the background color [b]
			q = request["b"];
			if ( ( q != null ) && ( q != "" ) ) {
				bgcolor = (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( q );
			}
						
			// Get the button width     [w]
			q = request["w"];
			if ( ( q != null ) && ( q != "" ) )
				wd = Convert.ToInt32( q );
						
			// Get the button height    [h]
			q = request["h"];
			if ( ( q != null ) && ( q != "" ) ) ht = Convert.ToInt32( q );
						
			// Get the text             [t]
			q = request["t"];
			if ( ( q != null ) && ( q != "" ) ) text = q;
						
		}

		/// <summary>
		/// Draw the button graphic based on current property values
		/// </summary>
		/// <param name="response">The HTTP Response we want to write to</param>
		protected void Draw(HttpResponse response) {
			try {
				// Set up the graf
				stringfmt.Alignment = StringAlignment.Center;
				tfont = new Font( fontName, fontSize, fontStyle );
				bmp = new Bitmap( wd, ht, PixelFormat.Format24bppRgb );
				graf = Graphics.FromImage( bmp );
				graf.TextRenderingHint = TextRenderingHint.AntiAlias;
				graf.SmoothingMode = SmoothingMode.HighQuality;
				// Draw the background
				graf.Clear( bgcolor );
				framebrush = new SolidBrush( frcolor );
				framepath = new GraphicsPath();
				if ( cornersz <= 0 ) cornersz = ht/2;
				framepath.AddArc( new Rectangle( 0, 0, cornersz, cornersz), 180, 90 );
				framepath.AddArc( new Rectangle( wd - cornersz - 1, 0, cornersz, cornersz ), -90, 90 );
				framepath.AddArc( new Rectangle( wd - cornersz - 1, ht - cornersz - 1, cornersz, cornersz ), 0, 90 );
				framepath.AddArc( new Rectangle( 0, ht - cornersz - 1, cornersz, cornersz), 90, 90 );
				framepath.CloseAllFigures();
				graf.FillPath( framebrush, framepath );
							
				// Draw the string
				tbrush = new SolidBrush( tcolor );
				text = text.Replace( "'", "�" );
				int hoffset = 0;
				int voffset = ( ht / 2 ) - fontSize + 2;
				graf.DrawString( text, tfont, tbrush, new RectangleF( hoffset, voffset, wd - hoffset, ht - voffset ), stringfmt );
							
				// Send the image & clean up
				response.ContentType = "image/gif";
				bmp.Save( response.OutputStream, ImageFormat.Gif );
				response.End();
							
			} catch {
			} finally {
				if ( tfont != null ) tfont.Dispose();
				if ( bmp != null ) bmp.Dispose(); 
				if ( graf != null ) graf.Dispose(); 
				if ( framebrush != null ) framebrush.Dispose();
				if ( framepath != null ) framepath.Dispose();
				if ( tbrush != null ) tbrush.Dispose();
			}
		}
				
		/// <summary>
		/// Process the request - parse the query string & draw the image
		/// </summary>
		/// <param name="context">The current HTTP Context</param>
		void IHttpHandler.ProcessRequest( HttpContext context ) {
			ParseQuery( context.Request );
			Draw( context.Response );
		}
				
		#endregion
		
	}
	
}
