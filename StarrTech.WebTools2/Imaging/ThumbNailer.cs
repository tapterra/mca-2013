using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Web;


namespace StarrTech.WebTools.Imaging {

	/// <summary>
	/// HttpHandler that allows image requests to be scaled to a factor of 1 or a max width/height
	/// </summary>
	public class ThumbNailer : IHttpHandler {
		
		#region Properties 
		
		bool IHttpHandler.IsReusable {
			get { return ( false ); }
		}

		private string src = "";
		private double scale = 1.0;
		private int maxwidth = 0;
		private int maxheight = 0;
		
		#endregion
		
		#region Methods 
		
		protected void ParseQuery(HttpRequest request) {
			string q = "";
			// Get the image source     [src]
			q = request["src"];
			if ( ( q != null ) && ( q != "" ) ) {
				this.src = q;
			}
			
			// Get the scaling factor   [x]
			q = request["x"];
			if ( ( q != null ) && ( q != "" ) ) {
				this.scale = Convert.ToDouble( q );
			}

			// Get the maximum-width		[w]
			q = request["w"];
			if ( ( q != null ) && ( q != "" ) ) {
				this.maxwidth = Convert.ToInt32( q );
			}
			
			// Get the maximum-height		[h]
			q = request["h"];
			if ( ( q != null ) && ( q != "" ) ) {
				this.maxheight = Convert.ToInt32( q );
			}
			
		}
		
		public void Draw( HttpResponse response ) {
			System.Drawing.Image pic = null;
			Bitmap bmp = null;
			Graphics graf = null;
			try {
				// Load the image
				pic = System.Drawing.Image.FromFile( HttpContext.Current.Server.MapPath( src ) );
				
				// Compute the new dimensions
				int ht = Convert.ToInt32( pic.Height * this.scale );
				int wd = Convert.ToInt32( pic.Width * this.scale );
				if ( this.maxwidth > 0 ) {
					if ( wd > this.maxwidth ) {
						double wscale = Convert.ToDouble(this.maxwidth) / Convert.ToDouble(wd);
						wd = Convert.ToInt32( wd * wscale );
						ht = Convert.ToInt32( ht * wscale );
					}
				}
				if ( this.maxheight > 0 ) {
					if ( ht > this.maxheight ) {
						double hscale = Convert.ToDouble(this.maxheight) / Convert.ToDouble(ht);
						wd = Convert.ToInt32( wd * hscale );
						ht = Convert.ToInt32( ht * hscale );
					}
				}

				// Set up the graf
				bmp = new Bitmap( wd, ht, PixelFormat.Format24bppRgb );
				graf = Graphics.FromImage( bmp );
				graf.Clear( Color.White );
							
				// Draw the thumbnail
				Point[] destpts = { new Point(0,0), new Point(wd,0), new Point(0,ht) };
				graf.DrawImage( pic, destpts );
							
				// Send the image & clean up
				response.ContentType = "image/jpeg";
				bmp.Save( response.OutputStream, ImageFormat.Jpeg );
				response.End();
			} catch {
			} finally {
				if ( pic != null ) pic.Dispose(); 
				if ( bmp != null ) bmp.Dispose(); 
				if ( graf != null ) graf.Dispose(); 
			}
		}

		void IHttpHandler.ProcessRequest( HttpContext context ) {
			this.ParseQuery( context.Request );
			this.Draw( context.Response );
		}
				
		#endregion

	}
		
}
