using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using System.Web;
using System.Web.Caching;

using StarrTech.WebTools;
using StarrTech.WebTools.CMS;

namespace StarrTech.WebTools.Panels {

	/// <summary>
	/// Generates a variety of DHTML menus from XML.
	/// </summary>
	[ParseChildrenAttribute(ChildrenAsProperties = false)]
	public class MenuPanel : WebControl {
		
		#region Properties 

		protected override HtmlTextWriterTag TagKey {
			get { return ( HtmlTextWriterTag.Div ); }
		}
		
		public ArrayList Items {
			get { return this.items; }
		}
		private ArrayList items = new ArrayList();
		
		#region Style 
		
		public MenuOrientation MainMenuOrientation {
			get { 
				object obj = ViewState["MainMenuOrientation"];
				return ( obj == null ? MenuOrientation.Horizontal : (MenuOrientation) obj ); 
			}
			set { ViewState["MainMenuOrientation"] = value; }
		}
					
		public MenuOrientation SubMenuOrientation {
			get { 
				object obj = ViewState["SubMenuOrientation"];
				return ( obj == null ? MenuOrientation.Vertical : (MenuOrientation) obj ); 
			}
			set { ViewState["SubMenuOrientation"] = value; }
		}
					
		public TableStyle MainMenuStyle {
			get { 
				object obj = ViewState["MainMenuStyle"];
				if ( obj == null ) {
					TableStyle ts = new TableStyle();
					ts.CellSpacing = 0;
					ts.CellPadding = 3;
					obj = ts;
				}
				return ( (TableStyle) obj ); 
			}
		}
					
		public TableStyle SubMenuStyle {
			get { 
				object obj = ViewState["SubMenuStyle"];
				if ( obj == null ) {
					TableStyle ts = new TableStyle();
					ts.CellSpacing = 0;
					ts.CellPadding = 3;
					obj = ts;
				}
				return ( (TableStyle) obj ); 
			}
		}
					
		public string MainMenuCssClass {
			get { 
				object obj = ViewState["MainMenuCssClass"];
				return ( obj == null ? "mainmenu" : (string) obj ); 
			}
			set { ViewState["MainMenuCssClass"] = value; }
		}
					
		public string MainMenuOverCssClass {
			get { 
				object obj = ViewState["MainMenuOverCssClass"];
				return ( obj == null ? "mainmenuover" : (string) obj ); 
			}
			set { ViewState["MainMenuOverCssClass"] = value; }
		}
					
		public string MainMenuDownCssClass {
			get { 
				object obj = ViewState["MainMenuDownCssClass"];
				return ( obj == null ? "mainmenudown" : (string) obj ); 
			}
			set { ViewState["MainMenuDownCssClass"] = value; }
		}
					
		public string SubMenuCssClass {
			get { 
				object obj = ViewState["SubMenuCssClass"];
				return ( obj == null ? "submenu" : (string) obj ); 
			}
			set { ViewState["SubMenuCssClass"] = value; }
		}
					
		public string SubMenuOverCssClass {
			get { 
				object obj = ViewState["SubMenuOverCssClass"];
				return ( obj == null ? "submenuover" : (string) obj ); 
			}
			set { ViewState["SubMenuOverCssClass"] = value; }
		}
					
		public string SubMenuDownCssClass {
			get { 
				object obj = ViewState["SubMenuDownCssClass"];
				return ( obj == null ? "submenudown" : (string) obj ); 
			}
			set { ViewState["SubMenuDownCssClass"] = value; }
		}
					
		public HorizontalAlign HorizontalAlign {
			get { 
				object obj = ViewState["HorizontalAlign"];
				return ( obj == null ? HorizontalAlign.Left : (HorizontalAlign) obj ); 
			}
			set { ViewState["HorizontalAlign"] = value; }
		}
					
		public override ControlCollection Controls {
			get {
				EnsureChildControls();
				return ( base.Controls );
			}
		}
				
		#endregion
		
		#endregion

		#region Events
		
		public event MenuAddEventHandler MenuAdd {
			add { Events.AddHandler( MenuAddEventKey, value ); }
			remove { Events.RemoveHandler( MenuAddEventKey, value ); }
		}
		public virtual void OnMenuAdd( Object sender, MenuAddEventArgs args ) {
			MenuAddEventHandler handler = (MenuAddEventHandler) Events[MenuAddEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object MenuAddEventKey = new object();

		protected bool ValidateMenuItem( MenuPanelItem mItem ) {
			MenuAddEventArgs args = new MenuAddEventArgs( mItem );
			this.OnMenuAdd( this, args );
			return ( args.IsValid );
		}

		#endregion
		
		#region Methods 
		
		#region Schema-specific
		
		#region Standard xml-schema constants

		private const string kRootNodeTagName = "menus";
		private const string kTopItemTagName = "menuitem";
		private const string kSubItemTagName = "menuitem";
		
		private const string kIDAttribName = "id";
		private const string kLabelAttribName = "label";
		private const string kUrlAttribName = "url";
		private const string kKeyAttribName = "permission";
		
		#endregion
		
		public void ParseXmlMenu( XmlDocument xdoc ) {
			this.ParseXmlMenu( xdoc.DocumentElement, kRootNodeTagName, kTopItemTagName, kSubItemTagName, 
				kIDAttribName, kLabelAttribName, kUrlAttribName, kKeyAttribName );
		}
		
		public void ParseXmlMenu( XmlDocument xdoc, string rootTag, string topTag, string subTag, 
			string idAttrib, string labelAttrib, string urlAttrib, string keyAttrib ) {
			this.ParseXmlMenu( xdoc.DocumentElement, rootTag, topTag, subTag, idAttrib, labelAttrib, urlAttrib, keyAttrib );
		}

		public void ParseXmlMenu( XmlNode xdoc ) {
			this.ParseXmlMenu( xdoc, kRootNodeTagName, kTopItemTagName, kSubItemTagName, 
				kIDAttribName, kLabelAttribName, kUrlAttribName, kKeyAttribName );
		}
		
		public void ParseXmlMenu( XmlNode xdoc, string rootTag, string topTag, string subTag, 
			string idAttrib, string labelAttrib, string urlAttrib, string keyAttrib ) {
			// Build the menubar...
			XmlNodeList topItemNodes = null;
			if ( xdoc != null ) {
				// Get the top-level nodes 
				topItemNodes = xdoc.SelectNodes( string.Format( "/{0}/{1}", rootTag, topTag ) );
				foreach ( XmlNode topItemNode in topItemNodes ) {
					MenuPanelItem topItem = this.ParseItemNode( topItemNode, idAttrib, labelAttrib, urlAttrib, keyAttrib );
					if ( this.ValidateMenuItem( topItem ) ) {
						topItem = this.AddItem( topItem );
					}
					// Get the sub-item nodes
					XmlNodeList subItemNodes = topItemNode.SelectNodes( subTag );
					foreach ( XmlNode subItemNode in subItemNodes ) {
						MenuPanelItem subItem = this.ParseItemNode( subItemNode, idAttrib, labelAttrib, urlAttrib, keyAttrib );
						if ( this.ValidateMenuItem( subItem ) ) {
							topItem.AddItem( subItem );
						}
					}
				}
			}
		}
		
		protected MenuPanelItem ParseItemNode( XmlNode itemNode, string idAttrib, string labelAttrib, string urlAttrib, string keyAttrib ) {
			XmlAttributeCollection attribs = itemNode.Attributes;
			XmlAttribute attrib;
			// Get the "id" attribute
			attrib = (XmlAttribute) attribs.GetNamedItem( idAttrib );
			string id = ( attrib == null ? "" : attrib.Value );
			// Get the "label" attribute
			attrib = (XmlAttribute) attribs.GetNamedItem( labelAttrib );
			string label = ( attrib == null ? "" : attrib.Value );
			// Get the "url" attribute
			attrib = (XmlAttribute) attribs.GetNamedItem( urlAttrib );
			string url = ( attrib == null ? "" : attrib.Value );
			// Get the "permission" attribute
			attrib = (XmlAttribute) attribs.GetNamedItem( keyAttrib );
			string key = ( attrib == null ? "" : attrib.Value );
			return ( new MenuPanelItem( id, label, url, key ) );
		}

		public void ParseCmsMenu( string SiteTreeKey ) {
			XmlNode root = ContentPublisher.SiteTreeXmlDoc( SiteTreeKey ).DocumentElement;
			if ( root != null ) {
				foreach ( XmlNode node in root ) {
					if ( node.NodeType == XmlNodeType.Element ) {
						XmlElement elem = node as XmlElement;
						if ( elem.Name != "treenode" ) continue;
						XmlAttributeCollection attribs = elem.Attributes;
						bool isVisible = Convert.ToBoolean( ((XmlAttribute) attribs.GetNamedItem("isvisible")).Value );
						if ( ! isVisible ) continue;
						MenuPanelItem topItem = this.ParseCmsItemNode( SiteTreeKey, elem, 0 );
						if ( this.ValidateMenuItem( topItem ) ) {
							topItem = this.AddItem( topItem );
						}
					}
				}
			}
		}
		
		public void ParseCmsSectionMenu( string SectionMenuTitle, string SiteTreeKey ) {
			XmlNode root = ContentPublisher.SiteTreeXmlDoc( SiteTreeKey ).DocumentElement;
			if ( root != null ) {
				MenuPanelItem topItem = null;
				foreach ( XmlNode node in root ) {
					if ( node.NodeType == XmlNodeType.Element ) {
						XmlElement elem = node as XmlElement;
						if ( elem.Name != "treenode" ) continue;
						XmlAttributeCollection attribs = elem.Attributes;
						bool isVisible = Convert.ToBoolean( ((XmlAttribute) attribs.GetNamedItem("isvisible")).Value );
						if ( ! isVisible ) continue;
						MenuPanelItem subItem = this.ParseCmsItemNode( SiteTreeKey, elem, 0 );
						if ( topItem == null ) {
							topItem = new MenuPanelItem( SiteTreeKey, SectionMenuTitle, subItem.Url, subItem.PermissionKey );
						}
						if ( this.ValidateMenuItem( subItem ) ) {
							subItem = topItem.AddItem( subItem );
						}
					}
				}
				if ( this.ValidateMenuItem( topItem ) ) {
					topItem = this.AddItem( topItem );
				}
			}
		}
		
		protected MenuPanelItem ParseCmsItemNode( string siteTreeKey, XmlElement menuElem, int level ) {
			// Build the main menu row
			XmlAttributeCollection attribs = menuElem.Attributes;
			string id = ((XmlAttribute) attribs.GetNamedItem("id")).Value;
			string key = ((XmlAttribute) attribs.GetNamedItem("key")).Value;
			string title = ((XmlAttribute) attribs.GetNamedItem("title")).Value;
			string typeKey = ((XmlAttribute) attribs.GetNamedItem("typekey")).Value;
			string publishedAt = ((XmlAttribute) attribs.GetNamedItem("publishedat")).Value;
			string contentItemID = ((XmlAttribute) attribs.GetNamedItem("contentitemid")).Value;
			string menuid = key + id;
			string label = title;
			string url;
			ContentTypeEditors editorType;
			ContentType thisType = ContentTypeManager.ContentTypes[typeKey];
			if ( thisType != null ) {
				editorType = thisType.EditorType;
			} else { 
				editorType = ContentTypeEditors.contentform;
			}
			if ( editorType != ContentTypeEditors.contentform || publishedAt != "~/content.aspx" ) {
				url = publishedAt.Replace( "~", "~/" + siteTreeKey );
			} else {
				// Build the node's "path" from it's key values...
				string path = "";
				XmlNode node = menuElem;
				while ( node.NodeType == XmlNodeType.Element ) {
					if ( typeKey == "homepage" ) break;
					attribs = node.Attributes;
					string pathkey = ((XmlAttribute) attribs.GetNamedItem("key")).Value;
					path = "/" + pathkey + path;
					if ( pathkey == siteTreeKey ) break;
					node = node.ParentNode;
				}
				url = string.Format( "~{0}/default.aspx", path );
			}
			XmlAttribute permAttrib = (XmlAttribute) attribs.GetNamedItem( "permission" );
			string keyString = ( permAttrib == null ? "None" : permAttrib.Value );
			MenuPanelItem mitem = new MenuPanelItem( id, label, url, keyString );
			if ( menuElem.HasChildNodes ) {
				foreach ( XmlNode item in menuElem ) {
					if ( item.NodeType == XmlNodeType.Element ) {
						XmlElement elem = item as XmlElement;
						if ( elem.Name != "treenode" ) continue;
						bool isVisible = Convert.ToBoolean( ((XmlAttribute) elem.Attributes.GetNamedItem("isvisible")).Value );
						if ( ! isVisible ) continue;
						MenuPanelItem subItem = this.ParseCmsItemNode( siteTreeKey, elem, level + 1 );
						if ( this.ValidateMenuItem( subItem ) ) {
							mitem.AddItem( subItem );
						}
					}
				}
			}
			return ( mitem );
		}
		
		#endregion
		
		protected MenuPanelItem AddItem( MenuPanelItem itm ) {
			this.Items.Add( itm );
			return ( itm );
		}

		protected MenuPanelItem AddItem( string id, string label, string url, string key ) {
			MenuPanelItem itm = new MenuPanelItem( id, label, url, key );
			return ( this.AddItem( itm ) );
		}
		
		protected TableCell CreateTopItemCell( MenuPanelItem itm ) {
			// Build the menu-item cell
			TableCell cell = new TableCell();
			cell.Text = itm.Label;
			cell.ID = ID + "_" + itm.ID;
			cell.Wrap = false;
			cell.HorizontalAlign = HorizontalAlign;
			if ( MainMenuCssClass != "" ) cell.CssClass = MainMenuCssClass;
			cell.Attributes["onmouseover"] = String.Format( "javascript:__{0}.topCellMouseOver(this);", ID );
			cell.Attributes["onmousedown"] = String.Format( "javascript:__{0}.topCellMouseDown(this, '{1}');", ID, ResolveUrl( itm.Url ) );
			cell.Attributes["onmouseout"] = String.Format( "javascript:__{0}.topCellMouseOut(this);", ID );
			// Build the sub-level menu table
			if ( itm.SubItems.Count > 0 ) {
				Table subTable = CreateSubMenuTable();
				subTable.ID = ID + "_" + itm.ID + "_submenu";
				TableRow subRow;
				switch ( SubMenuOrientation ) {
					case MenuOrientation.NotSet:
					case MenuOrientation.Vertical:
						foreach ( MenuPanelItem subItem in itm.SubItems ) {
							subRow = new TableRow();
							subRow.Cells.Add( CreateSubItemCell( itm.ID, subItem ) );
							subTable.Rows.Add( subRow );
						}
						break;
					case MenuOrientation.Horizontal:
						subRow = new TableRow();
						foreach ( MenuPanelItem subItem in itm.SubItems ) {
							subRow.Cells.Add( CreateSubItemCell( itm.ID, subItem ) );
						}
						subTable.Rows.Add( subRow );
						break;
				}
				Controls.Add( subTable );
			}
			return ( cell );
		}
		
		protected TableCell CreateSubItemCell( string topid, MenuPanelItem itm ) {
			// Build the menu-item cell
			TableCell cell = new TableCell();
			cell.Text = itm.Label;
			cell.Wrap = false;
			cell.HorizontalAlign = HorizontalAlign;
			cell.ID = this.ID + "_" + topid + "_" + itm.ID;
			if ( SubMenuCssClass != "" ) cell.CssClass = SubMenuCssClass;
			cell.Attributes["onmouseover"] = String.Format( "javascript:__{0}.subCellMouseOver(this);", this.ID );
			cell.Attributes["onmousedown"] = String.Format( "javascript:__{0}.subCellMouseDown(this, '{1}');", this.ID, ResolveUrl( itm.Url ) );
			cell.Attributes["onmouseout"] = String.Format( "javascript:__{0}.subCellMouseOut(this);", this.ID );
			return ( cell );
		}
		
		protected Table CreateMainMenuTable() {
			Table topTable = new Table();
			topTable.Style["border-collapse"] = "separate";
			topTable.HorizontalAlign = HorizontalAlign;
			topTable.ControlStyle.MergeWith( MainMenuStyle );
			return ( topTable );
		}
		
		protected Table CreateSubMenuTable() {
			Table subTable = new Table();
			subTable.ControlStyle.CopyFrom( SubMenuStyle );
			subTable.Attributes["onmouseover"] = String.Format( "javascript:__{0}.subTableMouseOver(this);", ID );
			subTable.Attributes["onmouseout"] = String.Format( "javascript:__{0}.subTableMouseOut(this);", ID );
			subTable.Style["border-collapse"] = "separate";
			subTable.Style["visibility"] = "hidden";
			subTable.Style["position"] = "absolute";
			subTable.Style["top"] = "0";
			subTable.Style["left"] = "0";
			return ( subTable );
		}
		
		protected void BuildMenus( Table topTable ) {
			TableRow topRow;
			switch ( this.MainMenuOrientation ) {
				case MenuOrientation.NotSet:
				case MenuOrientation.Horizontal:
					topRow = new TableRow();
					foreach ( MenuPanelItem topItem in this.Items ) {
						topRow.Cells.Add( CreateTopItemCell( topItem ) );
					}
					topTable.Rows.Add( topRow );
					break;
				case MenuOrientation.Vertical:
					foreach ( MenuPanelItem topItem in this.Items ) {
						topRow = new TableRow();
						topRow.Cells.Add( CreateTopItemCell( topItem ) );
						topTable.Rows.Add( topRow );
					}
					topTable.Width = new Unit( "100%" );
					break;
			}
		}
		
		protected override void CreateChildControls() {
			Style["position"] = "relative";
			Attributes["horizontalAlign"] = this.HorizontalAlign.ToString().ToLower();
			Attributes["mainMenuOrientation"] = this.MainMenuOrientation.ToString().ToLower();
			Attributes["subMenuOrientation"] = this.SubMenuOrientation.ToString().ToLower();
			if ( Width.IsEmpty ) {	// Must have a width assigned for the client-script to work
				Width = new Unit("100%");
			}
			// Build the menubar ...
			Table topTable = this.CreateMainMenuTable();
			Controls.Add( topTable );
			this.BuildMenus( topTable );
		}
		
		protected override void Render( HtmlTextWriter writer ) {
			EnsureChildControls();
			base.Render( writer );
			string path = this.Page.ClientScript.GetWebResourceUrl( this.GetType(), "StarrTech.WebTools.Resources.script.menupanel.js" );
			writer.Write( string.Format("<script language=\"javascript\" src=\"{0}\"></script>", path ) );
			string instanceScript = String.Format(
				@"<script language=""javascript"">var __{0} = new stixMenuBar('{1}','{2}','{3}','{4}','{5}');</script>",
				ID, ClientID, MainMenuCssClass, MainMenuOverCssClass, SubMenuCssClass, SubMenuOverCssClass
			);
			writer.Write( instanceScript );
		}
		
		#endregion

	}
	
	/// <summary>
	/// Defines the unique settings for orienting menu-bars and menus generated by the MenuPanel control.
	/// </summary>
	public enum MenuOrientation {
		NotSet,
		Horizontal,
		Vertical
	}
	
	/// <summary>
	/// Defines a single menu-item and it's submenu items
	/// </summary>
	public class MenuPanelItem {
		
		#region Properties
		
		public string ID {
			get { return this.id; }
			set { this.id = value; }
		}
		private string id;
		
		public string Label {
			get { return this.label; }
			set { this.label = value; }
		}
		private string label;
		
		public string Url {
			get { return this.url; }
			set { this.url = value; }
		}
		private string url;
		
		public ArrayList SubItems {
			get { return this.subItems; }
		}
		private ArrayList subItems = new ArrayList();
		
		public string PermissionKey {
			get { return this.permissionKey; }
			set { this.permissionKey = value; }
		}
		private string permissionKey;
		
		#endregion

		public MenuPanelItem( string id, string label, string url, string key ) {
			this.ID = id;
			this.Label = label;
			this.Url = url;
			this.PermissionKey = key;
		}
		
		public MenuPanelItem AddItem( MenuPanelItem itm ) {
			this.SubItems.Add( itm );
			return ( itm );
		}
		public MenuPanelItem AddItem( string id, string label, string url, string key ) {
			MenuPanelItem itm = new MenuPanelItem( id, label, url, key );
			return ( this.AddItem( itm ) );
		}
	}
	
	public delegate void MenuAddEventHandler(object sender, MenuAddEventArgs e);
	
	public class MenuAddEventArgs : System.EventArgs {
		
		public MenuPanelItem MenuItem {
			get { return ( this.menuItem ); }
			set { this.menuItem = value; }
		}
		private MenuPanelItem menuItem;
		
		public bool IsValid {
			get { return ( this.isValid ); }
			set { this.isValid = value; }
		}
		private bool isValid = true;
		
		public MenuAddEventArgs( MenuPanelItem item ) {
			this.menuItem = item;
		}
		
	}
	
}
