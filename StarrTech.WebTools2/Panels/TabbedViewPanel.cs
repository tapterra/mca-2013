using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using StarrTech.WebTools;
using StarrTech.WebTools.Controls;

namespace StarrTech.WebTools.Panels {

	public class TabbedViewPanel : Panel {

		private MultiView multiView = new MultiView();

		public ViewCollection Views {
			get { return ( this.multiView.Views ); }
		}

		private TabStrip tabs = new TabStrip();

		public string TabLabels {
			set { this.tabs.TabLabels = value; }
		}

		public int ActiveViewIndex {
			get { return ( this.multiView.ActiveViewIndex ); }
			set { this.multiView.ActiveViewIndex = value; }
		}

	}

}
