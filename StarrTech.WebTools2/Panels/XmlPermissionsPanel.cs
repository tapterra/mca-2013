using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Caching;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Xsl;


namespace StarrTech.WebTools.Panels {
	/// <summary>
	/// Generates a UI editor for assigning permissions to a group/user record.
	/// Permissions are defined in an XML file.
	/// </summary>
	public class XmlPermissionsPanel : Panel, INamingContainer {
		
		#region Properties 
		
		public string XmlFile {
			get {
				if ( string.IsNullOrEmpty( xmlfile ) ) {
					xmlfile = SitePrefs.CheckPref( "permissionsfile", "~/App_Data/Permissions.config" );
				}
				return ( HttpContext.Current.Server.MapPath( xmlfile ) ); 
			}
			set { xmlfile = value; }
		}
		private string xmlfile = "";
		
		public XmlDocument XmlDoc {
			get { 
				if ( xmldoc != null ) {
					// We have an assigned XML doc - return it
					return ( xmldoc );
				} else {
					// Use the cached XmlDocument created from XmlFile
					XmlDocument doc;
					string path = XmlFile;
					if ( HttpContext.Current.Cache[path] == null ) {
						// Need to (re)load the cache
						doc = new XmlDocument();
						doc.Load( path ); 
						HttpContext.Current.Cache.Insert( path, doc, new CacheDependency( path ) );
					} else {
						doc = (XmlDocument) HttpContext.Current.Cache[path];
					}
					return ( doc );
				}
			}
			set { xmldoc = value; }
		}
		private XmlDocument xmldoc = null;
		
		public int RepeatColumns {
			get { 
				object cols = ViewState["RepeatColumns"]; 
				return ( cols == null ? 4 : (int) cols );
			}
			set { ViewState["RepeatColumns"] = value; }
		}
		
		public virtual ArrayList SelectedValues {
			get {
				ArrayList values = new ArrayList();
				foreach ( Control ctrl in this.Controls ) 
					if ( ctrl is CheckBoxList ) {
						ListItemCollection items = ((CheckBoxList) ctrl).Items;
						foreach ( ListItem xbox in items ) {
							if ( xbox.Selected ) {
								values.Add( xbox.Value );
							}
						}
					}
				return ( values );
			}
			set {
				foreach ( Control ctrl in this.Controls ) {
					if ( ctrl is CheckBoxList ) {
						ListItemCollection items = ((CheckBoxList) ctrl).Items;
						foreach ( ListItem xbox in items ) {
							xbox.Selected = value.Contains( xbox.Value );
						}
					}
				}
			}
		}
		
		public override ControlCollection Controls {
			get {
				EnsureChildControls();
				return ( base.Controls );
			}
		}
				
		#endregion
		
		#region Composition Methods
		
		protected override void CreateChildControls() {               
			// Create a new ControlCollection. 
			this.CreateControlCollection();
			
			// Create child controls.
			this.BuildPermissionsPanel();
			
			// Prevent child controls from being created again.
			this.ChildControlsCreated = true;
		}
		
		protected void BuildInstructionsPanel( XmlNode item, string keyPrefix, int IndentLevel ) {
			// Build the Panel
			Panel instrPanel = new Panel();
			instrPanel.Width = this.Width;
			instrPanel.Style["padding"] = "10";
			instrPanel.Style["padding-left"] = (10 * IndentLevel).ToString();
			string itext = item.InnerText;
			Label lbl = new Label();
			lbl.Text = itext;
			lbl.Font.Name = "Verdana";
			lbl.Font.Size = new FontUnit("9pt");
			lbl.Font.Bold = false;
			instrPanel.Controls.Add( lbl );
			this.Controls.Add( instrPanel );
		}
		
		protected void BuildFunctionsPanel( XmlNode item, string keyPrefix, int IndentLevel ) {
			// Build the Panel
			IEnumerator itemEnum = item.GetEnumerator();
			CheckBoxList xboxes = new CheckBoxList();
			xboxes.CssClass = "xboxTable";
			xboxes.Width = this.Width;
			xboxes.Style["margin-left"] = (10 * IndentLevel).ToString();
			xboxes.ID = keyPrefix;
			xboxes.CellSpacing = 1;
			xboxes.TextAlign = TextAlign.Right;
			xboxes.RepeatDirection = RepeatDirection.Horizontal;
			xboxes.RepeatLayout = RepeatLayout.Table;
			xboxes.RepeatColumns = this.RepeatColumns;
			xboxes.Font.Bold = false;
			xboxes.Style["font-weight"] = "normal";
			xboxes.Enabled = this.Enabled;
			while ( itemEnum.MoveNext() ) {
				XmlNode subitem = (XmlNode) itemEnum.Current;
				string sublabel = ((XmlAttribute) subitem.Attributes.GetNamedItem("label")).Value;
				string subkey = ((XmlAttribute) subitem.Attributes.GetNamedItem("key")).Value;
				ListItem fxbox = new ListItem( sublabel, keyPrefix + subkey );
				xboxes.Items.Add( fxbox );
			}
			this.Controls.Add( xboxes );
		}
		
		protected void BuildCategoryPanel( XmlNode item, string keyPrefix, int IndentLevel ) {
			// "item" is an XmlNode representing a single category element.
			// Get the item's attributes (ID, label and key)
			XmlAttributeCollection attribs = item.Attributes;
			if ( attribs == null )
				return;
			string label = ((XmlAttribute) attribs.GetNamedItem("label")).Value;
			string key = ((XmlAttribute) attribs.GetNamedItem("key")).Value;
			// Build the Panel
			Panel catPanel = new Panel();
			catPanel.Width = this.Width;
			catPanel.Style["padding-left"] = (10 * IndentLevel).ToString();
			catPanel.Style["padding-top"] = "10";
			if ( label != "" ) {
				Label catLabel = new Label();
				catLabel.Text = label;
				catLabel.Font.Name = "Verdana";
				catLabel.Font.Size = new FontUnit( string.Format("{0}pt", 10 - IndentLevel*2) );
				catLabel.Font.Bold = true;
				catPanel.Controls.Add( catLabel );
			}
			this.Controls.Add( catPanel );
			// Build the subordinate items
			IEnumerator itemEnum = item.GetEnumerator();
			while ( itemEnum.MoveNext() ) {
				XmlNode subitem = (XmlNode) itemEnum.Current;
				switch ( subitem.LocalName.ToLower() ) {
					case "category":
						string sublabel = ((XmlAttribute) subitem.Attributes.GetNamedItem("label")).Value;
						string subkey = ((XmlAttribute) subitem.Attributes.GetNamedItem("key")).Value;
						this.BuildCategoryPanel( subitem, keyPrefix + key, IndentLevel + 1 );
						break;
					case "functions":
						this.BuildFunctionsPanel( subitem, keyPrefix + key, IndentLevel + 1 );
						break;
					case "instructions":
						this.BuildInstructionsPanel( subitem, keyPrefix + key, IndentLevel + 1 );
						break;
				}
			}
		}
		
		protected void BuildPermissionsPanel() {
			// Pull our permission-rule definitions from the xml file
			if ( XmlDoc == null )throw new Exception( "Invalid or missing XmlFile: " + XmlFile );
			XmlNode root = XmlDoc.DocumentElement;
			// Loop over each root category element
			IEnumerator catEnum = root.GetEnumerator();
			while ( catEnum.MoveNext() ) {
				// Build the category-panel for the current element
				BuildCategoryPanel( (XmlNode) catEnum.Current, "", 0 );
			}
		}

		#endregion
		
	}
	
}
