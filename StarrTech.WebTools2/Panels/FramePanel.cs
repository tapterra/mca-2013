using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
	

namespace StarrTech.WebTools.Panels {

	// Enum FramerType - enumerated framer styles 
	public enum FramerType {
		NoTitleFrame,
		LeftTitleFrame,
		TopTitleFrame,
		RightTitleFrame,
		BottomTitleFrame
	}
			
	
	// Class: FramePanel - builds a framed page-layout panel 
	[ParseChildrenAttribute(ChildrenAsProperties = false)]
	public class FramePanel : WebControl, INamingContainer {
		
		#region Properties 

		protected override HtmlTextWriterTag TagKey {
			get { return ( HtmlTextWriterTag.Table ); }
		}

		#region XML Renderer Properties

		private StarrTech.WebTools.Controls.Xml renderer = new StarrTech.WebTools.Controls.Xml();
		public StarrTech.WebTools.Controls.Xml Renderer {
			get { return ( renderer ); }
		}

		public string DocumentContent {
			set { Renderer.DocumentContent = value; } 
		}
						
		public string DocumentSource {
			set { Renderer.DocumentSource = value; } 
		}
						
		public string TransformSource {
			set { Renderer.TransformSource = value; } 
		}
						
		public string DocumentUrl {
			set { Renderer.DocumentUrl = value; } 
		}
						
		public string TransformUrl {
			set { Renderer.TransformUrl = value; } 
		}
						
		#endregion
				
		#region Table-Level Style Properties
		
		public FramerType FrameType {
			get { 
				object obj = ViewState["FrameType"];
				return ( obj == null ? FramerType.NoTitleFrame : (FramerType) obj ); 
			}
			set { ViewState["FrameType"] = value; }
		}
		
		public virtual string BackImageUrl {
			get {
				if ( ControlStyleCreated ) {
					return ( ((TableStyle) ControlStyle).BackImageUrl );
				}
				return ( String.Empty );
			}
			set { ((TableStyle) ControlStyle).BackImageUrl = value; }
		}
		
		public virtual HorizontalAlign HorizontalAlign {
			get {
				if ( ControlStyleCreated ) {
					return ( ((TableStyle) ControlStyle).HorizontalAlign );
				}
				return ( HorizontalAlign.NotSet );
			}
			set { ((TableStyle) ControlStyle).HorizontalAlign = value; }
		}
		
		// BackColor
		// ForeColor
		// BorderColor
		// BorderStyle
		// BorderWidth
		// Height
		// Width
		
		#endregion
		
		public string Title {
			get { 
				object obj = ViewState["Title"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["Title"] = value; }
		}
					
		public string  Label {
			get { return ( Title ); }
			set { Title = value; }
		}
					
		public string TitleLink {
			get { 
				object obj = ViewState["TitleLink"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["TitleLink"] = value; }
		}
					
		#region Title-Cell Style Properties
		
		public TableItemStyle TitleCell {
			get { return ( titlecell ); }
		}
		private TableItemStyle titlecell = new TableItemStyle();
						
		public string TitleBackImageUrl {
			get { 
				object obj = ViewState["TitleBackImageUrl"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["TitleBackImageUrl"] = value; }
		}
						
		#endregion			
				
		public string ContentText {
			get { 
				object obj = ViewState["ContentText"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["ContentText"] = value; }
		}
					
		#region Content-Cell Style Properties
		
		public TableItemStyle ContentCell {
			get { return ( contentcell ); }
		}
		private TableItemStyle contentcell = new TableItemStyle();
						
		public Unit ContentMargin {
			get { 
				object obj = ViewState["ContentMargin"];
				return ( obj == null ? Unit.Empty : (Unit) obj ); 
			}
			set { ViewState["ContentMargin"] = value; }
		}
						
		public string ContentBackImageUrl {
			get { 
				object obj = ViewState["ContentBackImageUrl"];
				return ( obj == null ? String.Empty : (string) obj ); 
			}
			set { ViewState["ContentBackImageUrl"] = value; }
		}
						
		#endregion

		public bool VerticalScroll {
			get { 
				object obj = ViewState["VerticalScroll"];
				return ( obj == null ? false : (bool) obj ); 
			}
			set { ViewState["VerticalScroll"] = value; }
		}
						
		public bool HorizontalScroll {
			get { 
				object obj = ViewState["HorizontalScroll"];
				return ( obj == null ? false : (bool) obj ); 
			}
			set { ViewState["HorizontalScroll"] = value; }
		}
						
		#endregion
				
		#region Methods 
		
		protected override Style CreateControlStyle() {
			// CssClass
			// Font
			// ForeColor
			TableStyle sty = new TableStyle( ViewState );
			sty.BorderWidth = 0;
			sty.CellSpacing = 0;
			sty.CellPadding = 3;
			sty.GridLines = GridLines.None;
			return ( sty );
		}
		
		protected override void RenderContents( HtmlTextWriter writer ) {
			switch ( FrameType ) {
				case FramerType.NoTitleFrame:
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderContentCell( writer );
					writer.RenderEndTag(); // tr
					break;
				case FramerType.TopTitleFrame:
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderTitleCell( writer );
					writer.RenderEndTag(); // tr
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderContentCell( writer );
					writer.RenderEndTag(); // tr
					break;
				case FramerType.LeftTitleFrame:
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderTitleCell( writer );
					RenderContentCell( writer );
					writer.RenderEndTag(); // tr
					break;
				case FramerType.BottomTitleFrame:
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderContentCell( writer );
					writer.RenderEndTag(); // tr
					writer.RenderBeginTag( HtmlTextWriterTag.Tr );
					RenderTitleCell( writer );
					writer.RenderEndTag(); // tr
					break;
			}
		}
				
		protected virtual bool HasTitle() {
			return ( ( Title != "" ) && ( FrameType != FramerType.NoTitleFrame ) );
		}

		protected virtual void RenderTitleCell( HtmlTextWriter writer ) {
			if ( ! HasTitle() ) return;
			TitleCell.AddAttributesToRender( writer );
			if ( TitleBackImageUrl != String.Empty ) {
				writer.AddStyleAttribute( "background-image", "url(" + ResolveUrl(TitleBackImageUrl) + ")" );
			}
			switch ( FrameType ) {
				case FramerType.LeftTitleFrame:
					writer.AddStyleAttribute( "padding-top", "2px" );
					writer.AddStyleAttribute( "padding-right", "5px" );
					break;
				case FramerType.TopTitleFrame:
					writer.AddStyleAttribute( "padding-left", "6px" );
					writer.AddStyleAttribute( "padding-bottom", "2px" );
					break;
			}
			writer.RenderBeginTag( HtmlTextWriterTag.Td );
			RenderTitle( writer );
			writer.RenderEndTag(); // td
		}
		
		protected virtual void RenderTitle( HtmlTextWriter writer ) {
			if ( TitleLink != "" ) {
				writer.AddAttribute( HtmlTextWriterAttribute.Href, this.ResolveUrl(TitleLink) );
				writer.RenderBeginTag( HtmlTextWriterTag.A );
			}
			writer.Write( Title );
			if ( TitleLink != "" ) writer.RenderEndTag();  // a
		}
				
		protected virtual void RenderContentCell( HtmlTextWriter writer ) {
			ContentCell.AddAttributesToRender( writer );
			if ( ContentBackImageUrl != String.Empty )
				writer.AddStyleAttribute( "background-image", "url(" + ResolveUrl(ContentBackImageUrl) + ")" );
			if ( ! ContentMargin.IsEmpty )
				writer.AddStyleAttribute( "padding", ContentMargin.ToString() );
			writer.RenderBeginTag( HtmlTextWriterTag.Td );
			if ( VerticalScroll || HorizontalScroll ) {
				writer.AddStyleAttribute( "height", ContentCell.Height.ToString() );
				writer.AddStyleAttribute( "width", ContentCell.Width.ToString() );
				writer.AddStyleAttribute( "scrollbar-base-color", TypeDescriptor.GetConverter(BackColor).ConvertToString(BackColor) );
						
				string sty = "overflow" + ( ! HorizontalScroll ? "-y" : "" ) + ( ! VerticalScroll ? "-x" : "" );
				writer.AddStyleAttribute( sty, "auto" );
				writer.RenderBeginTag( HtmlTextWriterTag.Div );
			}
			RenderContent( writer );
			if ( VerticalScroll || HorizontalScroll )
				writer.RenderEndTag(); // div
			writer.RenderEndTag(); // td
		}
	
		protected virtual void RenderContent( HtmlTextWriter writer ) {
			if ( Renderer != null ) writer.Write( Renderer.RenderAsText() );
			RenderChildren( writer );
			writer.Write( ContentText );
		}
				
		#endregion

	}
	
}
