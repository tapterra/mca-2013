function stixMenuBar( id, topCssName, topOverCssName, subCssName, subOverCssName ) {
	this.id = id;
	this.topCssName = topCssName;
	this.topOverCssName = topOverCssName;
	this.subCssName = subCssName;
	this.subOverCssName = subOverCssName;
	
	this.menuBar = document.getElementById( this.id );
	
	this.getSubMenu = function( mitem ) {
		return ( document.getElementById( mitem.id + '_submenu' ) );
	}
	
	this.topCellMouseOver = function( mitem ) {
		mitem.className = this.subOverCssName;
		var submenu = this.getSubMenu( mitem );
		if ( submenu != null ) {
			if ( this.menuBar.topMenuOrientation == 'vertical' ) {
				alert( submenu.id );
				submenu.style.top = mitem.offsetTop;
				
				if ( this.menuBar.horizontalAlign == 'right' ) {
					submenu.style.left = mitem.offsetLeft - submenu.offsetWidth;
				} else {
					submenu.style.left = mitem.offsetLeft + mitem.offsetWidth;
				}
				submenu.style.visibility = 'visible';
			} else {
				submenu.style.top = this.menuBar.offsetHeight;
				var topitemright = mitem.offsetLeft + mitem.offsetWidth;
				if ( this.menuBar.horizontalAlign == 'right' ) {
					submenu.style.left = mitem.offsetParent.offsetLeft + topitemright - submenu.offsetWidth; 
					if ( submenu.offsetLeft < 0 ) submenu.style.left = 0;
				} else {
					submenu.style.left = mitem.offsetLeft;
					var subright = submenu.offsetLeft + submenu.offsetWidth;
					if ( subright > this.menuBar.offsetWidth )
						submenu.style.left = this.menuBar.offsetWidth - submenu.offsetWidth;
				}
				submenu.style.visibility = 'visible';
			}
		}
	}
	this.topCellMouseOut = function( mitem ) {
		mitem.className = this.topCssName;
		// Make my submenu hidden
		var submenu = this.getSubMenu( mitem );
		if ( submenu != null ) submenu.style.visibility = 'hidden';
	}
	
	this.topCellMouseDown = function( mitem, url ) {
		if ( url != '' ) {
			document.location = url;
		}
	}
	
	this.subTableMouseOver = function( submenu ) {
		// Get the name of the top-menu item for this submenu
		var topname = submenu.id.substr( 0, submenu.id.lastIndexOf( '_' ) );
			
		// Set the top-menu item's css-class
		var topitem = document.getElementById( topname );
		topitem.className = this.topOverCssName;
			
		// Make sure this submenu stays visible
		submenu.style.visibility = 'visible';
			
	}
	
	this.subTableMouseOut = function( submenu ) {
		// Get the name of the top-menu item for this submenu
		var topname = submenu.id.substr( 0, submenu.id.lastIndexOf( '_' ) );
			
		// Set the top-menu item's css-class
		var topitem = document.getElementById( topname );
		topitem.className = this.topCssName;
			
		// Make this submenu hidden
		submenu.style.visibility = 'hidden';
			
	}
	
	this.subCellMouseOver = function( mitem ) {
		// Set this item's css-class
		mitem.className = this.subOverCssName;
	}
	
	this.subCellMouseOut = function( mitem ) {
		// Set this item's css-class
		mitem.className = this.subCssName;
	}
	
	this.subCellMouseDown = function( mitem, url ) {
		if ( url != '' ) {
			document.location = url;
		}
	}
	
}
