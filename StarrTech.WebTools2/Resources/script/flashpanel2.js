
// Eventually, this will be used to replace the FlashPanel control with one that also does plug-in detection.
// ... but right now, I'm not seeing clearly how that will happen.

// JavaScript Document
function CreateFlash( ID, Movie, Width, Height, BGC, Play, Loop, Qual, FlashVars, WMode, Scale ) {
	var ContentVersion = 8;
	var plugin = (navigator.mimeTypes && 
		navigator.mimeTypes["application/x-shockwave-flash"]) ? navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin : 0;
	if ( plugin ) {
		var words = navigator.plugins["Shockwave Flash"].description.split(" ");
		var PluginVersion = -1;
		for ( var i = 0 ; i < words.length ; ++i ) {
			if (isNaN(parseInt(words[i]))) continue;
			PluginVersion = words[i]; 
		}
		var MM_FlashCanPlay = PluginVersion >= ContentVersion;
	} else if ( navigator.userAgent && navigator.userAgent.indexOf( "MSIE" ) >= 0 && ( navigator.appVersion.indexOf("Win") != -1 ) ) {
		document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n'); //FS hide this from IE4.5 Mac by splitting the tag
		document.write('on error resume next \n');
		document.write('MM_FlashCanPlay = ( IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash." & ContentVersion)))\n');
		document.write('</SCR' + 'IPT\> \n');
	}
	function writeCode() {
		if ( MM_FlashCanPlay ) {
			var objectTag = 
				'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="flash"' + flashDimensions + '><param name="movie" value=' + flashName + 
				'><param name="quality" value="high"><embed src=' + flashName + ' quality="high" ' + flashDimensions + 
				' name="flash" type="application/x-shockwave-flash"></embed></object>';
			document.getElementById('flashcontent').innerHTML = objectTag;
		}
	}
}
// JavaScript File (flashpanel.js)
function CreateIEFlash( ID, Movie, Width, Height, BGC, Play, Loop, Qual, FlashVars, WMode, Scale ) {
	document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ');
	document.write('codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" ');
	document.write('width="'+ Width + '" '); 
	document.write('height="' + Height + '" ');
	document.write('id="' + ID + '">');
	document.write('<param name="movie" value="'+ Movie + '"');
	document.write('<param name="quality" value="'+Qual+'">');
	if ( BGC.length > 0 ) { document.write('<param name="bgcolor" value="'+ BGC +'>'); }
	if ( FlashVars.length > 0 ) { document.write('<param name="FlashVars" value="'+ FlashVars +'>'); }
	if ( WMode.length > 0 ) { document.write('<param name="wmode" value="'+ WMode +'>'); }
	if ( Scale.length > 0 ) { document.write('<param name="scale" value="'+ Scale +'>'); }
	if ( Loop.length > 0 ) { document.write('<param name="loop" value="' + Loop + '">'); }
	if ( Play.length > 0 ) { document.write('<param name="play" value="'+ Play +'">'); }
	document.write('</object>');
}