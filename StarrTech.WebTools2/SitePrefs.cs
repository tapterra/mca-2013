using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;

namespace StarrTech.WebTools {
	///<summary>
	/// Retrieves preference strings and builds style sheets from xml.
	///</summary>
	public class SitePrefs {

		#region Constants
		
		private static string kGetPrefPath = "/siteprefs/prefs/pref[@name='{0}']";
		
		private static string kGetCssSheetPath = "/siteprefs/cssclasses[@name='{0}']";
		
		#endregion
		
		#region Properties

		private static string documentsource = "~/App_Data/siteprefs.xml";
		public static string DocumentSource {
			get { return ( documentsource ); }
			set { 
				documentsource = value;
				// Make sure our XML is reloaded...
				xmldocument = null;
			}
		}
		

		private static XmlDocument xmldocument = null;
		private static XmlDocument XmlDocument {
			get { 
				string pathkey = HttpContext.Current.Server.MapPath( DocumentSource );
				xmldocument = (XmlDocument) HttpContext.Current.Cache[pathkey];
				if ( xmldocument == null ) {
					xmldocument = new XmlDocument();
					xmldocument.Load( pathkey ); 
					HttpContext.Current.Cache.Insert( pathkey, xmldocument, new CacheDependency( pathkey ) );
				}
				return ( xmldocument );
			}
		}
		
		
		#endregion
		
		#region BrowserCaps Properties

		///<summary>
		/// Returns true if the current client is a Windows machine
		///</summary>
		public static bool IsWin {
			get { return ( HttpContext.Current.Request.Browser.Platform.StartsWith( "Win" ) ); }
		}
		
		
		///<summary>
		/// Returns true if the current client is a Macintosh
		///</summary>
		public static bool IsMac {
			get { return ( HttpContext.Current.Request.Browser.Platform.StartsWith( "Mac" ) ); }
		}
		
		
		///<summary>
		/// Returns true if the current client is a Unix machine
		///</summary>
		public static bool IsUnix {
			get { return ( HttpContext.Current.Request.Browser.Platform.StartsWith( "Unix" ) ); }
		}
		
		
		///<summary>
		/// Returns true if the current client's browser is Internet Explorer
		///</summary>
		public static bool IsIE {
			get { return ( HttpContext.Current.Request.UserAgent.IndexOf( "MSIE" ) >= 0 ); }
		}
		
		
		///<summary>
		/// Returns true if the current client's browser is AOL
		///</summary>
		public static bool IsAOL {
			get { return ( HttpContext.Current.Request.Browser.AOL ); }
		}
		
		
		///<summary>
		/// A more rational definition of an "uplevel" browser
		///</summary>
		public static bool IsUpLevel {
			get { 
				bool result = false;
				if ( IsWin ) {
					if ( IsIE ) result = ( HttpContext.Current.Request.Browser.MajorVersion >= 4 );
					else result = ( HttpContext.Current.Request.Browser.MajorVersion >= 6 );
				}
				if ( IsMac ) {
					if ( IsIE ) result = ( HttpContext.Current.Request.Browser.MajorVersion >= 5 );
					else result = ( HttpContext.Current.Request.Browser.MajorVersion >= 6 );
				}
				return ( result ); 
			}
		}
		
		
		///<summary>
		/// Returns "ie" for Internet Explorer, else returns "ns" for sorting out platform-specific prefs
		///</summary>
		public static string BrowserCode {
			get { 
				if ( IsIE ) return ( "ie" );
				return ( "ns" );
			}
		}
		
		
		///<summary>
		/// Returns "win", "mac" or "unix" for sorting out platform-specific prefs
		///</summary>
		public static string PlatformCode {
			get { 
				if ( IsMac )  return ( "mac" );
				if ( IsUnix ) return ( "unix" );
				return ( "win" );
			}
		}
		
		
		///<summary>
		/// Returns true if the app is currently running on a test server
		///</summary>
		public static bool IsTesting {
			get { 
				string sname = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
				return ( sname.StartsWith( "test" ) || sname.StartsWith( "local" ) ); 
			}
		}
		
		
		#endregion

		private SitePrefs() {}
		
		
		#region Prefs Methods		
		
		///<summary>
		/// Extracts the proper (testvalue vs. value, text element vs. attribute) value for a given node
		///</summary>
		private static string GetValue( string key, XmlNode pref ) {
			XmlAttribute valueAttr;
			if ( pref == null ) throw( new Exception( "Undefined pref: " + key ) );
					
			// If we're running on a test server, look for a "testvalue" first, and return its value if found
			if ( IsTesting ) {
				valueAttr = (XmlAttribute) pref.Attributes.GetNamedItem("testvalue");
				if ( valueAttr != null ) return ( valueAttr.Value );
			}
			// Try the "value" attribute next...
			valueAttr = (XmlAttribute) pref.Attributes.GetNamedItem("value");
			if ( valueAttr != null) {
				return ( valueAttr.Value );
			}
			// Next, see if we have a "prefvalue" attribute - if so, it's value is a key & we need that key's value
			valueAttr = (XmlAttribute) pref.Attributes.GetNamedItem("prefvalue");
			if ( valueAttr != null ) {
				return ( GetPref( valueAttr.Value ) );
			}
			// We're still looking - see if the element has a child text node
			string prefValue = "";
			if ( pref.HasChildNodes ) {
				prefValue = pref.FirstChild.Value;
				if ( prefValue != null ) return ( prefValue );
			}
			// Last chance - look for a web.config entry with this key
			try {
				prefValue = WebConfigurationManager.AppSettings[key];
			} catch {}
			if ( string.IsNullOrEmpty( prefValue ) ) {
				throw ( new Exception( "Undefined value for pref: " + key ) );
			}
			return ( prefValue );
		}
		
		
		///<summary>
		/// Given a key, determine the pref value 
		///</summary>
		public static string GetPref( string key ) {
			string xpath = String.Format( kGetPrefPath, key );
			XmlNode thepref = null;
			try {
				XmlNodeList preflist = XmlDocument.DocumentElement.SelectNodes( xpath );
				if ( preflist.Count == 0 ) throw( new Exception( "Undefined value for pref: " + key ) );							
				if ( preflist.Count == 1 ) return ( GetValue( key, preflist[0] ) );
							
				// We have more than one element for this pref...
				foreach ( XmlNode pref in preflist ) {
					XmlAttribute browserAttr = (XmlAttribute) pref.Attributes.GetNamedItem("browser");
					XmlAttribute platformAttr = (XmlAttribute) pref.Attributes.GetNamedItem("platform");
					if ( ( browserAttr != null ) && ( platformAttr != null ) ) {
						if ( ( browserAttr.Value == BrowserCode ) && ( platformAttr.Value == PlatformCode ) ) {
							thepref = pref;
							break; // this is definitive
						}
					} else if ( browserAttr != null ) {
						if ( browserAttr.Value == BrowserCode ) {
							thepref = pref;
						}
					} else if ( platformAttr != null ) {
						if ( platformAttr.Value == PlatformCode ) {
							thepref = pref;
						}
					} else {
						thepref = pref;
					}
				}
				return ( GetValue( key, thepref ) );
			} catch {
				try {
					return ( WebConfigurationManager.AppSettings[key] );
				} catch {
					throw( new Exception( "Undefined value for pref: " + key ) );
				}
			}
		}
		
		
		///<summary>
		/// Return the proper value for a given key, or an empty string if not defined
		///</summary>
		public static string CheckPref( string key ) {
			string val = "";
			try { val = GetPref( key ); }
			catch {}
			return ( val );
		}
		
		
		///<summary>
		/// Return the proper value for a given key, or a specified default if not defined
		///</summary>
		public static string CheckPref( string key, string defaultvalue ) {
			string val = CheckPref( key );
			return ( string.IsNullOrEmpty( val ) ? defaultvalue : val );
		}
		
		
		///<summary>
		/// Returns the value for a given key converted to a Color
		///</summary>
		public static Color GetColor( string key ) {
			return ( (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( GetPref( key ) ) );
		}
		
		
		///<summary>
		/// Returns the value for a given key or a specified default value, converted to a Color
		///</summary>
		public static Color CheckColor( string key, string defaultvalue ) {
			return ( (Color) TypeDescriptor.GetConverter( typeof(Color) ).ConvertFromInvariantString( CheckPref( key, defaultvalue ) ) );
		}
				
		
		///<summary>
		/// Returns the value for a given key or a specified default value, converted to a Color
		///</summary>
		public static Unit GetUnit( string key ) {
			return ( new Unit( GetPref( key ) ) );
		}
		
		
		///<summary>
		/// Returns the value for a given key or a specified default value, converted to a Unit
		///</summary>
		public static Unit CheckUnit( string key, string defaultvalue ) {
			return ( new Unit( CheckPref( key, defaultvalue ) ) );
		}
				

		///<summary>
		/// Returns the value for a given key converted to a FontUnit
		///</summary>
		public static FontUnit GetFontUnit( string key ) {
			return ( new FontUnit( GetPref( key ) ) );
		}
		
		
		///<summary>
		/// Returns the value for a given key or a specified default value, converted to a FontUnit
		///</summary>
		public static FontUnit CheckFontUnit( string key, string defaultvalue ) {
			return ( new FontUnit( CheckPref( key, defaultvalue ) ) );
		}
		
		
		#endregion
		
		#region CSS Stylesheet Methods

		///<summary>
		/// Builds the specified CSS stylesheet and returns it as a string value.
		///</summary>
		public static string BuildCss( Page page, string sheetname ) {
			if ( sheetname == null ) return ( "" );
			StringBuilder cssBuilder = new StringBuilder();
			string xpath = String.Format( kGetCssSheetPath, sheetname );
			XmlNode sheet = XmlDocument.DocumentElement.SelectSingleNode( xpath );
			if ( sheet == null ) throw( new Exception( "Undefined sheetname: " + sheetname ) );
			
			XmlNodeList csslist = sheet.SelectNodes( "cssclass" );
			foreach ( XmlNode cssclass in csslist ) {
				string header = ((XmlAttribute) cssclass.Attributes.GetNamedItem("name")).Value;
				cssBuilder.AppendFormat( "{0} {{", header );
				XmlNodeList stylelist = cssclass.SelectNodes( "cssstyle" );
				foreach ( XmlNode style in stylelist ) {
					XmlAttributeCollection attribs = style.Attributes;
					string styname = ((XmlAttribute) attribs.GetNamedItem("name")).Value;
					string styval = "";
					XmlAttribute valueAttr = ((XmlAttribute) attribs.GetNamedItem("value"));
					if ( valueAttr == null ) {
						valueAttr = ( (XmlAttribute) attribs.GetNamedItem("prefvalue") );
						if ( valueAttr != null ) styval = GetPref( valueAttr.Value );
					} else {
						styval = valueAttr.Value;
					}
					// For URL values, see if they need to be resolved relative to the root
					if ( styval.StartsWith( "url(" ) && styval.EndsWith( ")" ) && styval.IndexOf("~") > 0 ) {
						styval = styval.Substring( 4, styval.Length - 5 );
						if ( page != null ) {
							styval = String.Format( "url({0})", page.ResolveUrl( styval ) );
						} else {
							// HACK: sometimes the Page just isn't available...
							styval = String.Format( "url({0})", styval.Replace( "~", "" ) );
						}
					}
					XmlAttribute browserAttr = ( (XmlAttribute) attribs.GetNamedItem("browser") );
					XmlAttribute platformAttr = ( (XmlAttribute) attribs.GetNamedItem("platform") );
					if ( ( browserAttr != null ) && ( platformAttr != null ) ) {
						if ( ( browserAttr.Value == BrowserCode ) && ( platformAttr.Value == PlatformCode ) ) {
							cssBuilder.AppendFormat( "{0} : {1}; ", styname, styval );
						}
					} else if ( browserAttr != null ) {
						if ( browserAttr.Value == BrowserCode ) {
							cssBuilder.AppendFormat( "{0} : {1}; ", styname, styval );
						}
					} else if ( platformAttr != null ) {
						if ( platformAttr.Value == PlatformCode ) {
							cssBuilder.AppendFormat( "{0} : {1}; ", styname, styval );
						}
					} else {
						cssBuilder.AppendFormat( "{0} : {1}; ", styname, styval );
					}
				}
				cssBuilder.Append( "}\r" );
			}
			return ( cssBuilder.ToString() );
		}
		

		///<summary>
		/// Builds the specified CSS stylesheet and writes it to the specified HtmlTextWriter.
		///</summary>
		public static void WriteCss( HtmlTextWriter writer, Page page, string sheetname ) {
			writer.Write( BuildCss( page, sheetname ) );
		}
		

		///<summary>
		/// Builds the specified CSS stylesheet and writes it to the current page-response.
		///</summary>
		public static void WriteCss( Page page, string sheetname ) {
			HttpContext.Current.Response.Write( BuildCss( page, sheetname ) );
		}
		
		
		#endregion
		
	}
	
	
	/// <summary>
	/// An Httphandler to extract a StyleSheet from the SitePrefs.xml by URL reference
	/// Needs either an ashx file, or an axd reference in web.config
	/// </summary>
	public class SitePrefsCssHandler : IHttpHandler {
		
		bool IHttpHandler.IsReusable {
			get { return ( false ); }
		}
		
		void IHttpHandler.ProcessRequest( HttpContext context ) {
			string sheetname = context.Request["sheetname"];
			context.Response.ContentType = "text/css";
			context.Response.Write( SitePrefs.BuildCss( null, sheetname ) );
			context.Response.End();
		}
		
	}
	
	
}


