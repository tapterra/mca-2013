﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;
using StarrTech.WebTools.CMS;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "StarrTech.WebTools2" )]
[assembly: AssemblyDescription( "" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "" )]
[assembly: AssemblyProduct( "StarrTech.WebTools2" )]
[assembly: AssemblyCopyright( "Copyright © 2005  StarrTech Interactive" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible( false )]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "fe3f33fa-0bd6-49dd-b910-3ccaae088bd9" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: AssemblyFileVersion( "1.0.0.0" )]

// 
// WebResource declarations
// 

// Used by the MenuPanel component...
[assembly: WebResource( "StarrTech.WebTools.Resources.script.menupanel.js", MimeTypes.TextScript )]
// Used by the FlashPanel component
[assembly: System.Web.UI.WebResource( "StarrTech.WebTools.Resources.script.flashpanel.js", MimeTypes.TextScript )]

// Used by the TreeUI controls...
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.minus.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.null.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.plus.gif", MimeTypes.ImageGif )]

[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.openfolder.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.closedfolder.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.flatfolder.gif", MimeTypes.ImageGif )]

[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.move-down.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.move-up.gif", MimeTypes.ImageGif )]

[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.menu.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.treeui.eyeball.gif", MimeTypes.ImageGif )]

// Used by the TextBox controls...
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.reqtagl.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.reqtagr.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.spacer.gif", MimeTypes.ImageGif )]
// Used by the DatePicker...
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.calendar-button.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.clear-calendar-button.gif", MimeTypes.ImageGif)]

// Used by the DBUI controls...
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.download-button.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.preview-button.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.sort-asc.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.sort-desc.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.move-up.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.move-down.gif", MimeTypes.ImageGif )]

[assembly: WebResource( "StarrTech.WebTools.Resources.controls.email-button.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.print-button.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.controls.eyeball.gif", MimeTypes.ImageGif )]

// Document Icons...
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.chm.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.exe.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.install.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.image_bmp.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.image_gif.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.image_jpeg.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.image_png.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-access.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-excel.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-powerpoint.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-publisher.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-visio.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.ms-word.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.pdf.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.photoshop.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.text_html.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.text_javascript.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.text_plain.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.unknown.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.zip.gif", MimeTypes.ImageGif )]
[assembly: WebResource( "StarrTech.WebTools.Resources.docicons.folder.gif", MimeTypes.ImageGif )]
