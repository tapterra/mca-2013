using System;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Web.Caching;


namespace StarrTech.WebTools.Data {
	
	/// <summary>
	/// Generates a Dataset for data-binding a (relatively) static lookup-table.
	/// Automatically manages caching (expires every hour).
	/// </summary>
	public abstract class DatasetLoader {
		
		protected string TableName = "";
		protected string IDColumnName = "id";
		protected string LabelName = "label";
		
		/// <summary>
		/// Generates a unique cache key using the private attributes.
		/// </summary>
		protected virtual string CacheKey {
			get { return ( String.Format( "{0}-{1}-{2}", TableName, IDColumnName, LabelName ) ); }
		}
		
		/// <summary>
		/// Returns the cached DataSet - automatically reloads if the cache has expired.
		/// </summary>
		public virtual DataSet CachedDataSet {
			get {
				object obj = HttpContext.Current.Cache[CacheKey];
				if ( obj == null ) obj = LoadDataSet();
				return ( (DataSet) obj );
			}
		}
		
		/// <summary>
		/// Recreates the DataSet from the data source and stores it in the application cache.
		/// </summary>
		/// <returns>The current DataSet as an Object after caching.</returns>
		protected virtual Object LoadDataSet() {
			DataSet ds;
			using ( SqlDatabase db = new SqlDatabase() ) {
				string sql = String.Format( "SELECT {0}, {1} FROM {2} ORDER BY {0}", IDColumnName, LabelName, TableName );
				ds = db.GetDataSet( TableName, sql );
			}
			return ( InsertToCache( ds ) );
		}
		
		/// <summary>
		/// Inserts the dataset into the application cache with the appropriate dependencies.
		/// </summary>
		/// <param name="ds">The newly-created DataSet.</param>
		/// <returns>The newly-created DataSet cast as an Object.</returns>
		protected virtual Object InsertToCache( DataSet ds ) {
			// Refresh the cache every hour
			DateTime expires = DateTime.Now.AddHours( 1 );
			HttpContext.Current.Cache.Insert( CacheKey, ds, null, expires, TimeSpan.Zero, CacheItemPriority.AboveNormal, null );
			return ( ds );
		}
		
	}
	
	/// <summary>
	/// Implements DatasetLoader for XML sources. The cache expires when the source file is modified.
	/// </summary>
	public abstract class XmlDatasetLoader : DatasetLoader {
		
		/// <summary>
		/// Returns the absolute path to the source XML data file.
		/// </summary>
		public string XmlPath {
			get { return ( HttpContext.Current.Server.MapPath( xmlpath ) ); }
		}
		private string xmlpath = "";
		
		/// <summary>
		/// Overrides the CacheKey property to use the source XML-file path.
		/// </summary>
		protected override string CacheKey {
			get { return ( XmlPath ); }
		}
		
		/// <summary>
		/// Load the dataset from the data source into the application cache.
		/// </summary>
		/// <returns>The cached DataSet cast as an Object.</returns>
		protected override Object LoadDataSet() {
			DataSet ds = new DataSet();
			DataTable dt = new DataTable( TableName );
			DefineColumns( dt );
			ds.Tables.Add( dt );
			ds.ReadXml( XmlPath, XmlReadMode.IgnoreSchema );
			return ( InsertToCache( ds ) );
		}
		
		/// <summary>
		/// Defines the dataset table columns.
		/// </summary>
		/// <param name="dt">A reference to the DataTable in the result DataSet.</param>
		protected virtual void DefineColumns( DataTable dt ) {
			dt.Columns.Add( IDColumnName, typeof( int ) );
			dt.Columns.Add( LabelName, typeof(string) );
		}
		
		/// <summary>
		/// Inserts the dataset into the application cache with the appropriate dependencies.
		/// </summary>
		/// <param name="ds">The newly-created DataSet.</param>
		/// <returns>The newly-created DataSet cast as an Object.</returns>
		protected override Object InsertToCache( DataSet ds ) {
			// Refresh the cache whenever the xml-file changes
			HttpContext.Current.Cache.Insert( CacheKey, ds, new CacheDependency( XmlPath ) );
			return ( ds );
		}
		
	}
	
}
