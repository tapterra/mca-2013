using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Reflection;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Data.Common;
using System.Web.Configuration;

namespace StarrTech.WebTools.Data {

	/// <summary>
	/// SQL Server data-layer class - encapsulates most of the common ADO.NET functions needed for web apps.
	/// </summary>
	public class SqlDatabase : IDisposable {

		#region Properties

		/// <summary>
		/// Key for retrieving the database connection-string from web.config (default is "default")
		/// </summary> 
		private string configKey = "default";

		/// <summary>
		/// A static, assignable connection string to use when no config files are available (as when testing).
		/// </summary>
		public static string DefaultConnectionString {
			get { return ( SqlDatabase.defaultConnectionString ); }
			set { SqlDatabase.defaultConnectionString = value; }
		}
		private static string defaultConnectionString = "";

		/// <summary>
		/// A static, assignable data-provider to use when no config files are available (as when testing).
		/// </summary>
		public static string DefaultProviderName {
			get { return ( SqlDatabase.defaultProviderName ); }
			set { SqlDatabase.defaultProviderName = value; }
		}
		private static string defaultProviderName = "System.Data.SqlClient";

		public static string OverrideConnectionString {
			get { return ( SqlDatabase.overrideConnectionString ); }
			set { SqlDatabase.overrideConnectionString = value; }
		}
		private static string overrideConnectionString;

		/// <summary>
		/// The database connection-string 
		/// </summary>
		protected string ConnectionStr {
			get {
				if ( !string.IsNullOrEmpty( overrideConnectionString ) ) {
					return ( overrideConnectionString );
				}
				if ( string.IsNullOrEmpty( this.connectionstr ) ) {
					// Try to load the connection string using our current configkey
					//Configuration webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration( "~/" );
					if ( 0 < WebConfigurationManager.ConnectionStrings.Count ) {
						ConnectionStringSettings connString = WebConfigurationManager.ConnectionStrings[this.configKey];
						if ( connString != null ) {
							this.connectionstr = connString.ConnectionString;
						} else {
							if ( string.IsNullOrEmpty( SqlDatabase.DefaultConnectionString ) ) {
								throw new ArgumentNullException( "ConnectionStr", "No connection string value or default defined for SqlDatabase." );
							} else {
								return ( SqlDatabase.DefaultConnectionString );
							}
						}
					}
				}
				return ( this.connectionstr );
			}
			set { this.connectionstr = value; }
		}
		private string connectionstr = "";

		/// <summary>
		/// Identifies the type of database
		/// </summary>
		protected string DataProvider {
			get {
				if ( string.IsNullOrEmpty( this.dataProvider ) ) {
					// Try to load the connection string using our current configkey
					//Configuration webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration( "~/" );
					if ( 0 < WebConfigurationManager.ConnectionStrings.Count ) {
						ConnectionStringSettings connString = WebConfigurationManager.ConnectionStrings[this.configKey];
						if ( connString != null ) {
							this.dataProvider = connString.ProviderName;
						} else {
							if ( string.IsNullOrEmpty( SqlDatabase.DefaultProviderName ) ) {
								throw new ArgumentNullException( "DataProvider", "No DataProvider value or default defined for SqlDatabase." );
							} else {
								return ( SqlDatabase.DefaultProviderName );
							}
						}
					}
				}
				return ( this.dataProvider );
			}
			set { dataProvider = value; }
		}
		private string dataProvider; //= "System.Data.SqlClient";

		/// <summary>
		/// The SqlConnection object used to connect to the database
		/// </summary>
		protected DbConnection Connection {
			get {
				if ( this.connection == null ) {
					//this.connection = new SqlConnection( this.ConnectionStr );
					this.connection = ProviderFactory.CreateConnection();
					this.connection.ConnectionString = this.ConnectionStr;
				}
				return ( this.connection );
			}
		}
		private DbConnection connection;

		/// <summary>
		/// Returns true when our database connection is open
		/// </summary>
		public bool IsConnected {
			get {
				if ( this.Connection == null )
					return ( false );
				return ( this.Connection.State == ConnectionState.Open );
			}
		}

		public const string DefaultIDColumnName = "id";
		public const string DefaultIsDeletedColumnName = "is_deleted";

		#endregion

		#region Constructors

		/// <summary>
		/// Use this to connect via the default connection-string config-key ("default").
		/// </summary>
		public SqlDatabase() { }

		/// <summary>
		/// Used to provide a new connection-string key (when we have more than one database to connect to).
		/// </summary>
		/// <param name="key">Key assigned to the connection-string in web.config</param>
		public SqlDatabase( string key ) {
			this.configKey = key;
		}

		#endregion

		#region Connection-Management Methods

		/// <summary>
		/// Opens the database connection (unless its already open).
		/// </summary>
		protected void OpenConnection() {
			if ( !this.IsConnected ) {
				this.Connection.Open();
			}
		}

		/// <summary>
		/// Closes the connection to the database.
		/// </summary>
		protected void CloseConnection() {
			while ( this.IsConnected ) {
				this.Connection.Close();
			}
		}

		/// <summary>
		/// Implement IDisposable so we can make use of the "using" operator.
		/// </summary>
		public void Dispose() {
			this.CloseConnection();
		}

		#endregion

		#region DataProvider Factory Methods

		/// <summary>
		/// Returns a provider-factory based on the class-name given for the ConnectionString DataProvider.
		/// This factory is used to generate instances of the appropriate ADO.NET classes for the current database.
		/// </summary>
		protected DbProviderFactory ProviderFactory {
			get { return ( DbProviderFactories.GetFactory( this.DataProvider ) ); }
		}

		public DbCommand NewCommand() {
			return ( this.ProviderFactory.CreateCommand() );
		}

		public DbDataAdapter NewAdapter() {
			return ( this.ProviderFactory.CreateDataAdapter() );
		}

		public DbParameter NewParameter() {
			return ( this.ProviderFactory.CreateParameter() );
		}

		public DbParameter NewParameter( string ParmName, object ParmValue ) {
			return ( NewParameter( this.DataProvider, ParmName, ParmValue ) );
		}

		public DbParameter NewParameter( string ParmName, DbType ParmType, object ParmValue ) {
			DbParameter parm = this.NewParameter( ParmName, ParmValue );
			parm.DbType = ParmType;
			return ( parm );
		}

		public static DbParameter NewParameter( string ProviderName ) {
			DbProviderFactory factory = DbProviderFactories.GetFactory( ProviderName );
			return ( factory.CreateParameter() );
		}

		public static DbParameter NewParameter( string ProviderName, string ParmName, object ParmValue ) {
			DbParameter parm = SqlDatabase.NewParameter( ProviderName );
			parm.ParameterName = ParmName;
			parm.Value = ParmValue;
			return ( parm );
		}

		public static DbParameter NewParameter( string ProviderName, string ParmName, DbType ParmType, object ParmValue ) {
			DbParameter parm = SqlDatabase.NewParameter( ProviderName, ParmName, ParmValue );
			parm.DbType = ParmType;
			return ( parm );
		}

		#endregion

		#region Public ADO.NET Wrapper Methods

		/// <summary>
		/// Create an DbCommand using our intrinsic connection
		/// </summary>
		/// <param name="commandString">A SQL statement.</param>
		/// <param name="parms">One or more DbParameters for the given SQL statement.</param>
		/// <returns>The resulting DbCommand object.</returns>
		public DbCommand GetCommand( string commandString, params DbParameter[] parms ) {
			return ( this.GetCommand( commandString, CommandType.Text, parms ) );
		}
		public DbCommand GetCommand( string commandString, CommandType cmdType, params DbParameter[] parms ) {
			DbCommand cmd = this.NewCommand();
			cmd.Connection = this.Connection;
			cmd.CommandText = commandString;
			cmd.CommandType = cmdType;
			foreach ( DbParameter parm in parms ) {
				cmd.Parameters.Add( parm );
			}
			return ( cmd );
		}

		/// <summary>
		/// Executes a pre-built DbCommand object.
		/// </summary>
		/// <param name="cmd">A pre-built DbCommand object.</param>
		/// <returns>The number of records affected.</returns>
		public int Execute( DbCommand cmd ) {
			this.OpenConnection();
			cmd.Connection = this.Connection;
			int result = cmd.ExecuteNonQuery();
			this.CloseConnection();
			return ( result );
		}

		/// <summary>
		/// Executes a SQL statement (as a string).
		/// </summary>
		/// <param name="sql">A SQL statement.</param>
		/// <param name="parms">One or more DbParameters for the given SQL statement.</param>
		/// <returns>The number of records affected.</returns>
		public int Execute( string sql, params DbParameter[] parms ) {
			return ( this.Execute( sql, CommandType.Text, parms ) );
		}
		public int Execute( string sql, CommandType cmdType, params DbParameter[] parms ) {
			DbCommand cmd = this.GetCommand( sql, cmdType, parms );
			return ( this.Execute( cmd ) );
		}

		/// <summary>
		/// Executes a SQL query that results in a single value.
		/// </summary>
		/// <param name="cmd">A pre-built DbCommand object.</param>
		/// <returns>The first column of the first row in the query's resultset.</returns>
		public object ExecuteScalar( DbCommand cmd ) {
			this.OpenConnection();
			cmd.Connection = this.Connection;
			object result = cmd.ExecuteScalar();
			this.CloseConnection();
			return ( result );
		}

		/// <summary>
		/// Executes a SQL query that results in a single value.
		/// </summary>
		/// <param name="sql">A SQL query statement (as a string).</param>
		/// <param name="parms">One or more DbParameters for the given SQL statement.</param>
		/// <returns>The first column of the first row in the query's resultset.</returns>
		public object ExecuteScalar( string sql, params DbParameter[] parms ) {
			DbCommand cmd = this.GetCommand( sql, parms );
			return ( this.ExecuteScalar( cmd ) );
		}

		/// <summary>
		/// Execute a SQL query string and return results as a DataReader.
		/// </summary>
		/// <param name="sql">A SQL query string.</param>
		/// <param name="parms">One or more DbParameters for the given SQL statement.</param>
		/// <returns>A SqlDataReader object connected to the query resultset.</returns>
		public IDataReader SelectRecords( string sql, params DbParameter[] parms ) {
			this.OpenConnection();
			DbCommand cmd = this.GetCommand( sql, parms );
			return ( cmd.ExecuteReader() );
		}

		/// <summary>
		/// For a given table-name and record ID value, build and execute a query to retrieve the record.
		/// Assumes the table has an int identity primary-key named DefaultIDColumnName.
		/// </summary>
		/// <param name="table">Name of the database table to query.</param>
		/// <param name="recID">Value of the record-ID to retrieve.</param>
		/// <returns>A SqlDataReader object connected to the query resultset.</returns>
		public IDataReader GetRecord( string table, int recID ) {
			return ( this.GetRecord( table, SqlDatabase.DefaultIDColumnName, recID ) );
		}

		/// <summary>
		/// For a given table-name and record ID value, build and execute a query to retrieve the record.
		/// </summary>
		/// <param name="table">Name of the database table to query.</param>
		/// <param name="idField">Name of the int primary-key field.</param>
		/// <param name="recID">Value of the record-ID to retrieve.</param>
		/// <returns>A SqlDataReader object connected to the query resultset.</returns>
		public IDataReader GetRecord( string table, string idField, int recID ) {
			return ( this.GetRecord( table, idField, "*", recID ) );
		}

		/// <summary>
		/// For a given table-name and record ID value, build and execute a query to retrieve the record.
		/// </summary>
		/// <param name="table">Name of the database table to query.</param>
		/// <param name="idField">Name of the int primary-key field.</param>
		/// <param name="fieldList">Comma-delimited list of field names to retrieve.</param>
		/// <param name="recID">Value of the record-ID to retrieve.</param>
		/// <returns>A SqlDataReader object connected to the query resultset.</returns>
		public IDataReader GetRecord( string table, string idField, string fieldList, int recID ) {
			this.OpenConnection();
			string sql = String.Format( "select {3} from {0} where ({1} = {2})", table, idField, recID, fieldList );
			return ( this.SelectRecords( sql ) );
		}

		/// <summary>
		/// For a given table-name and record ID value, build and execute a SQL stmt to delete the record.
		/// </summary>
		/// <param name="table">Name of the pertinent database table.</param>
		/// <param name="recID">Value of the record-ID for the record to delete.</param>
		public void DeleteRecord( string table, int recID ) {
			this.DeleteRecord( table, SqlDatabase.DefaultIDColumnName, recID );
		}

		/// <summary>
		/// For a given table-name and record ID value, build and execute a SQL stmt to delete the record.
		/// </summary>
		/// <param name="table">Name of the pertinent database table.</param>
		/// <param name="idField">Name of the int primary-key field.</param>
		/// <param name="recID">Value of the record-ID for the record to delete.</param>
		public void DeleteRecord( string table, string idField, int recID ) {
			if ( recID > 0 ) {
				this.Execute( String.Format( "delete from {0} where ({1} = {2})", table, idField, recID ) );
			}
		}

		/// <summary>
		/// Used instead of DeleteRecord when the table uses an is_deleted bitfield instead of actually deleting records.
		/// </summary>
		/// <param name="table">Name of the database table</param>
		/// <param name="deleteMarkField">Name of the bit column that marks deleted records</param>
		/// <param name="idField">Name of the record Identity column</param>
		/// <param name="recID">ID of the record to be "deleted"</param>
		/// <param name="deleted">True to delete, false to un-delete.</param>
		public void MarkRecordDeleted( string table, string deleteMarkField, string idField, int recID, bool deleted ) {
			if ( recID > 0 ) {
				string sql = String.Format( "update {0} set {1} = {4} where ( {2} = {3} ); ",
					table, deleteMarkField, idField, recID, deleted ? "1" : "0"
				);
				this.Execute( sql );
			}
		}

		/// <summary>
		/// Execute a SQL query string and return results as a disconnected DataSet.
		/// </summary>
		/// <param name="sql">A SQL query string.</param>
		/// <param name="tablename">A name to assign to the resulting table in the DataSet.</param>
		/// <param name="parms">One or more DbParameters for the given SQL statement.</param>
		/// <returns>A DataSet object containing the query results in a table named tablename.</returns>
		public DataSet GetDataSet( string tablename, string sql, params DbParameter[] parms ) {
			this.OpenConnection();
			// Create an adapter
			DbDataAdapter adapter = this.NewAdapter();
			// Create and associate a command using our sql string
			DbCommand cmd = this.GetCommand( sql, parms );
			adapter.SelectCommand = cmd;
			// Use the adapter to load a new Dataset
			DataSet ds = new DataSet();
			adapter.Fill( ds, tablename );
			// We don't need to maintain the connection when using a Dataset
			this.CloseConnection();
			return ( ds );
		}

		public DataTable GetDataTable( string sql, params DbParameter[] parms ) {
			this.OpenConnection();
			// Create an adapter
			DbDataAdapter adapter = this.NewAdapter();
			// Create and associate a command using our sql string
			DbCommand cmd = this.GetCommand( sql, parms );
			adapter.SelectCommand = cmd;
			// Use the adapter to load a new DataTable
			DataTable dt = new DataTable();
			adapter.Fill( dt );
			// We don't need to maintain the connection
			this.CloseConnection();
			return ( dt );
		}

		#endregion

		#region Public ISqlPersistable Methods

		/// <summary>
		/// Save this instance to the database via INSERT/UPDATE (based on ID value).
		/// </summary>
		/// <param name="target">The ISqlPersistable instance to be persisted.</param>
		public void PersistInstance( ISqlPersistable target ) {
			if ( target.ID > 0 ) {
				this.UpdateInstance( target );
			} else {
				this.InsertInstance( target );
			}
		}

		/// <summary>
		/// Persists the instance as a new record in the database.
		/// </summary>
		/// <param name="target">The ISqlPersistable instance to be persisted.</param>
		public void InsertInstance( ISqlPersistable target ) {
			target.ID = Convert.ToInt32( this.ExecuteScalar( BuildInsertCommand( target ) ) );
		}

		/// <summary>
		/// Persists the instance back to it's source record in the database.
		/// </summary>
		/// <param name="target">The ISqlPersistable instance to be persisted.</param>
		public void UpdateInstance( ISqlPersistable target ) {
			this.Execute( BuildUpdateCommand( target ) );
		}

		/// <summary>
		/// Deletes the instance's database record.
		/// </summary>
		/// <param name="target">The ISqlPersistable instance whose record (ID property value) is to be deleted.
		/// </param>
		public void DeleteInstance( ISqlPersistable target ) {
			SqlTableAttribute tableAtts = SqlDatabase.GetTableAttribute( target.GetType() );
			if ( tableAtts.IsMarkedDeleted ) {
				this.MarkRecordDeleted( tableAtts.TableName, tableAtts.IsDeletedColumnName, tableAtts.IDColumnName, target.ID, true );
			} else {
				this.DeleteRecord( tableAtts.TableName, tableAtts.IDColumnName, target.ID );
			}
			// The record for this instance no longer exists, so it's ID value is invalid...
			target.ID = -1;
		}

		/// <summary>
		/// Resets the instance's marked-for-deletion field in the database record.
		/// </summary>
		/// <param name="target">The ISqlPersistable instance whose record is to be un-deleted.</param>
		public void UndeleteInstance( ISqlPersistable target ) {
			SqlTableAttribute tableAtts = SqlDatabase.GetTableAttribute( target.GetType() );
			// This only works for IsMarkedDeleted tables
			if ( tableAtts.IsMarkedDeleted ) {
				this.MarkRecordDeleted( tableAtts.TableName, tableAtts.IsDeletedColumnName, tableAtts.IDColumnName, target.ID, false );
			}
		}

		/// <summary>
		/// Restores the instance's property values from it's database record.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="target">
		/// The ISqlPersistable instance to be restored (by ID property value) from the database.</param>
		/// <returns>True if a record was read.</returns>
		public bool RestoreInstance( ISqlPersistable target ) {
			SqlTableAttribute tableAttribute = SqlDatabase.GetTableAttribute( target.GetType() );
			string tableName = tableAttribute.TableName;
			string idName = tableAttribute.IDColumnName;
			string fieldList = SqlDatabase.GetFieldList( target.GetType() );
			return ( InstantiateFromDataRdr( this.GetRecord( tableName, idName, fieldList, target.ID ), target ) );
		}

		/// <summary>
		/// Restores the instance's property values from the first record in a SELECT query.
		/// </summary>
		/// <param name="sql">A SQL SELECT statement that will query the appropriate table.</param>
		/// <param name="target">The ISqlPersistable instance to be restored from the database query.</param>
		/// <returns>True if a record was read.</returns>
		public bool SelectInstance( ISqlPersistable target, string sql, params DbParameter[] parms ) {
			return ( InstantiateFromDataRdr( this.SelectRecords( sql, parms ), target ) );
		}

		/// <summary>
		/// Execute a SQL query string and return results as a disconnected DataSet.
		/// </summary>
		/// <param name="sql">A SQL query string.</param>
		/// <param name="targetType">The ISqlPersistable class type being queried.</param>
		/// <returns>A DataSet object containing the query results.</returns>
		public DataSet GetDataSet( Type targetType, string sql, params DbParameter[] parms ) {
			SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
			string tableName = tableAttribute.TableName;
			return ( this.GetDataSet( tableName, sql, parms ) );
		}

		#endregion

		#region Static ISqlPersistable Utilities

		/// <summary>
		/// Given a type properly decorated with SqlTableAttribute, return the SqlTableAttribute instance.
		/// </summary>
		/// <param name="targetType">A Type instance identifying the type to inspect - typeof(Foo).</param>
		/// <returns>The instance of SqlTableAttribute attached to the type.</returns>
		public static SqlTableAttribute GetTableAttribute( Type targetType ) {
			SqlTableAttribute tableAttribute = (SqlTableAttribute) Attribute.GetCustomAttribute(
				targetType, typeof( SqlTableAttribute )
			);
			// Make sure that the target class was properly decorated...
			if ( tableAttribute == null ) {
				throw new Exception( string.Format(
					"Class {0} requires a SqlTableAttribute to identify its table in the database",
					targetType.FullName
				) );
			}
			return ( tableAttribute );
		}

		/// <summary>
		/// Given a properly decorated type, return the associated SQL table name. 
		/// </summary>
		/// <param name="targetType">A Type instance identifying the type to inspect - typeof(Foo).</param>
		/// <returns>The name of the SQL database table associated with this type.</returns>
		public static string GetTableName( Type targetType ) {
			return ( GetTableAttribute( targetType ).TableName );
		}

		/// <summary>
		/// Truncates the given char/string type value to the specified max length
		/// </summary>
		/// <param name="dataValue">The value who's length is to be checked</param>
		/// <param name="dataType">The column's specific type</param>
		/// <param name="maxLength">The maximum length in chars</param>
		/// <returns>The data-value, truncated if necessary and appropriate</returns>
		protected static object ConvertSqlValue( object dataValue, DbType dataType, int maxLength ) {
			switch ( dataType ) {
				case DbType.AnsiStringFixedLength:
				case DbType.StringFixedLength:
				case DbType.AnsiString:
				case DbType.String:
					if ( maxLength <= 0 )
						return ( dataValue );
					// For string values, make sure we're trimmed and not exceeding the field size
					if ( dataValue == null )
						return ( "" );
					string val = dataValue.ToString().Trim();
					if ( val.Length > maxLength )
						val = val.Substring( 0, maxLength );
					return ( val );
				case DbType.Date:
				case DbType.DateTime:
					// For DateTime values, consider a value of DateTime.MinValue to be a null
					if ( dataValue is DateTime ) {
						if ( ( (DateTime) dataValue ) == DateTime.MinValue )
							return ( DBNull.Value );
						return ( dataValue );
					}
					return ( null );
				default:
					if ( dataValue == null )
						return ( DBNull.Value );
					return ( dataValue );
			}
		}

		/// <summary>
		/// Returns a comma-delimited list of SqlColumn field-names for the given ISqlPersistable instance
		/// </summary>
		/// <param name="target">ISqlPersistable instance</param>
		/// <returns>A comma-delimited list of SqlColumn field-names for the given ISqlPersistable instance</returns>
		public static string GetFieldList( Type targetType ) {
			StringBuilder sqlColumnNames = new StringBuilder();
			// Get a list of all instance-properties
			PropertyInfo[] properties = targetType.GetProperties(
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
			);
			string delim = "";
			foreach ( PropertyInfo property in properties ) {
				SqlColumnAttribute columnAttribute = (SqlColumnAttribute)
					Attribute.GetCustomAttribute( property, typeof( SqlColumnAttribute ) );
				if ( columnAttribute != null && property.CanRead ) { // Make sure we can read it's value 
					string columnName = columnAttribute.ColumnName;
					sqlColumnNames.AppendFormat( "{0}{1}", delim, columnName );
					delim = ", ";
				}
			}
			if ( string.IsNullOrEmpty( delim ) ) {
				// This means we didn't find any decorated properties
				throw new Exception( string.Format(
					"Class {0} requires at least one readable property decorated with SqlColumnAttribute.", targetType.FullName
				) );
			}
			// Add the "is_deleted" column for tables that use it...
			SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
			if ( tableAttribute.IsMarkedDeleted ) {
				sqlColumnNames.AppendFormat( "{0}{1}", delim, tableAttribute.IsDeletedColumnName );
			}
			return ( sqlColumnNames.ToString() );
		}

		#endregion

		#region Static ISqlPersistable SQL-statement methods

		/// <summary>
		/// Creates a complete SQL INSERT command-object for inserting a new ISqlPersistable instance.
		/// </summary>
		/// <param name="target">Instance of the ISqlPersistable class to be INSERTed.</param>
		/// <returns>A DbCommand object configured to persist the given instance to the database.</returns>
		public DbCommand BuildInsertCommand( ISqlPersistable target ) {
			DbCommand cmd = this.NewCommand();
			Type targetType = target.GetType();
			// Get the table name (and verify that the target class was decorated with SqlTableAttribute)
			SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
			string tableName = tableAttribute.TableName;
			string idName = tableAttribute.IDColumnName;
			// Build two separate lists of column-names and values...
			StringBuilder sqlColumnNames = new StringBuilder();
			StringBuilder sqlColumnValues = new StringBuilder();
			// This variable holds the ", " delimiter between column-names and values in the sql statement
			string delim = "";
			// Get a list of all instance-properties
			PropertyInfo[] properties = targetType.GetProperties(
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
			);
			foreach ( PropertyInfo property in properties ) {
				SqlColumnAttribute columnAttribute = (SqlColumnAttribute)
					Attribute.GetCustomAttribute( property, typeof( SqlColumnAttribute ) );
				if ( columnAttribute != null && property.CanRead ) { // Make sure we can read it's value 
					if ( !columnAttribute.PrimaryKey ) { // Skip the primary key 
						// Add the column name & parameter-placeholder to the respective lists
						string columnName = columnAttribute.ColumnName;
						sqlColumnNames.AppendFormat( "{0}{1}", delim, columnName );
						sqlColumnValues.AppendFormat( "{0}@{1}", delim, columnName );
						// Create a DbParameter instance for this column's value
						int dataSize = columnAttribute.Length;
						DbParameter parm = this.NewParameter();
						parm.ParameterName = "@" + columnName;
						parm.DbType = columnAttribute.DataType;
						if ( dataSize > 0 ) {
							parm.Size = columnAttribute.Length;
						}
						object val = property.GetValue( target, null );
						parm.Value = ConvertSqlValue( val, columnAttribute.DataType, dataSize );
						cmd.Parameters.Add( parm );
						delim = ", ";
					}
				}
			}
			if ( string.IsNullOrEmpty( delim ) ) {
				// This means we didn't find any decorated properties
				throw new Exception( string.Format(
					"Class {0} requires at least one readable property decorated with SqlColumnAttribute.",
					targetType.FullName )
					);
			}
			// Put the final INSERT statement together
			StringBuilder sql = new StringBuilder();
			sql.Append( "SET NOCOUNT ON; " );
			sql.AppendFormat(
				"INSERT INTO {0} ( {1} ) VALUES ( {2} ); ",
				tableName, sqlColumnNames, sqlColumnValues
			);
			sql.Append( "SELECT @@IDENTITY AS ID" );
			cmd.CommandText = sql.ToString();
			cmd.CommandType = CommandType.Text;
			return ( cmd );
		}

		/// <summary>
		/// Creates a complete SQL UPDATE command-object for updating an ISqlPersistable instance.
		/// </summary>
		/// <param name="target">Instance of the ISqlPersistable class to be updated.</param>
		/// <returns>A DbCommand object configured to persist the given instance to the database.</returns>
		public DbCommand BuildUpdateCommand( ISqlPersistable target ) {
			DbCommand cmd = this.NewCommand();
			Type targetType = target.GetType();
			// Get the table name (and verify that the target class was decorated with SqlTableAttribute)
			SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
			string tableName = tableAttribute.TableName;
			string idName = tableAttribute.IDColumnName;
			// Build two separate lists of column-names and values...
			StringBuilder sqlColumnExprs = new StringBuilder();
			// This variable holds the ", " delimiter between column-names and values in the sql statement
			string delim = "";
			// Get a list of all instance-properties
			PropertyInfo[] properties = targetType.GetProperties(
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
			);
			// Build the list of SET expressions
			foreach ( PropertyInfo property in properties ) {
				SqlColumnAttribute columnAttribute = (SqlColumnAttribute)
					Attribute.GetCustomAttribute( property, typeof( SqlColumnAttribute ) );
				if ( columnAttribute != null && property.CanRead ) { // Make sure we can read it's value 
					if ( !columnAttribute.PrimaryKey ) { // Skip the primary key 
						// Add the expression for this property
						string columnName = columnAttribute.ColumnName;
						sqlColumnExprs.AppendFormat( "{0}{1} = @{1}", delim, columnName );
						// Create a DbParameter for this property's value
						int dataSize = columnAttribute.Length;
						DbParameter parm = this.NewParameter();
						parm.ParameterName = "@" + columnName;
						parm.DbType = columnAttribute.DataType;
						if ( dataSize > 0 ) {
							parm.Size = columnAttribute.Length;
						}
						object val = property.GetValue( target, null );
						parm.Value = ConvertSqlValue( val, columnAttribute.DataType, dataSize );
						cmd.Parameters.Add( parm );
						delim = ", ";
					}
				}
			}
			if ( string.IsNullOrEmpty( delim ) ) {
				// This means we didn't find any decorated properties
				throw new Exception( string.Format(
					"Class {0} requires at least one readable property decorated with SqlColumnAttribute.",
					targetType.FullName
				) );
			}
			// Put the complete statement together...
			StringBuilder sql = new StringBuilder();
			sql.Append( "set nocount on; " );
			sql.AppendFormat( "update {0} set {2} where ( {1} = {3} ); ", tableName, idName, sqlColumnExprs, target.ID );
			cmd.CommandText = sql.ToString();
			cmd.CommandType = CommandType.Text;
			return ( cmd );
		}

		/// <summary>
		///	Initializes the target instance using the next read from the specified SqlDataReader.
		/// </summary>
		/// <param name="rdr">
		///	A SqlDataReader instance already connected to the database, ready to read
		///	the DataRow representing the instance we want to populate.
		/// </param>
		/// <param name="target">
		///	An ISqlPersistable instance whose properties are to be initialized.
		/// </param>
		/// <returns>
		///	True if the read was successful.
		/// </returns>
		public static bool InstantiateFromDataRdr( IDataReader rdr, ISqlPersistable target ) {
			if ( rdr.Read() ) {
				Type targetType = target.GetType();
				// Get the SqlTableAttribute (just to verify that our instance is properly decorated)
				SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
				string idName = tableAttribute.IDColumnName;
				// See if this record is marked for deletion first
				if ( tableAttribute.IsMarkedDeleted ) {
					// Skip over any 'deleted" records
					while ( true ) {
						if ( rdr.GetBoolean( rdr.GetOrdinal( tableAttribute.IsDeletedColumnName ) ) ) {
							// This record has been deleted - move ahead to the next record
							if ( !rdr.Read() ) {
								return ( false ); // no more records - we're done
							}
						} else {
							break;
						}
					}
				}
				// Keep track of whether or not we find any SqlColumnAttributes
				bool found = false;
				// Get a list of all instance properties
				PropertyInfo[] properties = targetType.GetProperties(
					BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
				);
				foreach ( PropertyInfo property in properties ) {
					// Get the SqlColumnAttribute associated with this property (if any)
					SqlColumnAttribute columnAttribute = (SqlColumnAttribute)
						Attribute.GetCustomAttribute( property, typeof( SqlColumnAttribute ) );
					if ( columnAttribute != null && property.CanRead ) { // Make sure we can read it's value 
						object propertyValue = null;
						// Get the reader's index for the column we want
						int ordinal;
						if ( columnAttribute.PrimaryKey ) {
							ordinal = rdr.GetOrdinal( idName );
						} else {
							ordinal = rdr.GetOrdinal( columnAttribute.ColumnName );
						}
						if ( !rdr.IsDBNull( ordinal ) ) { // Skip null values
							// Assign the value (let the PropertyInfo object do the type conversion)
							propertyValue = rdr.GetValue( ordinal );
							property.SetValue( target, propertyValue, null );
						}
						found = true; // We found a property with a SqlColumnAttribute decoration
					}
				}
				if ( !found ) {
					// This means we didn't find any decorated properties
					throw new Exception( string.Format(
						"Class {0} requires at least one readable property decorated with SqlColumnAttribute.",
						targetType.FullName
					) );
				}
				return ( true ); // We were able to read from the SqlDataReader
			}
			target.ID = -1;
			return ( false ); // We were NOT able to read from the SqlDataReader
		}

		public static bool InstantiateFromDataRow( DataRow row, ISqlPersistable target ) {
			Type targetType = target.GetType();
			// Get the SqlTableAttribute (just to verify that our instance is properly decorated)
			SqlTableAttribute tableAttribute = GetTableAttribute( targetType );
			string idName = tableAttribute.IDColumnName;
			// See if this record is marked for deletion first
			if ( tableAttribute.IsMarkedDeleted ) {
				// Skip over this record if it's marked "deleted"
				if ( (bool) row[tableAttribute.IsDeletedColumnName] ) {
					return ( false );
				}
			}
			// Keep track of whether or not we find any SqlColumnAttributes
			bool found = false;
			// Get a list of all instance properties
			PropertyInfo[] properties = targetType.GetProperties(
				BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
			);
			foreach ( PropertyInfo property in properties ) {
				// Get the SqlColumnAttribute associated with this property (if any)
				SqlColumnAttribute columnAttribute = (SqlColumnAttribute) Attribute.GetCustomAttribute( property, typeof( SqlColumnAttribute ) );
				if ( columnAttribute != null && property.CanRead ) { // Make sure we can read it's value 
					object propertyValue = null;
					if ( !row.IsNull( columnAttribute.ColumnName ) ) { // Skip null values
						// Assign the value (let the PropertyInfo object do the type conversion)
						propertyValue = row[columnAttribute.ColumnName];
						property.SetValue( target, propertyValue, null );
					}
					found = true; // We found a property with a SqlColumnAttribute decoration
				}
			}
			if ( !found ) {
				// This means we didn't find any decorated properties
				throw new Exception( string.Format(
					"Class {0} requires at least one readable property decorated with SqlColumnAttribute.",
					targetType.FullName
				) );
			}
			return ( true ); // We were able to read from the SqlDataReader
		}

		/// <summary>
		/// Performs a SQL query and return the result as a List of restored ISqlPersistable instances
		/// </summary>
		/// <typeparam name="T">An ISqlPersistable class</typeparam>
		/// <param name="sql">SQL Search query</param>
		/// <param name="parms">one or more DbParameters for the query</param>
		/// <returns>A typed List of restored instances</returns>
		public static List<T> DoSearch<T>( string sql, params DbParameter[] parms ) where T : ISqlPersistable, new() {
			return ( DoSearch<T>( 0, -1, sql, parms ) );
		}
		/// <summary>
		/// Performs a SQL query and return the result as a List of restored ISqlPersistable instances
		/// </summary>
		/// <typeparam name="T">An ISqlPersistable class</typeparam>
		/// <param name="firstItem">Index of the first search result item to include</param>
		/// <param name="maxItems">maximum number of result items to include</param>
		/// <param name="sql">SQL Search query</param>
		/// <param name="parms">one or more DbParameters for the query</param>
		/// <returns>A typed List of restored instances</returns>
		public static List<T> DoSearch<T>( int firstItem, int maxItems, string sql, params DbParameter[] parms ) where T : ISqlPersistable, new() {
			List<T> results = new List<T>();
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, parms ) ) {
					for ( int i = 0 ; i < firstItem ; i++ ) {
						if ( !rdr.Read() ) {
							return ( results );
						}
					}
					T item = new T();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						results.Add( item );
						if ( maxItems > 0 && results.Count >= maxItems ) {
							break;
						}
						item = new T();
					}
				}
			}
			return ( results );
		}

		#endregion
	}

}
