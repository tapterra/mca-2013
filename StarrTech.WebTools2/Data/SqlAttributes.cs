using System;
using System.Data;

namespace StarrTech.WebTools.Data {
	/// <summary>
	/// Used to associate a class with a specific database table and entity-specific labels.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class SqlTableAttribute : Attribute {
		
		#region Properties
		
		/// <summary>
		/// Defines the name of the database table used to persist this entity.
		/// </summary>
		public string TableName {
			get { return ( this.tablename ); }
			set { this.tablename = value; }
		}
		private string tablename = "";

		/// <summary>
		/// Used to indicate that when a record is "deleted", it should set a bit-field is_deleted to 1 
		/// instead of actually deleting the record. Search methods should usually exclude records so marked.
		/// </summary>
		public bool IsMarkedDeleted {
			get { return ( this.isMarkedDeleted ); }
			set { this.isMarkedDeleted = value; }
		}
		private bool isMarkedDeleted = false;

		/// <summary>
		/// Name of the primary-key identity column
		/// </summary>
		public string IDColumnName {
			get { return ( this.idColumnName ); }
			set { this.idColumnName = value; }
		}
		private string idColumnName = "id";

		/// <summary>
		/// Name of the marked-as-deleted bit column
		/// </summary>
		public string IsDeletedColumnName {
			get { return ( this.isDeletedColumnName ); }
			set { this.isDeletedColumnName = value; }
		}
		private string isDeletedColumnName = "is_deleted";

		/// <summary>
		/// Label for a single instance of this entity (User, Sale).
		/// </summary>
		public string SingularLabel {
			get { return ( this.singularLabel ); }
			set { this.singularLabel = value; }
		}
		private string singularLabel = "";

		/// <summary>
		/// Label for a group of multiple instances of this entity (Users, Sales).
		/// </summary>
		public string PluralLabel {
			get { return ( this.pluralLabel ); }
			set { this.pluralLabel = value; }
		}
		private string pluralLabel = "";

		#endregion
		
		#region Constructors
		
		public SqlTableAttribute( string tableName ) {
			this.TableName = tableName;
			// Defaults
			this.SingularLabel = tableName;
			this.PluralLabel = tableName + "s";
		}
		
		#endregion
	}
	
	
	/// <summary>
	/// Used to associate a class-property with a specific column in a database table 
	/// (which was presumeably identified for the class via SqlTableAttribute).
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class SqlColumnAttribute : Attribute {
		#region Properties
		/// <summary>
		/// Defines the name of the database column associated with this attribute.
		/// </summary>
		public string ColumnName {
			get { return ( this.columnname ); }
			set { this.columnname = value; }
		}
		private string columnname = "";
		
		/// <summary>
		/// Defines the SQL DataType for this property's column (defaults to NVarChar).
		/// </summary>
		public DbType DataType {
			get { return ( this.datatype ); }
			set { this.datatype = value; }
		}
		private DbType datatype = DbType.String;
		
		/// <summary>
		/// Identifies the column as a primary key for it's table (defaults to false).
		/// </summary>
		public bool PrimaryKey {
			get { return ( this.primarykey ); }
			set { this.primarykey = value; }
		}
		private bool primarykey = false;
		
		/// <summary>
		/// Defines the data length for the column.
		/// </summary>
		public int Length {
			get { return ( this.length ); }
			set { this.length = value; }
		}
		private int length = -1;
		
		#endregion
		
		#region Constructor
		
		public SqlColumnAttribute( string columnName, DbType dataType ) {
			this.ColumnName = columnName;
			this.DataType = dataType;
		}
		
		#endregion
	}
	
	
}
