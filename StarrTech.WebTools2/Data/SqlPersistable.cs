using System;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Text;
using System.Web;

namespace StarrTech.WebTools.Data {

	/// <summary>
	/// Marker interface for objects that SqlDatabase can automatically persist
	/// (as long as they also use the SqlAttributes).
	/// </summary>
	public interface ISqlPersistable {
		int ID { get; set; }
	}
		
	/// <summary>
	/// Base class that implements ISqlPersistable. 
	/// </summary>
	public abstract class BaseSqlPersistable : ISqlPersistable {

		#region Properties
		
		/// <summary>
		/// Represents the record ID column, an int, identity, primary-key of the database table.
		/// </summary>
		[SqlColumn( "id", DbType.Int32, PrimaryKey = true )]
		public int ID {
			get { return ( id ); }
			set { id = value; }
		}
		private int id = -1;

		/// <summary>
		/// Returns the SqlTable attribute's Name
		/// </summary>
		protected string TableName {
			get { return ( SqlDatabase.GetTableName( this.GetType() ) ); }
		}

		/// <summary>
		/// Collects error messages during validation operations
		/// </summary>
		public ArrayList ValidationMessages {
			get { return ( this.validationMessages ); }
		}
		private ArrayList validationMessages = new ArrayList();

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Used to construct a new, unpersisted instance.
		/// </summary>
		public BaseSqlPersistable() {
			this.ID = -1;
		}
		
		/// <summary>
		/// Used to restore an existing instance from the database.
		/// </summary>
		/// <param name="id">Record-ID for the instance to restore.</param>
		public BaseSqlPersistable( int id ) : this() {
			this.ID = id;
			this.Restore();
		}
		
		/// <summary>
		/// Used to restore an existing instance from the database via the next read from an IDataReader.
		/// </summary>
		/// <param name="rdr">
		///	A IDataReader instance already connected to the database, ready to read  
		///	the DataRow representing the instance we want to populate.
		/// </param>
		public BaseSqlPersistable( IDataReader rdr ) : this() {
			SqlDatabase.InstantiateFromDataRdr( rdr, this );
		}
		
		#endregion
		
		#region Database Methods
		
		/// <summary>
		/// Restores this instance from it's database record.
		/// </summary>
		public virtual void Restore() {
			if ( this.ID <= 0 ) return;
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.RestoreInstance( this );
			}
		}
		
		/// <summary>
		/// Allows subclasses to validate against the database before an insert or update.
		/// Overrides should call the base method to get the ball rolling.
		/// </summary>
		/// <returns>True if it's OK to persist; if not, one or more error messages should be 
		/// added to ValidationMessages to explain why.</returns>
		public virtual bool IsValidToPersist() {
			this.ValidationMessages.Clear();
			return ( true );
		}
		
		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public virtual bool Persist() {
			bool isValid = this.IsValidToPersist();
			if ( isValid ) { 
				using ( SqlDatabase db = new SqlDatabase() ) {
					db.PersistInstance( this );
				}
			}
			return ( isValid );
		}
		
		/// <summary>
		/// Allows subclasses to validate against the database before a delete.
		/// Overrides should call the base method to get the ball rolling.
		/// </summary>
		/// <returns>True if it's OK to persist; if not, one or more error messages should be 
		/// added to ValidationMessages to explain why.</returns>
		public virtual bool IsValidToDelete() {
			this.ValidationMessages.Clear();
			return ( true );
		}
		
		/// <summary>
		/// Removes the record for this instance (ID) from the database
		/// </summary>
		public virtual bool Delete() {
			bool isValid = this.IsValidToDelete();
			if ( isValid ) {
				using ( SqlDatabase db = new SqlDatabase() ) {
					db.DeleteInstance( this );
				}
			}
			return ( isValid );
		}
		
		#endregion
		
		#region Static Search methods
		
		/// <summary>
		/// Makes it a little easier for subclasses to build search expressions.
		/// </summary>
		/// <param name="sql">StringBuilder being used to accumulate the SQL WHERE clause expressions.</param>
		/// <param name="fname">Column name being compared against for this expression.</param>
		/// <param name="fvalue">Data value being compared for this expression.</param>
		/// <param name="delim">Conjunctive operand ('AND ', 'OR ', '') used to join this expression with 
		/// the previous ones.</param>
		protected static void AddSearchExpr( StringBuilder sql, string fname, string fvalue, ref string delim ) {
			if ( !string.IsNullOrEmpty( fvalue ) ) {
				fvalue = fvalue.Replace( "*", "%" );
				if ( fvalue.IndexOf( "%" ) < 0 ) fvalue = string.Format( "%{0}%", fvalue );
				sql.AppendFormat( "{0} ( {1} LIKE '{2}' ) ", delim, fname, fvalue.Trim() );
				delim = "AND ";
			}
		}
		
		protected static void AddSearchExpr( StringBuilder sql, string fname, int fvalue, ref string delim ) {
			if ( fvalue > 0 ) {
				sql.AppendFormat( "{2} ( {0} = {1} ) ", fname, fvalue, delim );
				delim = "AND ";
			}
		}
		
		protected static void AddSearchExpr( StringBuilder sql, string fname, bool fvalue, ref string delim ) {
			sql.AppendFormat( "{2} ( {0} = {1} ) ", fname, fvalue ? 1 : 0, delim );
			delim = "AND ";
		}
		
		protected static void AddSearchExpr( StringBuilder sql, string fname, DateTime fvalue, ref string delim ) {
			if ( fvalue > DateTime.MinValue && fvalue < DateTime.MaxValue ) {
				sql.AppendFormat( "{2} ( {0} = '{1}' ) ", fname, fvalue, delim );
				delim = "AND ";
			}
		}
		
		protected static void AddSearchExpr( StringBuilder sql, string fname, DateTime startValue, DateTime endValue, ref string delim ) {
			bool useStart = startValue > DateTime.MinValue;
			bool useEnd   = endValue < DateTime.MaxValue;
			if ( useStart || useEnd ) {
				if ( useStart && useEnd ) {
					sql.AppendFormat( "{3} ( {0} BETWEEN '{1}' AND '{2}' ) ", fname, startValue, endValue, delim );
				}
				else if ( useStart ) {
					sql.AppendFormat( "{2} ( {0} >= '{1}' ) ", fname, startValue, delim );
				}
				else if ( useEnd ) {
					sql.AppendFormat( "{2} ( {0} <= '{1}' ) ", fname, endValue, delim );
				}
				delim = "AND ";
			}
		}

		protected static void AddSearchExpr( StringBuilder sql, string startfname, string endfname, DateTime startValue, DateTime endValue, ref string delim ) {
			if ( string.IsNullOrEmpty( startfname ) ) {
				AddSearchExpr( sql, endfname, startValue, endValue, ref delim );
			}
			if ( string.IsNullOrEmpty( endfname ) ) {
				AddSearchExpr( sql, startfname, startValue, endValue, ref delim );
			}
			bool useStart = startValue > DateTime.MinValue;
			bool useEnd = endValue < DateTime.MaxValue;
			if ( useStart || useEnd ) {
				if ( useStart && useEnd ) {
					sql.AppendFormat( "{4} ( ( {0} BETWEEN '{2}' AND '{3}' ) OR ( {1} BETWEEN '{2}' AND '{3}' ) ) ", startfname, endfname, startValue, endValue, delim );
				} else if ( useStart ) {
					sql.AppendFormat( "{2} ( {0} >= '{1}' ) ", endfname, startValue, delim );
				} else if ( useEnd ) {
					sql.AppendFormat( "{2} ( {0} <= '{1}' ) ", startfname, endValue, delim );
				}
				delim = "AND ";
			}
		}

		public static string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static string sortExpression;
		
		#endregion
		
	}
	
}
