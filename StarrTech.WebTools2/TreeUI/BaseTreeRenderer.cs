using System;
using System.Web.UI;


namespace StarrTech.WebTools.TreeUI
{
	/// <summary>
	/// Base class for the TreeRenderer rendering model.
	/// </summary>
	public abstract class BaseTreeRenderer
	{
		protected BaseTreeRenderer( TreePanel treePanel ) { 
			this.treePanel = treePanel; 
		}

		protected TreePanel TreePanel {
			get { return ( treePanel ); }
		}
		private TreePanel treePanel;

		public abstract void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer );
		
		public abstract void RenderNodeControlEnd( TreeNodeControl node, HtmlTextWriter writer );
		
		public abstract void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer );
		
		public abstract void RenderCheckbox( TreeNodeControl node, HtmlTextWriter writer );
		
		public abstract void RenderNodeControlText( TreeNodeControl node, HtmlTextWriter writer );
		
		public abstract void RenderTreeStart( HtmlTextWriter writer );
		
		public abstract void RenderTreeEnd( HtmlTextWriter writer );
		
	}
	
}
