using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml;


namespace StarrTech.WebTools.TreeUI
{
	/// <summary>
	/// Summary description for XPTreeRenderer.
	/// </summary>
	public class XPTreeRenderer : StandardTreeRenderer {
	
		private bool first = true;
		
		public XPTreeRenderer( TreePanel treePanel ) : base( treePanel ) {}

		public override void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.Write( "<tr><td><nobr>" );
		}

		public override void RenderNodeControlEnd( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.Write( "</nobr></td></tr>" );
		}

		public override void RenderTreeStart( HtmlTextWriter writer ) {
			writer.WriteBeginTag( "table" );
			writer.WriteAttribute( "cellpadding", "0" );
			writer.WriteAttribute( "cellspacing", "0" );
			writer.WriteAttribute( "border", "0" );
			writer.Write( HtmlTextWriter.TagRightChar );
		}
		
		public override void RenderTreeEnd( HtmlTextWriter writer ) {
			writer.WriteEndTag( "table" );
		}
		
		private bool ParentHasSibling( TreeNodeControl source, int indentDiff ) {
			Control ctl = source;
			for ( int i = 0; i < indentDiff; i++ ) {
				ctl = ctl.Parent;
			}
			if ( ctl is TreeNodeControl ) {
				return ( ((TreeNodeControl) ctl).NextSibling() != null );
			} else {
				return ( false );
			}
		}
		private bool IsFirst() {
			if ( first ) {
				this.first = false;
				return ( true );
			}
			return ( false );
		}
		public override void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer ) {
			int indent = node.Indent; //0-based

			StringBuilder sb = new StringBuilder();
			while ( indent > 0 ) { //each indent level means one image
				bool hasSibling, parentHasSibling, isTop;

				hasSibling = node.NextSibling() != null;
				isTop = node.Parent is TreePanel;

				if ( node.Parent is TreeNodeControl )
					parentHasSibling = ParentHasSibling( node, node.Indent - indent );
				else {
					parentHasSibling = false;
				}

				if ( node.Indent == indent ) { //first item in the indent
					if ( node.HasControls() ) { //there are children here
						string anchorStart, anchorEnd;

						anchorStart = "<a href=\"" + this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID ) + "\">";
						anchorEnd = "</a>";
						
						if ( hasSibling ) { //down dots
							if ( node.IsExpanded ) { //minus image
								if ( IsFirst() ) {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "topexpandedsibling.gif' border='0'>" + anchorEnd );
								} else {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "middleexpandedsibling.gif' border='0'>" + anchorEnd );
								}
							} else { //plus image
								if ( IsFirst() ) {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "topcollapsedsibling.gif' border='0'>" + anchorEnd );
								} else {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "middlecollapsedsibling.gif' border='0'>" + anchorEnd );
								}
							}
						} else { //no down dots
							if ( node.IsExpanded ) { //minus image
								if ( IsFirst() ) {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "topexpandednosibling.gif' border='0'>" + anchorEnd );
								} else {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "middleexpandednosibling.gif' border='0'>" + anchorEnd );
								}
							} else { //plus image
								if ( IsFirst() ) {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "topcollapsednosibling.gif' border='0'>" + anchorEnd );
								} else {
									sb.Insert( 0, anchorStart + "<img align='top' src='" + this.TreePanel.ImageFolderPath + "middlecollapsednosibling.gif' border='0'>" + anchorEnd );
								}
							}
						}
					} else { // no children
						if ( hasSibling ) {
							sb.Insert( 0, "<img align='top' src='" + this.TreePanel.ImageFolderPath + "middlesiblingnochildren.gif' border='0'>" );
						} else {
							sb.Insert( 0, "<img align='top' src='" + this.TreePanel.ImageFolderPath + "bottomnosiblingnochildren.gif' border='0'>" );
						}
					}
				} else { // prior item in the indent
					if ( parentHasSibling ) {
						sb.Insert( 0, "<img align='top' src='" + this.TreePanel.ImageFolderPath + "vertbardots.gif' border='0'>" );
					} else {
						sb.Insert( 0, "<img align='top' src='" + this.TreePanel.ImageFolderPath + "clear.gif' border='0'>" );
					}
				}
				indent--;
			}
			writer.Write( sb.ToString() );
		}
		
		public override void RenderNodeControlText( TreeNodeControl node, HtmlTextWriter writer ) {
			bool useLink = ( node.Controls.Count == 0 ) && ( this.TreePanel.NodeDisplayStyle == NodeDisplayStyle.LeafNodesNoLink );
			if ( ! useLink ) {
				//name the anchor, in case you need to jump
				writer.Write( "<a name='" + node.Key + "'>&nbsp;</a>" );
				writer.WriteBeginTag( "a" );
				writer.WriteAttribute( "href", this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID ), false );
				writer.WriteAttribute( "class", this.TreePanel.CssClass );
				writer.Write( HtmlTextWriter.TagRightChar );

				if ( node.IsExpanded ) {
					writer.Write( "<img src='" + this.TreePanel.ImageFolderPath + "openfolder.gif' border='0'>" );
				} else {
					writer.Write( "<img src='" + this.TreePanel.ImageFolderPath + "closedfolder.gif' border='0'>" );
				}
			}
			writer.Write( "&nbsp;" );
			writer.Write( node.Title );
			if ( ! useLink ) {
				writer.WriteEndTag( "a" );
			}
		}
	}
}
