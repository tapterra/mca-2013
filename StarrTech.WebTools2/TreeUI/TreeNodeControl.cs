using System;
using System.Drawing;
using System.Reflection;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace StarrTech.WebTools.TreeUI {

	public sealed class TreeNodeControl : System.Web.UI.Control, INamingContainer {
		
		#region Public Properties
		public string this[string key] {
			get { return ( Attributes[key] ); }
			set { Attributes[key] = value; }
		}
		
		public NameValueCollection Attributes {
			get { return ( this.attributes ); }
		}
		private NameValueCollection attributes = new NameValueCollection();
		
		public string Key {
			get { return ( this.ID ); }
			set { this.ID = value; }
		}
		
		public bool IsExpanded {
			get { return ( ViewState["IsExpanded"] == null ? false : Convert.ToBoolean( ViewState["IsExpanded"] ) ); }
			set { ViewState["IsExpanded"] = value; }
		}
		
		/// <summary>
		/// Returns false if any of the node's ancestors are collapsed 
		/// </summary>
		public bool IsVisible {
			get {
				// If we're a top-level node, we're always visible
				if ( this.Parent is TreePanel ) return ( true );
				// Ask our parent if it's expanded
				TreeNodeControl parent = this.Parent as TreeNodeControl;
				if ( ! parent.IsExpanded ) return ( false );
				return ( parent.IsVisible );
			}
		}
		
		public bool IsChecked {
			get { return ( ViewState["IsChecked"] == null ? false : Convert.ToBoolean( ViewState["IsChecked"] ) ); }
			set { ViewState["IsChecked"] = value; }
		}
		
		public string Title {
			get { return ( title ); }
			set { title = value; }
		}
		private string title;
		
		#endregion
		
		#region Non-Public Properties
		private TreePanel ParentTreePanel {
			get {
				if ( this.parentTree == null ) {
					TreeNodeControl current = this;
					// Walk up the tree till we find the parent TreePanel...
					while ( ! ( current.Parent is TreePanel ) ) {
						current = (TreeNodeControl) current.Parent;
					}
					this.parentTree = (TreePanel) current.Parent;
				}
				return ( this.parentTree );
			}
		}
		private TreePanel parentTree;
		
		internal bool ShowCheckBox {
			get { return ( this.showCheckBox ); }
		}
		private bool showCheckBox;
		
		internal int Indent {
			get { 
				// Count and return the number of ancestor-generations this node has
				if ( ! (this.Parent is TreeNodeControl) ) return ( 0 );
				return ( (this.Parent as TreeNodeControl).Indent + 1 );
			}
		}
		
		internal bool IsFolder {
			get { return ( this.Controls.Count > 0 ); }
		}
		
		#endregion
		
		#region Constructors 
		
		internal TreeNodeControl( string title, string id, bool showCheckbox ) : base() {
			this.title = title;
			this.Key = id;
			this.showCheckBox = showCheckbox;
		}
		
		internal TreeNodeControl( string title, string id ) : this( title, id, false ) {}
		
		#endregion
		
		#region Public Methods
		
		/// <summary>
		/// Adds a tagged value to this node (used for tabular layout with headers
		/// </summary>
		/// <param name="tagName"></param>
		/// <param name="value"></param>
		public void AddTaggedValue( string tagName, string value ) {
			this.attributes.Add( tagName, value);
		}
		
		/// <summary>
		/// Find the next TreeNodeControl
		/// </summary>
		/// <returns>The next sibling TreeNodeControl, or null if there are no following TreeNodeControl siblings.</returns>
		public TreeNodeControl NextSibling() {
			try {
				int index = this.Parent.Controls.IndexOf( this );
				if ( this.Parent.Controls[index + 1] == null || ! ( this.Parent.Controls[index + 1] is TreeNodeControl ) ) {
					return ( null );
				} else {
					return ( (TreeNodeControl) this.Parent.Controls[index + 1] );
				}
			} catch ( ArgumentException ) { 
				return ( null ); 
			}
		}
		
		/// <summary>
		/// Find the previous TreeNodeControl
		/// </summary>
		/// <returns>The previous sibling TreeNodeControl, or null if there are no previous TreeNodeControl siblings.</returns>
		public TreeNodeControl PrevSibling() {
			try {
				int index = this.Parent.Controls.IndexOf( this );
				if ( index <= 0 ) return ( null );
				if ( this.Parent.Controls[index - 1] == null || ! ( this.Parent.Controls[index - 1] is TreeNodeControl ) ) {
					return ( null );
				} else {
					return ( (TreeNodeControl) this.Parent.Controls[index - 1] );
				}
			} catch ( ArgumentException ) { 
				return ( null ); 
			}
		}
		
		/// <summary>
		/// Expand the whole hierarchy of nodes.
		/// </summary>
		public void ExpandAll() {
			this.IsExpanded = true;
			foreach ( TreeNodeControl n in this.Controls ) {
				n.ExpandAll();
			}
		}
		
		/// <summary>
		/// Collapse the whole hierarchy of nodes.
		/// </summary>
		public void CollapseAll() {
			this.IsExpanded = false;
			foreach ( TreeNodeControl n in this.Controls ) {
				n.CollapseAll();
			}
		}
		
		/// <summary>
		/// Add a new TreeNodeControl child to this node.
		/// </summary>
		/// <param name="title"></param>
		/// <param name="key"></param>
		/// <param name="showCheckbox"></param>
		/// <returns></returns>
		public TreeNodeControl AddNode( string title, string key, bool showCheckbox ) {
			TreeNodeControl node = new TreeNodeControl( title, key, showCheckbox );
			this.AddNode( node );
			return ( node );
		}
		
		/// <summary>
		/// Add a new TreeNodeControl child to this node.
		/// </summary>
		/// <param name="title"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public TreeNodeControl AddNode( string title, string key ) {
			return ( this.AddNode( title, key, false ) );
		}
		
		/// <summary>
		/// Add an existing TreeNodeControl to this node
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public void AddNode( TreeNodeControl NodeToAdd ) {
			this.Controls.Add( NodeToAdd );
		}
		
		public TreeNodeControl AddNodeAfter( string title, string key, bool showCheckbox, TreeNodeControl AfterNode ) {
			TreeNodeControl node = new TreeNodeControl( title, key, showCheckbox );
			this.AddNodeAfter( node, AfterNode );
			return ( node );
		}
		
		public TreeNodeControl AddNodeAfter( TreeNodeControl NodeToAdd, TreeNodeControl AfterNode ) {
			this.Controls.AddAt( this.Controls.IndexOf( AfterNode ) + 1, NodeToAdd );
			return ( NodeToAdd );
		}
		
		public void MoveNodeLeft( TreeNodeControl NodeToMove ) {
			// Remove the node from my collection of controls...
			this.Controls.Remove( NodeToMove );
			// And insert it into my parent's, right after me
			int index = this.Parent.Controls.IndexOf( this );
			this.Parent.Controls.AddAt( ++index, NodeToMove );
		}
		
		public void MoveNodeRight( TreeNodeControl NodeToMove ) {
			TreeNodeControl prevNode = NodeToMove.PrevSibling();
			// If this node's at the top of it's sibling list, it can't go right!
			if ( prevNode == null ) return;
			// Remove the node from my collection of controls...
			this.Controls.Remove( NodeToMove );
			// And give it to it's most immediately-preceeding sibling...
			prevNode.Controls.Add( NodeToMove );
			prevNode.IsExpanded = true;
		}
		
		public void MoveNodeUp( TreeNodeControl NodeToMove ) {
			int index = this.Controls.IndexOf( NodeToMove );
			// If index is zero, we're already at the top of the list
			if ( index <= 0 ) return;
			this.Controls.Remove( NodeToMove );
			this.Controls.AddAt( --index, NodeToMove );
		}
		
		public void MoveNodeDown( TreeNodeControl NodeToMove ) {
			int index = this.Controls.IndexOf( NodeToMove );
			// If index is > the total number of children, we're already at the bottom of the list
			if ( index > this.Controls.Count ) return;
			this.Controls.Remove( NodeToMove );
			this.Controls.AddAt( ++index, NodeToMove );
		}
		
		/// <summary>
		/// Find a child TreeNodeControl given its key value
		/// </summary>
		/// <param name="key"></param>
		/// <returns>The specified TreeNodeControl or null if not found</returns>
		public TreeNodeControl FindTreeNode( string key ) {
			// Search the top level first
			foreach ( TreeNodeControl n in this.Controls ) {
				if ( n.Key == key ) return ( n );
			}
			// Search each child tree node
			foreach ( TreeNodeControl n in this.Controls ) {
				TreeNodeControl found = n.FindTreeNode( key );
				if ( found != null ) return ( found );
			}
			// No joy...
			return ( null );
		}
		
		/// <summary>
		/// Check the tree node
		/// </summary>
		public void Check() {
			this.Check( ! this.IsChecked );
		}

		/// <summary>
		/// Returns true if this node has Attributes["ismenuitem"] == "true", 
		/// all parents have ["issubmenu"] == "true", and
		/// the oldest (top-most) parent has Attributes["ismenuitem"] == "true"
		/// </summary>
		/// <returns>True if this node represents a visible menu item</returns>
		public bool IsVisibleMenuItem() {
			if ( !this.IsMenuItem() )
				return ( false );
			TreeNodeControl lastParent = null;
			TreeNodeControl parent = this.Parent as TreeNodeControl;
			while ( parent != null ) {
				if ( !parent.IsSubMenu() )
					return ( false );
				lastParent = parent;
				parent = parent.Parent as TreeNodeControl;
			}
			if ( lastParent != null && !lastParent.IsMenuItem() )
				return ( false );
			return ( true );
		}
		internal bool IsMenuItem() {
			return ( this.Attributes["ismenuitem"] == "true" );
		}
		internal bool IsSubMenu() {
			return ( this.Attributes["issubmenu"] == "true" );
		}

		#endregion
		
		#region Non-Public Methods
		
		/// <summary>
		/// Write an XML version of this tree node to the given XmlTextWriter
		/// </summary>
		/// <param name="writer"></param>
		internal void WriteXml( System.Xml.XmlTextWriter writer) {
			// Write the opening element
			writer.WriteStartElement( this.ParentTreePanel.XmlNodeTag );
			// Write out the attributes
			writer.WriteAttributeString( this.ParentTreePanel.XmlTitleAttribute, "", this.Title );
			writer.WriteAttributeString( this.ParentTreePanel.XmlKeyAttribute, "", this.Key );
			writer.WriteAttributeString( this.ParentTreePanel.XmlExpandedAttribute, "", this.IsExpanded.ToString() );
			if ( this.showCheckBox == false ) {
				// Don't even put the checkbox attribute in
			} else {
				// Write the showcheckbox attribute
				writer.WriteAttributeString( this.ParentTreePanel.XmlShowCheckAttribute, "", "True");
				// Write the is checked attribute
				writer.WriteAttributeString( this.ParentTreePanel.XmlCheckedAttribute, "", this.IsChecked.ToString());
			}
			// Write out any tagged values
			for ( int i = 0 ; i < this.Attributes.Count ; i++ ) {
//				writer.WriteStartElement( "taggedValue", "");
//				writer.WriteAttributeString( "", "tagName", "", this.Attributes.Keys[i]);
//				writer.WriteAttributeString( "", "tagValue", "", this.Attributes.GetValues( this.Attributes.Keys[i])[0]);
//				writer.WriteEndElement();
				writer.WriteAttributeString( this.Attributes.Keys[i], "", this.Attributes.GetValues( this.Attributes.Keys[i])[0] );
			}
			// Write out any child TreeNodeControls
			foreach ( TreeNodeControl n in this.Controls ) {
				n.WriteXml( writer );
			}
			writer.WriteEndElement();
		}

		internal void Check( bool isChecked ) {
			this.IsChecked = isChecked;
			if ( this.ParentTreePanel.ForceInheritedChecks ) {
				foreach ( TreeNodeControl n in this.Controls ) {
					n.Check( isChecked );
				}
			}
		}

		internal void TrackCheckedChildren() {
			if ( this.ParentTreePanel.ForceInheritedChecks ) {
				bool hasCheckedChildren = this.HasCheckedChildren();
				this.IsChecked = hasCheckedChildren;
				// Go up through the hierarchy
				if ( this.Parent is TreeNodeControl ) {
					((TreeNodeControl) this.Parent).TrackCheckedChildren();
				}
			}
		}
		
		private bool HasCheckedChildren() {
			foreach ( TreeNodeControl n in this.Controls ) {
				if ( n.IsChecked ) {
					return ( true );
				} else {
					return ( n.HasCheckedChildren() );
				}
			}
			return ( false );
		}
		
		/// <summary> 
		/// Render this control to the output parameter specified.
		/// </summary>
		/// <param name="output"> The HTML writer to write out to </param>
		protected override void Render( HtmlTextWriter output ) {			
			this.ParentTreePanel.Renderer.RenderNodeControlStart( this, output );
			this.ParentTreePanel.Renderer.RenderImageLink( this, output );
			this.ParentTreePanel.Renderer.RenderCheckbox( this, output );
			this.ParentTreePanel.Renderer.RenderNodeControlText( this, output );
			this.ParentTreePanel.Renderer.RenderNodeControlEnd( this, output );
			
			if ( this.IsFolder && this.IsExpanded ) {
				this.RenderChildren( output );
			}
		}
		
		#endregion
	}
	
}
