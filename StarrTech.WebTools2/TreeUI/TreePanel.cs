using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Xml;

using StarrTech.WebTools.CMS;
using StarrTech.WebTools.Data;
using StarrTech.WebTools.Imaging;

namespace StarrTech.WebTools.TreeUI {
	/// <summary>
	/// Summary description for TreePanel.
	/// </summary>
	[
	DefaultEvent( "SelectedNodeChanged" ),
	DefaultProperty( "Title" ), 
	ToolboxData( "<{0}:treepanel runat=server></{0}:treepanel>" )
	]
	public class TreePanel : Control, INamingContainer, IPostBackEventHandler {
		
		#region Private fields
		internal ArrayList headers = new ArrayList();
		
		// Names for the XML tags ...
		internal string XmlRootTag {
			get { return ( this.xmlRootTag ); }
			set { this.xmlRootTag = value; }
		}
		private string xmlRootTag = "tree";
		
		internal string XmlNodeTag {
			get { return ( this.xmlNodeTag ); }
			set { this.xmlNodeTag = value; }
		}
		private string xmlNodeTag = "treenode";
		
		internal string XmlTitleAttribute {
			get { return ( this.xmlTitleAttribute ); }
			set { this.xmlTitleAttribute = value; }
		}
		private string xmlTitleAttribute = "title";
		
		internal string XmlKeyAttribute {
			get { return ( this.xmlKeyAttribute ); }
			set { this.xmlKeyAttribute = value; }
		}
		private string xmlKeyAttribute = "id";
		
		internal string XmlPathKeyAttribute {
			get { return ( this.xmlPathKeyAttribute ); }
			set { this.xmlPathKeyAttribute = value; }
		}
		private string xmlPathKeyAttribute = "id";
		
		internal string XmlExpandedAttribute {
			get { return ( this.xmlExpandedAttribute ); }
			set { this.xmlExpandedAttribute = value; }
		}
		private string xmlExpandedAttribute = "isexpanded";
		
		internal string XmlCheckedAttribute {
			get { return ( this.xmlCheckedAttribute ); }
			set { this.xmlCheckedAttribute = value; }
		}
		private string xmlCheckedAttribute = "checked";
		
		internal string XmlShowCheckAttribute {
			get { return ( this.xmlShowCheckAttribute ); }
			set { this.xmlShowCheckAttribute = value; }
		}
		private string xmlShowCheckAttribute = "showcheck";
		
		#endregion
		
		#region Properties
		/// <summary>
		/// The Width of the TreePanel
		/// </summary>
		public Unit Width {
			get { return ( this.width ); }
			set { this.width = value; }
		}
		private Unit width;
		
		/// <summary>
		/// The Height of the TreePanel
		/// </summary>
		public Unit Height {
			get { return ( this.height ); }
			set { this.height = value; }
		}
		private Unit height;
		
		/// <summary>
		/// The CssClass for the TreeNodeControls
		/// </summary>
		public string CssClass {
			get { return ( this.cssClass ); }
			set { this.cssClass = value; }
		}
		private string cssClass = "";
		
		/// <summary>
		/// The output style of the TreePanel
		/// </summary>
		public TreeOutputStyle TreeOutputStyle {
			get { return ( this.outputStyle ); }
			set {
				this.outputStyle = value;
				switch ( this.outputStyle ) {
					case TreeOutputStyle.Standard:
						this.renderer = new StandardTreeRenderer( this );
						break;
					case TreeOutputStyle.Tabular:
						this.renderer = new TabularTreeRenderer( this );
						break;
					case TreeOutputStyle.WindowsXP:
						this.renderer = new XPTreeRenderer( this );
						break;
					default:
						throw new ArgumentException( "Invalid TreeOutputStyle" );
				}
			}
		}
		private TreeOutputStyle outputStyle = TreeOutputStyle.Standard;
		
		public virtual BaseTreeRenderer Renderer {
			get { return ( this.renderer ); }
		}
		private BaseTreeRenderer renderer;
		
		/// <summary>
		/// How nodes display themselves as links or not links
		/// </summary>
		public NodeDisplayStyle NodeDisplayStyle {
			get { return ( this.nodeDisplayStyle ); }
			set { this.nodeDisplayStyle = value; }
		}
		private NodeDisplayStyle nodeDisplayStyle = NodeDisplayStyle.Standard;
		
		/// <summary>
		/// When set to true, checking a parent causes all its children to check, and checkng a child causes all of its parents to check
		/// </summary>
		public bool ForceInheritedChecks {
			get { return ( forceInheritedChecks ); }
			set { forceInheritedChecks = value; }
		}
		private bool forceInheritedChecks = true;
		
		/// <summary>
		/// The root-resolved URL where the images for the TreePanel will reside.
		/// </summary>
		public string ImageFolderPath {
			get {
				string path = this.ResolveUrl( this.imageFolderPath );
				if ( ! path.EndsWith( "/" ) ) path += "/";
				return ( path );
			}
			set { this.imageFolderPath = value; }
		}
		private string imageFolderPath = "~/images/TreeUI/";
		
		/// <summary>
		/// The image that appears on expanded nodes that contain children
		/// </summary>
		public string ExpandedImage {
			get { return ( this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.expandedImage ) ); }
			set { this.expandedImage = value; }
		}
		private string expandedImage = "StarrTech.WebTools.Resources.treeui.minus.gif";
		
		/// <summary>
		/// The image that appears on collapsed nodes that contain children
		/// </summary>
		public string CollapsedImage {
			get { return ( this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.collapsedImage ) ); }
			set { this.collapsedImage = value; }
		}
		private string collapsedImage = "StarrTech.WebTools.Resources.treeui.plus.gif";
		
		/// <summary>
		/// The image that appears on nodes without children
		/// </summary>
		public string NullImage {
			get { return ( this.Page.ClientScript.GetWebResourceUrl( this.GetType(), this.nullImage ) ); }
			set { this.nullImage = value; }
		}
		private string nullImage = "StarrTech.WebTools.Resources.treeui.null.gif";
		
		/// <summary>
		/// The image that appears on nodes that contain no children
		/// </summary>
		public string NonFolderImage {
			get { return ( this.nonFolderImage ); }
			set { this.nonFolderImage = value; }
		}
		private string nonFolderImage = "~/images/treeui/clear.gif";
		
		/// <summary>
		/// Adds an overflow:auto style to the div tag. This only works with Standard style
		/// </summary>
		public bool Scrolling {
			get { return ( this.scrolling ); }
			set { this.scrolling = value; }
		}
		private bool scrolling = false;
		
		public string SelectedNodeKey {
			get { 
				object key = this.ViewState["SelectedNodeKey"];
				return ( key == null ? "" : (string) key ); 
			}
			set { this.ViewState["SelectedNodeKey"] = value; }
		}
		
		public TreeNodeControl SelectedNode {
			get { 
				if ( this.SelectedNodeKey == "0" ) return ( null );
				return ( this.FindTreeNodeControl( this.SelectedNodeKey ) ); 
			}
		}
		
		#region Tabular formatting properties
		/// <summary>
		/// The horizontal align of the header table
		/// </summary>
		public HorizontalAlign TableHeaderHorizAlign {
			get { return ( this.tableHeaderHorizAlign ); }
			set { this.tableHeaderHorizAlign = value; }
		}
		private HorizontalAlign tableHeaderHorizAlign = HorizontalAlign.Center;
		
		/// <summary>
		/// The back color for the table
		/// </summary>
		public Color TableBackColor {
			get	{ return ( this.tableBackColor ); }
			set { this.tableBackColor = value; }
		}
		private Color tableBackColor = Color.White;
		
		/// <summary>
		/// The cellpadding of the table
		/// </summary>
		public int TableCellPadding {
			get	{ return ( this.tableCellPadding ); }
			set { this.tableCellPadding = value; }
		}
		private int tableCellPadding = 3;
		
		/// <summary>
		/// The cellspacing of the table
		/// </summary>
		public int TableCellSpacing {
			get	{ return ( this.tableCellSpacing ); }
			set { this.tableCellSpacing = value; }
		}
		private int tableCellSpacing = 1;
		
		/// <summary>
		/// The border size of the table
		/// </summary>
		public int TableBorder {
			get	{ return ( this.tableBorder ); }
			set { this.tableBorder = value; }
		}
		private int tableBorder = 0;
		
		/// <summary>
		/// The back color for header text
		/// </summary>
		public Color HeaderBackColor {
			get { return ( this.headerBackColor ); }
			set { this.headerBackColor = value; }
		}
		private Color headerBackColor = Color.DarkGray;
		
		/// <summary>
		/// The fore color for header text
		/// </summary>
		public Color HeaderForeColor {
			get { return ( this.headerForeColor ); }
			set { this.headerForeColor = value; }
		}
		private Color headerForeColor = Color.White;
		
		/// <summary>
		/// The foreground color for node text
		/// </summary>
		public Color DataForeColor {
			get { return ( this.dataForeColor ); }
			set { this.dataForeColor = value; }
		}
		private Color dataForeColor = Color.Black;
		
		/// <summary>
		/// The background color for each node row
		/// </summary>
		public Color DataBackColor {
			get { return ( this.dataBackColor ); }
			set { this.dataBackColor = value; }
		}
		private Color dataBackColor = Color.White;
		
		/// <summary>
		/// The background color for each alternating node row
		/// </summary>
		public Color AltDataBackColor {
			get { return ( this.altDataBackColor ); }
			set { this.altDataBackColor = value; }
		}
		private Color altDataBackColor = Color.White;
		
		/// <summary>
		/// The background color for the panel's selected node row
		/// </summary>
		public Color SelectedDataBackColor {
			get { return ( this.selectedDataBackColor ); }
			set { this.selectedDataBackColor = value; }
		}
		private Color selectedDataBackColor = Color.Black;
		
		/// <summary>
		/// The foreground color for the panel's selected node row
		/// </summary>
		public Color SelectedDataForeColor {
			get { return ( this.selectedDataForeColor ); }
			set { this.selectedDataForeColor = value; }
		}
		private Color selectedDataForeColor = Color.White;

		/// <summary>
		/// The background color for the panel's selected node row
		/// </summary>
		public Color RowHilightedBackColor {
			get { return ( this.rowHilightedBackColor ); }
			set { this.rowHilightedBackColor = value; }
		}
		private Color rowHilightedBackColor = Color.LightGray;

		#endregion
		
		#endregion
		
		#region Constructors
		/// <summary>
		/// Create a new TreePanel
		/// </summary>
		public TreePanel() : base() {
			this.EnableViewState = true;
			this.renderer = new StandardTreeRenderer( this );
		}
		
		
		#endregion
		
		#region Events
		/// <summary>
		/// The event which indicates when the panel's "selected" node has changed.
		/// </summary>
		public event TreePanelNodeClickEventHandler NodeSelectedChanged {
			add { Events.AddHandler( NodeSelectedChangedEventKey, value ); }
			remove { Events.RemoveHandler( NodeSelectedChangedEventKey, value ); }
		}
		public virtual void OnNodeSelectedChanged( Object sender, TreePanelNodeClickEventArgs args ) {
			TreePanelNodeClickEventHandler handler = (TreePanelNodeClickEventHandler) Events[NodeSelectedChangedEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object NodeSelectedChangedEventKey = new object();
		
		/// <summary>
		/// The event which indicates when the "checked" state of a node has changed.
		/// </summary>
		public event TreePanelNodeClickEventHandler NodeCheckedChanged {
			add { Events.AddHandler( NodeCheckedChangedEventKey, value ); }
			remove { Events.RemoveHandler( NodeCheckedChangedEventKey, value ); }
		}
		public virtual void OnNodeCheckedChanged( Object sender, TreePanelNodeClickEventArgs args ) {
			TreePanelNodeClickEventHandler handler = (TreePanelNodeClickEventHandler) Events[NodeCheckedChangedEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object NodeCheckedChangedEventKey = new object();
		
		/// <summary>
		/// The event which indicates when the expanded/collapsed state of a node has changed.
		/// </summary>
		public event TreePanelNodeClickEventHandler NodeExpandedChanged {
			add { Events.AddHandler( NodeExpandedChangedEventKey, value ); }
			remove { Events.RemoveHandler( NodeExpandedChangedEventKey, value ); }
		}
		public virtual void OnNodeExpandedChanged( Object sender, TreePanelNodeClickEventArgs args ) {
			// we need to see if the current SelectedNode is hidden - if so, we need to clear it...
			if ( string.IsNullOrEmpty( this.SelectedNodeKey ) ) this.SelectedNodeKey = "0";
			while ( this.SelectedNodeKey != "0" && ! this.SelectedNode.IsVisible ) {
				if ( this.SelectedNode.Parent is TreeNodeControl ) {
					this.SelectedNodeKey = (this.SelectedNode.Parent as TreeNodeControl).Key;
				} else {
					this.SelectedNodeKey = "0";
				}
			}
			TreePanelNodeClickEventHandler handler = (TreePanelNodeClickEventHandler) Events[NodeExpandedChangedEventKey];
			if ( handler != null ) handler( sender, args );
		}
		private static readonly object NodeExpandedChangedEventKey = new object();
		
		#endregion
		
		#region Public Methods
		/// <summary>
		/// Add a header to the TreePanel. This is only usable in Tabular display format
		/// </summary>
		/// <param name="displayName">The display value of the header</param>
		/// <param name="formatString">The format string; leave blank to not format</param>
		/// <param name="valueType">The type of value stored in this data column</param>
		/// <param name="tagValueName">Used to retrieve the value for this header. 
		/// When using <see cref="XmlBind"/>, this is the attribute name to retrieve</param>
		/// <param name="dataHorizAlign">The horizontal alignment of data column</param>
		public void AddHeader( string displayName, string formatString, Type valueType, string tagValueName, HorizontalAlign dataHorizAlign ) {
			object[] objs = new object[] { displayName, formatString, valueType, tagValueName, dataHorizAlign.ToString() };
			this.headers.Add( objs );
		}
		
		
		/// <summary>
		/// Remove all headers from this TreePanel
		/// </summary>
		public void ClearHeaders() {
			this.headers.Clear();
		}
		
		
		/// <summary>
		/// Find a given TreeNodeControl based oon its key
		/// </summary>
		/// <param name="key"></param>
		/// <returns>The specified TreeNodeControl or null if not found</returns>
		public TreeNodeControl FindTreeNodeControl( string key ) {
			if ( string.IsNullOrEmpty( key ) ) return ( null );
			// Search the top level first
			foreach ( Control c in this.Controls ) {
				TreeNodeControl node = (TreeNodeControl) c;
				if ( node.Key == key ) {
					return ( node );
				}
			}
			// Search each node's children ...
			foreach ( Control c in this.Controls ) {
				TreeNodeControl node = (TreeNodeControl) c;
				TreeNodeControl found = node.FindTreeNode(key);
				if ( found != null ) return ( found );
			}
			// Bummer ...
			return ( null );
		}
		
		
		/// <summary>
		/// Guarantee that the __doPostBack function is defined
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e) {
			this.Page.ClientScript.GetPostBackEventReference( this, "" );
			base.OnInit( e ); 
		}
		
		
		/// <summary>
		/// Bind the TreePanel to an XML document.
		/// </summary>
		/// <param name="doc"></param>
		public void XmlBind( System.Xml.XmlDocument doc ) {
			this.Controls.Clear();
			foreach ( XmlNode node in doc.DocumentElement.ChildNodes ) {
				this.addChildNode( node, null );
			}
		}
		
		
		/// <summary>
		/// Bind the TreePanel to a DataSet
		/// </summary>
		/// <param name="data">The DataSet to bind to</param>
		/// <param name="dataTable">The DataTable to bind to</param>
		/// <param name="titleColumn"></param>
		/// <param name="keyColumn"></param>
		/// <param name="checkColumn">Whether or not to show a checkbox</param>
		/// <param name="checkDefaultsColumn">Whether or not to default the checkbox to checked</param>
		public void DataSetBind( DataSet data, string dataTable, string titleColumn, string keyColumn, string checkColumn, string checkDefaultsColumn ) {
			foreach ( DataRow row in data.Tables[dataTable].Rows ) {
				this.addChildNode( row, titleColumn, keyColumn, checkColumn, checkDefaultsColumn, null );
			}
		}
		
		
		/// <summary>
		/// Binds a DataSet without checkboxes
		/// </summary>
		/// <param name="data">The DataSet to bind to</param>
		/// <param name="dataTable">The DataTable to bind to</param>
		/// <param name="titleColumn"></param>
		/// <param name="keyColumn"></param>
		public void DataSetBind( DataSet data, string dataTable, string titleColumn, string keyColumn ) {
			DataSetBind( data, dataTable, titleColumn, keyColumn, "", "" );
		}
		
		
		/// <summary>
		/// Expand all TreeNodeControls in the TreePanel
		/// </summary>
		public void ExpandAll() {
			foreach ( Control c in this.Controls ) {
				TreeNodeControl node = c as TreeNodeControl;
				if ( node != null ) node.ExpandAll();
			}
			// Fire the event ...
			this.OnNodeExpandedChanged( this, new TreePanelNodeClickEventArgs( null ) );
		}
		
		
		/// <summary>
		/// Collapse all TreeNodeControls in the TreePanel
		/// </summary>
		public void CollapseAll() {
			foreach ( Control c in this.Controls ) {
				TreeNodeControl node = c as TreeNodeControl;
				if ( node != null ) node.CollapseAll();
			}
			// Fire the event ...
			this.OnNodeExpandedChanged( this, new TreePanelNodeClickEventArgs( null ) );
		}
		
		
		/// <summary>
		/// Add a new TreeNodeControl to the TreePanel
		/// </summary>
		/// <param name="title"></param>
		/// <param name="key"></param>
		/// <param name="showCheckbox"></param>
		/// <returns>The added TreeNodeControl</returns>
		public TreeNodeControl AddNode( string title, string id, bool showCheckbox ) {
			TreeNodeControl node = new TreeNodeControl( title, id, showCheckbox );
			this.Controls.Add( node );
			return ( node );
		}
		
		
		/// <summary>
		/// Add a new TreeNodeControl to the TreePanel
		/// </summary>
		/// <param name="title"></param>
		/// <param name="key"></param>
		/// <returns>The added TreeNodeControl</returns>
		public TreeNodeControl AddNode( string title, string id ) {
			return ( this.AddNode( title, id, false ) );
		}
		
		
		public TreeNodeControl AddNodeAfter( string title, string id, TreeNodeControl NodeToAddAfter ) {
			TreeNodeControl node = new TreeNodeControl( title, id, false );
			int index = this.Controls.IndexOf( NodeToAddAfter );
			this.Controls.AddAt( index + 1, node );
			return ( node );
		}
		
		
		public void MoveNodeUp( TreeNodeControl NodeToMove ) {
			// If this isn't a top-level node, send it to it's parent
			Control parent = NodeToMove.Parent;
			if ( parent is TreeNodeControl ) {
				((TreeNodeControl) parent).MoveNodeUp( NodeToMove );
				return;
			}
			// It's our's...
			int index = this.Controls.IndexOf( NodeToMove );
			// If index is zero, we're already at the top of the list
			if ( index <= 0 ) return;
			this.Controls.Remove( NodeToMove );
			this.Controls.AddAt( --index, NodeToMove );
		}
		
		
		public void MoveNodeDown( TreeNodeControl NodeToMove ) {
			// If this isn't a top-level node, send it to it's parent
			Control parent = NodeToMove.Parent;
			if ( parent is TreeNodeControl ) {
				((TreeNodeControl) parent).MoveNodeDown( NodeToMove );
				return;
			}
			// It's our's...
			int index = this.Controls.IndexOf( NodeToMove );
			// If index is > the total number of children, we're already at the bottom of the list
			if ( index > this.Controls.Count ) return;
			this.Controls.Remove( NodeToMove );
			this.Controls.AddAt( ++index, NodeToMove );
		}
		
		
		public void MoveNodeLeft( TreeNodeControl NodeToMove ) {
			// If this isn't a top-level node, send it to it's parent
			Control parent = NodeToMove.Parent;
			if ( parent is TreeNodeControl ) {
				((TreeNodeControl) parent).MoveNodeLeft( NodeToMove );
				return;
			}
			// ...can't get any more "left" than this!
		}
		
		
		public void MoveNodeRight( TreeNodeControl NodeToMove ) {
			// If this isn't a top-level node, send it to it's parent
			Control parent = NodeToMove.Parent;
			if ( parent is TreeNodeControl ) {
				((TreeNodeControl) parent).MoveNodeRight( NodeToMove );
				return;
			}
			// It's our's...
			TreeNodeControl prevNode = NodeToMove.PrevSibling();
			// If this node's at the top of it's sibling list, it can't go right!
			if ( prevNode == null ) return;
			// Remove the node from my collection of controls...
			this.Controls.Remove( NodeToMove );
			// And give it to it's most immediately-preceeding sibling...
			prevNode.Controls.Add( NodeToMove );
			prevNode.IsExpanded = true;
			//NodeToMove.Indent++;
		}
		
		
		public void RemoveNode( TreeNodeControl NodeToRemove ) {
			if ( this.SelectedNodeKey == NodeToRemove.Key ) {
				TreeNodeControl nextNode = NodeToRemove.NextSibling();
				this.SelectedNodeKey = nextNode == null ? "0" : nextNode.Key;
			}
			Control parent = NodeToRemove.Parent;
			parent.Controls.Remove( NodeToRemove );
		}
		
		
		/// <summary>
		/// Generate an XML string representation of the current TreePanel nodes.
		/// </summary>
		/// <returns>An XML string</returns>
		public string ToXml() {
			HttpContext.Current.Trace.Warn( ">>> TreePanel.ToXml" );
			// Turn the TreePanel into XML
			System.IO.MemoryStream stream = new System.IO.MemoryStream();
			System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter( stream, System.Text.Encoding.Default );
			writer.WriteStartElement( this.XmlRootTag, "" );
			// Write out all the child nodes
			foreach ( Control c in this.Controls ) {
				if ( c is TreeNodeControl ){
					TreeNodeControl node = (TreeNodeControl) c;
					node.WriteXml( writer );
				}
			}
			writer.WriteEndElement();
			writer.Flush();
			// Writing the XML is done, read it back out
			stream.Position = 0;
			System.IO.StreamReader reader = new System.IO.StreamReader( stream );
			string xml = reader.ReadToEnd();
			writer.Close();
			stream.Close();
			HttpContext.Current.Trace.Warn( "<<< TreePanel.ToXml" );
			return ( xml );
		}
		
		
		public void FromXml( string xml ) {
			HttpContext.Current.Trace.Warn( ">>> TreePanel.FromXml" );
			XmlDocument doc = new XmlDocument();
			doc.LoadXml( xml );
			this.XmlBind( doc );
			HttpContext.Current.Trace.Warn( "<<< TreePanel.FromXml" );
		}
		
		
		/// <summary>
		/// Handle the click of a node
		/// </summary>
		/// <param name="eventArgument"></param>
		public virtual void RaisePostBackEvent( String eventArgument ) {
			// find out how we got here...
			// True when a node's checkbox is clicked
			bool checkBoxClicked = false;
			if ( eventArgument.IndexOf( "checkbox" ) > -1 ) {
				eventArgument = eventArgument.Replace( ":checkbox", "" );
				checkBoxClicked = true;
			}
			// True when the node's title-link is clicked
			bool titleLinkClicked = false;
			if ( eventArgument.IndexOf( "select" ) > -1 ) {
				eventArgument = eventArgument.Replace( ":select", "" );
				titleLinkClicked = true;
			}
			// Get a ref to the node-control that was clicked...
			TreeNodeControl clickedNode = (TreeNodeControl) this.Page.FindControl( eventArgument );
			if ( titleLinkClicked ) {
				// Here, the title-link was clicked ...
				// We simply want to identify the node clicked as the new "selected" node for the panel
				this.SelectedNodeKey = clickedNode.Key;
				// If it's Selected, it should always be Expanded...
				//clickedNode.IsExpanded = true;
				// ... and fire the event...
				this.OnNodeSelectedChanged( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			} else if ( checkBoxClicked ) {
				// Here, a node's checkbox was clicked...
				// We want to toggle the checked-state...
				clickedNode.Check();
				// ...and force the parent to update its checked-state. 
				//TODO: This needs to be extended - in some cases, we'll want any-child-checked = parent-checked;
				//      in others, we'll want all-children-checked = parent-checked.
				//      This code shouldn't be here anyway - it should be in the node.Checked() method.
				// If any child in the hierarchy is checked, then all the parents of this node need to also be checked (?)
				if ( clickedNode.Parent is TreeNodeControl ) {
					((TreeNodeControl) clickedNode.Parent).TrackCheckedChildren();
				}
				// Fire the event ...
				this.OnNodeCheckedChanged( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			} else {
				// Here, a node's expand/collapse glyph was clicked...
				// We want to change it's IsExpanded state...
				clickedNode.IsExpanded = ! clickedNode.IsExpanded;
				// Fire the event ...
				this.OnNodeExpandedChanged( this, new TreePanelNodeClickEventArgs( clickedNode ) );
			}
		}
		
		
		#endregion
		
		#region Private Methods
		
		private void addChildNode( DataRow row, string titleColumn, string keyColumn, string checkColumn, string checkDefaultsColumn, TreeNodeControl ParentNode ) {
			string title;
			string key;
			bool checkbox = false;
			bool isChecked = false;

			title = row[titleColumn].ToString();
			key = row[keyColumn].ToString();
			if ( checkColumn != "" ) {
				if ( row[checkColumn] != DBNull.Value ) {
					checkbox = Convert.ToBoolean( row[checkColumn] );
				}
				if ( row[checkDefaultsColumn] != DBNull.Value ) {
					isChecked = Convert.ToBoolean( row[checkDefaultsColumn] );
				}
			}

			TreeNodeControl node;
			if ( ParentNode == null ) {
				// This is a top-level node - add it to the TreePanel
				node = this.AddNode( title, key, checkbox );
			} else {
				// ...add it to the ParentNode
				node = ParentNode.AddNode( title, key, checkbox );
			}
			// Now add the tagged values
			if ( this.headers.Count != 0 ) {
				foreach ( object[] objs in this.headers ) {
					// only add the tagged value if this row contains the column in question
					if ( row.Table.Columns.Contains( objs[0].ToString() ) ) {
						// objs[0] is the column name
						node.AddTaggedValue( objs[0].ToString(), row[objs[0].ToString()].ToString() );
					}
				}
			}
			// Now look for child nodes
			if ( row.Table.ChildRelations.Count > 0 ) {
				foreach ( DataRelation r in row.Table.ChildRelations ) {
					DataRow[] childRows = row.GetChildRows(r);
					foreach ( DataRow childRow in childRows ) {
						this.addChildNode( childRow, titleColumn, keyColumn, checkColumn, checkDefaultsColumn, node );
					}
				}
			}
			if ( checkbox ) {
				if ( ! this.Page.IsPostBack ) {
					node.Check(isChecked);
				}
			}
		}
		
		private void addChildNode( XmlNode xmlNode, TreeNodeControl parentNode ) {
			// Treats the "tagged values" as arbitrary attributes rather than nested elements
			// Get the "anticipated" attributes...
			string title = "";
			string key = "";
			bool checkbox = false;
			bool isChecked = false;
			bool isExpanded = false;
			title = xmlNode.Attributes[this.XmlTitleAttribute].Value;
			if ( xmlNode.Attributes[this.XmlKeyAttribute] != null ) {
				key = xmlNode.Attributes[this.XmlKeyAttribute].Value;
			}
			if ( xmlNode.Attributes[this.XmlShowCheckAttribute] != null ) {
				checkbox = Convert.ToBoolean( xmlNode.Attributes[this.XmlShowCheckAttribute].Value );
			}
			if ( xmlNode.Attributes[this.XmlCheckedAttribute] != null ) {
				isChecked = Convert.ToBoolean( xmlNode.Attributes[this.XmlCheckedAttribute].Value );
			}
			if ( xmlNode.Attributes[this.XmlExpandedAttribute] != null ) {
				isExpanded = Convert.ToBoolean( xmlNode.Attributes[this.XmlExpandedAttribute].Value );
			}
			// 
			TreeNodeControl nodeControl;
			if ( parentNode == null ) {
				// Add this node as a top-level child of the panel.
				nodeControl = this.AddNode( title, key, checkbox );
			} else {
				// Add to the parentNode
				nodeControl = parentNode.AddNode( title, key, checkbox );
			}
			// Get the tagged values
			foreach (XmlAttribute attr in xmlNode.Attributes ) {
				if ( ( attr.Name != this.XmlTitleAttribute ) && ( attr.Name != this.XmlKeyAttribute ) && ( attr.Name != this.XmlShowCheckAttribute ) 
				 && ( attr.Name != this.XmlCheckedAttribute ) && ( attr.Name != this.XmlExpandedAttribute ) ) {
					nodeControl.AddTaggedValue( attr.Name, attr.Value );
				}
			}
			// Add the child nodes ...
			foreach ( XmlNode child in xmlNode.ChildNodes ) {
				if ( child.Name == this.XmlNodeTag ) {
					this.addChildNode( child, nodeControl );
				}
			}
			if ( this.XmlShowCheckAttribute != "" && xmlNode.Attributes[this.XmlShowCheckAttribute] != null && 
				xmlNode.Attributes[this.XmlShowCheckAttribute].Value.Trim() != "" ) {
				if ( ! this.Page.IsPostBack ) {
					nodeControl.Check( isChecked );
				}
			}
			nodeControl.IsExpanded = isExpanded;
		}
		
		/// <summary>
		/// Render this control.
		/// </summary>
		/// <param name="writer">The current response's HtmlTextWriter</param>
		protected override void Render( HtmlTextWriter writer ) {
			HttpContext.Current.Trace.Warn( ">>> TreePanel.Render" );
			this.Renderer.RenderTreeStart( writer );
			base.Render( writer );
			this.Renderer.RenderTreeEnd( writer );
			HttpContext.Current.Trace.Warn( "<<< TreePanel.Render" );
		}
		
		/// <summary>
		/// Generate an object containing the panel's current state
		/// </summary>
		/// <returns>The panel's current state</returns>
		protected override object SaveViewState() {
			object[] state = new object[3];
			object baseState = base.SaveViewState();
			state[0] = baseState;
			state[1] = ""; //this.ToXml();
			state[2] = this.SelectedNodeKey;
			return ( state );
		}
		
		/// <summary>
		/// Load the TreePanel from the viewstate XML
		/// </summary>
		/// <param name="savedState">the statebag object</param>
		protected override void LoadViewState( object savedState ) {
			HttpContext.Current.Trace.Warn( ">>> TreePanel.LoadViewState." );
			if ( savedState != null ) {
				object[] state = (object[]) savedState;
				if ( state[0] != null ) base.LoadViewState( state[0] );
				//if ( state[1] != null ) this.FromXml( (string) state[1] );
				if ( state[2] != null ) this.SelectedNodeKey = (string) state[2];
			}
			HttpContext.Current.Trace.Warn( "<<< TreePanel.LoadViewState." );
		}
		
		#endregion
	}
	
	[Serializable()]
	public enum NodeDisplayStyle {
		/// <summary>
		/// Every node is a link
		/// </summary>
		Standard = 1,
		/// <summary>
		/// Leaf nodes are not linked
		/// </summary>
		LeafNodesNoLink
	}
	
	[Serializable()]
	public enum TreeOutputStyle {
		/// <summary>
		/// Nodes uses standard div tags to render output
		/// </summary>
		Standard = 1,
		/// <summary>
		/// Nodes use tables to render output
		/// </summary>
		/// <remarks>This option supports adding additional data to each node</remarks>
		Tabular = 2,
		/// <summary>
		/// Like Standard, however this output style dictates the folder images, adds 
		/// tree lines, and uses +/- images.
		/// </summary>
		WindowsXP = 3
	}
	
	/// <summary>
	/// Delegate type used for our custom node-clicked events
	/// </summary>
	public delegate void TreePanelNodeClickEventHandler(object sender, TreePanelNodeClickEventArgs e);
	
	public class TreePanelNodeClickEventArgs : System.EventArgs {
		
		public TreeNodeControl Node {
			get { return ( this.node ); }
		}
		private TreeNodeControl node;
		
		public TreePanelNodeClickEventArgs( TreeNodeControl n ) {
			this.node = n;
		}
		
	}
	
}
