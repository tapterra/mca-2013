using System;
using System.Web.UI;


namespace StarrTech.WebTools.TreeUI {
	/// <summary>
	/// Summary description for StandardTreeRenderer.
	/// </summary>
	public class StandardTreeRenderer : BaseTreeRenderer {

		private const int IndentPixels = 10;

		public StandardTreeRenderer( TreePanel treePanel ) : base( treePanel ) { }

		public override void RenderTreeStart( HtmlTextWriter writer ) {
			writer.WriteBeginTag( "div" );

			string style = "overflow:";
			style += this.TreePanel.Scrolling ? "auto" : "none";
			if ( !this.TreePanel.Width.IsEmpty ) {
				style += ";width:" + this.TreePanel.Width.ToString();
			}
			if ( !this.TreePanel.Height.IsEmpty ) {
				style += ";height:" + this.TreePanel.Height.ToString();
			}
			writer.WriteAttribute( "style", style );
			writer.Write( HtmlTextWriter.TagRightChar );
		}

		public override void RenderTreeEnd( HtmlTextWriter writer ) {
			writer.WriteEndTag( "div" );
		}

		public override void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.WriteBeginTag( "div" );
			writer.WriteAttribute( "style", "margin-left:" + ( node.Indent * IndentPixels ) + "px;" );
			writer.Write( HtmlTextWriter.TagRightChar );
		}

		public override void RenderNodeControlEnd( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.WriteEndTag( "div" );
		}

		public override void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer ) {
			if ( node.IsFolder ) {
				string script = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID );
				if ( this.TreePanel.ExpandedImage.Trim().Length != 0 && this.TreePanel.ExpandedImage.Trim().Length != 0 ) {
					writer.WriteBeginTag( "a" );
					writer.WriteAttribute( "href", script, false );
					writer.Write( HtmlTextWriter.TagRightChar );
					writer.Write( string.Format( "<img src='{0}' border='0'>",
						this.TreePanel.Page.ResolveUrl( node.IsExpanded ? this.TreePanel.ExpandedImage : this.TreePanel.CollapsedImage ) ) );
					writer.WriteEndTag( "a" );
				}
			} else {
				if ( this.TreePanel.NonFolderImage.Trim().Length != 0 ) {
					writer.Write( string.Format( "<img src='{0}'>", this.TreePanel.Page.ResolveUrl( this.TreePanel.NonFolderImage ) ) );
				} else {
					writer.Write( "&nbsp;&nbsp;&nbsp;&nbsp;" );
				}
			}
		}

		public override void RenderCheckbox( TreeNodeControl node, HtmlTextWriter writer ) {
			if ( node.ShowCheckBox ) {
				//string script = this.TreePanel.Page.GetPostBackClientEvent( this.TreePanel, node.UniqueID + ":checkbox" );
				string script = this.TreePanel.Page.ClientScript.GetPostBackEventReference( this.TreePanel, node.UniqueID + ":checkbox" );
				writer.Write( "<input type='checkbox' onclick=\"this.blur();" );
				writer.Write( script );
				writer.Write( "\"" );
				if ( node.IsChecked ) {
					writer.Write( " checked" );
				}
				writer.Write( ">" );
			}
		}

		public override void RenderNodeControlText( TreeNodeControl node, HtmlTextWriter writer ) {
			bool useLink = node.Controls.Count == 0 && this.TreePanel.NodeDisplayStyle == NodeDisplayStyle.LeafNodesNoLink;
			if ( !useLink ) {
				//name the anchor, in case you need to jump
				//writer.Write( "<a name='" + node.Key + "'>&nbsp;</a>" );
				writer.Write( string.Format( "<a name='{0}'>&nbsp;</a>", node.Key ) );
				writer.WriteBeginTag( "a" );
				writer.WriteAttribute( "href", this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID + ":select" ), false );
				writer.WriteAttribute( "class", this.TreePanel.CssClass );
				writer.Write( HtmlTextWriter.TagRightChar );
			}
			writer.Write( node.Title );
			if ( !useLink ) {
				writer.WriteEndTag( "a" );
			}
		}
	}
}
