using System;
using System.Web.UI;
using System.Drawing;
using System.Reflection;


namespace StarrTech.WebTools.TreeUI {
	/// <summary>
	/// Summary description for TabularTreeRenderer.
	/// </summary>
	public class TabularTreeRenderer : StandardTreeRenderer {
		
		protected const int IndentPixels = 20;
		
		protected int rowIndex = 0;
		
		public TabularTreeRenderer( TreePanel treePanel ) : base( treePanel ) {}
		
		public override void RenderTreeStart( HtmlTextWriter writer ) {
			writer.Write( String.Format( "<table cellpadding='{0}' cellspacing='{1}' border='{2}' bgcolor='{3}' width='{4}'>",
				this.TreePanel.TableCellPadding, this.TreePanel.TableCellSpacing, this.TreePanel.TableBorder, this.TreePanel.TableBackColor.Name, 
				this.TreePanel.Width
			) );
			if ( this.TreePanel.headers.Count > 0 ) {
				writer.WriteFullBeginTag( "tr" );
				writer.WriteBeginTag( "td" );
				writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.HeaderBackColor ) );
				writer.WriteAttribute( "style", "color:" + ColorTranslator.ToHtml( this.TreePanel.HeaderForeColor ) + ";" );
				writer.WriteAttribute( "align", this.TreePanel.TableHeaderHorizAlign.ToString() );
				writer.Write( HtmlTextWriter.TagRightChar );
				writer.Write( "&nbsp;" );
				writer.WriteEndTag( "td" );
				foreach ( object[] header in this.TreePanel.headers ) {
					writer.WriteBeginTag( "td" );
					writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.HeaderBackColor ) );
					writer.WriteAttribute( "style", "color:" + ColorTranslator.ToHtml( this.TreePanel.HeaderForeColor ) + ";" );
					writer.WriteAttribute( "align", this.TreePanel.TableHeaderHorizAlign.ToString() );
					writer.Write( HtmlTextWriter.TagRightChar );
					writer.Write( header[0] );
					writer.WriteEndTag( "td" );
				}
				writer.WriteEndTag( "tr" );
			}
		}
		
		
		public override void RenderTreeEnd( HtmlTextWriter writer ) {
			writer.Write( "</table>" );
		}
		
		
		public override void RenderNodeControlStart( TreeNodeControl node, HtmlTextWriter writer ) {
			this.rowIndex++;
			writer.WriteFullBeginTag( "tr" );
			writer.WriteBeginTag( "td" );
			if ( node.Key == this.TreePanel.SelectedNodeKey ) {
				writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.SelectedDataBackColor ) );
			} else if ( this.rowIndex % 2 != 0 ) {
				writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.DataBackColor ) );
			} else {
				writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.AltDataBackColor ) );
			}
			writer.WriteAttribute( "style", "color:" + ColorTranslator.ToHtml( this.TreePanel.DataForeColor ) + ";" );
			writer.Write( HtmlTextWriter.TagRightChar );
			writer.WriteBeginTag( "div" );
			writer.WriteAttribute( "style", "margin-left:" + (node.Indent * IndentPixels) + "px;" );
			writer.Write( HtmlTextWriter.TagRightChar );
		}
		
		
		public override void RenderNodeControlEnd( TreeNodeControl node, HtmlTextWriter writer ) {
			writer.WriteEndTag( "div" );
			writer.WriteEndTag( "td" );
			// Write out any tagged values
			foreach ( object[] o in this.TreePanel.headers ) {
				string displayName = (string) o[0];
				string formatString = (string) o[1];
				Type valueType = (Type) o[2];
				string tagValueName = (string) o[3];
				
				// Write out a <td> for this header
				writer.WriteBeginTag( "td" );
				if ( this.rowIndex % 2 != 0 ) {
					writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.DataBackColor ) );
				} else {
					writer.WriteAttribute( "bgcolor", ColorTranslator.ToHtml( this.TreePanel.AltDataBackColor ) );
				}
				writer.WriteAttribute( "style", "color:" + ColorTranslator.ToHtml( this.TreePanel.DataForeColor ) + ";" );
				writer.WriteAttribute( "align", o[4].ToString() );
				writer.Write( HtmlTextWriter.TagRightChar );
				if ( node.Attributes[tagValueName] != null ) {
					string outpt = node.Attributes[tagValueName];
					if ( formatString.Trim() != "" && valueType.FullName != "System.String" ) {
						// Create an object of the correct type by Parse()'ing it
						object theValue = valueType.InvokeMember( "Parse", 
							BindingFlags.Static | BindingFlags.Public | BindingFlags.InvokeMethod, null, null, new object[] { outpt } 
						);
						// Format the output
						outpt = (string) valueType.InvokeMember( "ToString", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance, 
							null, theValue, new object[] { formatString } 
						);
					}
					// Now write the value
					writer.Write( outpt );
				} else {
					writer.Write( "&nbsp;" );
				}
				writer.WriteEndTag( "td" );
			}
			writer.WriteEndTag( "tr" );
		}
		
		
		public override void RenderImageLink( TreeNodeControl node, HtmlTextWriter writer ) {
			if ( node.IsFolder ) {
				string script = this.TreePanel.Page.ClientScript.GetPostBackClientHyperlink( this.TreePanel, node.UniqueID );
				if ( this.TreePanel.ExpandedImage.Trim().Length != 0 && this.TreePanel.ExpandedImage.Trim().Length != 0 ) {
					writer.WriteBeginTag( "a" );
					writer.WriteAttribute( "href", script, false );
					writer.Write( HtmlTextWriter.TagRightChar );
					writer.Write( string.Format( "<img src='{0}' border='0'>", 
						//this.TreePanel.Page.ResolveUrl
						( node.IsExpanded ? this.TreePanel.ExpandedImage : this.TreePanel.CollapsedImage ) ) );
					writer.WriteEndTag( "a" );
				}
			} else {
				writer.Write( string.Format( "<img src='{0}'>", //this.TreePanel.Page.ResolveUrl
				( this.TreePanel.NullImage ) ) );
			}
		}
		
	}
}
