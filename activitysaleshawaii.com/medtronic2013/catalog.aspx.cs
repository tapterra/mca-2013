using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ABS;

public partial class Catalog : Page {

	#region Properties

	public string IslandKey {
		get {
			object o = Session["CurrentIslandKey"];
			return ( o == null ? "bi" : (string) o );
		}
		set {
			Session["CurrentIslandKey"] = value;
			// RKP: Begin Change - 20090205
			// RKP: Being anal, but we should dispose of any current Island
			this.island = null;
			// RKP: End Change
			this.Rebind();
		}
	}

	public Island Island {
		get {
			if ( this.island == null ) {
				this.island = new Island( this.IslandKey );
			}
			return ( this.island );
		}
	}
	private Island island;

	public string CategoryKey {
		get {
			object o = Session["CurrentCategoryKey"];
			return ( o == null ? "activities" : (string) o );
		}
		set {
			Session["CurrentCategoryKey"] = value;
			this.Rebind();
		}
	}

	public Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryKey );
			}
			return ( this.category );
		}
	}
	private Category category;

	#endregion

	#region Methods

	protected void IslandChanged( Object sender, EventArgs args ) {
		this.Rebind();
	}

	// RKP: Begin Change - 20090205
	// RKP: For each case, assign this.CategoryKey to the appropriate publisher-control 
	// RKP: CategoryKey property, and remove call to Rebind
	protected void Rebind() {
		this.MGPub.Visible = false;
		this.ActPub.Visible = false;
		this.SpaPub.Visible = false;
		switch ( this.CategoryKey ) {
			case "leigreeting":
				this.MGPub.Visible = true;
				this.MGPub.CategoryKey = this.CategoryKey;
				//this.MGPub.Rebind();
				break;
			case "golf":
			case "spa":
				this.SpaPub.Visible = true;
				this.SpaPub.CategoryKey = this.CategoryKey;
				//this.SpaPub.Rebind();
				break;
			case "dining":
			case "activities":
				this.ActPub.Visible = true;
				this.ActPub.CategoryKey = this.CategoryKey;
				//this.ActPub.Rebind();
				break;
		}
	}
	// RKP: End Change

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
			this.Rebind();
		}
	}

	#endregion


}
