<%@ Page Language="C#" AutoEventWireup="true" CodeFile="catalog.aspx.cs" Inherits="Catalog" %>
<%@ register tagprefix="abs" tagname="mandgpublisher" src="controls/MAndGPublisher.ascx" %>
<%@ register tagprefix="abs" tagname="activitiespublisher" src="controls/ActivitiesPublisher.ascx" %>
<%@ register tagprefix="abs" tagname="spapublisher" src="controls/SpaPublisher.ascx" %>
<%@ import namespace="ABS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
	
<div class="PublisherPanel">
		
		<abs:islandpicker runat="server" id="IslandPicker" onislandchanged="IslandChanged"  IslandKey="oahu" Visible="false"/>
		
		<abs:mandgpublisher runat="server" id="MGPub" />
		
		<abs:activitiespublisher runat="server" id="ActPub" visible="false" />
		
		<abs:spapublisher runat="server" id="SpaPub" visible="false" />
		
	</div>
	
</asp:Content>
