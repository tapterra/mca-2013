<%@ Page Language="C#" inherits="ABS.AdminPage" %>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminReservations );
	}
	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Reservations</h1>
	<adm:reseditor runat="server" id="ResGrid" />

</asp:Content>
