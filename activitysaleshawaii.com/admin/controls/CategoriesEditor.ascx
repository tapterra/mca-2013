<%@ Control Language="C#" ClassName="CategoriesEditor" %>
<%@ register tagPrefix="adm" tagName="subcatsgrid"			src="SubcatGridPanel.ascx" %>
<%@ import namespace="ABS" %>

<script runat="server">

	protected Category Category {
		get {
			return ( this.AdminGrid.DBItem as Category );
		}
	}

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}
	
	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		LabeledTextBox LabelBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:LabelBox" );
		LabeledTextBox KeyBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:KeyBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );
		
		// Get a reference to the business object to be edited...
		Category cat = new Category( this.AdminGrid.FormController.RecordID );
		cat.Label = LabelBox.Text;
		cat.KeyName = KeyBox.Text;
		cat.Comments = CommentsBox.Text;

		this.AdminGrid.DBItem = cat;
	}
	
	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( Category );
		
		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminCategoriesUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminCategoriesAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminCategoriesDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			this.AdminGrid.GridController.SortExpression = "seq";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}
	
	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	public void DoReorderUp( Object sender, DataGridClickEventArgs args ) {
		Category cat = new Category( args.RecordID );
		cat.MoveUp();
		this.ResetEditor();
	}

	public void DoReorderDown( Object sender, DataGridClickEventArgs args ) {
		Category cat = new Category( args.RecordID );
		cat.MoveDown();
		this.ResetEditor();
	}

</script>

<script runat="server">

	protected SubcatGridPanel SubcatsGrid {
		get { return ( (SubcatGridPanel) AdminGrid.FindControl( "FormControls:SubcatsGrid" ) ); }
	}

	protected HtmlTable CategoryFormPanel {
		get { return ( (HtmlTable) AdminGrid.FindControl( "FormControls:CategoryFormPanel" ) ); }
	}

	protected void PersistCategory() {
		this.ReadFormControls( null, null );
		this.Category.Persist();
		this.AdminGrid.FormController.RecordID = this.Category.ID;
		this.SubcatsGrid.CategoryID = this.Category.ID;
	}

	protected void SubcatFormOpening( object sender, System.EventArgs e ) {
		// The Subcat Form is opening ...
		// Make sure our parent record has been saved
		if ( this.AdminGrid.FormController.RecordID <= 0 ) {
			this.PersistCategory();
		}
		// Update the static Category fields
		this.ReadFormControls( null, null );
		this.SubcatsGrid.CategoryID = this.Category.ID;
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.CategoryFormPanel.Visible = false;
	}

	protected void SubcatFormClosing( object sender, System.EventArgs e ) {
		// The Subcat Form is closing - show the main Category form
		this.AdminGrid.FormController.Visible = true;
		this.CategoryFormPanel.Visible = true;
	}

</script>


<dbui:admindatagrid runat="server" id="AdminGrid" OnFormControlsRead="ReadFormControls" 
 allowusersorting="false" includereordercontrols="true" onreorderdown="DoReorderDown" onreorderup="DoReorderUp"
>
	
	<formcontrols>
	
		<table runat="server" id="CategoryFormPanel" border="0" cellpadding="2" cellspacing="0" width="100%">
			
			<tr>
				<td width="30%">
					<stic:labeledtextbox runat="server" id="LabelBox" label="Label"
					 text='<%# ((ABS.Category) AdminGrid.DBItem).Label %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="LabelBoxReqVal" controltovalidate="LabelBox" 
					 setfocusonerror="true" errormessage="A value is required for Label."
					/>
				</td>
				<td width="20%">
					<stic:labeledtextbox runat="server" id="KeyBox" label="Category Key"
					 text='<%# ((ABS.Category) AdminGrid.DBItem).KeyName %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator2" controltovalidate="KeyBox" 
					 setfocusonerror="true" errormessage="A value is required for Category Key."
					/>
				</td>
				<td>
				</td>
			</tr>
			
			<tr>
				<td colspan="3">
					<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
					 text='<%# ((ABS.Category) AdminGrid.DBItem).Comments %>'
					 textmode="MultiLine" rows="6" wrap="true"
					/>
				</td>
			</tr>
			
		</table>
		
		<adm:subcatsgrid runat="server" id="SubcatsGrid" 
		 categoryid='<%# this.Category.ID %>' 
		 onformclosing="SubcatFormClosing"
		 onformopening="SubcatFormOpening"
		/>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Category"			datafield="Label"			/> 
		<asp:boundcolumn headertext="Key"						datafield="KeyName"		/> 
	</gridcolumns>
	
</dbui:admindatagrid>
