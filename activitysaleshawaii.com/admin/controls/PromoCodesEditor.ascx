﻿<%@ Control Language="C#" ClassName="PromoCodesEditor" %>
<%@ import namespace="ABS" %>

<script runat="server">

	public int CurrentWebsiteID {
		get { return ( Convert.ToInt32( this.SWebsiteLBox.SelectedValue ) ); }
	}

	public Website Website {
		get { return ( new Website( this.CurrentWebsiteID ) ); }
	}

	private LabeledDropDownList SWebsiteLBox {
		get { return ( AdminGrid.FindControl( "SearchControls:SWebsiteLBox" ) as LabeledDropDownList ); }
	}

	private LabeledDropDownList WebsiteLBox {
		get { return ( AdminGrid.FindControl( "FormControls:WebsiteLBox" ) as LabeledDropDownList ); }
	}
	private LabeledTextBox PromoCodeBox {
		get { return ( AdminGrid.FindControl( "FormControls:PromoCodeBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox CommentsBox {
		get { return ( AdminGrid.FindControl( "FormControls:CommentsBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox StartDateBox {
		get { return ( AdminGrid.FindControl( "FormControls:StartDateBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox EndDateBox {
		get { return ( AdminGrid.FindControl( "FormControls:EndDateBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox FlatDiscountBox {
		get { return ( AdminGrid.FindControl( "FormControls:FlatDiscountBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox PctDiscountBox {
		get { return ( AdminGrid.FindControl( "FormControls:PctDiscountBox" ) as LabeledTextBox ); }
	}
	private LabeledTextBox MinPurchaseBox {
		get { return ( AdminGrid.FindControl( "FormControls:MinPurchaseBox" ) as LabeledTextBox ); }
	}

	//LabeledDropDownList WebsiteLBox;
	//LabeledTextBox PromoCodeBox;
	//LabeledTextBox CommentsBox;
	//LabeledTextBox StartDateBox;
	//LabeledTextBox EndDateBox;
	//LabeledTextBox FlatDiscountBox;
	//LabeledTextBox PctDiscountBox;
	//LabeledTextBox MinPurchaseBox;
			
</script>

<script runat="server">

	public void InitFormControls( Object sender, EventArgs args ) {
		PromoCode code = this.AdminGrid.DBItem as PromoCode;
		if ( code.WebsiteID <= 0 ) {
			code.WebsiteID = this.CurrentWebsiteID;
		}
		SetupWebsiteLBox();
		WebsiteLBox.SelectedValue = code.WebsiteID.ToString();
	}

	private void SetupSWebsiteLBox() {
		this.SWebsiteLBox.Items.Clear();
		System.Collections.Generic.Dictionary<int, Website> sites = Websites.CurrentPaySites;
		foreach ( int key in sites.Keys ) {
			SWebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		SWebsiteLBox.SelectedIndex = 0;
	}
	private void SetupWebsiteLBox() {
		WebsiteLBox.Items.Clear();
		System.Collections.Generic.Dictionary<int, Website> sites = Websites.CurrentPaySites;
		foreach ( int key in sites.Keys ) {
			WebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		WebsiteLBox.SelectedIndex = 0;
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to the business object to be edited...
		PromoCode code = new PromoCode( this.AdminGrid.FormController.RecordID );
		code.WebsiteID = Convert.ToInt32( this.WebsiteLBox.SelectedValue );
		code.Code = PromoCodeBox.Text;
		code.Comments = CommentsBox.Text;
		DateTime start = DateTime.MinValue;
		DateTime.TryParse( StartDateBox.Text, out start );
		code.StartDate = start;
		DateTime end = DateTime.MinValue;
		DateTime.TryParse( EndDateBox.Text, out end );
		code.EndDate = end;
		int amount = 0;
		Int32.TryParse( FlatDiscountBox.Text, out amount );
		code.FlatDiscount = amount;
		amount = 0;
		Int32.TryParse( PctDiscountBox.Text, out amount );
		code.PctDiscount = amount;
		amount = 0;
		Int32.TryParse( MinPurchaseBox.Text, out amount );
		code.MinPurchase = amount;
		code.Comments = CommentsBox.Text;
		this.AdminGrid.DBItem = code;
	}
	
	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( PromoCode );
		
		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminPromoCodesUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminPromoCodesAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminPromoCodesDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			this.SetupSWebsiteLBox();
			this.SetupWebsiteLBox();
			this.AdminGrid.GridController.SortExpression = "start_date";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void DoSearch( object sender, System.EventArgs e ) {
		this.AdminGrid.BindGrid();
	}
	
	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	private void BindDataGrid() {
		// Re-instantiate to insure that the activities are reloaded
		this.AdminGrid.Grid.DataSource = this.Website.ActivePromoCodes;
		this.AdminGrid.DataBind();
	}

	public void ClearSearchControls( Object sender, EventArgs args ) {
		this.SWebsiteLBox.SelectedIndex = 0;
	}

	public void ReadSearchControls( Object sender, EventArgs args ) {
		PromoCode.NewSearch();
		PromoCode.SWebsiteID = Convert.ToInt32( this.SWebsiteLBox.SelectedValue );
	}

</script>

<dbui:admindatagrid runat="server" id="AdminGrid" 
 onsearchcontrolsclear="ClearSearchControls" onsearchcontrolsread="ReadSearchControls"
 onformcontrolsinit="InitFormControls" onformcontrolsread="ReadFormControls"
 allowusersorting="false" includeshowallbutton="false" 
>
	<searchcontrols>
		<div style="width:200px">
			<stic:labeleddropdownlist runat="server" id="SWebsiteLBox" label="Website" 
			 autopostback="true" onselectedindexchanged="DoSearch"
			/>
		</div>

	</searchcontrols>
	<formcontrols>
		<table runat="server" id="FormPanel" border="0" cellpadding="2" cellspacing="0" width="100%">
			<tr>
				<td>
					<stic:labeleddropdownlist runat="server" id="WebsiteLBox" label="Website"
					/>
				</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<stic:labeledtextbox runat="server" id="PromoCodeBox" label="Promo Code"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).Code %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="LabelBoxReqVal" controltovalidate="PromoCodeBox" 
					 setfocusonerror="true" errormessage="A value is required for Promo Code."
					/>
				</td>
				<td width="100">
					<stic:labeledtextbox runat="server" id="StartDateBox" label="Start Date"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).StartDateString %>'
					/>
				</td>
				<td width="100">
					<stic:labeledtextbox runat="server" id="EndDateBox" label="End Date"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).EndDateString %>'
					/>
					<asp:comparevalidator id="Comparevalidator2" runat="server" controltovalidate="StartDateBox" controltocompare="EndDateBox"
					 operator="LessThan" type="Date" display="None"
					 setfocusonerror="true" errormessage="Start Date must be earlier than End Date."
					/>
				</td>
				<td width="100">
					<stic:labeledtextbox runat="server" id="FlatDiscountBox" label="Flat Discount"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).FlatDiscount.ToString("0") %>'
					/>
					<asp:comparevalidator id="Comparevalidator1" runat="server" controltovalidate="FlatDiscountBox"
					 operator="GreaterThanEqual" type="Currency" valuetocompare="0"
					 display="None" setfocusonerror="true" 
					 errormessage="Please enter a whole number greater than zero for Flat Discount."
					/>
				</td>
				<td width="100">
					<stic:labeledtextbox runat="server" id="PctDiscountBox" label="% Discount"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).PctDiscount %>'
					/>
					<asp:comparevalidator id="Comparevalidator3" runat="server" controltovalidate="PctDiscountBox"
					 operator="GreaterThanEqual" type="Integer" valuetocompare="0"
					 display="None" setfocusonerror="true" 
					 errormessage="Please enter a whole number between zero and 100 for % Discount."
					/>
					<asp:comparevalidator id="Comparevalidator4" runat="server" controltovalidate="PctDiscountBox"
					 operator="LessThan" type="Integer" valuetocompare="100"
					 display="None" setfocusonerror="true" 
					 errormessage="Please enter a whole number between zero and 100 for % Discount."
					/>
				</td>
				<td width="100">
					<stic:labeledtextbox runat="server" id="MinPurchaseBox" label="Min Purchase"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).MinPurchase.ToString("0") %>'
					/>
					<asp:comparevalidator id="Comparevalidator5" runat="server" controltovalidate="MinPurchaseBox"
					 operator="GreaterThanEqual" type="Currency" valuetocompare="0"
					 display="None" setfocusonerror="true" 
					 errormessage="Please enter a whole number greater than zero for Minimum Purchase."
					/>
				</td>
			</tr>
			<tr>
				<td colspan="6">
					<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
					 text='<%# ((ABS.PromoCode) AdminGrid.DBItem).Comments %>'
					 textmode="MultiLine" rows="6" wrap="true"
					/>
				</td>
			</tr>			
		</table>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Code"					datafield="Code" /> 
		<asp:boundcolumn headertext="Start"					datafield="StartDateString"
			itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" 
		/> 
		<asp:boundcolumn headertext="End"						datafield="EndDateString"
			itemstyle-width="100px" itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" 
		/> 
		<asp:boundcolumn headertext="Flat Discount" datafield="FlatDiscountString"
			itemstyle-width="100px" itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
		/> 
		<asp:boundcolumn headertext="% Discount"		datafield="PctDiscountString"	
			itemstyle-width="100px" itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
		/> 
		<asp:boundcolumn headertext="Min Purchase"	datafield="MinPurchaseString"	
			itemstyle-width="100px" itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
		/> 
	</gridcolumns>
	
</dbui:admindatagrid>
