using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using StarrTech.WebTools.Controls;
using ABS;

public partial class WebsitesEditor : System.Web.UI.UserControl {

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		LabeledTextBox NameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:NameBox" );
		LabeledTextBox JobNoBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:JobNoBox" );
		LabeledTextBox HostVendorAidBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:HostVendorAidBox" );
		CheckBox IsBookingTestModeXBox = (CheckBox) AdminGrid.FindControl( "FormControls:IsBookingTestModeXBox" );
		CheckBox IsPmtProcessorTestModeXBox = (CheckBox) AdminGrid.FindControl( "FormControls:IsPmtProcessorTestModeXBox" );
		CheckBox IsNoPayXBox = (CheckBox) AdminGrid.FindControl( "FormControls:IsNoPayXBox" );
		LabeledDropDownList BookingStartMoLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:BookingStartMoLBox" );
		LabeledDropDownList BookingStartYrLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:BookingStartYrLBox" );
		LabeledDropDownList BookingEndMoLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:BookingEndMoLBox" );
		LabeledDropDownList BookingEndYrLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:BookingEndYrLBox" );
		LabeledTextBox ExcReportsToAddressBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:ExcReportsToAddressBox" );
		LabeledTextBox HostVendorAddressBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:HostVendorAddressBox" );
		LabeledTextBox VoucherLogoUrlBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:VoucherLogoUrlBox" );
		LabeledTextBox VoucherEmailFromAddressBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:VoucherEmailFromAddressBox" );
		LabeledTextBox VoucherEmailBccAddressesBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:VoucherEmailBccAddressesBox" );
		LabeledTextBox VoucherEmailSubjectBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:VoucherEmailSubjectBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );
		// Get a reference to the business object to be edited...
		Website site = new Website( this.AdminGrid.FormController.RecordID );
		site.Name = NameBox.Text;
		site.JobNo = JobNoBox.Text;
		site.HostVendorAid = HostVendorAidBox.Text;
		LabeledTextBox SiteRegionAidBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:SiteRegionAidBox" );
		site.SiteRegionAid = SiteRegionAidBox.Text;
		site.IsBookingTestMode = IsBookingTestModeXBox.Checked;
		site.IsPmtProcessorTestMode = IsPmtProcessorTestModeXBox.Checked;
		site.IsNoPaymentSite = IsNoPayXBox.Checked;
		site.BookingStartMo = Int32.Parse( BookingStartMoLBox.SelectedValue );
		site.BookingStartYr = Int32.Parse( BookingStartYrLBox.SelectedValue );
		site.BookingEndMo = Int32.Parse( BookingEndMoLBox.SelectedValue );
		site.BookingEndYr = Int32.Parse( BookingEndYrLBox.SelectedValue );
		site.ExcReportsToAddress = ExcReportsToAddressBox.Text;
		site.HostVendorAddress = HostVendorAddressBox.Text;
		site.VoucherLogoUrl = VoucherLogoUrlBox.Text;
		site.VoucherEmailFromAddress = VoucherEmailFromAddressBox.Text;
		site.VoucherEmailBccAddresses = VoucherEmailBccAddressesBox.Text;
		site.VoucherEmailSubject = VoucherEmailSubjectBox.Text;
		site.Comments = CommentsBox.Text;
		this.AdminGrid.DBItem = site;
	}

	public void InitEditor() {
		// Let the grid know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( Website );
		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminWebsitesUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminWebsitesAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminWebsitesDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			this.AdminGrid.GridController.SortExpression = "name";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

}
