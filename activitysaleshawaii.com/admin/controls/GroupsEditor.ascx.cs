using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.Controls;
using StarrTech.WebTools.Data;
using StarrTech.WebTools.DBUI;
using StarrTech.WebTools.Panels;
using ABS;

/// <summary>
///		Summary description for GroupsEditor.
/// </summary>
public partial class GroupsEditor : System.Web.UI.UserControl, IAdminDataGridContentEditor {

	int IAdminDataGridContentEditor.NodeID {
		get { return ( this.nodeID ); }
		set { this.nodeID = value; }
	}
	private int nodeID = -1;

	AdminDataGrid IAdminDataGridContentEditor.DataGrid {
		get { return ( this.AdminGrid ); }
	}

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}

	public void InitFormControls( Object sender, EventArgs args ) {
		// Here, we can implement any special features needed by the data-entry form controls, such as
		// special data-binding for related-table controls.
		SetupFormMembersBox();
		SetupPermissionsPanel();
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// This is where we populate an instance of our BaseSqlPersistable subclass with values from the 
		// data-entry form, so that it's record can be persisted to the database...
		// Get a reference to all the form-panel's controls
		LabeledTextBox groupName = (LabeledTextBox) AdminGrid.FindControl( "FormControls:GroupName" );
		LabeledTextBox comments = (LabeledTextBox) AdminGrid.FindControl( "FormControls:Comments" );
		LabeledCheckBoxList memberBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:MemberBoxList" );
		// Get a reference to the PermissionGroup object to be persisted...
		PermissionGroup group = (PermissionGroup) AdminGrid.DBItem;
		group.ID = this.AdminGrid.FormController.RecordID;
		if ( groupName != null )
			group.GroupName = groupName.Text;
		if ( comments != null )
			group.Comments = comments.Text;
		if ( memberBoxList != null )
			for ( int i = 0 ; i < memberBoxList.Items.Count ; i++ )
				if ( memberBoxList.Items[i].Selected )
					group.MemberIDs.Add( Convert.ToInt32( memberBoxList.Items[i].Value ) );
		// Pass the instance to the datagrid so that it can pass it to the database
		this.AdminGrid.DBItem = group;
		// Read the permission assignments...
		XmlPermissionsPanel permpanel = (XmlPermissionsPanel) AdminGrid.FindControl( "FormControls:permpanel" );
		if ( permpanel != null ) {
			group.PermissionKeys.Clear();
			group.PermissionKeys.AddRange( permpanel.SelectedValues );
		}
	}

	private void SetupFormMembersBox() {
		// Make sure the checkboxes in the MemberBoxList control properly reflect's the current group's membership
		// Get a reference to the MemberBoxList control
		LabeledCheckBoxList memberBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:MemberBoxList" );
		// Get a reference to the PermissionGroup object to be edited...
		PermissionGroup group = (PermissionGroup) AdminGrid.DBItem;
		if ( memberBoxList != null )
			// Check the boxes for each of this group's members
			for ( int i = 0 ; i < memberBoxList.Items.Count ; i++ ) {
				memberBoxList.Items[i].Selected = group.MemberIDs.Contains( Convert.ToInt32( memberBoxList.Items[i].Value ) );
			}
		}

	private void SetupPermissionsPanel() {
		// Get a reference to the XmlPermissionsPanel control
		XmlPermissionsPanel permpanel = (XmlPermissionsPanel) AdminGrid.FindControl( "FormControls:permpanel" );
		// Get a reference to the PermissionGroup object to be edited...
		PermissionGroup group = (PermissionGroup) AdminGrid.DBItem;
		if ( permpanel != null ) {
			permpanel.SelectedValues = group.PermissionKeys;
		}
	}

	private void BindFormMemberBoxList() {
		// Load the MemberBoxList control with checkboxes for each user in the database
		// Get a reference to the form-panel's MemberBoxList control
		LabeledCheckBoxList memberBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:MemberBoxList" );
		if ( memberBoxList != null ) {
			// Load a DataSet of all the existing users
			DataSet ds;
			using ( SqlDatabase db = new SqlDatabase() ) {
				ds = db.GetDataSet( "USERS",
					"select id, full_name = last_name + ', ' + first_name from USERS where ( is_deleted = 0 ) order by full_name"
				);
			}
			// Bind the control to the DataSet
			memberBoxList.DataSource = ds.Tables["USERS"].DefaultView;
			memberBoxList.DataTextField = "full_name";
			memberBoxList.DataValueField = "id";
			memberBoxList.DataBind();
		}
	}

	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( PermissionGroup );

		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminGroupsUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminGroupsAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminGroupsDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !IsPostBack ) {
			this.BindFormMemberBoxList();
			this.AdminGrid.GridController.SortExpression = "group_name";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

}
