<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupsEditor.ascx.cs" Inherits="GroupsEditor" %>
<%@ import namespace="ABS" %>

<dbui:admindatagrid runat="server" id="AdminGrid" 
	OnFormControlsInit="InitFormControls"
	OnFormControlsRead="ReadFormControls"
	>
	
	<formcontrols>
		<br />
		<table border="0" cellpadding="0" cellspacing="4" width="100%">
			<tr>
				<td width="33%">
					<stic:labeledtextbox runat="server" id="GroupName" label="Group Name" width="100%" 
						text='<%# ((PermissionGroup) AdminGrid.DBItem).GroupName %>'
						tabindex='1'
						cssclass="req" 
					/>
					<asp:requiredfieldvalidator id="GroupNameReqVal" runat="server"
						controltovalidate="GroupName" display="none" errormessage="A value is required for Group Name."
					/>
				</td>
				<td width="33%">
					&nbsp;
				</td>
				<td rowspan="2" width="33%" valign="top" style="padding:0; vartical-align:top" height="241" >
					<stic:labeledcheckboxlist runat="server" id="MemberBoxList" label="Members" height="241" width="100%" 
						tabindex='1'
					/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<stic:labeledtextbox runat="server" id="Comments" label="Comments" textmode="multiline" rows="15" wrap="true" width="100%" 
						text='<%# ((PermissionGroup) AdminGrid.DBItem).Comments %>'
						height="200"
						tabindex='1'
					/>
				</td>
			</tr>
		</table>
		<stip:xmlpermissionspanel runat="server" id="permpanel" repeatcolumns="4" width="100%" 
		 style="text-align:left"
		 tabindex='1'
		/>
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn datafield="GroupName" sortexpression="group_name" headertext="Group Name" /> 
	</gridcolumns>
	
</dbui:admindatagrid>
