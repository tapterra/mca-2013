using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ActivityFormPanel : System.Web.UI.UserControl {

	#region Properties

	public int ActivityID {
		get {
			object obj = ViewState["ActivityID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ActivityID"] = value; }
	}

	public int ActivityGroupID {
		get {
			object obj = ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ActivityGroupID"] = value; }
	}

	public int Seq {
		get {
			object obj = ViewState["Seq"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["Seq"] = value; }
	}

	public string ActivityGroupName {
		get { return ( this.ActivityGroupBox.Text ); }
		set { this.ActivityGroupBox.Text = value; }
	}

	public string VendorName {
		get { return ( this.VendorNameBox.Text ); }
		set { this.VendorNameBox.Text = value; }
	}

	public string WebsiteName {
		get { return ( this.WebsiteBox.Text ); }
		set { this.WebsiteBox.Text = value; }
	}

	public string ActivityAID {
		get { return ( this.ActivityAIDBox.Text ); }
		set { this.ActivityAIDBox.Text = value; }
	}

	public string ActivityTitle {
		get { return ( this.ActivityTitleBox.Text ); }
		set { this.ActivityTitleBox.Text = value; }
	}

	public string Description {
		get { return ( this.DescriptionBox.Text ); }
		set { this.DescriptionBox.Text = value; }
	}

	public string Comments {
		get { return ( this.CommentsBox.Text ); }
		set { this.CommentsBox.Text = value; }
	}

	public Activity Activity {
		get {
			Activity activity = new Activity( this.ActivityID );
			activity.ActivityGroupID = this.ActivityGroupID;
			activity.Seq = this.Seq;
			activity.Title = this.ActivityTitle;
			activity.AID = this.ActivityAID;
			activity.Description = this.Description;
			activity.Comments = this.Comments;
			return ( activity );
		}
		set {
			this.ActivityID = value.ID;
			this.ActivityGroupID = value.ActivityGroupID;
			this.Seq = value.Seq;
			this.ActivityTitle = value.Title;
			this.ActivityAID = value.AID;
			this.Description = value.Description;
			this.Comments = value.Comments;
			this.DataBind();
		}
	}

	#endregion

	#region Events
	
	#region SaveItem
	public event EventHandler SaveItem {
		add { Events.AddHandler( SaveItemEventKey, value ); }
		remove { Events.RemoveHandler( SaveItemEventKey, value ); }
	}
	protected virtual void OnSaveItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SaveItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SaveItemEventKey = new object();
	#endregion

	#region CancelItem
	public event EventHandler CancelItem {
		add { Events.AddHandler( CancelItemEventKey, value ); }
		remove { Events.RemoveHandler( CancelItemEventKey, value ); }
	}
	protected virtual void OnCancelItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelItemEventKey = new object();
	#endregion

	#region DeleteItem
	public event EventHandler DeleteItem {
		add { Events.AddHandler( DeleteItemEventKey, value ); }
		remove { Events.RemoveHandler( DeleteItemEventKey, value ); }
	}
	protected virtual void OnDeleteItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[DeleteItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object DeleteItemEventKey = new object();
	#endregion
	
	#endregion

	public void SetupFocus() {
		this.ActivityTitleBox.Focus();
	}

}
