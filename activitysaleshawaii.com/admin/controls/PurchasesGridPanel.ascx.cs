using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class PurchasesGridPanel : System.Web.UI.UserControl {

	public int CustomerID {
		get {
			object obj = this.ViewState["CustomerID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set {
			this.ViewState["CustomerID"] = value;
			this.customer = null;
			this.BindDataGrid();
			this.FormPanel.DataBind();
		}
	}

	protected Customer Customer {
		get {
			if ( this.customer == null ) {
				this.customer = new Customer( this.CustomerID );
			}
			return ( this.customer );
		}
	}
	private Customer customer;

	#region Events
	
	#region FormClosing
	public event EventHandler FormClosing {
		add { Events.AddHandler( FormClosingEventKey, value ); }
		remove { Events.RemoveHandler( FormClosingEventKey, value ); }
	}
	protected virtual void OnFormClosing( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormClosingEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormClosingEventKey = new object();
	#endregion

	#region FormOpening
	public event EventHandler FormOpening {
		add { Events.AddHandler( FormOpeningEventKey, value ); }
		remove { Events.RemoveHandler( FormOpeningEventKey, value ); }
	}
	protected virtual void OnFormOpening( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormOpeningEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormOpeningEventKey = new object();
	#endregion
	
	#endregion

	protected void EditItem( Object sender, EventArgs args ) {
		this.ShowForm();
		int itinID = Convert.ToInt32( this.Grid.LastRowClickedKey );
		this.FormPanel.Itinerary = new Itinerary( itinID );
	}

	protected void CancelItem( Object sender, EventArgs args ) {
		this.HideForm();
	}

	protected void ShowForm() {
		this.OnFormOpening( this, new EventArgs() );
		this.FormPanel.Visible = true;
		this.GridPanel.Visible = false;
		int itinID = Convert.ToInt32( this.Grid.LastRowClickedKey );
		this.FormPanel.Itinerary = new Itinerary( itinID );
		this.FormPanel.CustomerName = this.Customer.FullName;
		this.FormPanel.AtcoCustomerID = this.Customer.UID;
		this.FormPanel.WebsiteName = this.Customer.WebsiteName;
		this.FormPanel.SetupFocus();
	}

	protected void HideForm() {
		this.OnFormClosing( this, new EventArgs() );
		this.BindDataGrid();
		this.FormPanel.Visible = false;
		this.GridPanel.Visible = true;
	}

	private void BindDataGrid() {
		// Re-instantiate to insure that the activities are reloaded
		Customer cust = new Customer( this.CustomerID );
		this.Grid.DataSource = cust.Itineraries;
		this.Grid.DataBind();
	}

	protected void SaveItem( Object sender, EventArgs args ) {
		Itinerary itin = this.FormPanel.Itinerary;
		itin.Persist();
		this.HideForm();
	}

}
