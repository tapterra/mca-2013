using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;
using StarrTech.WebTools.Controls;

public partial class ActivitiesGridPanel : System.Web.UI.UserControl {

	#region Properties

	public int ActivityGroupID {
		get {
			object obj = this.ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set {
			this.ViewState["ActivityGroupID"] = value;
			this.activityGroup = null;
			this.BindDataGrid();
			this.FormPanel.DataBind();
		}
	}

	protected ActivityGroup ActivityGroup {
		get {
			if ( this.activityGroup == null ) {
				this.activityGroup = new ActivityGroup( this.ActivityGroupID );
			}
			return ( this.activityGroup );
		}
	}
	private ActivityGroup activityGroup;

	public string ActivityGroupName {
		get { return ( this.FormPanel.ActivityGroupName ); }
		set { this.FormPanel.ActivityGroupName = value; }
	}

	public string VendorName {
		get { return ( this.FormPanel.VendorName ); }
		set { this.FormPanel.VendorName = value; }
	}

	public string WebsiteName {
		get { return ( this.FormPanel.WebsiteName ); }
		set { this.FormPanel.WebsiteName = value; }
	}

	#endregion

	#region Events

	#region FormClosing
	public event EventHandler FormClosing {
		add { Events.AddHandler( FormClosingEventKey, value ); }
		remove { Events.RemoveHandler( FormClosingEventKey, value ); }
	}
	protected virtual void OnFormClosing( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormClosingEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormClosingEventKey = new object();
	#endregion

	#region FormOpening
	public event EventHandler FormOpening {
		add { Events.AddHandler( FormOpeningEventKey, value ); }
		remove { Events.RemoveHandler( FormOpeningEventKey, value ); }
	}
	protected virtual void OnFormOpening( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormOpeningEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormOpeningEventKey = new object();
	#endregion

	#endregion

	#region Event Handlers

	public void DoReorderUp( Object sender, DataGridClickEventArgs args ) {
		ActivityGroup group = this.ActivityGroup;
		group.Activities.MoveUp( args.RecordSeq );
		this.BindDataGrid();
	}

	public void DoReorderDown( Object sender, DataGridClickEventArgs args ) {
		ActivityGroup group = this.ActivityGroup;
		group.Activities.MoveDown( args.RecordSeq );
		this.BindDataGrid();
	}

	protected void AddItem( Object sender, EventArgs args ) {
		this.ShowForm();
		Activity activity = new Activity();
		activity.ActivityGroupID = this.ActivityGroupID;
		activity.Seq = this.Grid.DataKeys.Count;
		this.FormPanel.Activity = activity;
	}

	protected void EditItem( Object sender, EventArgs args ) {
		this.ShowForm();
		int activityID = Convert.ToInt32( this.Grid.LastRowClickedKey );
		this.FormPanel.Activity = new Activity( activityID );
	}

	protected void CancelItem( Object sender, EventArgs args ) {
		this.HideForm();
	}

	protected void DeleteItem( Object sender, EventArgs args ) {
		Activity activity = this.FormPanel.Activity;
		activity.Delete();
		this.HideForm();
	}

	protected void SaveItem( Object sender, EventArgs args ) {
		Activity activity = this.FormPanel.Activity;
		activity.Persist();
		this.HideForm();
	}

	#endregion

	protected void ShowForm() {
		this.OnFormOpening( this, new EventArgs() );
		this.FormPanel.Visible = true;
		this.GridPanel.Visible = false;
		this.FormPanel.SetupFocus();
	}

	protected void HideForm() {
		this.OnFormClosing( this, new EventArgs() );
		this.BindDataGrid();
		this.FormPanel.Visible = false;
		this.GridPanel.Visible = true;
	}

	private void BindDataGrid() {
		// Re-instantiate to insure that the activities are reloaded
		ActivityGroup group = new ActivityGroup( this.ActivityGroupID );
		this.Grid.DataSource = group.Activities;
		this.Grid.DataBind();
	}

}
