<%@ Control Language="C#" ClassName="SubcatFormPanel" %>
<%@ import namespace="ABS" %>

<script runat="server">

	public int SubcategoryID {
		get {
			object obj = ViewState["SubcategoryID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["SubcategoryID"] = value; }
	}

	public int CategoryID {
		get {
			object obj = ViewState["CategoryID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { 
			ViewState["CategoryID"] = value;
			if ( CategoriesList.Current.Contains( value ) ) {
				this.CategoryBox.Text = CategoriesList.Current.GetItemByID( value ).Label;
			}
		}
	}

	public int Seq {
		get {
			object obj = ViewState["Seq"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["Seq"] = value; }
	}

	public string SubcategoryLabel {
		get { return ( this.SubcategoryLabelBox.Text ); }
		set { this.SubcategoryLabelBox.Text = value; }
	}

	public string Comments {
		get { return ( this.CommentsBox.Text ); }
		set { this.CommentsBox.Text = value; }
	}

	public Subcategory Subcategory {
		get {
			Subcategory subcat = new Subcategory( this.SubcategoryID );
			subcat.CategoryID = this.CategoryID;
			subcat.Seq = this.Seq;
			subcat.Label = this.SubcategoryLabel;
			subcat.Comments = this.Comments;
			return ( subcat );
		}
		set {
			this.SubcategoryID = value.ID;
			this.CategoryID = value.CategoryID;
			this.Seq = value.Seq;
			this.SubcategoryLabel = value.Label;
			this.Comments = value.Comments;
			this.DataBind();
		}
	}

</script>

<script runat="server">

	// SaveItem
	public event EventHandler SaveItem {
		add { Events.AddHandler( SaveItemEventKey, value ); }
		remove { Events.RemoveHandler( SaveItemEventKey, value ); }
	}
	protected virtual void OnSaveItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SaveItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SaveItemEventKey = new object();

	// CancelItem
	public event EventHandler CancelItem {
		add { Events.AddHandler( CancelItemEventKey, value ); }
		remove { Events.RemoveHandler( CancelItemEventKey, value ); }
	}
	protected virtual void OnCancelItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelItemEventKey = new object();

	// DeleteItem
	public event EventHandler DeleteItem {
		add { Events.AddHandler( DeleteItemEventKey, value ); }
		remove { Events.RemoveHandler( DeleteItemEventKey, value ); }
	}
	protected virtual void OnDeleteItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[DeleteItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object DeleteItemEventKey = new object();

</script>

<script runat="server">

	public void SetupFocus() {
		this.SubcategoryLabelBox.Focus();
	}

</script>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
	
	<tr>
		<td width="50%" style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Subcategory Form" cssclass="h2" />
		</td>
		<td align="right" style="padding-bottom:20px">
			<stic:button Runat="server" ID="SaveButton" Text="Save" tabindex="9" onclick="OnSaveItem" />
			<stic:button Runat="server" ID="CancelButton" Text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancelItem" />
			<stic:button Runat="server" ID="DeleteButton" Text="Delete" tabindex="9" causesvalidation="false" onclick="OnDeleteItem" 
				onclientclick="return confirm( 'Are you sure you want to delete this Subcategory?' );"
			/>
		</td>
	</tr>

	<tr>
		<td width="50%">
			<stic:labeledtextbox runat="server" id="CategoryBox" label="Category"
			 enabled="false"
			/>
		</td>
		<td>
			<stic:labeledtextbox runat="server" id="SubcategoryLabelBox" label="Subcategory Label"
			 frame-labelmark="*"
			/>
			<asp:requiredfieldvalidator runat="server" display="none" 
			 id="SubcategoryLabelReqVal" controltovalidate="SubcategoryLabelBox" 
			 setfocusonerror="true" errormessage="A value is required for Subcategory Label."
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
			 textmode="MultiLine" rows="4" wrap="true"
			/>
		</td>
	</tr>
	
</table>
