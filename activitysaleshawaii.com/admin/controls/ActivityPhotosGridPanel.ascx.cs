using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.Controls;
using StarrTech.WebTools.DBUI;

using ABS;

public partial class ActivityPhotosGridPanel : System.Web.UI.UserControl {

	#region Properties

	public int ActivityGroupID {
		get {
			object obj = this.ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set {
			this.ViewState["ActivityGroupID"] = value;
			this.activityGroup = null;
			this.BindDataGrid();
			this.FormPanel.DataBind();
		}
	}

	protected ActivityGroup ActivityGroup {
		get {
			if ( this.activityGroup == null ) {
				this.activityGroup = new ActivityGroup( this.ActivityGroupID );
			}
			return ( this.activityGroup );
		}
	}
	private ActivityGroup activityGroup;

	public string ActivityGroupName {
		get { return ( this.FormPanel.ActivityGroupName ); }
		set { this.FormPanel.ActivityGroupName = value; }
	}

	public string VendorName {
		get { return ( this.FormPanel.VendorName ); }
		set { this.FormPanel.VendorName = value; }
	}

	public string WebsiteName {
		get { return ( this.FormPanel.WebsiteName ); }
		set { this.FormPanel.WebsiteName = value; }
	}

	#endregion

	#region Events

	#region FormClosing
	public event EventHandler FormClosing {
		add { Events.AddHandler( FormClosingEventKey, value ); }
		remove { Events.RemoveHandler( FormClosingEventKey, value ); }
	}
	protected virtual void OnFormClosing( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormClosingEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormClosingEventKey = new object();
	#endregion

	#region FormOpening
	public event EventHandler FormOpening {
		add { Events.AddHandler( FormOpeningEventKey, value ); }
		remove { Events.RemoveHandler( FormOpeningEventKey, value ); }
	}
	protected virtual void OnFormOpening( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormOpeningEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormOpeningEventKey = new object();
	#endregion

	#endregion

	#region Event Handlers

	public void DoReorderUp( Object sender, DataGridClickEventArgs args ) {
		ActivityGroup group = this.ActivityGroup;
		group.Photos.MoveUp( args.RecordSeq );
		this.BindDataGrid();
	}

	public void DoReorderDown( Object sender, DataGridClickEventArgs args ) {
		ActivityGroup group = this.ActivityGroup;
		group.Photos.MoveDown( args.RecordSeq );
		this.BindDataGrid();
	}

	protected void AddItem( Object sender, EventArgs args ) {
		this.ShowForm();
		ActivityPhoto photo = new ActivityPhoto();
		photo.ActivityGroupID = this.ActivityGroupID;
		photo.Seq = this.PhotosGrid.DataKeys.Count;
		this.FormPanel.ActivityPhoto = photo;
	}

	protected void EditItem( Object sender, EventArgs args ) {
		this.ShowForm();
		int photoID = Convert.ToInt32( this.PhotosGrid.LastRowClickedKey );
		this.FormPanel.ActivityPhoto = new ActivityPhoto( photoID );
	}

	protected void CancelItem( Object sender, EventArgs args ) {
		this.HideForm();
	}

	protected void DeleteItem( Object sender, EventArgs args ) {
		ActivityPhoto photo = this.FormPanel.ActivityPhoto;
		photo.Delete();
		this.HideForm();
	}

	protected void SaveItem( Object sender, EventArgs args ) {
		ActivityPhoto photo = this.FormPanel.ActivityPhoto;
		photo.Persist();
		this.HideForm();
	}

	#endregion

	protected void ShowForm() {
		this.OnFormOpening( this, new EventArgs() );
		this.FormPanel.Visible = true;
		this.GridPanel.Visible = false;
	}

	protected void HideForm() {
		this.OnFormClosing( this, new EventArgs() );
		this.BindDataGrid();
		this.FormPanel.Visible = false;
		this.GridPanel.Visible = true;
	}

	private void BindDataGrid() {
		// Re-instantiate to insure that the photos are reloaded
		ActivityGroup group = new ActivityGroup( this.ActivityGroupID );
		this.PhotosGrid.DataSource = group.Photos;
		this.PhotosGrid.DataBind();
	}

}
