<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PurchasesGridPanel.ascx.cs" Inherits="PurchasesGridPanel" %>
<%@ import namespace="ABS" %>
<%@ register tagPrefix="adm" tagName="purchaseform" src="PurchaseFormPanel.ascx" %>

<table runat="server" id="GridPanel" cellpadding="0" cellspacing="0" border="0" style="width:700; margin-top:20px">
	<tr>
		<td width="50%" style="padding-bottom:0px">
			<asp:Label id="HeaderLabel" runat="server" text="Purchase History" cssclass="h3" />
		</td>
		<td align="right" style="padding-bottom:0px">
		</td>
	</tr>
	<tr>
		<td colspan="2" >
			<stic:datagrid runat="server" id="Grid"
			 cellpadding="3" cellspacing="1" 
			 autogeneratecolumns="false" datakeyfield="id"
			 allowcustompaging="false" allowpaging="false" allowsorting="false" 
			 onrowclicked="EditItem"
			 includereordercontrols="false"
			 width="700"
			>
				<columns>
					<asp:boundcolumn headertext="ID"											datafield="ID" visible="false" /> 
					<asp:boundcolumn headertext="Purchase Date/Time"			datafield="CreatedDateTimeStr"	/> 
					<asp:boundcolumn headertext="Account Code"						datafield="LoginAccountNumber"	/> 

					<asp:boundcolumn headertext="# Bookings"							datafield="BookingsCount"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					/>
					<asp:boundcolumn headertext="Bookings $"							datafield="TotalBookingsBasePrice"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					 dataformatstring="{0:f}" 
					/>

					<asp:boundcolumn headertext="# Res"										datafield="ReservationsCount"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					/>
					<asp:boundcolumn headertext="Res $"										datafield="TotalReservationsPrice"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					 dataformatstring="{0:f}" 
					/>

					<asp:boundcolumn headertext="Total $"									datafield="TotalBasePrice"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					 dataformatstring="{0:f}" 
					/>
					<asp:boundcolumn headertext="Payments"								datafield="TotalPayments"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					 dataformatstring="{0:f}" 
					/>
					<asp:boundcolumn headertext="ATCO Voucher ID"					datafield="VoucherTransactionIDStr"	/> 
				</columns>
			</stic:datagrid>
		</td>
	</tr>
</table>

<adm:purchaseform runat="server" id="FormPanel" visible="false" 
 oncancelitem="CancelItem"
 onsaveitem="SaveItem"
/>
