using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class PurchaseFormPanel : System.Web.UI.UserControl {

	#region Properties

	public int ItineraryID {
		get {
			object obj = ViewState["ItineraryID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { 
			ViewState["ItineraryID"] = value;
			this.ItineraryPanel.ItineraryID = value;
		}
	}

	public int CustomerID {
		get {
			object obj = ViewState["CustomerID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["CustomerID"] = value; }
	}

	public string CustomerName {
		set { this.CustomerNameBox.Text = value; }
	}

	public DateTime PurchaseDateTime {
		set { this.PurchaseDateTimeBox.Text = value.ToString(); }
	}

	public string AtcoCustomerID {
		set { this.AtcoCustomerIDBox.Text = value; }
	}

	public string WebsiteName {
		set { this.WebsiteBox.Text = value; }
	}

	public string Comments {
		get { return ( this.CommentsBox.Text ); }
		set { this.CommentsBox.Text = value; }
	}

	public Itinerary Itinerary {
		get {
			Itinerary itin = new Itinerary( this.ItineraryID );
			itin.Comments = this.Comments;
			return ( itin );
		}
		set {
			this.ItineraryID = value.ID;
			this.CustomerID = value.CustomerID;
			//this.CustomerName = value.CustomerFullName;
			this.PurchaseDateTime = value.CreatedDateTime;
			this.Comments = value.Comments;
			this.DataBind();
			this.VoucherHolder.Text = value.VoucherHtml;
			this.VouchersButton.Enabled = !string.IsNullOrEmpty( this.VoucherHolder.Text );
		}
	}

	#endregion

	#region Events
	#region SaveItem
	public event EventHandler SaveItem {
		add { Events.AddHandler( SaveItemEventKey, value ); }
		remove { Events.RemoveHandler( SaveItemEventKey, value ); }
	}
	protected virtual void OnSaveItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SaveItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SaveItemEventKey = new object();
	#endregion
	#region CancelItem
	public event EventHandler CancelItem {
		add { Events.AddHandler( CancelItemEventKey, value ); }
		remove { Events.RemoveHandler( CancelItemEventKey, value ); }
	}
	protected virtual void OnCancelItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelItemEventKey = new object();
	#endregion
	#endregion

	public void SetupFocus() {
		this.CommentsBox.Focus();
	}

}
