<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityOptionsGridPanel.ascx.cs" Inherits="ActivityOptionsGridPanel" %>
<%@ import namespace="ABS" %>
<%@ register tagPrefix="adm" tagName="optionform" src="ActivityOptionFormPanel.ascx" %>

<table runat="server" id="GridPanel" cellpadding="0" cellspacing="0" border="0" style="width:700; margin-top:20px">
	<tr>
		<td width="50%" style="padding-bottom:0px">
			<asp:Label id="HeaderLabel" runat="server" text="Activity Options" cssclass="h3" />
		</td>
		<td align="right" style="padding-bottom:0px">
			<stic:button runat="server" id="AddButton" text="Add New Option" tabindex="9" 
				causesvalidation="true" style="width:140px;" 
				onclick="AddItem"
			/>
		</td>
	</tr>
	<tr>
		<td colspan="2" >
			<stic:datagrid runat="server" id="Grid"
			 cellpadding="3" cellspacing="1" 
			 autogeneratecolumns="false" datakeyfield="id"
			 allowcustompaging="false" allowpaging="false" allowsorting="false" 
			 onrowclicked="EditItem"
			 includereordercontrols="true"
			 onreorderup="DoReorderUp"
			 onreorderdown="DoReorderDown"
			 width="700"
			>
				<columns>
					<asp:boundcolumn headertext="ID"									datafield="ID" visible="false" /> 
					<asp:boundcolumn headertext="Question Text"				datafield="QuestionText"	/> 
					<asp:boundcolumn headertext="Response Type"				datafield="ResponseType" />
					<asp:boundcolumn headertext="Response Choices"		datafield="ResponseChoices" />
				</columns>
			</stic:datagrid>
		</td>
	</tr>
</table>

<adm:optionform runat="server" id="FormPanel" visible="false" 
 oncancelitem="CancelItem"
 ondeleteitem="DeleteItem"
 onsaveitem="SaveItem"
/>
