using System;
using System.Web.UI.WebControls;
using StarrTech.WebTools.Controls;
using ABS;

public partial class CustomersEditor : System.Web.UI.UserControl {

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}

	protected Customer Customer {
		get { return ( this.AdminGrid.DBItem as Customer ); }
	}

	protected Panel CustomerFormPanel {
		get { return ( (Panel) AdminGrid.FindControl( "FormControls:CustomerFormPanel" ) ); }
	}

	protected PurchasesGridPanel PurchasesGrid {
		get { return ( (PurchasesGridPanel) AdminGrid.FindControl( "FormControls:PurchasesGrid" ) ); }
	}

	#region Event Handlers

	public void ClearSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledTextBox SNameBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SNameBox" );
		LabeledTextBox SEmailBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SEmailBox" );
		LabeledTextBox SAtcoIDBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SAtcoIDBox" );
		LabeledTextBox SZipCodeBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SZipCodeBox" );
		// Reset the controls
		SWebsiteLBox.SelectedIndex = 0;
		SNameBox.Text = "";
		SEmailBox.Text = "";
		SAtcoIDBox.Text = "";
		SZipCodeBox.Text = "";
	}

	public void ReadSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledTextBox SNameBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SNameBox" );
		LabeledTextBox SEmailBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SEmailBox" );
		LabeledTextBox SAtcoIDBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SAtcoIDBox" );
		LabeledTextBox SZipCodeBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SZipCodeBox" );
		// Clear out any previous search values
		Customer.NewSearch();
		// Assign values to the static search properties
		Customer.SWebsiteID = Convert.ToInt32( SWebsiteLBox.SelectedValue );
		Customer.SName = SNameBox.Text;
		Customer.SEmail = SEmailBox.Text;
		Customer.SAtcoID = SAtcoIDBox.Text;
		Customer.SZipCode = SZipCodeBox.Text;
	}

	public void InitFormControls( Object sender, EventArgs args ) {}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		LabeledDropDownList WebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:WebsiteLBox" );
		LabeledTextBox CreatedDTBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CreatedDTBox" );
		LabeledTextBox FirstNameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:FirstNameBox" );
		LabeledTextBox LastNameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:LastNameBox" );
		LabeledTextBox EmailAddressBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:EmailAddressBox" );
		LabeledTextBox PasswordBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:PasswordBox" );
		LabeledTextBox BillingNameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingNameBox" );
		LabeledTextBox BillingStreet1Box = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingStreet1Box" );
		LabeledTextBox BillingStreet2Box = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingStreet2Box" );
		LabeledTextBox BillingCityBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingCityBox" );
		LabeledTextBox BillingStateBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingStateBox" );
		LabeledTextBox BillingZipBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:BillingZipBox" );
		LabeledTextBox TelephoneBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:TelephoneBox" );
		LabeledTextBox CustomerAIDBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CustomerAIDBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );
		// Get a reference to the business object to be edited...
		//Customer customer = new Customer( this.AdminGrid.FormController.RecordID );
		Customer customer = new Customer();
		customer.ID = this.AdminGrid.FormController.RecordID;
		customer.CreatedDT = DateTime.Parse( CreatedDTBox.Text );
		customer.WebsiteID = Convert.ToInt32( WebsiteLBox.SelectedValue );
		customer.FirstName = FirstNameBox.Text;
		customer.LastName = LastNameBox.Text;
		customer.EmailAddress = EmailAddressBox.Text;
		customer.Password = PasswordBox.Text;
		customer.BillingName = BillingNameBox.Text;
		customer.BillingStreet1 = BillingStreet1Box.Text;
		customer.BillingStreet2 = BillingStreet2Box.Text;
		customer.BillingCity = BillingCityBox.Text;
		customer.BillingState = BillingStateBox.Text;
		customer.BillingZip = BillingZipBox.Text;
		customer.Telephone = TelephoneBox.Text;
		customer.CustomerAID = CustomerAIDBox.Text;
		customer.Comments = CommentsBox.Text;
		this.AdminGrid.DBItem = customer;
	}

	public void InitEditor() {
		// Initialize the AdminDataGrid...
		this.AdminGrid.DBItemType = typeof( Customer );
		// Set up the grid for the user's permissions
		this.AdminGrid.CanAddRecord = false;
		this.AdminGrid.CanDeleteRecord = false;
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminCustomersUpdate );
		} else {
			this.AdminGrid.CanEditRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			// Set up the special search controls
			this.SetupSWebsiteLBox();
			// Set up the special form controls
			this.SetupWebsiteLBox();
			this.AdminGrid.GridController.SortExpression = "last_name, first_name";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	protected void PurchaseFormOpening( object sender, System.EventArgs e ) {
		// The Option Form is opening ...
		// Make sure our parent record has been saved
		if ( this.AdminGrid.FormController.RecordID <= 0 ) {
			this.Customer.Persist();
		}
		// Update the static Customer fields
		this.ReadFormControls( null, null );
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.CustomerFormPanel.Visible = false;
		this.PurchasesGrid.Visible = true;
	}

	protected void PurchaseFormClosing( object sender, System.EventArgs e ) {
		// The Option Form is closing - show the main Customer form
		this.AdminGrid.FormController.Visible = true;
		this.CustomerFormPanel.Visible = true;
		this.PurchasesGrid.Visible = true;
	}

	#endregion

	#region Private Methods

	private void SetupSWebsiteLBox() {
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		SWebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			SWebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		SWebsiteLBox.SelectedIndex = 0;
	}

	private void SetupWebsiteLBox() {
		LabeledDropDownList WebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:WebsiteLBox" );
		WebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			WebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		WebsiteLBox.SelectedIndex = 0;
	}

	#endregion

}
