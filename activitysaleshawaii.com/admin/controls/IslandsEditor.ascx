<%@ Control Language="C#" ClassName="IslandsEditor" %>
<%@ import namespace="ABS" %>

<script runat="server">

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}
	
	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		LabeledTextBox NameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:NameBox" );
		LabeledTextBox AidBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:AidBox" );
		LabeledTextBox KeyBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:KeyBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );
		
		// Get a reference to the business object to be edited...
		Island island = new Island( this.AdminGrid.FormController.RecordID );
		island.Name = NameBox.Text;
		island.AID = AidBox.Text;
		island.KeyName = KeyBox.Text;
		island.Comments = CommentsBox.Text;
		
		this.AdminGrid.DBItem = island;
	}
	
	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( Island );
		
		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminIslandsUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminIslandsAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminIslandsDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			this.AdminGrid.GridController.SortExpression = "seq";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}
	
	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	public void DoReorderUp( Object sender, DataGridClickEventArgs args ) {
		Island island = new Island( args.RecordID );
		island.MoveUp();
		this.ResetEditor();
	}

	public void DoReorderDown( Object sender, DataGridClickEventArgs args ) {
		Island island = new Island( args.RecordID );
		island.MoveDown();
		this.ResetEditor();
	}

</script>

<dbui:admindatagrid runat="server" id="AdminGrid" OnFormControlsRead="ReadFormControls" 
 allowusersorting="false" includereordercontrols="true" onreorderdown="DoReorderDown" onreorderup="DoReorderUp"
>
	
	<formcontrols>
	
		<table border="0" cellpadding="2" cellspacing="0" width="100%">
			
			<tr>
				<td width="40%">
					<stic:labeledtextbox runat="server" id="NameBox" label="Name"
					 text='<%# ((ABS.Island) AdminGrid.DBItem).Name %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="NameBoxReqVal" controltovalidate="NameBox" 
					 setfocusonerror="true" errormessage="A value is required for Name."
					/>
				</td>
				<td width="30%">
					<stic:labeledtextbox runat="server" id="AidBox" label="ATCO Island ID"
					 text='<%# ((ABS.Island) AdminGrid.DBItem).AID %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator1" controltovalidate="AidBox" 
					 setfocusonerror="true" errormessage="A value is required for ATCO Island ID."
					/>
				</td>
				<td width="30%">
					<stic:labeledtextbox runat="server" id="KeyBox" label="Island Key"
					 text='<%# ((ABS.Island) AdminGrid.DBItem).KeyName %>'
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator2" controltovalidate="KeyBox" 
					 setfocusonerror="true" errormessage="A value is required for Island Key."
					/>
				</td>
			</tr>
			
			<tr>
				<td colspan="3">
					<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
					 text='<%# ((ABS.Island) AdminGrid.DBItem).Comments %>'
					 textmode="MultiLine" rows="6" wrap="true"
					/>
				</td>
			</tr>
			
		</table>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Island"				datafield="Name"			/> 
		<asp:boundcolumn headertext="ATCO ID"				datafield="AID"				/> 
		<asp:boundcolumn headertext="Key"						datafield="KeyName"		/> 
	</gridcolumns>
	
</dbui:admindatagrid>
