<%@ Control Language="C#" ClassName="SubcatGridPanel" %>
<%@ register tagPrefix="adm" tagName="subcatform" src="SubcatFormPanel.ascx" %>
<%@ import namespace="ABS" %>

<script runat="server">

	protected Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryID );
			}
			return ( this.category );
		}
	}
	private Category category;

	public int CategoryID {
		get { return ( this.FormPanel.CategoryID ); }
		set { 
			this.FormPanel.CategoryID = value;
			this.BindDataGrid();
			this.FormPanel.DataBind();
		}
	}

	public string Width {
		get { return ( this.GridPanel.Width ); }
		set { this.GridPanel.Width = value; }
	}
	
</script>
	
<script runat="server">

	public event EventHandler FormClosing {
		add { Events.AddHandler( FormClosingEventKey, value ); }
		remove { Events.RemoveHandler( FormClosingEventKey, value ); }
	}
	protected virtual void OnFormClosing( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormClosingEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormClosingEventKey = new object();

	
	public event EventHandler FormOpening {
		add { Events.AddHandler( FormOpeningEventKey, value ); }
		remove { Events.RemoveHandler( FormOpeningEventKey, value ); }
	}
	protected virtual void OnFormOpening( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[FormOpeningEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object FormOpeningEventKey = new object();

</script>
	
<script runat="server">

	public void DoReorderUp( Object sender, DataGridClickEventArgs args ) {
		Category cat = this.Category;
		cat.Subcategories.MoveUp( args.RecordSeq );
		this.BindDataGrid();
	}

	public void DoReorderDown( Object sender, DataGridClickEventArgs args ) {
		Category cat = this.Category;
		cat.Subcategories.MoveDown( args.RecordSeq );
		this.BindDataGrid();
	}

	protected void AddItem( Object sender, EventArgs args ) {
		this.ShowForm();
		Subcategory subcat = new Subcategory();
		subcat.CategoryID = this.CategoryID;
		subcat.Seq = this.Grid.DataKeys.Count;
		this.FormPanel.Subcategory = subcat;
	}

	protected void EditItem( Object sender, EventArgs args ) {
		this.ShowForm();
		int subcategoryID = Convert.ToInt32( this.Grid.LastRowClickedKey );
		this.FormPanel.Subcategory = new Subcategory( subcategoryID );
	}

	protected void CancelItem( Object sender, EventArgs args ) {
		this.HideForm();
	}

	protected void DeleteItem( Object sender, EventArgs args ) {
		Subcategory subcat = this.FormPanel.Subcategory;
		subcat.Delete();
		this.HideForm();
	}

	protected void SaveItem( Object sender, EventArgs args ) {
		Subcategory subcat = this.FormPanel.Subcategory;
		subcat.Persist();
		this.HideForm();
	}

	protected void ShowForm() {
		this.OnFormOpening( this, new EventArgs() );
		this.FormPanel.Visible = true;
		this.GridPanel.Visible = false;
		this.FormPanel.SetupFocus();
	}

	protected void HideForm() {
		this.OnFormClosing( this, new EventArgs() );
		this.BindDataGrid();
		this.FormPanel.Visible = false;
		this.GridPanel.Visible = true;
	}

	private void BindDataGrid() {
		// Re-instantiate to insure that the subcategories are reloaded
		Category cat = new Category( this.CategoryID );
		this.Grid.DataSource = cat.Subcategories;
		this.Grid.DataBind();
	}

</script>

<table runat="server" id="GridPanel" cellpadding="0" cellspacing="0" border="0" style="margin-top:20px">
	<tr>
		<td width="50%" style="padding-bottom:0px">
			<asp:Label id="HeaderLabel" runat="server" text="Subcategories" cssclass="h3" />
		</td>
		<td align="right" style="padding-bottom:0px">
			<stic:button runat="server" id="AddButton" text="Add New Subcategory" tabindex="9" 
				causesvalidation="true" style="width:160px;" 
				onclick="AddItem"
			/>
		</td>
	</tr>
	<tr>
		<td colspan="2" >
			<stic:datagrid runat="server" id="Grid"
			 cellpadding="3" cellspacing="1" 
			 autogeneratecolumns="false" datakeyfield="id"
			 allowcustompaging="false" allowpaging="false" allowsorting="false" 
			 onrowclicked="EditItem"
			 includereordercontrols="true"
			 onreorderup="DoReorderUp"
			 onreorderdown="DoReorderDown"
			 width="700"
			 pagesize="100"
			>
				<columns>
					<asp:boundcolumn headertext="ID"				   					datafield="ID" visible="false" /> 
					<asp:boundcolumn headertext="Subcategory Label"			datafield="Label"	/> 
				</columns>
			</stic:datagrid>
		</td>
	</tr>
</table>

<adm:subcatform runat="server" id="FormPanel" visible="false" 
 oncancelitem="CancelItem"
 ondeleteitem="DeleteItem"
 onsaveitem="SaveItem"
/>
