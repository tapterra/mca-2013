using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ABS;

public partial class ActivityPhotoFormPanel : System.Web.UI.UserControl {

	#region Properties

	public int ActivityPhotoID {
		get {
			object obj = ViewState["ActivityPhotoID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ActivityPhotoID"] = value; }
	}

	public int ActivityGroupID {
		get {
			object obj = ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ActivityGroupID"] = value; }
	}

	public int Seq {
		get {
			object obj = ViewState["Seq"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["Seq"] = value; }
	}

	public string ActivityGroupName {
		get { return ( this.ActivityGroupBox.Text ); }
		set { this.ActivityGroupBox.Text = value; }
	}

	public string VendorName {
		get { return ( this.VendorNameBox.Text ); }
		set { this.VendorNameBox.Text = value; }
	}

	public string WebsiteName {
		get { return ( this.WebsiteBox.Text ); }
		set { this.WebsiteBox.Text = value; }
	}

	public int ImageDocumentID {
		get {
			object obj = ViewState["ImageDocumentID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ImageDocumentID"] = value; }
	}

	public string PhotoFileName {
		get { return ( this.FileNameBox.Text ); }
		set { this.FileNameBox.Text = value; }
	}

	public string PhotoMimeType {
		get { return ( this.MimeTypeBox.Text ); }
		set { this.MimeTypeBox.Text = value; }
	}

	public string PhotoSize {
		get { return ( this.SizeBox.Text ); }
		set { this.SizeBox.Text = value; }
	}

	public string Comments {
		get { return ( this.CommentsBox.Text ); }
		set { this.CommentsBox.Text = value; }
	}

	public ActivityPhoto ActivityPhoto {
		get {
			ActivityPhoto photo = new ActivityPhoto( this.ActivityPhotoID );
			photo.ActivityGroupID = this.ActivityGroupID;
			photo.Seq = this.Seq;
			photo.Comments = this.Comments;
			if ( this.FileUploadBox.PostedFile.ContentLength > 0 ) {
				ImageDocument pixDoc = new ImageDocument( this.FileUploadBox.PostedFile );
				pixDoc.Persist();
				photo.ImageDocumentID = pixDoc.ID;
			} else {
				photo.ImageDocumentID = this.ImageDocumentID;
			}
			return ( photo );
		}
		set {
			this.ActivityPhotoID = value.ID;
			this.ActivityGroupID = value.ActivityGroupID;
			this.Seq = value.Seq;
			this.Comments = value.Comments;
			this.PhotoFileName = value.PhotoFileName;
			this.PhotoMimeType = value.PhotoMimeType;
			this.PhotoSize = value.PhotoDataSizeStr;
			this.ImageDocumentID = value.ImageDocumentID;
			if ( value.ImageDocumentID > 0 ) {
				this.PreviewImage.ImageUrl = value.PhotoThumbnailUrl;
				this.PreviewImage.NavigateUrl = string.Format( ABS.ImageDocument.ImageUrlFormat, value.ImageDocumentID );
				this.PreviewImage.Visible = true;
				this.FileUploadReqVal.Enabled = false;
			} else {
				PreviewImage.Visible = false;
				this.FileUploadReqVal.Enabled = true;
			}
			this.DataBind();
		}
	}

	#endregion

	#region Event Handlers

	public void DownloadPhoto( Object sender, EventArgs args ) {
		int id = this.ActivityPhotoID;
		if ( id > 0 ) {
			ActivityPhoto photo = new ActivityPhoto( id );
			this.Response.Redirect( string.Format( ImageDocument.DownloadUrlFormat, photo.ImageDocumentID ) );
		}
	}

	#endregion

	#region Events
	#region SaveItem
	public event EventHandler SaveItem {
		add { Events.AddHandler( SaveItemEventKey, value ); }
		remove { Events.RemoveHandler( SaveItemEventKey, value ); }
	}
	protected virtual void OnSaveItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SaveItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SaveItemEventKey = new object();
	#endregion
	#region CancelItem
	public event EventHandler CancelItem {
		add { Events.AddHandler( CancelItemEventKey, value ); }
		remove { Events.RemoveHandler( CancelItemEventKey, value ); }
	}
	protected virtual void OnCancelItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelItemEventKey = new object();
	#endregion
	#region DeleteItem
	public event EventHandler DeleteItem {
		add { Events.AddHandler( DeleteItemEventKey, value ); }
		remove { Events.RemoveHandler( DeleteItemEventKey, value ); }
	}
	protected virtual void OnDeleteItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[DeleteItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object DeleteItemEventKey = new object();
	#endregion
	#endregion

}
