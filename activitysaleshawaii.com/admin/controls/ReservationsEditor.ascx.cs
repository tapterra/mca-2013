using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;
using StarrTech.WebTools.Controls;

public partial class ReservationsEditor : System.Web.UI.UserControl {

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}

	protected Reservation Reservation {
		get { return ( this.AdminGrid.DBItem as Reservation ); }
	}

	#region Event Handlers

	public void ClearSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledDropDownList SProcessedLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SProcessedLBox" );

		// Reset the controls
		SWebsiteLBox.SelectedIndex = 0;
		SProcessedLBox.SelectedIndex = 0;
	}

	public void ReadSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledDropDownList SProcessedLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SProcessedLBox" );

		// Clear out any previous search values
		Reservation.NewSearch();

		// Assign values to the static search properties
		Reservation.SWebsiteID = Convert.ToInt32( SWebsiteLBox.SelectedValue );
		Reservation.SProcessed = Convert.ToInt32( SProcessedLBox.SelectedValue );
	}

	public void InitFormControls( Object sender, EventArgs args ) {
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		CheckBox IsProcessedXBox = (CheckBox) AdminGrid.FindControl( "FormControls:IsProcessedXBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );

		// Get a reference to the business object to be edited...
		Reservation res = new Reservation( this.AdminGrid.FormController.RecordID );
		res.IsProcessed = IsProcessedXBox.Checked;
		res.Comments = CommentsBox.Text;
		this.AdminGrid.DBItem = res;
	}

	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( Reservation );

		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminReservationsUpdate );
		} else {
			this.AdminGrid.CanEditRecord = false;
		}
		this.AdminGrid.CanAddRecord = false;
		this.AdminGrid.CanDeleteRecord = false;

		// Start things rolling
		if ( !this.IsPostBack ) {
			// Set up the special search controls
			this.SetupSWebsiteLBox();
			// Set up the special form controls
			this.SetupWebsiteLBox();
			this.AdminGrid.GridController.SortExpression = "r.created_dt";
			this.AdminGrid.GridController.SortOrder = "desc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	#endregion

	#region Private Methods

	private void SetupSWebsiteLBox() {
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		SWebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			SWebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		SWebsiteLBox.SelectedIndex = 0;
	}

	private void SetupWebsiteLBox() {
		LabeledDropDownList WebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:WebsiteLBox" );
		WebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			WebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		WebsiteLBox.SelectedIndex = 0;
	}

	#endregion

}
