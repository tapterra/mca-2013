<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityPhotosGridPanel.ascx.cs" Inherits="ActivityPhotosGridPanel" %>
<%@ import namespace="ABS" %>
<%@ register tagPrefix="adm" tagName="photoform" src="ActivityPhotoFormPanel.ascx" %>

<table runat="server" id="GridPanel" cellpadding="0" cellspacing="0" border="0" style="width:700; margin-top:20px">
	<tr>
		<td width="50%" style="padding-bottom:0px">
			<asp:Label id="HeaderLabel" runat="server" text="Activity Photos" cssclass="h3" />
		</td>
		<td align="right" style="padding-bottom:0px">
			<stic:button runat="server" id="AddButton" text="Add New Photo" tabindex="9" 
				causesvalidation="true" style="width:140px;" 
				onclick="AddItem"
			/>
		</td>
	</tr>
	<tr>
		<td colspan="2" >
			<stic:datagrid runat="server" id="PhotosGrid"
			 cellpadding="3" cellspacing="1" 
			 autogeneratecolumns="false" datakeyfield="id"
			 allowcustompaging="false" allowpaging="false" allowsorting="false" 
			 onrowclicked="EditItem"
			 includereordercontrols="true"
			 onreorderup="DoReorderUp"
			 onreorderdown="DoReorderDown"
			 width="700"
			>
				<columns>
					<asp:boundcolumn headertext="ID"									datafield="ID" visible="false" /> 
					<asp:boundcolumn headertext="Photo File Name"			datafield="PhotoFileName"	/> 
					<asp:boundcolumn headertext="MIME Type"						datafield="PhotoMIMEType" 
					 itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" 
					/> 
					<asp:boundcolumn headertext="Posted"							datafield="PhotoPostedDTStr" /> 
					<asp:boundcolumn headertext="Size"								datafield="PhotoDataSizeStr"
					 itemstyle-horizontalalign="right" headerstyle-horizontalalign="right" 
					/>
				</columns>
			</stic:datagrid>
		</td>
	</tr>
</table>

<adm:photoform runat="server" id="FormPanel" visible="false" 
 oncancelitem="CancelItem"
 ondeleteitem="DeleteItem"
 onsaveitem="SaveItem"
/>
