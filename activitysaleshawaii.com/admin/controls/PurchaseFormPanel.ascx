<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PurchaseFormPanel.ascx.cs" Inherits="PurchaseFormPanel" %>
<%@ register tagprefix="adm" tagname="itinpanel" src="ItinPanel.ascx" %>
<%@ import namespace="ABS" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
	
	<tr>
		<td colspan="2" style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Customer Itinerary" cssclass="h2" />
		</td>
		<td colspan="2" align="right" style="padding-bottom:20px">
			<stic:button runat="server" id="SaveButton" text="Save" tabindex="9" onclick="OnSaveItem" />
			<stic:button runat="server" id="CancelButton" text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancelItem" />
		</td>
	</tr>

	<tr>
		<td width="30%">
			<stic:labeledtextbox runat="server" id="CustomerNameBox" label="Customer Name"
			 enabled="false"
			/>
		</td>
		<td width="25%">
			<stic:labeledtextbox runat="server" id="PurchaseDateTimeBox" label="Purchase Date/Time"
			 enabled="false"
			/>
		</td>
		<td width="25%">
			<stic:labeledtextbox runat="server" id="AtcoCustomerIDBox" label="ATCO Customer ID"
			 enabled="false"
			/>
		</td>
		<td width="20%">
			<stic:labeledtextbox runat="server" id="WebsiteBox" label="Website"
			 enabled="false"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="3">&nbsp;</td>
		<td>
			<stic:button runat="server" id="VouchersButton" text="Show Vouchers" tabindex="1" 
				causesvalidation="false" usesubmitbehavior="false" 
				onclientclick="showVoucher();" 
				style="width:140px"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
			 textmode="MultiLine" rows="4" wrap="true"
			 tabindex="1"
			/>
		</td>
	</tr>
	
</table>

<adm:itinpanel runat="server" id="ItineraryPanel"
/>

<script type="text/javascript">
	function showVoucher() {
		if ( document.getElementById != null ) {
			var html = "";
			var voucherPanel = document.getElementById( "voucherPanel" );
			if ( voucherPanel != null ) {
				html += voucherPanel.innerHTML;
			}
			//var win = window.open("","vouchers", "height=1000,width=1000,top=20,left=20,directories=yes,location=yes,menubar=yes,toolbar=no,status=yes,resizeable=yes" );
			var win = window.open("","vouchers", "" );
			win.document.open();
			win.document.write(html);
			win.document.close();
		}
	}
</script>

<div id="voucherPanel" style="width:500px; visibility:hidden" >
	<asp:literal runat="server" id="VoucherHolder" />
</div>
