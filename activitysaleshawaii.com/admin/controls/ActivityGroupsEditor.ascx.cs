using System;
using System.Web.UI.WebControls;
using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.Controls;
using ABS;

public partial class ActivityGroupsEditor : System.Web.UI.UserControl {

	#region Properties

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}

	protected ActivityGroup Group {
		get {
			return ( this.AdminGrid.DBItem as ActivityGroup );
		}
	}

	protected Panel ActivityGroupFormPanel {
		get { return ( (Panel) AdminGrid.FindControl( "FormControls:ActivityGroupFormPanel" ) ); }
	}

	protected ActivityPhotosGridPanel PhotosGrid {
		get { return ( (ActivityPhotosGridPanel) AdminGrid.FindControl( "FormControls:PhotosGrid" ) ); }
	}

	protected ActivityOptionsGridPanel OptionsGrid {
		get { return ( (ActivityOptionsGridPanel) AdminGrid.FindControl( "FormControls:OptionsGrid" ) ); }
	}

	protected ASP.WebsitePickerPanel SitePicker {
		get { return ( (ASP.WebsitePickerPanel) AdminGrid.FindControl( "FormControls:SitePicker" ) ); }
	}

	protected ActivitiesGridPanel ActivitiesGrid {
		get { return ( (ActivitiesGridPanel) AdminGrid.FindControl( "FormControls:ActivitiesGrid" ) ); }
	}

	protected System.Web.UI.WebControls.Button DuplicateBtn {
		get { return ( (System.Web.UI.WebControls.Button) AdminGrid.FindControl( "FormControls:DuplicateBtn" ) ); }
	}

	public string EyeballGlyphUrl {
		get {
			if ( string.IsNullOrEmpty( this.eyeballGlyphUrl ) ) {
				this.eyeballGlyphUrl = this.Page.ClientScript.GetWebResourceUrl(
					typeof( StarrTech.WebTools.Controls.LabeledTextBox ), "StarrTech.WebTools.Resources.treeui.eyeball.gif"
				);
			}
			return ( this.eyeballGlyphUrl );
		}
	}
	private string eyeballGlyphUrl;

	public string EyeballGlyphTag {
		get {
			return ( string.Format( "<img src='{0}' />", this.EyeballGlyphUrl ) );
		}
	}
	
	public string IsApprovedGlyph( bool IsApproved ) {
		return ( IsApproved ? this.EyeballGlyphTag : "" );
	}

	#endregion

	#region Event Handlers

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	public void ClearSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledDropDownList SIslandLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SIslandLBox" );
		LabeledDropDownList SCategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SCategoryLBox" );
		LabeledDropDownList SSubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SSubcategoryLBox" );
		LabeledTextBox STitleBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:STitleBox" );
		LabeledTextBox SVendorBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SVendorBox" );
		LabeledDropDownList SApprovedLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SApprovedLBox" );

		// Reset the controls
		SWebsiteLBox.SelectedIndex = 0;
		SIslandLBox.SelectedIndex = 0;
		SCategoryLBox.SelectedIndex = 0;
		SSubcategoryLBox.SelectedIndex = 0;
		STitleBox.Text = "";
		SVendorBox.Text = "";
		SApprovedLBox.SelectedIndex = 0;
	}

	public void ReadSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		LabeledDropDownList SIslandLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SIslandLBox" );
		LabeledDropDownList SCategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SCategoryLBox" );
		LabeledDropDownList SSubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SSubcategoryLBox" );
		LabeledTextBox STitleBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:STitleBox" );
		LabeledTextBox SVendorBox = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SVendorBox" );
		LabeledDropDownList SApprovedLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SApprovedLBox" );

		// Clear out any previous search values
		ActivityGroup.NewSearch();

		// Assign values to the static search properties
		ActivityGroup.SWebsiteID = Convert.ToInt32( SWebsiteLBox.SelectedValue );
		ActivityGroup.SIslandID = Convert.ToInt32( SIslandLBox.SelectedValue );
		ActivityGroup.SCategoryID = Convert.ToInt32( SCategoryLBox.SelectedValue );
		ActivityGroup.SSubcategoryID = SSubcategoryLBox.SelectedIndex >= 0 ? Convert.ToInt32( SSubcategoryLBox.SelectedValue ) : -1;
		ActivityGroup.STitle = STitleBox.Text;
		ActivityGroup.SVendorName = SVendorBox.Text;
		ActivityGroup.SIsApproved = Convert.ToInt32( SApprovedLBox.SelectedValue );
	}

	public void InitFormControls( Object sender, EventArgs args ) {
		ActivityGroup group = this.AdminGrid.DBItem as ActivityGroup;
		if ( group != null ) {
			LabeledDropDownList CategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:CategoryLBox" );
			CategoryLBox.SelectedValue = group.CategoryID.ToString();
			this.SetupSubcategoryLBox();
			LabeledDropDownList SubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:SubcategoryLBox" );
			try {
				SubcategoryLBox.SelectedValue = group.SubcategoryID.ToString();
			} catch {
				SubcategoryLBox.SelectedIndex = 0;
			}
		}
		this.DuplicateBtn.Enabled = ( this.Group.ID > 0 ) && ( Websites.Current.Count > 1 );
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Get a reference to all the form-panel's controls
		LabeledTextBox TitleBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:TitleBox" );
		LabeledTextBox VendorNameBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:VendorNameBox" );
		LabeledDropDownList WebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:WebsiteLBox" );
		LabeledDropDownList IslandLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:IslandLBox" );
		LabeledDropDownList CategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:CategoryLBox" );
		LabeledDropDownList SubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:SubcategoryLBox" );
		CheckBox IsApprovedXBox = (CheckBox) AdminGrid.FindControl( "FormControls:IsApprovedXBox" );
		LabeledTextBox ReservationPriceBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:ReservationPriceBox" );
		LabeledTextBox CommentsBox = (LabeledTextBox) AdminGrid.FindControl( "FormControls:CommentsBox" );
		HtmlTextBox BriefDescriptionBox = (HtmlTextBox) AdminGrid.FindControl( "FormControls:BriefDescriptionBox" );
		HtmlTextBox TopDescriptionBox = (HtmlTextBox) AdminGrid.FindControl( "FormControls:TopDescriptionBox" );
		HtmlTextBox BottomDescriptionBox = (HtmlTextBox) AdminGrid.FindControl( "FormControls:BottomDescriptionBox" );

		// Get a reference to the business object to be edited...
		ActivityGroup group = new ActivityGroup();
		group.ID = this.AdminGrid.FormController.RecordID;
		group.Title = TitleBox.Text;
		group.VendorName = VendorNameBox.Text;
		group.WebsiteID = Convert.ToInt32( WebsiteLBox.SelectedValue );
		group.IslandID = Convert.ToInt32( IslandLBox.SelectedValue );
		group.CategoryID = Convert.ToInt32( CategoryLBox.SelectedValue );
		// RKP 20090327 - Replaced the try...catch with TryParse
		//try {
		//  group.SubcategoryID = Convert.ToInt32( SubcategoryLBox.SelectedValue );
		//} catch { }
		int subcat = -1;
		if ( Int32.TryParse( SubcategoryLBox.SelectedValue, out subcat ) ) {
			group.SubcategoryID = subcat;
		}
		group.IsApproved = IsApprovedXBox.Checked;
		group.ReservationPrice = Convert.ToDecimal( ReservationPriceBox.Text );
		group.Comments = CommentsBox.Text;
		group.BriefDescription = BriefDescriptionBox.Text;
		group.TopDescription = TopDescriptionBox.Text;
		group.BottomDescription = BottomDescriptionBox.Text;
		this.AdminGrid.DBItem = group;
	}

	public void SelectedSCategoryChanged( Object sender, EventArgs args ) {
		this.SetupSSubcategoryLBox();
		LabeledDropDownList SSubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SSubcategoryLBox" );
		SSubcategoryLBox.SelectedIndex = 0;
	}

	public void SelectedCategoryChanged( Object sender, EventArgs args ) {
		this.SetupSubcategoryLBox();
		LabeledDropDownList SubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:SubcategoryLBox" );
		if ( SubcategoryLBox.Items.Count == 0 ) {
			return;
		}
		SubcategoryLBox.SelectedIndex = 0;
	}

	protected void OptionFormOpening( object sender, System.EventArgs e ) {
		// The Option Form is opening ...
		// Make sure our parent record has been saved
		if ( this.AdminGrid.FormController.RecordID <= 0 ) {
			this.PersistActivityGroup();
		}
		// Update the static ActivityGroup fields
		this.ReadFormControls( null, null );
		this.OptionsGrid.ActivityGroupName = this.Group.Title;
		this.OptionsGrid.VendorName = this.Group.VendorName;
		this.OptionsGrid.WebsiteName = this.Group.WebsiteName;
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.ActivityGroupFormPanel.Visible = false;
		this.ActivitiesGrid.Visible = false;
		this.PhotosGrid.Visible = false;
	}
	protected void OptionFormClosing( object sender, System.EventArgs e ) {
		// The Option Form is closing - show the main ActivityGroup form
		this.AdminGrid.FormController.Visible = true;
		this.ActivityGroupFormPanel.Visible = true;
		this.PhotosGrid.Visible = true;
		this.ActivitiesGrid.Visible = true;
	}

	protected void PhotoFormOpening( object sender, System.EventArgs e ) {
		// The Photo Form is opening ...
		// Make sure our parent record has been saved
		if ( this.AdminGrid.FormController.RecordID <= 0 ) {
			this.PersistActivityGroup();
		}
		// Update the static ActivityGroup fields
		this.ReadFormControls( null, null );
		this.PhotosGrid.ActivityGroupName = this.Group.Title;
		this.PhotosGrid.VendorName = this.Group.VendorName;
		this.PhotosGrid.WebsiteName = this.Group.WebsiteName;
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.ActivityGroupFormPanel.Visible = false;
		this.ActivitiesGrid.Visible = false;
		this.OptionsGrid.Visible = false;
	}
	protected void PhotoFormClosing( object sender, System.EventArgs e ) {
		// The Photo Form is closing - show the main ActivityGroup form
		this.AdminGrid.FormController.Visible = true;
		this.ActivityGroupFormPanel.Visible = true;
		this.OptionsGrid.Visible = true;
		this.ActivitiesGrid.Visible = true;
	}

	protected void ActivityFormOpening( object sender, System.EventArgs e ) {
		// The Activity Form is opening ...
		// Make sure our parent record has been saved
		if ( this.AdminGrid.FormController.RecordID <= 0 ) {
			this.PersistActivityGroup();
		}
		// Update the static ActivityGroup fields
		this.ReadFormControls( null, null );
		this.ActivitiesGrid.ActivityGroupName = this.Group.Title;
		this.ActivitiesGrid.VendorName = this.Group.VendorName;
		this.ActivitiesGrid.WebsiteName = this.Group.WebsiteName;
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.ActivityGroupFormPanel.Visible = false;
		this.OptionsGrid.Visible = false;
		this.PhotosGrid.Visible = false;
	}
	protected void ActivityFormClosing( object sender, System.EventArgs e ) {
		// The Activity Form is closing - show the main ActivityGroup form
		this.AdminGrid.FormController.Visible = true;
		this.ActivityGroupFormPanel.Visible = true;
		this.OptionsGrid.Visible = true;
		this.PhotosGrid.Visible = true;
	}

	protected void DuplicateClicked( object sender, System.EventArgs e ) {
		// The Duplicate button was Clicked ...
		// We can assume that the ActivityGroup has been persisted at least once
		// Hide the parent-form panels
		this.AdminGrid.FormController.Visible = false;
		this.ActivityGroupFormPanel.Visible = false;
		this.ActivitiesGrid.Visible = false;
		this.OptionsGrid.Visible = false;
		this.PhotosGrid.Visible = false;
		// Show the SitePicker ... if it is confirmed, we'll handle the actual duplication then
		this.SitePicker.Visible = true;
	}
	
	protected void SitePickerSelect( object sender, System.EventArgs e ) {
		// The SitePicker was confirmed ...
		// Duplicate the current activity-group
		int siteID = this.SitePicker.SelectedSiteID;
		ActivityGroup thisGroup = new ActivityGroup( this.AdminGrid.FormController.RecordID );
		ActivityGroup newGroup = thisGroup.DeepCopy( siteID );
		// Show the main ActivityGroup form
		this.AdminGrid.FormController.Visible = true;
		this.ActivityGroupFormPanel.Visible = true;
		this.ActivitiesGrid.Visible = true;
		this.OptionsGrid.Visible = true;
		this.PhotosGrid.Visible = true;
		this.SitePicker.Visible = false;
	}

	protected void SitePickerCancel( object sender, System.EventArgs e ) {
		// The SitePicker was cancelled - no duplication
		// Show the main ActivityGroup form
		this.AdminGrid.FormController.Visible = true;
		this.ActivityGroupFormPanel.Visible = true;
		this.ActivitiesGrid.Visible = true;
		this.OptionsGrid.Visible = true;
		this.PhotosGrid.Visible = true;
		this.SitePicker.Visible = false;
	}

	#endregion

	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( ActivityGroup );

		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminActivitiesUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminActivitiesAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminActivitiesDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}

		// Start things rolling
		if ( !this.IsPostBack ) {
			// Set up the special search controls
			this.SetupSWebsiteLBox();
			this.SetupSIslandLBox();
			this.SetupSCategoryLBox();
			this.SetupSSubcategoryLBox();
			// Set up the special form controls
			this.SetupWebsiteLBox();
			this.SetupIslandLBox();
			this.SetupCategoryLBox();
			this.AdminGrid.GridController.SortExpression = "name";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}

	protected void PersistActivityGroup() {
		this.ReadFormControls( null, null );
		this.Group.Persist();
		this.AdminGrid.FormController.RecordID = this.Group.ID;
		this.PhotosGrid.ActivityGroupID = this.Group.ID;
		this.ActivitiesGrid.ActivityGroupID = this.Group.ID;
		this.OptionsGrid.ActivityGroupID = this.Group.ID;
	}

	#region Private Methods

	private void SetupSWebsiteLBox() {
		LabeledDropDownList SWebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SWebsiteLBox" );
		SWebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			SWebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		SWebsiteLBox.SelectedIndex = 0;
	}
	private void SetupSIslandLBox() {
		LabeledDropDownList SIslandLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SIslandLBox" );
		SIslandLBox.Items.Clear();
		SIslandLBox.Items.Add( new ListItem( "(Any)", "-1" ) );
		foreach ( Island island in Islands.Current ) {
			SIslandLBox.Items.Add( new ListItem( island.Name, island.ID.ToString() ) );
		}
		SIslandLBox.SelectedIndex = 0;
	}
	private void SetupSCategoryLBox() {
		LabeledDropDownList SCategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SCategoryLBox" );
		SCategoryLBox.Items.Clear();
		SCategoryLBox.Items.Add( new ListItem( "(Any)", "-1" ) );
		foreach ( Category cat in CategoriesList.Current ) {
			SCategoryLBox.Items.Add( new ListItem( cat.Label, cat.ID.ToString() ) );
		}
		SCategoryLBox.SelectedIndex = 0;
	}
	private void SetupSSubcategoryLBox() {
		LabeledDropDownList SCategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SCategoryLBox" );
		LabeledDropDownList SSubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "SearchControls:SSubcategoryLBox" );
		int CategoryID = Convert.ToInt32( SCategoryLBox.SelectedValue );
		SSubcategoryLBox.Items.Clear();
		SSubcategoryLBox.Items.Add( new ListItem( "(Any)", "-1" ) );
		if ( CategoryID < 0 ) {
			SSubcategoryLBox.Enabled = false;
			return;
		}
		SubcategoriesList subcategories = CategoriesList.Current[CategoryID].Subcategories;
		foreach ( Subcategory sub in subcategories ) {
			SSubcategoryLBox.Items.Add( new ListItem( sub.Label, sub.ID.ToString() ) );
		}
		SSubcategoryLBox.Enabled = ( SSubcategoryLBox.Items.Count > 1 );
	}

	private void SetupWebsiteLBox() {
		LabeledDropDownList WebsiteLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:WebsiteLBox" );
		WebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		foreach ( int key in sites.Keys ) {
			WebsiteLBox.Items.Add( new ListItem( sites[key].Name, key.ToString() ) );
		}
		WebsiteLBox.SelectedIndex = 0;
	}
	private void SetupIslandLBox() {
		LabeledDropDownList IslandLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:IslandLBox" );
		IslandLBox.Items.Clear();
		foreach ( Island island in Islands.Current ) {
			IslandLBox.Items.Add( new ListItem( island.Name, island.ID.ToString() ) );
		}
		IslandLBox.SelectedIndex = 0;
	}
	private void SetupCategoryLBox() {
		LabeledDropDownList CategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:CategoryLBox" );
		CategoryLBox.Items.Clear();
		//CategoryLBox.Items.Add( new ListItem( "(Any)", "-1" ) );
		foreach ( Category cat in CategoriesList.Current ) {
			CategoryLBox.Items.Add( new ListItem( cat.Label, cat.ID.ToString() ) );
		}
		CategoryLBox.SelectedIndex = 0;
	}
	private void SetupSubcategoryLBox() {
		LabeledDropDownList CategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:CategoryLBox" );
		int CategoryID = Convert.ToInt32( CategoryLBox.SelectedValue );
		LabeledDropDownList SubcategoryLBox = (LabeledDropDownList) AdminGrid.FindControl( "FormControls:SubcategoryLBox" );
		SubcategoryLBox.Items.Clear();
		SubcategoriesList subcategories = CategoriesList.Current[CategoryID].Subcategories;
		SubcategoryLBox.Enabled = false;
		foreach ( Subcategory sub in subcategories ) {
			SubcategoryLBox.Items.Add( new ListItem( sub.Label, sub.ID.ToString() ) );
			SubcategoryLBox.Enabled = true;
		}
	}

	#endregion

}
