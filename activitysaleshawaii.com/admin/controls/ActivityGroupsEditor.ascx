<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityGroupsEditor.ascx.cs" Inherits="ActivityGroupsEditor" %>
<%@ import namespace="ABS" %>
<%@ register tagPrefix="adm" tagName="photosgrid"			src="ActivityPhotosGridPanel.ascx" %>
<%@ register tagPrefix="adm" tagName="optionsgrid"		src="ActivityOptionsGridPanel.ascx" %>
<%@ register tagPrefix="adm" tagName="activitiesgrid" src="ActivitiesGridPanel.ascx" %>
<%@ register tagPrefix="adm" tagName="sitepicker"			src="WebsitePickerPanel.ascx" %>

<dbui:admindatagrid runat="server" id="AdminGrid" 
	OnSearchControlsRead="ReadSearchControls"
	OnSearchControlsClear="ClearSearchControls"
	OnFormControlsInit="InitFormControls"
	OnFormControlsRead="ReadFormControls"
>
	
	<searchcontrols>
		<br />
		<table border="0" cellpadding="0" cellspacing="4" width="100%" style="border-bottom:1px solid #0055aa">
			<tr>
				<td rowspan="3" align="left" valign="top">
					<table border="0" cellpadding="0" cellspacing="4" width="100%">
						<tr>
							<td width="25%">
								<stic:labeleddropdownlist runat="server" id="SWebsiteLBox" label="Website" />
							</td>
							<td width="25%">
								<stic:labeleddropdownlist runat="server" id="SIslandLBox" label="Island" />
							</td>
							<td width="25%">
								<stic:labeleddropdownlist runat="server" id="SCategoryLBox" label="Category" 
								 autopostback="true" onselectedindexchanged="SelectedSCategoryChanged"
								/>
							</td>
							<td width="25%">
								<stic:labeleddropdownlist runat="server" id="SSubcategoryLBox" label="Subcategory" />
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<stic:labeledtextbox runat="server" id="STitleBox" label="Title" />
							</td>
							<td>
								<stic:labeledtextbox runat="server" id="SVendorBox" label="Vendor" />
							</td>
							<td>
								<stic:labeleddropdownlist runat="server" id="SApprovedLBox" label="Approved" width="100%"
									tabindex='1'
								>
									<asp:listitem value="0">Any</asp:listitem>
									<asp:listitem value="-1">Not Approved</asp:listitem>
									<asp:listitem value="1">Approved</asp:listitem>
								</stic:labeleddropdownlist>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	
	</searchcontrols>
	
	<formcontrols>
		<br />
		<asp:panel runat="server" id="ActivityGroupFormPanel" >
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" align="right">
						<asp:button runat="server" id="DuplicateBtn" text="Duplicate" style="width:100px"
						 onclick="DuplicateClicked"
						/>
					</td>
				</tr>
				<tr>
					<td width="50%">
						<stic:labeledtextbox runat="server" id="TitleBox" label="Title"
						 text='<%# this.Group.Title %>'
						 frame-labelmark="*"
						/>
						<asp:requiredfieldvalidator runat="server" display="none" 
						 id="TitleBoxReqVal" controltovalidate="TitleBox" 
						 setfocusonerror="true" errormessage="A value is required for Title."
						/>
					</td>
					<td width="30%">
						<stic:labeledtextbox runat="server" id="VendorNameBox" label="Vendor"
						 text='<%# this.Group.VendorName %>'
						/>
					</td>
					<td>
						<stic:labeleddropdownlist runat="server" id="WebsiteLBox" label="Website" 
						 selectedvalue='<%# this.Group.WebsiteID %>'
						 enabled='<%# this.Group.ID <= 0 %>'
						/>
					</td>
				</tr>
			</table>
							
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td width="25%">
						<stic:labeleddropdownlist runat="server" id="IslandLBox" label="Island" 
						 selectedvalue='<%# this.Group.IslandID %>'
						/>
					</td>
					<td width="25%">
						<stic:labeleddropdownlist runat="server" id="CategoryLBox" label="Category" 
						 selectedvalue='<%# this.Group.CategoryID %>'
						 autopostback="true" onselectedindexchanged="SelectedCategoryChanged"
						/>
					</td>
					<td width="25%">
						<stic:labeleddropdownlist runat="server" id="SubcategoryLBox" label="Subcategory" 
						 selectedvalue='<%# this.Group.SubcategoryID %>'
						/>
					</td>
					<td width="15%">
						<stic:labeledtextbox runat="server" id="ReservationPriceBox" label="Reservation Price"
						 text='<%# this.Group.ReservationPrice.ToString( "#,##0.00" ) %>'
						/>
						<asp:comparevalidator runat="server" id="ReservationPriceCompVal" controltovalidate="ReservationPriceBox"
						 operator="GreaterThanEqual" valuetocompare="0" setfocusonerror="true" type="Currency" display="None"
						 enableclientscript="true" errormessage="Please enter a numeric value for Reservation Price." cssclass="ErrorMessages" 
						/>
					</td>
					<td style="padding-top:8px" align="right" class="xbox" >
						<asp:checkbox id="IsApprovedXBox" runat="server" text="Approved?" 
							checked='<%# this.Group.IsApproved %>'
							enabled='<%# SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminActivitiesApprove ) %>'
							tabindex='1'
						/>
					</td>
				</tr>
				
				<tr>
					<td colspan="5">
						<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
						 text='<%# this.Group.Comments %>'
						 textmode="MultiLine" rows="4" wrap="true"
						/>
					</td>
				</tr>
				
				<tr>
					<td colspan="5" style="padding-top:12px">
						<cms:htmltextbox runat="server" id="BriefDescriptionBox" label="Brief Description"
						 text='<%# this.Group.BriefDescription %>'
						 rows="5" 
						 width="100%"
						 configmode="Catalog"
						/>
					</td>
				</tr>
				
				<tr>
					<td colspan="5" style="padding-top:12px">
						<cms:htmltextbox runat="server" id="TopDescriptionBox" label="Top Description"
						 text='<%# this.Group.TopDescription %>'
						 rows="10" 
						 width="100%"
						 configmode="Catalog"
						/>
					</td>
				</tr>
				
				<tr>
					<td colspan="5" style="padding-top:12px">
						<cms:htmltextbox runat="server" id="BottomDescriptionBox" label="Bottom Description"
						 text='<%# this.Group.BottomDescription %>'
						 width="100%"
						 rows="10" 
						 configmode="Catalog"
						/>
					</td>
				</tr>
				
			</table>
		</asp:panel>
		
		<adm:activitiesgrid runat="server" id="ActivitiesGrid" 
		 activitygroupid='<%# this.Group.ID %>' 
		 activitygroupname='<%# this.Group.Title %>' 
		 vendorname='<%# this.Group.VendorName %>' 
		 websitename='<%# this.Group.WebsiteName %>' 
		 onformclosing="ActivityFormClosing"
		 onformopening="ActivityFormOpening"
		/>
		
		<adm:photosgrid runat="server" id="PhotosGrid" 
		 activitygroupid='<%# this.Group.ID %>' 
		 activitygroupname='<%# this.Group.Title %>' 
		 vendorname='<%# this.Group.VendorName %>' 
		 websitename='<%# this.Group.WebsiteName %>' 
		 onformclosing="PhotoFormClosing"
		 onformopening="PhotoFormOpening"
		/>
		
		<adm:optionsgrid runat="server" id="OptionsGrid" 
		 activitygroupid='<%# this.Group.ID %>' 
		 activitygroupname='<%# this.Group.Title %>' 
		 vendorname='<%# this.Group.VendorName %>' 
		 websitename='<%# this.Group.WebsiteName %>' 
		 onformclosing="OptionFormClosing"
		 onformopening="OptionFormOpening"
		/>
		
		<adm:sitepicker runat="server" id="SitePicker"
		 activitygroupid='<%# this.Group.ID %>' 
		 onselect="SitePickerSelect"
		 oncancel="SitePickerCancel"
		 visible="false"
		/>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Website"						datafield="WebsiteName"			sortexpression="website_id" /> 
		<asp:boundcolumn headertext="Title"							datafield="Title"						sortexpression="name" /> 
		<asp:boundcolumn headertext="Vendor"						datafield="VendorName"			sortexpression="vendor_name" /> 
		<asp:boundcolumn headertext="Island"						datafield="IslandName"			sortexpression="island_id" /> 
		<asp:boundcolumn headertext="Category"					datafield="CatLabel"				sortexpression="subcategory_id" /> 
		<asp:templatecolumn itemstyle-horizontalalign="center" headerstyle-horizontalalign="center"	sortexpression="n.is_approved" >
			<headertemplate>
				<asp:image runat="server" src='<%# this.EyeballGlyphUrl %>' />
			</headertemplate>
			<itemtemplate>
				<asp:literal runat="server" text='<%# this.IsApprovedGlyph( (bool) DataBinder.Eval(Container.DataItem, "IsApproved") ) %>' />
			</itemtemplate>
		</asp:templatecolumn>
	</gridcolumns>
	
</dbui:admindatagrid>
