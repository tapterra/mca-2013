<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityOptionFormPanel.ascx.cs" Inherits="ActivityOptionFormPanel" %>
<%@ import namespace="ABS" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td width="48%" style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Activity Option Form" cssclass="h2" />
		</td>
		<td colspan="3" align="right" style="padding-bottom:20px">
			<stic:button Runat="server" ID="SaveButton" Text="Save" tabindex="9" onclick="OnSaveItem" />
			<stic:button Runat="server" ID="CancelButton" Text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancelItem" />
			<stic:button Runat="server" ID="DeleteButton" Text="Delete" tabindex="9" causesvalidation="false" onclick="OnDeleteItem" 
				onclientclick="return confirm( 'Are you sure you want to delete this Activity Option?' );"
			/>
		</td>
	</tr>

	<tr>
		<td width="48%">
			<stic:labeledtextbox runat="server" id="ActivityGroupBox" label="Activity Group"
			 enabled="false"
			/>
		</td>
		<td width="30%">
			<stic:labeledtextbox runat="server" id="VendorNameBox" label="Vendor"
			 enabled="false"
			/>
		</td>
		<td width="22%">
			<stic:labeledtextbox runat="server" id="WebsiteBox" label="Website"
			 enabled="false"
			/>
		</td>
	</tr>
	
	<tr>
		<td width="63%" colspan="2">
			<stic:labeledtextbox runat="server" id="QuestionTextBox" label="Question Text"
			 frame-labelmark="*"
			/>
			<asp:requiredfieldvalidator runat="server" display="none" 
			 id="QuestionTextReqVal" controltovalidate="QuestionTextBox" 
			 setfocusonerror="true" errormessage="A value is required for Question Text."
			/>
		</td>
		<td>
			<stic:labeleddropdownlist runat="server" id="OptionTypeLBox" label="Option Type" 
			 autopostback="true" onselectedindexchanged="OptionTypeChanged"
			>
				<asp:listitem value="PlainText"			text="Text Box" />
				<asp:listitem value="DropDownList"	text="Drop-Down List" />
				<asp:listitem value="Checkbox"			text="Checkbox" />
			</stic:labeleddropdownlist>
		</td>
	</tr>
	
	<tr>
		<td runat="server" id="ResponseChoicesRow" colspan="4">
			<stic:labeledtextbox runat="server" id="ResponseChoicesBox" label="Response Choices (separated by commas)"
			 frame-labelmark="*"
			/>
			<asp:requiredfieldvalidator runat="server" display="none" 
			 id="ResponseChoicesReqVal" controltovalidate="ResponseChoicesBox" 
			 setfocusonerror="true" errormessage="A value is required for Response Choices."
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
			 textmode="MultiLine" rows="4" wrap="true"
			/>
		</td>
	</tr>
	
</table>
