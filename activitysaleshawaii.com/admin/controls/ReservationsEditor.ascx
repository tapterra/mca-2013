<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReservationsEditor.ascx.cs" Inherits="ReservationsEditor" %>

<%@ import namespace="ABS" %>

<dbui:admindatagrid runat="server" id="AdminGrid" 
	OnSearchControlsRead="ReadSearchControls"
	OnSearchControlsClear="ClearSearchControls"
	OnFormControlsInit="InitFormControls"
	OnFormControlsRead="ReadFormControls"
>

	<searchcontrols>
		<table border="0" cellpadding="2" cellspacing="0" width="50%">
			<tr>
				<td>
					<stic:labeleddropdownlist runat="server" id="SWebsiteLBox" label="Website" 
						tabindex='1'
					/>
				</td>
				<td>
					<stic:labeleddropdownlist runat="server" id="SProcessedLBox" label="Status" width="100%"
						tabindex='1'
					>
						<asp:listitem value="0">Any</asp:listitem>
						<asp:listitem value="-1">Not Processed</asp:listitem>
						<asp:listitem value="1">Processed</asp:listitem>
					</stic:labeleddropdownlist>
				</td>
			</tr>
		</table>
	</searchcontrols>	

	<formcontrols>
	
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td width="25%">
						<stic:labeleddropdownlist runat="server" id="WebsiteLBox" label="Website" 
						 selectedvalue='<%# this.Reservation.WebsiteID %>'
						 enabled='false'
						/>
					</td>
					<td colspan="2" width="50%">
						<stic:labeledtextbox runat="server" id="ActivityNameBox" label="Activity"
						 text='<%# this.Reservation.ActivityTitle %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="CreatedDTBox" label="Created On"
						 text='<%# this.Reservation.CreatedDTStr %>'
						 enabled="false"
						/>
					</td>
				</tr>
				<tr>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="ResDateBox" label="Reservation Date"
						 text='<%# this.Reservation.ResDateStr %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="PaxCountBox" label="# Guests"
						 text='<%# this.Reservation.PaxCount %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="PrefTimeBox" label="Pref. Time"
						 text='<%# this.Reservation.PrefTime %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="AltTimeBox" label="Alt. Time"
						 text='<%# this.Reservation.AltTime %>'
						 enabled="false"
						/>
					</td>
				</tr>
				<tr>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="CustomerNameBox" label="Customer Name"
						 text='<%# this.Reservation.CustomerName %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="ResNameBox" label="Reservations For"
						 text='<%# this.Reservation.FullNameLF %>'
						 enabled="false"
						/>
					</td>
					<td width="50%" style="padding-top:8px" align="right" class="xbox" colspan="2" >
						<asp:checkbox id="IsProcessedXBox" runat="server" text="Processed?" 
							checked='<%# this.Reservation.IsProcessed %>'
							tabindex='1'
						/>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
						 text='<%# this.Reservation.Comments %>'
						 textmode="MultiLine" rows="6" wrap="true" tabindex='1'
						/>
					</td>
				</tr>
			</table>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Website"							datafield="WebsiteName"			sortexpression="w.website_id" /> 
		<asp:boundcolumn headertext="Posted Date"					datafield="CreatedDTStr"		sortexpression="r.created_dt" /> 
		<asp:boundcolumn headertext="Activity"						datafield="ActivityTitle"		sortexpression="r.activity_id" /> 
		<asp:boundcolumn headertext="Reservation Name"		datafield="FullNameLF"			sortexpression="r.last_name, r.first_name" /> 
		<asp:boundcolumn headertext="Res Date"						datafield="ResDateStr"			sortexpression="r.res_date" /> 
		<asp:boundcolumn headertext="&bull;"							datafield="ProcessedMark"		sortexpression="r.is_processed" /> 
	</gridcolumns>
	
</dbui:admindatagrid>
