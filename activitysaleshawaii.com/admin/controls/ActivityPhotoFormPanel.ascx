<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityPhotoFormPanel.ascx.cs" Inherits="ActivityPhotoFormPanel" %>
<%@ import namespace="ABS" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td width="48%" style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Activity Photo Form" cssclass="h2" />
		</td>
		<td colspan="3" align="right" style="padding-bottom:20px">
			<stic:button Runat="server" ID="SaveButton" Text="Save" tabindex="9" onclick="OnSaveItem" />
			<stic:button Runat="server" ID="CancelButton" Text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancelItem" />
			<stic:button Runat="server" ID="DeleteButton" Text="Delete" tabindex="9" causesvalidation="false" onclick="OnDeleteItem" 
				onclientclick="return confirm( 'Are you sure you want to delete this Photo?' );"
			/>
		</td>
	</tr>

	<tr>
		<td width="48%">
			<stic:labeledtextbox runat="server" id="ActivityGroupBox" label="Activity Group"
			 enabled="false"
			/>
		</td>
		<td width="30%">
			<stic:labeledtextbox runat="server" id="VendorNameBox" label="Vendor"
			 enabled="false"
			/>
		</td>
		<td width="22%" colspan="2">
			<stic:labeledtextbox runat="server" id="WebsiteBox" label="Website"
			 enabled="false"
			/>
		</td>
	</tr>
	
	<tr>
		<td width="48%">
			<stic:labeledtextbox runat="server" id="FileNameBox" label="File Name" width="100%" 
				enabled="false"
			/>
		</td>
		<td width="30%">
			<stic:labeledtextbox runat="server" id="MimeTypeBox" label="Content Type"  width="100%"
				enabled="false"
			/>
		</td>
		<td width="10%">
			<stic:labeledtextbox runat="server" id="SizeBox" label="Size"  width="100%"
				enabled="false"
			/>
		</td>
		<td width="12%" align="right">
			<stic:button runat="server" id="Download" text="Download" onclick="DownloadPhoto" width="80" font-bold="false" 
				enabled='<%# this.ActivityPhotoID > 0 %>'
				style="margin-top:6px"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<stic:labeledfilebox runat="server" id="FileUploadBox" label="Upload a New Image File" width="100%" />
			<asp:requiredfieldvalidator runat="server" display="none" 
			 id="FileUploadReqVal" controltovalidate="FileUploadBox" 
			 setfocusonerror="true" errormessage="Please enter/select an image file to upload."
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
			 textmode="MultiLine" rows="4" wrap="true"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<asp:hyperlink runat="server" id="PreviewImage" target="_blank" />
		</td>
	</tr>
	
</table>
