<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomersEditor.ascx.cs" Inherits="CustomersEditor" %>
<%@ register tagPrefix="adm" tagName="purchasesgrid" src="PurchasesGridPanel.ascx" %>
<%@ import namespace="ABS" %>

<dbui:admindatagrid runat="server" id="AdminGrid" 
	OnSearchControlsRead="ReadSearchControls"
	OnSearchControlsClear="ClearSearchControls"
	OnFormControlsInit="InitFormControls"
	OnFormControlsRead="ReadFormControls"
>

	<searchcontrols>
		<table border="0" cellpadding="2" cellspacing="0" width="100%">
			<tr>
				<td>
					<stic:labeleddropdownlist runat="server" id="SWebsiteLBox" label="Website" />
				</td>
				<td>
					<stic:labeledtextbox runat="server" id="SNameBox" label="Last Name" />
				</td>
				<td>
					<stic:labeledtextbox runat="server" id="SEmailBox" label="Email Address" />
				</td>
				<td>
					<stic:labeledtextbox runat="server" id="SAtcoIDBox" label="ATCO ID" />
				</td>
				<td>
					<stic:labeledtextbox runat="server" id="SZipCodeBox" label="Zip Code" />
				</td>
			</tr>
		</table>
	</searchcontrols>	

	<formcontrols>
	
		<asp:panel runat="server" id="CustomerFormPanel" >
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="FirstNameBox" label="First Name"
						 text='<%# this.Customer.FirstName %>'
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="LastNameBox" label="Last Name"
						 text='<%# this.Customer.LastName %>'
						/>
					</td>
					<td width="25%">
						<stic:labeledtextbox runat="server" id="CreatedDTBox" label="Created On"
						 text='<%# this.Customer.CreatedDateStr %>'
						 enabled="false"
						/>
					</td>
					<td width="25%">
						<stic:labeleddropdownlist runat="server" id="WebsiteLBox" label="Website" 
						 selectedvalue='<%# this.Customer.WebsiteID %>'
						 enabled='false'
						/>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<tr>
					<td width="35%" colspan="1">
						<stic:labeledtextbox runat="server" id="BillingNameBox" label="Billing Name"
						 text='<%# this.Customer.BillingName %>'
						/>
					</td>
					<td width="35%" colspan="2">
						<stic:labeledtextbox runat="server" id="EmailAddressBox" label="Email Address"
						 text='<%# this.Customer.EmailAddress %>'
						/>
					</td>
					<td width="30%" colspan="1">
						<stic:labeledtextbox runat="server" id="PasswordBox" label="Password"
						 text='<%# this.Customer.Password %>'
						/>
					</td>
				</tr>
				<tr>
					<td width="70%" colspan="3">
						<stic:labeledtextbox runat="server" id="BillingStreet1Box" label="Billing Address"
						 text='<%# this.Customer.BillingStreet1 %>'
						/>
					</td>
					<td width="30%" colspan="1">
						<stic:labeledtextbox runat="server" id="TelephoneBox" label="Telephone"
						 text='<%# this.Customer.Telephone %>'
						/>
					</td>
				</tr>
				<tr>
					<td width="70%" colspan="3">
						<stic:labeledtextbox runat="server" id="BillingStreet2Box" label=""
						 text='<%# this.Customer.BillingStreet2 %>'
						/>
					</td>
					<td width="30%" colspan="1">
					</td>
				</tr>
				<tr>
					<td width="40%" colspan="1">
						<stic:labeledtextbox runat="server" id="BillingCityBox" label="City"
						 text='<%# this.Customer.BillingCity %>'
						/>
					</td>
					<td width="15%" colspan="1">
						<stic:labeledtextbox runat="server" id="BillingStateBox" label="State"
						 text='<%# this.Customer.BillingState %>'
						/>
					</td>
					<td width="15%" colspan="1">
						<stic:labeledtextbox runat="server" id="BillingZipBox" label="Zip Code"
						 text='<%# this.Customer.BillingZip %>'
						/>
					</td>
					<td width="30%" colspan="1">
						<stic:labeledtextbox runat="server" id="CustomerAIDBox" label="ATCO Customer ID"
						 text='<%# this.Customer.CustomerAID %>'
						/>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
						 text='<%# this.Customer.Comments %>'
						 textmode="MultiLine" rows="6" wrap="true"
						/>
					</td>
				</tr>
			</table>
			
		</asp:panel>

		<adm:purchasesgrid runat="server" id="PurchasesGrid" 
		 customerid='<%# this.Customer.ID %>' 
		 onformclosing="PurchaseFormClosing"
		 onformopening="PurchaseFormOpening"
		/>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Website"					datafield="WebsiteName"		sortexpression="website_id" /> 
		<asp:boundcolumn headertext="Customer Name"		datafield="FullNameLF"			sortexpression="last_name, first_name" /> 
		<asp:boundcolumn headertext="Email"						datafield="EmailAddress"	sortexpression="email_address" /> 
		<asp:boundcolumn headertext="Telephone"				datafield="Telephone"			sortexpression="telephone" /> 
		<asp:boundcolumn headertext="Created"					datafield="CreatedDateStr"	sortexpression="created_dt" /> 
	</gridcolumns>
	
</dbui:admindatagrid>
