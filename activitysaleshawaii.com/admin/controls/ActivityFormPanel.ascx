<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivityFormPanel.ascx.cs" Inherits="ActivityFormPanel" %>
<%@ import namespace="ABS" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
	
	<tr>
		<td width="48%" style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Activity Form" cssclass="h2" />
		</td>
		<td colspan="3" align="right" style="padding-bottom:20px">
			<stic:button Runat="server" ID="SaveButton" Text="Save" tabindex="9" onclick="OnSaveItem" />
			<stic:button Runat="server" ID="CancelButton" Text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancelItem" />
			<stic:button Runat="server" ID="DeleteButton" Text="Delete" tabindex="9" causesvalidation="false" onclick="OnDeleteItem" 
				onclientclick="return confirm( 'Are you sure you want to delete this Activity?' );"
			/>
		</td>
	</tr>

	<tr>
		<td width="48%">
			<stic:labeledtextbox runat="server" id="ActivityGroupBox" label="Activity Group"
			 enabled="false"
			/>
		</td>
		<td width="30%" colspan="2">
			<stic:labeledtextbox runat="server" id="VendorNameBox" label="Vendor"
			 enabled="false"
			/>
		</td>
		<td width="22%" colspan="2">
			<stic:labeledtextbox runat="server" id="WebsiteBox" label="Website"
			 enabled="false"
			/>
		</td>
	</tr>
	
	<tr>
		<td width="63%" colspan="2">
			<stic:labeledtextbox runat="server" id="ActivityTitleBox" label="Activity Title"
			 frame-labelmark="*"
			/>
			<asp:requiredfieldvalidator runat="server" display="none" 
			 id="ActivityTitleReqVal" controltovalidate="ActivityTitleBox" 
			 setfocusonerror="true" errormessage="A value is required for Activity Title."
			/>
		</td>
		<td colspan="3">
			<stic:labeledtextbox runat="server" id="ActivityAIDBox" label="ATCO Activity ID"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4" style="padding-top:12px">
			<cms:htmltextbox runat="server" id="DescriptionBox" label="Description"
			 rows="5" width="100%" tabindex="1"
			 configmode="Catalog"
			/>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">
			<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
			 textmode="MultiLine" rows="4" wrap="true"
			/>
		</td>
	</tr>
	
</table>
