<%@ Control Language="C#" ClassName="ItinPanel" %>
<%@ import namespace="ABS" %>
<%@ import namespace="System.Collections.Generic" %>

<script runat="server">

	public int ItineraryID {
		get {
			object obj = this.ViewState["ItineraryID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set {
			this.ViewState["ItineraryID"] = value;
			this.itinerary = null;
			this.Rebind();
		}
	}

	public Itinerary Itinerary {
		get {
			if ( this.itinerary == null ) {
				this.itinerary = new Itinerary( this.ItineraryID );
			}
			return ( this.itinerary );
		}
		set {
			this.itinerary = value;
			this.ItineraryID = value.ID;
			this.Rebind();
		}
	}
	private Itinerary itinerary;

</script>

<script runat="server">

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
			this.Rebind();
		}
	}

	public void Rebind() {
		this.BookingRepeater.DataSource = this.Itinerary.Bookings;
		this.BookingRepeater.Visible = this.Itinerary.Bookings.Count > 0;
		
		this.ResRepeater.DataSource = this.Itinerary.Reservations;
		this.ResRepeater.Visible = this.Itinerary.Reservations.Count > 0;

		this.PaymentsRepeater.DataSource = this.Itinerary.Payments;
		this.PaymentsRepeater.Visible = this.Itinerary.Payments.Count > 0;
		
		this.DataBind();
	}

</script>


<asp:repeater runat="server" id="BookingRepeater">
	<headertemplate>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="ItinBookPanel">
	</headertemplate>

	<itemtemplate>
		<tr>
			<td>
				<table width="100%" cellpadding="2" cellspacing="0" border="0" class="ItinBookHeaderTable">
					<tr>
						<td width="90" >
							<asp:label runat="server" id="ActivityLabelLbl" text="Activity" cssclass="ItinPanelLabel" />
						</td>
						<td>
							<asp:label runat="server" id="ActivityNameLbl" cssclass="ItinPanelLabel" 
							 text='<%# DataBinder.Eval(Container.DataItem, "ActivityTitle") %>'
							/>
						</td>
						<td align="right" rowspan="3" valign="baseline">
							<asp:label runat="server" id="Label2" cssclass="ItinPanelLabel" 
							 text='<%# DataBinder.Eval( Container.DataItem, "TotalPriceStr" ) %>'
							/>
						</td>
					</tr>
					<tr>
						<td>
							<asp:label runat="server" id="DateTimeLabelLbl" text="Date/Time" cssclass="ItinPanelLabel" />
						</td>
						<td>
							<asp:label runat="server" id="DateTimeLbl" cssclass="ItinPanelValue" 
							 text='<%# DataBinder.Eval(Container.DataItem, "SelectedDateTimeString") %>'
							/>
						</td>
					</tr>
					<tr>
						<td>
							<asp:label runat="server" id="HotelLabelLbl" text="Hotel" cssclass="ItinPanelLabel" />
						</td>
						<td>
							<asp:label runat="server" id="HotelLbl" cssclass="ItinPanelValue" 
							 text='<%# DataBinder.Eval(Container.DataItem, "HotelName") %>'
							/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="background-color: white">
				<asp:repeater runat="server" id="GuestsRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "Guests") %>' >
					
					<headertemplate>
						<table width="100%" cellpadding="2" cellspacing="0" border="0" class="ItinGuestPanel">
					</headertemplate>
					<itemtemplate>
							<tr style="background-color:#eeeeee">
								<td width="90">
									<asp:label runat="server" id="GuestIndexLbl" cssclass="ItinPanelLabel" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestNumberDisplay") %>'
									/>
								</td>
								<td>
									<asp:label runat="server" id="GuestNameLbl" cssclass="ItinPanelLabel" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestNameDisplay") %>'
 									/>
								</td>
								<td>
									<asp:label runat="server" id="GuestTypeLbl" cssclass="ItinPanelValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestTypeDisplay") %>'
									/>
								</td>
								<td align="right" >
									<asp:label runat="server" id="PriceLbl" cssclass="ItinPanelValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "PriceDisplay") %>'
									/>
								</td>
							</tr>
							
							<tr id="Tr1" runat="server"  style="background-color:#eeeeee"
							 visible='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplayVisible") %>'
							>
								<td> &nbsp; </td>
								<td colspan="3" >
									<asp:label runat="server" id="OptionsLbl" cssclass="ItinPanelGuestTypeValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplay") %>'
									/>
								</td>
							</tr>
					</itemtemplate>
					<alternatingitemtemplate>
							<tr style="background-color:#ffffff">
								<td width="90">
									<asp:label runat="server" id="GuestIndexLbl" cssclass="ItinPanelLabel" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestNumberDisplay") %>'
									/>
								</td>
								<td>
									<asp:label runat="server" id="GuestNameLbl" cssclass="ItinPanelLabel" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestNameDisplay") %>'
 									/>
								</td>
								<td>
									<asp:label runat="server" id="GuestTypeLbl" cssclass="ItinPanelValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "GuestTypeDisplay") %>'
									/>
								</td>
								<td align="right" >
									<asp:label runat="server" id="PriceLbl" cssclass="ItinPanelValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "PriceDisplay") %>'
									/>
								</td>
							</tr>
							
							<tr id="Tr1" runat="server"  style="background-color:#ffffff"
							 visible='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplayVisible") %>'
							>
								<td> &nbsp; </td>
								<td colspan="3" >
									<asp:label runat="server" id="OptionsLbl" cssclass="ItinPanelGuestTypeValue" 
										text='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplay") %>'
									/>
								</td>
							</tr>
					</alternatingitemtemplate>
					
					<footertemplate>
						</table>
					</footertemplate>
				</asp:repeater>
			</td>
		</tr>
	</itemtemplate>

	<footertemplate>
		</table>
	</footertemplate>
	
</asp:repeater>

<asp:repeater runat="server" id="ResRepeater">

	<itemtemplate>
		<table width="100%" cellpadding="2" cellspacing="0" border="0" class="ItinBookHeaderTable" style="margin-top:2px">
			<tr>
				<td width="90">
					<asp:label runat="server" id="ActivityLabelLbl" text="Reservation" cssclass="ItinPanelLabel" />
				</td>
				<td colspan="6">
					<asp:label runat="server" id="ActivityNameLbl" cssclass="ItinPanelLabel" 
					 text='<%# DataBinder.Eval(Container.DataItem, "ActivityTitle") %>'
					/>
				</td>
				<td align="right">
					<asp:label runat="server" id="Label3" cssclass="ItinPanelLabel" 
					 text='<%# DataBinder.Eval(Container.DataItem, "TotalPriceStr") %>'
					/>
				</td>
			</tr>
			<tr style="background-color:white">
				<td width="90">
					<asp:label runat="server" id="DateTimeLabelLbl" text="Date" cssclass="ItinPanelLabel" />
				</td>
				<td>
					<asp:label runat="server" id="DateTimeLbl" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval(Container.DataItem, "ResDateString") %>'
					/>
				</td>
				<td width="90">
					<asp:label runat="server" id="GuestsLabelLbl" text="Guests" cssclass="ItinPanelLabel" />
				</td>
				<td>
					<asp:label runat="server" id="PaxCountLbl" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval(Container.DataItem, "PaxCount") %>'
					/>
				</td>
				<td width="90">
					<asp:label runat="server" id="PrefTimeLabelLbl" text="Pref. Time" cssclass="ItinPanelLabel" />
				</td>
				<td>
					<asp:label runat="server" id="PrefTimeLbl" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval(Container.DataItem, "PrefTime") %>'
					/>
				</td>
				<td width="90">
					<asp:label runat="server" id="AltTimeLabelLbl" text="Alt. Time" cssclass="ItinPanelLabel" />
				</td>
				<td>
					<asp:label runat="server" id="AltTimeLbl" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval(Container.DataItem, "AltTime") %>'
					/>
				</td>
			</tr>
		</table>
	</itemtemplate>

</asp:repeater>

<table width="100%" cellpadding="3" cellspacing="0" border="0" class="ItinFooter" style="margin-top:2px">
	<tr class="ItinFooter">
		<td>
			<asp:label runat="server" id="TotalLabelLbl" text="Itinerary&nbsp;Total" cssclass="ItinPanelLabel" />
		</td>
		<td align="right">
			<asp:label runat="server" id="TotalLbl" cssclass="ItinPanelLabel"
			 text='<%# this.Itinerary.TotalPriceStr %>'
			/>
		</td>
	</tr>
</table>

<asp:repeater runat="server" id="PaymentsRepeater">

	<headertemplate>
		<table width="100%" cellpadding="2" cellspacing="0" border="0" class="ItinBookHeaderTable" style="margin-top:2px">
			<tr>
				<td class="ItinPanelLabel" colspan="5">Payments</td>
			</tr>
			<tr>
				<td class="ItinPanelLabel">Date</td>
				<td class="ItinPanelLabel">Method</td>
				<td class="ItinPanelLabel">Card Ref</td>
				<td class="ItinPanelLabel">Auth Code</td>
				<td class="ItinPanelLabel" align="right">Amount</td>
			</tr>
	</headertemplate>
	
	<itemtemplate>
			<tr style="background-color:#eeeeee">
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label6" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "PostedDT" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label5" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "MethodType" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label4" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "CCRef" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label1" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "AuthCode" ) %>'
					/>
				</td>
				<td class="ItinPanelValue" align="right">
					<asp:label runat="server" id="Label2" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "AmountStr" ) %>'
					/>
				</td>
			</tr>
	</itemtemplate>
	<alternatingitemtemplate>
			<tr style="background-color:#ffffff">
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label6" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "PostedDT" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label5" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "MethodType" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label4" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "CCRef" ) %>'
					/>
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label1" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "AuthCode" ) %>'
					/>
				</td>
				<td class="ItinPanelValue" align="right">
					<asp:label runat="server" id="Label2" cssclass="ItinPanelValue" 
					 text='<%# DataBinder.Eval( Container.DataItem, "AmountStr" ) %>'
					/>
				</td>
			</tr>
	</alternatingitemtemplate>	
	
	<footertemplate>
		</table>
	</footertemplate>

</asp:repeater>
