<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActivitiesGridPanel.ascx.cs" Inherits="ActivitiesGridPanel" %>
<%@ import namespace="ABS" %>
<%@ register tagPrefix="adm" tagName="activityform" src="ActivityFormPanel.ascx" %>

<table runat="server" id="GridPanel" cellpadding="0" cellspacing="0" border="0" style="width:700; margin-top:20px">
	<tr>
		<td width="50%" style="padding-bottom:0px">
			<asp:Label id="HeaderLabel" runat="server" text="Activities" cssclass="h3" />
		</td>
		<td align="right" style="padding-bottom:0px">
			<stic:button runat="server" id="AddButton" text="Add New Activity" tabindex="9" 
				causesvalidation="true" style="width:140px;" 
				onclick="AddItem"
			/>
		</td>
	</tr>
	<tr>
		<td colspan="2" >
			<stic:datagrid runat="server" id="Grid"
			 cellpadding="3" cellspacing="1" 
			 autogeneratecolumns="false" datakeyfield="id"
			 allowcustompaging="false" allowpaging="false" allowsorting="false" 
			 onrowclicked="EditItem"
			 includereordercontrols="true"
			 onreorderup="DoReorderUp"
			 onreorderdown="DoReorderDown"
			 width="700"
			>
				<columns>
					<asp:boundcolumn headertext="ID"									datafield="ID" visible="false" /> 
					<asp:boundcolumn headertext="Activity Title"			datafield="Title"	/> 
					<asp:boundcolumn headertext="ATCO Activity ID"		datafield="AID"
					 itemstyle-horizontalalign="center" headerstyle-horizontalalign="center" 
					 itemstyle-width="130"
					/>
				</columns>
			</stic:datagrid>
		</td>
	</tr>
</table>

<adm:activityform runat="server" id="FormPanel" visible="false" 
 oncancelitem="CancelItem"
 ondeleteitem="DeleteItem"
 onsaveitem="SaveItem"
/>
