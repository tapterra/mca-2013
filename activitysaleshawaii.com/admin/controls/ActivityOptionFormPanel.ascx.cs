using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ActivityOptionFormPanel : System.Web.UI.UserControl {

	#region Properties

	public int OptionID {
		get {
			object obj = ViewState["OptionID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["OptionID"] = value; }
	}

	public int ActivityGroupID {
		get {
			object obj = ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["ActivityGroupID"] = value; }
	}

	public int Seq {
		get {
			object obj = ViewState["Seq"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["Seq"] = value; }
	}

	public string ActivityGroupName {
		get { return ( this.ActivityGroupBox.Text ); }
		set { this.ActivityGroupBox.Text = value; }
	}

	public string VendorName {
		get { return ( this.VendorNameBox.Text ); }
		set { this.VendorNameBox.Text = value; }
	}

	public string WebsiteName {
		get { return ( this.WebsiteBox.Text ); }
		set { this.WebsiteBox.Text = value; }
	}

	public string QuestionText {
		get { return ( this.QuestionTextBox.Text ); }
		set { this.QuestionTextBox.Text = value; }
	}

	public OptionResponseTypes ResponseType {
		get { return ( (OptionResponseTypes) Enum.Parse( typeof( OptionResponseTypes ), this.OptionTypeLBox.SelectedValue ) ); }
		set { this.OptionTypeLBox.SelectedValue = value.ToString(); }
	}

	public string ResponseChoices {
		get { return ( this.ResponseChoicesBox.Text ); }
		set { this.ResponseChoicesBox.Text = value; }
	}

	public string Comments {
		get { return ( this.CommentsBox.Text ); }
		set { this.CommentsBox.Text = value; }
	}

	public Option Option {
		get {
			Option option = new Option( this.OptionID );
			option.ActivityGroupID = this.ActivityGroupID;
			option.Seq = this.Seq;
			option.QuestionText = this.QuestionText;
			option.ResponseType = this.ResponseType;
			option.ResponseChoices = this.ResponseChoices;
			option.Comments = this.Comments;
			return ( option );
		}
		set {
			this.OptionID = value.ID;
			this.ActivityGroupID = value.ActivityGroupID;
			this.Seq = value.Seq;
			this.QuestionText = value.QuestionText;
			this.ResponseType = value.ResponseType;
			this.ResponseChoices = value.ResponseChoices;
			this.Comments = value.Comments;
			this.OptionTypeChanged( null, null );
			this.DataBind();
		}
	}

	#endregion

	#region Events

	#region SaveItem
	public event EventHandler SaveItem {
		add { Events.AddHandler( SaveItemEventKey, value ); }
		remove { Events.RemoveHandler( SaveItemEventKey, value ); }
	}
	protected virtual void OnSaveItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SaveItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SaveItemEventKey = new object();
	#endregion

	#region CancelItem
	public event EventHandler CancelItem {
		add { Events.AddHandler( CancelItemEventKey, value ); }
		remove { Events.RemoveHandler( CancelItemEventKey, value ); }
	}
	protected virtual void OnCancelItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelItemEventKey = new object();
	#endregion

	#region DeleteItem
	public event EventHandler DeleteItem {
		add { Events.AddHandler( DeleteItemEventKey, value ); }
		remove { Events.RemoveHandler( DeleteItemEventKey, value ); }
	}
	protected virtual void OnDeleteItem( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[DeleteItemEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object DeleteItemEventKey = new object();
	#endregion

	#endregion

	protected void OptionTypeChanged( Object sender, EventArgs args ) {
		this.ResponseChoicesRow.Visible = ( this.ResponseType == OptionResponseTypes.DropDownList );
	}

}
