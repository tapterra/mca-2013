<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebsitesEditor.ascx.cs" Inherits="WebsitesEditor" %>
<%@ import namespace="ABS" %>

<dbui:admindatagrid runat="server" id="AdminGrid" OnFormControlsRead="ReadFormControls" >
	
	<formcontrols>
		<br />
		<table border="0" cellpadding="2" cellspacing="0" width="100%">
			<tr>
				<td>
					<stic:labeledtextbox runat="server" id="NameBox" label="Site Name"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).Name %>'
					 frame-labelmark="*"
					 tooltip="A name/title for this website"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="NameBoxReqVal" controltovalidate="NameBox" 
					 setfocusonerror="true" errormessage="A value is required for Site Name."
					/>
				</td>
				<td colspan="2">
					<stic:labeledtextbox runat="server" id="JobNoBox" label="MC&A Job Number"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).JobNo %>'
					 frame-labelmark="*"
					 tooltip="The MC&A job number for this website"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator1" controltovalidate="JobNoBox" 
					 setfocusonerror="true" errormessage="A value is required for Job Number."
					/>
				</td>
				<td colspan="2">
					<stic:labeledtextbox runat="server" id="HostVendorAidBox" label="ATCO Vendor AID"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).HostVendorAid %>'
					 frame-labelmark="*"
					 tooltip="The ATCO Host Vendor ID for this website"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator2" controltovalidate="HostVendorAidBox" 
					 setfocusonerror="true" errormessage="A value is required for ATCO Vendor AID."
					/>
				</td>
				<td>
					<stic:labeledtextbox runat="server" id="SiteRegionAidBox" label="Site Region AID"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).SiteRegionAid %>'
					 tooltip="The site-specific Region ID for this website"
					/>
				</td>
			</tr>
			<tr>
				<td style="padding-top:8px" class="xbox" >
					<asp:checkbox id="IsNoPayXBox" runat="server" text="No-Payment Site?" 
						checked='<%# ((ABS.Website) AdminGrid.DBItem).IsNoPaymentSite %>'
						tabindex='1'
					/>
				</td>
				<td>
					<stic:labeleddropdownlist runat="server" id="BookingStartMoLBox" 
					  label="First Month"
						selectedvalue='<%# ((ABS.Website) AdminGrid.DBItem).BookingStartMo %>'
						tabindex='1'
					>
						<asp:listitem value="1" text="Jan" />
						<asp:listitem value="2" text="Feb" />
						<asp:listitem value="3" text="Mar" />
						<asp:listitem value="4" text="Apr" />
						<asp:listitem value="5" text="May" />
						<asp:listitem value="6" text="Jun" />
						<asp:listitem value="7" text="Jul" />
						<asp:listitem value="8" text="Aug" />
						<asp:listitem value="9" text="Sep" />
						<asp:listitem value="10" text="Oct" />
						<asp:listitem value="11" text="Nov" />
						<asp:listitem value="12" text="Dec" />
					</stic:labeleddropdownlist>
				</td>
				<td>
					<stic:labeleddropdownlist runat="server" id="BookingStartYrLBox" label="Year" 
						selectedvalue='<%# ((ABS.Website) AdminGrid.DBItem).BookingStartYr %>'
						tabindex='1'
					>
						<asp:listitem value="2010" text="2010" />
						<asp:listitem value="2011" text="2011" />
						<asp:listitem value="2012" text="2012" />
						<asp:listitem value="2013" text="2013" />
						<asp:listitem value="2014" text="2014" />
						<asp:listitem value="2015" text="2015" />
					</stic:labeleddropdownlist>
				</td>
				<td>
					<stic:labeleddropdownlist runat="server" id="BookingEndMoLBox" 
					  label="Last Month"
						selectedvalue='<%# ((ABS.Website) AdminGrid.DBItem).BookingEndMo %>'
						tabindex='1'
					>
						<asp:listitem value="1" text="Jan" />
						<asp:listitem value="2" text="Feb" />
						<asp:listitem value="3" text="Mar" />
						<asp:listitem value="4" text="Apr" />
						<asp:listitem value="5" text="May" />
						<asp:listitem value="6" text="Jun" />
						<asp:listitem value="7" text="Jul" />
						<asp:listitem value="8" text="Aug" />
						<asp:listitem value="9" text="Sep" />
						<asp:listitem value="10" text="Oct" />
						<asp:listitem value="11" text="Nov" />
						<asp:listitem value="12" text="Dec" />
					</stic:labeleddropdownlist>
				</td>
				<td>
					<stic:labeleddropdownlist runat="server" id="BookingEndYrLBox" label="Year"
						selectedvalue='<%# ((ABS.Website) AdminGrid.DBItem).BookingEndYr %>'
						tabindex='1'
					>
						<asp:listitem value="2010" text="2010" />
						<asp:listitem value="2011" text="2011" />
						<asp:listitem value="2012" text="2012" />
						<asp:listitem value="2013" text="2013" />
						<asp:listitem value="2014" text="2014" />
						<asp:listitem value="2015" text="2015" />
					</stic:labeleddropdownlist>
				</td>
				<td>
					
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="border-top: 1px solid white; margin-top:10px;">
			<tr><td colspan="3" style="padding-bottom:6px" class="h3">Voucher Header:</td></tr>
			<tr>
				<td valign="top">
					<stic:labeledtextbox runat="server" id="HostVendorAddressBox" label="Vendor Address"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).HostVendorAddress %>'
					 textmode="MultiLine" rows="6" wrap="true"
					 frame-labelmark="*"
					 tooltip="Company name and address text for the voucher letter-head."
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator4" controltovalidate="HostVendorAddressBox" 
					 setfocusonerror="true" errormessage="A value is required for Vendor Address."
					/>
				</td>
				<td colspan="2" valign="top">
					<stic:labeledtextbox runat="server" id="VoucherLogoUrlBox" label="Logo Graphic URL"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).VoucherLogoUrl %>'
					 frame-labelmark="*"
					 tooltip="URL to the graphics file for the voucher letter-head logo."
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator6" controltovalidate="VoucherLogoUrlBox" 
					 setfocusonerror="true" errormessage="A value is required for Logo Graphic URL."
					/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="border-top: 1px solid white; margin-top:10px;">
			<tr><td colspan="3" style="padding-bottom:6px" class="h3">Voucher Emails:</td></tr>
			<tr>
				<td colspan="1">
					<stic:labeledtextbox runat="server" id="VoucherEmailFromAddressBox" label="From Address"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).VoucherEmailFromAddress %>'
					 tooltip="Email address to use to send the booking vouchers"
					 frame-labelmark="*"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator3" controltovalidate="VoucherEmailFromAddressBox" 
					 setfocusonerror="true" errormessage="A value is required for From Address."
					/>
				</td>
				<td colspan="2">
					<stic:labeledtextbox runat="server" id="VoucherEmailBccAddressesBox" label="Addresses to BCC"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).VoucherEmailBccAddresses %>'
					 tooltip="One or more email address to blind-copy all voucher emails to"
					/>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<stic:labeledtextbox runat="server" id="VoucherEmailSubjectBox" label="Email Subject Line"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).VoucherEmailSubject %>'
					 frame-labelmark="*"
					 tooltip="The text for the voucher-email subject line"
					/>
					<asp:requiredfieldvalidator runat="server" display="none" 
					 id="Requiredfieldvalidator5" controltovalidate="VoucherEmailSubjectBox" 
					 setfocusonerror="true" errormessage="A value is required for Email Subject Line."
					/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="border-top: 1px solid white; margin-top:10px;">
			<tr><td colspan="3" style="padding-bottom:6px" class="h3">Testing:</td></tr>
			<tr>
				<td valign="top">
					<asp:checkbox runat="server" id="IsBookingTestModeXBox" text="Booking Engine Test Mode"
					 checked='<%# ((ABS.Website) AdminGrid.DBItem).IsBookingTestMode %>'
					 tooltip="Check this box to run ATCO booking in TEST mode"
					/>
				</td>
				<td valign="top">
					<asp:checkbox runat="server" id="IsPmtProcessorTestModeXBox" text="Credit Card Test Mode"
					 checked='<%# ((ABS.Website) AdminGrid.DBItem).IsPmtProcessorTestMode %>'
					 tooltip="Check this box to run credit-card processing in TEST mode"
					/>
				</td>
				<td valign="top">
					<stic:labeledtextbox runat="server" id="ExcReportsToAddressBox" label="Email Error Reports To"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).ExcReportsToAddress %>'
					 tooltip="The email address(es) to send system error reports to"
					/>
				</td>
			</tr>
		</table>
		<table border="0" cellpadding="2" cellspacing="0" width="100%" style="border-top: 1px solid white; margin-top:10px;">
			<tr>
				<td>
					<stic:labeledtextbox runat="server" id="CommentsBox" label="Comments"
					 text='<%# ((ABS.Website) AdminGrid.DBItem).Comments %>'
					 textmode="MultiLine" rows="6" wrap="true"
					/>
				</td>
			</tr>
		</table>
		
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Website"				datafield="Name"			sortexpression="name" /> 
		<asp:boundcolumn headertext="Job Number"		datafield="JobNo"			sortexpression="jobno" /> 
	</gridcolumns>
	
</dbui:admindatagrid>
