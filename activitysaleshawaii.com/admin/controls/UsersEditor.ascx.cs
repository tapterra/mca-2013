using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using StarrTech.WebTools.CMS.UI;
using StarrTech.WebTools.Controls;
using StarrTech.WebTools.Data;
using StarrTech.WebTools.DBUI;
using ABS;

/// <summary>
///		Summary description for UsersEditor.
/// </summary>
public partial class UsersEditor : System.Web.UI.UserControl, IAdminDataGridContentEditor {

	int IAdminDataGridContentEditor.NodeID {
		get { return ( this.nodeID ); }
		set { this.nodeID = value; }
	}
	private int nodeID = -1;

	AdminDataGrid IAdminDataGridContentEditor.DataGrid {
		get { return ( this.AdminGrid ); }
	}

	public Unit Width {
		get { return ( AdminGrid.Width ); }
		set { AdminGrid.Width = value; }
	}


	#region Event Handlers

	public void ClearSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledTextBox firstName = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SFirstName" );
		LabeledTextBox lastName = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SLastName" );
		LabeledTextBox title = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:STitle" );

		// Reset the controls
		if ( firstName != null ) firstName.Text = "";
		if ( lastName != null ) lastName.Text = "";
		if ( title != null ) title.Text = "";
	}

	public void ReadSearchControls( Object sender, EventArgs args ) {
		// Get a reference to all the search-panel controls
		LabeledTextBox firstName = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SFirstName" );
		LabeledTextBox lastName = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:SLastName" );
		LabeledTextBox title = (LabeledTextBox) AdminGrid.FindControl( "SearchControls:STitle" );

		// Clear out any previous search values
		SiteUser.NewSearch();

		// Assign values to the static search properties
		if ( firstName != null ) SiteUser.SFirstName = firstName.Text;
		if ( lastName != null ) SiteUser.SLastName = lastName.Text;
		if ( title != null ) SiteUser.STitle = title.Text;

	}

	public void InitFormControls( Object sender, EventArgs args ) {
		// Set up the GroupBoxList control's binding to the values in the SiteUser.GroupIDs property
		this.BindFormGroupBoxList();
	}

	public void ReadFormControls( Object sender, EventArgs args ) {
		// Copy data from the Form Controls to the current DBItem
		// Get a reference to all the form-panel's controls
		LabeledTextBox firstName = (LabeledTextBox) AdminGrid.FindControl( "FormControls:FirstName" );
		LabeledTextBox lastName = (LabeledTextBox) AdminGrid.FindControl( "FormControls:LastName" );
		LabeledTextBox title = (LabeledTextBox) AdminGrid.FindControl( "FormControls:Title" );
		CheckBox supervisor = (CheckBox) AdminGrid.FindControl( "FormControls:IsSupervisor" );
		LabeledTextBox emailAddress = (LabeledTextBox) AdminGrid.FindControl( "FormControls:EmailAddress" );
		LabeledTextBox password = (LabeledTextBox) AdminGrid.FindControl( "FormControls:Password" );
		LabeledCheckBoxList groupBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:GroupBoxList" );
		LabeledTextBox comments = (LabeledTextBox) AdminGrid.FindControl( "FormControls:Comments" );
		// Get a reference to the SiteUser object to be edited...
		SiteUser user = new SiteUser();
		user.ID = this.AdminGrid.FormController.RecordID;
		if ( firstName != null )
			user.FirstName = firstName.Text;
		if ( lastName != null )
			user.LastName = lastName.Text;
		if ( title != null )
			user.Title = title.Text;
		if ( emailAddress != null )
			user.EmailAddress = emailAddress.Text;
		if ( password != null )
			user.Password = password.Text;
		if ( groupBoxList != null ) {
			for ( int i = 0 ; i < groupBoxList.Items.Count ; i++ ) {
				if ( groupBoxList.Items[i].Selected )
					user.GroupIDs.Add( Convert.ToInt32( groupBoxList.Items[i].Value ) );
			}
		}
		if ( comments != null )
			user.Comments = comments.Text;
		this.AdminGrid.DBItem = user;
	}

	public void InitEditor() {
		// Initialize the AdminDataGrid...
		// Let it know which BaseSqlPersistable class it is working with for this page
		this.AdminGrid.DBItemType = typeof( SiteUser );

		// Set up the grid for the user's permissions
		if ( SiteUser.CurrentUser != null ) {
			this.AdminGrid.CanEditRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminUsersUpdate );
			this.AdminGrid.CanAddRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminUsersAdd );
			this.AdminGrid.CanDeleteRecord = SiteUser.CurrentUser.HasPermission( PermissionKeys.AdminUsersDelete );
		} else {
			this.AdminGrid.CanEditRecord = false;
			this.AdminGrid.CanAddRecord = false;
			this.AdminGrid.CanDeleteRecord = false;
		}
		// Start things rolling
		if ( !this.IsPostBack ) {
			this.SetupFormGroupBoxList();
			this.AdminGrid.GridController.SortExpression = "full_name";
			this.AdminGrid.GridController.SortOrder = "asc";
			this.AdminGrid.InitPanel();
		}
	}

	public void ResetEditor() {
		this.AdminGrid.BindGrid();
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		this.InitEditor();
	}

	#endregion

	#region Private Methods

	private void BindFormGroupBoxList() {
		// Set up the GroupBoxList control's binding to the values in the SiteUser.GroupIDs property
		// Get a reference to the form-panel's GroupBoxList control
		LabeledCheckBoxList groupBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:GroupBoxList" );
		if ( groupBoxList != null ) {
			// Get a reference to the SiteUser object being edited...
			SiteUser user = (SiteUser) AdminGrid.DBItem;
			// Loop over each item in the GroupBoxList
			for ( int i = 0 ; i < groupBoxList.Items.Count ; i++ ) {
				// If this list-item's value is in the user's GroupIDs collection, check the item's checkbox
				groupBoxList.Items[i].Selected = user.GroupIDs.Contains( Convert.ToInt32( groupBoxList.Items[i].Value ) );
			}
		}
	}

	private void SetupFormGroupBoxList() {
		// Get a reference to the form-panel's GroupBoxList control
		LabeledCheckBoxList groupBoxList = (LabeledCheckBoxList) AdminGrid.FindControl( "FormControls:GroupBoxList" );
		if ( groupBoxList != null ) {
			// Load a DataSet of all the existing permission-groups
			DataSet ds;
			using ( SqlDatabase db = new SqlDatabase() )
				ds = db.GetDataSet( "PERMISSION_GROUPS", 
					"select id, group_name from PERMISSION_GROUPS where ( is_deleted = 0 ) order by group_name"
				);
			// Bind the control to the DataSet
			groupBoxList.DataSource = ds.Tables["PERMISSION_GROUPS"].DefaultView;
			groupBoxList.DataTextField = "group_name";
			groupBoxList.DataValueField = "id";
			groupBoxList.DataBind();
		}
	}

	#endregion

}
