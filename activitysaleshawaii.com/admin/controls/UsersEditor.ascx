<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UsersEditor.ascx.cs" Inherits="UsersEditor" %>
<%@ import namespace="ABS" %>

<dbui:admindatagrid runat="server" id="AdminGrid" 
	OnSearchControlsRead="ReadSearchControls"
	OnSearchControlsClear="ClearSearchControls"
	OnFormControlsInit="InitFormControls"
	OnFormControlsRead="ReadFormControls"
>
	
	<searchcontrols>
		<table border="0" cellpadding="0" cellspacing="4" width="100%" style="border-bottom:1px solid #0055aa">
			<tr>
				<td rowspan="3" align="left" valign="top">
					<table border="0" cellpadding="0" cellspacing="4" width="100%">
						<tr>
							<td width="50%">
								<stic:labeledtextbox runat="server" id="SFirstName" label="First Name" width="100%" 
									tabindex='1'
								/>
							</td>
							<td width="50%">
								<stic:labeledtextbox runat="server" id="SLastName" label="Last Name" width="100%" 
									tabindex='1'
								/>
							</td>
						</tr>
						<tr>
							<td width="50%">
								<stic:labeledtextbox runat="server" id="STitle" label="Title/Department"  width="100%" 
									tabindex='1'
								/>
							</td>
							<td width="50%">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</searchcontrols>
	
	<formcontrols>
		<table border="0" cellpadding="0" cellspacing="4" width="100%">
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td width="50%">
					<stic:labeledtextbox runat="server" id="FirstName" label="First Name" width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).FirstName %>'
						cssclass="req"
						tabindex='1'
						/>
					<asp:requiredfieldvalidator id="FirstNameReqVal" runat="server"
						controltovalidate="FirstName" display="none" errormessage="A value is required for First Name."
					/>
				</td>
				<td width="50%">
					<stic:labeledtextbox runat="server" id="LastName" label="Last Name" width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).LastName %>'
						cssclass="req"
						tabindex='1'
					/>
					<asp:requiredfieldvalidator id="LastNameReqVal" runat="server"
						controltovalidate="LastName" display="none" errormessage="A value is required for Last Name."
					/>
				</td>
				<td rowspan="3" valign="top" style="padding:0; vertical-align:top" width="200" height="104">
					<stic:labeledcheckboxlist runat="server" id="GroupBoxList" label="Groups" height="104" width="200" 
						tabindex='2'
					/>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<stic:labeledtextbox runat="server" id="Title" label="Title/Department"  width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).Title %>'
						tabindex='1'
					/>
				</td>
				<td width="50%">
				</td>
			</tr>
			<tr>
				<td width="50%">
					<stic:labeledtextbox runat="server" id="EmailAddress" label="Email Address" width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).EmailAddress %>'
						tabindex='1'
					/>
					<stic:emailvalidator runat="server" id="EmailRegexVal" 
						controltovalidate="EmailAddress" display="none" 
						errormessage="A valid email-address (account@domain.xxx) is required for Email Address."
					/>
				</td>
				<td width="50%">
					<stic:labeledtextbox runat="server" id="Password" label="Password" width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).Password %>'
						tabindex='1'
					/>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<stic:labeledtextbox runat="server" id="Comments" label="Comments" textmode="multiline" rows="12" wrap="true" width="100%" 
						text='<%# ((SiteUser) AdminGrid.DBItem).Comments %>'
						tabindex='3'
					/>
				</td>
			</tr>
		</table>
	</formcontrols>
	
	<gridcolumns>
		<asp:boundcolumn headertext="Name"							datafield="FullNameLF"			sortexpression="full_name" /> 
		<asp:boundcolumn headertext="Title/Department"	datafield="Title"						sortexpression="u.title" /> 
		<asp:boundcolumn headertext="Email Address"			datafield="EmailAddress"		sortexpression="u.email_address" /> 
	</gridcolumns>
	
</dbui:admindatagrid>
