<%@ Control Language="C#" ClassName="WebsitePickerPanel" %>
<%@ import namespace="ABS" %>
<%@ import namespace="System.Collections.Generic" %>

<script runat="server">

	public int ActivityGroupID {
		get {
			object obj = this.ViewState["ActivityGroupID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set {
			this.ViewState["ActivityGroupID"] = value;
			this.activityGroup = null;
			this.Rebind();
		}
	}

	protected ActivityGroup ActivityGroup {
		get {
			if ( this.activityGroup == null ) {
				this.activityGroup = new ActivityGroup( this.ActivityGroupID );
			}
			return ( this.activityGroup );
		}
	}
	private ActivityGroup activityGroup;


	public int SelectedSiteID {
		get { return ( Convert.ToInt32( this.WebsiteLBox.SelectedValue ) ); }
	}
	
</script>

<script runat="server">

	public void Rebind() {
		this.WebsiteLBox.Items.Clear();
		Websites sites = Websites.CurrentReload;
		int currentSiteID = this.ActivityGroup.WebsiteID;
		foreach ( int siteID in sites.Keys ) {
			if ( siteID != currentSiteID ) {
				Website site;
				if ( sites.TryGetValue( siteID, out site ) ) {
					this.WebsiteLBox.Items.Add( new ListItem( site.Name, site.ID.ToString() ) );
				}
			}
		}
		if ( this.WebsiteLBox.Items.Count > 0 ) {
			this.WebsiteLBox.SelectedIndex = 0;
		}
	}

</script>

<script runat="server">

	public event EventHandler Select {
		add { Events.AddHandler( SelectEventKey, value ); }
		remove { Events.RemoveHandler( SelectEventKey, value ); }
	}
	protected virtual void OnSelect( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[SelectEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object SelectEventKey = new object();
	
</script>

<script runat="server">

	public event EventHandler Cancel {
		add { Events.AddHandler( CancelEventKey, value ); }
		remove { Events.RemoveHandler( CancelEventKey, value ); }
	}
	protected virtual void OnCancel( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[CancelEventKey];
		if ( handler != null ) {
			handler( sender, args );
		}
	}
	private static readonly object CancelEventKey = new object();
	
</script>

<table border="0" cellpadding="2" cellspacing="0" width="400">
	<tr>
		<td style="padding-bottom:20px">
			<asp:Label id="HeaderLabel" runat="server" text="Select Website" cssclass="h2" />
		</td>
		<td align="right" style="padding-bottom:20px">
			<stic:button runat="server" id="SelectButton" text="Select" tabindex="9" onclick="OnSelect" />
			<stic:button runat="server" id="CancelButton" text="Cancel" tabindex="9" causesvalidation="false" onclick="OnCancel" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<asp:label runat="server" font-names="Verdana" font-size="8pt" font-bold="true">
				Please select the website you wish to copy this Activity Group to:
			</asp:label>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<asp:listbox runat="server" id="WebsiteLBox" rows="12" selectionmode="Single" />
		</td>
	</tr>
</table>
