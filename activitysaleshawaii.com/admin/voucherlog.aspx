﻿<%@ Page Language="C#" theme="admin" inherits="ABS.AdminPage" %>
<%@ import namespace="ABS" %>
<%@ import namespace="StarrTech.WebTools.Data" %>
<%@ import namespace="System.Data" %>

<script runat="server">
	private void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			this.VoucherDataBox.Text = NoVoucherNoMsg;
		}
		this.VoucherNoBox.Focus();
	}

	private const string NoVoucherNoMsg = "Enter Voucher Number(s) above separated by commas and click Submit.";
	private const string BookingNotFoundMsg = "No booking with the given voucher # was found - please try again.";
	private const string NullVoucherDataMsg = "The booking for the given voucher # had no logged voucher data.";
	
	protected virtual void OnSubmit( Object sender, EventArgs args ) {
		string voucherNo = this.VoucherNoBox.Text;
		if ( string.IsNullOrEmpty( voucherNo ) ) {
			this.VoucherDataBox.Text = NoVoucherNoMsg;
			return;
		}
		this.FindBooking( voucherNo );
	}
	
	protected void FindBooking( string VoucherNo ) {
		this.VoucherDataBox.Text = "";
		bool wereHits = false;
		try {
			string sql = "select * from BOOKINGS where voucher_no in ({0}) and is_deleted = 0 order by created_dt";
			// Clean up and single-quote the voucher-no values
			string vnos = "'" + VoucherNo.Replace( " ", "" ).Replace( ",", "', '" ) + "'";
			sql = string.Format( sql, vnos );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					while ( rdr.Read() ) {
						wereHits = true;
						this.VoucherDataBox.Text += Server.HtmlEncode( CleanupXml( rdr.GetString( rdr.GetOrdinal( "voucher_data" ) ) ) );
					}
				}
			}
			if ( string.IsNullOrEmpty( this.VoucherDataBox.Text ) ) {
				if ( !wereHits ) {
					this.VoucherDataBox.Text = BookingNotFoundMsg;
				} else {
					this.VoucherDataBox.Text = NullVoucherDataMsg;
				}
			}
		} catch (Exception e) {
			this.VoucherDataBox.Text = string.Format( "An error occurred: {0}", e.Message );
		}
	}

	protected string CleanupXml( string xml ) {
		if ( string.IsNullOrEmpty( xml ) ) { 
			return ( xml ); 
		}
		string junk1 = @" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""";
		string junk2 = @" xmlns=""http://www.atcosoftware.com/2004/05/Voucher""";
		return ( xml.Replace( junk1, "" ).Replace( junk2, "" ) + "\r\r" );
	}

</script>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Voucher Logs</h1>
	
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr>
			<td style="width:140px; font-family: Verdana; font-size:8pt; font-weight:bold">Voucher Numbers:</td>
			<td style="width:500px; font-family: Verdana; font-size:8pt">
				<asp:textbox runat="server" id="VoucherNoBox" style="font-family: Verdana; font-size:8pt; width:100%" tabindex="1" />
			</td>
			<td align="right">
				<stic:button runat="server" id="SubmitButton" text="Submit" onclick="OnSubmit" tabindex="2" />
			</td>
		</tr>
		<tr>
			<td colspan="3" style="border: 1px solid #999; padding:6px">
				<pre><asp:literal runat="server" id="VoucherDataBox" /></pre>
			</td>
		</tr>
	</table>
	
</asp:Content>

