<%@ Page Language="C#" inherits="ABS.AdminPage" maintainscrollpositiononpostback="true" validaterequest="false" %>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminWebsites );
	}
	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Websites</h1>

	<adm:websiteseditor runat="server" id="WebsitesEditorPanel" />

</asp:Content>

