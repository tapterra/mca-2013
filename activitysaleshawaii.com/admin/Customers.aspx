<%@ Page Language="C#" inherits="ABS.AdminPage" 
	maintainscrollpositiononpostback="true" validaterequest="false"
%>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminCustomers );
	}
	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

<h1>Customers</h1>

<adm:customerseditor runat="server" id="CustomersEditorPanel" />

</asp:Content>

