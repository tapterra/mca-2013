<%@ Page Language="C#" inherits="ABS.AdminPage" %>
<%@ import namespace="ABS" %>

<script runat="server">
 
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminUsers );
	}
	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Administrative Users</h1>
	<adm:userseditor runat="server" id="UsersGrid" />

</asp:Content>
