<%@ Page Language="C#" inherits="ABS.AdminPage" 
	maintainscrollpositiononpostback="true" validaterequest="false"
%>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminCategories );
	}
	
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Categories</h1>

	<adm:cateditor runat="server" id="CategoriesEditorPanel" />

</asp:Content>


