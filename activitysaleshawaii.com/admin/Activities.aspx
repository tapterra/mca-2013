<%@ Page Language="C#" inherits="ABS.AdminPage" 
	maintainscrollpositiononpostback="true" validaterequest="false"
%>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminActivities );
	}
	
</script>

<asp:content runat="Server" id="Content1" contentplaceholderid="MainPlaceHolder">

	<h1>Activity Groups</h1>

	<adm:activitieseditor runat="server" id="ActivitiesEditorPanel" />

</asp:content>
	
