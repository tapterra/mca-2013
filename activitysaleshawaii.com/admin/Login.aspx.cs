using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;
using StarrTech.WebTools;
using System.Drawing;
using System.Net.Mail;

public partial class Login : AdminPage {

	public override bool IsLogonPage {
		get { return ( true ); }
	}

	private static string appName = "Activity Booking System";

	public void DoSignIn( Object sender, EventArgs args ) {
		StatusMsgLbl.Text = "";
		string uid = UserIDBox.Text.Trim().ToLower();
		string pw = UserPWBox.Text.Trim().ToLower();
		StatusMsgLbl.Text = this.SignIn( uid, pw );
		if ( this.UserAuthorized ) {
			Response.Redirect( "~/admin/default.aspx" );
		}
	}

	public void DoIForgot( Object sender, EventArgs args ) {
		try {
			string uid = UserIDBox.Text.Trim().ToLower();
			if ( string.IsNullOrEmpty( uid ) ) {
				StatusMsgLbl.Text = "You must enter your <b>email address</b> before clicking the <b>I Forgot</b> button.";
				return;
			}
			SiteUser theUser = SiteUser.GetCurrent( uid );
			if ( theUser.ID > 0 ) {
				string emsg = String.Format( @"
{0} Access

You recently requested a reminder of your password for accessing the {0}:

    User ID:  {1}

    Password: {2}

Please contact the system administrator for the website if you have any further problems.
", 
				appName, theUser.EmailAddress.ToLower(), theUser.Password.ToLower() ).Replace( "\t", "" );

				string webmasterEmail = ConfigMgr.VoucherEmailFromAddress; 
				MailMessage msg = new MailMessage( webmasterEmail, theUser.EmailAddress, appName, emsg );
				SmtpClient client = new SmtpClient();
				client.Send( msg );
				StatusMsgLbl.Text = "Your access information has been sent to you by email - check your inbox (it could take several minutes for the message to reach you) and try again.";
				return;
			} else {
				StatusMsgLbl.Text = String.Format(
					"The email address <b>{0}</b> is invalid. Please make sure that the email address has been entered accurately, and try again.",
					theUser.EmailAddress
				);
			}
		} catch {
			StatusMsgLbl.Text = "There was a problem sending your access information to you - please try again later.";
			StatusMsgLbl.ForeColor = Color.DarkRed;
			StatusMsgLbl.Font.Bold = true;
		}
	}

	public string SignIn( string uid, string pw ) {
		// Make sure a user-name was entered
		if ( string.IsNullOrEmpty( uid ) ) {
			return ( "You must enter your <b>email address</b> before clicking the <b>Sign In</b> button." );
		}
		// Make sure a password was entered
		if ( string.IsNullOrEmpty( pw ) ) {
			return ( "You must enter your <b>password</b> before clicking the <b>Sign In</b> button." );
		}
		// try to load the User instance...
		SiteUser currentUser = SiteUser.GetCurrent( uid );
		if ( currentUser == null || currentUser.ID <= 0 ) {
			// Couldn't find this user - bad email address
			return ( "The <b>email address</b> you entered is unknown - please try again." );
		}
		// Make sure the password matches
		if ( pw != currentUser.Password.Trim().ToLower() ) {
			// If we made it to this point, the password didn't match...
			return ( "The <b>password</b> you entered is invalid - please try again." );
		}
		// We have a winner...
		SiteUser.CurrentUser = currentUser;
		return ( "" );
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		SiteUser.CurrentUser = null;
		UserIDBox.Focus();
	}

}
