using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;
using System.Web.Caching;
using System.Xml;

public partial class Masters_admin : System.Web.UI.MasterPage {

	private System.Xml.XmlDocument FixedMenusXmlDoc {
		get {
			// Use the cached XmlDocument created from the file at Page.MenuXmlFile
			string path = HttpContext.Current.Server.MapPath( "~/App_Data/AdminMenu.config" );
			System.Xml.XmlDocument doc = HttpContext.Current.Cache[path] as System.Xml.XmlDocument;
			if ( doc == null ) {
				// Need to (re)load the cache
				doc = new System.Xml.XmlDocument();
				doc.Load( path );
				// Insert the doc into the cache, with a trigger to expire if the source file changes
				HttpContext.Current.Cache.Insert( path, doc, new CacheDependency( path ) );
			}
			return ( doc );
		}
	}

	private void BuildFixedMenuItem( System.Xml.XmlElement item ) {
		if ( item.Name != "menuitem" ) {
			return;
		}
		System.Xml.XmlAttributeCollection attribs = item.Attributes;
		string id = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
		string label = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "label" ) ).Value;
		object urlAttrib = attribs.GetNamedItem( "url" );
		string url = "";
		if ( urlAttrib != null )
			url = ( (System.Xml.XmlAttribute) urlAttrib ).Value;
		string keyString = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "permission" ) ).Value;
		PermissionKeys key = (PermissionKeys) Enum.Parse( typeof( PermissionKeys ), keyString, true );
		if ( ( this.Page is AdminPage ) && ( SiteUser.CurrentUser != null ) && SiteUser.CurrentUser.HasPermission( key ) ) {
			MenuItem menuItem = new MenuItem( label, id, "", url );
			//topMenu.ChildItems.Add( menuItem );
			this.AdminMenu.Items.Add( menuItem );
		}
	}

	private void BuildFixedMenu( System.Xml.XmlElement menuElement ) {
		// Build the main menu row
		System.Xml.XmlAttributeCollection attribs = menuElement.Attributes;
		string id = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
		string label = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "label" ) ).Value.ToUpper();
		object urlAttrib = attribs.GetNamedItem( "url" );
		string url = "";
		if ( urlAttrib != null )
			url = ( (System.Xml.XmlAttribute) urlAttrib ).Value;
		string keyString = ( (System.Xml.XmlAttribute) attribs.GetNamedItem( "permission" ) ).Value;
		PermissionKeys key = (PermissionKeys) Enum.Parse( typeof( PermissionKeys ), keyString, true );
		if ( ( this.Page is AdminPage ) && ( SiteUser.CurrentUser != null ) && SiteUser.CurrentUser.HasPermission( key ) ) {
			//this.AddMenuCell( id, label, url, "primaryNav", 0 );
			//MenuItem menuItem = new MenuItem( label, id, "", url );
			//menuItem.Selectable = !string.IsNullOrEmpty( url );
			//this.AdminMenu.Items.Add( menuItem );
			// Build the sub item rows
			foreach ( System.Xml.XmlNode item in menuElement ) {
				if ( item.NodeType == System.Xml.XmlNodeType.Element ) {
					BuildFixedMenuItem( item as System.Xml.XmlElement );
				}
			}
		}
	}

	private void BuildFixedMenus() {
		System.Xml.XmlNode root = this.FixedMenusXmlDoc.DocumentElement;
		foreach ( System.Xml.XmlNode node in root ) {
			if ( node.NodeType == System.Xml.XmlNodeType.Element ) {
				System.Xml.XmlElement elem = node as System.Xml.XmlElement;
				if ( elem.Name == "menu" )
					BuildFixedMenu( elem );
			}
		}
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			//this.BuildFixedMenus();
			this.BuildMenus();
		}
	}

	private void BuildMenus() {
		System.Xml.XmlNode root = this.MenusXmlDoc.DocumentElement;
		foreach ( System.Xml.XmlNode node in root ) {
			if ( node.NodeType == System.Xml.XmlNodeType.Element ) {
				System.Xml.XmlElement elem = node as System.Xml.XmlElement;
				if ( elem.Name == "menu" ) {
					BuildMenu( elem );
				}
			}
		}
	}

	private void BuildMenu( System.Xml.XmlElement menuElement ) {
		XmlAttributeCollection attribs = menuElement.Attributes;
		string id = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
		string label = ( (XmlAttribute) attribs.GetNamedItem( "label" ) ).Value;
		object attrib = attribs.GetNamedItem( "url" );
		string url = attrib == null ? "" : ( (XmlAttribute) attrib ).Value;

		attrib = attribs.GetNamedItem( "newwindow" );
		string newwindow = attrib == null ? "" : ( (XmlAttribute) attrib ).Value;
		string target = newwindow.ToLower() == "true" ? "_blank" : "";
		MenuItem topMenuItem = new MenuItem( label, id, "", url, target );
		foreach ( System.Xml.XmlNode item in menuElement ) {
			if ( item.NodeType == System.Xml.XmlNodeType.Element ) {
				BuildMenuItem( item as System.Xml.XmlElement, topMenuItem );
			}
		}
		XmlAttribute permAttrib = (XmlAttribute) attribs.GetNamedItem( "permission" );
		if ( permAttrib != null ) {
			string key = permAttrib.Value;
			if ( UserHasPermission( key ) ) {
				this.AdminMenu.Items.Add( topMenuItem );
			}
		} else {
			if ( topMenuItem.ChildItems.Count > 0 ) {
				this.AdminMenu.Items.Add( topMenuItem );
			}
		}
	}

	protected struct MItem {
		public string ID;
		public string Label;
		public string Url;
		public string Target;
		public string Key;

		public MItem( string id, string label, string url, string target, string key ) {
			this.ID = id;
			this.Label = label;
			this.Url = url;
			this.Target = target;
			this.Key = key;
		}
	}

	private MItem ParseMenuElement( System.Xml.XmlElement item ) {
		XmlAttributeCollection attribs = item.Attributes;
		string id = ( (XmlAttribute) attribs.GetNamedItem( "id" ) ).Value;
		string label = ( (XmlAttribute) attribs.GetNamedItem( "label" ) ).Value;
		object attrib = attribs.GetNamedItem( "url" );
		string url = attrib == null ? "" : ( (XmlAttribute) attrib ).Value;
		attrib = attribs.GetNamedItem( "newwindow" );
		string newwindow = attrib == null ? "" : ( (XmlAttribute) attrib ).Value;
		string target = newwindow.ToLower() == "true" ? "_blank" : "";
		string keyString = ( (XmlAttribute) attribs.GetNamedItem( "permission" ) ).Value;
		return ( new MItem( id, label, url, target, keyString ) );
	}

	private void BuildMenuItem( System.Xml.XmlElement item, MenuItem topMenuItem ) {
		if ( item.Name != "menuitem" ) { return; }
		MItem mitem = this.ParseMenuElement( item );
		if ( UserHasPermission( mitem.Key ) ) {
			MenuItem menuItem = new MenuItem( mitem.Label, mitem.ID, "", mitem.Url, mitem.Target );
			topMenuItem.ChildItems.Add( menuItem );
		}
	}

	private XmlDocument MenusXmlDoc {
		get {
			// Use the cached XmlDocument created from the file at Page.MenuXmlFile
			string path = HttpContext.Current.Server.MapPath( "~/App_Data/AdminMenu.config" );
			XmlDocument doc = HttpContext.Current.Cache[path] as XmlDocument;
			if ( doc == null ) {
				doc = new XmlDocument();
				doc.Load( path );
				HttpContext.Current.Cache.Insert( path, doc, new CacheDependency( path ) );
			}
			return ( doc );
		}
	}

	private bool UserHasPermission( string KeyString ) {
		return ( ( SiteUser.CurrentUser != null ) && SiteUser.CurrentUser.HasPermission( KeyString ) );
	}
	
}
