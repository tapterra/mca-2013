<%@ Page Language="C#" theme="admin" inherits="ABS.AdminPage" maintainscrollpositiononpostback="true" validaterequest="false" %>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminIslands );
	}
	
</script>

<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Islands</h1>

	<adm:islandseditor runat="server" id="IslandsEditorPanel" />

</asp:Content>

