<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Welcome</h1>
	<p><asp:label runat="server" id="tracemsg" forecolor="red" /></p>
	<p>
		<asp:label runat="server" id="StatusMsgLbl">
			Access to this site is restricted, and we are unable to identify you. 
			Please enter your email address and password in the fields below and click the <b>Sign In</b> button.
		</asp:label>
	</p>
	<br />
	<center>
		<table bgcolor="#DDDDDD" cellspacing="5" cellpadding="10" border="1" width="340" style="margin:40px">
			<tr>
				<td>
					<table id="Table1" runat="server" cellspacing="0" cellpadding="5" border="0" width="100%" >
						<tr>
							<td align="right">
								<asp:label runat="server" id="useridlbl" text="Email&nbsp;Address:" font-name="tahoma" font-size="8pt" font-bold="true" />
								</td>
							<td>
								<asp:textbox runat="server" id="UserIDBox" columns="20" font-name="tahoma" />
								</td>
							</tr>
						<tr>
							<td align="right">
								<asp:label runat="server" id="userpwlbl" text="Password:" font-name="tahoma" font-size="8pt" font-bold="true" />
								</td>
							<td>
								<asp:textbox runat="server" id="UserPWBox" columns="20" textmode="password" font-name="tahoma" />
								</td>
							</tr>
						<tr>
							<td colspan="2" align="center">
								<asp:checkbox runat="server" id="savetoclient" text="Save this information on my computer" font-name="tahoma" font-size="8pt" font-bold="true" 
									visible="false" />
								</td>
							</tr>
						<tr>
							<td colspan="2" align="center">
								<asp:button runat="server" id="submitbtn" text="Sign In" forecolor="#006600" 
									onclick="DoSignIn" width="80" font-name="tahoma" font-size="10pt" font-bold="true" />
								<asp:button runat="server" id="iforgotbtn" text="I Forgot" forecolor="#990000" 
									onclick="DoIForgot" width="80" font-name="tahoma" font-size="10pt" font-bold="true" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
	</center>
	<br />
	<br />

	<asp:panel runat="server" id="logonnotes">
		<p>Enter your email address and password and click the <b>Sign In</b> button to proceed.</p>
		<p>Enter your email address and click the <b>I Forgot</b> button if you forgot your password.</p>
	</asp:panel>
	
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
</asp:Content>

