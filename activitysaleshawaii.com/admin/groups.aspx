<%@ Page Language="C#" inherits="ABS.AdminPage" %>
<%@ import namespace="ABS" %>

<script runat="server">
	
	private void Page_Load( object sender, System.EventArgs e ) {
		// Authorize the user...
		this.AuthorizeUser( PermissionKeys.AdminGroups );
	}
	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPlaceHolder" Runat="Server">

	<h1>Permission Groups</h1>
	<adm:groupseditor runat="server" id="GroupsGrid" />

</asp:Content>
