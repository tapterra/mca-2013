<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/dh1.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, it has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago.<br />
<Br />
Oahu is the most popular of the Hawaiian Islands and it is easy to understand why!  With Waikiki as a central hub, you can explore the legendary North Shore of Oahu one day, and spend the next day on the east side snorkeling at Hanauma Bay, a protected marine sanctuary with tons of colorful fish.  It is clear that Oahu offers just the right amount of diversity for the adventurous as well as the cautious visitor.  Thrill seekers can skydive at Mokuleia while daydreamers can relax peacefully on the beach.  Exquisite dining and exciting nightlife also entice people to Oahu again and again.<br />
<Br />
			The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.
For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits and cover-ups is the usual daytime attire. This attire should also be appropriate for recreation and sightseeing activities.<br />
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
