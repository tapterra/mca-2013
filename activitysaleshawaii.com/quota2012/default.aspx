<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/oahurs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Welcome to the website to sign up for Quota International's 2012 Convention Tours. From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. Feel free to browse this website and discover all of these activities for yourself. <br />
<Br />
Reservations are now closed. Online bookings are no longer being taken. Limited tickets for some activities may be available on-site, so check with the hospitality desk upon arrival. 
<Br />
<Br />
For questions concerning an existing Recreation & Sightseeing Activity Reservation, contact:
<Br />
<Br />
MC&A, Inc.<Br />
Attention: Quota International 2012<Br />
615 Piikoi Street, Suite 1000<Br />
Honolulu, Hawaii  96814<Br />
<Br />
Toll Free Phone: 877-589-5589 (United States and Canada only)<Br />
Phone: 808-589-5558<Br />
Fax: 808-589-5583<Br />
<Br />Email:
<a href="mailto:quota2012@mcahawaii.com"><font color="#0177CC">quota2012@mcahawaii.com</a>
<Br />


		</div>
	

</asp:content>
