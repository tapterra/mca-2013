<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<div style="width: 600px; height: auto; position: absolute;">
<div style="margin-right:20px; float: left;">
<img src="/images/maui-guide.jpg" alt="Makawao, Maui"><br>
<img src="/images/maui-waterfall.jpg" alt="Makawao, Maui">
</div>
<p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">Although made up of several dozen islands, the Hawaiian Islands are defined by eight major ones - Oahu, Kauai, Maui, Hawaii, Molokai, Lanai, Niihau and Kahoolawe. While each island rests in relatively close proximity of one another, each has its own qualities and characteristics.<br />
</p>

<p class="PublisherDescription">For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits & cover-ups is the usual daytime attire.</p>
</p>
<p class="PublisherGroupTitle">About Maui</p>
<p class="PublisherDescription">Maui has a smaller population than you'd expect, making it popular with visitors who are looking for sophisticated diversions and amenities in the small, intimate towns peppered throughout the island.<Br>
<br>Maui's unique vistas also make it a much anticipated destination. From beaches voted among the best in the world to the scenic heights of Haleakala Crater, a visit to "The Magic Isle" recharges the senses. But like every good magic trick, you'll have to see it for yourself to believe it.</p>
</div>
<div style="width: 250px; height: 500px; background-color: #fcaf3e; float: right;">
<img src="/images/head-funfacts.jpg" alt="Island Fun Facts!" width="250" />
<p ID="popact">
The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.<Br><br>Maui, "The Magic Isle," is the second largest Hawaiian island.<Br><br>
Whale watching season begins in mid-December and ends in mid-May.<br><br>
Kahului Airport (OGG) is the island's main airport. There are two smaller airports on Maui as well: Kapalua Airport (JHM), on the Kaanapali side, services the major hotels and resorts, while Hana Airport (HNM) gets travelers to this isolated area.<br><Br>
</p>

</div>

</asp:content>
