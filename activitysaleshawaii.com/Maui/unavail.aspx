<%@ Page Language="C#" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

	<div style="width:600px" >
		<font color="#0066cc" style="font-face: arial; font-size: 16pt;" face="Arial"><b>Unavailable</b></font><br /><br />
		<p>
			We're sorry, but the feature you are requesting is temporarily unavailable. 
		</p>
		<br />
		<p>
			Please try again later, or call our service desk at 1 (877) 589-5568.<br>
			If you have any questions, email us at <a href="mailto:r2@mcahawaii.com">R3@mcahawaii.com</a>.<br>
		</p>
		<br />
	</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>
