<%@ Page Language="C#" AutoEventWireup="true" CodeFile="itinerary.aspx.cs" Inherits="Itinerary" %>
<%@ import namespace="ABS" %>

<asp:content runat="server" contentplaceholderid="MainContent" >

	<asp:panel runat="server" id="EmptyCartMsgPanel">
		<br />
		<p class="publisherDescription">
			Your itinerary is currently empty.
		</p>
		<br />
		<p class="publisherDescription">
			Please select an option above to see the available activities.
		</p>
	</asp:panel>
	
	<abs:itinerary runat="server" id="ItineraryPanel" 
		cssclass="ItinBookPanel"
		headercellcssclass="ItinHeader"
		footercellcssclass="ItinFooter"
		labelcssclass="ItinPanelLabel" 
		valuecssclass="ItinPanelValue"
		buttoncssclass="StepButton"
		
		BookPanelCssClass="ItinBookPanel"
		BookPanelCellSpacing="0"
		BookPanelCellPadding="0"
		BookPanelHeaderTableCssClass="ItinBookHeaderTable"
		BookPanelFooterTableCssClass="ItinBookFooterTable"
		BookPanelGuestPanelsCellCssClass=""
		BookPanelLabelCssClass="ItinPanelLabel"
		BookPanelValueCssClass="ItinPanelValue"
		BookPanelActivityNameCssClass="ItinPanelLabel"
		BookPanelButtonCssClass="ItemButton"

		GuestPanelCssClass="ItinGuestPanel"
		GuestPanelCellSpacing="0"
		GuestPanelCellPadding="0"
		GuestPanelLabelCssClass="ItinPanelLabel"
		GuestPanelGuestNameCssClass="ItinPanelValue"
		GuestPanelGuestTypeCssClass="ItinPanelGuestTypeValue"
		GuestPanelPriceCssClass="ItinPanelValue"
		GuestPanelOptionsCssClass="ItinPanelValue"
	/>

</asp:content>
