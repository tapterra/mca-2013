<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >

<div style="width: 800px; margin:0">

<div style="width: 650px; position: absolute;">
	<div style="float: left; margin:0;">
	<img src="/images/maui-main1.jpg" alt="Aloha! Welcome to Maui!">
	</div>
	<div style="position: relative; width: 300px auto;">
	<img src="/images/maui-main2.gif"><br>
	<p class="PublisherDescription">Adventure & excitement await you on the island of Maui. From a snorkel adventure in Molokini Crater to an exhilarating bike down the side of Haleakala, Maui offers a wide variety of activities, perfect for everyone! Space is limited for each activity, so please be sure to register early.<br>
<br>
Enjoy the warmth of the islands' timeless Polynesian hospitality and you will discover that you have truly found paradise.
	</p>
	</div>
</div>
</div>

<div style="width: 220px; height: 400px; background-color: #fcaf3e; float: right; margin: 0px; border:0;">
<a href="abouthi.aspx"><img src="/images/main-firstvisit.jpg" alt="First visit to Hawaii? Click here!" border=0></a>
<a href="otherislands.aspx"><img src="/images/main-otherisles.jpg" alt="See other island activities" border=0></a>
<a href="contact.aspx"><img src="/images/main-question.jpg" alt="Got a question? Let us help!" border=0></a>
</div>
	

</asp:content>