<%@ Control Language="C#" AutoEventWireup="true" enableviewstate="false"
 CodeFile="SpaPublisher.ascx.cs" Inherits="SpaPublisher" %>
<%@ register tagPrefix="abs" tagName="bookbtn" src="~/abs/ReserveItBtn.ascx" %>

<asp:repeater runat="server" id="ActGroupRepeater">
	
	<headertemplate>
		<table width="100%" cellspacing="1" cellpadding="5" class="PublisherTable" >
	</headertemplate>
	
	<itemtemplate>
		<tr>
			<td class="PublisherCatRow" colspan="2">
				<asp:label runat="server" id="ActGroupLbl" text='<%# DataBinder.Eval(Container.DataItem, "Title").ToString().ToUpper() %>' />
			</td>
		</tr>
		<tr>
			<td valign="Top" width="134">
				<asp:image runat="server" id="GroupImage" borderstyle="none" imageurl='<%# DataBinder.Eval(Container.DataItem, "ThumbUrl") %>' />
			</td>
			<td valign="Top">
				<asp:label runat="server" id="GroupTopDescr" text='<%# DataBinder.Eval(Container.DataItem, "TopDescription") %>' />
				<asp:repeater runat="server" id="ActivityRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "Activities") %>'>
					<itemtemplate>
						<asp:label runat="server" id="ActDescription" text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' cssclass="PublisherBriefDescr" />
						<br />
						<abs:bookbtn id="Bookbtn1" runat="server" ActivityID='<%# DataBinder.Eval(Container.DataItem, "ID") %>' />
					</itemtemplate>
				</asp:repeater>
			</td>
		</tr>
		<tr>
			<td valign="Top">
				<asp:label runat="server" id="Label1" text='<%# DataBinder.Eval(Container.DataItem, "BottomDescription") %>' />
			</td>
		</tr>
	</itemtemplate>
	
	<footertemplate>
		</table>
	</footertemplate>
	
</asp:repeater>
