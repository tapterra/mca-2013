<%@ Page Language="C#" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
	
	<abs:resbookingpanel runat="server" id="BookingPanel" 
		cssclass="BookingPanel"
		HeaderCellCssClass="BookingStepTitle"
		HeaderStepCssClass="BookingInstructions"
		LabelsCssClass="FormLabel"
		ValuesCssClass="FormInput"
		nextbuttoncssclass="StepButton"
	/>
	
</asp:Content>
