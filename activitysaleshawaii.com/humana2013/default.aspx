<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/oahurs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Welcome to the Humana Leaders Club 2013 activity website. Within this website you will discover additional exciting tours being offered on the island of Oahu that are not included in your travel package. <br /> <br />From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. <Br /><Br />
Reservations will close on <strong>Monday, March 15, 2013 at 5:00 p.m. Hawaii Standard Time</strong>. Space is limited so make your reservations early.<br />
<Br />
<br />
To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities.<br />
<Br />
3. Select your tours. You will be asked for credit card information. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab. <br />
<br />
6. If you would like to reserve an activity for a date not offered, or reserve an activity that is not offered, please call our toll-free reservation line at  877-589-5589 and a representative may assist you with your request.  <br />
<Br />
7. Should you need to alter your order, please e-mail us at <a href="mailto: humana2013@mcahawaii.com"><span font color="#0177CC"> humana2013@mcahawaii.com</span></a>.<br />
<Br /><br />
Let us know if we can be of service.  We look forward to welcoming you to Paradise.<br />

			<br /><br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
