<%@ Page Language="C#" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

	<div style="width:600px" >
		<font color="#0066cc" style="font-face: arial; font-size: 16pt;" face="Arial"><b>Unavailable</b></font><br /><br />
		<p>
			We're sorry, but the feature you are requesting is temporarily unavailable. 
		</p>
		<br />
		<p>
			Please try again later. If you continue to receive this error message, please call our service desk at 1 (877) 589-5589 or email:
<a href="mailto: soroptimist2012@mcahawaii.com"><font color="#0177CC"> soroptimist2012@mcahawaii.com</a>. 
		</p>
		<br />
	</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>
