<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/dh1.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, it has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago.<br />
<Br />
Oahu is the most popular of the Hawaiian Islands and we hope you are able to extend your stay so you can easily understand why!  With Waikiki as a central hub, you can explore the legendary North Shore of Oahu one day, and spend the next day on the east side snorkeling at Hanauma Bay, a protected marine sanctuary with tons of colorful fish.  It is clear that Oahu offers just the right amount of diversity for the adventurous as well as the cautious visitor.  Thrill seekers can skydive at Mokuleia while daydreamers can relax peacefully on the beach.  Exquisite dining and exciting nightlife also entice people to Oahu again and again.<br />
<Br />
			The average daytime temperature is 85&deg;F (29.4&deg;C). Temperatures at night are around 75&deg;F. For travel to Hawaii, business casual attire is appropriate for your meetings, and you may want a sweater as air conditioning in meeting rooms can be chilly. For your free time and most recreation and sightseeing activities, comfortable casual wear is recommended: shorts, sandals or other flat, comfortable shoes, bathing suits and cover-ups.<br />
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
