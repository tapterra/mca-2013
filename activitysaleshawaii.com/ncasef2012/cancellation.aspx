<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/dh1.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Cancellation Policy</p>
<p class="PublisherDescription">
<Br />
1) There will be no refunds for cancellations made less than 72 hours, Hawaii Standard Time, prior to activity.<Br /><Br />
2) Prior to 72 hours before the start of the tour, you must cancel in writing by emailing ncasef2012@mcahawaii.com in order to receive a refund.<Br /><Br />
3) If a tour is cancelled due to low registration or inclement weather, a full refund will be issued.
<Br />
<Br />
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
