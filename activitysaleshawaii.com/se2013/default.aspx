<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
		<div style="float: left; margin: 0 20px 50px 0;">
		<img src="http://activitysaleshawaii.com/images/mauirs.jpg">
		</div>
		<div style="width: 500px; float: left;"><p class="PublisherGroupTitle">Aloha State Electric!</p>
<p class="PublisherDescription">Thank you for visiting the State Electric activity website. Within this website, you will discover the exciting tours available on Maui. <br /><br />From snorkeling with the Pacific's exotic marine life to surfing Maui's waves, Hawaii offers a rich variety of activities perfect for everyone. <br /><br />Feel free to browse this website and discover all of these activities for yourself. <Br /><Br />
Reservations will close on Wednesday, April 3 at 5:00 pm, Hawaii Standard Time. Space is limited so make your reservations early!<br />
<Br />
<b><u>To make a Reservation</u></b>
<ol style="font-size:12px;">
<li>Click on the 'Tours and Activities' link above.</li>
<li>Browse the tours and activities offered.</li>
<li>Select your tours. You will be asked for credit card information. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.</li>
<li>Should you need to review an activity, select the My Itinerary tab.</li>
<li>If you need to review your order, select the My Account tab.</li>
</ol>
<p class="PublisherDescription">Should your plans bring you to Hawaii early or if you decide to stay later than the group, MC&A is happy to assist with hotels and activities on any of the Hawaiian Islands. Feel free to contact us at <a href="mailto:se2013@mcahawaii.com">se2013@mcahawaii.com</a>. Please be sure to include your choice of island and dates of travel. A representative will contact you regarding available options.</p>

<p class="PublisherDescription">Let us know if we can be of service.  We look forward to welcoming you to Paradise.</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
