<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">
		<div style="float: left; margin: 0 20px 50px 0;">
		<img src="/images/silversword.jpg">
		</div>
		<div style="width: 500px; float: left;">
		<p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, it has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago.<br />
<Br />
<b>About Maui</b><br>
Once you've settled in you'll want to explore Maui's sweeping canvas of attractions. The western, or leeward side, is the drier side of the island and features Maui's world-famous beaches including the beautiful Kaanapali Beach, home to a nightly sunset cliff diving ceremony. West Maui is also home to historic Lahaina, where you can find great shopping, dining and entertainment.
<br><Br>
The eastern, or windward side, of the island is the wetter side of the island, home to the lush Iao Valley and the scenic road to Hana. The cool, elevated slopes of Haleakala are where you can find the farms and gardens of Upcountry Maui and the soaring summit of Haleakala National Park. There is so much to see and do on Maui it's best to plan ahead. Just don't forget to send your friends a postcard.<br />
<Br />
			The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.
For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits and cover-ups is the usual daytime attire. This attire should also be appropriate for recreation and sightseeing activities.<br />
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
