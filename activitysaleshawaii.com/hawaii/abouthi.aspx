<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<div style="width: 600px; height: auto; position: absolute;">
<div style="margin-right:20px; float: left;">
<img src="/images/hawaii-about2.jpg" alt="Hiking in Hawaii"><br>
<img src="/images/hawaii-about.jpg" alt="Whalewatching">
</div>
<p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">Although made up of several dozen islands, the Hawaiian Islands are defined by eight major ones - Oahu, Kauai, Maui, Hawaii, Molokai, Lanai, Niihau and Kahoolawe. While each island rests in relatively close proximity of one another, each has its own qualities and characteristics.<br />
</p>
<p class="PublisherDescription">The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.</p>

<p class="PublisherDescription">For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits & cover-ups is the usual daytime attire.</p>
</p>
<p class="PublisherGroupTitle">About the Big Island</p>
<p class="PublisherDescription">The Island of Hawaii is the youngest island in the Hawaiian chain and is also by far the biggest, providing a vast canvas of environments to discover a variety of unrivaled natural wonders. This is the home of one of the world's most active volcanoes (Kilauea), the tallest sea mountain in the world at more than 33,000 feet (Maunakea), the most massive mountain in the world (Maunaloa) and the largest park in the state (Hawaii Volcanoes National Park).
</div>
<div style="width: 250px; height: 500px; background-color: #fcaf3e; float: right;">
<img src="/images/head-funfacts.jpg" alt="Popular Activities" width="250" />
<p id="popact">
Today, Hawaii's Big Island remains a vital touchstone for Hawaiian culture. Throughout its modernization, one ancient Hawaiian god is believed to be alive and well here. Pele, the volcano goddess, has settled in Kilauea Volcano at Hawaii Volcanoes National Park after moving south along the island chain. She continues to display her power today, keeping Kilauea in a constant state of eruption since 1983.
<Br>
<Br>
Hawaii's Big Island was also the home to King Kamehameha's court until it moved to Oahu in 1804.
<br><br>
</div>

</asp:content>
