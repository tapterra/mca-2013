using ABS;

public partial class SpaPublisher : System.Web.UI.UserControl {

	#region Properties

	public string IslandKey {
		get {
			object o = Session["CurrentIslandKey"];
			return ( o == null ? "oahu" : (string) o );
		}
		set {
			Session["CurrentIslandKey"] = value;
			// RKP: Begin Change - 20090205
			// RKP: Being anal, but we should dispose of any current Island
			this.island = null;
			// RKP: End Change
			this.Rebind();
		}
	}

	public Island Island {
		get {
			if ( this.island == null ) {
				this.island = new Island( this.IslandKey );
			}
			return ( this.island );
		}
	}
	private Island island;

	public string CategoryKey {
		get {
			object o = Session["CurrentCategoryKey"];
			// RKP: Begin Change - 20090205
			// RKP: Changed the default key to "spa"
			return ( o == null ? "spa" : (string) o );
			// RKP: End Change
		}
		set {
			Session["CurrentCategoryKey"] = value;
			this.Rebind();
		}
	}

	public Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryKey );
			}
			return ( this.category );
		}
	}
	private Category category;

	#endregion

	#region Methods

	public void Rebind() {
		this.ActGroupRepeater.DataSource = ActivityGroup.GetActivityGroups( this.Category, this.Island.ID );
		this.ActGroupRepeater.DataBind();
	}

	// RKP: Begin Change - 20090205
	// RKP: This call to Rebind is redundant and causes the catalog to be loaded from the db twice
	//protected void Page_Load( object sender, EventArgs e ) {
	//  if ( !this.IsPostBack ) {
	//    this.Rebind();
	//  }
	//}

	// RKP: This method is not used, does nothing
	//protected void MoreInfoCommand( object sender, CommandEventArgs e ) {
	//  int groupID = Convert.ToInt32( e.CommandArgument );
	//}
	// RKP: End Change

	#endregion

}