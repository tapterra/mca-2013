<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/oahurs.jpg" align=right style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Welcome to the Sweet Adelines International 2013 Convention activity website. Within this website you will discover additional exciting tours being offered on the island of Oahu that are not included in your travel package. <br /> <br />From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. <Br /><Br />
Reservations are open now and will close on <strong>Friday, October 18, 2013 at 5:00 p.m. Hawaii Standard Time</strong>. Space is limited so make your reservations early.<br />
<Br />
<br />
To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities.<br />
<Br />
3. Select your tours. You will be asked for credit card information. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab. <br />
<br />
6. Should you need to alter your order, please e-mail us at <a href="mailto: sweetadelines2013@mcahawaii.com"><span font color="#0177CC"> sweetadelines2013@mcahawaii.com</span></a>.<br />
<Br /><br />
Let us know if we can be of service.  We look forward to welcoming you to Paradise.<br />

			<br /><br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
