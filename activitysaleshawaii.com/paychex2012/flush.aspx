<%@ Page Language="C#" %>

<script runat="server" >
	
	protected void Page_Load( object sender, EventArgs e ) {
		ABS.VoucherServiceFactory.FlushCache();
		this.Session.Abandon();
		Response.Redirect( ABS.ConfigMgr.SitePageUrl( this, "default.aspx" ) );
	}
	
</script>
