<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/hulasunset.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha Paychex!</p>
<p class="PublisherDescription">Welcome to the Big Island of Hawaii. Within this website, you will be selecting your Paychex sponsored activity.<br />
<Br />
Paychex will be sponsoring one group activity that will be offered primarily on Tuesday, October 2 for Conference qualifiers only. Space is limited for each activity, so be sure to register early. The deadline for reserving all tours is August 30, 2012 at 5:00 p.m. Hawaii Standard Time.<br />
<Br />
			Enjoy the warmth of the islands' timeless Polynesian hospitality and you'll discover that you have truly found paradise.<br />
<Br />
Browse the 'Tours and Activities' tab above to view your choices.<br />
<Br />
To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities.<br />
<Br />
3. Select your tours. Upon confirmation of an activity or tour, print the tour voucher as confirmation for your selection.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab.<br />
<Br />
6. After you place your order, you will be notified via the contact telephone number or email address that you provided. If you are cancelling or changing an activity prior to arriving onsite, please email paychex2012@mcahawaii.com. All cancellations must be in writing.<br /><Br />

Let us know if we can be of service.  We look forward to welcoming you to Paradise.<br />
			<br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
