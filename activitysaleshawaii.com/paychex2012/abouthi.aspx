<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/kilauea.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, it has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago.<br />
<Br />
Explore Hawaii's Big Island, and sense the power of volcano goddess Pele as she creates new land over old.  See new vegetation growing in seemingly impossible pockets -- micro views of natural cycles that civilization obscures elsewhere.  Molten lava flows glowing in the dark night are a spectacular sight -- destruction and creation on a grand scale. 
Molten lava is the Big Island's hottest attraction, but you can also explore the cool depths of a sun-struck ocean; see local sights on foot or horseback; uncover art treasures in tiny plantation towns, play top-flight golf on internationally-renown courses; walk the paths ancient Hawaiians traveled; have a cup of Kona coffee at the source, or visit cattle country, home of the paniolo Hawaiian cowboys.  Wherever you go on Hawaii's Big Island, chances are it'll be a new experience, even if you've been there before! E komo mai, explore to your heart's content. Everyone is welcome.<br />
<Br />
			The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.  For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits and cover-ups is the usual daytime attire. This attire should also be appropriate for recreation and sightseeing activities.<br />
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
