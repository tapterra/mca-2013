<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/hulasunset.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha Paychex!</p>
<p class="PublisherDescription">Welcome to the Big Island of Hawaii. Within this website, you will discover helpful information and the exciting sponsored tours available on the Big Island of Hawaii. From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone.<br />
<Br />
Paychex will be sponsoring one activity for program qualifiers only. No guests allowed. Space is limited for each activity, so be sure to register early. The deadline for reserving all tours is September 16, 2012 at 5:00 p.m. Hawaii Standard Time.<br />
<Br />
			Enjoy the warmth of the islands' timeless Polynesian hospitality and you'll discover that you have truly found paradise.<br />
<Br />
To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities.<br />
<Br />
3. Select your tours. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab. The activities do not have a cash value and if you choose to cancel a reservation, you will not receive cash back.<br />
<Br /><br /><Br />

Let us know if we can be of service.  We look forward to welcoming you to Paradise.<br />
			<br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
