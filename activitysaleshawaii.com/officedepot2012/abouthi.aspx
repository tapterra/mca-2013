<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/iao1.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, Hawaii has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago. Although made up of several dozen islands, the Hawaiian Islands are defined by the eight major islands of Oahu, Kauai, Maui, Hawaii, Molokai, Lanai, Niihau and Kahoolawe. While each island rests in relative close proximity of one another, they have their own qualities and characteristics, making each one unique and special.<br><br>The average daytime temperature in Hawaii is 81 degrees F (29.4 C) and the average low is 70 degrees F. For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear such as shorts, sandals, walking shoes, flip-flops, bathing suits and cover-ups is the usual daytime attire and appropriate for recreation and sightseeing activities. Wherever you go on Maui, chances are it will be a new experience, even if you've been there before! <br><br>E komo mai, explore to your heart's content. Everyone is welcome.<br>
<Br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
