<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/mauirs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">
Welcome to the Champion's Circle activity website where you will discover the exciting tours available on Maui during your stay. <br><br>
From snorkeling with the Pacific Ocean's exotic marine life to dining over picture-perfect sunsets, Maui offers an abundant variety of activities to suit everyone's taste and pace. You can browse the website with the links above and discover all of these activities for yourself. <br><br>
We look forward to welcoming you to Paradise in March.<br><br>
Please note: The website will be undergoing maintenance on Saturday, February 18 from 9:00 p.m. - 12:00 midnite (Hawaii Standard Time). Registration for activities will be temporarily unavailable during this time. We apologize for any inconvenience.<br><br>
<p class="PublisherGroupTitle"><b>Mahalo!</b>
<br><br>
<p class="PublisherDescription">Space is limited so make your reservations early. On-line Reservations will close on February 20, 2012 at 5:00 p.m. Hawaii Standard Time (9:00 p.m. EST). 
<Br />
<br>
How to make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities offered.<br />
<Br />
3. Once you have found a tour or activity you would like to purchase, click 'Reserve', and select the date that you would like to participate.  Enter your information in the steps that follow. You can check out to purchase that activity or continue shopping for as many activities as you would like.  Each activity will be added to 'My Itinerary' until you are ready to check out.<br><br>
4. When you are ready to check out go to 'My Itinerary' and click on 'Check Out'.  You will be asked for your email address, credit card information and be assigned an account number. <br><br>
5. Upon confirmation of an activity or tour, please print the tour voucher and bring it with you to Hawaii. YOU MUST PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.<br><br>
6. Should you need to review a confirmed activity, select the 'My Itinerary' tab. <br><br>
7. If you need to review your order, select the My Account tab and enter your email address and account number. <br><br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

</asp:content>
