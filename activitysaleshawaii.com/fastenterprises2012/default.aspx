<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/oahurs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Welcome to the Fast Enterprises LLC Hawaii activity website. Within this website you will discover the many exciting tours being offered on the island of Oahu. <br /><br />Feel free to browse this website and discover all of these activities you can book for your day of leisure. <Br /><Br />
Reservations are now closed. If you wish to purchase an activity after the online reservation date, you may purchase them at the Fast Enterprises LLC Hospitality Desk onsite. Activity availability will be based on a space available basis. <br />
<Br />
If you have questions concerning an existing activity reservation, please contact us at:

<Br />
MC&A Service Desk<Br />
Monday - Friday<Br />
8:00 a.m. - 4:30 p.m. Hawaii Standard Time<Br />
Phone: 877-589-5589<Br />
<Br />

We look forward to welcoming you to Paradise.<br />

			<br /><br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
