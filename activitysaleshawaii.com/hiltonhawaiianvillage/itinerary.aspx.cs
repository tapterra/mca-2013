using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ABS;

public partial class Itinerary : Page {

	private void Page_Load( object sender, System.EventArgs e ) {
		ABS.Itinerary itin = ABS.Customer.Current.CurrentItinerary;
		// Clean out any incomplete bookings
		int bCount = itin.Bookings.Count;
		int i = 0;
		for ( int b = 0 ; b < bCount ; b++ ) {
			ABS.Booking booking = itin.Bookings[i];
			if ( !booking.IsBookingCompleted ) {
				itin.RemoveBooking( booking );
			} else {
				i++;
			}
		}
		// Hide the itinerary panel if we don't have any bookings
		if ( itin.ResBookingsCount == 0 ) {
			this.ItineraryPanel.Visible = false;
			this.EmptyCartMsgPanel.Visible = true;
		} else {
			this.ItineraryPanel.Visible = true;
			this.EmptyCartMsgPanel.Visible = false;
			// Once the user reaches this point, we can tag them with a cookie for this itinerary
			// so that they can leave the site and when they return, the open itinerary will be restored.
			itin.Customer.SetupItineraryCookie();
		}
	}
	
}
