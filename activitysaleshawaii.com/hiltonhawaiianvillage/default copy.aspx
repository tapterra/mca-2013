<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/hulasunset.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Within this website, you will discover helpful information and a range of the many exciting tours available on the Big Island of Hawaii. From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. Space is limited for each activity, so be sure to register early. <br />
<Br />
If there is an activity that is not offered on the website, we will be happy to assist you onsite at the Ambassador Travel Information/Tour Desk at the Hilton Waikoloa Village.<br />
<Br />
The deadlines for reserving all tours are as follows: <br />Session 1: March 15, 2009.
<br />Session 2: March 22, 2009.
<br />Session 3: March 29, 2009.
<br />Session 4: April 5, 2009.
<br />Session 5: April 12, 2009.<br />
<Br />
			Enjoy the warmth of the islands' timeless Polynesian hospitality and you'll discover that you have truly found paradise.<br />
			<br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
