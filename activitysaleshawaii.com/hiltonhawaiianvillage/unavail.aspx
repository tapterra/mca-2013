<%@ Page Language="C#" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

	<div style="width:600px" >
		<font color="#0066cc" style="font-face: arial; font-size: 16pt;" face="Arial"><b>Unavailable</b></font><br /><br />
		<p>
			We're sorry, but the feature you are requesting is temporarily unavailable. 
		</p>
		<p>
			Please try again later. If you continue to receive this error message, please call the Diamond Head Vacations service desk Monday - Friday, between 8:00 a.m. and 4:30 p.m. Hawaii Standard Time at 1 (877) 589-5568 or email:
<a href="mailto:hhv@mcahawaii.com"><font color="#0177CC">hhv@mcahawaii.com</a>. 
		</p>
		<br />
	</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FooterContent" Runat="Server">
</asp:Content>
