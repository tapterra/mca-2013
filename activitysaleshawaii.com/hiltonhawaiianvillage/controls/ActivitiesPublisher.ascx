<%@ control language="c#" autoeventwireup="true" enableviewstate="false"
 codefile="ActivitiesPublisher.ascx.cs" inherits="ActivitiesPublisher" classname="ActivitiesPublisher" 
%>
<%@ register tagPrefix="abs" tagName="bookbtn" src="~/abs/ReserveItBtn.ascx" %>

<asp:repeater runat="server" id="SubCatRepeater" visible="true">
	
	<headertemplate>
		<table width="100%" cellspacing="1" cellpadding="5" class="PublisherTable" >
	</headertemplate>

	<itemtemplate>

		<tr><td colspan="2" class="PublisherCatRow">
			<asp:label runat="server" id="SubCategoryLbl" text='<%# DataBinder.Eval(Container.DataItem, "Label").ToString().ToUpper() %>' />
		</td></tr>
			
		<asp:repeater runat="server" id="ActGroupRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "ActivityGroups") %>' >
			
			<itemtemplate>
				<tr>
					<td valign="Top" width="134" style="border-top:1px solid #999999">
						<asp:image runat="server" id="GroupImage" borderstyle="none" 
							imageurl='<%# DataBinder.Eval(Container.DataItem, "ThumbnailUrl") %>'
						/>
					</td>
					<td valign="top" style="border-top:1px solid #999999">
						<asp:label runat="server" id="GroupTitle" cssclass="PublisherGroupTitle"
						 text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' 
						/>
						<br />
						<asp:label runat="server" id="GroupDescription"
						 text='<%# DataBinder.Eval(Container.DataItem, "BriefDescription") %>' 
						 cssclass="PublisherBriefDescr" 
						/>
						<br />
<%-- RKP: Begin Change - 20090206 - replace the linkbutton with a hyperlink+querystring
						<asp:linkbutton runat="server" id="MoreLink" commandname="MoreLink"
						 commandargument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
						 text="&gt;&gt;More Info"
						 cssclass="PublisherMoreLink"
						 oncommand="MoreInfoCommand"
						 visible='<%# this.CategoryKey == "activities" %>'
						/>
--%>
						<asp:hyperlink runat="server" id="MoreLink" cssclass="PublisherMoreLink"
						 visible='<%# this.CategoryKey == "activities" %>'
						 navigateurl='<%# string.Format( "{0}?id={1}", this.Page.ResolveUrl("catalog.aspx"), 
						 DataBinder.Eval(Container.DataItem, "ID") ) %>'
						>&gt;&gt;More Info</asp:hyperlink>
<%-- RKP: End Change --%>
						
						<asp:repeater runat="server" id="ActivityRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "Activities") %>' 
						 visible='<%# this.CategoryKey != "activities" %>'
						>
							<headertemplate>
								<table cellpadding="0" cellspacing="0" border="0" style="margin:0px; margin-top:3px">
							</headertemplate>
							<itemtemplate>
									<tr>
										<td valign="middle">
											<abs:bookbtn id="Bookbtn2" runat="server" activityid='<%# DataBinder.Eval(Container.DataItem, "ID") %>' />
										</td>
										<td valign="top">
											<asp:label runat="server" id="ActivityDescription"
											 text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' 
											 cssclass="PublisherBriefDescr" 
											/>
										</td>
									</tr>
							</itemtemplate>
							<footertemplate>
								</table>
							</footertemplate>
						</asp:repeater>
					</td>
				</tr>
			</itemtemplate>
			
		</asp:repeater>

	</itemtemplate>
	
	<footertemplate>
		</table>
	</footertemplate>
	
</asp:repeater>

<asp:panel runat="server" id="GroupDetailsPanel" visible="false" width="600px">
	<table width="100%" cellspacing="1" cellpadding="5" style="margin-top:20px" >
		<tr>
			<td colspan="2">
				<asp:label runat="server" id="GroupTitle" cssclass="PublisherGroupTitle"
				 text='<%# this.SelectedGroup.Title %>' 
				/>
			</td>
		</tr>	
		<tr>
			<td valign="Top" width="180" >
				<asp:repeater runat="server" id="PhotosRepeater" datasource='<%# this.SelectedGroup.Photos %>' >
					<itemtemplate>
						<asp:image runat="server" id="GroupImage" borderstyle="none" 
							imageurl='<%# DataBinder.Eval(Container.DataItem, "ThumbUrl") %>'
						/>
					</itemtemplate>
				</asp:repeater>
			</td>
			<td valign="Top">
				<asp:label runat="server" id="GroupDescription"
				 text='<%# this.SelectedGroup.TopDescription %>' 
				 cssclass="PublisherDescription" 
				/>
				<asp:repeater runat="server" id="ActsRepeater" datasource='<%# this.SelectedGroup.Activities %>' >
					<headertemplate>
						<table cellspacing="0" cellpadding="0">
					</headertemplate>

					<itemtemplate>
						<tr>
							<td>
								<asp:label runat="server" id="ActDescr" cssclass="PublisherDescription"
								 text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' 
								/>
							</td>
							<td>
								<abs:bookbtn id="Bookbtn1" runat="server" activityaid='<%# DataBinder.Eval(Container.DataItem, "AID") %>' />
							</td>
						</tr>
					</itemtemplate>
					<footertemplate>
						</table>
					</footertemplate>
				</asp:repeater>
			</td>
		</tr>	
		<tr>
			<td colspan="2">
				<asp:label runat="server" id="Label1" cssclass="PublisherDescription"
				 text='<%# this.SelectedGroup.BottomDescription %>' 
				/>
			</td>
		</tr>	
	</table>
</asp:panel>
