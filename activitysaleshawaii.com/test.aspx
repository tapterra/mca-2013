<%@ Page Language="C#" %>
<%@ import namespace="System.Net.Mail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
//10.181.129.249
	protected void OnTestClicked( object sender, System.EventArgs e ) {
		SendEmail();
	}

	public void SendEmail() {
		MailMessage mailer = new MailMessage();
		mailer.From = new MailAddress( "admin@mcahawaii.com" );
		mailer.To.Add( "mike.fukumoto@mcahawaii.com" );
		mailer.To.Add( "bob@tapterra.com" );
		mailer.Body = "Testing - if you receive this message, the problem is solved. Please let Bob know.";
		mailer.IsBodyHtml = false;
		mailer.Subject = "Email Testing";
		SmtpClient client = new SmtpClient();
		client.Send(mailer);
	}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

		<asp:button id="test" runat="server" text="Test - No Inline Creds" onclick="OnTestClicked" />

    </div>
    </form>
</body>
</html>
