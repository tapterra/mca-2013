<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/hulasunset.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Welcome to the Islands of Aloha!</p>
<p class="PublisherDescription">Within this website, you will discover helpful information and a range of the many exciting tours available in the Hawaiian Islands. From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. <br />
<Br />We also offer lei greeting and airport transfer services for your convenience. <br />
<Br />
If there is an activity you would like to arrange that is not offered on the website, click on the 'Contact Us' link above and we will be happy to assist you.<br />
			<br />
		</p>
<p class="PublisherGroupTitle"><font color="#0177CC">Mahalo!<br />

		</div>
	

</asp:content>
