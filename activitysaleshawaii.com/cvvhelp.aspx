<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
		<title>What is CVV2?</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="main.css" type="text/css">
	</head>

	<body onload='window.focus();' bgcolor="#ffffff">
		<div style="width:600px">
		<h1 style="text-align:center">What is CVV2?</h1>
		<p>
			CVV2 is an important security feature for credit card transactions on the Internet and over the phone. 
			"CVV" stands for "Card Verification Value".
		</p>
		<table cellpadding="4">
			<tr>
				<td>
					<ul>
						<li>It is the three-digit number printed in the signature space on the <em>back</em> of most credit cards, 
						such as <b>Visa</b> and <b>Mastercard</b>. 
						The CVV2 number is always the last group of numbers in the signature space on the back of the card. 
						It is not part of your regular credit card number.</li>
					</ul>
				</td>
				<td><img runat="server" src="~/images/CVV2-visa.gif" /></td>
			</tr>
			<tr>
				<td>
					<ul>
						<li>It is a four-digit number on the front of <b>American Express</b> cards. It is printed (flat), not embossed like the card number.</li>
					</ul>
				</td>
				<td><img runat="server" src="~/images/CVV2-amex.gif" /></td>
			</tr>
		</table>
		<p>The CVV2 number enhances fraud protection and helps to validate two things:</p>
		<ul>
			<li>The customer has the credit card in their possession.</li>
			<li>The credit card number is legitimate</li>
		</ul>
		<p>By helping to prevent credit card fraud, the CVV2 number keeps costs down for everyone.</p>
		</div>
	</body>
</html>

