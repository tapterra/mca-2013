<%@ control language="c#" autoeventwireup="true" enableviewstate="false"
 codefile="MAndGPublisher.ascx.cs" inherits="MAndGPublisher" classname="MAndGPublisher" 
%>
<%@ register tagPrefix="abs" tagName="bookbtn" src="~/abs/ReserveItBtn.ascx" %>

<asp:repeater runat="server" id="ActGroupRepeater" >
	
	<headertemplate>
		<table width="100%" cellspacing="1" cellpadding="5" class="PublisherTable" >
	</headertemplate>

	<itemtemplate>

		<tr><td class="PublisherCatRow">
			<asp:label runat="server" id="ActGroupLbl" text='<%# DataBinder.Eval(Container.DataItem, "Title").ToString().ToUpper() %>' />
		</td></tr>
		
		<tr><td valign="Top">
			<asp:label runat="server" id="GroupTopDescr" text='<%# DataBinder.Eval(Container.DataItem, "TopDescription") %>' />
		</td></tr>
		
		<asp:repeater runat="server" id="ActivityRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "Activities") %>' >
			<itemtemplate>
				<tr>
					<td valign="Top">
						<asp:label runat="server" id="ActDescription"
						 text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' 
						 cssclass="PublisherBriefDescr" 
						/>
						<abs:bookbtn id="Bookbtn1" runat="server" activityaid='<%# DataBinder.Eval(Container.DataItem, "AID") %>'
						/>
					</td>
				</tr>
			</itemtemplate>
		</asp:repeater>

		<tr><td valign="Top">
			<asp:label runat="server" id="Label1" text='<%# DataBinder.Eval(Container.DataItem, "BottomDescription") %>' />
		</td></tr>
		
	</itemtemplate>
	
	<footertemplate>
		</table>
	</footertemplate>
	
</asp:repeater>
