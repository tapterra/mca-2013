<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

		<div style="width: 800px; margin-left:50px;">
<p class="PublisherGroupTitle">Click on each island to view that island's tours & activities:</p>
<div style="float: left; border-bottom: 2px dotted #999;">
<a href="/oahu"><img src="/images/island-oahu.jpg" alt="OAHU" align=left border=0 style="margin-right:20px"/></a>
<p class="PublisherGroupTitle">The Heart of Hawaii</p>
<p class="PublisherDescription">
From fantastic shopping to historic city tours, the metropolitan capital of Hawaii has something for everyone. Spotlight tours: the Polynesian Cultural Center, the Arizona Memorial, world-famous North Shore and many more!</p>
</div>

<div style="float: left; border-bottom: 2px dotted #999;">
<a href="/maui"><img src="/images/island-maui.jpg" alt="MAUI" align=left border=0 style="margin-right:20px"/></a>
<p class="PublisherGroupTitle">The Magic Isle</p>
<p class="PublisherDescription">
From the amazing landscape of Haleakala to the historic oceanside town of Lahaina, Maui is brimming with beauty and magic. It's also a ferry ride away from Lanai, one of the most romantic getaway islands. Spotlight tours: Haleakala Downhill Biking, Hana and more!</p>
</div>

<div style="float: left; border-bottom: 2px dotted #999;">
<a href="/kauai"><img src="/images/island-kauai.jpg" alt="KAUAI" align=left border=0 style="margin-right:20px"/></a>
<p class="PublisherGroupTitle">The Island of Discovery</p>
<p class="PublisherDescription">
From lush rainforests to towering waterfalls, Kauai has so much to discover and explore! Spotlight tours: Na Pali Coast Sailing, Zipline tours, Nature hikes and more!</p>
</div>

<div style="float: left;">
<a href="/hawaii"><img src="/images/island-bi.jpg" alt="BIG ISLAND" align=left border=0 style="margin-right:20px"/></a>
<p class="PublisherGroupTitle">The Island of Adventure</p>
<p class="PublisherDescription">
The largest of the Hawaiian islands, the Big Island has a diverse array of things to do! Spotlight tours: Volcano helicopter tours, zipline adventures, horseback riding and more!</p>
</div>
<br>
<Br>
<Br>
		</div>

</asp:content>