<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<img src="/images/maui-cars.jpg" alt="Rental Cars" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Transportation</p>
<p class="PublisherDescription">Enjoy the freedom of the open road and the beauty of the islands at your own leisure. There is lots to see and do on the island of Hawaii!<br><br>Rental Cars - If you are planning to rent a car, it is a must that you make your reservations as soon as possible prior to arriving to the islands as availability may be limited. Note - rental must be picked up and dropped off at the same location to avoid a drop charge. Special rates have been provided for the R3 group. <Br><a href="/rentalcar-r3.pdf">Click here for car rental specials.</a><br><br>Activity Transportation - Some tours do not provide transportation. It is recommended that you rent a car to get yourselves to and from activities. Taxis are available; however, the cost would be much more than renting a car in most cases. If you are not renting a car from the airport, you may book one through the link above which will allow you to rent a car for one or multiple days from a location near your hotel. Again, cars are based on availability so please make your reservations as soon as possible. Drive times are provided in activity descriptions. 
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
