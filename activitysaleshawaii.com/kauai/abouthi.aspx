<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">

<div style="width: 600px; height: auto; position: absolute;">
<div style="margin-right:20px; float: left;">
<img src="/images/kauai-about2.jpg" alt="Na Pali Coast, Kauai"><br>
<img src="/images/kauai-about.jpg" alt="Kauai Zipline">
</div>
<p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">Although made up of several dozen islands, the Hawaiian Islands are defined by eight major ones - Oahu, Kauai, Maui, Hawaii, Molokai, Lanai, Niihau and Kahoolawe. While each island rests in relatively close proximity of one another, each has its own qualities and characteristics.<br />
</p>
<p class="PublisherDescription">The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.</p>

<p class="PublisherDescription">For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits & cover-ups is the usual daytime attire.</p>
</p>
<p class="PublisherGroupTitle">About Kauai</p>
<p class="PublisherDescription">Kauai is carpeted in lush greenery, enshrouded with flowers of all colors, fresh island fruit, and oftentimes a gentle, cool mist. Each Island has its own distinctive charm, and Kauai is the tropical centerpiece of Hawaii.<br>
</div>
<div style="width: 250px; height: 500px; background-color: #fcaf3e; float: right;">
<img src="/images/head-funfacts.jpg" alt="Popular Activities" width="250" />
<p id="popact">
The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.<Br><br>
Over 60 feature films have been shot here. Majestic Manawaiopuna Falls was shot in Jurassic Park. The tranquil Wailua River was used in Blue Hawaii. The Anahola Mountains were the backdrop for Raiders of the Lost Ark.<br><br>
</div>

</asp:content>
