using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ABS;

public partial class Completed : Page {

	protected void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			ABS.Itinerary itin = Customer.Current.CurrentItinerary;
			EmailLoginLbl.Text = itin.Customer.EmailAddress;
			AccountNoLbl.Text = itin.LoginAccountNumber;
			if ( itin.BookingsCount > 0 ) {
				// We have ATCO bookings...
				this.VoucherConfirmationMsg.Visible = true;
				// Load the ATCO vouchers
				itin.VoucherHtml = ABS.VoucherServiceFactory.BuildVouchers( true );
				this.VoucherPanel.Text = itin.VoucherHtml;
				if ( this.VoucherPanel.Text == "" ) {
					Response.Redirect( ConfigMgr.SitePageUrl( this, "itinerary.aspx" ) );
				}
			}
			if ( itin.ReservationsCount > 0 ) {
				// We have non-ATCO reservations...
				this.ResConfirmationMsg.Visible = true;
			}
		}
	}

	protected override void OnUnload( EventArgs e ) {
		if ( !this.IsPostBack ) {
			// We're done with this session - drop it
			this.Session.Abandon();
		}
		base.OnUnload( e );
	}

}
