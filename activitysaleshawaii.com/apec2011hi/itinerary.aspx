<%@ Page Language="C#" AutoEventWireup="true" CodeFile="itinerary.aspx.cs" Inherits="Itinerary" %>
<%@ import namespace="ABS" %>

<asp:content runat="server" contentplaceholderid="MainContent" >

	<asp:panel runat="server" id="EmptyCartMsgPanel">
		<br />
		<p style="color: black; font-family: arial; font-size: 9pt; font-weight: bold">
			Your itinerary is currently empty.
		</p>
		<br />
		<p style="color: black; font-family: arial; font-size: 9pt; font-weight: bold">
			Please select an option from the panel on the left to see the available activities.
		</p>
	</asp:panel>
	
	<abs:itinerary runat="server" id="ItineraryPanel" 
		cssclass="ItinBookPanel"
		headercellcssclass="ItinHeader"
		footercellcssclass="ItinFooter"
		labelcssclass="ItinPanelLabel" 
		valuecssclass="ItinPanelValue"
		buttoncssclass="StepButton"
		
		BookPanelCssClass="ItinBookPanel"
		BookPanelCellSpacing="0"
		BookPanelCellPadding="0"
		BookPanelHeaderTableCssClass="ItinBookHeaderTable"
		BookPanelFooterTableCssClass="ItinBookFooterTable"
		BookPanelGuestPanelsCellCssClass=""
		BookPanelLabelCssClass="ItinPanelLabel"
		BookPanelValueCssClass="ItinPanelValue"
		BookPanelActivityNameCssClass="ItinPanelLabel"
		BookPanelButtonCssClass="ItemButton"

		GuestPanelCssClass="ItinGuestPanel"
		GuestPanelCellSpacing="0"
		GuestPanelCellPadding="0"
		GuestPanelLabelCssClass="ItinPanelLabel"
		GuestPanelGuestNameCssClass="ItinPanelValue"
		GuestPanelGuestTypeCssClass="ItinPanelGuestTypeValue"
		GuestPanelPriceCssClass="ItinPanelValue"
		GuestPanelOptionsCssClass="ItinPanelValue"
	/>

</asp:content>
