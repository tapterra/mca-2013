using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[ValidationProperty( "SelectedTime" )]
public partial class TimePicker : System.Web.UI.UserControl {

	public string Label {
		get { return ( this.LabelLbl.Text ); }
		set { this.LabelLbl.Text = value; }
	}

	public string LabelCssClass {
		get { return ( this.LabelLbl.CssClass ); }
		set { this.LabelLbl.CssClass = value; }
	}

	public string ValueCssClass {
		get { return ( this.TimeLBox.CssClass ); }
		set {
			this.TimeLBox.CssClass = value;
			this.AmpmLBox.CssClass = value;
		}
	}

	public string SelectedTime {
		get {
			if ( this.TimeLBox.SelectedIndex == 0 || this.AmpmLBox.SelectedIndex == 0 ) {
				return ( "" );
			}
			return ( string.Format( "{0} {1}", this.TimeLBox.SelectedValue, this.AmpmLBox.SelectedValue ) );
		}
		set {
			this.EnsureChildControls();
			if ( string.IsNullOrEmpty( value ) ) {
				this.TimeLBox.SelectedIndex = 0;
				this.AmpmLBox.SelectedIndex = 0;
			} else {
				string[] parts = value.Split( ' ' );
				this.TimeLBox.SelectedValue = parts[0];
				this.AmpmLBox.SelectedValue = parts[1];
			}
		}
	}

}
