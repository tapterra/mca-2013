<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckoutPanel.ascx.cs" Inherits="CheckoutPanel" %>
<%@ register tagprefix="abs" tagname="creditcard" src="checkoutcreditcardpanel.ascx" %>
<%@ register tagprefix="abs" tagname="address" src="checkoutbillingaddresspanel.ascx" %>
<%@ register tagprefix="abs" tagname="contact" src="checkoutcontactinfopanel.ascx" %>
<%@ import namespace="ABS" %>

<asp:table runat="server" id="MainTable" >
	
	<asp:tablerow runat="server" id="HeaderRow" >
		<asp:tablecell runat="server" id="HeaderCell">
			<asp:label runat="server" id="HeaderLbl" text="Check Out..." />
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="MessageRow" >
		<asp:tablecell runat="server" id="MessageCell" cssclass="ErrorMsgSubPanel" style="padding:16px">
			<asp:label runat="server" id="MessageLbl" text="Space For Rent" />
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="ContactInfoRow" >
		<asp:tablecell runat="server" id="ContactInfoCell">
			<abs:contact runat="server" id="ContactInfoPanel"
			 cssclass="CheckoutSubPanel"
			 headercssclass="CheckoutPanelHeader"
			 bodytablecssclass="CheckoutBodyTable"
			 bodytablespacing="6"
			 formlabelcssclass="FormLabel"
			 forminputcssclass="FormValue"
			/>
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="CreditCardRow" visible='<%# !Website.Current.IsNoPaymentSite %>' >
		<asp:tablecell runat="server" id="CreditCardCell">
			<abs:creditcard runat="server" id="CreditCardPanel" 
			 cssclass="CheckoutSubPanel"
			 headercssclass="CheckoutPanelHeader"
			 bodytablecssclass="CheckoutBodyTable"
			 bodytablespacing="6"
			 formlabelcssclass="FormLabel"
			 forminputcssclass="FormValue"
			/>
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="BillingAddressRow" >
		<asp:tablecell runat="server" id="BillingAddressCell">
			<abs:address runat="server" id="BillingAddressPanel"
			 cssclass="CheckoutSubPanel"
			 headercssclass="CheckoutPanelHeader"
			 bodytablecssclass="CheckoutBodyTable"
			 bodytablespacing="6"
			 formlabelcssclass="FormLabel"
			 forminputcssclass="FormValue"
			/>
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="FooterRow" >
		<asp:tablecell runat="server" id="FooterCell" >
			<table width="100%" class="ItinFooter"> 
				<tr>
					<td colspan="2" align="left">
						<asp:label runat="server" id="LabelClickMsg" text="Please do NOT click CHECK OUT more than once or click the browser BACK button."/>
			    </td>
				</tr>
				<tr>
					<td align="left">
						<asp:label runat="server" id="CheckoutTotalLbl" />
					</td>
					<td align="right">
						<asp:button runat="server" id="CheckoutBtn" text="Check Out" 
						 onclick="DoCheckout"
						 tabindex="1"
						/>
					</td>
				</tr>
			</table>
		</asp:tablecell>
	</asp:tablerow>

</asp:table>
