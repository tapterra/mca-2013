using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;
using System.Net.Mail;
using System.Text;

public partial class CheckoutPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public int CellSpacing {
		get { return ( this.MainTable.CellSpacing ); }
		set { this.MainTable.CellSpacing = value; }
	}

	public int CellPadding {
		get { return ( this.MainTable.CellPadding ); }
		set { this.MainTable.CellPadding = value; }
	}

	public string HeaderCellCssClass {
		get { return ( this.HeaderCell.CssClass ); }
		set { this.HeaderCell.CssClass = value; }
	}

	public string HeaderLabelCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}

	public string FooterCellCssClass {
		get { return ( this.FooterCell.CssClass ); }
		set { this.FooterCell.CssClass = value; }
	}

	public string LabelCssClass {
		get {
			object obj = ViewState["LabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["LabelCssClass"] = value; }
	}

	public string ValueCssClass {
		get {
			object obj = ViewState["ValueCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ValueCssClass"] = value; }
	}

	public string ButtonCssClass {
		get { return ( this.CheckoutBtn.CssClass ); }
		set { this.CheckoutBtn.CssClass = value; }
	}

	#endregion

	#region Data

	public string ErrorMessage {
		get { return ( this.MessageLbl.Text ); }
		set {
			this.MessageLbl.Text = value;
			this.MessageRow.Visible = !string.IsNullOrEmpty( value );
		}
	}

	#region CreditCardPanel

	public string CardholderName {
		get { return ( this.CreditCardPanel.CardholderName ); }
		set { this.CreditCardPanel.CardholderName = value; }
	}

	public string CardType {
		get { return ( this.CreditCardPanel.CardType ); }
		set { this.CreditCardPanel.CardType = value; }
	}

	public string CardNumber {
		get { return ( this.CreditCardPanel.CardNumber ); }
		set { this.CreditCardPanel.CardNumber = value; }
	}

	public string ExpMM {
		get { return ( this.CreditCardPanel.ExpMM ); }
		set { this.CreditCardPanel.ExpMM = value; }
	}

	public string ExpYYYY {
		get { return ( this.CreditCardPanel.ExpYYYY ); }
		set { this.CreditCardPanel.ExpYYYY = value; }
	}

	public string CvvNumber {
		get { return ( this.CreditCardPanel.CvvNumber ); }
		set { this.CreditCardPanel.CvvNumber = value; }
	}

	#endregion

	#region BillingAddressPanel

	public string StreetAddress1 {
		get { return ( this.BillingAddressPanel.StreetAddress1 ); }
		set { this.BillingAddressPanel.StreetAddress1 = value; }
	}

	public string StreetAddress2 {
		get { return ( this.BillingAddressPanel.StreetAddress2 ); }
		set { this.BillingAddressPanel.StreetAddress2 = value; }
	}

	public string City {
		get { return ( this.BillingAddressPanel.City ); }
		set { this.BillingAddressPanel.City = value; }
	}

	public string State {
		get { return ( this.BillingAddressPanel.State ); }
		set { this.BillingAddressPanel.State = value; }
	}

	public string Zip {
		get { return ( this.BillingAddressPanel.Zip ); }
		set { this.BillingAddressPanel.Zip = value; }
	}

	#endregion

	#region ContactInfoPanel

	public string CustomerFirstName {
		get { return ( this.ContactInfoPanel.CustomerFirstName ); }
		set { this.ContactInfoPanel.CustomerFirstName = value; }
	}

	public string CustomerLastName {
		get { return ( this.ContactInfoPanel.CustomerLastName ); }
		set { this.ContactInfoPanel.CustomerLastName = value; }
	}

	public string Telephone {
		get { return ( this.ContactInfoPanel.Telephone ); }
		set { this.ContactInfoPanel.Telephone = value; }
	}

	public string Email {
		get { return ( this.ContactInfoPanel.Email ); }
		set { this.ContactInfoPanel.Email = value; }
	}

	#endregion

	#endregion

	#endregion

	protected void ReadCustomerInfo() {
		Customer cust = Customer.Current;
		// Read the form fields for the card info
		cust.FirstName = this.CustomerFirstName;
		cust.LastName = this.CustomerLastName;
		cust.Telephone = this.Telephone;
		cust.EmailAddress = this.Email;
		cust.BillingName = this.CardholderName;
		cust.BillingCity = this.City;
		cust.BillingStreet1 = this.StreetAddress1;
		cust.BillingStreet2 = this.StreetAddress2;
		cust.BillingState = this.State;
		cust.BillingZip = this.Zip;
		cust.Persist();
	}

	protected void InitPmtProcessor() {
		this.ReadCustomerInfo();
		// Read the form fields for the card info
		Itinerary itin = Customer.Current.CurrentItinerary;
		IPaymentProcessor pp = itin.PmtProcessor;
		pp.CardHolderName = this.CardholderName;
		pp.BillingCity = this.City;
		pp.BillingStreet1 = this.StreetAddress1;
		pp.BillingStreet2 = this.StreetAddress2;
		pp.BillingState = this.State;
		pp.BillingZip = this.Zip;
		pp.CardCVVNumber = this.CvvNumber;
		pp.CardExpiresMM = Convert.ToInt32( this.ExpMM );
		pp.CardExpiresYY = Convert.ToInt32( this.ExpYYYY );
		pp.CardNumber = this.CardNumber;
		pp.CardType = this.CardType;
		pp.PaymentAmount = itin.TotalDiscountedPrice;
	}

	protected void DoCheckout( object sender, EventArgs e ) {
		bool needToCancel = false;
		Itinerary itin = Customer.Current.CurrentItinerary;
		// Reset any outstanding errors
		itin.ErrorMessage = "";
		// Read the form fields for the card info
		this.InitPmtProcessor();
		using ( IVoucherService voucherService = VoucherServiceFactory.NewVoucherService() ) {
			if ( voucherService.ReserveBookings() ) {	// The bookings all cleared inventory...
				// Process the credit-card payment...
				PaymentProcessorResponses pmtResp = itin.PmtProcessor.DoAuthorizeTransaction();
				if ( pmtResp == PaymentProcessorResponses.ApprovedResponse ) {
					// Pay for the vouchers with ATCO
					if ( voucherService.PayForItinerary() ) {
						// Nail down the credit-card payment
						pmtResp = itin.PmtProcessor.DoCaptureTransaction();
						if ( pmtResp == PaymentProcessorResponses.ApprovedResponse ) {
							// Capture the payment-record
							itin.CaptureNewPayment();
							// Send the voucher out by email
							voucherService.SendVoucherEmail();
							// Kill the itinerary cookie
							Customer.Current.ClearItineraryCookie( itin );
							// Display the thank-you page
							Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "completed.aspx" ) );
						} else {
							// Payment capture failed ...
							needToCancel = true;
							ExcLogger.SendExceptionReport( "CheckoutPanel.DoCheckout: CaptureFailed", itin.PmtProcessor.ResponseMessage );
							this.ErrorMessage = ConfigMgr.ErrPaymentMsg( itin.PmtProcessor.ResponseMessage );
							itin.ErrorMessage = "";
						}
					} else {
						// Voucher request failed...
						needToCancel = true;
						this.ErrorMessage = itin.ErrorMessage;
						itin.ErrorMessage = "";
					}
				} else {
					// Payment failed - inform the user & let them fix it at the Checkout page
					needToCancel = true;
					this.ErrorMessage = ConfigMgr.ErrPaymentMsg( itin.PmtProcessor.ResponseMessage );
					itin.ErrorMessage = "";
				}
			} else {
				voucherService.CancelBookings( itin, "DoCheckout failed" );
				// Let the user know which bookings failed, ask them to revise or remove them
				Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
			}
			if ( needToCancel ) {
				voucherService.CancelBookings( itin, "DoCheckout failed" );
			}
		}
	}

	protected void Page_Load( object sender, EventArgs e ) {
		Customer cust = Customer.Current;
		cust.Persist();
		Itinerary itin = cust.CurrentItinerary;
		this.CheckoutTotalLbl.Text = Website.Current.IsNoPaymentSite ? "" : string.Format( "Current Total: {0:C}", itin.TotalDiscountedPrice );
		this.CheckoutBtn.Enabled = Website.Current.IsNoPaymentSite || itin.TotalDiscountedPrice > 0M;
		this.CreditCardRow.Visible = !Website.Current.IsNoPaymentSite;
		if ( !this.IsPostBack ) {
			try {
				this.CustomerFirstName = itin.CustomerFirstName;
				this.CustomerLastName = itin.CustomerLastName;
				this.Telephone = itin.ContactPhone;
				this.Email = itin.ContactEmail;
				this.City = itin.Customer.BillingCity;
				this.StreetAddress1 = itin.Customer.BillingStreet1;
				this.StreetAddress2 = itin.Customer.BillingStreet2;
				this.State = itin.Customer.BillingState;
				this.Zip = itin.Customer.BillingZip;
				this.CvvNumber = itin.PmtProcessor.CardCVVNumber;
				this.ExpMM = itin.PmtProcessor.CardExpiresMM.ToString( "00" );
				this.ExpYYYY = itin.PmtProcessor.CardExpiresYY.ToString( "0000" );
				this.CardholderName = itin.PmtProcessor.CardHolderName;
				if ( this.CardholderName == "" ) {
					this.CardholderName = string.Format( "{0} {1}", this.CustomerFirstName, this.CustomerLastName );
				}
				this.CardNumber = itin.PmtProcessor.CardNumber;
				this.CardType = itin.PmtProcessor.CardType;
			} catch { }
		}
		this.ErrorMessage = itin.ErrorMessage;
		itin.ErrorMessage = "";
	}

}
