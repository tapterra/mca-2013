using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

using ABS;

/// <summary>
///		Summary description for BookingPanel.
/// </summary>
public partial class BookingPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public string HeaderCellCssClass {
		get { return ( this.TopCell.CssClass ); }
		set { this.TopCell.CssClass = value; }
	}

	public string BookingCssClass {
		get { return ( this.BookingLbl.CssClass ); }
		set { this.BookingLbl.CssClass = value; }
	}

	public string HeaderCssClass {
		get { return ( this.ActivityTitleLbl.CssClass ); }
		set { this.ActivityTitleLbl.CssClass = value; }
	}

	public string HeaderDateTimeCssClass {
		get { return ( this.BookedDateTimeLbl.CssClass ); }
		set { this.BookedDateTimeLbl.CssClass = value; }
	}

	#endregion

	public string ActivityTitle {
		get { return ( this.ActivityTitleLbl.Text ); }
		set { this.ActivityTitleLbl.Text = value; }
	}

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
		set { this.ViewState["ActivityAID"] = value; }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current;
			if ( aBooking == null ) {
				if ( string.IsNullOrEmpty( this.ActivityAID ) ) {
					throw new ArgumentNullException( "ActivityAID", "Can't have a booking without a valid Activity AID." );
				}
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
		set {
			Booking.Current = value;
			this.ActivityTitle = value.Activity.ItineraryTitle;
		}
	}

	#endregion

	#region Methods

	protected void OnGuestsInfoCompleted( object sender, System.EventArgs e ) {
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
	}

	protected void OnHotelSelectCompleted( object sender, System.EventArgs e ) {
		this.Wizard.ActiveViewIndex++;
	}

	protected void OnDateTimeSelectCompleted( object sender, System.EventArgs e ) {
		this.TheBooking.SelectedScheduleDateTime = this.TheBooking.Activity.ScheduleDateTimes[this.DateTimePanel.SelectedDateTime.Value];
		this.BookedDateTimeLbl.Text = this.TheBooking.SelectedDateTimeString;
		this.Wizard.ActiveViewIndex++;
	}

	protected void OnDateSelectionChanged( object sender, System.EventArgs e ) {
		this.SetupBookedDateTimeLbl();
	}

	private void SetupBookedDateTimeLbl() {
		DateTime? selectedDate = this.DateTimePanel.SelectedDate;
		if ( !selectedDate.HasValue ) {
			this.BookedDateTimeLbl.Visible = true;
			this.BookedDateTimeLbl.Text = "&nbsp;";
		} else {
			this.BookedDateTimeLbl.Visible = true;
			this.BookedDateTimeLbl.Text = selectedDate.Value.ToString( "dddd, MMMM d, yyyy" );
		}
	}

	public void SetupForCurrentBooking() {
		Itinerary itin = Customer.Current.CurrentItinerary;
		// Find our reference instance
		Booking refBooking;
		bool isRevision = false;
		if ( this.TheBooking.SelectedScheduleDateTime != null && this.TheBooking.SelectedScheduleDateTime.Value > DateTime.MinValue ) {
			refBooking = this.TheBooking;
			isRevision = true;
		} else {
			if ( itin.Bookings.Count > 1 ) {
				refBooking = itin.Bookings[itin.Bookings.Count - 2];
				if ( refBooking.SelectedScheduleDateTime == null ) {
					refBooking = null;
				}
			} else {
				refBooking = null;
			}
		}
		// Set up the DateTimePanel...
		if ( isRevision ) {
			// We're editing an existing booking - restore the selected date & time
			this.DateTimePanel.SelectedDate = refBooking.SelectedScheduleDateTime.Value;
		} else {
			// New booking...
			this.DateTimePanel.SelectedDate = null;
			if ( refBooking != null ) {
				// Show the same month as the previous booking...
				this.DateTimePanel.VisibleDate = refBooking.SelectedScheduleDateTime.Value;
			}
		}
		this.SetupBookedDateTimeLbl();
		// Set up the HotelPaxCountPanel ...
		this.HotelPaxCountPanel.CustomerFirstName = itin.CustomerFirstName;
		this.HotelPaxCountPanel.CustomerLastName = itin.CustomerLastName;
		if ( refBooking != null ) {
			if ( !string.IsNullOrEmpty( refBooking.HotelAID ) ) {
				this.HotelPaxCountPanel.SelectedHotelAID = refBooking.HotelAID;
			}
			this.HotelPaxCountPanel.OtherHotelName = refBooking.OtherHotelName;
			//this.HotelPaxCountPanel.HotelRoomNo = refBooking.HotelRoomNumber;
		}
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			Itinerary itin = Customer.Current.CurrentItinerary;
			string bx = this.Request.QueryString["bx"];
			if ( !string.IsNullOrEmpty( bx ) ) {
				// We're editing an existing booking, via it's index in the itinerary
				int bookingIndex = Int32.Parse( bx );
				if ( bookingIndex >= 0 && bookingIndex < itin.Bookings.Count ) {
					Booking booking = itin.Bookings[bookingIndex];
					this.HotelPaxCountPanel.ActivityAID = this.ActivityAID = booking.Activity.AID;
					this.TheBooking = booking;
				}
			} else {
				// We're creating a new booking via the ActivityAID
				this.HotelPaxCountPanel.ActivityAID = this.ActivityAID = this.Request.QueryString["id"];
				if ( string.IsNullOrEmpty( this.ActivityAID ) ) {
					Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "default.aspx" ) );
				}
				this.TheBooking = new Booking( this.ActivityAID );
				itin.Bookings.Add( this.TheBooking );
			}
			this.SetupForCurrentBooking();
		}
	}

	#endregion

}
