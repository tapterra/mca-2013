<%@ control language="c#" autoeventwireup="true" codefile="BookingCalendar.ascx.cs" inherits="BookingCalendar" classname="BookingCalendar" %>

<asp:table runat="server" id="MainTable" 
	font-names="Tahoma" font-size="8pt"
	cellspacing="0" cellpadding="1" 
	style="border:1px solid darkblue; margin-top: 14px" 
	>
	<asp:tablerow runat="server" id="HeaderRow">
		<asp:tablecell runat="server" id="PrevBtnCell" style="border:1px solid darkblue" horizontalalign="center" width="30" >
			<asp:LinkButton runat="server" id="PrevBtn" text="<<" OnClick="PrevMonth" />
		</asp:tablecell>
		<asp:tablecell runat="server" id="MonthListBoxCell" style="border:1px solid darkblue" horizontalalign="center" >
			<asp:DropDownList runat="server" id="MonthListBox" AutoPostBack="True" OnSelectedIndexChanged="MonthChanged" width="170" >
				<asp:listitem value="January" />
				<asp:listitem value="February" />
				<asp:listitem value="March" />
				<asp:listitem value="April" />
				<asp:listitem value="May" />
				<asp:listitem value="June" />
				<asp:listitem value="July" />
				<asp:listitem value="August" />
				<asp:listitem value="September" />
				<asp:listitem value="October" />
				<asp:listitem value="November" />
				<asp:listitem value="December" />
			</asp:DropDownList>
		</asp:tablecell>
		<asp:tablecell runat="server" id="YearListBoxCell" style="border:1px solid darkblue" horizontalalign="center" >
			<asp:DropDownList runat="server" id="YearListBox" AutoPostBack="True" OnSelectedIndexChanged="MonthChanged" width="60" />
		</asp:tablecell>
		<asp:tablecell runat="server" id="NextBtnCell" style="border:1px solid darkblue" horizontalalign="center" width="30" >
			<asp:LinkButton runat="server" id="NextBtn" text=">>" OnClick="NextMonth" />
		</asp:tablecell>
	</asp:tablerow>
	<asp:tablerow runat="server" id="BodyRow">
		<asp:tablecell runat="server" id="BodyCell" ColumnSpan="4" style="border:1px solid darkblue">
			<asp:calendar runat="server" id="Calendar" 
				font-names="Tahoma" font-size="8pt" 
				shownextprevmonth="false" showtitle="false"
				cellpadding="5"
				width="100%"
				daynameformat="FirstLetter"
				showgridlines="True"
				ondayrender="DayRender"
				onselectionchanged="OnDateSelectionChanged"
				onvisiblemonthchanged="OnVisibleMonthChanged"
			>
				<dayheaderstyle backcolor="darkblue" forecolor="white" font-bold="true" />
				<daystyle />
				<othermonthdaystyle backcolor="#cccccc" forecolor="#cccccc" />
				<selecteddaystyle backcolor="#ffcccc" forecolor="black" font-bold="true" />
			</asp:calendar>
		</asp:tablecell>
	</asp:tablerow>
</asp:table>
