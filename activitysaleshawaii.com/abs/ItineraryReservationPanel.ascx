<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItineraryReservationPanel.ascx.cs" Inherits="ItineraryReservationPanel" classname="ItineraryReservationPanel" %>

<asp:table runat="server" id="MainTable" >
	
	<asp:tablerow runat="server" id="HeaderTableRow" >
		<asp:tablecell runat="server" id="HeaderTableCell">
			
			<asp:table runat="server" id="HeaderTable" width="100%">
				<asp:tablerow id="ActivityRow" runat="server">
					<asp:tablecell id="ActivityLabelCell" runat="server" width="100">
						<asp:label runat="server" id="ActivityLabelLbl" text="Activity" />
					</asp:tablecell>
					<asp:tablecell id="ActivityNameCell" runat="server" columnspan="3">
						<asp:label runat="server" id="ActivityNameLbl" text="Oahu Deluxe Super Fun Activity" />
					</asp:tablecell>
				</asp:tablerow>

				<asp:tablerow id="ResDateRow" runat="server">
					<asp:tablecell id="ResDateLabelCell" runat="server" >
						<asp:label runat="server" id="ResDateLabelLbl" text="Date" />
					</asp:tablecell>
					<asp:tablecell id="ResDateCell" runat="server" >
						<asp:label runat="server" id="ResDateLbl" text="Saturday, November 11, 2005" />
					</asp:tablecell>
					<asp:tablecell id="PaxLabelCell" runat="server" >
						<asp:label runat="server" id="PaxLabelLbl" text="Guests" />
					</asp:tablecell>
					<asp:tablecell id="PaxCell" runat="server" >
						<asp:label runat="server" id="PaxLbl" text="3" />
					</asp:tablecell>
				</asp:tablerow>

				<asp:tablerow id="ResTimeRow" runat="server">
					<asp:tablecell id="PrefTimeLabelCell" runat="server" >
						<asp:label runat="server" id="PrefTimeLabelLbl" text="Preferred&nbsp;Time" />
					</asp:tablecell>
					<asp:tablecell id="PrefTimeCell" runat="server" >
						<asp:label runat="server" id="PrefTimeLbl" text="6:30 PM" />
					</asp:tablecell>
					<asp:tablecell id="AltTimeLabelCell" runat="server" width="100">
						<asp:label runat="server" id="AltTimeLabelLbl" text="Alternate&nbsp;Time" />
					</asp:tablecell>
					<asp:tablecell id="AltTimeCell" runat="server" >
						<asp:label runat="server" id="AltTimeLbl" text="7:30 PM" />
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="FooterTableRow" >
		<asp:tablecell runat="server" id="FooterTableCell" >
			
			<asp:table runat="server" id="FooterTable" width="100%" >

				<asp:tablerow id="TaxRow" runat="server" visible="false" >
					<asp:tablecell id="TaxLabelCell" runat="server" width="10%">
						<asp:label runat="server" id="TaxLabelLbl" text="Tax" />
					</asp:tablecell>
					<asp:tablecell id="TaxCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="TaxLbl" text="US$ 12.34" />
					</asp:tablecell>
				</asp:tablerow>

				<asp:tablerow id="TotalRow" runat="server">
					<asp:tablecell id="TotalLabelCell" runat="server" width="10%">
						<asp:label runat="server" id="TotalLabelLbl" text="Total" />
					</asp:tablecell>
					<asp:tablecell id="TotalCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="TotalLbl" text="US$ 5.00" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="ButtonRow" runat="server">
					<asp:tablecell id="ButtonCell" runat="server" columnspan="2" horizontalalign="center" >
						<asp:button runat="server" id="ReviseBtn" text="Revise" 
						 commandname="revise" oncommand="OnReservationCommand" 
						/>
						&nbsp;
						&nbsp;
						<asp:button runat="server" id="RemoveBtn" text="Remove" 
						 commandname="remove" oncommand="OnReservationCommand" 
						 onclientclick="return confirm( 'Are you sure you want to remove this item from your itinerary?' );"
						/>
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			
		</asp:tablecell>
	</asp:tablerow>

</asp:table>

