<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckoutBillingAddressPanel.ascx.cs" Inherits="CheckoutBillingAddressPanel" %>

<asp:panel runat="server" id="MainPanel" >
	
	<asp:label runat="server" id="HeaderLbl" />
	
	<asp:table runat="server" id="BodyTable" >
		
		<asp:tablerow id="Tablerow1" runat="server">
			<asp:tablecell id="Tablecell1" runat="server" columnspan="3">
				<stic:labeledtextbox runat="server" id="StreetAddress1Box" 
				 width="400" label='Street<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:textbox runat="server" id="StreetAddress2Box" 
				 width="100%"
				 tabindex="1"
				/><br />
				<asp:requiredfieldvalidator runat="server" id="StreetAddress1ReqVal"
				 controltovalidate="StreetAddress1Box" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;the&nbsp;billing&nbsp;street&nbsp;address."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>
		
		<asp:tablerow id="Tablerow2" runat="server">
			<asp:tablecell id="Tablecell3" runat="server" width="230" >
				<stic:labeledtextbox runat="server" id="CityBox" 
				 width="230"
				 label='City<span style="color:red">*</span>'
				 tabindex="1"
				/>
			</asp:tablecell>
			<asp:tablecell id="Tablecell2" runat="server" width="50">
				<stic:labeledtextbox runat="server" id="StateBox" 
				 width="100"
				 label='State<span style="color:red">*</span>'
				 tabindex="1"
				/>
			</asp:tablecell>
			<asp:tablecell id="Tablecell4" runat="server" >
				<stic:labeledtextbox runat="server" id="ZipBox" 
					width="100"
					label='Zip Code<span style="color:red">*</span>'
				 tabindex="1"
				/>
			</asp:tablecell>
		</asp:tablerow>
		<asp:tablerow runat="server">
			<asp:tablecell runat="server" columnspan="3">
				<asp:requiredfieldvalidator runat="server" id="CityReqVal"
				 controltovalidate="CityBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;the&nbsp;billing&nbsp;address&nbsp;city."
				 cssclass="ErrorMessages"
				/>
				<asp:requiredfieldvalidator runat="server" id="StateReqVal"
				 controltovalidate="StateBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="<br />Please&nbsp;enter&nbsp;the&nbsp;billing&nbsp;address&nbsp;state."
				 cssclass="ErrorMessages"
				/>
				<asp:requiredfieldvalidator runat="server" id="ZipReqVal"
				 controltovalidate="ZipBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="<br />Please&nbsp;enter&nbsp;the&nbsp;billing&nbsp;address&nbsp;zip-code."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell> 
		</asp:tablerow>
	</asp:table>
	
</asp:panel>
