using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ItineraryBookingPanel : System.Web.UI.UserControl {

	#region Properties

	public Booking TheBooking {
		get { return ( this.theBooking ); }
		set { this.theBooking = value; }
	}
	private Booking theBooking;

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public int CellSpacing {
		get { return ( this.MainTable.CellSpacing ); }
		set { this.MainTable.CellSpacing = value; }
	}

	public int CellPadding {
		get { return ( this.MainTable.CellPadding ); }
		set { this.MainTable.CellPadding = value; }
	}

	public string HeaderTableCssClass {
		get { return ( this.HeaderTable.CssClass ); }
		set { this.HeaderTable.CssClass = value; }
	}

	public string FooterTableCssClass {
		get { return ( this.FooterTable.CssClass ); }
		set { this.FooterTable.CssClass = value; }
	}

	public string GuestPanelsCellCssClass {
		get { return ( this.GuestPanelsCell.CssClass ); }
		set { this.GuestPanelsCell.CssClass = value; }
	}

	public string LabelCssClass {
		get {
			object obj = ViewState["LabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["LabelCssClass"] = value; }
	}

	public string ValueCssClass {
		get {
			object obj = ViewState["ValueCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ValueCssClass"] = value; }
	}

	public string ActivityNameCssClass {
		get { return ( this.ActivityNameLbl.CssClass ); }
		set { this.ActivityNameLbl.CssClass = value; }
	}

	public string ButtonCssClass {
		get {
			object obj = ViewState["ButtonCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ButtonCssClass"] = value; }
	}

	#region GuestPanel CSS Classes

	public string GuestPanelCssClass {
		get {
			object obj = ViewState["GuestPanelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelCssClass"] = value; }
	}

	public int GuestPanelCellSpacing {
		get {
			object obj = ViewState["GuestPanelCellSpacing"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["GuestPanelCellSpacing"] = value; }
	}

	public int GuestPanelCellPadding {
		get {
			object obj = ViewState["GuestPanelCellPadding"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["GuestPanelCellPadding"] = value; }
	}

	public string GuestPanelLabelCssClass {
		get {
			object obj = ViewState["GuestPanelLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelLabelCssClass"] = value; }
	}

	public string GuestPanelGuestNameCssClass {
		get {
			object obj = ViewState["GuestPanelGuestNameCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelGuestNameCssClass"] = value; }
	}

	public string GuestPanelGuestTypeCssClass {
		get {
			object obj = ViewState["GuestPanelGuestTypeCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelGuestTypeCssClass"] = value; }
	}

	public string GuestPanelPriceCssClass {
		get {
			object obj = ViewState["GuestPanelPriceCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelPriceCssClass"] = value; }
	}

	public string GuestPanelOptionsCssClass {
		get {
			object obj = ViewState["GuestPanelOptionsCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelOptionsCssClass"] = value; }
	}

	#endregion

	#endregion

	public int BookingIndex {
		get {
			object obj = ViewState["BookingIndex"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["BookingIndex"] = value; }
	}
	
	public string ActivityName {
		get { return ( this.ActivityNameLbl.Text ); }
		set { this.ActivityNameLbl.Text = value; }
	}

	public string BookedDateTime {
		get { return ( this.DateTimeLbl.Text ); }
		set { this.DateTimeLbl.Text = value; }
	}

	public string HotelInfo {
		get { return ( this.HotelLbl.Text ); }
		set { this.HotelLbl.Text = value; }
	}

	public decimal DiscountAmount {
		set {
			if ( Website.Current.IsNoPaymentSite ) {
				this.DiscountLbl.Text = "&nbsp;";
			} else {
				this.DiscountLbl.Text = string.Format( "US$ {0:F}", value );
				this.DiscountRow.Visible = ( value > 0 );
			}
		}
	}

	public decimal TotalPrice {
		set {
			if ( Website.Current.IsNoPaymentSite ) {
				this.TotalLbl.Text = "&nbsp;";
			} else {
				this.TotalLbl.Text = string.Format( "US$ {0:F}", value );
			}
		}
	}

	public decimal Tax {
		set {
			if ( Website.Current.IsNoPaymentSite ) {
				this.TaxLbl.Text = "&nbsp;";
			} else {
				this.TaxLbl.Text = string.Format( "US$ {0:F}", value );
			}
		}
	}

	#endregion

	private void BuildPanel() {
		this.GuestPanelsCell.Controls.Clear();
		if ( this.TheBooking == null ) {
			this.ActivityName = "";
			this.BookedDateTime = "";
			this.HotelInfo = "";
			return;
		}
		this.ActivityName = this.theBooking.Activity.ItineraryTitle;
		this.BookedDateTime = this.TheBooking.SelectedDateTimeString;
		this.HotelInfo = this.TheBooking.HotelInfo;
		int g = 0;
		foreach ( Guest guest in this.theBooking.Guests ) {
			ASP.ItineraryGuestPanel gPanel = this.Page.LoadControl( "~/abs/ItineraryGuestPanel.ascx" ) as ASP.ItineraryGuestPanel;
			gPanel.CssClass = this.GuestPanelCssClass;
			gPanel.CellSpacing = this.GuestPanelCellSpacing;
			gPanel.CellPadding = this.GuestPanelCellPadding;
			gPanel.LabelCssClass = this.GuestPanelLabelCssClass;
			gPanel.GuestNameCssClass = this.GuestPanelGuestNameCssClass;
			gPanel.GuestTypeCssClass = this.GuestPanelGuestTypeCssClass;
			gPanel.PriceCssClass = this.GuestPanelPriceCssClass;
			gPanel.OptionsCssClass = this.GuestPanelOptionsCssClass;
			gPanel.Guest = guest;
			gPanel.GuestIndex = ++g;
			this.GuestPanelsCell.Controls.Add( gPanel );
		}
		this.DiscountAmount = this.TheBooking.TotalDiscountAmount;
		this.TotalPrice = this.TheBooking.TotalDiscountedPricePlusTax;
		this.Tax = this.theBooking.Tax;
		this.RemoveBtn.CommandArgument = this.BookingIndex.ToString();
		this.ReviseBtn.CommandArgument = this.BookingIndex.ToString();
	}

	#region Events

	#region OnReviseBooking
	public event ItineraryItemEventHandler ReviseBooking {
		add { Events.AddHandler( ReviseItineraryItemEventKey, value ); }
		remove { Events.RemoveHandler( ReviseItineraryItemEventKey, value ); }
	}
	protected virtual void OnReviseBooking( Object sender, ItineraryItemEventArgs args ) {
		ItineraryItemEventHandler handler = (ItineraryItemEventHandler) Events[ReviseItineraryItemEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object ReviseItineraryItemEventKey = new object();
	#endregion

	#region OnRemoveBooking
	public event ItineraryItemEventHandler RemoveBooking {
		add { Events.AddHandler( RemoveItineraryItemEventKey, value ); }
		remove { Events.RemoveHandler( RemoveItineraryItemEventKey, value ); }
	}
	protected virtual void OnRemoveBooking( Object sender, ItineraryItemEventArgs args ) {
		ItineraryItemEventHandler handler = (ItineraryItemEventHandler) Events[RemoveItineraryItemEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object RemoveItineraryItemEventKey = new object();
	#endregion

	#endregion

	#region Event Handlers

	private void Page_Load( object sender, System.EventArgs e ) {
		this.BuildPanel();
	}

	protected override void OnPreRender( EventArgs e ) {
		if ( this.LabelCssClass.Length > 0 ) {
			this.ActivityLabelLbl.CssClass = this.LabelCssClass;
			this.DateTimeLabelLbl.CssClass = this.LabelCssClass;
			this.HotelLabelLbl.CssClass = this.LabelCssClass;
			this.TaxLabelLbl.CssClass = this.LabelCssClass;
			this.DiscountLabelLbl.CssClass = this.LabelCssClass;
			this.TotalLabelLbl.CssClass = this.LabelCssClass;

			this.DateTimeLbl.CssClass = this.ValueCssClass;
			this.HotelLbl.CssClass = this.ValueCssClass;
			this.TaxLbl.CssClass = this.ValueCssClass;
			this.DiscountLbl.CssClass = this.ValueCssClass;
			this.TotalLbl.CssClass = this.ValueCssClass;

			this.ReviseBtn.CssClass = this.ButtonCssClass;
			this.RemoveBtn.CssClass = this.ButtonCssClass;
		}
		base.OnPreRender( e );
	}

	protected void OnBookingCommand( object sender, CommandEventArgs e ) {
		int bookingIndex = Convert.ToInt32( e.CommandArgument );
		switch ( e.CommandName ) {
			case "revise":
				this.OnReviseBooking( this, new ItineraryItemEventArgs( bookingIndex ) );
				break;
			case "remove":
				this.OnRemoveBooking( this, new ItineraryItemEventArgs( bookingIndex ) );
				break;
		}
	}

	#endregion

}
