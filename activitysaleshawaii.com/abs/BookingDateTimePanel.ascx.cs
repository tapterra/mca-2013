using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ABS;

/// <summary>
///		Summary description for BookingDateTimePanel.
/// </summary>
public partial class BookingDateTimePanel : System.Web.UI.UserControl {

	#region Properties

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current as Booking;
			if ( aBooking == null ) {
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
	}

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainCell.CssClass ); }
		set { this.MainCell.CssClass = value; }
	}

	public string HeaderStepCssClass {
		get { return ( this.HeaderStepLbl.CssClass ); }
		set { this.HeaderStepLbl.CssClass = value; }
	}

	public string HeaderDateCssClass {
		get { return ( this.HeaderDateLbl.CssClass ); }
		set { this.HeaderDateLbl.CssClass = value; }
	}

	public string TimeInstructionsCssClass {
		get { return ( this.TimeInstructionsLbl.CssClass ); }
		set { this.TimeInstructionsLbl.CssClass = value; }
	}

	public string TimeOptionsCssClass {
		get { return ( this.AvailableTimesRBoxList.CssClass ); }
		set { this.AvailableTimesRBoxList.CssClass = value; }
	}

	public string NextButtonCssClass {
		get { return ( this.NextStepBtn.CssClass ); }
		set { this.NextStepBtn.CssClass = value; }
	}

	#endregion

	public string kTimeInstructions = "	On {0:dddd, MMMM d}, this activity is available at {0:t}.";

	public string kTimeOptionInstructions = "	On {0:dddd, MMMM d}, this activity is available at the following times ...<br />please select one:";

	public string TimeInstructions {
		get { return ( this.TimeInstructionsLbl.Text ); }
		set { this.TimeInstructionsLbl.Text = value; }
	}

	public DateTime VisibleDate {
		get { return ( this.Calendar.VisibleDate ); }
		set { this.Calendar.VisibleDate = value; }
	}

	public DateTime? SelectedDate {
		get { return ( this.Calendar.SelectedDate ); }
		set { this.Calendar.SelectedDate = value; }
	}

	public DateTime? SelectedDateTime {
		get {
			if ( !this.SelectedDate.HasValue ) {
				return ( null );
			}
			string timeStr = this.AvailableTimesRBoxList.SelectedItem.Value;
			DateTime result = this.SelectedDate.Value.AddTicks( Convert.ToInt64( timeStr ) );
			return ( result );
		}
	}

	#endregion

	#region Events

	#region VisibleMonthChanged
	public event MonthChangedEventHandler VisibleMonthChanged {
		add { Events.AddHandler( VisibleMonthChangedEventKey, value ); }
		remove { Events.RemoveHandler( VisibleMonthChangedEventKey, value ); }
	}
	protected virtual void OnVisibleMonthChanged( Object sender, MonthChangedEventArgs args ) {
		MonthChangedEventHandler handler = (MonthChangedEventHandler) Events[VisibleMonthChangedEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object VisibleMonthChangedEventKey = new object();
	#endregion

	#region DateSelectionChanged
	public event EventHandler DateSelectionChanged {
		add { Events.AddHandler( DateSelectionChangedEventKey, value ); }
		remove { Events.RemoveHandler( DateSelectionChangedEventKey, value ); }
	}
	protected virtual void OnDateSelectionChanged( Object sender, EventArgs args ) {
		EventHandler handler = (EventHandler) Events[DateSelectionChangedEventKey];
		if ( handler != null )
			handler( sender, args );
		this.ShowSelectedDateTimeOptions();
		this.OnNextStepButtonClicked( sender, args );
	}
	private static readonly object DateSelectionChangedEventKey = new object();
	#endregion

	#region NextStepButtonClicked
	public event EventHandler NextStepButtonClicked {
		add { Events.AddHandler( NextStepButtonClickedEventKey, value ); }
		remove { Events.RemoveHandler( NextStepButtonClickedEventKey, value ); }
	}
	protected virtual void OnNextStepButtonClicked( Object sender, EventArgs args ) {
		EventHandler handler = (EventHandler) Events[NextStepButtonClickedEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object NextStepButtonClickedEventKey = new object();
	#endregion

	#endregion

	#region Methods

	/// <summary>
	/// Called when a new date is selected on the calendar, to set up the time-of-day 
	/// selection control for the selected date.
	/// </summary>
	protected void ShowSelectedDateTimeOptions() {
		if ( this.SelectedDate.HasValue ) {
			this.TimeSelectorRow.Visible = true;
			this.AvailableTimesRBoxList.Items.Clear();
			// Step through each of the current activity's scheduled dates
			DateTime lastDate = DateTime.MinValue;
			foreach ( ScheduleDateTime sdt in this.TheBooking.Activity.ScheduleDateTimes ) {
				if ( sdt.Value.Date.Equals( this.SelectedDate.Value.Date ) ) {
					this.AvailableTimesRBoxList.Items.Add( new ListItem( sdt.Value.ToShortTimeString(), sdt.Value.TimeOfDay.Ticks.ToString() ) );
					lastDate = sdt.Value;
				}
			}
			if ( this.AvailableTimesRBoxList.Items.Count == 1 ) {
				if ( lastDate.TimeOfDay.Ticks > 0 ) {
					this.TimeInstructions = string.Format( this.kTimeInstructions, lastDate );
				} else {
					this.TimeInstructions = "";
				}
				this.AvailableTimesRBoxList.Visible = false;
			} else {
				this.TimeInstructions = string.Format( this.kTimeOptionInstructions, this.SelectedDate.Value );
				this.AvailableTimesRBoxList.Visible = true;
			}

			this.AvailableTimesRBoxList.SelectedIndex = 0;
			this.NextStepBtn.Enabled = true;
		}
	}

	private void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			if ( this.SelectedDate.HasValue ) {
				this.Calendar.InitCalendar( this.SelectedDate );
				this.VisibleDate = this.SelectedDate.Value;
				this.ShowSelectedDateTimeOptions();
			} else {
				this.TimeInstructions = "";
				this.Calendar.InitCalendar( null );
			}
			this.Calendar.ResetCalendar();
		}
	}

	#endregion

}
