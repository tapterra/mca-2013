<%@ control language="c#" autoeventwireup="true" codefile="BookingGuestsPanel.ascx.cs" inherits="BookingGuestsPanel" classname="BookingGuestsPanel" %>
<%@ reference virtualpath="GuestInfoPanel.ascx" %>

<asp:table runat="server" ID="MainTable">
	
	<asp:tablerow id="Tablerow1" runat="server">
		<asp:tablecell runat="server" id="HeaderStepCell" >
			<asp:label runat="server" id="HeaderStepLbl" text="Step&nbsp;3:&nbsp;&nbsp;&nbsp;&nbsp;" />
		</asp:tablecell>
		<asp:tablecell runat="server" id="HeaderGuestCell" >
			<asp:label runat="server" id="HeaderGuestLbl" text="Guest Information:" />
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow id="Tablerow2" runat="server">
		<asp:tablecell runat="server" id="DummyCell" >
		</asp:tablecell>
		<asp:tablecell runat="server" id="HeaderInstructionsCell" style="padding-top:6px" >
			<asp:label runat="server" id="HeaderInstructionsLbl" >
				The following information about the members of your party is required by the activity vendor...
			</asp:label>
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="BodyRow" >
		<asp:tablecell runat="server" id="Tablecell1" >
		</asp:tablecell>
		<asp:tablecell runat="server" ID="BodyCell" style="padding-bottom: 20px" >
			
			<asp:table runat="server" id="BodyTable" cellspacing="6" cellpadding="0" />
			
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="BottomRow" >
		<asp:tableheadercell runat="server" ID="BottomCell" columnspan="2" cssclass="BookingFooterCell" >
			<asp:button runat="server" id="NextStepBtn" text="Next Step >>" 
				onclick="OnNextStepButtonClicked"
				enabled="true" 
			 tabindex="1"
			/>
		</asp:tableheadercell>
	</asp:tablerow>
	
</asp:table>
