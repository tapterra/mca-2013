<%@ control language="C#" autoeventwireup="true" codefile="CustomerAccountPanel.ascx.cs" inherits="CustomerAccountPanel" %>

<asp:panel runat="server" id="LoginFormPanel">
	<div class="custAcctHeader">Sign In</div>
	<p class="custAcctInstructions">
		Please enter the email address and account number provided when you completed payment of your itinerary:
	</p>
	<div style="text-align: center">
		<table cellspacing="0" width="600">
			<tr>
				<td>
				</td>
				<td valign="top">
					<asp:requiredfieldvalidator runat="server" id="EmailReqVal" controltovalidate="EmailBox" display="dynamic" setfocusonerror="true" enableclientscript="true" errormessage="Please&nbsp;enter&nbsp;your&nbsp;email&nbsp;address." cssclass="ErrorMessages" />
					<stic:emailvalidator runat="server" id="EmailBoxValidator" controltovalidate="EmailBox" display="dynamic" setfocusonerror="true" enableclientscript="true" errormessage="Please&nbsp;enter&nbsp;a&nbsp;valid&nbsp;email&nbsp;address." cssclass="ErrorMessages" />
				</td>
			</tr>
			<tr>
				<td valign="top">
					<asp:label runat="server" id="EmailLbl" text="Email Address:" cssclass="custAcctInstructions" font-size="9pt" font-bold="true" />
				</td>
				<td valign="middle">
					<asp:textbox runat="server" id="EmailBox" text="" cssclass="custAcctInstructions" tabindex="1" />
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td valign="top">
					<asp:requiredfieldvalidator runat="server" id="AccountNoReqVal" controltovalidate="AccountNoBox" display="dynamic" setfocusonerror="true" enableclientscript="true" errormessage="Please&nbsp;enter&nbsp;your&nbsp;account&nbsp;number." cssclass="ErrorMessages" />
				</td>
			</tr>
			<tr>
				<td valign="top">
					<asp:label runat="server" id="AccountNoLbl" text="Account&nbsp;Number:&nbsp;" cssclass="custAcctInstructions" font-size="9pt" font-bold="true" />
				</td>
				<td valign="middle">
					<asp:textbox runat="server" id="AccountNoBox" text="" cssclass="custAcctInstructions" tabindex="1" />
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<stic:button runat="server" id="SignInBtn" text="Sign In" cssclass="StepButton" width="120px" onclick="DoSignIn" />
				</td>
			</tr>
		</table>
	</div>
	<br />
	<asp:label runat="server" id="ErrorMsg" cssclass="custAcctErrorMsgs" visible="false" text="The email address or account code you entered do not match our records. Please try again." />
</asp:panel>

<asp:panel runat="server" id="AccountInfoPanel" visible="false">
	<div class="custAcctHeader">Customer Itinerary</div>
	
	<table border="0" cellpadding="2" cellspacing="0" width="600" class="ItinHeader">
		
		<tr>
			<td class="ItinPanelLabel">Customer</td>
			<td class="ItinPanelLabel">Date/Time</td>
			<td class="ItinPanelLabel">Email</td>
			<td class="ItinPanelLabel">Telephone</td>
		</tr>
		
		<tr>
			<td>
				<asp:label runat="server" id="CustomerNameLbl" cssclass="ItinPanelValue" 
				 text='<%# this.Itinerary.CustomerFullName %>'
				/>
			</td>
			<td>
				<asp:label runat="server" id="PurchaseDateTimeLbl" cssclass="ItinPanelValue" 
				 text='<%# this.Itinerary.CreatedDateTimeStr %>'
				/>
			</td>
			<td>
				<asp:label runat="server" id="CustEmailLbl" cssclass="ItinPanelValue" 
				 text='<%# this.Itinerary.ContactEmail %>'
				/>
			</td>
			<td>
				<asp:label runat="server" id="CustPhoneLbl" cssclass="ItinPanelValue" 
				 text='<%# this.Itinerary.ContactPhone %>'
				/>
			</td>
		</tr>
		
		<tr>
			<td colspan="4"></td>
			<td>
				<asp:button runat="server" id="VouchersButton" text="Show Vouchers" tabindex="1" 
					causesvalidation="false" usesubmitbehavior="false" 
					onclientclick="showVoucher();" 
					style="width:140px"
				/>
			</td>
		</tr>
		
	</table>
	
	<script type="text/javascript">
		function showVoucher() {
			if ( document.getElementById != null ) {
				var html = "";
				var voucherPanel = document.getElementById( "voucherPanel" );
				if ( voucherPanel != null ) {
					html += voucherPanel.innerHTML;
				}
				//var win = window.open("","vouchers", "height=1000,width=1000,top=20,left=20,directories=yes,location=yes,menubar=yes,toolbar=no,status=yes,resizeable=yes" );
				var win = window.open("","vouchers", "" );
				win.document.open();
				win.document.write(html);
				win.document.close();
			}
		}
	</script>

	<div id="voucherPanel" style="width:600px; display:none" >
		<asp:literal runat="server" id="VoucherHolder" />
	</div>

	<asp:repeater runat="server" id="BookingRepeater">
		<headertemplate>
			<table width="600" cellpadding="0" cellspacing="0" border="0" class="ItinBookPanel">
		</headertemplate>
		<itemtemplate>
			<tr>
				<td>
					<table width="600" cellpadding="0" cellspacing="0" border="0" class="ItinBookHeaderTable">
						<tr>
							<td width="90">
								<asp:label runat="server" id="ActivityLabelLbl" text="Activity" cssclass="ItinPanelLabel" />
							</td>
							<td>
								<asp:label runat="server" id="ActivityNameLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "ActivityTitle") %>' />
							</td>
							<td align="right" rowspan="3" valign="baseline">
								<asp:label runat="server" id="Label2" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval( Container.DataItem, "TotalPriceStr" ) %>' />
							</td>
						</tr>
						<tr>
							<td>
								<asp:label runat="server" id="DateTimeLabelLbl" text="Date/Time" cssclass="ItinPanelLabel" />
							</td>
							<td>
								<asp:label runat="server" id="DateTimeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "SelectedDateTimeStr") %>' />
							</td>
						</tr>
						<tr>
							<td>
								<asp:label runat="server" id="HotelLabelLbl" text="Hotel" cssclass="ItinPanelLabel" />
							</td>
							<td>
								<asp:label runat="server" id="HotelLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "HotelName") %>' />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<asp:repeater runat="server" id="GuestsRepeater" datasource='<%# DataBinder.Eval(Container.DataItem, "Guests") %>'>
						<headertemplate>
							<table width="600" cellpadding="0" cellspacing="0" border="0" class="ItinGuestPanel">
						</headertemplate>
						<itemtemplate>
							<tr>
								<td width="90">
									<asp:label runat="server" id="GuestIndexLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "GuestNumberDisplay") %>' />
								</td>
								<td>
									<asp:label runat="server" id="GuestNameLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "GuestNameDisplay") %>' />
								</td>
								<td>
									<asp:label runat="server" id="GuestTypeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "GuestTypeDisplay") %>' />
								</td>
								<td align="right">
									<asp:label runat="server" id="PriceLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "PriceDisplay") %>' />
								</td>
							</tr>
							<tr id="Tr1" runat="server" cssclass="ItinPanelValue" visible='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplayVisible") %>'>
								<td>
									&nbsp;
								</td>
								<td colspan="3">
									<asp:label runat="server" id="OptionsLbl" cssclass="ItinPanelGuestTypeValue" text='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplay") %>' />
								</td>
							</tr>
						</itemtemplate>
<%--
						<alternatingitemtemplate>
							<tr>
								<td width="90">
									<asp:label runat="server" id="GuestIndexLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "GuestNumberDisplay") %>' />
								</td>
								<td>
									<asp:label runat="server" id="GuestNameLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "GuestNameDisplay") %>' />
								</td>
								<td>
									<asp:label runat="server" id="GuestTypeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "GuestTypeDisplay") %>' />
								</td>
								<td align="right">
									<asp:label runat="server" id="PriceLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "PriceDisplay") %>' />
								</td>
							</tr>
							<tr id="Tr1" runat="server" style="background-color: #ffffff" visible='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplayVisible") %>'>
								<td>
									&nbsp;
								</td>
								<td colspan="3">
									<asp:label runat="server" id="OptionsLbl" cssclass="ItinPanelGuestTypeValue" text='<%# DataBinder.Eval(Container.DataItem, "OptionsDisplay") %>' />
								</td>
							</tr>
						</alternatingitemtemplate>
--%>
						<footertemplate>
							</table>
						</footertemplate>
					</asp:repeater>
				</td>
			</tr>
		</itemtemplate>
		<footertemplate>
			</table>
		</footertemplate>
	</asp:repeater>
	<asp:repeater runat="server" id="ResRepeater">
		<itemtemplate>
			<table width="600" cellpadding="2" cellspacing="0" border="0" class="ItinBookHeaderTable" style="margin-top: 2px">
				<tr>
					<td width="90">
						<asp:label runat="server" id="ActivityLabelLbl" text="Reservation" cssclass="ItinPanelLabel" />
					</td>
					<td colspan="6">
						<asp:label runat="server" id="ActivityNameLbl" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "ActivityTitle") %>' />
					</td>
					<td align="right">
						<asp:label runat="server" id="Label3" cssclass="ItinPanelLabel" text='<%# DataBinder.Eval(Container.DataItem, "TotalPriceStr") %>' />
					</td>
				</tr>
				<tr style="background-color: white">
					<td width="90">
						<asp:label runat="server" id="DateTimeLabelLbl" text="Date" cssclass="ItinPanelLabel" />
					</td>
					<td>
						<asp:label runat="server" id="DateTimeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "ResDateStr") %>' />
					</td>
					<td width="90">
						<asp:label runat="server" id="GuestsLabelLbl" text="Guests" cssclass="ItinPanelLabel" />
					</td>
					<td>
						<asp:label runat="server" id="PaxCountLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "PaxCount") %>' />
					</td>
					<td width="90">
						<asp:label runat="server" id="PrefTimeLabelLbl" text="Pref. Time" cssclass="ItinPanelLabel" />
					</td>
					<td>
						<asp:label runat="server" id="PrefTimeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "PrefTime") %>' />
					</td>
					<td width="90">
						<asp:label runat="server" id="AltTimeLabelLbl" text="Alt. Time" cssclass="ItinPanelLabel" />
					</td>
					<td>
						<asp:label runat="server" id="AltTimeLbl" cssclass="ItinPanelValue" text='<%# DataBinder.Eval(Container.DataItem, "AltTime") %>' />
					</td>
				</tr>
			</table>
		</itemtemplate>
	</asp:repeater>

	<table width="600" cellpadding="3" cellspacing="0" border="0" class="ItinFooter" style="margin-top: 2px">
		<tr class="ItinFooter">
			<td>
				<asp:label runat="server" id="TotalLabelLbl" text="Itinerary&nbsp;Total" cssclass="ItinPanelLabel" />
			</td>
			<td align="right">
				<asp:label runat="server" id="TotalLbl" cssclass="ItinPanelLabel" text='<%# this.Itinerary.TotalPriceStr %>' />
			</td>
		</tr>
	</table>
	<asp:repeater runat="server" id="PaymentsRepeater">
		<headertemplate>
			<table width="600" cellpadding="2" cellspacing="0" border="0" class="ItinBookHeaderTable" style="margin-top: 2px">
				<tr>
					<td class="ItinPanelLabel" colspan="5">Payments</td>
				</tr>
				<tr>
					<td class="ItinPanelLabel">Date</td>
					<td class="ItinPanelLabel">Method</td>
					<td class="ItinPanelLabel">Card Ref</td>
					<td class="ItinPanelLabel">Auth Code</td>
					<td class="ItinPanelLabel" align="right">Amount</td>
				</tr>
		</headertemplate>
		<itemtemplate>
			<tr>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label6" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "PostedDT" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label5" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "MethodType" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label4" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "CCRef" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label1" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "AuthCode" ) %>' />
				</td>
				<td class="ItinPanelValue" align="right">
					<asp:label runat="server" id="Label2" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "AmountStr" ) %>' />
				</td>
			</tr>
		</itemtemplate>
<%--
		<alternatingitemtemplate>
			<tr style="background-color: #ffffff">
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label6" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "PostedDT" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label5" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "MethodType" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label4" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "CCRef" ) %>' />
				</td>
				<td class="ItinPanelValue">
					<asp:label runat="server" id="Label1" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "AuthCode" ) %>' />
				</td>
				<td class="ItinPanelValue" align="right">
					<asp:label runat="server" id="Label2" cssclass="ItinPanelValue" text='<%# DataBinder.Eval( Container.DataItem, "AmountStr" ) %>' />
				</td>
			</tr>
		</alternatingitemtemplate>
--%>
		<footertemplate>
			</table>
		</footertemplate>
	</asp:repeater>
</asp:panel>
