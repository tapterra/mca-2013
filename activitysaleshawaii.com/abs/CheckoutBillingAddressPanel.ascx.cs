using System;
using ABS;

public partial class CheckoutBillingAddressPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainPanel.CssClass ); }
		set { this.MainPanel.CssClass = value; }
	}

	public string HeaderCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}
	
	public string BodyTableCssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}
	
	public int BodyTableSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}
	
	public string FormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}
	
	public string FormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	#region Data

	public string HeaderLabel {
		get { return ( this.HeaderLbl.Text ); }
		set { this.HeaderLbl.Text = value; }
	}

	public string StreetAddress1 {
		get { return ( this.StreetAddress1Box.Text ); }
		set { this.StreetAddress1Box.Text = value; }
	}

	public string StreetAddress2 {
		get { return ( this.StreetAddress2Box.Text ); }
		set { this.StreetAddress2Box.Text = value; }
	}
	
	public string City {
		get { return ( this.CityBox.Text ); }
		set { this.CityBox.Text = value; }
	}
	
	public string State {
		get { return ( this.StateBox.Text ); }
		set { this.StateBox.Text = value; }
	}

	public string Zip {
		get { return ( this.ZipBox.Text ); }
		set { this.ZipBox.Text = value; }
	}

	#endregion

	#endregion

	protected override void OnPreRender( EventArgs e ) {
		base.OnPreRender( e );
		if ( Website.Current.IsNoPaymentSite ) {
			this.HeaderLabel = "Contact Address";
		} else {
			this.HeaderLabel = "Billing Address (for Credit Card Verification)";
		}
		if ( this.FormLabelCssClass.Length > 0 ) {
			this.StreetAddress1Box.LabelStyle.CssClass = this.FormLabelCssClass;
			this.CityBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.StateBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.ZipBox.LabelStyle.CssClass = this.FormLabelCssClass;
		}
		if ( this.FormInputCssClass.Length > 0 ) {
			this.StreetAddress1Box.CssClass = this.FormInputCssClass;
			this.StreetAddress2Box.CssClass = this.FormInputCssClass;
			this.CityBox.CssClass = this.FormInputCssClass;
			this.StateBox.CssClass = this.FormInputCssClass;
			this.ZipBox.CssClass = this.FormInputCssClass;
		}
	}

}
