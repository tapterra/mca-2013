using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ABS;
using StarrTech.WebTools.Controls;

/// <summary>
///		Summary description for BookingGuestsPanel.
/// </summary>
public partial class BookingGuestsPanel : System.Web.UI.UserControl {

	#region Properties

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current;
			if ( aBooking == null ) {
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
	}

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public string HeaderStepCssClass {
		get { return ( this.HeaderStepCell.CssClass ); }
		set { this.HeaderStepCell.CssClass = value; }
	}

	public string HeaderGuestCssClass {
		get { return ( this.HeaderGuestCell.CssClass ); }
		set { this.HeaderGuestCell.CssClass = value; }
	}

	public string HeaderInstructionsCssClass {
		get { return ( this.HeaderInstructionsCell.CssClass ); }
		set { this.HeaderInstructionsCell.CssClass = value; }
	}

	public string NextButtonCssClass {
		get { return ( this.NextStepBtn.CssClass ); }
		set { this.NextStepBtn.CssClass = value; }
	}


	public string GuestCssClass {
		get {
			object obj = ViewState["GuestCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestCssClass"] = value; }
	}

	public string GuestHeaderCssClass {
		get {
			object obj = ViewState["GuestHeaderCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestHeaderCssClass"] = value; }
	}

	public string GuestBodyTableCssClass {
		get {
			object obj = ViewState["GuestBodyTableCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestBodyTableCssClass"] = value; }
	}

	public int GuestBodyTableSpacing {
		get {
			object obj = ViewState["GuestBodyTableSpacing"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["GuestBodyTableSpacing"] = value; }
	}

	public string GuestFormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}

	public string GuestFormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	#region Text Labels

	public string HeaderStepLabel {
		get { return ( this.HeaderStepLbl.Text ); }
		set { this.HeaderStepLbl.Text = value; }
	}

	public string HeaderGuestLabel {
		get { return ( this.HeaderGuestLbl.Text ); }
		set { this.HeaderGuestLbl.Text = value; }
	}

	public string HeaderInstructionsLabel {
		get { return ( this.HeaderInstructionsLbl.Text ); }
		set { this.HeaderInstructionsLbl.Text = value; }
	}

	#endregion

	#endregion

	#region Events

	#region NextStepButtonClicked
	public event EventHandler NextStepButtonClicked {
		add { Events.AddHandler( NextStepButtonClickedEventKey, value ); }
		remove { Events.RemoveHandler( NextStepButtonClickedEventKey, value ); }
	}
	protected virtual void OnNextStepButtonClicked( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		EventHandler handler = (EventHandler) Events[NextStepButtonClickedEventKey];
		if ( handler != null ) {
			this.ReadGuestInfoResponses();
			handler( sender, args );
		}
	}
	private static readonly object NextStepButtonClickedEventKey = new object();
	#endregion

	#endregion

	protected override void CreateChildControls() {
		base.CreateChildControls();
		this.BuildGuestinfoPanels();
	}

	private void BuildGuestinfoPanels() {
		bool isFirstGuest = true;
		foreach ( Guest g in this.TheBooking.Guests ) {
			ASP.GuestInfoPanel panel = this.Page.LoadControl( "~/abs/GuestInfoPanel.ascx" ) as ASP.GuestInfoPanel;
			ScheduleDateTime dt = this.TheBooking.SelectedScheduleDateTime;
			foreach ( PriceCode pc in this.TheBooking.Activity.ScheduleDateTimes[dt.Value].PriceCodes ) {
				if ( pc.UID == g.PriceCodeUID ) {
					panel.PriceCodeUID = pc.UID;
					int pcTotalCount = this.TheBooking.PaxCounts[pc.UID].PaxQty;
					panel.HeaderLabel = string.Format( "{0} {1} of {2}", pc.Label, g.Seq, pcTotalCount );
					panel.ID = string.Format( "{0}_{1}", pc.UID, g.Seq );
				}
			}
			panel.Seq = g.Seq;
			panel.CssClass = this.GuestCssClass;
			panel.HeaderCssClass = this.GuestHeaderCssClass;
			panel.BodyTableCssClass = this.GuestBodyTableCssClass;
			panel.BodyTableSpacing = this.GuestBodyTableSpacing;
			panel.FormLabelCssClass = this.GuestFormLabelCssClass;
			panel.FormInputCssClass = this.GuestFormInputCssClass;
			if ( this.TheBooking.Guests.Count > 1 && isFirstGuest && this.TheBooking.Activity.Options.Count > 0 ) {
				panel.ShowCopyOptionsButton = true;
				panel.CopyOptions += new EventHandler( DoCopyOptions );
			}
			System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
			cell.ColumnSpan = 2;
			cell.Controls.Add( panel );
			TableRow row = new TableRow();
			row.Cells.Add( cell );
			this.BodyTable.Rows.Add( row );
			isFirstGuest = false;
		}
	}

	public void DoCopyOptions( object sender, EventArgs e ) {
		// Copy the option responses from the first guest to all remaining guests
		ASP.GuestInfoPanel firstGuestPanel = this.BodyTable.Rows[0].Cells[0].Controls[0] as ASP.GuestInfoPanel;
		foreach ( Option op in this.TheBooking.Activity.Options ) {
			string firstResponse = firstGuestPanel.ReadOptionResponse( op );
			for ( int r = 1 ; r < this.BodyTable.Rows.Count ; r++ ) {
				ASP.GuestInfoPanel nextGuestPanel = this.BodyTable.Rows[r].Cells[0].Controls[0] as ASP.GuestInfoPanel;
				switch ( op.ResponseType ) {
					case OptionResponseTypes.Checkbox:
						CheckBox xbox = nextGuestPanel.FindControl( op.ControlID ) as CheckBox;
						xbox.Checked = firstResponse == "Yes";
						break;
					case OptionResponseTypes.DropDownList:
						LabeledDropDownList lbox = nextGuestPanel.FindControl( op.ControlID ) as LabeledDropDownList;
						lbox.SelectedValue = firstResponse;
						break;
					case OptionResponseTypes.PlainText:
						LabeledTextBox tbox = nextGuestPanel.FindControl( op.ControlID ) as LabeledTextBox;
						tbox.Text = firstResponse;
						break;
				}
			}
		}
	}

	private void ReadGuestInfoResponses() {
		foreach ( TableRow row in this.BodyTable.Rows ) {
			System.Web.UI.WebControls.TableCell cell = row.Cells[0];
			ASP.GuestInfoPanel panel = cell.Controls[0] as ASP.GuestInfoPanel;
			string[] args = panel.ID.Split( '_' );
			string pcUid = args[0];
			int guestSeq = Convert.ToInt32( args[1] );
			Guest g = this.TheBooking.Guests.GetItem( pcUid, guestSeq );
			g.FirstName = panel.FirstName;
			g.LastName = panel.LastName;
			g.OptionResponses.Clear();
			foreach ( Option op in this.TheBooking.Activity.Options ) {
				g.OptionResponses.Add( new OptionResponse( g.ID, op.ID, op.QuestionText, panel.ReadOptionResponse( op ) ) );
			}

		}
	}

}
