<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItineraryGuestPanel.ascx.cs" Inherits="ItineraryGuestPanel" classname="ItineraryGuestPanel" %>

	<asp:table runat="server" id="BodyTable" >
		
		<asp:tablerow id="TopRow" runat="server">
			<asp:tablecell id="LabelCell" runat="server" width="90">
				<asp:label runat="server" id="GuestIndexLbl" text="Guest 1" />
			</asp:tablecell>
			<asp:tablecell id="GuestNameCell" runat="server" >
				<asp:label runat="server" id="GuestNameLbl" />
			</asp:tablecell>
			<asp:tablecell id="GuestTypeCell" runat="server" >
				<asp:label runat="server" id="GuestTypeLbl" />
			</asp:tablecell>
			<asp:tablecell id="PriceCell" runat="server" horizontalalign="right" >
				<asp:label runat="server" id="PriceLbl" />
			</asp:tablecell>
		</asp:tablerow>
		
		<asp:tablerow id="OptionsRow" runat="server" visible="false" >
			<asp:tablecell id="OptionsLabelCell" runat="server" >
				&nbsp;
			</asp:tablecell>
			<asp:tablecell id="OptionsCell" runat="server" columnspan="3" >
				<asp:label runat="server" id="OptionsLbl" />
			</asp:tablecell>
		</asp:tablerow>
		
	</asp:table>
