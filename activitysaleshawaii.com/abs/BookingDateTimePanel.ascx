<%@ control language="c#" autoeventwireup="true" codefile="BookingDateTimePanel.ascx.cs" inherits="BookingDateTimePanel" classname="BookingDateTimePanel" %>
<%@ register tagprefix="abs" tagname="bookingcalendar" src="BookingCalendar.ascx" %>

<asp:table runat="server" id="MainTable" cellpadding="3" width="100%">
	<asp:tablerow runat="server" id="MainRow" backcolor="lightgray" >
		
		<asp:tablecell runat="server" ID="MainCell" style="padding-bottom: 20px" >
			<span style="float:left">
				<asp:label runat="server" id="HeaderStepLbl" text="Step&nbsp;1:&nbsp;&nbsp;&nbsp;&nbsp;" />
				<asp:label runat="server" id="HeaderDateLbl" text="Select the date you wish to attend.  You can use the drop down arrows to navigate to the selected month and year." />
			</span>
			<span style="float:right">
				<abs:bookingcalendar runat="server" ID="Calendar" 
					width="300"
					ondateselectionchanged="OnDateSelectionChanged"
					onvisiblemonthchanged="OnVisibleMonthChanged"
				/>
			</span>
		</asp:tablecell>
		
	</asp:tablerow>
	<asp:tablerow runat="server" id="TimeSelectorRow" visible="true" >
		
		<asp:tablecell runat="server" ID="TimeSelectorCell" style="padding-bottom: 20px; padding-left:20px" >
			<asp:label runat="server" id="TimeInstructionsLbl">
				On Saturday, November 19th, 2005, this activity is available at the following times -<br />please select one:
			</asp:label>
			
			<asp:radiobuttonlist runat="server" id="AvailableTimesRBoxList" 
				style="float:right; margin:6px; margin-right:100px" 
				visible="false"
				 tabindex="1"
			/>
			<asp:requiredfieldvalidator runat="server" id="AvailableTimesRBoxReqVal"
			 controltovalidate="AvailableTimesRBoxList"
			 display="static"
			 visible="false"
			 enableclientscript="true"
			 cssclass="ErrorMessages"
			 errormessage="<br /><br /><br />Please select a scheduled time"
			/>
			<br />
			<br />
			<br />
		</asp:tablecell>
		
	</asp:tablerow>
	<asp:tablerow runat="server" id="BottomRow" >
		<asp:tableheadercell runat="server" ID="BottomCell" cssclass="BookingFooterCell" >
			<asp:button runat="server" id="NextStepBtn" text="Next Step >>" 
				 onclick="OnNextStepButtonClicked"
				 enabled="false"
				 tabindex="1"
			/>
		</asp:tableheadercell>
	</asp:tablerow>
</asp:table>
