using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using StarrTech.WebTools;
using StarrTech.WebTools.Controls;
using System.Web.UI;

using ABS;

/// <summary>
///		Summary description for GuestInfoPanel.
/// </summary>
public partial class GuestInfoPanel : System.Web.UI.UserControl {

	#region Properties

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current;
			if ( aBooking == null ) {
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
	}

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainPanel.CssClass ); }
		set { this.MainPanel.CssClass = value; }
	}

	public string HeaderCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}

	public string BodyTableCssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}

	public int BodyTableSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}

	public string FormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}

	public string FormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	public int Seq {
		get {
			object obj = ViewState["Seq"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { ViewState["Seq"] = value; }
	}

	public string HeaderLabel {
		get { return ( this.HeaderLbl.Text ); }
		set { this.HeaderLbl.Text = value; }
	}

	public string PriceCodeUID {
		get {
			object obj = ViewState["PriceCodeUID"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["PriceCodeUID"] = value; }
	}

	public string FirstName {
		get { return ( this.FirstNameBox.Text ); }
		set { this.FirstNameBox.Text = value; }
	}

	public string LastName {
		get { return ( this.LastNameBox.Text ); }
		set { this.LastNameBox.Text = value; }
	}

	public bool ShowCopyOptionsButton {
		get { return ( this.CopyOptionsBtn.Visible ); }
		set { this.CopyOptionsBtn.Visible = value; }
	}

	#endregion

	#region CopyOptions
	// When this button is clicked we need to copy all the Activity Option responses for the first guest 
	// into the option response controls for all the other guests.
	// This is a guest-specific panel, so we need to fire an event that will be handled by our parent BookingGuestsPanel
	public event EventHandler CopyOptions {
		add { Events.AddHandler( CopyOptionsEventKey, value ); }
		remove { Events.RemoveHandler( CopyOptionsEventKey, value ); }
	}
	protected virtual void OnCopyOptions( Object sender, EventArgs args ) {
		EventHandler handler = (EventHandler) Events[CopyOptionsEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object CopyOptionsEventKey = new object();
	#endregion

	#region Event Handlers

	protected override void OnPreRender( EventArgs e ) {
		if ( this.FormLabelCssClass.Length > 0 ) {
			this.FirstNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.LastNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
		}
		if ( this.FormInputCssClass.Length > 0 ) {
			this.FirstNameBox.CssClass = this.FormInputCssClass;
			this.LastNameBox.CssClass = this.FormInputCssClass;
		}
		base.OnPreRender( e );
	}

	#endregion

	#region Methods

	public string ReadOptionResponse( Option option ) {
		foreach ( TableRow row in this.BodyTable.Rows ) {
			foreach ( Control ctl in row.Cells[0].Controls ) {
				if ( ctl.ID != option.ControlID )
					continue;
				switch ( option.ResponseType ) {
					case OptionResponseTypes.PlainText:
						LabeledTextBox tbox = ctl as LabeledTextBox;
						//return ( string.Format( "{0}: {1}", option.QuestionText, tbox.Text ) );
						return ( tbox.Text );
					case OptionResponseTypes.DropDownList:
						LabeledDropDownList lbox = ctl as LabeledDropDownList;
						//return ( string.Format( "{0}: {1}", option.QuestionText, lbox.SelectedValue ) );
						return ( lbox.SelectedValue );
					case OptionResponseTypes.Checkbox:
						CheckBox xbox = ctl as CheckBox;
						//return ( string.Format( "{0}: {1}", option.QuestionText, ( xbox.Checked ? "Yes" : "No" ) ) );
						return ( xbox.Checked ? "Yes" : "No" );
				}
			}
		}
		throw new Exception( "Option ID not found by GuestInfoPanel.ReadOptionResponse" );
	}

	protected override void CreateChildControls() {
		base.CreateChildControls();
		Guest refGuest = null;
		// Try to find an existing Guest with a matching PriceCodeUID and Sequence Number
		if ( this.TheBooking.Guests.Count > 0 ) {
			foreach ( Guest g in this.TheBooking.Guests ) {
				if ( g.Seq == this.Seq && g.PriceCodeUID == this.PriceCodeUID ) {
					refGuest = g;
					this.FirstName = g.FirstName;
					this.LastName = g.LastName;
				}
			}
		}
		// Add controls for any required Options...
		foreach ( Option op in this.TheBooking.Activity.Options ) {
			this.AddOptionControls( op, refGuest );
		}
		// See if this booking isn't the first; if so, populate the GuestPicker control
		Dictionary<string, Guest> guests = Customer.Current.CurrentItinerary.PriorGuests;
		int gCount = guests.Count;
		if ( gCount > 1 ) {
			this.GuestPickerLBox.Items.Clear();
			this.GuestPickerLBox.Visible = true;
			this.GuestPickerLBox.Items.Add( new ListItem( " - Select - ", "" ) );
			foreach ( KeyValuePair<string, Guest> kvp in guests ) {
				this.GuestPickerLBox.Items.Add( new ListItem( kvp.Key, kvp.Key ) );
			}
		} else {
			this.GuestPickerLBox.Visible = false;
		}
	}

	public void AddOptionControls( Option option, Guest refGuest ) {
		TableRow row = new TableRow();
		System.Web.UI.WebControls.TableCell cell = new System.Web.UI.WebControls.TableCell();
		string refValue = "";
		if ( refGuest != null && refGuest.OptionResponses.Count > 0 ) {
			OptionResponse refOR = refGuest.OptionResponses.GetItemByOptionID( option.ID );
			if ( refOR != null ) {
				refValue = refOR.Response;
			}
		}
		switch ( option.ResponseType ) {
			case OptionResponseTypes.PlainText:
				LabeledTextBox tbox = new LabeledTextBox();
				tbox.Label = option.QuestionText + "<span style=\"color:red\">*</span>";
				tbox.ID = option.ControlID; 
				tbox.LabelStyle.CssClass = this.FormLabelCssClass;
				tbox.CssClass = this.FormInputCssClass;
				tbox.Text = refValue;
				tbox.TabIndex = 1;
				cell.Controls.Add( tbox );
				RequiredFieldValidator rval = new RequiredFieldValidator();
				rval.ControlToValidate = tbox.ID;
				rval.Display = ValidatorDisplay.Dynamic;
				rval.EnableClientScript = true;
				rval.CssClass = "ErrorMessages";
				rval.ErrorMessage = string.Format( "Please&nbsp;enter&nbsp;this&nbsp;guest's&nbsp;{0}.", option.QuestionText );
				cell.Controls.Add( rval );
				break;
			case OptionResponseTypes.DropDownList:
				LabeledDropDownList lbox = new LabeledDropDownList();
				lbox.Label = option.QuestionText + "<span style=\"color:red\">*</span>";
				string[] choices = option.ResponseChoices.Split( ',' );
				foreach ( string choice in choices ) {
					lbox.Items.Add( new ListItem( choice ) );
				}
				lbox.ID = option.ControlID;
				lbox.LabelStyle.CssClass = this.FormLabelCssClass;
				lbox.CssClass = this.FormInputCssClass;
				lbox.SelectedText = refValue;
				lbox.TabIndex = 1;
				cell.Controls.Add( lbox );
				break;
			case OptionResponseTypes.Checkbox:
				CheckBox xbox = new CheckBox();
				xbox.Text = option.QuestionText + "<span style=\"color:red\">*</span>";
				xbox.ID = option.ControlID;
				xbox.CssClass = this.FormLabelCssClass;
				xbox.Checked = ( refValue == "Yes" );
				xbox.TabIndex = 1;
				cell.Controls.Add( xbox );
				break;
		}
		cell.ColumnSpan = 2;
		row.Cells.Add( cell );
		BodyTable.Rows.Add( row );
	}

	public void OnGuestPicker( Object sender, EventArgs e ) {
		string gName = this.GuestPickerLBox.SelectedValue;
		if ( string.IsNullOrEmpty( gName ) )
			return;
		string[] names = gName.Split( ',' );
		this.FirstName = names[1].Trim();
		this.LastName = names[0].Trim();
		this.GuestPickerLBox.SelectedIndex = 0;
	}

	#endregion

}
