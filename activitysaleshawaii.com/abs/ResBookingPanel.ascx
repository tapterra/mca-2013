<%@ control language="C#" autoeventwireup="true" codefile="ResBookingPanel.ascx.cs" inherits="ResBookingPanel" %>
<%@ register tagPrefix="abs" tagName="timepicker" src="TimePicker.ascx" %>
<%@ register tagPrefix="abs" tagName="datepicker" src="DatePicker.ascx" %>

<table runat="server" id="MainTable" cellpadding="3">
	<tr style="background:#5092DB" >
		<th >
			<table runat="server" id="TopRowTable" width="600">
				<tr>
					<td valign="top" align="left">
						<asp:label runat="server" id="BookingLbl" text="Reservation:&nbsp;" />
					</td>
					<td align="left" style="width: 90%">
						<asp:label runat="server" id="ActivityTitleLbl" text="Activity Title" />
					</td>
				</tr>
			</table>
		</th>
	</tr>

	<tr>
		<td >
			<table cellpadding="8" width="100%" style="padding-right: 20px">
				<tr >
					<td width="40" rowspan="6">&nbsp;</td>
					<td colspan="2">
						<asp:label runat="server" id="HeaderStepLbl" text="Reservations will be made in the name of:" />
					</td>
				</tr>

				<tr >
					<td width="40%">
						<stic:labeledtextbox runat="server" id="FirstNameBox" width="200" label='First Name<span style="color:red">*</span>' tabindex="1" />
						<asp:requiredfieldvalidator runat="server" id="FirstNameReqVal" controltovalidate="FirstNameBox" display="dynamic" setfocusonerror="true" 
						enableclientscript="true" errormessage="Please enter a value for First Name." cssclass="ErrorMessages" />
					</td>
					<td width="60%">
						<stic:labeledtextbox runat="server" id="LastNameBox" width="250" label='Last Name<span style="color:red">*</span>' tabindex="1" />
						<asp:requiredfieldvalidator runat="server" id="LastNameReqVal" controltovalidate="LastNameBox" display="dynamic" setfocusonerror="true" 
						enableclientscript="true" errormessage="Please enter a value for Last Name." cssclass="ErrorMessages" />
					</td>
				</tr>
				
				<tr >
					<td style="padding-top: 0px">
						<asp:label runat="server" id="PaxCountLbl" text='Number of Guests<span style="color:red">*</span>' />
					</td>
					<td style="padding-top: 0px">
						<asp:textbox runat="server" id="PaxCountBox" columns="3" tabindex="1" />
						<asp:requiredfieldvalidator runat="server" id="Requiredfieldvalidator2" controltovalidate="PaxCountBox" display="dynamic" setfocusonerror="true" 
						 enableclientscript="true" errormessage="Please enter the Number of Guests." cssclass="ErrorMessages" 
						/>
						<asp:comparevalidator id="Comparevalidator1" runat="server" controltovalidate="PaxCountBox" display="dynamic" setfocusonerror="true" 
						 operator="GreaterThan" valuetocompare="0" type="Integer"
						 enableclientscript="true" errormessage="Please enter a numeric value greater than zero for Number of Guests." cssclass="ErrorMessages" 
						/>
					</td>
				</tr>
				
				<tr >
					<td style="padding-top: 0px">
						<asp:label runat="server" id="ResDateLbl" text='Reservation Date<span style="color:red">*</span>' />
					</td>
					<td style="padding-top: 0px">
						<abs:datepicker id="ResDatePicker" runat="server" />
						<asp:requiredfieldvalidator runat="server" id="DatePickerReqVal"
						 controltovalidate="ResDatePicker" 
						 display="dynamic" setfocusonerror="true"
						 enableclientscript="true"
						 errormessage="Please select the date for your reservation. "
						 cssclass="ErrorMessages"
						/>
						<asp:customvalidator runat="server" id="ResDateCustomVal" 
						 controltovalidate="ResDatePicker" 
						 display="dynamic" setfocusonerror="true"
						 enableclientscript="false" onservervalidate="OnValidateResDate"
						 errormessage="Please select a date greater than today's date."
						 cssclass="ErrorMessages"
						/>
					</td>
				</tr>

				<tr >
					<td style="padding-top: 0px">
						<asp:label runat="server" id="PrefTimeLbl" text='Preferred Time<span style="color:red">*</span>' />
					</td>
					<td style="padding-top: 0px">
						<abs:timepicker id="PrefTimePicker" runat="server" />
						<asp:requiredfieldvalidator runat="server" id="Requiredfieldvalidator1"
						 controltovalidate="PrefTimePicker" 
						 display="dynamic" setfocusonerror="true"
						 enableclientscript="true"
						 errormessage="Please select the preferred time for your reservation. "
						 cssclass="ErrorMessages"
						/>
					</td>
				</tr>

				<tr >
					<td style="padding-top: 0px">
						<asp:label runat="server" id="AltTimeLbl" text='Alternate Time<span style="color:red">*</span>' />
					</td>
					<td style="padding-top: 0px">
						<abs:timepicker id="AltTimePicker" runat="server" />
						<asp:requiredfieldvalidator runat="server" id="Requiredfieldvalidator3"
						 controltovalidate="AltTimePicker" 
						 display="dynamic" setfocusonerror="true"
						 enableclientscript="true"
						 errormessage="Please select an alternate time for your reservation. "
						 cssclass="ErrorMessages"
						/>
					</td>
				</tr>

			</table>
		</td>
	</tr>

	<tr>
		<td class="BookingFooterCell">
			<asp:button runat="server" id="NextStepBtn" text="Next Step >>" onclick="OnNextStepButtonClicked" enabled="true" tabindex="1" />
		</td>
	</tr>
	
</table>
