using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CheckoutContactInfoPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainPanel.CssClass ); }
		set { this.MainPanel.CssClass = value; }
	}
	public string HeaderCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}
	public string BodyTableCssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}
	public int BodyTableSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}
	public string FormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}
	public string FormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	#region Data

	public string CustomerFirstName {
		get { return ( this.CustomerFirstNameBox.Text ); }
		set { this.CustomerFirstNameBox.Text = value; }
	}

	public string CustomerLastName {
		get { return ( this.CustomerLastNameBox.Text ); }
		set { this.CustomerLastNameBox.Text = value; }
	}

	public string Telephone {
		get { return ( this.TelephoneBox.Text ); }
		set { this.TelephoneBox.Text = value; }
	}

	public string Email {
		get { return ( this.EmailBox.Text ); }
		set { this.EmailBox.Text = value; }
	}

	#endregion

	#endregion

	protected override void OnPreRender( EventArgs e ) {
		if ( this.FormLabelCssClass.Length > 0 ) {
			this.CustomerFirstNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.CustomerLastNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.TelephoneBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.EmailBox.LabelStyle.CssClass = this.FormLabelCssClass;
		}
		if ( this.FormInputCssClass.Length > 0 ) {
			this.CustomerFirstNameBox.CssClass = this.FormInputCssClass;
			this.CustomerLastNameBox.CssClass = this.FormInputCssClass;
			this.TelephoneBox.CssClass = this.FormInputCssClass;
			this.EmailBox.CssClass = this.FormInputCssClass;
		}
		base.OnPreRender( e );
	}

}
