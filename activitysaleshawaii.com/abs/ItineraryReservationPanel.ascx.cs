using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ItineraryReservationPanel : System.Web.UI.UserControl {

	#region Properties

	public Reservation TheReservation {
		get { return ( this.theReservation ); }
		set { this.theReservation = value; }
	}
	private Reservation theReservation;

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public int CellSpacing {
		get { return ( this.MainTable.CellSpacing ); }
		set { this.MainTable.CellSpacing = value; }
	}

	public int CellPadding {
		get { return ( this.MainTable.CellPadding ); }
		set { this.MainTable.CellPadding = value; }
	}

	public string HeaderTableCssClass {
		get { return ( this.HeaderTable.CssClass ); }
		set { this.HeaderTable.CssClass = value; }
	}

	public string FooterTableCssClass {
		get { return ( this.FooterTable.CssClass ); }
		set { this.FooterTable.CssClass = value; }
	}

	public string LabelCssClass {
		get {
			object obj = ViewState["LabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["LabelCssClass"] = value; }
	}

	public string ValueCssClass {
		get {
			object obj = ViewState["ValueCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ValueCssClass"] = value; }
	}

	public string ActivityNameCssClass {
		get { return ( this.ActivityNameLbl.CssClass ); }
		set { this.ActivityNameLbl.CssClass = value; }
	}

	public string ButtonCssClass {
		get {
			object obj = ViewState["ButtonCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ButtonCssClass"] = value; }
	}

	#endregion

	public int ReservationIndex {
		get {
			object obj = ViewState["ReservationIndex"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["ReservationIndex"] = value; }
	}

	public string ActivityName {
		get { return ( this.ActivityNameLbl.Text ); }
		set { this.ActivityNameLbl.Text = value; }
	}

	public string ResDate {
		get { return ( this.ResDateLbl.Text ); }
		set { this.ResDateLbl.Text = value; }
	}

	public string PaxCount {
		get { return ( this.PaxLbl.Text ); }
		set { this.PaxLbl.Text = value; }
	}

	public string PrefTime {
		get { return ( this.PrefTimeLbl.Text ); }
		set { this.PrefTimeLbl.Text = value; }
	}

	public string AltTime {
		get { return ( this.AltTimeLbl.Text ); }
		set { this.AltTimeLbl.Text = value; }
	}

	public decimal TotalPrice {
		set { this.TotalLbl.Text = string.Format( "US$ {0:F}", value ); }
	}

	#endregion

	private void BuildPanel() {
		this.ActivityName = this.TheReservation.Activity.ItineraryTitle;
		this.ResDate = this.TheReservation.ResDateString;
		this.PaxCount = this.TheReservation.PaxCount.ToString();
		this.PrefTime = this.TheReservation.PrefTime;
		this.AltTime = this.TheReservation.AltTime;
		this.TotalPrice = this.TheReservation.TotalPrice;
		this.RemoveBtn.CommandArgument = this.ReservationIndex.ToString();
		this.ReviseBtn.CommandArgument = this.ReservationIndex.ToString();
	}

	#region Events

	#region OnReviseReservation
	public event ItineraryItemEventHandler ReviseReservation {
		add { Events.AddHandler( ReviseReservationEventKey, value ); }
		remove { Events.RemoveHandler( ReviseReservationEventKey, value ); }
	}
	protected virtual void OnReviseReservation( Object sender, ItineraryItemEventArgs args ) {
		ItineraryItemEventHandler handler = (ItineraryItemEventHandler) Events[ReviseReservationEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object ReviseReservationEventKey = new object();
	#endregion

	#region OnRemoveReservation
	public event ItineraryItemEventHandler RemoveReservation {
		add { Events.AddHandler( RemoveReservationEventKey, value ); }
		remove { Events.RemoveHandler( RemoveReservationEventKey, value ); }
	}
	protected virtual void OnRemoveReservation( Object sender, ItineraryItemEventArgs args ) {
		ItineraryItemEventHandler handler = (ItineraryItemEventHandler) Events[RemoveReservationEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object RemoveReservationEventKey = new object();
	#endregion

	#endregion

	#region Event Handlers

	private void Page_Load( object sender, System.EventArgs e ) {
		this.BuildPanel();
	}

	protected override void OnPreRender( EventArgs e ) {
		if ( !string.IsNullOrEmpty( this.LabelCssClass ) ) {
			this.ActivityLabelLbl.CssClass = this.LabelCssClass;
			this.ActivityNameLbl.CssClass = this.LabelCssClass;
			this.ResDateLabelLbl.CssClass = this.LabelCssClass;
			this.PaxLabelLbl.CssClass = this.LabelCssClass;
			this.PrefTimeLabelLbl.CssClass = this.LabelCssClass;
			this.AltTimeLabelLbl.CssClass = this.LabelCssClass;
			this.TotalLabelLbl.CssClass = this.LabelCssClass;
		}
		if ( !string.IsNullOrEmpty( this.ValueCssClass ) ) {
			this.ResDateLbl.CssClass = this.ValueCssClass;
			this.PaxLbl.CssClass = this.ValueCssClass;
			this.PrefTimeLbl.CssClass = this.ValueCssClass;
			this.AltTimeLbl.CssClass = this.ValueCssClass;
			this.TotalLbl.CssClass = this.ValueCssClass;
		}
		if ( !string.IsNullOrEmpty( this.ButtonCssClass ) ) {
			this.ReviseBtn.CssClass = this.ButtonCssClass;
			this.RemoveBtn.CssClass = this.ButtonCssClass;
		}
		base.OnPreRender( e );
	}

	protected void OnReservationCommand( object sender, CommandEventArgs e ) {
		int resIndex = Convert.ToInt32( e.CommandArgument );
		switch ( e.CommandName ) {
			case "revise":
				this.OnReviseReservation( this, new ItineraryItemEventArgs( resIndex ) );
				break;
			case "remove":
				this.OnRemoveReservation( this, new ItineraryItemEventArgs( resIndex ) );
				break;
		}
	}

	#endregion

}
