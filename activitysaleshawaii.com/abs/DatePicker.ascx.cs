using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[ValidationProperty("SelectedDate")]
public partial class DatePicker : System.Web.UI.UserControl {

	public string Label {
		get { return ( this.LabelLbl.Text ); }
		set { this.LabelLbl.Text = value; }
	}

	public string LabelCssClass {
		get { return ( this.LabelLbl.CssClass ); }
		set { this.LabelLbl.CssClass = value; }
	}

	public string ValueCssClass {
		get { return ( this.MonthLBox.CssClass ); }
		set {
			this.MonthLBox.CssClass = value;
			this.DayLBox.CssClass = value;
			this.YearLBox.CssClass = value;
		}
	}

	public DateTime Value {
		get {
			if ( this.MonthLBox.SelectedIndex == 0 || this.DayLBox.SelectedIndex == 0 || this.YearLBox.SelectedIndex == 0 ) {
				return ( DateTime.MinValue );
			}
			int year = Int32.Parse( this.YearLBox.SelectedValue );
			int month = Int32.Parse( this.MonthLBox.SelectedValue );
			int day = Int32.Parse( this.DayLBox.SelectedValue );
			return ( new DateTime( year, month, day ) );
		}
		set {
			this.EnsureChildControls();
			if ( value == DateTime.MinValue ) {
				this.MonthLBox.SelectedIndex = 0;
				this.DayLBox.SelectedIndex = 0;
				this.YearLBox.SelectedIndex = 0;
			} else {
				this.YearLBox.SelectedValue = value.Year.ToString();
				this.DayLBox.SelectedValue = value.Day.ToString();
				this.MonthLBox.SelectedValue = value.Month.ToString();
			}
		}
	}

	public String SelectedDate {
		get {
			// Assume the selection has been validated
			if ( this.MonthLBox.SelectedIndex == 0 || this.DayLBox.SelectedIndex == 0 || this.YearLBox.SelectedIndex == 0 ) {
				return ( "" );
			}
			return ( string.Format( "{0} {1}, {2}", this.MonthLBox.SelectedItem.Text, this.DayLBox.SelectedValue, this.YearLBox.SelectedValue ) );
		}
		set {
			// We're only using this to reset
			if ( string.IsNullOrEmpty( value ) ) {
				this.MonthLBox.SelectedIndex = 0;
				this.DayLBox.SelectedIndex = 0;
				this.YearLBox.SelectedIndex = 0;
			}
		}
	}

	protected override void CreateChildControls() {
		base.CreateChildControls();
		this.LoadListBoxes();
	}

	private void LoadListBoxes() {
		this.LoadMonthBox();
		this.LoadDayBox();
		this.LoadYearBox();
	}

	private void LoadDayBox() {
		this.DayLBox.Items.Clear();
		this.DayLBox.Items.Add( new ListItem( " -- ", "0" ) );
		for ( int i = 1 ; i < 32 ; i++ ) {
			this.DayLBox.Items.Add( new ListItem( i.ToString(), i.ToString() ) );
		}
	}
	
	private void LoadMonthBox() {
		this.MonthLBox.Items.Clear();
		this.MonthLBox.Items.Add( new ListItem( " -- ", "0" ) );
		this.MonthLBox.Items.Add( new ListItem( "January", "1" ) );
		this.MonthLBox.Items.Add( new ListItem( "February", "2" ) );
		this.MonthLBox.Items.Add( new ListItem( "March", "3" ) );
		this.MonthLBox.Items.Add( new ListItem( "April", "4" ) );
		this.MonthLBox.Items.Add( new ListItem( "May", "5" ) );
		this.MonthLBox.Items.Add( new ListItem( "June", "6" ) );
		this.MonthLBox.Items.Add( new ListItem( "July", "7" ) );
		this.MonthLBox.Items.Add( new ListItem( "August", "8" ) );
		this.MonthLBox.Items.Add( new ListItem( "September", "9" ) );
		this.MonthLBox.Items.Add( new ListItem( "October", "10" ) );
		this.MonthLBox.Items.Add( new ListItem( "November", "11" ) );
		this.MonthLBox.Items.Add( new ListItem( "December", "12" ) );
	}
	
	private void LoadYearBox() {
		this.YearLBox.Items.Clear();
		this.YearLBox.Items.Add( new ListItem( " -- ", "0" ) );
		int cyear = DateTime.Today.Year;
		for ( int i = 0 ; i < 2 ; i++ ) {
			string year = ( cyear + i ).ToString();
			this.YearLBox.Items.Add( new ListItem( year, year ) );
		}
	}

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
		}
	}
}
