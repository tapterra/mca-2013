<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItineraryPanel.ascx.cs" Inherits="ItineraryPanel" classname="ItineraryPanel" %>
<%@ register tagprefix="abs" tagname="bookingpanel" src="itinerarybookingpanel.ascx" %>
<%@ register tagprefix="abs" tagname="respanel" src="itineraryreservationpanel.ascx" %>
<%@ reference virtualpath="itinerarybookingpanel.ascx" %>
<%@ reference virtualpath="itineraryreservationpanel.ascx" %>
<%@ import namespace="ABS" %>

<asp:table runat="server" id="MainTable" >
	
	<asp:tablerow runat="server" id="HeaderRow" >
		<asp:tablecell runat="server" id="HeaderCell">
			<asp:label runat="server" id="HeaderLbl" text="Your Itinerary..." />
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="MessageRow" >
		<asp:tablecell runat="server" id="MessageCell" cssclass="ErrorMsgSubPanel" >
			<asp:label runat="server" id="MessageLbl" text="Space For Rent" />
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="BodyRow" >
		<asp:tablecell runat="server" id="BodyCell" />
	</asp:tablerow>

	<asp:tablerow runat="server" id="PromoCodeRow" >
		<asp:tablecell runat="server" id="PromoCodeCell" cssclass="ItinFooter">
				<table class="ItinFooter">
					<tr>
						<td>
							<asp:label id="PromoCodeLbl" runat="server" text="Enter Promo Code Here:" cssclass="CheckoutPanelHeader" />
						</td>
						<td>
							<asp:textbox id="PromoCodeBox" runat="server" width="200px" cssclass="FormValue" tabindex="1" />
						</td>
						<td>
							<asp:button runat="server" id="PromoUpdateBtn" text="Update" 
							 onclick="OnPromoCodeChg"
							 tabindex="1"
							/>
						</td>
					</tr>
				</table>
		</asp:tablecell>
	</asp:tablerow>

	<asp:tablerow runat="server" id="FooterRow" >
		<asp:tablecell runat="server" id="FooterCell">
			
			<asp:table runat="server" id="FooterTable" width="100%" >

				<asp:tablerow id="TotalRow" runat="server" visible='<%# !Website.Current.IsNoPaymentSite %>' >
					<asp:tablecell id="TotalLabelCell" runat="server" >
						<asp:label runat="server" id="TotalLabelLbl" text="Grand Total" />
					</asp:tablecell>
					<asp:tablecell id="TotalCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="TotalLbl" text="US$ 567.89" />
					</asp:tablecell>
				</asp:tablerow>
								
				<asp:tablerow id="ButtonRow" runat="server">
					<asp:tablecell id="InfoCell" runat="server" horizontalalign="right" />
					<asp:tablecell id="ButtonCell" runat="server" horizontalalign="right" >
						<asp:button runat="server" id="ContinueBtn" text="Continue Shopping" 
						 onclick="OnContinueClicked"
						 tabindex="1"
						/>
						<asp:button runat="server" id="CheckoutBtn" text="Check Out >>" 
						 onclick="OnCheckoutClicked"
						 tabindex="1"
						/>
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			
		</asp:tablecell>
	</asp:tablerow>

</asp:table>
