<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckoutContactInfoPanel.ascx.cs" Inherits="CheckoutContactInfoPanel" %>

<asp:panel runat="server" id="MainPanel" >
	
	<asp:label runat="server" id="HeaderLbl" text="Contact Information" />
	
	<asp:table runat="server" id="BodyTable" >
		
		<asp:tablerow id="Tablerow2" runat="server">
			<asp:tablecell id="Tablecell2" runat="server" columnspan="2" width="40%" >
				<stic:labeledtextbox runat="server" id="CustomerFirstNameBox" 
					width="96%"
					label='Your First Name<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="Requiredfieldvalidator1"
				 controltovalidate="CustomerFirstNameBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;your&nbsp;first&nbsp;name."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
			<asp:tablecell id="Tablecell3" runat="server" columnspan="3" width="60%" >
				<stic:labeledtextbox runat="server" id="CustomerLastNameBox" 
					width="96%"
					label='Your Last Name<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="Requiredfieldvalidator2"
				 controltovalidate="CustomerLastNameBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;your&nbsp;last&nbsp;name."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>
		
		<asp:tablerow id="Tablerow1" runat="server">
			<asp:tablecell id="Tablecell1" runat="server" columnspan="2" width="40%" >
				<stic:labeledtextbox runat="server" id="TelephoneBox" 
					width="96%"
					label='Telephone<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="TelephoneReqVal"
				 controltovalidate="TelephoneBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;your&nbsp;telephone&nbsp;number."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
			<asp:tablecell id="Tablecell5" runat="server" columnspan="3" width="60%" >
				<stic:labeledtextbox runat="server" id="EmailBox" 
					width="96%"
					label='Email Address<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="EmailReqVal"
				 controltovalidate="EmailBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;your&nbsp;email&nbsp;address."
				 cssclass="ErrorMessages"
				/>
				<stic:emailvalidator runat="server" id="EmailBoxValidator" controltovalidate="EmailBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;a&nbsp;valid&nbsp;email&nbsp;address."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>
		
	</asp:table>
	
</asp:panel>
