<%@ Control Language="c#" AutoEventWireup="true" CodeFile="BookingHotelPaxCountPanel.ascx.cs" Inherits="BookingHotelPaxCountPanel" classname="BookingHotelPaxCountPanel" %>

<asp:table runat="server" id="MainTable" cellpadding="3" width="100%">
	<asp:tablerow runat="server" id="StepRow" >
		
		<asp:tablecell runat="server" ID="StepCell" style="padding-bottom: 0px" >
			<asp:label runat="server" id="HeaderStepLbl" text="Step&nbsp;2:&nbsp;&nbsp;&nbsp;&nbsp;" />
			<asp:label runat="server" id="HeaderLbl" text="Describe yourself and your party:" />
		</asp:tablecell>
		
	</asp:tablerow>
	<asp:tablerow runat="server" id="BodyRow" visible="true" >
		
		<asp:tablecell runat="server" ID="BodyCell" style="padding-bottom: 0px; padding-left:80px" >
			
			<asp:table runat="server" id="BodyTable" cellspacing="6" cellpadding="0" 
			 width="420"
			>
				<asp:tablerow runat="server" >
					<asp:tablecell runat="server" style="padding-right: 20px" >
						<stic:labeledtextbox runat="server" id="FirstNameBox" 
						 width="120"
						 label='Your First Name<span style="color:red">*</span>'
							tabindex="1"
						/>
					</asp:tablecell>
					<asp:tablecell runat="server" >
						<stic:labeledtextbox runat="server" id="LastNameBox" 
						 width="250"
						 label='Your Last Name<span style="color:red">*</span>'
							tabindex="1"
						/>
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="Tablerow1" runat="server">
					<asp:tablecell id="Tablecell1" runat="server" columnspan="2" style="padding-top:0px">
					<asp:requiredfieldvalidator runat="server" id="FirstNameReqVal"
					 controltovalidate="FirstNameBox" 
					 display="dynamic" setfocusonerror="true"
					 enableclientscript="true"
					 errormessage="Please enter your first name."
					 cssclass="ErrorMessages"
					/>
					<asp:requiredfieldvalidator runat="server" id="LastNameReqVal"
					 controltovalidate="LastNameBox" 
					 display="dynamic" setfocusonerror="true"
					 enableclientscript="true"
					 errormessage="<br />Please enter your last name."
					 cssclass="ErrorMessages"
					/>
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow runat="server">
					<asp:tablecell runat="server" columnspan="2" style="padding-top:0px">
						<asp:label runat="server" id="PaxCountsHeaderLbl" text='Number of Guests<span style="color:red">*</span>' />
						<asp:customvalidator runat="server" id="PaxQtyCustomVal"
						 OnServerValidate="PaxQtyValidate"
						 display="dynamic" setfocusonerror="true"
						 errormessage="<br/>Please enter the number of guest(s) attending."
						 cssclass="ErrorMessages"
						/>
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			<table width="420" style="margin-top: 6px" cellspacing="6" cellpadding="0" >
				<tr>
					<td colspan="2" style="padding-top:0px" >
						<asp:label runat="server" id="HotelListBoxLbl" text='Hotel<span style="color:red">*</span>' /><br />
						<asp:listbox runat="server" id="HotelListBox" width="100%" 
							style="border: 1px solid blue"
							selectionmode="single" 
							tabindex="1"
						/>
						<asp:customvalidator runat="server" id="HotelNameCustomVal"
						 OnServerValidate="HotelNameValidate"
						 display="dynamic" setfocusonerror="true"
						 errormessage="<br />Please select or enter the name of the hotel where you will be staying."
						 cssclass="ErrorMessages"
						/>
					</td>
				</tr>
				<tr>
					<td width="100" >
						<asp:checkbox runat="server" id="OtherHotelXBox" text="Other&nbsp;Hotel:&nbsp;" 
							tabindex="1"
						/>
					</td>
					<td>
						<asp:textbox runat="server" id="OtherHotelBox" width="300" 
							tabindex="1"
						/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:label runat="server" id="LblOtherTxt" style="font-family:'Arial';font-size:10pt;" text='If you checked &quot;Other&quot;, we will contact you by separate email regarding activity pick up time and location for those activities that include transportation' /><br />
					</td>
				</tr>
				
<%--
				<tr>
					<td width="80" >
						<asp:label runat="server" id="RoomNoLbl" text="Room #:&nbsp;" />
					</td>
					<td>
						<asp:textbox runat="server" id="RoomNoBox" width="100" />
					</td>
				</tr>
--%>
			</table>
			
		</asp:tablecell>
		
	</asp:tablerow>
	<asp:tablerow runat="server" id="BottomRow" >
		
		<asp:tablecell runat="server" ID="BottomCell" cssclass="BookingFooterCell" >
			<asp:button runat="server" id="NextStepBtn" text="Next Step >>" 
				onclick="OnNextStepButtonClicked"
				enabled="true"
				tabindex="1"
			/>
		</asp:tablecell>
		
	</asp:tablerow>
</asp:table>
