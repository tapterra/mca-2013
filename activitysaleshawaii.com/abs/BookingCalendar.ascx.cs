using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using ABS;

/// <summary>
///	Summary description for BookingCalendar.
/// </summary>
public partial class BookingCalendar : System.Web.UI.UserControl {

	#region Properties

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
		set { this.ViewState["ActivityAID"] = value; }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current as Booking;
			if ( aBooking == null ) {
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
	}

	public Unit Width {
		get { return ( this.MainTable.Width ); }
		set { this.MainTable.Width = value; }
	}

	protected int VisibleMonth {
		get { return ( this.MonthListBox.SelectedIndex + 1 ); }
		set { this.MonthListBox.SelectedIndex = value - 1; }
	}

	protected int VisibleYear {
		get {
			if ( string.IsNullOrEmpty( this.YearListBox.SelectedValue ) ) {
				return ( DateTime.Today.Year );
			}
			return ( Convert.ToInt32( this.YearListBox.SelectedValue ) ); 
		}
		set { this.YearListBox.SelectedValue = value.ToString(); }
	}

	public DateTime FirstDate {
		get {
			DateTime result = new DateTime( Website.Current.BookingStartYr, Website.Current.BookingStartMo, 1 );
			if ( result < DateTime.Today ) {
				result = DateTime.Today;
			}
			return ( result );
		}
	}

	public DateTime LastDate {
		get {
			//DateTime result = new DateTime( Website.Current.BookingEndYr, Website.Current.BookingEndMo + 1, 1 ).AddDays( -1 );
			DateTime result = new DateTime( Website.Current.BookingEndYr, Website.Current.BookingEndMo, 1 );
			return ( result );
		}
	}

	public DateTime VisibleDate {
		get { return ( new DateTime( this.VisibleYear, this.VisibleMonth, 1 ) ); }
		set {
			this.VisibleMonth = value.Month;
			this.VisibleYear = value.Year;
			this.ResetCalendar();
		}
	}

	public DateTime? SelectedDate {
		get { return ( 
			this.Calendar.SelectedDate == DateTime.MinValue ? (DateTime?) null : (DateTime?) this.Calendar.SelectedDate 
		); }
		set { this.Calendar.SelectedDate = ( value.HasValue ? value.Value.Date : DateTime.MinValue ); }
	}

	#endregion

	#region Events

	#region VisibleMonthChanged
	public event MonthChangedEventHandler VisibleMonthChanged {
		add { Events.AddHandler( VisibleMonthChangedEventKey, value ); }
		remove { Events.RemoveHandler( VisibleMonthChangedEventKey, value ); }
	}
	protected virtual void OnVisibleMonthChanged( Object sender, MonthChangedEventArgs args ) {
		MonthChangedEventHandler handler = (MonthChangedEventHandler) Events[VisibleMonthChangedEventKey];
		if ( handler != null ) handler( sender, args );
	}
	private static readonly object VisibleMonthChangedEventKey = new object();
	#endregion

	#region DateSelectionChanged
	public event EventHandler DateSelectionChanged {
		add { Events.AddHandler( DateSelectionChangedEventKey, value ); }
		remove { Events.RemoveHandler( DateSelectionChangedEventKey, value ); }
	}
	protected virtual void OnDateSelectionChanged( Object sender, EventArgs args ) {
		EventHandler handler = (EventHandler) Events[DateSelectionChangedEventKey];
		if ( handler != null ) handler( sender, args );
	}
	private static readonly object DateSelectionChangedEventKey = new object();
	#endregion

	#endregion

	#region Methods

	protected void ReloadActivitySchedule() {
		DateTime startDate = new DateTime( this.VisibleYear, this.VisibleMonth, 1 );
		DateTime endDate = startDate.AddMonths( 1 ).AddDays( -1 );
		this.TheBooking.Activity.ScheduleDateTimes.ReloadSchedule( startDate, endDate );
	}

	protected bool IsDateBookable( DateTime CalDate ) {
		if ( CalDate < this.FirstDate || CalDate > this.LastDate ) {
			return ( false );
		}
		foreach ( ScheduleDateTime sdt in this.TheBooking.Activity.ScheduleDateTimes ) {
			if ( sdt.Value.Date.Equals( CalDate.Date ) )
				return ( sdt.SeatsAvailable > 0 );
		}
		return ( false );
	}

	protected void DayRender( Object source, DayRenderEventArgs e ) {
		if ( e.Day.IsOtherMonth ) {
			// Clear the link from the days outside the visible month.
			this.OtherMonthCell( e );
		} else if ( e.Day.Date <= DateTime.Today ) {
			// Clear the link from the days prior to today
			this.DisableCell( e );
		} else {
			if ( !this.IsDateBookable( e.Day.Date ) ) {
				this.UnavailableCell( e );
			}
		}
	}

	private void UnavailableCell( DayRenderEventArgs e ) {
		//TODO: Make this a CssClass property
		//e.Cell.BackColor = Color.White;
		//e.Cell.ForeColor = Color.White;
		//e.Cell.Style["background-image"] = "url(controls/images/x.gif)";
		//e.Cell.Style["background-repeat"] = "no-repeat";
		//e.Cell.Style["background-position"] = "center center";
		//e.Cell.Controls.Clear();
		//e.Cell.Controls.Add( new LiteralControl( "&nbsp;" ) );
		this.DisableCell( e );
	}

	private void DisableCell( DayRenderEventArgs e ) {
		e.Cell.BackColor = Color.LightGray;
		e.Cell.ForeColor = Color.Gray;
		e.Cell.Controls.Clear();
		e.Cell.Controls.Add( new LiteralControl( e.Day.DayNumberText ) );
	}

	private void OtherMonthCell( DayRenderEventArgs e ) {
		//TODO: Make this a CssClass property
		e.Cell.BackColor = Color.LightGray; //this.Calendar.OtherMonthDayStyle.BackColor;
		//e.Cell.ForeColor = Color.LightGray; //this.Calendar.OtherMonthDayStyle.ForeColor;
		e.Cell.Controls.Clear();
		//e.Cell.Controls.Add( new LiteralControl( e.Day.DayNumberText ) );
	}

	private bool isFirstMonthVisible {
		get { return ( this.VisibleDate.Year == this.FirstDate.Year && this.VisibleDate.Month == this.FirstDate.Month ); }
	}

	private bool isLastMonthVisible {
		get { return ( this.VisibleDate.Year == this.LastDate.Year && this.VisibleDate.Month == this.LastDate.Month ); }
	}

	public void ResetCalendar() {
		if ( this.Calendar.VisibleDate != this.VisibleDate ) {
			this.ReloadActivitySchedule();
			this.OnVisibleMonthChanged( this.Calendar, new MonthChangedEventArgs( this.VisibleDate, this.Calendar.VisibleDate ) );
		}
		this.Calendar.VisibleDate = this.VisibleDate;
		this.Calendar.PrevMonthText = this.isFirstMonthVisible ? "" : "<<";
		this.Calendar.NextMonthText = isLastMonthVisible ? "" : ">>";
	}

	protected void PrevMonth( object sender, System.EventArgs e ) {
		if ( ! this.isFirstMonthVisible ) {
			if ( this.VisibleMonth > 1 ) {
				this.VisibleMonth--;
			} else {
				if ( this.YearListBox.SelectedIndex == 0 ) {
					return;
				}
				this.VisibleMonth = 12;
				this.VisibleYear--;
			}
			this.ResetCalendar();
		} 
	}

	protected void NextMonth( object sender, System.EventArgs e ) {
		if ( !this.isLastMonthVisible ) {
			if ( this.VisibleMonth < 12 ) {
				this.VisibleMonth++;
			} else {
				if ( this.YearListBox.SelectedIndex == this.YearListBox.Items.Count - 1 ) {
					return;
				}
				this.VisibleMonth = 1;
				this.VisibleYear++;
			}
			this.ResetCalendar();
		}
	}

	protected void MonthChanged( object sender, System.EventArgs e ) {
		this.ResetCalendar();
	}

	public void InitCalendar( DateTime? SelectedDate ) {
		DateTime startDay = DateTime.Today.AddDays( 1 );
		this.Calendar.TodaysDate = startDay;
		if ( startDay < this.FirstDate ) {
			startDay = this.FirstDate;
		}
		for ( int year = startDay.Year ; year <= this.LastDate.Year ; year++ ) {
			this.YearListBox.Items.Add( new ListItem( year.ToString(), year.ToString() ) );
		} 
		if ( SelectedDate.HasValue ) {
			this.VisibleDate = SelectedDate.Value;
		} else {
			this.VisibleDate = startDay;
		}
		this.SelectedDate = SelectedDate;
	}

	#endregion

}
