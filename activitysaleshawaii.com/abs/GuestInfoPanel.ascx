<%@ control language="c#" autoeventwireup="false" codefile="GuestInfoPanel.ascx.cs" inherits="GuestInfoPanel" classname="GuestInfoPanel" %>

<asp:panel runat="server" id="MainPanel" >
	
	<div style="float:right">
		<asp:dropdownlist runat="server" id="GuestPickerLBox" visible="false" 
			onselectedindexchanged="OnGuestPicker"
			autopostback="true"
			font-names="Verdana"
			font-size="8pt"
			font-bold="false"
		 tabindex="1"
		/>
	</div>
	<asp:label runat="server" id="HeaderLbl" />
	
	<asp:table runat="server" id="BodyTable" >
		<asp:tablerow runat="server">
			<asp:tablecell runat="server" width="40%">
				<stic:labeledtextbox runat="server" id="FirstNameBox" 
					width="120"
					label='First Name<span style="color:red">*</span>'
				 tabindex="1"
				/>
			</asp:tablecell>
			<asp:tablecell runat="server" width="60%">
				<stic:labeledtextbox runat="server" id="LastNameBox" 
					width="220"
					label='Last Name<span style="color:red">*</span>'
				 tabindex="1"
				/>
			</asp:tablecell>
		</asp:tablerow>
		<asp:tablerow id="Tablerow1" runat="server">
			<asp:tablecell id="Tablecell1" runat="server" columnspan="2" style="padding:0px;" >
				<asp:requiredfieldvalidator runat="server" id="FirstNameReqVal"
				 controltovalidate="FirstNameBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please enter this guest's First Name."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>
		<asp:tablerow id="Tablerow2" runat="server">
			<asp:tablecell id="Tablecell2" runat="server" columnspan="2" style="padding:0px;" >
				<asp:requiredfieldvalidator runat="server" id="LastNameReqVal"
				 controltovalidate="LastNameBox" 
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please enter this guest's Last Name."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>
	</asp:table>
	
	<asp:button runat="server" id="CopyOptionsBtn" visible="false" style="float:right"
	 text="Copy to Other Guests"
	 tooltip="Copy the responses for this guest to the additional guests below."
	 onclick="OnCopyOptions"
	 causesvalidation="false"
	 cssclass="CopyButton"
	 tabindex="1"
	/>
	
</asp:panel>
