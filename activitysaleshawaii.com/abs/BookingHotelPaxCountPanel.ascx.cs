using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ABS;

/// <summary>
///		Summary description for BookingHotelPaxCountPanel.
/// </summary>
public partial class BookingHotelPaxCountPanel : System.Web.UI.UserControl {

	#region Properties

	public string ActivityAID {
		get { return ( this.ViewState["ActivityAID"] as string ); }
		set { this.ViewState["ActivityAID"] = value; }
	}

	public Booking TheBooking {
		get {
			Booking aBooking = Booking.Current;
			if ( aBooking == null ) {
				Booking.Current = new Booking( this.ActivityAID );
				aBooking = Booking.Current;
			}
			return ( aBooking );
		}
	}

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public string HeaderStepCssClass {
		get { return ( this.HeaderStepLbl.CssClass ); }
		set { this.HeaderStepLbl.CssClass = value; }
	}

	public string HeaderCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}

	public string BodyTableCssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}

	public int BodyTableSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}

	public string NextButtonCssClass {
		get { return ( this.NextStepBtn.CssClass ); }
		set { this.NextStepBtn.CssClass = value; }
	}

	public string FormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}

	public string FormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	#region Data Properties

	public string CustomerLastName {
		get { return ( this.LastNameBox.Text ); }
		set { this.LastNameBox.Text = value; }
	}

	public string CustomerFirstName {
		get { return ( this.FirstNameBox.Text ); }
		set { this.FirstNameBox.Text = value; }
	}

	public string SelectedHotelAID {
		get {
			if ( this.OtherHotelXBox.Checked ) {
				return ( "" );
			}
			if ( this.HotelListBox.SelectedItem == null ) {
				return ( "" );
			}
			return ( this.HotelListBox.SelectedValue );
		}
		set { 
			//this.HotelListBox.SelectedValue = value; 
			this.assignedHotelAID = value;
		}
	}
	private string assignedHotelAID = "";

	public string OtherHotelName {
		get {
			if ( this.OtherHotelXBox.Checked ) {
				return ( this.OtherHotelBox.Text );
			}
			//if ( string.IsNullOrEmpty( this.SelectedHotelAID ) ) {
			//  return ( this.OtherHotelBox.Text );
			//}
			return ( "" );
		}
		set {
			if ( string.IsNullOrEmpty( value ) ) {
				this.OtherHotelXBox.Checked = false;
				this.OtherHotelBox.Text = "";
			} else {
				this.OtherHotelXBox.Checked = true;
				this.OtherHotelBox.Text = value;
			}
		}
	}

	//public string HotelRoomNo {
	//  get { return ( this.RoomNoBox.Text ); }
	//  set { this.RoomNoBox.Text = value; }
	//}

	public TableRowCollection BodyTableRows {
		get { return ( this.BodyTable.Rows ); }
	}

	#endregion

	#endregion

	#region Events

	#region NextStepButtonClicked
	public event EventHandler NextStepButtonClicked {
		add { Events.AddHandler( NextStepButtonClickedEventKey, value ); }
		remove { Events.RemoveHandler( NextStepButtonClickedEventKey, value ); }
	}
	protected virtual void OnNextStepButtonClicked( Object sender, EventArgs args ) {
		this.EnsureChildControls();
		if ( this.Page.IsValid ) {
			this.ReadHotelPaxResponses();
			EventHandler handler = (EventHandler) Events[NextStepButtonClickedEventKey];
			if ( handler != null )
				handler( sender, args );
		}
	}
	private static readonly object NextStepButtonClickedEventKey = new object();
	#endregion

	#endregion

	#region Methods

	protected void ReadHotelPaxResponses() {
		// Collect the user's responses ...
		Itinerary itin = Customer.Current.CurrentItinerary;
		itin.CustomerLastName = this.CustomerLastName;
		itin.CustomerFirstName = this.CustomerFirstName;
		this.TheBooking.HotelAID = this.SelectedHotelAID;
		this.TheBooking.OtherHotelName = this.OtherHotelName;
		// Step thru each qty-textbox and extract the qty...
		foreach ( TableRow row in this.BodyTableRows ) {
			if ( row.Cells.Count < 2 ) continue;
			TableCell cell = row.Cells[1];
			TextBox tbox = cell.Controls[0] as TextBox;
			if ( tbox != null ) {
				string priceCodeUID = tbox.ID.Replace( "_Qty", "" );
				ScheduleDateTime dt = this.TheBooking.SelectedScheduleDateTime;
				// Step thru each PriceCode for the current booking-date to find the one that matches our qty
				foreach ( PriceCode pc in this.TheBooking.Activity.ScheduleDateTimes[dt.Value].PriceCodes ) {
					if ( pc.UID == priceCodeUID ) {
						// We have the qty for this PriceCode...
						int qty = Convert.ToInt32( tbox.Text );
						// Add/update the matching PaxCount instance...
						if ( !this.TheBooking.PaxCounts.Contains( pc.UID ) ) {
							this.TheBooking.PaxCounts.Add( new PaxCount( pc, qty ) );
						} else {
							this.TheBooking.PaxCounts[pc.UID].PaxQty = qty;
						}
						// Make sure we have enough Guests...
						for ( int p = 1 ; p <= qty ; p++ ) {
							Guest g = this.TheBooking.Guests.GetItem( pc.UID, p );
							if ( g == null ) {
								this.TheBooking.Guests.Add( new Guest( this.TheBooking, pc.UID, p ) );
							}
						}
					}
				}
			}
		}
		// Step through each Guest to make sure we don't have too many
		for ( int g = 0 ; g < this.TheBooking.Guests.Count ; ) {
			Guest refG = this.TheBooking.Guests[g];
			// Find the PaxCount instance for this Guest's PriceCode
			if ( this.TheBooking.PaxCounts.Contains( refG.PriceCodeUID ) ) {
				PaxCount pc = this.TheBooking.PaxCounts[refG.PriceCodeUID];
				if ( refG.Seq > pc.PaxQty ) {
					// Qty for this pricecode was reduced - this Guest no longer booked
					this.TheBooking.Guests.Remove( refG );
					continue;
				}
			} else {
				// No matching PaxCount found - waste it
				this.TheBooking.Guests.Remove( refG );
				continue;
			}
			g++;
		}
	}

	protected override void CreateChildControls() {
		base.CreateChildControls();
		this.BuildPaxCountControls();
	}

	protected void BindHotelsList() {
		if ( !this.IsPostBack ) {
			using ( IVoucherService provider = VoucherServiceFactory.NewVoucherService() ) {
				if ( provider.Hotels == null ) {
					return;
				}
				this.HotelListBox.Items.Clear();
				this.HotelListBox.SelectedIndex = -1;
				// RKP: To support the new site-specific special region-code mechanism, see if the current Website
				// RKP: has a SiteRegionAid value - if it does, use that code instead of the Activity.RegionAID:
				string regionAid = Website.Current.SiteRegionAid;
				if ( string.IsNullOrEmpty( regionAid ) ) {
					regionAid = this.TheBooking.Activity.RegionAID;
				}
				this.HotelListBox.DataSource = provider.RegionHotels( regionAid ).Values;
			}
			this.HotelListBox.DataTextField = "Name";
			this.HotelListBox.DataValueField = "AID";
			this.HotelListBox.DataBind();
			if ( this.assignedHotelAID != "" ) {
				if ( this.HotelListBox.Items.FindByValue( this.assignedHotelAID ) != null ) {
					this.HotelListBox.SelectedValue = this.assignedHotelAID;
				} 
			}
		}
	}

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.Page.IsPostBack ) {
			this.BindHotelsList();
		}
	}

	protected override void OnPreRender( EventArgs e ) {
		if ( this.FormLabelCssClass.Length > 0 ) {
			this.FirstNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.LastNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.PaxCountsHeaderLbl.CssClass = this.FormLabelCssClass;
			this.HotelListBoxLbl.CssClass = this.FormLabelCssClass;
			this.OtherHotelXBox.CssClass = this.FormLabelCssClass;
			//this.RoomNoLbl.CssClass = this.FormLabelCssClass;
		}
		if ( this.FormInputCssClass.Length > 0 ) {
			this.FirstNameBox.CssClass = this.FormInputCssClass;
			this.LastNameBox.CssClass = this.FormInputCssClass;
			this.HotelListBox.CssClass = this.FormInputCssClass;
			this.OtherHotelBox.CssClass = this.FormInputCssClass;
			this.HotelListBox.CssClass = this.FormInputCssClass;
			//this.RoomNoBox.CssClass = this.FormInputCssClass;
		}
		base.OnPreRender( e );
	}

	protected void BuildPaxCountControls() {
		ScheduleDateTime dt = this.TheBooking.SelectedScheduleDateTime;
		foreach ( PriceCode pc in this.TheBooking.Activity.ScheduleDateTimes[dt.Value].PriceCodes ) {
			// If this is a revision to a previous booking, we'll want to restore the qty's...
			int qty = 0;
			// Find the matching PaxCount
			try {
				PaxCount pax = this.TheBooking.PaxCounts[pc.UID];
				if ( pax != null ) qty = pax.PaxQty;
			} catch { }
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			// Add the price-code label
			Label pcLabel = new Label();
			pcLabel.Text = pc.Label;
			pcLabel.CssClass = this.FormInputCssClass;
			cell.Controls.Add( pcLabel );
			row.Cells.Add( cell );
			cell = new TableCell();
			// Add the qty textbox
			TextBox qtyBox = new TextBox();
			qtyBox.Text = "0";
			qtyBox.ID = string.Format( "{0}_Qty", pc.UID );
			qtyBox.CssClass = this.FormInputCssClass;
			qtyBox.Width = new Unit( "60" );
			qtyBox.Text = qty.ToString();
			qtyBox.TabIndex = 1;
			cell.Controls.Add( qtyBox );
			// Add the price label
			Label pcPrice = new Label();
			if ( Website.Current.IsNoPaymentSite ) {
				pcPrice.Text = "&nbsp;";
			} else {
				pcPrice.Text = string.Format( "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;US$ {0:F} ea.", pc.Price );
			}
			pcPrice.CssClass = this.FormInputCssClass;
			cell.Controls.Add( pcPrice );
			row.Cells.Add( cell );
			this.BodyTable.Rows.Add( row );
		}
	}

	protected void HotelNameValidate( object source, ServerValidateEventArgs args ) {
		try {
			args.IsValid = !( string.IsNullOrEmpty( this.SelectedHotelAID ) && string.IsNullOrEmpty( this.OtherHotelName ) );
		} catch {
			args.IsValid = false;
		}
	}

	protected void PaxQtyValidate( object source, ServerValidateEventArgs args ) {
		try {
			// Test to see if at least one of the qty-boxes is > 0
			int totalQty = 0;
			foreach ( TableRow row in this.BodyTableRows ) {
				if ( row.Cells.Count < 2 )
					continue;
				TableCell cell = row.Cells[1];
				TextBox tbox = cell.Controls[0] as TextBox;
				if ( tbox != null ) {
					string priceCodeUID = tbox.ID.Replace( "_Qty", "" );
					ScheduleDateTime dt = this.TheBooking.SelectedScheduleDateTime;
					foreach ( PriceCode pc in this.TheBooking.Activity.ScheduleDateTimes[dt.Value].PriceCodes ) {
						if ( pc.UID == priceCodeUID ) {
							int qty = Convert.ToInt32( tbox.Text );
							totalQty += qty;
						}
					}
				}
			}
			args.IsValid = totalQty > 0;
		} catch {
			args.IsValid = false;
		}
	}

	#endregion

}
