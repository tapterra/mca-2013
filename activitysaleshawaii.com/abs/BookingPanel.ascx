<%@ control language="c#" autoeventwireup="true" codefile="BookingPanel.ascx.cs" inherits="BookingPanel" classname="BookingPanel" %>
<%@ register tagprefix="abs" tagname="datetimepanel" src="BookingDateTimePanel.ascx" %>
<%@ register tagprefix="abs" tagname="hotelpaxcountpanel" src="BookingHotelPaxCountPanel.ascx" %>
<%@ register tagprefix="abs" tagname="guestspanel" src="BookingGuestsPanel.ascx" %>

<asp:table runat="server" id="MainTable" cellpadding="3" >
	
	<asp:tablerow runat="server" id="TopRow" backcolor="#5092DB" >
		<asp:tableheadercell runat="server" ID="TopCell" >
			<table width="100%">
				<tr>
					<td rowspan="2" valign="top" align="left" >
						<asp:label runat="server" id="BookingLbl"
							text="Booking:&nbsp;" 
						/>
					</td>
					<td align="left" style="width:90%" >
						<asp:label runat="server" ID="ActivityTitleLbl" 
							text="Activity Title"
						/>
					</td>
				</tr>
				<tr>
					<td align="left" >
						<asp:label runat="server" id="BookedDateTimeLbl"
							text="&nbsp;" 
							visible="true"
						/>
					</td>
				</tr>
			</table>
		</asp:tableheadercell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="BodyRow" >
		<asp:tablecell runat="server" ID="BodyCell" >
			
			<asp:multiview runat="server" id="Wizard" activeviewindex="0">
				
				<asp:view runat="server" id="DateTimeView">
					
					<abs:datetimepanel runat="server" id="DateTimePanel"
					 cssclass="BookingStepPanel" 
					 headerstepcssclass="BookingStepHeader"
					 headerdatecssclass="BookingHeader"
					 timeinstructionscssclass="BookingInstructions"
					 timeoptionscssclass="BookingHeader"
					 nextbuttoncssclass="StepButton"
					 ondateselectionchanged="OnDateSelectionChanged"
					 onnextstepbuttonclicked="OnDateTimeSelectCompleted"
					/>
					
				</asp:view>
				
				<asp:view runat="server" id="HotelPaxCountView">
					
					<abs:hotelpaxcountpanel runat="server" id="HotelPaxCountPanel"
					 formlabelcssclass="FormLabel"
					 forminputcssclass="FormInput"
					 headerstepcssclass="BookingStepHeader"
					 headercssclass="BookingHeader" 
					 nextbuttoncssclass="StepButton"
					 onnextstepbuttonclicked="OnHotelSelectCompleted"
					/>
					
				</asp:view>
			
				<asp:view runat="server" id="GuestsInfoView">
					
					<abs:guestspanel runat="server" id="GuestsPanel"
					 cssclass="BookingStepPanel" 
					 headerguestcssclass="BookingHeader"
					 headerinstructionscssclass="BookingInstructions"
					 headerstepcssclass="BookingStepHeader"
					 nextbuttoncssclass="StepButton"
					 guestcssclass="GuestInfoPanel" 
					 guestheadercssclass="GuestInfoHeader" 
					 guestbodytablecssclass="GuestInfoBodyTable"
					 guestbodytablespacing="0"
					 guestformlabelcssclass="FormLabel"
					 guestforminputcssclass="FormInput"
					 onnextstepbuttonclicked="OnGuestsInfoCompleted"
					/>
					
				</asp:view>
			
			</asp:multiview>
			
		</asp:tablecell>
	</asp:tablerow>
	
</asp:table>
