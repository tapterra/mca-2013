using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ReserveItBtn : System.Web.UI.UserControl {

	protected string AtcoLinkUrl = "booking.aspx?id={0}";
	protected string NonAtcoActivityUrl = "resbooking.aspx?id={0}";

	public string ActivityAID {
		set { this.LinkButton.NavigateUrl = ConfigMgr.SitePageUrl( this.Page, string.Format( this.AtcoLinkUrl, value ) ); }
	}

	public string ActivityID {
		set { this.LinkButton.NavigateUrl = ConfigMgr.SitePageUrl( this.Page, string.Format( this.NonAtcoActivityUrl, value ) ); }
	}

	public string NavigateUrl {
		set { this.LinkButton.NavigateUrl = value; }
	}

	public CssStyleCollection Style {
		get { return ( this.LinkButton.Style ); }
	}

}
