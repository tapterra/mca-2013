using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ItineraryPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.MainTable.CssClass ); }
		set { this.MainTable.CssClass = value; }
	}

	public int CellSpacing {
		get { return ( this.MainTable.CellSpacing ); }
		set { this.MainTable.CellSpacing = value; }
	}

	public int CellPadding {
		get { return ( this.MainTable.CellPadding ); }
		set { this.MainTable.CellPadding = value; }
	}

	public string HeaderCellCssClass {
		get { return ( this.HeaderCell.CssClass ); }
		set { this.HeaderCell.CssClass = value; }
	}

	public string HeaderLabelCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}

	public string FooterCellCssClass {
		get { return ( this.FooterCell.CssClass ); }
		set {
			this.FooterCell.CssClass = value;
			this.FooterTable.CssClass = value;
		}
	}

	public string LabelCssClass {
		get {
			object obj = ViewState["LabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["LabelCssClass"] = value; }
	}

	public string ValueCssClass {
		get {
			object obj = ViewState["ValueCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["ValueCssClass"] = value; }
	}

	public string ButtonCssClass {
		get { return ( this.CheckoutBtn.CssClass ); }
		set {
			this.CheckoutBtn.CssClass = value;
			this.ContinueBtn.CssClass = value;
		}
	}

	#endregion

	#region Booking Panel CSS Classes

	public string BookPanelCssClass {
		get {
			object obj = ViewState["BookPanelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelCssClass"] = value; }
	}

	public int BookPanelCellSpacing {
		get {
			object obj = ViewState["BookPanelCellSpacing"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["BookPanelCellSpacing"] = value; }
	}

	public int BookPanelCellPadding {
		get {
			object obj = ViewState["BookPanelCellPadding"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["BookPanelCellPadding"] = value; }
	}

	public string BookPanelHeaderTableCssClass {
		get {
			object obj = ViewState["BookPanelHeaderTableCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelHeaderTableCssClass"] = value; }
	}

	public string BookPanelFooterTableCssClass {
		get {
			object obj = ViewState["BookPanelFooterTableCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelFooterTableCssClass"] = value; }
	}

	public string BookPanelGuestPanelsCellCssClass {
		get {
			object obj = ViewState["BookPanelGuestPanelsCellCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelGuestPanelsCellCssClass"] = value; }
	}

	public string BookPanelLabelCssClass {
		get {
			object obj = ViewState["BookPanelLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelLabelCssClass"] = value; }
	}

	public string BookPanelValueCssClass {
		get {
			object obj = ViewState["BookPanelValueCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelValueCssClass"] = value; }
	}

	public string BookPanelActivityNameCssClass {
		get {
			object obj = ViewState["BookPanelActivityNameCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelActivityNameCssClass"] = value; }
	}

	public string BookPanelButtonCssClass {
		get {
			object obj = ViewState["BookPanelButtonCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["BookPanelButtonCssClass"] = value; }
	}

	#endregion

	#region GuestPanel CSS Classes

	public string GuestPanelCssClass {
		get {
			object obj = ViewState["GuestPanelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelCssClass"] = value; }
	}

	public int GuestPanelCellSpacing {
		get {
			object obj = ViewState["GuestPanelCellSpacing"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["GuestPanelCellSpacing"] = value; }
	}

	public int GuestPanelCellPadding {
		get {
			object obj = ViewState["GuestPanelCellPadding"];
			return ( obj == null ? 0 : (int) obj );
		}
		set { ViewState["GuestPanelCellPadding"] = value; }
	}

	public string GuestPanelLabelCssClass {
		get {
			object obj = ViewState["GuestPanelLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelLabelCssClass"] = value; }
	}

	public string GuestPanelGuestNameCssClass {
		get {
			object obj = ViewState["GuestPanelGuestNameCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelGuestNameCssClass"] = value; }
	}

	public string GuestPanelGuestTypeCssClass {
		get {
			object obj = ViewState["GuestPanelGuestTypeCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelGuestTypeCssClass"] = value; }
	}

	public string GuestPanelPriceCssClass {
		get {
			object obj = ViewState["GuestPanelPriceCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelPriceCssClass"] = value; }
	}

	public string GuestPanelOptionsCssClass {
		get {
			object obj = ViewState["GuestPanelOptionsCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["GuestPanelOptionsCssClass"] = value; }
	}

	#endregion

	public string ErrorMessage {
		get { return ( this.MessageLbl.Text ); }
		set {
			this.MessageLbl.Text = value;
			this.MessageRow.Visible = !string.IsNullOrEmpty( value );
		}
	}

	public decimal TotalPrice {
		set {
			if ( Website.Current.IsNoPaymentSite ) {
				this.TotalLbl.Text = "&nbsp;";
			} else {
				this.TotalLbl.Text = string.Format( "US$ {0:F}", value );
			}
		}
	}

	public string CustomerPromoCode {
		get { return ( this.PromoCodeBox.Text ); }
		set { this.PromoCodeBox.Text = value; }
	}

	#endregion

	protected void Page_Load( object sender, EventArgs e ) {
		// By the time we get here, updates to the itinerary are in place and ready to persist
		if ( Customer.Current.ID < 0 ) {
			Customer.Current.Persist();
		}
		Itinerary itin = Customer.Current.CurrentItinerary;
		if ( this.IsPostBack ) {
			itin.CustomerID = Customer.Current.ID;
			itin.CustomerPromoCode = this.CustomerPromoCode;
			itin.Persist();
		}
		this.BuildPanel();
		this.ErrorMessage = itin.ErrorMessage;
		itin.ErrorMessage = "";
		this.PromoCodeRow.Visible = Website.Current.ActivePromoCodes.Count > 0;
		this.TotalRow.Visible = !Website.Current.IsNoPaymentSite;
		if ( !this.IsPostBack ) {
			this.CustomerPromoCode = itin.CustomerPromoCode;
		}
	}

	protected void BuildPanel() {
		Itinerary itin = Customer.Current.CurrentItinerary;
		this.BodyCell.Controls.Clear();
		this.TotalPrice = itin.TotalDiscountedPrice;
		int b = 0;
		// First, clean up any empty bookings...
		for ( int i = 0 ; i < itin.Bookings.Count ; ) {
			Booking booking = itin.Bookings[i];
			if ( booking.PaxCounts.TotalQty == 0 ) {
				itin.Bookings.Remove( booking );
				if ( itin.ResBookingsCount == 0 ) {
					//this.Session.Abandon();
					Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
				}
			} else {
				i++;
			}
		}
		// ...as well as any empty reservations...
		for ( int i = 0 ; i < itin.Reservations.Count ; ) {
			Reservation reservation = itin.Reservations[i];
			if ( reservation.PaxCount == 0 ) {
				itin.Reservations.Remove( reservation );
				if ( itin.ResBookingsCount == 0 ) {
					//this.Session.Abandon();
					Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
				}
			} else {
				i++;
			}
		}
		// Create the booking panels
		foreach ( Booking booking in itin.Bookings ) {
			if ( booking.PaxCounts.TotalQty == 0 ) {
				continue;
			}
			ItineraryBookingPanel bPanel = (ItineraryBookingPanel) this.Page.LoadControl( "~/abs/ItineraryBookingPanel.ascx" );
			bPanel.BookingIndex = b++;
			bPanel.CssClass = this.BookPanelCssClass;
			if ( booking.IsErrorHighlighted ) {
				bPanel.CssClass = "ErrorMsgSubPanel";
				booking.IsErrorHighlighted = false;
			}
			bPanel.CellSpacing = this.BookPanelCellSpacing;
			bPanel.CellPadding = this.BookPanelCellPadding;
			bPanel.HeaderTableCssClass = this.BookPanelHeaderTableCssClass;
			bPanel.FooterTableCssClass = this.BookPanelFooterTableCssClass;
			bPanel.GuestPanelsCellCssClass = this.BookPanelGuestPanelsCellCssClass;
			bPanel.LabelCssClass = this.BookPanelLabelCssClass;
			bPanel.ValueCssClass = this.BookPanelValueCssClass;
			bPanel.ActivityNameCssClass = this.BookPanelActivityNameCssClass;
			bPanel.ButtonCssClass = this.BookPanelButtonCssClass;
			bPanel.GuestPanelCssClass = this.GuestPanelCssClass;
			bPanel.GuestPanelCellSpacing = this.GuestPanelCellSpacing;
			bPanel.GuestPanelCellPadding = this.GuestPanelCellPadding;
			bPanel.GuestPanelLabelCssClass = this.GuestPanelLabelCssClass;
			bPanel.GuestPanelGuestNameCssClass = this.GuestPanelGuestNameCssClass;
			bPanel.GuestPanelGuestTypeCssClass = this.GuestPanelGuestTypeCssClass;
			bPanel.GuestPanelPriceCssClass = this.GuestPanelPriceCssClass;
			bPanel.GuestPanelOptionsCssClass = this.GuestPanelOptionsCssClass;
			bPanel.TheBooking = booking;
			bPanel.RemoveBooking += new ItineraryItemEventHandler( RemoveBooking );
			bPanel.ReviseBooking += new ItineraryItemEventHandler( ReviseBooking );
			this.BodyCell.Controls.Add( bPanel );
		}
		// Create the reservation panels
		foreach ( Reservation reservation in itin.Reservations ) {
			if ( reservation.PaxCount == 0 ) {
				continue;
			}
			ItineraryReservationPanel bPanel = (ItineraryReservationPanel) this.Page.LoadControl( "~/abs/ItineraryReservationPanel.ascx" );
			bPanel.ReservationIndex = b++;
			bPanel.CssClass = this.BookPanelCssClass;
			if ( reservation.IsErrorHighlighted ) {
				bPanel.CssClass = "ErrorMsgSubPanel";
				reservation.IsErrorHighlighted = false;
			}
			bPanel.CellSpacing = this.BookPanelCellSpacing;
			bPanel.CellPadding = this.BookPanelCellPadding;
			bPanel.HeaderTableCssClass = this.BookPanelHeaderTableCssClass;
			bPanel.FooterTableCssClass = this.BookPanelFooterTableCssClass;
			bPanel.LabelCssClass = this.BookPanelLabelCssClass;
			bPanel.ValueCssClass = this.BookPanelValueCssClass;
			bPanel.ActivityNameCssClass = this.BookPanelActivityNameCssClass;
			bPanel.ButtonCssClass = this.BookPanelButtonCssClass;
			bPanel.TheReservation = reservation;
			bPanel.RemoveReservation += new ItineraryItemEventHandler( RemoveReservation );
			bPanel.ReviseReservation += new ItineraryItemEventHandler( ReviseReservation );
			this.BodyCell.Controls.Add( bPanel );
		}
		this.CheckoutBtn.Enabled = itin.ResBookingsCount > 0;
	}

	protected void ReviseBooking( object sender, ItineraryItemEventArgs e ) {
		// Send this booking to the BookingPanel page for revision
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, string.Format( "booking.aspx?bx={0}", e.BookingIndex ) ) );
	}

	protected void RemoveBooking( object sender, ItineraryItemEventArgs e ) {
		// Remove this booking from the itinerary, and rebuild our UI...
		Itinerary itin = Customer.Current.CurrentItinerary;
		Booking b = itin.Bookings[e.BookingIndex];
		itin.Bookings.Remove( b );
		b.Delete();
		//if ( itin.ResBookingsCount == 0 ) {
		//  this.Session.Abandon();
		//}
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
	}

	protected void ReviseReservation( object sender, ItineraryItemEventArgs e ) {
		// Send this Reservation to the ReservationPanel page for revision
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, string.Format( "resbooking.aspx?rx={0}", e.BookingIndex ) ) );
	}

	protected void RemoveReservation( object sender, ItineraryItemEventArgs e ) {
		// Remove this Reservation from the itinerary, and rebuild our UI...
		Itinerary itin = Customer.Current.CurrentItinerary;
		Reservation r = itin.Reservations[e.BookingIndex];
		itin.Reservations.Remove( r );
		r.Delete();
		//if ( itin.ResBookingsCount == 0 ) {
		//  this.Session.Abandon();
		//}
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
	}

	protected void OnPromoCodeChg( object sender, System.EventArgs e ) {
		Customer.Current.CurrentItinerary.CustomerPromoCode = this.CustomerPromoCode;
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
	}

	protected void OnCheckoutClicked( object sender, System.EventArgs e ) {
		Response.Redirect( ConfigMgr.SiteSslPageUrl( this.Page, "checkout.aspx" ) );
	}

	protected void OnContinueClicked( object sender, System.EventArgs e ) {
		Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "catalog.aspx" ) );
	}

}
