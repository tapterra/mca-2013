<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePicker.ascx.cs" Inherits="TimePicker" %>

<table width="261">
	<tr>
		<td>
			<asp:label runat="server" id="LabelLbl" text="Time/Hour (choose one)" />
		</td>
	</tr>
	<tr>
		<td>
			<asp:dropdownlist runat="server" id="TimeLBox" tabindex="1" >
				<asp:listitem selected="true" text=" -- " value="" />
				<asp:listitem value="12:00" />				<asp:listitem value="12:15" />				<asp:listitem value="12:30" />				<asp:listitem value="12:45" />				<asp:listitem value="1:00" />				<asp:listitem value="1:15" />				<asp:listitem value="1:30" />				<asp:listitem value="1:45" />				<asp:listitem value="2:00" />				<asp:listitem value="2:15" />				<asp:listitem value="2:30" />				<asp:listitem value="2:45" />				<asp:listitem value="3:00" />				<asp:listitem value="3:15" />				<asp:listitem value="3:30" />				<asp:listitem value="3:45" />				<asp:listitem value="4:00" />				<asp:listitem value="4:15" />				<asp:listitem value="4:30" />				<asp:listitem value="4:45" />				<asp:listitem value="5:00" />				<asp:listitem value="5:15" />				<asp:listitem value="5:30" />				<asp:listitem value="5:45" />				<asp:listitem value="6:00" />				<asp:listitem value="6:15" />				<asp:listitem value="6:30" />				<asp:listitem value="6:45" />				<asp:listitem value="7:00" />				<asp:listitem value="7:15" />				<asp:listitem value="7:30" />				<asp:listitem value="7:45" />				<asp:listitem value="8:00" />				<asp:listitem value="8:15" />				<asp:listitem value="8:30" />				<asp:listitem value="8:45" />				<asp:listitem value="9:00" />				<asp:listitem value="9:15" />				<asp:listitem value="9:30" />				<asp:listitem value="9:45" />				<asp:listitem value="10:00" />				<asp:listitem value="10:15" />				<asp:listitem value="10:30" />				<asp:listitem value="10:45" />				<asp:listitem value="11:00" />				<asp:listitem value="11:15" />				<asp:listitem value="11:30" />				<asp:listitem value="11:45" />			</asp:dropdownlist>/<asp:dropdownlist runat="server" id="AmpmLBox" tabindex="1">
				<asp:listitem selected="true" text=" -- " value="" />
				<asp:listitem value="AM" />
				<asp:listitem value="PM" />
			</asp:dropdownlist>
		</td>
	</tr>
</table>
