using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class IslandPicker : System.Web.UI.UserControl {

	#region Properties

	public string IslandKey {
		get {
			object o = Session["CurrentIslandKey"];
			return ( o == null ? "oahu" : (string) o );
		}
		set { Session["CurrentIslandKey"] = value; }
	}

	public Island Island {
		get {
			if ( this.island == null ) {
				this.island = new Island( this.IslandKey );
			}
			return ( this.island );
		}
	}
	private Island island;

	public string CategoryKey {
		get {
			object o = Session["CurrentCategoryKey"];
			return ( o == null ? "activities" : (string) o );
		}
		set { Session["CurrentCategoryKey"] = value; }
	}

	public Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryKey );
			}
			return ( this.category );
		}
	}
	private Category category;

	#endregion

	#region Events
	public event EventHandler IslandChanged {
		add { Events.AddHandler( IslandChangedEventKey, value ); }
		remove { Events.RemoveHandler( IslandChangedEventKey, value ); }
	}
	protected virtual void OnIslandChanged( Object sender, EventArgs args ) {
		EventHandler handler = (EventHandler) Events[IslandChangedEventKey];
		if ( handler != null )
			handler( sender, args );
	}
	private static readonly object IslandChangedEventKey = new object();
	#endregion

	#region Methods

	protected void SetupHeaderImage() {
		this.HeaderImage.ImageUrl = ConfigMgr.SitePageUrl( this.Page, string.Format( "images/titles/{0}{1}title.gif", this.IslandKey, this.CategoryKey ) );
	}

	protected void SetupPickerItems() {
		this.IslandPickerBox.Items.Clear();
		string catLabel = this.Category.Label;
		this.IslandPickerBox.Items.Add( new ListItem( string.Format( "Choose an Island - {0}", catLabel ), "" ) );
		foreach ( Island isl in Islands.Current ) {
			string lbl = string.Format( "{0} {1}", isl.Name, catLabel );
			this.IslandPickerBox.Items.Add( new ListItem( lbl, isl.KeyName ) );
		}
	}

	public void Reset() {
		this.SetupHeaderImage();
		this.SetupPickerItems();
	}

	protected void NewIslandSelected( object sender, System.EventArgs e ) {
		string islandKey = this.IslandPickerBox.SelectedValue;
		if ( !string.IsNullOrEmpty( islandKey ) ) {
			this.IslandKey = islandKey;
			// Fire our own event here
			this.OnIslandChanged( this, new EventArgs() );
			this.Reset();
			this.IslandPickerBox.SelectedIndex = 0;
		}
	}

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
			this.Reset();
		}
	}

	#endregion

}