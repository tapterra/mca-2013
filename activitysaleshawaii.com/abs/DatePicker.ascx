<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.ascx.cs" Inherits="DatePicker" %>

<table>
	<tr>
		<td>
			<asp:label runat="server" id="LabelLbl" text="Month/Day/Year (choose one)" />
		</td>
	</tr>
	<tr>
		<td>
			<asp:dropdownlist runat="server" id="MonthLBox" tabindex="1"
			 />/<asp:dropdownlist runat="server" id="DayLBox" tabindex="1" 
			 />/<asp:dropdownlist runat="server" id="YearLBox" tabindex="1" />
		</td>
	</tr>
</table>
