<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckoutCreditCardPanel.ascx.cs" Inherits="CheckoutCreditCardPanel" classname="CheckoutCreditCardPanel" %>

<asp:panel runat="server" id="MainPanel" >

	<asp:label runat="server" id="HeaderLbl" text="Credit Card Information" />

	<asp:table runat="server" id="BodyTable" >

		<asp:tablerow id="Tablerow1" runat="server">
			<asp:tablecell id="Tablecell1" runat="server" columnspan="2">
				<stic:labeledtextbox runat="server" id="CardholderNameBox"
					width="400"
					label='Cardholder Name (as printed on card)<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="CardholderNameReqVal"
				 controltovalidate="CardholderNameBox"
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;the&nbsp;cardholder's&nbsp;name."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>

		<asp:tablerow id="Tablerow2" runat="server">
			<asp:tablecell id="Tablecell2" runat="server" width="160" >
				<stic:labeleddropdownlist runat="server" id="CardTypeListBox"
					width="150"
					label='Card Type<span style="color:red">*</span>'
				 tabindex="1"
				>
					<asp:listitem value=""					text=" --- Select --- " />
					<asp:listitem value="AMEX"			text="AMEX" />
					<asp:listitem value="Discover"	text="Discover" />
					<asp:listitem value="MC"				text="Master Card" />
					<asp:listitem value="VISA"			text="Visa" />
				</stic:labeleddropdownlist>
				<asp:requiredfieldvalidator runat="server" id="CardTypeReqVal"
				 controltovalidate="CardTypeListBox"
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;select&nbsp;the&nbsp;card&nbsp;type."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
			<asp:tablecell id="Tablecell3" runat="server" >
				<stic:labeledtextbox runat="server" id="CardNumberBox"
					width="230"
					label='Card Number<span style="color:red">*</span>'
				 tabindex="1"
				/>
				<asp:requiredfieldvalidator runat="server" id="CardNumberReqVal"
				 controltovalidate="CardNumberBox"
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;the&nbsp;credit&nbsp;card&nbsp;number."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>
		</asp:tablerow>

		<asp:tablerow id="Tablerow3" runat="server">

			<asp:tablecell id="Tablecell4" runat="server" >
				<table border="0" cellpadding="0" cellspacing="2" >
					<tr><td colspan="2"><asp:label runat="server" id="ExpDateLbl" text="Expiration" /></td></tr>
					<tr>
						<td>
							<asp:dropdownlist runat="server" id="ExpMMListBox"
							 tabindex="1"
							>
								<asp:listitem value="01" text="01" />
								<asp:listitem value="02" text="02" />
								<asp:listitem value="03" text="03" />
								<asp:listitem value="04" text="04" />
								<asp:listitem value="05" text="05" />
								<asp:listitem value="06" text="06" />
								<asp:listitem value="07" text="07" />
								<asp:listitem value="08" text="08" />
								<asp:listitem value="09" text="09" />
								<asp:listitem value="10" text="10" />
								<asp:listitem value="11" text="11" />
								<asp:listitem value="12" text="12" />
							</asp:dropdownlist>
						</td>
						<td>
							<asp:dropdownlist runat="server" id="ExpYYYYListBox" tabindex="1" />
						</td>
					</tr>
				</table>
			</asp:tablecell>
			<asp:tablecell id="Tablecell5" runat="server" >
				<table>
					<tr><td valign="middle">
						<stic:labeledtextbox runat="server" id="CvvNumberBox"
							width="150"
							label='CVV Number<span style="color:red">*</span>'
						 tabindex="1"
						/>
					</td><td valign="middle" style="padding-top:12px">
						<asp:hyperlink runat="server" id="WhatsThisLink" text="What's This?"
						 navigateurl="~/cvvhelp.aspx" target="_blank"
						 style="width:60px"
						 tabindex="1"
						/>
					</td></tr>
				</table>
				<asp:requiredfieldvalidator runat="server" id="CvvNumberReqVal"
				 controltovalidate="CvvNumberBox"
				 display="dynamic" setfocusonerror="true"
				 enableclientscript="true"
				 errormessage="Please&nbsp;enter&nbsp;the&nbsp;credit&nbsp;card&nbsp;CVV&nbsp;Number."
				 cssclass="ErrorMessages"
				/>
			</asp:tablecell>

		</asp:tablerow>

	</asp:table>

</asp:panel>
