using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class ItineraryGuestPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}

	public int CellSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}

	public int CellPadding {
		get { return ( this.BodyTable.CellPadding ); }
		set { this.BodyTable.CellPadding = value; }
	}

	//public Unit Width {
	//  get { return ( this.BodyTable.Width ); }
	//  set { this.BodyTable.Width = value; }
	//}

	public string LabelCssClass {
		get {
			object obj = ViewState["LabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["LabelCssClass"] = value; }
	}

	public string GuestNameCssClass {
		get { return ( this.GuestNameLbl.CssClass ); }
		set { this.GuestNameLbl.CssClass = value; }
	}

	public string GuestTypeCssClass {
		get { return ( this.GuestTypeLbl.CssClass ); }
		set { this.GuestTypeLbl.CssClass = value; }
	}

	public string PriceCssClass {
		get { return ( this.PriceLbl.CssClass ); }
		set { this.PriceLbl.CssClass = value; }
	}

	public string OptionsCssClass {
		get { return ( this.OptionsLbl.CssClass ); }
		set { this.OptionsLbl.CssClass = value; }
	}

	#endregion

	/// <summary>
	/// Assigned the Guest this panel is to display
	/// </summary>
	public Guest Guest {
		set {
			this.GuestNameLbl.Text = value.GuestNameDisplay;
			this.GuestTypeLbl.Text = value.GuestTypeDisplay;
			this.PriceLbl.Text = value.PriceDisplay;
			this.OptionsLbl.Text = value.OptionsDisplay;
			this.OptionsRow.Visible = value.OptionsDisplayVisible;
		}
	}

	public int GuestIndex {
		set { this.GuestIndexLbl.Text = string.Format( "Guest&nbsp;{0}", value ); }
	}

	#endregion

	#region Event Handlers
	
	protected override void OnPreRender( EventArgs e ) {
		if ( this.LabelCssClass.Length > 0 ) {
			this.GuestIndexLbl.CssClass = this.LabelCssClass;
			//this.OptionsLabelLbl.CssClass = this.LabelCssClass;
		}
		base.OnPreRender( e );
	}

	#endregion

}
