<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItineraryBookingPanel.ascx.cs" Inherits="ItineraryBookingPanel" classname="ItineraryBookingPanel" %>
<%@ register tagprefix="abs" tagname="guestpanel" src="ItineraryGuestPanel.ascx" %>

<asp:table runat="server" id="MainTable" >
	
	<asp:tablerow runat="server" id="HeaderTableRow" >
		<asp:tablecell runat="server" id="HeaderTableCell">
			
			<asp:table runat="server" id="HeaderTable" width="100%">
				<asp:tablerow id="ActivityRow" runat="server">
					<asp:tablecell id="ActivityLabelCell" runat="server" width="90">
						<asp:label runat="server" id="ActivityLabelLbl" text="Activity" />
					</asp:tablecell>
					<asp:tablecell id="ActivityNameCell" runat="server" >
						<asp:label runat="server" id="ActivityNameLbl" text="Oahu Deluxe Super Fun Activity" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="DateTimeRow" runat="server">
					<asp:tablecell id="DateTimeLabelCell" runat="server" >
						<asp:label runat="server" id="DateTimeLabelLbl" text="Date/Time" />
					</asp:tablecell>
					<asp:tablecell id="DateTimeCell" runat="server" >
						<asp:label runat="server" id="DateTimeLbl" text="Saturday, November 11, 2005  10:00 AM" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="HotelRow" runat="server">
					<asp:tablecell id="HotelLabelCell" runat="server" >
						<asp:label runat="server" id="HotelLabelLbl" text="Hotel" />
					</asp:tablecell>
					<asp:tablecell id="HotelCell" runat="server" >
						<asp:label runat="server" id="HotelLbl" text="Aston Aloha Surf Hotel" />
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			
		</asp:tablecell>
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="GuestPanelsRow" >
		<asp:tablecell runat="server" id="GuestPanelsCell" />
	</asp:tablerow>
	
	<asp:tablerow runat="server" id="FooterTableRow" >
		<asp:tablecell runat="server" id="FooterTableCell" >
			
			<asp:table runat="server" id="FooterTable" width="100%" >
				<asp:tablerow id="TaxRow" runat="server" visible="false" >
					<asp:tablecell id="TaxLabelCell" runat="server" width="10%">
						<asp:label runat="server" id="TaxLabelLbl" text="Tax" />
					</asp:tablecell>
					<asp:tablecell id="TaxCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="TaxLbl" text="US$ 12.34" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="DiscountRow" runat="server" visible="false" >
					<asp:tablecell id="DiscountLabelCell" runat="server" width="10%">
						<asp:label runat="server" id="DiscountLabelLbl" text="Discount" />
					</asp:tablecell>
					<asp:tablecell id="DiscountCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="DiscountLbl" text="US$ 12.34" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="TotalRow" runat="server">
					<asp:tablecell id="TotalLabelCell" runat="server" width="10%">
						<asp:label runat="server" id="TotalLabelLbl" text="Total" />
					</asp:tablecell>
					<asp:tablecell id="TotalCell" runat="server" horizontalalign="right" >
						<asp:label runat="server" id="TotalLbl" text="US$ 567.89" />
					</asp:tablecell>
				</asp:tablerow>
				<asp:tablerow id="ButtonRow" runat="server">
					<asp:tablecell id="ButtonCell" runat="server" columnspan="2" horizontalalign="center" >
						<asp:button runat="server" id="ReviseBtn" text="Revise" 
						 commandname="revise" oncommand="OnBookingCommand" 
						/>
						&nbsp;
						&nbsp;
						<asp:button runat="server" id="RemoveBtn" text="Remove" 
						 commandname="remove" oncommand="OnBookingCommand" 
						 onclientclick="return confirm( 'Are you sure you want to remove this activity from your itinerary?' );"
						/>
					</asp:tablecell>
				</asp:tablerow>
			</asp:table>
			
		</asp:tablecell>
	</asp:tablerow>

</asp:table>

