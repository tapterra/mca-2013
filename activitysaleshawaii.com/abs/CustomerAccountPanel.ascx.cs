using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ABS;

public partial class CustomerAccountPanel : System.Web.UI.UserControl {

	protected int CustomerID {
		get {
			object obj = this.ViewState["CustomerID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { this.ViewState["CustomerID"] = value; }
	}

	protected int ItineraryID {
		get {
			object obj = this.ViewState["ItineraryID"];
			return ( obj == null ? -1 : (int) obj );
		}
		set { 
			this.ViewState["ItineraryID"] = value;
			if ( this.itinerary != null && this.itinerary.ID != value ) {
				this.itinerary = null;
			}
		}
	}

	public Itinerary Itinerary {
		get {
			if ( this.itinerary == null ) {
				this.itinerary = new Itinerary( this.ItineraryID );
			}
			return ( this.itinerary );
		}
		set {
			this.itinerary = value;
			this.ItineraryID = value.ID;
		}
	}
	private Itinerary itinerary;

	protected void ShowSigninPanel() {
		this.LoginFormPanel.Visible = true;
		this.AccountInfoPanel.Visible = false;
		this.EmailBox.Focus();
	}

	protected void ShowAccountInfoPanel() {
		this.LoginFormPanel.Visible = false;
		this.AccountInfoPanel.Visible = true;
		this.Rebind();
	}

	protected void DoSignIn( object sender, System.EventArgs e ) {
		string email = this.EmailBox.Text;
		string code = this.AccountNoBox.Text;
		int iid = -1;
		int cid = -1;
		if ( Itinerary.ParseLoginAccountNumber( code, out iid, out cid ) ) {
			this.ItineraryID = iid;
			this.CustomerID = cid;
			this.SetupforAccount( email );
		} else {
			this.ItineraryID = -1;
			this.CustomerID = -1;
			this.ShowSigninPanel();
			this.ErrorMsg.Visible = true;
		}
	}

	protected void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			int cid = -1;
			string c = this.Request.QueryString["c"];
			if ( !string.IsNullOrEmpty( c ) ) {
				Int32.TryParse( c, out cid );
			}
			int iid = -1;
			string i = this.Request.QueryString["i"];
			if ( !string.IsNullOrEmpty( i ) ) {
				Int32.TryParse( i, out iid );
			}
			if ( cid > 0 && iid > 0 ) {
				this.CustomerID = cid;
				this.ItineraryID = iid;
				this.SetupforAccount( "" );
			} else {
				this.ShowSigninPanel();
			}
		}
	}

	protected void SetupforAccount( string email ) {
		if ( this.CustomerID > 0 && this.ItineraryID > 0 ) {
			Customer cust;
			if ( string.IsNullOrEmpty( email ) ) {
				cust = new Customer( this.CustomerID );
			} else {
				cust = new Customer( email, this.CustomerID );
			}
			if ( cust.ID <= 0 ) {
				// No match found
				this.ShowSigninPanel();
				this.ErrorMsg.Visible = !string.IsNullOrEmpty( email );
				return;
			}
			//Itinerary itin = cust.Itineraries.GetItemByID( this.ItineraryID );
			Itinerary itin = cust.Itineraries[this.ItineraryID];
			if ( itin == null ) {
				// No match found
				this.ShowSigninPanel();
				this.ErrorMsg.Visible = !string.IsNullOrEmpty( email );
				return;
			}
			this.ShowAccountInfoPanel();
		} else {
			this.ShowSigninPanel();
		}
	}

	public void Rebind() {
		this.BookingRepeater.DataSource = this.Itinerary.Bookings;
		this.BookingRepeater.Visible = this.Itinerary.Bookings.Count > 0;

		this.ResRepeater.DataSource = this.Itinerary.Reservations;
		this.ResRepeater.Visible = this.Itinerary.Reservations.Count > 0;

		this.PaymentsRepeater.DataSource = this.Itinerary.Payments;
		this.PaymentsRepeater.Visible = this.Itinerary.Payments.Count > 0;

		this.DataBind();

		this.VoucherHolder.Text = this.Itinerary.VoucherHtml;
		this.VouchersButton.Visible = !string.IsNullOrEmpty( this.VoucherHolder.Text );
	}

}
