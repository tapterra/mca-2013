using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ABS;

public partial class ResBookingPanel : System.Web.UI.UserControl {

	#region Properties

	#region CSS Classes

	public string CssClass {
		set { this.MainTable.Attributes["class"] = value; }
	}

	public string HeaderCellCssClass {
		set {
			this.BookingLbl.Attributes["class"] = value;
			this.ActivityTitleLbl.Attributes["class"] = value;
		}
	}

	public string HeaderStepCssClass {
		set { this.HeaderStepLbl.CssClass = value; }
	}

	public string LabelsCssClass {
		set {
			this.FirstNameBox.Frame.LabelStyle.CssClass = value;
			this.LastNameBox.Frame.LabelStyle.CssClass = value;
			this.PaxCountLbl.CssClass = value;
			this.ResDateLbl.CssClass = value;
			this.ResDatePicker.LabelCssClass = value;
			this.PrefTimeLbl.CssClass = value;
			this.PrefTimePicker.LabelCssClass = value;
			this.AltTimeLbl.CssClass = value;
			this.AltTimePicker.LabelCssClass = value;
		}
	}

	public string ValuesCssClass {
		set {
			this.FirstNameBox.CssClass = value;
			this.LastNameBox.CssClass = value;
			this.PaxCountBox.CssClass = value;
			this.ResDatePicker.ValueCssClass = value;
			this.PrefTimePicker.ValueCssClass = value;
			this.AltTimePicker.ValueCssClass = value;
		}
	}

	public string NextButtonCssClass {
		get { return ( this.NextStepBtn.CssClass ); }
		set { this.NextStepBtn.CssClass = value; }
	}

	#endregion

	public string FirstName {
		get { return ( this.FirstNameBox.Text ); }
		set { this.FirstNameBox.Text = value; }
	}

	public string LastName {
		get { return ( this.LastNameBox.Text ); }
		set { this.LastNameBox.Text = value; }
	}

	public int PaxCount {
		get {
			try {
				return ( Int32.Parse( this.PaxCountBox.Text ) );
			} catch {
				return ( 0 );
			}
		}
		set { this.PaxCountBox.Text = value.ToString(); }
	}

	public DateTime ReservationDate {
		get { return ( this.ResDatePicker.Value ); }
		set { this.ResDatePicker.Value = value; }
	}

	public string PrefTime {
		get { return ( this.PrefTimePicker.SelectedTime ); }
		set { this.PrefTimePicker.SelectedTime = value; }
	}

	public string AltTime {
		get { return ( this.AltTimePicker.SelectedTime ); }
		set { this.AltTimePicker.SelectedTime = value; }
	}

	public string ActivityTitle {
		get { return ( this.ActivityTitleLbl.Text ); }
		set { this.ActivityTitleLbl.Text = value; }
	}

	public int ActivityID {
		get { return ( (int) this.ViewState["ActivityID"] ); }
		set { this.ViewState["ActivityID"] = value; }
	}

	public Activity Activity {
		get {
			if ( this.activity == null ) {
				this.activity = new Activity( this.ActivityID );
			}
			return ( this.activity );
		}
	}
	private Activity activity;

	public Reservation Reservation {
		get {
			Reservation aReservation = Reservation.Current;
			if ( aReservation == null ) {
				Reservation.Current = new Reservation( this.Activity );
				aReservation = Reservation.Current;
			}
			return ( aReservation );
		}
		set {
			Reservation.Current = value;
			this.ActivityID = value.ActivityID;
			this.ActivityTitle = value.Activity.ItineraryTitle;
		}
	}

	#endregion

	#region Methods

	protected void Page_Load( object sender, System.EventArgs e ) {
		if ( !this.IsPostBack ) {
			string rx = this.Request.QueryString["rx"];
			if ( !string.IsNullOrEmpty( rx ) ) {
				// We're editing an existing reservation, via it's index in the itinerary
				int resIndex = Int32.Parse( rx );
				if ( resIndex >= 0 && resIndex < Customer.Current.CurrentItinerary.Reservations.Count ) {
					this.Reservation = Customer.Current.CurrentItinerary.Reservations[resIndex];
					this.ActivityID = this.Reservation.Activity.ID;
				}
			} else {
				// We're creating a new reservation via the Activity ID
				int actID = Int32.Parse( this.Request.QueryString["id"] );
				this.ActivityID = actID;
				Activity activity = new Activity( actID );
				this.Reservation = new Reservation( activity );
				this.Reservation.FirstName = Customer.Current.CurrentItinerary.CustomerFirstName;
				this.Reservation.LastName = Customer.Current.CurrentItinerary.CustomerLastName;
				Customer.Current.CurrentItinerary.Reservations.Add( this.Reservation );
			}
			this.SetupForCurrentReservation();
			this.FirstNameBox.Focus();
		}
	}

	public void SetupForCurrentReservation() {
		Reservation res = this.Reservation;
		this.FirstName = res.FirstName;
		this.LastName = res.LastName;
		this.PaxCount = res.PaxCount;
		this.ReservationDate = res.ResDate;
		this.PrefTime = res.PrefTime;
		this.AltTime = res.AltTime;
	}

	protected void OnValidateResDate( object sender, ServerValidateEventArgs e ) {
		DateTime dateValue = DateTime.Parse( e.Value );
		e.IsValid = ( dateValue > DateTime.Today );
	}

	protected void OnNextStepButtonClicked( object sender, EventArgs e ) {
		Page.Validate();
		if ( Page.IsValid ) {
			Reservation res = this.Reservation;
			res.FirstName = this.FirstName;
			res.LastName = this.LastName;
			res.PaxCount = this.PaxCount;
			res.ResDate = this.ReservationDate;
			res.PrefTime = this.PrefTime;
			res.AltTime = this.AltTime;
			Customer.Current.CurrentItinerary.CustomerFirstName = this.FirstName;
			Customer.Current.CurrentItinerary.CustomerLastName = this.LastName;
			Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "itinerary.aspx" ) );
		}
	}

	#endregion

}
