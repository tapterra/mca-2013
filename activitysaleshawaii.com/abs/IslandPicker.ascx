<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IslandPicker.ascx.cs" Inherits="IslandPicker" %>

<asp:image runat="server" id="HeaderImage" /><br />
<span class="legalese">
	
	<asp:dropdownlist id="IslandPickerBox" runat="server" cssclass="legalese" autopostback="true" onselectedindexchanged="NewIslandSelected"  
	 style="width:320px"
	 tabindex="1"
	/>
	
</span>
