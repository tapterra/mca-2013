using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using StarrTech.WebTools.Controls;

public partial class CheckoutCreditCardPanel : System.Web.UI.UserControl {

	#region Properties
	
	#region CSS Classes

	public string CssClass {
		get { return ( this.MainPanel.CssClass ); }
		set { this.MainPanel.CssClass = value; }
	}

	public string HeaderCssClass {
		get { return ( this.HeaderLbl.CssClass ); }
		set { this.HeaderLbl.CssClass = value; }
	}

	public string BodyTableCssClass {
		get { return ( this.BodyTable.CssClass ); }
		set { this.BodyTable.CssClass = value; }
	}

	public int BodyTableSpacing {
		get { return ( this.BodyTable.CellSpacing ); }
		set { this.BodyTable.CellSpacing = value; }
	}

	public string FormLabelCssClass {
		get {
			object obj = ViewState["FormLabelCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormLabelCssClass"] = value; }
	}

	public string FormInputCssClass {
		get {
			object obj = ViewState["FormInputCssClass"];
			return ( obj == null ? "" : (string) obj );
		}
		set { ViewState["FormInputCssClass"] = value; }
	}

	#endregion

	#region Data

	public string CardholderName {
		get { return ( this.CardholderNameBox.Text ); }
		set { this.CardholderNameBox.Text = value; }
	}

	public string CardType {
		get { return ( this.CardTypeListBox.SelectedValue ); }
		set { this.CardTypeListBox.SelectedValue = value; }
	}

	public string CardNumber {
		get { return ( this.CardNumberBox.Text ); }
		set { this.CardNumberBox.Text = value; }
	}

	public string ExpMM {
		get { return ( this.ExpMMListBox.SelectedValue ); }
		set { this.ExpMMListBox.SelectedValue = value; }
	}

	public string ExpYYYY {
		get { return ( this.ExpYYYYListBox.SelectedValue ); }
		set { this.ExpYYYYListBox.SelectedValue = value; }
	}

	public string CvvNumber {
		get { return ( this.CvvNumberBox.Text ); }
		set { this.CvvNumberBox.Text = value; }
	}

	#endregion

	#endregion

	protected override void OnPreRender( EventArgs e ) {
		if ( this.FormLabelCssClass.Length > 0 ) {
			this.CardholderNameBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.CardTypeListBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.CardNumberBox.LabelStyle.CssClass = this.FormLabelCssClass;
			this.ExpDateLbl.CssClass = this.FormLabelCssClass;
			this.CvvNumberBox.LabelStyle.CssClass = this.FormLabelCssClass;
		}
		if ( this.FormInputCssClass.Length > 0 ) {
			this.CardholderNameBox.CssClass = this.FormInputCssClass;
			this.CardTypeListBox.CssClass = this.FormInputCssClass;
			this.CardNumberBox.CssClass = this.FormInputCssClass;
			this.ExpMMListBox.CssClass = this.FormInputCssClass;
			this.ExpYYYYListBox.CssClass = this.FormInputCssClass;
			this.CvvNumberBox.CssClass = this.FormInputCssClass;
		}
		//this.CCNumValidator.AcceptedCardTypes = CreditCardTypes.Amex | CreditCardTypes.MasterCard | CreditCardTypes.VISA;
		this.WhatsThisLink.CssClass = "CvvLink";
		base.OnPreRender( e );
	}

	// RKP Begin Change - 20080326
	// Replacing the listitem tags with auto-generated items in the ExpYYYY dropdown

	private void LoadYearBox() {
		this.ExpYYYYListBox.Items.Clear();
		int cyear = DateTime.Today.Year;
		for ( int i = 0 ; i < 10 ; i++ ) {
			string year = ( cyear + i ).ToString();
			this.ExpYYYYListBox.Items.Add( new ListItem( year, year ) );
		}
	}

	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
			this.LoadYearBox();
		}
	}
	// RKP End Change - 20080326

}
