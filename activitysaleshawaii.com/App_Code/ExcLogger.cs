﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace ABS {

	public static class ExcLogger {

		/// <summary>
		/// Simple email-reporting method used when the ATCO server throws an exception back
		/// </summary>
		/// <param name="location">Name of the method where the exception was caught</param>
		/// <param name="Msg">Primary message</param>
		/// <param name="SubMsg">Optional secondary message</param>
		public static void SendExceptionReport( string location, string Msg ) {
			SendExceptionReport( location, Msg, "" );
		}

		public static void SendExceptionReport( string location, string Msg, string SubMsg ) {
			try {
				MailMessage mailer = new MailMessage();
				mailer.From = new MailAddress( ConfigMgr.VoucherEmailFromAddress );
				mailer.To.Add( ConfigMgr.ExcReportsToAddress );
				string devAddress = ConfigMgr.DevEmailAddress;
				if ( !string.IsNullOrEmpty( devAddress ) ) {
					mailer.Bcc.Add( devAddress );
				}
				mailer.Body = Msg;
				if ( !string.IsNullOrEmpty( SubMsg ) ) {
					mailer.Body += string.Format( "{0}{0} -- {0}{0}{1}", Environment.NewLine, SubMsg );
				}
				mailer.IsBodyHtml = false;
				mailer.Subject = string.Format( "MC&A Shopping Cart Exception Report - {0}", location );
				SmtpClient client = new SmtpClient();
				client.Send( mailer );
			} catch { }
		}

		/// <summary>
		/// Email reporting for special debugging, developer-only situations
		/// </summary>
		/// <param name="location">Name of the method where the report was generated</param>
		/// <param name="msgs">Report messages</param>
		public static void SendTraceReport( string location, params string[] msgs ) {
			try {
				// Make sure this reporting is turned on (& we have a dev address to send it to)
				if ( !ConfigMgr.SendItinTraceEmails ) { return; }
				MailMessage mailer = new MailMessage();
				mailer.From = new MailAddress( ConfigMgr.VoucherEmailFromAddress );
				mailer.To.Add( ConfigMgr.DevEmailAddress );
				foreach ( string msg in msgs ) {
					mailer.Body += string.Format( "{1}{0}{0} --------------- {0}{0}", Environment.NewLine, msg );
				}
				mailer.IsBodyHtml = false;
				mailer.Subject = string.Format( "MC&A Trace Report - {0}", location );
				SmtpClient client = new SmtpClient();
				client.Send( mailer );
			} catch { }
		}

	}

}
