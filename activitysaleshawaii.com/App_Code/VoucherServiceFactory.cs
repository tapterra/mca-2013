﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	public static class VoucherServiceFactory {

		public static IVoucherService NewVoucherService() {
			IVoucherService result;
			if ( ConfigMgr.IsVoucherDemoMode ) {
				result = new TestDemoVoucherService();
			} else {
				// We can provider-ize this later
				result = new AtcoVoucherService();
			}
			return ( result );
		}

		public static void FlushCache() {
			using ( IVoucherService vs = NewVoucherService() ) {
				vs.FlushCache();
			}
		}

		public static string BuildVouchers(bool FullPage ) {
			using ( IVoucherService vs = NewVoucherService() ) {
				return ( vs.BuildVouchers( FullPage ) );
			}
		}

	}

}
