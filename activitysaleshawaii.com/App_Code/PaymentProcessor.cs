using System;
using System.Configuration;

namespace ABS {

	/// <summary>
	/// IPaymentProcessor base-class implementation.
	/// </summary>
	public abstract class PaymentProcessor : IPaymentProcessor {
		
		#region Properties
		/// <summary>
		/// Name of the processing service (to report to the voucher service).
		/// </summary>
		public string ServiceName {
			get { return ( ConfigMgr.PmtProcessorServiceName ); }
		}
		
		/// <summary>
		/// The customer's name on the credit card
		/// </summary>
		public string CardHolderName { get; set; }
		
		/// <summary>
		/// The type of credit card (visa, mastercard, etc...)
		/// </summary>
		public string CardType { get; set; }

		/// <summary>
		/// The credit card number
		/// </summary>
		public string CardNumber { get; set; }

		/// <summary>
		/// The month the credit card expires on
		/// </summary>
		public int CardExpiresMM { get; set; }

		/// <summary>
		/// The year the credit card expires on
		/// </summary>
		public int CardExpiresYY { get; set; }

		/// <summary>
		/// The CVV number (usually last 3 digit number) on the back of the credit card
		/// </summary>
		public string CardCVVNumber { get; set; }

		/// <summary>
		/// Line 1 of the billing street address for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingStreet1 { get; set; }

		/// <summary>
		/// Line 2 of the billing street address for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingStreet2 { get; set; }

		/// <summary>
		/// The billing city for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingCity { get; set; }

		/// <summary>
		/// The billing state for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingState { get; set; }

		/// <summary>
		/// The billing province for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingProvince { get; set; }

		/// <summary>
		/// The billing zipcode for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingZip { get; set; }

		/// <summary>
		/// The billing country for the customer (usually same as for the credit card)
		/// </summary>
		public string BillingCountry { get; set; }

		/// <summary>
		/// The total amount of payment being charged
		/// </summary>
		public decimal PaymentAmount { get; set; }

		/// <summary>
		/// The authorization code returned by the service for this payment
		/// </summary>
		public string AuthCode { get; set; }

		/// <summary>
		/// The transaction ID assigned by the payment processing service when the payment is submitted.
		/// </summary>
		public string TransactionID { get; set; }

		/// <summary>
		/// The Response Code returned by the payment processing service when the payment is submitted.
		/// </summary>
		public string ResponseCode { get; set; }

		/// <summary>
		/// The Response Message returned by the payment processing service when the payment is submitted.
		/// </summary>
		public string ResponseMessage { get; set; }

		/// <summary>
		/// Set this to true when running the application in demo/test mode.
		/// No actual transactions should be charged to anyone's account.
		/// </summary>
		//public bool IsDemoMode {
		//  get { return ( ConfigMgr.IsPaymentDemoMode ); }
		//}

		/// <summary>
		/// String used for the PAYMENT VIA field on the vouchers
		/// </summary>
		public string PaymentType {
			get { return ( this.CardType ); }
		}

		#endregion
		
		#region Methods

		public abstract PaymentProcessorResponses DoAuthorizeTransaction();

		public abstract PaymentProcessorResponses DoCaptureTransaction();

		// Not currently being used
		public abstract PaymentProcessorResponses DoSaleTransaction();

		#endregion

	}

}
