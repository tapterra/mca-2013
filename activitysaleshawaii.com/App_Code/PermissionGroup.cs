using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;

namespace ABS {

	[SqlTable( "PERMISSION_GROUPS", SingularLabel = "Group", PluralLabel = "Groups", IsMarkedDeleted = true )]
	public class PermissionGroup : BaseSqlPersistable {
		#region Properties

		[SqlColumn( "group_name", DbType.String, Length = 50 )]
		public string GroupName {
			get { return ( groupname ); }
			set { groupname = value; }
		}
		private string groupname = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( comments ); }
			set { comments = value; }
		}
		private string comments = "";

		public ArrayList MemberIDs {
			get { return ( memberids ); }
		}
		private ArrayList memberids = new ArrayList();

		public ArrayList PermissionKeys {
			get { return ( permissionkeys ); }
		}
		private ArrayList permissionkeys = new ArrayList();

		#endregion

		#region Constructors

		// Access the base constructors
		public PermissionGroup() : base() { }
		public PermissionGroup( int ID )
			: base( ID ) {
			this.RestoreMemberIDs();
			this.RestorePermissionKeys();
		}

		// Restore a persisted instance by group-name
		public PermissionGroup( string groupName )
			: base() {
			string sql = String.Format( "SELECT * FROM PERMISSION_GROUPS WHERE group_name = '{0}' ", groupName );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql );
			}
			this.RestoreMemberIDs();
			this.RestorePermissionKeys();
		}

		// Create an unpersisted instance from user-provided property-values
		public PermissionGroup( string groupName, string comments )
			: base() {
			this.ID = -1;
			this.GroupName = groupName;
			this.Comments = comments;
		}

		#endregion

		#region Public Methods
		public override bool IsValidToPersist() {
			this.ValidationMessages.Clear();
			bool isValid = false;
			PermissionGroup testGroup = new PermissionGroup();
			// Make sure GroupName is unique
			if ( this.GroupName != "" ) {
				string sql = String.Format( "select * from PERMISSION_GROUPS where group_name = '{0}' ", this.GroupName );
				using ( SqlDatabase db = new SqlDatabase() )
					db.SelectInstance( testGroup, sql );
				isValid = ( testGroup.ID == this.ID || testGroup.ID == -1 );
				if ( !isValid ) {
					this.ValidationMessages.Add( "Another group in the database has the same Group Name - please enter a unique value." );
				}
			}
			if ( !isValid )
				return ( false );
			return ( base.IsValidToPersist() );
		}

		// Persist - saves this instance to the database
		public override bool Persist() {
			bool isValid = this.IsValidToPersist();
			if ( isValid ) {
				base.Persist();
				this.PersistMemberIDs();
				this.PersistPermissionKeys();
			}
			return ( isValid );
		}

		// Restore - restores this instance from it's database record
		public override void Restore() {
			base.Restore();
			this.RestoreMemberIDs();
			this.RestorePermissionKeys();
		}

		// Delete - removes the record for this instance (ID) from the database
		public override bool Delete() {
			bool isValid = this.IsValidToDelete();
			if ( isValid ) {
				// Delete any existing group-memberships & permission-keys first
				this.DeleteMemberIDs( false );
				this.DeletePermissionKeys( false );
				// Now we can delete the group record
				base.Delete();
			}
			return ( isValid );
		}

		#endregion

		#region Private methods for working with group memberships

		// PersistMemberIDs - Save the MemberIDs list for this group to the database
		protected void PersistMemberIDs() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid Group ID
			// First, we want to delete any existing GROUP_USERS records
			this.DeleteMemberIDs( false );
			// Now, we can INSERT a new GROUP_USERS record for each MemberIDs item
			using ( SqlDatabase db = new SqlDatabase() ) {
				foreach ( int memberID in this.MemberIDs ) {
					string sql = String.Format( "INSERT INTO GROUP_USERS ( user_id, group_id ) VALUES ( {0}, {1} );", memberID, this.ID );
					db.Execute( sql );
				}
			}
		}

		// RestoreMemberIDs - Load the MemberIDs list for this group's member users
		protected void RestoreMemberIDs() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			string sql = String.Format( "SELECT user_id FROM GROUP_USERS WHERE group_id = '{0}' ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					while ( rdr.Read() ) {
						this.MemberIDs.Add( rdr.GetInt32( rdr.GetOrdinal( "user_id" ) ) );
					}
				}
			}
		}

		// DeleteMemberIDs - Delete all the group-membership records for this group
		protected void DeleteMemberIDs() {
			this.DeleteMemberIDs( true );
		}

		protected void DeleteMemberIDs( bool clearList ) {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid Group ID
			string sql = String.Format( "DELETE FROM GROUP_USERS WHERE group_id = '{0}' ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql );
			}
			if ( clearList )
				this.MemberIDs.Clear(); // don't need these no mo...
		}

		#endregion

		#region Private methods for working with the group's permission-keys

		// PersistPermissionKeys - Save the PermissionKeys list for this group to the database
		protected void PersistPermissionKeys() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid Group ID
			// First, we want to delete any existing GROUP_PERMISSIONS records
			this.DeletePermissionKeys( false );
			// Now, we can INSERT a new GROUP_PERMISSIONS record for each PermissionKeys item
			using ( SqlDatabase db = new SqlDatabase() ) {
				foreach ( string key in this.PermissionKeys ) {
					string sql = String.Format( "INSERT INTO GROUP_PERMISSIONS ( group_id, permission_key ) VALUES ( {0}, '{1}' );", this.ID, key );
					db.Execute( sql );
				}
			}
		}

		// RestorePermissionKeys - Load the PermissionKeys list for this group's permissions
		protected void RestorePermissionKeys() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			string sql = String.Format( "SELECT permission_key FROM GROUP_PERMISSIONS WHERE group_id = '{0}' ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					while ( rdr.Read() ) {
						this.PermissionKeys.Add( rdr.GetString( rdr.GetOrdinal( "permission_key" ) ) );
					}
				}
			}
		}

		// DeletePermissionKeys - Delete all the permission-key records for this group
		protected void DeletePermissionKeys() {
			this.DeletePermissionKeys( true );
		}

		protected void DeletePermissionKeys( bool clearList ) {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid Group ID
			string sql = String.Format( "DELETE FROM GROUP_PERMISSIONS WHERE group_id = '{0}' ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql );
			}
			if ( clearList )
				this.PermissionKeys.Clear(); // don't need these no mo...
		}

		#endregion

		#region Static (search) Members

		#region Methods

		/// <summary>
		/// Clear all the static search properties
		/// </summary>
		public static void NewSearch() {}

		/// <summary>
		/// Generates a SQL select statement based on the curent values of the static search properties
		/// </summary>
		/// <returns>a SQL select statement</returns>
		public static string GetSearchSql() {
			// No deleted records
			string sql = "select * from PERMISSION_GROUPS where ( is_deleted = 0 )";
			if ( string.IsNullOrEmpty( SortExpression ) )
				SortExpression = "group_name";
			sql += string.Format( " ORDER BY {0}", SortExpression );
			return ( sql );
		}

		/// <summary>
		/// Execute the search based on the current values of the search-value properties.
		/// </summary>
		/// <returns>A list of instances matching the current search criteria</returns>
		public static List<PermissionGroup> DoSearchList() {
			return ( SqlDatabase.DoSearch<PermissionGroup>( PermissionGroup.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
