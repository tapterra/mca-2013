using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Collections.ObjectModel;

namespace ABS {

	[SqlTable( "SUBCATEGORIES", SingularLabel = "Subcategory", PluralLabel = "Subcategories", IsMarkedDeleted = false )]
	public class Subcategory : BaseSqlPersistable {

		#region Properties

		//[category_id] [int] NOT NULL,
		[SqlColumn( "category_id", DbType.Int32 )]
		public int CategoryID {
			get { return ( this.categoryID ); }
			set { this.categoryID = value; }
		}
		private int categoryID = -1;

		//[seq] [int] NOT NULL,
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//[label] [nvarchar](50) NOT NULL,
		[SqlColumn( "label", DbType.String, Length = 50 )]
		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label;

		//[comments] [ntext] NOT NULL,
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		#region Constructors

		// Access the base constructors
		public Subcategory() : base() { }

		public Subcategory( int ID ) : base( ID ) { }

		#endregion

		#region Methods

		#region BaseSqlPersistable overrides

		public override bool IsValidToDelete() {
			this.ValidationMessages.Clear();
			string sql = "select top 1 id from ACTIVITY_GROUPS where subcategory_id = @subcategory_id and is_deleted = 0";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@subcategory_id", this.ID ) ) ) {
					if ( rdr.Read() ) {
						this.ValidationMessages.Add( "This Subcategory can't be deleted because it is used by one or more Activity Groups." );
						return ( false );
					}
				}
			}
			return base.IsValidToDelete();
		}

		public override bool Delete() {
			if ( !this.IsValidToDelete() ) {
				return ( false );
			}
			return ( base.Delete() );
		}

		#endregion

		/// <summary>
		/// Use this for generating datasources for the publishers
		/// </summary>
		public List<ActivityGroup> ActivityGroups {
			get { return ( this.activityGroups ); }
		}
		private List<ActivityGroup> activityGroups = new List<ActivityGroup>();

		/// <summary>
		/// For the given Category and Island, generate a list of Subcategories with their associated ActivityGroups
		/// </summary>
		public static List<Subcategory> GetCatActivityGroups( Category category, int islandID ) {
			List<Subcategory> results = new List<Subcategory>();
			Website site = Website.Current;
			using ( SqlDatabase db = new SqlDatabase() ) {
				foreach ( Subcategory subcat in category.Subcategories ) {
					subcat.ActivityGroups.Clear();
					string sql = "select * from ACTIVITY_GROUPS where ( website_id = @website_id and subcategory_id = @subcategory_id and island_id = @island_id and is_deleted = 0 and is_approved = 1 ) order by id";
					using ( IDataReader rdr = db.SelectRecords( sql,
						db.NewParameter( "@website_id", site.ID ),
						db.NewParameter( "@subcategory_id", subcat.ID ),
						db.NewParameter( "@island_id", islandID ) 
					) ) {
						ActivityGroup group = new ActivityGroup();
						while ( SqlDatabase.InstantiateFromDataRdr( rdr, group ) ) {
							subcat.ActivityGroups.Add( group );
							group = new ActivityGroup();
						}
					}
					if ( subcat.ActivityGroups.Count > 0 ) {
						results.Add( subcat );
					}
				}
			}
			return ( results );
		}

		#endregion

	}

}
