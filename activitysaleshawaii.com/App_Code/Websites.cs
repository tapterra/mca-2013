using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using StarrTech.WebTools.Data;
using System.Data;
using System.Web.Caching;

namespace ABS {

	public class Websites : Dictionary<int, Website> {

		private static string CacheKey = "WEBSITES";

		/// <summary>
		/// Returns the current (cached) list of known websites
		/// </summary>
		public static Websites Current {
			get {
				Websites current = HttpContext.Current.Cache[CacheKey] as Websites;
				if ( current == null ) {
					current = Websites.CurrentReload;
				}
				return ( current );
			}
		}

		/// <summary>
		/// Called by the admin apps to always force a reload on the Current list
		/// </summary>
		public static Websites CurrentReload {
			get {
				Websites current = new Websites();
				HttpContext.Current.Cache.Insert( CacheKey, current, null, DateTime.Now.AddHours( 1 ), Cache.NoSlidingExpiration );
				return ( current );
			}
		}

		/// <summary>
		/// Returns a list of all non-nopayment sites (eligible for promo codes)
		/// </summary>
		public static Dictionary<int, Website> CurrentPaySites {
			get { return ( LoadNonNoPayItems() ); }
		}

		private Websites() : base() {
			this.LoadItems();
		}

		private void LoadItems() {
			string sql = "select * from WEBSITES where is_deleted = 0 order by name";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Website website = new Website();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, website ) ) {
						this.Add( website.ID, website );
						website = new Website();
					}
				}
			}
		}

		private static Dictionary<int, Website> LoadNonNoPayItems() {
			Dictionary<int, Website> results = new Dictionary<int, Website>();
			string sql = "select * from WEBSITES where is_nopayment_site = 0 and is_deleted = 0 order by name";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Website website = new Website();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, website ) ) {
						results.Add( website.ID, website );
						website = new Website();
					}
				}
			}
			return ( results );
		}

	}

}
