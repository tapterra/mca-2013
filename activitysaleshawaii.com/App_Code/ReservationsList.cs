using System;
using System.Collections;
using System.Collections.Generic;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of Reservations in an Itinerary
	/// </summary>
	public class ReservationsList : List<Reservation> {

		#region Properties

		public Itinerary Owner {
			get { return ( this.owner ); }
		}
		private Itinerary owner = null;

		/// <summary>
		/// Total of the prices for all Reservations in this list
		/// </summary>
		public decimal TotalPrice {
			get {
				decimal result = 0M;
				foreach ( Reservation b in this ) {
					result += b.TotalPrice;
				}
				return ( result );
			}
		}

		#endregion

		#region Constructors

		private ReservationsList() { }

		public ReservationsList( Itinerary owner )
			: base() {
			this.owner = owner;
			this.LoadItems();
		}

		#endregion

		private void LoadItems() {
			if ( this.owner == null || this.owner.ID <= 0 ) {
				return;
			}
			string sql = "select * from RESERVATION_BOOKINGS where ( itinerary_id = @itinerary_id ) order by id";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@itinerary_id", this.owner.ID ) ) ) {
					Reservation item = new Reservation( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new Reservation( this.owner );
					}
				}
			}
		}

		protected internal void Persist() {
			for ( int i = 0 ; i < this.Count ; ) {
				Reservation item = this[i];
				if ( !item.IsBookingCompleted ) {
					this.Remove( item );
					item.Delete();
				} else {
					item.ItineraryID = this.Owner.ID;
					item.Persist();
					i++;
				}
			}
		}

	}

}
