using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using StarrTech.WebTools.Data;

namespace ABS {

	public class PaymentsList : List<Payment> {

		private Itinerary owner;

		private PaymentsList() { }

		public PaymentsList( Itinerary itinerary ) : base() {
			this.owner = itinerary;
			this.LoadItems();
		}

		private void LoadItems() {
			if ( this.owner == null ) {
				return;
			}
			string sql = "select * from PAYMENTS where ( itinerary_id = @itinerary_id and is_deleted = 0 ) order by posted_dt desc";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@itinerary_id", this.owner.ID ) ) ) {
					Payment pmt = new Payment( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, pmt ) ) {
						this.Add( pmt );
						pmt = new Payment( this.owner );
					}
				}
			}
		}

		public decimal Total {
			get {
				decimal result = 0M;
				foreach ( Payment item in this ) {
					result += item.Amount;
				}
				return ( result );
			}
		}

		protected internal void Persist() {
			foreach ( Payment item in this ) {
				item.ItineraryID = owner.ID;
				item.Persist();
			}
		}

	}

}
