using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Imaging;
using System.Text;
using System.Web;

using StarrTech.WebTools.Data;
using StarrTech.WebTools.CMS;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace ABS {

	[SqlTable( "IMAGE_DOCUMENT", SingularLabel = "Image", PluralLabel = "Images", IsMarkedDeleted = true )]
	public class ImageDocument : BaseSqlPersistable {

		#region Properties

		/// <summary>
		/// Comma-delimited list of acceptable mime-types for posted files (see HtmlInputFile.Accept).
		/// </summary>
		public string AcceptedTypes {
			get { return ( "image/*" ); }
			set { }
		}

		#region Database Columns

		/// <summary>
		/// Date/time the file was uploaded
		/// </summary>
		[SqlColumn( "posted_dt", DbType.DateTime )]
		public DateTime PostedDT {
			get { return ( this.postedDT ); }
			set { this.postedDT = value; }
		}
		private DateTime postedDT = DateTime.Now;

		/// <summary>
		/// Content MIME type of the document
		/// </summary>
		[SqlColumn( "mime_type", DbType.String, Length = 50 )]
		public string MimeType {
			get { return ( this.mimeType ); }
			set { this.mimeType = value; }
		}
		private string mimeType = "";

		/// <summary>
		/// Name of the uploaded/downloaded file
		/// </summary>
		[SqlColumn( "file_name", DbType.String, Length = 200 )]
		public string FileName {
			get { return ( this.fileName ); }
			set { this.fileName = value; }
		}
		private string fileName = "";

		/// <summary>
		/// Actual content of the document
		/// </summary>
		// RKP: Begin Change - 20090205
		[SqlColumn( "document_data", DbType.Binary )]
		public virtual byte[] DocumentData {
			get { return ( this.documentData ); }
			set { this.documentData = value; }
		}
		private byte[] documentData = null;

		//// RKP: For performance sake, we don't want to map this property to it's column in the database
		//// RKP: We only want to load this data when it's needed by the HttpHandlers for display/download
		//protected internal virtual byte[] DocumentData {
		//  get {
		//    if ( this.documentData == null ) {
		//      this.RestoreDocumentData();
		//    }
		//    return ( this.documentData );
		//  }
		//  set { this.documentData = value; }
		//}
		//private byte[] documentData = null;

		///// <summary>
		///// Restores the unmapped DocumentData property from the database
		///// </summary>
		//protected void RestoreDocumentData() {
		//  if ( this.ID <= 0 ) {
		//    return; // Nothing to do if the instance hasn't been persisted yet
		//  }
		//  string sql = "select document_data from IMAGE_DOCUMENT where id = @id";
		//  using ( SqlDatabase db = new SqlDatabase() ) {
		//    using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@id", this.ID ) ) ) {
		//      while ( rdr.Read() ) {
		//        this.documentData = new byte[this.dataSize];
		//        rdr.GetBytes( 0, 0, this.documentData, 0, this.dataSize );
		//      }
		//    }
		//  }
		//}

		///// <summary>
		///// Persists the unmapped DocumentData property to the database
		///// </summary>
		//protected void PersistDocumentData() {
		//  if ( this.IsValidToPersist() ) {
		//    if ( this.ID <= 0 ) {
		//      this.Persist();
		//    }
		//    string sql = "update IMAGE_DOCUMENT set document_data = @document_data where id = @id";
		//    using ( SqlDatabase db = new SqlDatabase() ) {
		//      db.Execute( sql, db.NewParameter( "@id", this.ID ), 
		//        db.NewParameter( "@document_data", this.documentData ) 
		//      );
		//    }
		//  }
		//}
		// RKP: End Change

		/// <summary>
		/// Number of bytes in document data
		/// </summary>
		[SqlColumn( "data_size", DbType.Int32 )]
		public int DataSize {
			get { return ( this.dataSize ); }
			set { this.dataSize = value; }
		}
		private int dataSize;

		#endregion

		#region Publishing

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static string UrlFormat = "ImageDocument.axd?id={0}&sc={1}&mw={2}&mh={3}&ax={4}";

		/// <summary>
		/// String.Format template for this document's URL.
		/// </summary>
		public static string DownloadUrlFormat = "ImageDocument.axd?id={0}&ax=1";

		/// <summary>
		/// String.Format template for this (image) document's display URL.
		/// </summary>
		public static string ImageUrlFormat = "ImageDocument.axd?id={0}";

		/// <summary>
		/// String.Format template (with scaling) for this document's thumbnail display URL.
		/// </summary>
		public static string ThumbnailUrlFormat = "ImageDocument.axd?id={0}&mw={1}";

		/// <summary>
		/// Returns the URL required to download this document (via the CMSDocumentHandler).
		/// Note: Assumes that the following handler is installed in the &lt;httpHandlers&gt; section of web.config:
		/// &lt;add verb="GET" path="ImageDocument.axd" type="StarrTech.WebTools.CMS.CMSDocumentHandler, StarrTech.WebTools" /&gt;
		/// </summary>
		public string DownloadUrl {
			get { return ( string.Format( DownloadUrlFormat, this.ID ) ); }
		}

		/// <summary>
		/// Returns the URL used to display a scaled thumbnail version of this (image) document
		/// </summary>
		public string ThumbnailUrl {
			get { return ( string.Format( ThumbnailUrlFormat, this.ID, 130 ) ); }
		}

		public string ThumbUrl( int wd ) {
			return ( string.Format( ThumbnailUrlFormat, this.ID, wd ) );
		}

		/// <summary>
		/// Returns the URL used to display a full-scale version of this (image) document
		/// </summary>
		public string ImageUrl {
			get { return ( string.Format( ImageUrlFormat, this.ID ) ); }
		}

		/// <summary>
		/// Generates an appropriate ImageFormat object for this document's Mime Type (null for non-image types)
		/// </summary>
		public ImageFormat ImageFormat {
			get {
				switch ( this.MimeType ) {
					case MimeTypes.ImageBmp:
						return ( ImageFormat.Bmp );
					case MimeTypes.ImageGif:
						return ( ImageFormat.Gif );
					case MimeTypes.ImageJpeg:
					case "image/pjpeg":
						return ( ImageFormat.Jpeg );
					case MimeTypes.ImagePng:
						return ( ImageFormat.Png );
					case MimeTypes.ImageEmf:
						return ( ImageFormat.Emf );
					case MimeTypes.ImageTiff:
						return ( ImageFormat.Tiff );
					case MimeTypes.ImageExif:
						return ( ImageFormat.Exif );
					case MimeTypes.ImageWmf:
						return ( ImageFormat.Wmf );
					default:
						return ( null );
				}
			}
		}

		/// <summary>
		/// Returns true if the MimeType is that of an image/graphic.
		/// </summary>
		public bool IsImage {
			get { return ( this.ImageFormat != null ); }
		}

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Create a new, empty instance
		/// </summary>
		public ImageDocument() : base() { }

		/// <summary>
		/// Restore a previously-persisted instance by ID
		/// </summary>
		/// <param name="ID">Database-record ID of the instance to restore</param>
		public ImageDocument( int ID ) : base( ID ) { }


		public ImageDocument( HttpPostedFile postedFile )	: base() {
			this.LoadPostedFile( postedFile );
		}

		#endregion

		#region Upload/Download Methods

		/// <summary>
		/// Given an HttpPostedFile, load the related properties
		/// </summary>
		/// <param name="postedFile">An HttpPostedFile instance (as provided by HtmlInputFile.PostedFile).</param>
		public virtual void LoadPostedFile( HttpPostedFile postedFile ) {
			if ( postedFile.ContentLength <= 0 )
				return;
			this.MimeType = postedFile.ContentType;
			this.DataSize = postedFile.ContentLength;
			this.FileName = postedFile.FileName.Substring( postedFile.FileName.LastIndexOf( "\\" ) + 1 );
			this.documentData = new byte[this.DataSize];
			int n = postedFile.InputStream.Read( this.documentData, 0, this.DataSize );
			// RKP: Begin Change - 20090205
			//this.PersistDocumentData();
			// RKP: End Change
		}

		/// <summary>
		/// Given an HttpResponse instance, write the document to the response as a downloaded file.
		/// </summary>
		/// <param name="response">The HttpResponse instance to write to</param>
		public void DownloadDocument( HttpResponse Response ) {
			DownloadDocument( Response, true, 0, 0, 0 );
		}

		/// <summary>
		/// Write the document to the given HttpResponse, with an attachment-header if indicated,
		/// and scaled to a given percentage, maximum width, and/or max height (if the document is an image).
		/// </summary>
		/// <param name="response">The HttpResponse instance to write to</param>
		/// <param name="AsAttachment">A switch ti indicate the need for an attachment-header</param>
		/// <param name="ScalePct">An integer percentage-value to scale the image by.</param>
		/// <param name="MaxWidth">An integer giving the maximum width in pixels desired (after scaling).</param>
		/// <param name="MaxHeight">An integer giving the maximum height in pixels desired (after scaling).</param>
		public void DownloadDocument( HttpResponse Response, bool AsAttachment, int ScalePct, int MaxWidth, int MaxHeight ) {
			Response.Clear();
			Response.ContentType = this.MimeType;
			if ( this.MimeType == "image/pjpeg" )
				Response.ContentType = "image/jpeg";
			if ( ( ScalePct == 0 || ScalePct == 100 ) && MaxWidth == 0 && MaxHeight == 0 ) {
				if ( AsAttachment ) {
					Response.AppendHeader( "Content-Disposition", string.Format( "attachment; filename={0}", this.FileName ) );
				}
				Response.AppendHeader( "Content-Length", this.DataSize.ToString() );
				Response.BinaryWrite( this.DocumentData );
			} else {
				this.DrawThumbnail( Response, ScalePct, MaxWidth, MaxHeight );
			}
			Response.Flush();
		}

		/// <summary>
		/// Scales the image document into a thumbnail
		/// </summary>
		/// <param name="ScalePct">An integer percentage-value to scale the image by.</param>
		/// <param name="MaxWidth">An integer giving the maximum width in pixels desired (after scaling).</param>
		/// <param name="MaxHeight">An integer giving the maximum height in pixels desired (after scaling).</param>
		public void DrawThumbnail( HttpResponse Response, int ScalePct, int MaxWidth, int MaxHeight ) {
			// This only works for image documents
			if ( this.ImageFormat == null )
				return;
			System.Drawing.Image pic = null;
			Bitmap bmp = null;
			Graphics graf = null;
			MemoryStream rawStream = null;
			if ( ScalePct == 0 )
				ScalePct = 100;
			double Scale = ScalePct / 100D;
			try {
				// Load the main image
				rawStream = new MemoryStream();
				rawStream.Write( this.DocumentData, 0, this.DataSize );
				pic = System.Drawing.Image.FromStream( rawStream );

				// Compute the new dimensions
				int ht = Convert.ToInt32( pic.Height * Scale );
				int wd = Convert.ToInt32( pic.Width * Scale );
				if ( MaxWidth > 0 ) {
					//if ( wd > MaxWidth ) {
						double wScale = Convert.ToDouble( MaxWidth ) / Convert.ToDouble( wd );
						wd = Convert.ToInt32( wd * wScale );
						ht = Convert.ToInt32( ht * wScale );
					//}
				}
				if ( MaxHeight > 0 ) {
					//if ( ht > MaxHeight ) {
						double hScale = Convert.ToDouble( MaxHeight ) / Convert.ToDouble( ht );
						wd = Convert.ToInt32( wd * hScale );
						ht = Convert.ToInt32( ht * hScale );
					//}
				}
				// Set up the canvas
				bmp = new Bitmap( wd, ht );
				graf = Graphics.FromImage( bmp );
				graf.Clear( Color.White );
				graf.DrawRectangle( new Pen( Color.LightGray, 1 ), 0, 0, wd - 1, ht - 1 );
				graf.SmoothingMode = SmoothingMode.HighQuality;
				graf.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graf.PixelOffsetMode = PixelOffsetMode.HighQuality;

				// Draw the thumbnail
				Rectangle destRect = new Rectangle( 4, 4, wd - 8, ht - 8 );
				Rectangle srcRect = new Rectangle( 0, 0, pic.Width, pic.Height );
				graf.DrawImage( pic, destRect, srcRect, GraphicsUnit.Pixel );

				// Write the image & clean up
				bmp.Save( Response.OutputStream, this.ImageFormat );
				Response.Flush();
				//Response.End();
			} catch ( Exception e ) {
				string msg = e.Message;
			} finally {
				if ( rawStream != null )
					rawStream.Close();
				if ( pic != null )
					pic.Dispose();
				if ( bmp != null )
					bmp.Dispose();
				if ( graf != null )
					graf.Dispose();
			}
		}

		#endregion

		#region SqlPersistable Overrides

		/// <summary>
		/// Verify that it's OK to persist this User
		/// Overridden to verify that WindowsName and EmailAddress are unique
		/// </summary>
		/// <returns>True if it's OK to persist</returns>
		public override bool IsValidToPersist() {
			this.ValidationMessages.Clear();
			bool isValid = this.MimeType.StartsWith( "image/" );
			if ( !isValid ) {
				this.ValidationMessages.Add( "The file you selected to upload is not a valid image type." );
				if ( this.ID > 0 )
					this.Restore();
				return ( false );
			}
			return ( base.IsValidToPersist() );
		}

		#endregion

	}

}
