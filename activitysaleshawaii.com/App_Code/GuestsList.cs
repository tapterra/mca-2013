using System;
using System.Collections;
using System.Collections.Generic;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of Guests in an Booking
	/// </summary>
	public class GuestsList : List<Guest> {

		#region Properties

		/// <summary>
		/// The Booking this list is holding Guests for.
		/// </summary>
		public Booking Owner {
			get { return ( this.owner ); }
		}
		private Booking owner = null;

		/// <summary>
		/// Total undiscounted price for all guests before taxes
		/// </summary>
		public decimal TotalBasePrice {
			get {
				decimal result = 0M;
				foreach ( Guest g in this ) {
					result += g.BasePrice;
				}
				return ( result );
			}
		}

		/// <summary>
		/// The sales-tax to be applied to this guest-list's ticket sales
		/// </summary>
		//public decimal Tax {
		//  get {
		//    if ( this.Owner == null ) {
		//      return ( 0M );
		//    }
		//    return ( this.BasePrice * this.Owner.Activity.TaxRate ); 
		//  }
		//}

		/// <summary>
		/// The total (base + tax) charge for this guest-list
		/// </summary>
		//public decimal TotalPrice {
		//  get {
		//    if ( this.Owner == null ) {
		//      return ( 0M );
		//    }
		//    return ( this.BasePrice + this.Tax ); 
		//  }
		//}

		public string ResponsesString {
			get {
				string result = "";
				foreach ( Guest g in this ) {
					string options = g.OptionResponses.ResponsesString;
					if ( !string.IsNullOrEmpty( options ) ) {
						result += string.Format( "{0}: {1}{2}", g.FullName, options, Environment.NewLine );
					}
				}
				return ( result );
			}
		}
		
		#endregion
		
		#region Constructors
		
		private GuestsList() {}
		
		public GuestsList( Booking owner ) : base() {
			this.owner = owner;
			this.LoadItems();
		}
		
		#endregion

		private void LoadItems() {
			if ( this.owner == null || this.owner.ID <= 0 ) {
				return;
			}
			string sql = "select * from GUESTS where ( booking_id = @booking_id and is_deleted = 0 ) order by id";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@booking_id", this.owner.ID ) ) ) {
					Guest item = new Guest( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new Guest( this.owner );
					}
				}
			}
		}

		public Guest GetItem( string priceCodeUid, int seq ) {
			foreach ( Guest g in this ) {
				if ( g.PriceCodeUID == priceCodeUid && g.Seq == seq ) {
					return ( g );
				}
			}
			return ( null );
		}

		internal void Persist() {
			foreach ( Guest item in this ) {
				item.BookingID = this.Owner.ID;
				item.Persist();
			}
		}

	}
	
}
