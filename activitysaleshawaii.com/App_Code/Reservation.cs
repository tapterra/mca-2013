using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using StarrTech.WebTools.Data;
using System.Web;

namespace ABS {

	/// <summary>
	/// Summary description for Reservation
	/// </summary>
	[SqlTable( "RESERVATION_BOOKINGS", SingularLabel = "Reservation", PluralLabel = "Reservations" )]
	public class Reservation : BaseSqlPersistable {

		#region Properties

		public static Reservation Current {
			get { return ( HttpContext.Current.Session["CurrentReservation"] as Reservation ); }
			set { HttpContext.Current.Session["CurrentReservation"] = value; }
		}

		#region Database Columns

		[SqlColumn( "is_processed", DbType.Boolean )]
		public bool IsProcessed {
			get { return ( this.isProcessed ); }
			set { this.isProcessed = value; }
		}
		private bool isProcessed = false;

		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDT {
			get { return ( this.createdDT ); }
			set { this.createdDT = value; }
		}
		private DateTime createdDT = DateTime.Now;

		[SqlColumn( "itinerary_id", DbType.Int32 )]
		public int ItineraryID {
			get { return ( this.itineraryID ); }
			set { this.itineraryID = value; }
		}
		private int itineraryID = -1;

		[SqlColumn( "activity_id", DbType.Int32 )]
		public int ActivityID {
			get { return ( this.activityID ); }
			set { this.activityID = value; }
		}
		private int activityID = -1;

		[SqlColumn( "first_name", DbType.String, Length = 50 )]
		public string FirstName {
			get { return ( this.firstName ); }
			set { this.firstName = value; }
		}
		private string firstName;

		[SqlColumn( "last_name", DbType.String, Length = 100 )]
		public string LastName {
			get { return ( this.lastName ); }
			set { this.lastName = value; }
		}
		private string lastName;

		[SqlColumn( "pax_count", DbType.Int32 )]
		public int PaxCount {
			get { return ( this.paxCount ); }
			set { this.paxCount = value; }
		}
		private int paxCount;

		[SqlColumn( "res_date", DbType.DateTime )]
		public DateTime ResDate {
			get { return ( this.resDate ); }
			set { this.resDate = value; }
		}
		private DateTime resDate;

		[SqlColumn( "pref_time", DbType.String, Length = 20 )]
		public string PrefTime {
			get { return ( this.prefTime ); }
			set { this.prefTime = value; }
		}
		private string prefTime;

		[SqlColumn( "alt_time", DbType.String, Length = 20 )]
		public string AltTime {
			get { return ( this.altTime ); }
			set { this.altTime = value; }
		}
		private string altTime;

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		public string ProcessedMark {
			get { return ( this.IsProcessed ? "&bull;" : "&nbsp;" ); }
		}
		
		public string CreatedDTStr {
			get { return ( this.CreatedDT.ToString() ); }
		}

		public string ResDateStr {
			get { return ( this.ResDate.ToString( "d" ) ); }
		}

		public string ResDateString {
			get { return ( this.ResDate.ToString( "dddd, MMMM d, yyyy" ) ); }
		}

		public string FullNameLF {
			get { return ( string.Format( "{0}, {1}", this.LastName, this.FirstName ) ); }
		}

		public Activity Activity {
			get {
				if ( this.activity == null ) {
					this.activity = new Activity( this.activityID );
				}
				return ( this.activity ); 
			}
			set {
				this.activity = value;
				this.ActivityID = value.ID;
			}
		}
		private Activity activity;

		public Itinerary Itinerary {
			get {
				if ( this.itinerary == null ) {
					this.itinerary = new Itinerary( this.ItineraryID );
				}
				return ( this.itinerary );
			}
			set {
				this.itinerary = value;
				this.ItineraryID = value.ID;
			}
		}
		private Itinerary itinerary;

		public string ActivityTitle {
			get { return ( this.Activity.ItineraryTitle ); }
		}

		public int WebsiteID {
			get { return ( this.Activity.ActivityGroup.WebsiteID ); }
		}

		public string WebsiteName {
			get { return ( this.Activity.ActivityGroup.WebsiteName ); }
		}

		public Customer Customer {
			get { return ( this.Itinerary.Customer ); }
		}

		public string CustomerName {
			get { return ( this.Customer.FullNameLF ); }
		}

		/// <summary>
		/// Base (pre-tax) price for this booking
		/// </summary>
		public decimal BasePrice {
			get { return ( this.Activity.ActivityGroup.ReservationPrice ); }
		}

		/// <summary>
		/// Tax charges for this booking
		/// </summary>
		public decimal Tax {
			get { return ( 0M ); }
		}

		/// <summary>
		/// Total charge for this booking
		/// </summary>
		public decimal TotalPrice {
			get { return ( this.BasePrice + this.Tax ); }
		}
		public string TotalPriceStr {
			get { return ( string.Format( "US$ {0:F}", this.TotalPrice ) ); }
		}

		/// <summary>
		/// Flag to determine if the booking process was completed by the user
		/// </summary>
		public bool IsBookingCompleted {
			get {
				return ( !( string.IsNullOrEmpty( this.FirstName ) || string.IsNullOrEmpty( this.LastName ) || this.PaxCount <= 0 || this.ResDate == DateTime.MinValue ||
					string.IsNullOrEmpty( this.PrefTime ) || string.IsNullOrEmpty( this.AltTime ) ) );
			}
		}

		/// <summary>
		/// Flag set when the system fails to reserve the booking. 
		/// Itinerary UI will highlight the booking for revision by the user.
		/// </summary>
		public bool IsErrorHighlighted {
			get { return ( false ); }
			set { }
		}

		#endregion

		#region Constructors
		// Access the base constructors
		public Reservation() : base() {
			this.Comments = "";
			this.IsProcessed = false;
		}

		public Reservation( int ID ) : base( ID ) { }

		public Reservation( Activity Activity ) : this() {
			// Load the Activity being booked...
			this.Activity = Activity;
		}

		protected internal Reservation( Itinerary owner ) : this() {
			this.itineraryID = owner.ID;
		}

		#endregion


		#region Static (search) Members

		#region Properties

		public static int SWebsiteID {
			get { return ( sWebsiteID ); }
			set { sWebsiteID = value; }
		}
		private static int sWebsiteID = 0;

		public static int SProcessed {
			get { return ( sProcessed ); }
			set { sProcessed = value; }
		}
		private static int sProcessed = 0;

		#endregion

		#region Admin Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			Reservation.SProcessed = 0;
			Reservation.SWebsiteID = 0;
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";

			//AddSearchExpr( whereBuilder, "created_dt", Reservation.SStartDate, Reservation.SEndDate, ref delim );

			switch ( Reservation.SProcessed ) {
				case -1: // find unapproved
					AddSearchExpr( whereBuilder, "r.is_processed", false, ref delim );
					break;
				case 1: // find approved
					AddSearchExpr( whereBuilder, "r.is_processed", true, ref delim );
					break;
			}

			//AddSearchExpr( whereBuilder, "last_name", SLastName, ref delim );
			AddSearchExpr( whereBuilder, "w.id", SWebsiteID, ref delim );

			string sql = @"select r.*, w.id from RESERVATION_BOOKINGS as r ";

			sql += "left join ACTIVITIES as a on a.id = r.activity_id ";
			sql += "left join ACTIVITY_GROUPS as g on g.id = a.activity_group_id ";
			sql += "left join WEBSITES as w on w.id = g.website_id ";

			if ( whereBuilder.Length != 0 ) {
				sql += String.Format( " where ( {0} ) ", whereBuilder );
			}

			if ( !string.IsNullOrEmpty( Reservation.SortExpression ) ) {
				sql += string.Format( " order by {0}", Reservation.SortExpression );
			}
			return ( sql );
		}

		/// <summary>
		/// Executes the search defined by GetSearchSql, and uses SqlDatabase to generate 
		/// an IList of instances from the results.
		/// </summary>
		public static List<Reservation> DoSearchList() {
			return ( SqlDatabase.DoSearch<Reservation>( Reservation.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
