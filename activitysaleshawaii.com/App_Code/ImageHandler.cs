using System;
using System.Web;

namespace ABS {

	/// <summary>
	/// Summary description for Imagehandler
	/// </summary>
	public class ImageHandler : IHttpHandler {

		/// <summary>
		/// This handler is marked as reusable and always return true.
		/// </summary>
		public bool IsReusable {
			get { return ( false ); }
		}

		/// <summary>
		/// Process the request
		/// </summary>
		/// <param name="context">The current HTTP context, from which we get to access the Request and the Response.</param>
		public virtual void ProcessRequest( HttpContext context ) {
			// Parse the query string 
			string idStr = context.Request.QueryString["id"];
			string scaleStr = context.Request.QueryString["sc"];
			string widthStr = context.Request.QueryString["mw"];
			string heightStr = context.Request.QueryString["mh"];
			string attachStr = context.Request.QueryString["ax"];
			if ( idStr != null && idStr != "" ) {
				try {
					// Determine the proper parameter values
					int id = Convert.ToInt32( idStr );
					int scale = 0;
					int width = 0;
					int height = 0;
					bool attach = false;
					if ( id <= 0 )
						return;
					if ( scaleStr != null && scaleStr != "" )
						scale = Convert.ToInt32( scaleStr );
					if ( widthStr != null && widthStr != "" )
						width = Convert.ToInt32( widthStr );
					if ( heightStr != null && heightStr != "" )
						height = Convert.ToInt32( heightStr );
					// Load the document from the database
					ImageDocument doc = new ImageDocument( id );
					// Is this an attachment?
					if ( attachStr == null ) {
						attach = !doc.IsImage;
					} else {
						attach = ( attachStr == "1" );
					}
					// Fetch the bits
					doc.DownloadDocument( context.Response, attach, scale, width, height );
				} catch ( Exception e ) {
					string msg = e.Message;
				}
			}
		}

	}
}
