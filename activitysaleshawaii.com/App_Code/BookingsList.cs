using System;
using System.Collections;
using System.Collections.Generic;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of Bookings in an Itinerary
	/// </summary>
	public class BookingsList : List<Booking> {

		#region Properties

		public Itinerary Owner {
			get { return ( this.owner ); }
		}
		private Itinerary owner = null;

		/// <summary>
		/// Total of the pre-tax, pre-discount prices for all Guests in this booking
		/// </summary>
		public decimal TotalBasePrice {
			get {
				decimal result = 0M;
				foreach ( Booking b in this ) {
					result += b.TotalBasePrice;
				}
				return ( result );
			}
		}

		/// <summary>
		/// Total of the pre-tax, discounted prices for all Guests in this booking
		/// </summary>
		public decimal TotalDiscountedPrice {
			get {
				decimal result = 0M;
				foreach ( Booking b in this ) {
					result += b.TotalDiscountedPrice;
				}
				return ( result );
			}
		}

		/// <summary>
		/// Total of the tax charges for all guests in this booking
		/// </summary>
		public decimal Tax {
			get {
				decimal result = 0M;
				foreach ( Booking b in this ) {
					result += b.Tax;
				}
				return ( result );
			}
		}

		/// <summary>
		/// Total discounted charge plus tax for all guests in this booking
		/// </summary>
		public decimal TotalDiscountedPricePlusTax {
			get {
				decimal result = 0M;
				foreach ( Booking b in this ) {
					result += b.TotalDiscountedPricePlusTax;
				}
				return ( result );
			}
		}

		/// <summary>
		/// Displayable total discounted charge plus tax for all guests in this booking
		/// </summary>
		public string TotalPriceStr {
			get {
				if ( Website.Current.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.TotalDiscountedPricePlusTax ) );
				}
			}
		}

		#endregion
		
		#region Constructors
		
		private BookingsList() {}
		
		public BookingsList( Itinerary owner ) : base() {
			this.owner = owner;
			this.LoadItems();
		}
		
		#endregion

		private void LoadItems() {
			if ( this.owner == null || this.owner.ID <= 0 ) {
				return;
			}
			string sql = "select * from BOOKINGS where ( itinerary_id = @itinerary_id and is_deleted = 0 ) order by created_dt desc";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@itinerary_id", this.owner.ID ) ) ) {
					Booking item = new Booking( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new Booking( this.owner );
					}
				}
			}
		}

		protected internal void Persist() {
			for ( int i = 0 ; i < this.Count ; ) {
				Booking item = this[i];
				if ( !item.IsBookingCompleted ) {
					this.Remove( item );
					item.Delete();
				} else {
					item.ItineraryID = this.Owner.ID;
					item.Persist();
					i++;
				}
			}
		}

	}
	
}
