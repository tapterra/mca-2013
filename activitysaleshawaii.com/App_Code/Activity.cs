using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ABS {
	
	/// <summary>
	/// Represents one of the tourism activities (events) to be booked by the system
	/// </summary>
	[SqlTable( "ACTIVITIES", SingularLabel = "Activity", PluralLabel = "Activities", IsMarkedDeleted = true )]
	public class Activity : BaseSqlPersistable {
		
		#region Properties

		#region Database Columns
		
		[SqlColumn( "activity_aid", DbType.String, Length = 30 )]
		public string AID {
			get { return ( this.aid ); }
			set { this.aid = value; }
		}
		private string aid;

		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		[SqlColumn( "activity_group_id", DbType.Int32 )]
		public int ActivityGroupID {
			get { return ( this.activityGroupID ); }
			set { this.activityGroupID = value; }
		}
		private int activityGroupID = -1;

		[SqlColumn( "title", DbType.String, Length = 200 )]
		public string Title {
			get { return ( this.title ); }
			set { this.title = value; }
		}
		private string title;

		[SqlColumn( "description", DbType.String )]
		public string Description {
			get { return ( this.description ); }
			set { this.description = value; }
		}
		private string description;

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		public decimal TaxRate {
			get { return ( this.taxRate ); }
			set { this.taxRate = value; }
		}
		private decimal taxRate = 0.0M;

		public ActivityGroup ActivityGroup {
			get {
				if ( this.group == null ) {
					this.group = new ActivityGroup( this.ActivityGroupID );
				}
				return ( this.group ); 
			}
			set {
				group = value;
			}
		}
		private ActivityGroup group;

		public string RegionAID {
			get { return ( this.ActivityGroup.Island.AID ); }
		}

		public ScheduleDateTimesList ScheduleDateTimes {
			get { 
				if ( this.scheduleDateTimes == null ) {
					this.scheduleDateTimes = new ScheduleDateTimesList( this );
				}
				return ( this.scheduleDateTimes ); 
			}
		}
		private ScheduleDateTimesList scheduleDateTimes;

		public OptionsList Options {
			get { 
				return ( this.ActivityGroup.Options ); 
			}
		}

		public string ItineraryTitle {
			get {
				string result = this.ActivityGroup.Title;
				// RKP 20090326 - we're getting exceptions here because sometimes ActivityGroup.Category is null...
				// if ( this.ActivityGroup.Activities.Count > 1 || this.ActivityGroup.Category.Label != "Activities" ) {
				string catLabel = this.ActivityGroup.Category == null ? "Activities" : this.ActivityGroup.Category.Label;
				if ( this.ActivityGroup.Activities.Count > 1 || catLabel != "Activities" ) {
					result += " - " + this.Title;
				}
				return ( result );
			}
		}
		
		#endregion
		
		#region Constructors

		// Access the base constructors
		public Activity() : base() { }
		public Activity( int ID ) : base( ID ) { }

		public Activity( string aid ) {
			using ( SqlDatabase db = new SqlDatabase() ) {
				//string sql = "SELECT a.*, g.* FROM ACTIVITIES AS a INNER JOIN ACTIVITY_GROUPS AS g ON g.id = a.activity_group_id WHERE (a.activity_aid = @aid) AND (a.is_deleted = 0) AND (g.is_deleted = 0)";
				//db.SelectInstance( this, sql, db.NewParameter( "@aid", aid ) );
				string sql = "SELECT a.*, g.* FROM ACTIVITIES AS a INNER JOIN ACTIVITY_GROUPS AS g ON g.id = a.activity_group_id WHERE (a.activity_aid = @aid) AND (a.is_deleted = 0) AND (g.website_id = @wid) AND (g.is_deleted = 0)";
				db.SelectInstance( this, sql, db.NewParameter( "@aid", aid ), db.NewParameter( "@wid", Website.Current.ID ) );
			}
			if ( string.IsNullOrEmpty( this.AID ) ) {
				// RKP: 20130809-kill this, it's driving me nuts and not helping any
				//throw new ArgumentException( String.Format( "Tried to restore an Activity with an unknown AID: {0}", aid ) );
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Produces a deep copy of this activity 
		/// </summary>
		/// <returns></returns>
		protected internal Activity DeepCopy() {
			// Make a shallow copy of this instance
			Activity newActivity = (Activity) this.MemberwiseClone();
			newActivity.ID = -1;
			newActivity.AID = "";
			// nothing deeper here?
			return ( newActivity );
		}

		#endregion
		
	}

}
