using System;
using System.Data;
using System.IO;
using System.Web;
using System.Xml.Serialization;
using StarrTech.WebTools.Data;

namespace ABS {
	
	/// <summary>
	/// Activity booking information
	/// </summary>
	[SqlTable( "BOOKINGS", SingularLabel = "Booking", PluralLabel = "Bookings", IsMarkedDeleted = true )]
	public class Booking : BaseSqlPersistable {

		#region Properties

		public static Booking Current {
			get { return ( HttpContext.Current.Session["CurrentBooking"] as Booking ); }
			set { HttpContext.Current.Session["CurrentBooking"] = value; }
		}

		#region Database Columns
		
		//[created_dt] [datetime] NOT NULL,
		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDT {
			get { return ( this.createdDT ); }
			set { this.createdDT = value; }
		}
		private DateTime createdDT = DateTime.Now;

		//[itinerary_id] [int] NOT NULL,
		[SqlColumn( "itinerary_id", DbType.Int32 )]
		public int ItineraryID {
			get { return ( this.itineraryID ); }
			set { this.itineraryID = value; }
		}
		private int itineraryID = -1;

		//[activity_id] [int] NOT NULL,
		[SqlColumn( "activity_id", DbType.Int32 )]
		public int ActivityID {
			get { return ( this.activityID ); }
			set { this.activityID = value; }
		}
		private int activityID = -1;

		//[selected_dt] [datetime] NOT NULL,
		[SqlColumn( "selected_dt", DbType.DateTime )]
		public DateTime SelectedDateTime {
			get {
				if ( this.persistedScheduleDateTime > DateTime.MinValue ) {
					return ( this.persistedScheduleDateTime );
				}
				return ( this.SelectedScheduleDateTime.Value ); 
			}
			set { this.persistedScheduleDateTime = value; }
		}
		private DateTime persistedScheduleDateTime = DateTime.MinValue;
		
		//[hotel_aid] [nvarchar](30) NULL,
		[SqlColumn( "hotel_aid", DbType.String, Length = 30 )]
		public string HotelAID {
			get { return ( this.hotelAID ); }
			set { this.hotelAID = value; }
		}
		private string hotelAID;

		//[other_hotel_name] [nvarchar](200) NULL,
		[SqlColumn( "other_hotel_name", DbType.String, Length = 200 )]
		public string OtherHotelName {
			get { return ( this.otherHotelName ); }
			set { this.otherHotelName = value; }
		}
		private string otherHotelName;

		//[hotel_room_no] [nvarchar](15) NULL,
		[SqlColumn( "hotel_room_no", DbType.String, Length = 15 )]
		public string HotelRoomNumber {
			get { return ( this.hotelRoomNumber ); }
			set { this.hotelRoomNumber = value; }
		}
		private string hotelRoomNumber = "";

		/// <summary>
		/// Voucher number returned by AppendBooking when the booking is successfully reserved by the ATCO webservice.
		/// </summary>
		//[voucher_no] [nvarchar](30) NULL,
		[SqlColumn( "voucher_no", DbType.String, Length = 30 )]
		public string VoucherNumber {
			get { return ( this.voucherNumber ); }
			set { this.voucherNumber = value; }
		}
		private string voucherNumber;

		//[voucher_data] [ntext] NULL,
		[SqlColumn( "voucher_data", DbType.String )]
		public string VoucherData {
			// RKP: Begin change - 20090107
			// RKP: The getter below is unnecessarily complex
			//get { return ( string.IsNullOrEmpty( this.voucherData ) ? "" : this.voucherData ); }
			get { return ( this.voucherData ); }
			// RKP: End change
			set { this.voucherData = value; }
		}
		private string voucherData = "";

		public AtcoVoucherApi.PrintableVoucher Voucher {
			get { return ( this.voucher ); }
			set { 
				this.voucher = value; 
				// RKP: Begin change - 20090107
				// RKP: Whenever we attach a PrintableVoucher to the booking, serialize and persist it as VoucherData
				// RKP: We can now trace the voucher data received from ATCO, to verify whether a data error is 
				// RKP: introduced by our code or if it is received from ATCO.
				this.VoucherData = this.SerializeVoucher( value );
				// RKP: End change
			}
		}
		private AtcoVoucherApi.PrintableVoucher voucher;

		// RKP: Begin change - 20090107
		/// <summary>
		/// Given an instance of the ATCO PrintableVoucher, return the serialized XML representation of it's value
		/// </summary>
		/// <param name="voucher">an ATCO PrintableVoucher instance</param>
		/// <returns>the XML-serialized voucher string</returns>
		private string SerializeVoucher( AtcoVoucherApi.PrintableVoucher voucher ) {
			if ( voucher == null ) { return ( "" ); }
			string result = "";
			XmlSerializer serializer = new XmlSerializer( typeof( AtcoVoucherApi.PrintableVoucher ) );
			using ( MemoryStream stream = new MemoryStream() ) {
				serializer.Serialize( stream, voucher );
				stream.Position = 0;
				System.IO.StreamReader reader = new System.IO.StreamReader( stream );
				result = reader.ReadToEnd();
			}
			return ( result );
		}
		// RKP: End change

		#endregion

		public Activity Activity {
			get {
				if ( this.activity == null && this.ActivityID >= 0 ) {
					this.activity = new Activity( this.ActivityID );
				}
				return ( this.activity ); 
			}
			set { 
				this.activity = value;
				this.ActivityID = value.ID;
			}
		}
		private Activity activity;

		public string ActivityTitle {
			get { return ( this.Activity.ItineraryTitle ); }
		}
		
		/// <summary>
		/// The scheduled date & time selected by the customer for booking
		/// </summary>
		public ScheduleDateTime SelectedScheduleDateTime {
			get { return ( this.selectedScheduleDateTime ); }
			set { this.selectedScheduleDateTime = value; }	
		}
		private ScheduleDateTime selectedScheduleDateTime;

		/// <summary>
		/// The selected date-time formatted for UI display
		/// </summary>
		public string SelectedDateTimeString {
			get {
				DateTime dt = this.SelectedDateTime;
				if ( dt.TimeOfDay.Ticks > 0 ) {
					return ( dt.ToString( "dddd, MMMM d, yyyy h:mm tt" ) );
				} else {
					return ( dt.ToString( "dddd, MMMM d, yyyy" ) );
				}
			}
		}
		public string SelectedDateTimeStr {
			get {
				DateTime dt = this.SelectedDateTime;
				if ( dt.TimeOfDay.Ticks > 0 ) {
					return ( dt.ToString() );
				} else {
					return ( dt.ToString( "d" ) );
				}
			}
		}

		/// <summary>
		/// The name of the hotel 
		/// </summary>
		public string HotelName {
			get {
				if ( string.IsNullOrEmpty( this.HotelAID ) ) {
					return ( "Other - " + this.OtherHotelName );
				}
				using ( IVoucherService provider = VoucherServiceFactory.NewVoucherService() ) {
					if ( provider.Hotels != null && provider.Hotels.ContainsKey( this.HotelAID ) ) {
						return ( provider.Hotels[this.HotelAID].Name );
					}
				}
				return ( "" );
			}
		}
		
		/// <summary>
		/// Hotel name and room number, for reporting/display
		/// </summary>
		public string HotelInfo {
			get {
				string result = this.HotelName;
				if ( !string.IsNullOrEmpty( this.HotelRoomNumber ) ) {
					result += ", Room " + this.HotelRoomNumber;
				}
				return ( result );
			}
		}

		/// <summary>
		/// The collection of Pax Counts for this booking
		/// </summary>
		public PaxCountsList PaxCounts { 
			get { 
				if ( this.paxCounts == null ) { 
					this.paxCounts = new PaxCountsList( this );
				}
				return ( this.paxCounts );
			}
		}
		private PaxCountsList paxCounts;
		
		/// <summary>
		/// The collection of Guests for this booking
		/// </summary>
		public GuestsList Guests {
			get { 
				if ( this.guests == null ) {
					this.guests = new GuestsList( this );
				}
				return ( this.guests ); 
			}
		}
		private GuestsList guests;

		public Itinerary Itinerary {
			get {
				if ( this.itinerary == null ) {
					if ( this.ItineraryID >= 0 ) {
						this.itinerary = new Itinerary( this.ItineraryID );
					} else {
						this.itinerary = Customer.Current.CurrentItinerary;
					}
				}
				return ( this.itinerary );
			}
		}
		private Itinerary itinerary;

		private PromoCode PromoCode {
			get { return ( this.Itinerary.PromoCode ); }
		}

		/// <summary>
		/// Total of the pre-tax, pre-discount prices for all Guests in this booking
		/// </summary>
		public decimal TotalBasePrice {
			get { return ( this.Guests.TotalBasePrice ); }
		}

		/// <summary>
		/// Total of the pre-tax, discounted prices for all Guests in this booking
		/// </summary>
		public decimal TotalDiscountedPrice {
			get {
				if ( this.PromoCode == null ) {
					return ( this.TotalBasePrice );
				}
				return ( this.PromoCode.CalculateDiscountedPrice( this.Itinerary.TotalBookingsBasePrice, this.TotalBasePrice ) );
			}
		}

		public decimal TotalDiscountAmount {
			get { return ( this.TotalBasePrice - TotalDiscountedPrice ); }
		}

		/// <summary>
		/// Total of the tax charges for all guests in this booking
		/// </summary>
		public decimal Tax {
			get { return ( this.TotalDiscountedPrice * this.Activity.TaxRate ); }
		}

		/// <summary>
		/// Total discounted charge plus tax for all guests in this booking
		/// </summary>
		public decimal TotalDiscountedPricePlusTax {
			get { return ( this.TotalDiscountedPrice + this.Tax ); }
		}

		/// <summary>
		/// Displayable total discounted charge plus tax for all guests in this booking
		/// </summary>
		public string TotalPriceStr {
			get {
				if ( this.Itinerary.Website.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.TotalDiscountedPricePlusTax ) );
				}
			}
		}

		/// <summary>
		/// Flag to determine if we were successful in reserving the seats.
		/// </summary>
		public bool IsReserved {
			get { return ( string.IsNullOrEmpty( this.voucherNumber ) ); }
		}

		/// <summary>
		/// Flag to determine if the booking process was completed by the user
		/// </summary>
		public bool IsBookingCompleted {
			get {
				// See if we have any guests...
				if ( this.Guests.Count == 0 ) {
					return ( false );
				}
				// See if any  of the guests are missing a first/last name
				foreach ( Guest g in this.Guests ) {
					if ( string.IsNullOrEmpty( g.FirstName ) || string.IsNullOrEmpty( g.LastName ) ) {
						return ( false );
					}
				}
				return ( true );
			}
		}

		/// <summary>
		/// Flag set when the system fails to reserve the booking. 
		/// Itinerary UI will highlight the booking for revision by the user.
		/// </summary>
		public bool IsErrorHighlighted {
			get { return ( this.isErrorHighlighted ); }
			set { this.isErrorHighlighted = value; }
		}
		private bool isErrorHighlighted = false;

		#endregion
		
		#region Constructors
		
		// Not to be used
		private Booking() : base() { }
		
		/// <summary>
		/// Used by the Admin app (?)
		/// </summary>
		public Booking( int ID ) : base( ID ) { }

		/// <summary>
		/// Used by the public booking UI to add a new Booking
		/// </summary>
		public Booking( string ActivityAID ) : base() {
			if ( string.IsNullOrEmpty( ActivityAID ) ) {
				throw new ArgumentNullException( "ActivityAID", "Tried to create a Booking with a null Activity AID." );
			}
			this.Activity = new Activity( ActivityAID );
			if ( this.Activity == null ) {
				throw new ArgumentException( String.Format( "Tried to create a Booking with an unknown Activity AID: {0}", ActivityAID ) );
			}
		}

		/// <summary>
		/// Used by BookingList when loading the Bookings for a given Itinerary
		/// </summary>
		protected internal Booking( Itinerary owner ) : base() {
			this.itineraryID = owner.ID;
		}

		#endregion

		public override bool Persist() {
			bool result = base.Persist();
			if ( result ) {
				this.PaxCounts.Persist();
				this.Guests.Persist();
			}
			return ( result );
		}
	}
	
}
