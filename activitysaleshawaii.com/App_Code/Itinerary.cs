using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using StarrTech.WebTools.Data;

namespace ABS {

	/// <summary>
	/// This collects customer information
	/// </summary>
	[SqlTable( "ITINERARIES", SingularLabel = "Itinerary", PluralLabel = "Itineraries", IsMarkedDeleted = true )]
	public class Itinerary : BaseSqlPersistable {

		#region Properties

		#region Database columns

		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDateTime {
			get { return ( this.createdDateTime ); }
			set { this.createdDateTime = value; }
		}
		private DateTime createdDateTime = DateTime.Now;

		[SqlColumn( "customer_id", DbType.Int32 )]
		public int CustomerID {
			get { return ( this.customerID ); }
			set { this.customerID = value; }
		}
		private int customerID = -1;

		[SqlColumn( "voucher_transaction_id", DbType.Int32 )]
		public int VoucherTransactionID {
			get { return ( this.voucherTransactionID ); }
			set { this.voucherTransactionID = value; }
		}
		private int voucherTransactionID = -1;

		[SqlColumn( "customer_promo_code", DbType.String, Length = 50 )]
		public string CustomerPromoCode {
			get { return ( this.customerPromoCode ); }
			set { this.customerPromoCode = value; }
		}
		private string customerPromoCode = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		// RKP: Begin change 20090428 
		// RKP: We want to persist the itinerary whenever the vouchers are created at the completion of the sale...
		public string VoucherHtml {
			get { return ( this._VoucherHtml ); }
			set {
				this._VoucherHtml = value;
				// RKP: At this point we know the itinerary has been completed - make sure everything gets persisted
				this.Persist();
			}
		}
		private string _VoucherHtml = "";

		// RKP: ...but not when it's loaded from the database
		[SqlColumn( "voucher_html", DbType.String )]
		public string VoucherHtmlColumn {
			get { return ( this._VoucherHtml ); }
			set { this._VoucherHtml = value; }
		}
		// RKP: End change 

		#endregion

		/// <summary>
		/// Static property to reference the session-specific current itinerary
		/// </summary>
		protected internal static Itinerary Current {
			get { return ( Customer.Current.CurrentItinerary ); }
		}

		#region Navigational properties
		
		/// <summary>
		/// Access to the Customer for this Itinerary
		/// </summary>
		public Customer Customer {
			get {
				if ( this.customer == null ) {
					this.customer = new Customer( this.CustomerID );
				}
				return ( this.customer );
			}
		}
		private Customer customer = null;

		/// <summary>
		/// The customer's first name
		/// </summary>
		public string CustomerFirstName {
			get { return ( this.Customer.FirstName ); }
			set { this.Customer.FirstName = value; }
		}

		/// <summary>
		/// The customer's last name.
		/// </summary>
		public string CustomerLastName {
			get { return ( this.Customer.LastName ); }
			set { this.Customer.LastName = value; }
		}

		/// <summary>
		/// Lastname, Firstname
		/// </summary>
		public string CustomerFullName {
			get { return ( this.Customer.FullNameLF ); }
		}

		/// <summary>
		/// The contact phone number for the customer (usually same as for the credit card).
		/// </summary>
		public string ContactPhone {
			get { return ( this.Customer.Telephone ); }
			set { this.Customer.Telephone = value; }
		}

		/// <summary>
		/// The contact email for the customer.
		/// </summary>
		public string ContactEmail {
			get { return ( this.Customer.EmailAddress ); }
			set { this.Customer.EmailAddress = value; }
		}

		public Website Website {
			get { return ( this.Customer.Website ); }
		}

		/// <summary>
		/// The collection of Activity Bookings for this Itinerary.
		/// </summary>
		public BookingsList Bookings {
			get {
				if ( this.bookings == null ) {
					this.bookings = new BookingsList( this );
				}
				return ( this.bookings );
			}
		}
		private BookingsList bookings;

		/// <summary>
		/// The collection of Reservations for this Itinerary.
		/// </summary>
		public ReservationsList Reservations {
			get {
				if ( this.reservations == null ) {
					this.reservations = new ReservationsList( this );
				}
				return ( this.reservations );
			}
		}
		private ReservationsList reservations;

		public PromoCode PromoCode {
			get {
				PromoCode result = null;
				if ( !String.IsNullOrEmpty( this.CustomerPromoCode ) ) {
					if ( this.Website != null ) {
						if ( this.Website.ActivePromoCodes.Count > 0 ) {
							result = this.Website.GetUserPromoCode( this.CustomerPromoCode );
						}
					}
				}
				return ( result );
			}
		}

		#endregion

		public string CreatedDateTimeStr {
			get { return ( this.CreatedDateTime.ToString() ); }
		}

		public string VoucherTransactionIDStr {
			get { return ( this.VoucherTransactionID <= 0 ? "" : this.VoucherTransactionID.ToString() ); }
		}

		/// <summary>
		/// Generates a list of Guests for previous bookings.
		/// Used to populate the GuestPicker controls on the booking UI
		/// </summary>
		public Dictionary<string, Guest> PriorGuests {
			get {
				Dictionary<string, Guest> guests = new Dictionary<string, Guest>();
				foreach ( Booking b in this.Bookings ) {
					foreach ( Guest g in b.Guests ) {
						if ( string.IsNullOrEmpty( g.FirstName ) ) continue;
						string key = string.Format( "{0}, {1}", g.LastName, g.FirstName );
						if ( !guests.ContainsKey( key ) ) {
							guests.Add( key, g );
						}
					}
				}
				return ( guests );
			}
		}

		/// <summary>
		/// The date of the first day of the current booking-calendar month.
		/// </summary>
		public DateTime CurrentBookingMonth {
			get { 
				if ( this.currentBookingMonth == DateTime.MinValue ) {
					// Return Day-1 of the current month
					DateTime now = DateTime.Now;
					this.currentBookingMonth = new DateTime( now.Year, now.Month, 1 );
				}
				return ( this.currentBookingMonth ); 
			}
			set { 
				// Assign any date, but only the first-of-the-month sticks
				this.currentBookingMonth = new DateTime( value.Year, value.Month, 1 ); 
			}
		}
		private DateTime currentBookingMonth = DateTime.MinValue;
		
		/// <summary>
		///  The total number of ATCO Bookings for this itinerary
		/// </summary>
		public int BookingsCount {
			get { return ( this.Bookings.Count ); }
		}
		
		/// <summary>
		///  The total number of non-ATCO Reservations for this itinerary
		/// </summary>
		public int ReservationsCount {
			get { return ( this.Reservations.Count ); }
		}
		
		/// <summary>
		///  The total number of Bookings and Reservations for this itinerary
		/// </summary>
		public int ResBookingsCount {
			get { return ( this.BookingsCount + this.ReservationsCount ); }
		}

		/// <summary>
		/// The collection of Payments for this Itinerary.
		/// </summary>
		public PaymentsList Payments {
			get {
				if ( this.payments == null ) {
					this.payments = new PaymentsList( this );
				}
				return ( this.payments );
			}
		}
		private PaymentsList payments;

		/// <summary>
		/// Total of the undiscounted, pre-tax prices for all Bookings & Reservations.
		/// </summary>
		public decimal TotalBasePrice {
			get { return ( this.TotalBookingsBasePrice + this.TotalReservationsPrice ); }
		}

		/// <summary>
		/// Total of the undiscounted, pre-tax prices for all Bookings.
		/// </summary>
		public decimal TotalBookingsBasePrice {
			get { return ( this.Bookings.TotalBasePrice ); }
		}

		/// <summary>
		/// Total of the undiscounted, pre-tax prices for all Reservations.
		/// </summary>
		public decimal TotalReservationsPrice {
			get { return ( this.Reservations.TotalPrice ); }
		}

		/// <summary>
		/// Total of the discounted, pre-tax prices for all Bookings.
		/// </summary>
		public decimal TotalBookingsDiscountedPrice {
			get { return ( this.Bookings.TotalDiscountedPrice ); }
		}

		/// <summary>
		/// Total of the discounted prices plus tax for all Bookings.
		/// </summary>
		public decimal TotalBookingsDiscountedPricePlusTax {
			get { return ( this.Bookings.TotalDiscountedPricePlusTax ); }
		}

		/// <summary>
		/// Total of the discounted, pre-tax prices for all Bookings & Reservations.
		/// </summary>
		public decimal TotalDiscountedPrice {
			get { return ( this.TotalBookingsDiscountedPrice + this.TotalReservationsPrice ); }
		}

		/// <summary>
		/// Total of the discounted prices plus tax for all Bookings & Reservations.
		/// </summary>
		public decimal TotalDiscountedPricePlusTax {
			get { return ( this.TotalBookingsDiscountedPricePlusTax + this.TotalReservationsPrice ); }
		}

		private bool IsNoPaymentSite {
			get {
				if ( this.Website == null ) {
					return ( false );
				}
				return ( this.Website.IsNoPaymentSite );
			}
		}

		/// <summary>
		/// Formatted total of the discounted prices plus tax for all Bookings & Reservations.
		/// </summary>
		public string TotalPriceStr {
			get {
				if ( this.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.TotalDiscountedPricePlusTax ) );
				}
			}
		}

		/// <summary>
		/// Total amount of all completed payments.
		/// </summary>
		public decimal TotalPayments {
			get { return ( this.Payments.Total ); }
		}

		public string TotalPaymentsStr {
			get {
				if ( this.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.TotalPayments ) );
				}
			}
		}

		/// <summary>
		/// PaymentProcessor responsible for handling our CC transactions.
		/// </summary>
		public IPaymentProcessor PmtProcessor {
			get { 
				if ( this.pmtProcessor == null ) {
					this.pmtProcessor = PaymentProcessorFactory.NewPaymentProcessor();
				}
				return ( this.pmtProcessor );
			}
		}
		private IPaymentProcessor pmtProcessor;

		/// <summary>
		/// Error message generated during checkout, for display in the UI
		/// </summary>
		public string ErrorMessage {
			get { return ( this.errorMessage ); }
			set { this.errorMessage = value; }
		}
		private string errorMessage;

		/// <summary>
		/// Returns true if the itinerary has already been paid for and booked.
		/// A completed itinerary can't be modifed.
		/// </summary>
		public bool IsCompleted {
			get {
				// Once an itinerary has been paid for, it's all done
				// This is good for now, but later... ??
				return ( this.TotalPayments > 0 );
			}
		}

		/// <summary>
		/// Generates the login "Account Number" for this itinerary & it's customer
		/// </summary>
		public string LoginAccountNumber {
			get {
				// Assumes we've already been persisted
				if ( this.CustomerID < 0 ) {
					throw new Exception( "Attempt to generate LoginAccountNumber for an unpersisted Customer." );
				}
				if ( this.ID < 0 ) {
					throw new Exception( "Attempt to generate LoginAccountNumber for an unpersisted Itinerary." );
				}
				return ( string.Format( "I{0:######0000}C{1:######0000}", this.ID, this.CustomerID ) );
			}
		}

		/// <summary>
		/// Given an "Account Number" value from the My Account signin form, parse out the itinerary & customer IDs
		/// </summary>
		/// <param name="code">User-supplied account number code</param>
		/// <param name="itinID">The Itinerary ID or -1 if not a valid code</param>
		/// <param name="custID">The Customer ID or -1 if not a valid code</param>
		/// <returns>True if the code is valid</returns>
		public static bool ParseLoginAccountNumber( string code, out int itinID, out int custID ) {
			itinID = -1;
			custID = -1;
			try {
				int cLoc = code.IndexOf( 'C' );
				string i = code.Substring( 1, cLoc - 1 );
				string c = code.Substring( cLoc + 1, code.Length - cLoc - 1 );
				if ( !Int32.TryParse( i, out itinID ) ) {
					return ( false );
				}
				if ( !Int32.TryParse( c, out custID ) ) {
					return ( false );
				}
				return ( true );
			} catch {
				return ( false );
			}
		}

		private StringBuilder _TraceLog;

		internal void AddToTrace( string msg ) {
			if ( this._TraceLog == null ) {
				this._TraceLog = new StringBuilder();
			}
			this._TraceLog.AppendFormat( "{1}: {2}{0}{0}", Environment.NewLine, DateTime.Now, msg );
		}

		#endregion
		
		#region Constructors

		private Itinerary() : base() {}
		
		public Itinerary( int ID ) : base( ID ) { }

		public Itinerary( Customer customer ) : this() {
			this.customer = customer;
			this.CustomerID = customer.ID;
		}

		#endregion

		#region Methods

		public void CaptureNewPayment() {
			// Assume that all the payment info is up-to-date in the PmtProcessor
			Payment pmt = new Payment( this );
			pmt.PostedDT = DateTime.Now;
			pmt.MethodType = this.PmtProcessor.PaymentType;
			pmt.Amount = this.PmtProcessor.PaymentAmount;
			pmt.AuthCode = this.PmtProcessor.AuthCode;
			pmt.CCRef = this.PmtProcessor.CardNumber;
			this.Payments.Add( pmt );
			this.Customer.Persist();
		}

		public void RemoveBooking( Booking booking ) {
			this.Bookings.Remove( booking );
			booking.Delete();
		}

		public void RemoveReservation( Reservation res ) {
			this.Reservations.Remove( res );
			res.Delete();
		}

		public override bool Persist() {
			bool result = base.Persist();
			if ( result ) {
				this.Bookings.Persist();
				this.Reservations.Persist();
				this.Payments.Persist();
			}
			return ( result );
		}

		public override bool IsValidToPersist() {
			// No sense saving an empty itinerary
			if ( this.ResBookingsCount > 0 ) {
				return base.IsValidToPersist();
			}
			return ( false );
		}

		public override bool IsValidToDelete() {
			// Can't do this
			return ( false );
		}

		public override bool Delete() {
			// Can't do this
			return ( false );
		}

		#endregion

	}

}
