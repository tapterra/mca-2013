﻿using System.Data;
using StarrTech.WebTools.Data;
using System;
using System.Text;
using System.Collections.Generic;

namespace ABS {

	[SqlTable( "PROMO_CODES", SingularLabel = "Promo Code", PluralLabel = "Promo Codes", IsMarkedDeleted = true )]
	public class PromoCode : BaseSqlPersistable {
		
		#region Database Columns

		[SqlColumn( "website_id", DbType.Int32 )]
		public int WebsiteID {
			get { return ( this._WebsiteID ); }
			set { this._WebsiteID = value; }
		}
		private int _WebsiteID = -1;

		[SqlColumn( "code", DbType.String, Length = 50 )]
		public string Code {
			get { return ( this._Code ); }
			set { this._Code = value; }
		}
		private string _Code = "";

		[SqlColumn( "start_date", DbType.Date )]
		public DateTime StartDate {
			get { return ( this._StartDate ); }
			set { this._StartDate = value; }
		}
		private DateTime _StartDate = DateTime.MinValue;

		[SqlColumn( "end_date", DbType.Date )]
		public DateTime EndDate {
			get { return ( this._EndDate ); }
			set { this._EndDate = value; }
		}
		private DateTime _EndDate = DateTime.MaxValue;

		[SqlColumn( "flat_discount", DbType.Int32 )]
		public int FlatDiscount {
			get { return ( this._FlatDiscount ); }
			set { this._FlatDiscount = value; }
		}
		private int _FlatDiscount = 0;

		[SqlColumn( "pct_discount", DbType.Int32 )]
		public int PctDiscount {
			get { return ( this._PctDiscount ); }
			set { this._PctDiscount = value; }
		}
		private int _PctDiscount = 0;

		[SqlColumn( "min_purchase", DbType.Int32 )]
		public int MinPurchase {
			get { return ( this._MinPurchase ); }
			set { this._MinPurchase = value; }
		}
		private int _MinPurchase = 0;

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this._Comments ); }
			set { this._Comments = value; }
		}
		private string _Comments = "";

		#endregion

		#region Derived Properties

		public Website Website {
			get {
				if ( Websites.Current.ContainsKey( this.WebsiteID ) ) {
					return ( Websites.Current[this.WebsiteID] );
				}
				return ( null );
			}
		}
		
		public string WebsiteName {
			get { return ( this.Website == null ? "" : this.Website.Name ); }
		}

		public string StartDateString {
			get { return ( this._StartDate > DateTime.MinValue ? this._StartDate.ToShortDateString() : "" ); }
		}

		public string EndDateString {
			get { return ( this._EndDate < DateTime.MaxValue ? this._EndDate.ToShortDateString() : "" ); }
		}

		public string FlatDiscountString {
			get { return ( this.FlatDiscount > 0M ? this.FlatDiscount.ToString( "C" ) : "" ); }
		}

		public string PctDiscountString {
			get { return ( this.PctDiscount > 0 ? string.Format( "{0} %", this.PctDiscount) : "" ); }
		}

		public string MinPurchaseString {
			get { return ( this.MinPurchase > 0 ? this.MinPurchase.ToString( "C" ) : "" ); }
		}

		#endregion

		#region Constructors

		public PromoCode() : base() { }
		
		public PromoCode( int ID ) : base( ID ) { }

		public PromoCode( int websiteID, string code ) {
			string sql = "select * from PROMO_CODES where ( website_id = @website_id and code = @code )";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@website_id", websiteID ), db.NewParameter( "@code", code ) );
			}
		}

		#endregion

		public decimal CalculateDiscountedPrice( decimal TotalItineraryBasePrice, decimal BasePrice ) {
			decimal result = BasePrice;
			if ( TotalItineraryBasePrice < this.MinPurchase ) {
				return ( result );
			}
			if ( this.PctDiscount > 0 ) {
				result *= 1 - ( this.PctDiscount / 100M );
			}
			if ( this.FlatDiscount > 0M ) {
				result -= ( BasePrice * this.FlatDiscount / TotalItineraryBasePrice );
			}
			return ( result > 0M ? result : 0M );
		}

		#region Static Admin Search Members

		#region Properties

		public static new string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static new string sortExpression = "start_date asc";

		public static int SWebsiteID {
			get { return ( sWebsiteID ); }
			set { sWebsiteID = value; }
		}
		private static int sWebsiteID = -1;

		#endregion

		#region Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			PromoCode.SWebsiteID = -1;
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "website_id", PromoCode.SWebsiteID, ref delim );
			AddSearchExpr( whereBuilder, "is_deleted", false, ref delim );

			string sql = @"select * from	PROMO_CODES ";
			if ( whereBuilder.Length != 0 ) {
				sql += string.Format( " WHERE ( {0} ) ", whereBuilder );
			}
			if ( string.IsNullOrEmpty( PromoCode.SortExpression ) ) {
				PromoCode.SortExpression = "start_date";
			}
			sql += string.Format( " order by {0}", PromoCode.SortExpression );
			return ( sql );
		}

		public static List<PromoCode> DoSearchList() {
			return ( SqlDatabase.DoSearch<PromoCode>( PromoCode.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
