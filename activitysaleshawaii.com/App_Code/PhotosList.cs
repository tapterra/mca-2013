using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {

	public class PhotosList : List<ActivityPhoto> {

		private ActivityGroup Owner;

		private PhotosList() { }

		public PhotosList( ActivityGroup group ) : base() {
			this.Owner = group;
			this.LoadItems();
		}

		private void LoadItems() {
			if ( this.Owner == null || this.Owner.ID <= 0 ) {
				return;
			}
			string sql = string.Format( "select * from ACTIVITY_PHOTOS where ( activity_group_id = {0} and is_deleted = 0 ) order by seq", this.Owner.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					ActivityPhoto item = new ActivityPhoto();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new ActivityPhoto();
					}
				}
			}
		}

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_group_photos";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure, db.NewParameter( "@group_id", this.Owner.ID ) );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public int HighestSeq {
			get { return ( this.ResetItemRanks() - 1 ); }
		}

		/// <summary>
		/// Increments the sequence-number of the item at the given sequence-number, moving it down in the sequence.
		/// </summary>
		/// <param name="Seq">Current sequence-number of the item to move.</param>
		public void MoveDown( int Seq ) {
			// We can't demote if we're already at the end
			if ( Seq >= this.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ACTIVITY_PHOTOS set seq = -1 where activity_group_id = @activity_group_id and seq = @seq; ";
			// Move the Activity below me up to my spot
			sql += "update ACTIVITY_PHOTOS set seq = @seq where activity_group_id = @activity_group_id and seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update ACTIVITY_PHOTOS set seq = @seq + 1 where activity_group_id = @activity_group_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@activity_group_id", this.Owner.ID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

		/// <summary>
		/// Decrements the sequence-number of the item at the given sequence-number, moving it up in the sequence.
		/// </summary>
		/// <param name="Seq">Current sequence-number of the iyem to move.</param>
		public void MoveUp( int Seq ) {
			this.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ACTIVITY_PHOTOS set seq = -1 where activity_group_id = @activity_group_id and seq = @seq; ";
			// Move the item above me down to my spot
			sql += "update ACTIVITY_PHOTOS set seq = @seq where activity_group_id = @activity_group_id and seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update ACTIVITY_PHOTOS set seq = @seq - 1 where activity_group_id = @activity_group_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@activity_group_id", this.Owner.ID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

		//public void CleanupSeqs() {
		//  string sproc = "dbo.renumber_group_photos";
		//  using ( SqlDatabase db = new SqlDatabase() ) {
		//    db.Execute( sproc,
		//      CommandType.StoredProcedure,
		//      db.NewParameter( "@activity_group_id", this.Owner.ID )
		//    );
		//  }
		//}

	}

}
