using System;
using StarrTech.WebTools.Data;
using System.Configuration;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	[SqlTable( "CUSTOMERS", SingularLabel = "Customer", PluralLabel = "Customers", IsMarkedDeleted = true )]
	public class Customer : BaseSqlPersistable {

		#region Cookie operations

		/// <summary>
		/// ATCO-Assigned vendor ID for the host of this site (HOST_VENDOR_AID key in web.config).
		/// Needed to call any of the atco ws methods.
		/// </summary>
		protected static string HostVendorAID {
			get {
				if ( string.IsNullOrEmpty( hostVendorAID ) ) {
					hostVendorAID = ConfigMgr.HostVendorAID;
				}
				return ( hostVendorAID );
			}
		}
		private static string hostVendorAID;

		protected static string CustomerCookieName {
			get { return ( Customer.HostVendorAID + "_CustomerID" ); }
		}
		
		protected static string OpenItinCookieName {
			get { return ( Customer.HostVendorAID + "_OpenItineraryID" ); }
		}

		/// <summary>
		/// Once the user reaches this point, we can tag them with a cookie for this itinerary
		/// so that they can leave the site and when they return, the open itinerary will be restored.
		/// </summary>
		public void SetupItineraryCookie() {
			Itinerary itin = this.CurrentItinerary;
			// Make sure the customer & current itinerary are persisted
			if ( this.ID < 0 ) {
				this.WebsiteID = Website.Current.ID;
				this.Persist(); 
			}
			// Create the Customer ID cookie
			HttpCookie cCookie = new HttpCookie( Customer.CustomerCookieName );
			cCookie.Value = this.ID.ToString();
			cCookie.Expires = DateTime.Now.AddDays( 90 );
			HttpContext.Current.Response.Cookies.Add( cCookie );
			// Create the Current-Itinerary ID cookie
			HttpCookie iCookie = new HttpCookie( Customer.OpenItinCookieName );
			iCookie.Value = itin.ID.ToString();
			iCookie.Expires = DateTime.Now.AddDays( 90 );
			HttpContext.Current.Response.Cookies.Add( iCookie );
		}

		/// <summary>
		/// Clear the itinerary cookie (when the transaction is completed)
		/// </summary>
		public void ClearItineraryCookie( Itinerary itin ) {
			//Itinerary itin = this.CurrentItinerary;
			HttpCookie iCookie = new HttpCookie( Customer.OpenItinCookieName );
			// We delete a cookie by making it expire...
			iCookie.Expires = DateTime.Now.AddDays( -1 );
			HttpContext.Current.Response.Cookies.Add( iCookie );
		}

		/// <summary>
		/// Returns the user's customer object based on the CustomerCookieName cookie
		/// </summary>
		public static Customer RestoreFromCookie() {
			// Restore the Customer...
			Customer aCustomer = new Customer();
			HttpCookie cCookie = HttpContext.Current.Request.Cookies[Customer.CustomerCookieName];
			if ( cCookie != null ) {
				int custID = -1;
				if ( Int32.TryParse( cCookie.Value, out custID ) ) {
					// We've got a hit ...
					aCustomer = new Customer( custID );
					if ( aCustomer.ID < 0 ) {
						aCustomer = new Customer();
					} else {
						try {
							HttpCookie iCookie = HttpContext.Current.Request.Cookies[Customer.OpenItinCookieName];
							if ( iCookie != null ) {
								int itinID = -1;
								if ( Int32.TryParse( iCookie.Value, out itinID ) ) {
									// RKP 20090327 - this is taking way too long...
									//Itinerary itin = aCustomer.Itineraries.GetItemByID( itinID );
									Itinerary itin = new Itinerary( itinID );
									if ( itin.IsCompleted ) {
										itin = null;
									}
									aCustomer.currentItinerary = itin;
								}
							}
						} catch ( Exception e ) {
							ExcLogger.SendTraceReport( "Customer.RestoreFromCookie", "Had to generate a new Customer", e.Message );
							aCustomer = new Customer();
						}
					}
				}
			}
			return ( aCustomer );
		}

		#endregion

		#region Properties

		/// <summary>
		/// Static property to reference the session-specific current customer
		/// </summary>
		public static Customer Current {
			get {
				Customer aCustomer = HttpContext.Current.Session["CurrentCustomer"] as Customer;
				if ( aCustomer == null ) {
					aCustomer = Customer.RestoreFromCookie();
					if ( aCustomer == null ) {
						aCustomer = new Customer();
					}
					HttpContext.Current.Session["CurrentCustomer"] = aCustomer;
				}
				return ( aCustomer );
			}
		}

		#region Database Columns

		//[website_id] [int] NULL,
		[SqlColumn( "website_id", DbType.Int32 )]
		public int WebsiteID {
			get { return ( this.websiteID ); }
			set { this.websiteID = value; }
		}
		private int websiteID = -1;

		//[created_dt] [datetime] NOT NULL,
		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDT {
			get { return ( this.createdDT ); }
			set { this.createdDT = value; }
		}
		private DateTime createdDT = DateTime.Now;

		public string CreatedDateStr {
			get { return ( this.CreatedDT.ToString() ); }
		}

		//[first_name] [nvarchar](50) NOT NULL,
		[SqlColumn( "first_name", DbType.String, Length = 50 )]
		public string FirstName {
			get { return ( this.firstName ); }
			set { this.firstName = value; }
		}
		private string firstName = "";

		//[last_name] [nvarchar](100) NOT NULL,
		[SqlColumn( "last_name", DbType.String, Length = 100 )]
		public string LastName {
			get { return ( this.lastName ); }
			set { this.lastName = value; }
		}
		private string lastName = "";

		//[telephone] [nvarchar](30) NOT NULL,
		[SqlColumn( "telephone", DbType.String, Length = 30 )]
		public string Telephone {
			get { return ( this.telephone ); }
			set { this.telephone = value; }
		}
		private string telephone = "";

		//[email_address] [nvarchar](250) NOT NULL,
		[SqlColumn( "email_address", DbType.String, Length = 250 )]
		public string EmailAddress {
			get { return ( this.emailAddress ); }
			set { this.emailAddress = value; }
		}
		private string emailAddress = "";

		//[password] [nvarchar](50) NOT NULL,
		[SqlColumn( "password", DbType.String, Length = 50 )]
		public string Password {
			get { return ( this.password ); }
			set { this.password = value; }
		}
		private string password = "";

		//[customer_aid] [nvarchar](50) NOT NULL,
		[SqlColumn( "customer_aid", DbType.String, Length = 50 )]
		public string CustomerAID {
			get { return ( this.customerAID ); }
			set { this.customerAID = value; }
		}
		private string customerAID = "";

		//[billing_name] [nvarchar](200) NOT NULL
		[SqlColumn( "billing_name", DbType.String, Length = 200 )]
		public string BillingName {
			get { return ( this.billingName ); }
			set { this.billingName = value; }
		}
		private string billingName = "";

		//[billing_street_1] [nvarchar](200) NOT NULL
		[SqlColumn( "billing_street_1", DbType.String, Length = 200 )]
		public string BillingStreet1 {
			get { return ( this.billingStreet1 ); }
			set { this.billingStreet1 = value; }
		}
		private string billingStreet1 = "";

		//[billing_street_2] [nvarchar](200) NOT NULL
		[SqlColumn( "billing_street_2", DbType.String, Length = 200 )]
		public string BillingStreet2 {
			get { return ( this.billingStreet2 ); }
			set { this.billingStreet2 = value; }
		}
		private string billingStreet2 = "";

		//[billing_city] [nvarchar](200) NOT NULL
		[SqlColumn( "billing_city", DbType.String, Length = 200 )]
		public string BillingCity {
			get { return ( this.billingCity ); }
			set { this.billingCity = value; }
		}
		private string billingCity = "";

		//[billing_state] [nvarchar](100) NOT NULL
		[SqlColumn( "billing_state", DbType.String, Length = 100 )]
		public string BillingState {
			get { return ( this.billingState ); }
			set { this.billingState = value; }
		}
		private string billingState = "";

		//[billing_zip] [nvarchar](15) NOT NULL
		[SqlColumn( "billing_zip", DbType.String, Length = 15 )]
		public string BillingZip {
			get { return ( this.billingZip ); }
			set { this.billingZip = value; }
		}
		private string billingZip = "";

		//[comments] [ntext] NULL,
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		#endregion

		/// <summary>
		/// An arbitrary unique identifier used by the ATCO services
		/// </summary>
		public string UID {
			get {
				if ( this.ID <= 0 ) {
					this.Persist();
				}
				return ( this.ID.ToString() );
			}
		}

		/// <summary>
		/// The Customer's first & last name
		/// </summary>
		public string FullName {
			get { return ( this.firstName + " " + this.lastName ); }
		}
		public string FullNameLF {
			get { return ( this.lastName + ", " + this.firstName ); }
		}

		/// <summary>
		/// The itinerary currently being booked against
		/// </summary>
		public Itinerary CurrentItinerary {
			get {
				if ( this.currentItinerary == null ) {
					//if ( this.ID <= 0 ) {
					//  this.Persist();
					//}
					this.currentItinerary = new Itinerary( this );
					this.Itineraries.Add( this.currentItinerary );
					//this.currentItinerary.Persist();
				}
				return ( this.currentItinerary );
			}
		}
		private Itinerary currentItinerary;

		/// <summary>
		/// The list of previous Itineraries purchased by this customer
		/// </summary>
		public ItinerariesList Itineraries {
			get {
				if ( this.itineraries == null ) {
					this.itineraries = new ItinerariesList( this );
				}
				return ( this.itineraries );
			}
		}
		private ItinerariesList itineraries;

		public Website Website {
			get {
				if ( Websites.Current.ContainsKey( this.WebsiteID ) ) {
					return ( Websites.Current[this.WebsiteID] );
				}
				return ( null );
			}
		}
		public string WebsiteName {
			get { return ( this.Website == null ? "" : this.Website.Name ); }
		}

		#endregion

		#region Constructors

		// Access the base constructors
		public Customer()
			: base() {
			if ( Website.Current != null ) {
				this.WebsiteID = Website.Current.ID;
			}
		}
		public Customer( int ID ) : base( ID ) { }

		public Customer( string aid ) {
			string sql = "select * from CUSTOMERS where ( customer_aid = @aid )";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@aid", aid ) );
			}
		}

		public Customer( string email, int id ) {
			string sql = "select * from CUSTOMERS where ( id = @id and email_address = @email )";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@id", id ), db.NewParameter( "@email", email ) );
			}
		}

		#endregion

		#region Methods

		public override bool Persist() {
			bool result = base.Persist();
			if ( result ) {
				if ( this.currentItinerary != null ) {
					this.currentItinerary.CustomerID = this.ID;
					this.currentItinerary.Persist();
				}
			}
			return ( result );
		}

		public override bool IsValidToDelete() {
			// Can't do this
			return ( false );
		}

		public override bool Delete() {
			// Can't do this
			return ( false );
		}

		#endregion

		#region Static Search Members

		#region Properties

		public static new string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static new string sortExpression = "last_name, first_name asc";

		public static int SWebsiteID {
			get { return ( sWebsiteID ); }
			set { sWebsiteID = value; }
		}
		private static int sWebsiteID = -1;

		public static string SName {
			get { return ( sName ); }
			set { sName = value; }
		}
		private static string sName = "";

		public static string SEmail {
			get { return ( sEmail ); }
			set { sEmail = value; }
		}
		private static string sEmail = "";

		public static string SAtcoID {
			get { return ( sAtcoID ); }
			set { sAtcoID = value; }
		}
		private static string sAtcoID = "";

		public static string SZipCode {
			get { return ( sZipCode ); }
			set { sZipCode = value; }
		}
		private static string sZipCode = "";
		#endregion

		#region Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			Customer.SWebsiteID = -1;
			Customer.SName = "";
			Customer.SEmail = "";
			Customer.SAtcoID = "";
			Customer.SZipCode = "";
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "website_id", Customer.SWebsiteID, ref delim );
			AddSearchExpr( whereBuilder, "last_name", Customer.SName, ref delim );
			AddSearchExpr( whereBuilder, "email_address", Customer.SEmail, ref delim );
			AddSearchExpr( whereBuilder, "customer_aid", Customer.SAtcoID, ref delim );
			AddSearchExpr( whereBuilder, "billing_zip", Customer.SZipCode, ref delim );
			AddSearchExpr( whereBuilder, "is_deleted", false, ref delim );

			string sql = @"select * from	CUSTOMERS ";
			if ( whereBuilder.Length != 0 ) {
				sql += string.Format( " WHERE ( {0} ) ", whereBuilder );
			}
			if ( string.IsNullOrEmpty( Customer.SortExpression ) ) {
				Customer.SortExpression = "name";
			}
			sql += string.Format( " order by {0}", Customer.SortExpression );
			return ( sql );
		}

		public static List<Customer> DoSearchList() {
			return ( SqlDatabase.DoSearch<Customer>( Customer.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
