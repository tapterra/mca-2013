using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Configuration;

namespace ABS {

	[SqlTable( "WEBSITES", SingularLabel = "Website", PluralLabel = "Websites", IsMarkedDeleted = true )]
	public class Website : BaseSqlPersistable {

		#region Database Columns
		//[name] [nvarchar](50) NOT NULL
		[SqlColumn( "name", DbType.String, Length = 50 )]
		public string Name {
			get { return ( this._Name ); }
			set { this._Name = value; }
		}
		private string _Name;

		//[jobno] [nvarchar](50) NOT NULL
		[SqlColumn( "jobno", DbType.String, Length = 50 )]
		public string JobNo {
			get { return ( this._JobNo ); }
			set { this._JobNo = value; }
		}
		private string _JobNo = "";

		//[host_vendor_aid] [nvarchar](50) NOT NULL
		[SqlColumn( "host_vendor_aid", DbType.String, Length = 50 )]
		public string HostVendorAid {
			get { return ( this._HostVendorAid ); }
			set { this._HostVendorAid = value; }
		}
		private string _HostVendorAid;

		// RKP: Begin Change - 20080813
		// RKP: New property for holding the database column site_region_aid, the site-specific
		// RKP: region code used to pull hotels based on a special site-specific region.
		//[site_region_aid] [nvarchar](50) NOT NULL
		[SqlColumn( "site_region_aid", DbType.String, Length = 50 )]
		public string SiteRegionAid {
			get { return ( this._SiteRegionAid ); }
			set { this._SiteRegionAid = value; }
		}
		private string _SiteRegionAid;
		// RKP: End Change

		//[is_booking_testmode] [bit] NOT NULL
		[SqlColumn( "is_booking_testmode", DbType.Boolean )]
		public bool IsBookingTestMode {
			get { return ( this._IsBookingTestMode ); }
			set { this._IsBookingTestMode = value; }
		}
		private bool _IsBookingTestMode;

		//[is_pmtprocessor_testmode] [bit] NOT NULL
		[SqlColumn( "is_pmtprocessor_testmode", DbType.Boolean )]
		public bool IsPmtProcessorTestMode {
			get { return ( this._IsPmtProcessorTestMode ); }
			set { this._IsPmtProcessorTestMode = value; }
		}
		private bool _IsPmtProcessorTestMode;

		//[is_nopayment_site] [bit] NOT NULL
		[SqlColumn( "is_nopayment_site", DbType.Boolean )]
		public bool IsNoPaymentSite {
			get { return ( this._IsNoPaymentSite ); }
			set { this._IsNoPaymentSite = value; }
		}
		private bool _IsNoPaymentSite;

		//[voucher_email_from_address] [ntext] NOT NULL
		[SqlColumn( "voucher_email_from_address", DbType.String )]
		public string VoucherEmailFromAddress {
			get { return ( this._VoucherEmailFromAddress ); }
			set { this._VoucherEmailFromAddress = value; }
		}
		private string _VoucherEmailFromAddress = "";

		//[voucher_email_bcc_addresses] [ntext] NOT NULL
		[SqlColumn( "voucher_email_bcc_addresses", DbType.String )]
		public string VoucherEmailBccAddresses {
			get { return ( this._VoucherEmailBccAddresses ); }
			set { this._VoucherEmailBccAddresses = value; }
		}
		private string _VoucherEmailBccAddresses = "";

		//[voucher_email_subject] [nvarchar](250) NOT NULL
		[SqlColumn( "voucher_email_subject", DbType.String, Length = 250 )]
		public string VoucherEmailSubject {
			get { return ( this._VoucherEmailSubject ); }
			set { this._VoucherEmailSubject = value; }
		}
		private string _VoucherEmailSubject;

		//[host_vendor_address] [ntext] NOT NULL
		[SqlColumn( "host_vendor_address", DbType.String )]
		public string HostVendorAddress {
			get { return ( this._HostVendorAddress ); }
			set { this._HostVendorAddress = value; }
		}
		private string _HostVendorAddress = "";

		//[voucher_logo_url] [nvarchar](250) NOT NULL
		[SqlColumn( "voucher_logo_url", DbType.String, Length = 250 )]
		public string VoucherLogoUrl {
			get { return ( this._VoucherLogoUrl ); }
			set { this._VoucherLogoUrl = value; }
		}
		private string _VoucherLogoUrl;

		//[exc_reports_to_address] [nvarchar](250) NOT NULL
		[SqlColumn( "exc_reports_to_address", DbType.String, Length = 250 )]
		public string ExcReportsToAddress {
			get { return ( this._ExcReportsToAddress ); }
			set { this._ExcReportsToAddress = value; }
		}
		private string _ExcReportsToAddress;

		//[comments] [ntext] NOT NULL
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this._Comments ); }
			set { this._Comments = value; }
		}
		private string _Comments = "";

		[SqlColumn( "booking_start_mo", DbType.Int32 )]
		public int BookingStartMo {
			get {
				if ( this._BookingStartMo <= 0 ) {
					this._BookingStartMo = 1;
				}
				return ( this._BookingStartMo ); 
			}
			set { this._BookingStartMo = value; }
		}
		private int _BookingStartMo = 1;

		[SqlColumn( "booking_start_yr", DbType.Int32 )]
		public int BookingStartYr {
			get {
				if ( this._BookingStartYr <= 0 ) {
					this._BookingStartYr = DateTime.Today.Year;
				}
				return ( this._BookingStartYr ); 
			}
			set { this._BookingStartYr = value; }
		}
		private int _BookingStartYr = -1;

		[SqlColumn( "booking_end_mo", DbType.Int32 )]
		public int BookingEndMo {
			get {
				if ( this._BookingEndMo <= 0 ) {
					this._BookingEndMo = 12;
				}
				return ( this._BookingEndMo ); 
			}
			set { this._BookingEndMo = value; }
		}
		private int _BookingEndMo = 12;

		[SqlColumn( "booking_end_yr", DbType.Int32 )]
		public int BookingEndYr {
			get {
				if ( this._BookingEndYr <= 0 ) {
					this._BookingEndYr = DateTime.Today.Year + 3;
				}
				return ( this._BookingEndYr ); 
			}
			set { this._BookingEndYr = value; }
		}
		private int _BookingEndYr = -1;

		#endregion

		#region Constructors

		// Access the base constructors
		public Website() : base() { }
		public Website( int ID ) : base( ID ) { }

		public Website( string jobNo ) {
			string sql = "select * from WEBSITES where jobno = @jobno";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@jobno", jobNo ) );
			}
		}

		#endregion

		public static Website Current {
			get {
				Website result = null;
				string jobno = ConfigMgr.WebsiteJobNo;
				if ( string.IsNullOrEmpty( jobno ) ) {
					//throw new ArgumentException( "WEBSITE_JOBNO", "Website job-number not defined for this site." );
					return ( null );  
				}
				// Derive a cache key based on the job-number
				string cacheKey = string.Format( "CURRENT_WEBSITE_{0}", jobno );
				// See if we have an instance for this site in the cache
				result = HttpContext.Current.Cache[cacheKey] as Website;
				if ( result == null ) { 
					// Instance not found in the cache - recreate it
					result = new Website( jobno );
					if ( result == null || result.ID < 0 ) {
						throw new ArgumentException( jobno, "Invalid or unknown website job-number defined for this site." );
					}
					// Add the new instance to the cache
					HttpContext.Current.Cache.Insert( 
						cacheKey, result, null, DateTime.Now.AddMinutes( 1 ), Cache.NoSlidingExpiration 
					);
				}
				return ( result );
			}
		}

		public static void ClearCurrentCache() {
			string jobno = ConfigMgr.WebsiteJobNo;
			ClearCurrentCache( jobno );
		}
		
		public static void ClearCurrentCache( string JobNo ) {
			string cacheKey = string.Format( "CURRENT_WEBSITE_{0}", JobNo );
			HttpContext.Current.Cache.Remove( cacheKey );
		}

		public List<ActivityGroup> ActivityGroups( int SubcategoryID ) {
			List<ActivityGroup> result = new List<ActivityGroup>();
			string sql = "select * from ACTIVITY_GROUPS where website_id = @website_id and subcategory_id = @subcategory_id  and is_deleted = 0 ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@website_id", this.ID ), db.NewParameter( "@subcategory_id", SubcategoryID ) ) ) {
					ActivityGroup group = new ActivityGroup();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, group ) ) {
						result.Add( group );
						group = new ActivityGroup();
					}
				}
			}
			return ( result );
		}

		/// <summary>
		/// Return the list of all known PromoCodes for this site
		/// </summary>
		public List<PromoCode> PromoCodes {
			get {
				List<PromoCode> result = new List<PromoCode>();
				// Promo codes do not apply to no-pay sites
				if ( !this.IsNoPaymentSite ) {
					string sql = "select * from PROMO_CODES where website_id = @website_id and is_deleted = 0 order by start_date";
					using ( SqlDatabase db = new SqlDatabase() ) {
						using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@website_id", this.ID ) ) ) {
							PromoCode code = new PromoCode();
							while ( SqlDatabase.InstantiateFromDataRdr( rdr, code ) ) {
								result.Add( code );
								code = new PromoCode();
							}
						}
					}
				}
				return ( result );
			}
		}

		/// <summary>
		/// Returns the list of all known, active (by start/end date) 
		/// PromoCodes for this site
		/// </summary>
		public List<PromoCode> ActivePromoCodes {
			get {
				List<PromoCode> result = new List<PromoCode>();
				// Promo codes do not apply to no-pay sites
				if ( !this.IsNoPaymentSite ) {
					string sql = "select * from PROMO_CODES where website_id = @website_id and ( @today between start_date and end_date ) and is_deleted = 0 order by start_date";
					using ( SqlDatabase db = new SqlDatabase() ) {
						using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@website_id", this.ID ), db.NewParameter( "@today", DateTime.Today ) ) ) {
							PromoCode code = new PromoCode();
							while ( SqlDatabase.InstantiateFromDataRdr( rdr, code ) ) {
								result.Add( code );
								code = new PromoCode();
							}
						}
					}
				}
				return ( result );
			}
		}

		/// <summary>
		/// Given the user-provided promocode string, return the first matching PromoCode 
		/// instance from the site's list of active PromoCodes
		/// </summary>
		/// <param name="UserCode">The code entered by the current user</param>
		/// <returns>First matching PromoCode instance, or null if no match found</returns>
		public PromoCode GetUserPromoCode( string UserCode ) {
			PromoCode result = null;
			if ( !this.IsNoPaymentSite ) {
				List<PromoCode> codes = this.ActivePromoCodes;
				result = codes.Find( delegate( PromoCode code ) {
					return ( code.Code.ToUpper() == UserCode.ToUpper() );
				} );
			}
			return ( result );
		}

		#region Static Search Members

		public static void NewSearch() {}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			string sql = @"select * from	WEBSITES where is_deleted = 0 ";
			if ( string.IsNullOrEmpty( Website.SortExpression ) ) {
				Website.SortExpression = "name";
			}
			sql += string.Format( " order by {0}", Website.SortExpression );
			return ( sql );
		}

		public static List<Website> DoSearchList() {
			return ( SqlDatabase.DoSearch<Website>( Website.GetSearchSql() ) );
		}

		#endregion

	}

}
