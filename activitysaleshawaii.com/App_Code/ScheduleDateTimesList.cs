using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of ScheduleDateTimes in an Activity
	/// Generated entirely by the ATCO service - not represented in the database
	/// </summary>
	public class ScheduleDateTimesList : KeyedCollection<DateTime, ScheduleDateTime> {

		#region Properties

		/// <summary>
		/// The Activity this list is holding ScheduleDateTimes for.
		/// </summary>
		public Activity Owner {
			get { return ( this.owner ); }
		}
		private Activity owner = null;
		
		/// <summary>
		/// The Start Date for the activity
		/// </summary>
		public DateTime StartDate {
			get { return ( this.startDate ); }
			set { 
				bool reload = ( this.startDate != value );
				this.startDate = value; 
				if ( reload ) this.ReloadSchedule();
			}
		}
		private DateTime startDate;
		
		/// <summary>
		/// The End Date for the activity
		/// </summary>
		public DateTime EndDate {
			get { return ( this.endDate ); }
			set { 
				bool reload = ( this.endDate != value );
				this.endDate = value; 
				if ( reload ) this.ReloadSchedule();
			}
		}
		private DateTime endDate;
		
		#endregion
		
		#region Constructors
		
		private ScheduleDateTimesList() {}
		
		public ScheduleDateTimesList( Activity owner ) : base() {
			this.owner = owner;
		}
		
		#endregion
		
		#region Public Methods
		
		// Called when the start/end date changes, to reload the activity's schedule from the voucher service
		public void ReloadSchedule( DateTime Start, DateTime End ) {
			this.startDate = Start;
			this.endDate = End;
			this.ReloadSchedule();
		}

		public void ReloadSchedule() {
			using ( IVoucherService provider = VoucherServiceFactory.NewVoucherService() ) {
				provider.GetActivitySchedule( this.Owner );
			}
		}

		protected override DateTime GetKeyForItem( ScheduleDateTime item ) {
			return ( item.Value );
		}

		#endregion

	}
	
}
