using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Security.Principal;
using System.Text;

using StarrTech.WebTools;
using StarrTech.WebTools.Data;

namespace ABS {
	[SqlTable( "USERS", SingularLabel = "User", PluralLabel = "Users", IsMarkedDeleted = true )]
	public class SiteUser : BaseSqlPersistable {

		#region Properties

		[SqlColumn( "first_name", DbType.String, Length = 100 )]
		public string FirstName {
			get { return ( firstName ); }
			set { firstName = value; }
		}
		private string firstName = "";

		[SqlColumn( "last_name", DbType.String, Length = 200 )]
		public string LastName {
			get { return ( lastName ); }
			set { lastName = value; }
		}
		private string lastName = "";

		[SqlColumn( "title", DbType.String, Length = 200 )]
		public string Title {
			get { return ( title ); }
			set { title = value; }
		}
		private string title = "";

		[SqlColumn( "email_address", DbType.String, Length = 200 )]
		public string EmailAddress {
			get { return ( emailAddress ); }
			set { emailAddress = value; }
		}
		private string emailAddress = "";

		[SqlColumn( "password", DbType.String, Length = 20 )]
		public string Password {
			get { return ( password ); }
			set { password = value; }
		}
		private string password = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( comments ); }
			set { comments = value; }
		}
		private string comments = "";

		public ArrayList GroupIDs {
			get { return ( groupids ); }
		}
		private ArrayList groupids = new ArrayList();

		public ArrayList Permissions {
			get { return ( permissions ); }
		}
		private ArrayList permissions = new ArrayList();

		public string FullName {
			get { return ( this.FirstName + " " + this.LastName ); }
		}

		public string FullNameLF {
			get { return ( this.LastName + ", " + this.FirstName ); }
		}

		#endregion

		#region Constructors
		// Access the base constructors
		public SiteUser() : base() { }
		public SiteUser( int ID ) : base( ID ) { }

		/// <summary>
		/// Create an instance at startup, by email address 
		/// </summary>
		/// <param name="winName">Email address to key on</param>
		public SiteUser( string Email ) : base() {
			string sql = string.Format( "select * from USERS where email_address = '{0}' and is_deleted = 0 ", Email );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql );
			}
			this.RestoreGroupIDs();
		}

		/// <summary>
		/// Create an unpersisted instance from user-provided property-values.
		/// </summary>
		public SiteUser( string EmailAddress, string Password, string FirstName, string LastName, string Title, string Comments ) : base() {
			this.EmailAddress = EmailAddress;
			this.FirstName = FirstName;
			this.LastName = LastName;
			this.Title = Title;
			this.Password = Password;
			this.Comments = Comments;
		}

		#endregion

		#region Public Methods

		#region SqlPersistable...

		/// <summary>
		/// Verify that it's OK to persist this User
		/// Overridden to verify that WindowsName and EmailAddress are unique
		/// </summary>
		/// <returns>True if it's OK to persist</returns>
		public override bool IsValidToPersist() {
			this.ValidationMessages.Clear();
			bool isValid = false;
			SiteUser testUser = new SiteUser();
			// Make sure EmailAddress is unique
			if ( this.EmailAddress != "" ) {
				// This follows the same approach...
				string sql = string.Format( "select * from USERS where email_address = '{0}' and is_deleted = 0 ",
					this.EmailAddress );
				using ( SqlDatabase db = new SqlDatabase() ) {
					db.SelectInstance( testUser, sql );
				}
				isValid = ( testUser.ID == this.ID || testUser.ID == -1 );
				if ( !isValid ) {
					this.ValidationMessages.Add(
						"Another user in the database has the same Email Address - please enter a unique value."
					);
				}
			}
			if ( !isValid )
				return ( false );
			return ( base.IsValidToPersist() );
		}

		/// <summary>
		/// Saves this instance to the database
		/// Overridden to handle persisting the group-membership join records
		/// </summary>
		/// <returns>True if the record was persisted successfully</returns>
		public override bool Persist() {
			bool isValid = this.IsValidToPersist();
			if ( isValid ) {
				if ( base.Persist() ) {
					this.PersistGroupIDs();
				}
			}
			return ( isValid );
		}

		/// <summary>
		/// Restores this instance from it's database record.
		/// Overridden to restore the group memberships.
		/// </summary>
		public override void Restore() {
			base.Restore();
			this.RestoreGroupIDs();
		}

		/// <summary>
		/// Removes the record for this instance from the database.
		/// Overridden to clean up the group memberships.
		/// </summary>
		/// <returns>True if the record was successfully deleted.</returns>
		public override bool Delete() {
			bool isValid = this.IsValidToDelete();
			if ( isValid ) {
				// Delete any existing group-memberships first
				this.DeleteGroupIDs();
				isValid = base.Delete();
			}
			return ( isValid );
		}

		#endregion

		#region Permissions...

		/// <summary>
		/// Given a permission-key, determine if this user has the corresponding or dependent permission.
		/// </summary>
		/// <param name="Key">Key to test</param>
		/// <returns>True if this user has any keys that begin with or match the given key.</returns>
		public bool HasPermission( PermissionKeys Key ) {
			return ( HasPermission( Key.ToString() ) );
		}
		public bool HasPermission( string Key ) {
			// Everyone has permission to do nothing...
			if ( string.IsNullOrEmpty( Key ) ) {
				return ( true );
			}
			if ( Key == "None" ) {
				return ( true );
			}
			// Look for a Key that's an exact match...
			if ( this.Permissions.Contains( Key ) ) {
				return ( true );
			}
			// Look at each of the keys until we find one that starts with our test key
			foreach ( string aKey in this.Permissions ) {
				if ( aKey.StartsWith( Key ) ) {
					return ( true );
				}
			}
			// No joy...
			return ( false );
		}

		#endregion

		#endregion

		#region Private Methods (working with group-memberships)

		/// <summary>
		/// Save the GroupIDs list for this user to the database
		/// </summary>
		protected void PersistGroupIDs() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			// First, we want to delete any existing GROUP_USERS records
			this.DeleteGroupIDs( false );
			// Now, we can simply insert a new GROUP_USERS record for each GroupIDs item
			using ( SqlDatabase db = new SqlDatabase() ) {
				foreach ( int groupID in this.GroupIDs ) {
					string sql = string.Format(
						"INSERT INTO GROUP_USERS ( user_id, group_id ) VALUES ( {0}, {1} );", this.ID, groupID
					);
					db.Execute( sql );
				}
			}
		}

		/// <summary>
		/// Load the GroupIDs list for this User's group memberships
		/// </summary>
		protected void RestoreGroupIDs() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			string sql = string.Format( "select group_id from GROUP_USERS where user_id = {0} ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					while ( rdr.Read() ) {
						this.GroupIDs.Add( rdr.GetInt32( rdr.GetOrdinal( "group_id" ) ) );
					}
				}
			}
			this.RestorePermissions();
		}

		/// <summary>
		/// Delete all the group-membership records for this group
		/// </summary>
		protected void DeleteGroupIDs() {
			this.DeleteGroupIDs( true );
		}

		/// <summary>
		/// Delete all the group-membership records for this group
		/// Overloaded to allow the records to be deleted without clearing the GroupIDs collection.
		/// </summary>
		/// <param name="ClearList">Pass false to delete the records without clearing the GroupIDs</param>
		protected void DeleteGroupIDs( bool ClearList ) {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			string sql = string.Format( "delete from GROUP_USERS where user_id = '{0}' ", this.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql );
			}
			if ( ClearList )
				this.GroupIDs.Clear(); // don't need these no more
		}

		/// <summary>
		/// Load the Permissions list with this User's permission keywords
		/// </summary>
		protected void RestorePermissions() {
			if ( this.ID <= 0 )
				return; // Make sure we have a valid ID
			if ( this.GroupIDs.Count <= 0 )
				return; // If we don't have any groups, we're done
			// Build a comma-delimited list of all of this user's group-IDs
			try {
				string[] groupIDsArray = new string[this.GroupIDs.Count];
				for ( int i = 0 ; i < this.GroupIDs.Count ; i++ ) {
					groupIDsArray[i] = this.GroupIDs[i].ToString();
				}
				// Now load a list of all of these grups' permission keywords
				string sql = string.Format(
					"select permission_key from GROUP_PERMISSIONS where group_id in ( {0} ) ",
					string.Join( ", ", groupIDsArray )
					);
				using ( SqlDatabase db = new SqlDatabase() ) {
					using ( IDataReader rdr = db.SelectRecords( sql ) ) {
						while ( rdr.Read() ) {
							this.Permissions.Add( rdr.GetString( rdr.GetOrdinal( "permission_key" ) ) );
						}
					}
				}
			} catch ( Exception e ) {
				System.Diagnostics.Trace.Write( e.Message );
			}
		}

		#endregion

		#region Static Factory Methods

		/// <summary>
		/// Generates a new User instance for a user, based on email address
		/// </summary>
		/// <returns>A User instance representing the user with the given email address.</returns>
		public static SiteUser GetCurrent( string email ) {
			SiteUser newUser = new SiteUser();
			string sql = string.Format( "select * from USERS where email_address = '{0}' and is_deleted = 0 ", email );
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( newUser, sql );
			}
			if ( newUser == null )
				return ( null ); // won't happen, just being anal
			if ( newUser.ID <= 0 )
				return ( null );
			newUser.RestoreGroupIDs();
			return ( newUser );
		}

		/// <summary>
		/// Given a user ID, return the user's full name (LF)
		/// </summary>
		public static string GetUserName( int id ) {
			SiteUser user = new SiteUser( id );
			return ( user.FullNameLF );
		}

		public static SiteUser CurrentUser {
			get {
				return ( System.Web.HttpContext.Current.Session["CurrentUser"] as SiteUser );
			}
			set {
				System.Web.HttpContext.Current.Session["CurrentUser"] = value;
				if ( value != null ) {
					System.Web.HttpContext.Current.Session["CurrentUserID"] = value.ID;
				}
			}
		}

		#endregion

		#region Static (search) Members

		#region Properties

		public static string SFirstName {
			get { return ( sfirstName ); }
			set { sfirstName = value; }
		}
		private static string sfirstName = "";

		public static string SLastName {
			get { return ( slastName ); }
			set { slastName = value; }
		}
		private static string slastName = "";

		public static string STitle {
			get { return ( stitle ); }
			set { stitle = value; }
		}
		private static string stitle = "";

		#endregion

		#region Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			SFirstName = "";
			SLastName = "";
			STitle = "";
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "u.first_name", SFirstName, ref delim );
			AddSearchExpr( whereBuilder, "u.last_name", SLastName, ref delim );
			AddSearchExpr( whereBuilder, "u.title", STitle, ref delim );
			// No deleted records
			whereBuilder.AppendFormat( "{0} ( u.is_deleted = 0 ) ", delim );

			string sql = @"select u.*, full_name = u.last_name + ', ' + u.first_name from USERS as u ";
			if ( whereBuilder.Length != 0 ) {
				sql += string.Format( " WHERE ( {0} ) ", whereBuilder );
			}
			if ( string.IsNullOrEmpty( SortExpression ) ) {
				SortExpression = "full_name";
			}
			sql += string.Format( " ORDER BY {0}", SortExpression );
			return ( sql );
		}

		public static List<SiteUser> DoSearchList() {
			return ( SqlDatabase.DoSearch<SiteUser>( SiteUser.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}
}
