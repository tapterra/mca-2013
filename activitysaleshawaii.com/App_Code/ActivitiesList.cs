using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {

	public class ActivitiesList : KeyedCollection<int, Activity> {

		/// <summary>
		/// The Activity this list is holding options for.
		/// </summary>
		public ActivityGroup Owner {
			get { return ( this.owner ); }
		}
		private ActivityGroup owner = null;

		public ActivitiesList( ActivityGroup group ) : base() {
			if ( group.ID <= 0 ) {
				group.Persist();
			}
			this.owner = group;
			this.LoadItems();
		}

		protected override int GetKeyForItem( Activity item ) {
			return ( item.ID );
		}

		protected override void InsertItem( int index, Activity item ) {
			base.InsertItem( index, item );
			item.ActivityGroupID = Owner.ID;
			if ( item.Seq != index ) {
				item.Seq = index;
				item.Persist();
			}
		}

		private void LoadItems() {
			if ( this.Owner == null || this.Owner.ID <= 0 ) {
				return;
			}
			string sql = string.Format( "select * from ACTIVITIES where ( activity_group_id = {0} and is_deleted = 0 ) order by seq", this.Owner.ID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Activity item = new Activity();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						item.ActivityGroup = this.Owner;
						this.Add( item );
						item = new Activity();
					}
				}
			}
		}

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_group_activities";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure, db.NewParameter( "@group_id", this.Owner.ID ) );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public int HighestSeq {
			get { return ( this.ResetItemRanks() - 1 ); }
		}

		/// <summary>
		/// Increments the sequence-number of the item at the given sequence-number, moving it down in the sequence.
		/// </summary>
		/// <param name="ActivitySeq">Current sequence-number of the item to move.</param>
		public void MoveDown( int Seq ) {
			// We can't demote if we're already at the end
			if ( Seq >= this.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ACTIVITIES set seq = -1 where activity_group_id = @activity_group_id and seq = @seq; ";
			// Move the Activity below me up to my spot
			sql += "update ACTIVITIES set seq = @seq where activity_group_id = @activity_group_id and seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update ACTIVITIES set seq = @seq + 1 where activity_group_id = @activity_group_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@activity_group_id", this.Owner.ID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

		/// <summary>
		/// Decrements the sequence-number of the item at the given sequence-number, moving it up in the sequence.
		/// </summary>
		/// <param name="Seq">Current sequence-number of the iyem to move.</param>
		public void MoveUp( int Seq ) {
			this.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ACTIVITIES set seq = -1 where activity_group_id = @activity_group_id and seq = @seq; ";
			// Move the Activity above me down to my spot
			sql += "update ACTIVITIES set seq = @seq where activity_group_id = @activity_group_id and seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update ACTIVITIES set seq = @seq - 1 where activity_group_id = @activity_group_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@activity_group_id", this.Owner.ID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

	}

}
