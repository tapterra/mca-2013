using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;

using StarrTech.WebTools.Data;

namespace ABS {

	public class ItinerariesList : KeyedCollection<int, Itinerary> {

		private Customer Customer;

		public ItinerariesList( Customer customer ) : base() {
			this.Customer = customer;
			this.LoadItems();
		}

		protected override int GetKeyForItem( Itinerary item ) {
			return ( item.ID );
		}

		private void LoadItems() {
			if ( this.Customer == null ) {
				return;
			}
			string sql = "select * from ITINERARIES where ( customer_id = @customer_id and is_deleted = 0 ) order by created_dt desc";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@customer_id", this.Customer.ID ) ) ) {
					Itinerary itinerary = new Itinerary( this.Customer );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, itinerary ) ) {
						this.Add( itinerary );
						itinerary = new Itinerary( this.Customer );
					}
				}
			}
		}

	}

}
