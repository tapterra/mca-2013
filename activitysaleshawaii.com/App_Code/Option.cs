using System;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	
	public enum OptionResponseTypes {
		NotSpecified,
		PlainText,
		DropDownList,
		Checkbox
	}
	
	[SqlTable( "ACTIVITY_OPTIONS", SingularLabel = "Activity Option", PluralLabel = "Activity Options", IsMarkedDeleted = true )]
	public class Option : BaseSqlPersistable {
		
		#region Properties

		//  [activity_group_id] [int] NOT NULL,
		[SqlColumn( "activity_group_id", DbType.Int32 )]
		public int ActivityGroupID {
			get { return ( this.activityGroupID ); }
			set { this.activityGroupID = value; }
		}
		private int activityGroupID = -1;

		//[seq] [int] NOT NULL,
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//  [question_text] [nvarchar](250) NOT NULL,
		[SqlColumn( "question_text", DbType.String, Length = 250 )]
		public string QuestionText {
			get { return ( this.questionText ); }
			set { this.questionText = value; }
		}
		private string questionText;

		//  [response_type] [int] NOT NULL,
		[SqlColumn( "response_type", DbType.Int32 )]
		public int ResponseTypeDB {
			get { return ( (int) this.responseType ); }
			set { this.responseType = (OptionResponseTypes) value; }
		}
		public OptionResponseTypes ResponseType {
			get { return ( this.responseType ); }
			set { this.responseType = value; }
		}
		private OptionResponseTypes responseType = OptionResponseTypes.NotSpecified;

		//  [response_choices] [ntext] NOT NULL,
		[SqlColumn( "response_choices", DbType.String )]
		public string ResponseChoices {
			get { return ( this.responseChoices ); }
			set { this.responseChoices = value; }
		}
		private string responseChoices;

		//  [comments] [ntext] NOT NULL,
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		public string ControlID {
			get { return ( string.Format( "opt{0}", this.ID ) ); }
		}

		#endregion

		#region Constructors

		public Option() : base() { }
		public Option( int ID ) : base( ID ) { }

		#endregion

		#region Methods

		/// <summary>
		/// Produces a deep copy of this Option 
		/// </summary>
		protected internal Option DeepCopy() {
			// Make a shallow copy of this instance
			Option newOption = (Option) this.MemberwiseClone();
			newOption.ID = -1;
			// nothing deeper here?
			return ( newOption );
		}

		#endregion
			
	}

}
