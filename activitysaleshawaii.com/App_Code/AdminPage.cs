using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Xml;

using StarrTech.WebTools;
using StarrTech.WebTools.CMS;
using StarrTech.WebTools.Controls;
using StarrTech.WebTools.Data;

namespace ABS {

	public class AdminPage : System.Web.UI.Page {

		#region Properties

		//public string MenuXmlFile {
		//  get { return ( menuxmlfile ); }
		//  set { menuxmlfile = value; }
		//}
		//private string menuxmlfile = "";

		public PermissionKeys PermissionKey {
			get { return ( permissionKey ); }
			set { permissionKey = value; }
		}
		private PermissionKeys permissionKey = PermissionKeys.None;

		public virtual bool UserAuthenticated {
			get { return ( SiteUser.CurrentUser != null ); }
		}

		public virtual bool UserAuthorized {
			get {
				if ( !this.UserAuthenticated )
					return ( false );
				return ( SiteUser.CurrentUser.HasPermission( this.PermissionKey ) );
			}
		}

		public virtual bool IsLogonPage {
			get { return ( false ); }
		}

		public virtual bool ShowNewsPanel {
			get { return ( false ); }
			set { }
		}

		public virtual bool ShowEventsPanel {
			get { return ( false ); }
			set { }
		}

		public virtual string HeaderUrlOverride {
			get { return ( "" ); }
		}

		#endregion

		#region Constructors

		public AdminPage()
			: base() {
			//this.MenuXmlFile = "~/App_Data/AdminMenu.config";
			this.ClientTarget = "uplevel";
		}

		#endregion

		#region Methods

		public virtual void AuthorizeUser( PermissionKeys pageKey ) {
			this.PermissionKey = pageKey;
			this.AuthorizeUser();
		}

		public virtual void AuthorizeUser() {
			if ( !this.UserAuthenticated ) {
				if ( !this.IsLogonPage ) {
					Response.Redirect( "login.aspx", true );
				}
				return;
			}
			if ( !this.UserAuthorized ) {
				Response.Redirect( "notauthorized.aspx", true );
			}
		}

		protected override void OnInit( EventArgs e ) {
			this.AuthorizeUser();
			base.OnInit( e );
		}

		#endregion

	}

}
