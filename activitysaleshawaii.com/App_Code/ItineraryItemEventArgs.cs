using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {
	public class ItineraryItemEventArgs : EventArgs {

		public int BookingIndex {
			get { return ( this.bookingIndex ); }
			set { this.bookingIndex = value; }
		}
		private int bookingIndex;

		public ItineraryItemEventArgs( int index ) {
			this.BookingIndex = index;
		}

	}

	public delegate void ItineraryItemEventHandler( object sender, ItineraryItemEventArgs e );

}
