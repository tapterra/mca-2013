namespace ABS {

	public class rss {

		[System.Xml.Serialization.XmlAttributeAttribute]
		public string version = "2.0";

		public channel channel;

	}

	public class channel {

		// required
		public string title = "ATCO Activity RSS Feed. Authorized use only.";
		public string link = "http://www.atcosoftware.com/";
		public string description = "Authorized use only.";
		[System.Xml.Serialization.XmlElementAttribute]
		public item[] item;

		// recommended
		public string managingEditor = "someone@mca.com";
		public string webMaster = "us@atcosoftware.com";
		public Rfc822DateTime pubDate = new Rfc822DateTime( System.DateTime.Parse( "12/28/2005 3:00:00 am" ) );
		public Rfc822DateTime lastBuildDate = new Rfc822DateTime( System.DateTime.Parse( "12/28/2005 3:00:00 am" ) );
		public string generator = "ATCOSoftware RSS Feed alpha";
		public string docs = "http://www.atcosoftware.com/docs/todo";

		// optional
		public int ttl;
		[System.Xml.Serialization.XmlIgnoreAttribute]
		public bool ttlSpecified;
	}

	//public class item {
	//  // 1 of the following required; all recommended
	//  public string title;
	//  public string link;
	//  public string description;

	//  // optional
	//  public guid guid;
	//  public Rfc822DateTime pubDate;

	//  // atco stuff
	//  [System.Xml.Serialization.XmlElement( Namespace = "http://atcosoftware.com/docs/rss/price" )]
	//  public decimal price;

	//  [System.Xml.Serialization.XmlElement( Namespace = "http://atcosoftware.com/docs/rss/price" )]
	//  public Rfc822DateTime expireDate;

	//  public string category;

	//}
	public class item {
		// 1 of the following required; all recommended
		public string title;
		public string link;
		public string description;

		// optional
		public guid guid;
		public Rfc822DateTime pubDate;
		[System.Xml.Serialization.XmlElementAttribute]
		public category[] category;

		// atco stuff
		[System.Xml.Serialization.XmlElement( Namespace = "http://atcosoftware.com/docs/rss/price" )]
		public decimal price;
		[System.Xml.Serialization.XmlElement( Namespace = "http://atcosoftware.com/docs/rss/price" )]
		public Rfc822DateTime expireDate;
	}

	public class category {
		[System.Xml.Serialization.XmlAttributeAttribute]
		public string domain;
		[System.Xml.Serialization.XmlAttributeAttribute( Namespace = "http://atcosoftware.com/docs/rss/region" )]
		public string regionId;
		[System.Xml.Serialization.XmlTextAttribute]
		public string Text;
	}

	public class guid {

		[System.Xml.Serialization.XmlAttributeAttribute]
		[System.ComponentModel.DefaultValueAttribute( true )]
		public bool isPermaLink = true; // specified in RSS std.

		[System.Xml.Serialization.XmlTextAttribute]
		public string Text;

	}

	public class Rfc822DateTime {

		[System.Xml.Serialization.XmlIgnoreAttribute]
		public System.DateTime SysDateTime;

		[System.Xml.Serialization.XmlTextAttribute]
		public string Value {
			get { return SysDateTime.ToString( "r" ); }
			set { SysDateTime = System.DateTime.Parse( value ); }
		}

		public Rfc822DateTime() { }

		public Rfc822DateTime( System.DateTime Date ) {
			this.SysDateTime = Date;
		}
	}

	public class RssClient {

		public static rss GetRss( string RssUrl ) {
			System.Net.HttpWebRequest Req = (System.Net.HttpWebRequest) System.Net.WebRequest.Create( RssUrl );
			System.Net.HttpWebResponse Res = (System.Net.HttpWebResponse) Req.GetResponse();
			System.Xml.Serialization.XmlSerializer Ser = new System.Xml.Serialization.XmlSerializer( typeof( rss ) );
			rss o = (rss) Ser.Deserialize( Res.GetResponseStream() );
			Res.Close();
			return o;
		}

	}

}