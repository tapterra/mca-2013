﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	public class TestDemoPaymentProcessor : PaymentProcessor {

		#region IPaymentProcessor Implementation
		
		public override PaymentProcessorResponses DoAuthorizeTransaction() {
			return ( this.ProcessTransaction() );
		}

		public override PaymentProcessorResponses DoCaptureTransaction() {
			return ( this.ProcessTransaction() );
		}

		// Not currently being used
		public override PaymentProcessorResponses DoSaleTransaction() {
			return ( this.ProcessTransaction() );
		}

		#endregion

		private PaymentProcessorResponses ProcessTransaction() {
			// If this is a No-Payment site...
			if ( Website.Current.IsNoPaymentSite ) {
				// TODO Verify the proper "dummy" authcode for ATCO
				this.AuthCode = "NOPAYSITE";
				this.ResponseCode = PaymentProcessorResponses.ApprovedResponse.ToString();
				this.ResponseMessage = "";
				PaymentProcessorResponses dummyResponse = PaymentProcessorResponses.ApprovedResponse;
				return ( dummyResponse );
			}
			PaymentProcessorResponses demoResponse;
			// To trigger a failure, pass a value of "1" for the MONTH that the card expires
			if ( this.CardExpiresMM == 1 ) {
				this.AuthCode = "";
				this.ResponseCode = PaymentProcessorResponses.DeclinedResponse.ToString();
				this.ResponseMessage = "TESTING - Payment Declined";
				demoResponse = PaymentProcessorResponses.DeclinedResponse;
			} else {
				this.AuthCode = "A123456";
				this.ResponseCode = PaymentProcessorResponses.ApprovedResponse.ToString();
				this.ResponseMessage = "";
				demoResponse = PaymentProcessorResponses.ApprovedResponse;
			}
			return ( demoResponse );

		}


	}

}
