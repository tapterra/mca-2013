using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

using System.Configuration;
using System.Collections.ObjectModel;

namespace ABS {

	[SqlTable( "PAYMENTS", SingularLabel = "Payment", PluralLabel = "Payments", IsMarkedDeleted = true )]
	public class Payment : BaseSqlPersistable {

		#region Database Columns
		//  [itinerary_id] [int]
		[SqlColumn( "itinerary_id", DbType.Int32 )]
		public int ItineraryID {
			get { return ( this.itineraryID ); }
			set { this.itineraryID = value; }
		}
		private int itineraryID;

		//  [posted_dt] [datetime]
		[SqlColumn( "posted_dt", DbType.DateTime )]
		public DateTime PostedDT {
			get { return ( this.postedDT ); }
			set { this.postedDT = value; }
		}
		private DateTime postedDT = DateTime.Now;

		//  [method_type] [nvarchar](20)
		[SqlColumn( "method_type", DbType.String, Length = 20 )]
		public string MethodType {
			get { return ( this.methodType ); }
			set { this.methodType = value; }
		}
		private string methodType = "";
		
		//  [amount] [money]
		[SqlColumn( "amount", DbType.Currency )]
		public decimal Amount {
			get { return ( this.amount ); }
			set { this.amount = value; }
		}
		private decimal amount;
		
		//  [auth_code] [nvarchar](20)
		[SqlColumn( "auth_code", DbType.String, Length = 20 )]
		public string AuthCode {
			get { return ( this.authCode ); }
			set { this.authCode = value; }
		}
		private string authCode = "";
		
		//  [cc_ref] [nchar](5)
		[SqlColumn( "cc_ref", DbType.String, Length = 5 )]
		public string CCRef {
			get { return ( this.ccRef ); }
			set {
				this.ccRef = value;
				// We only want to save the last 5 characters ...
				if ( this.ccRef.Length > 5 ) {
					this.ccRef = this.ccRef.Substring( this.ccRef.Length - 5, 5 );
				}
			}
		}
		private string ccRef = "";

		#endregion

		public Itinerary Itinerary {
			get {
				if ( this.itinerary == null ) {
					if ( this.ItineraryID >= 0 ) {
						this.itinerary = new Itinerary( this.ItineraryID );
					} else {
						this.itinerary = Customer.Current.CurrentItinerary;
					}
				}
				return ( this.itinerary );
			}
		}
		private Itinerary itinerary;

		public string AmountStr {
			get {
				Website site = this.Itinerary.Website;
				if ( site.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.Amount ) );
				}
			}
		}

		#region Constructors

		// Access the base constructors
		private Payment() : base() { }
		public Payment( int ID ) : base( ID ) { }

		public Payment( Itinerary itinerary ) : base() {
			this.itineraryID = itinerary.ID;
		}

		#endregion
	}

}
