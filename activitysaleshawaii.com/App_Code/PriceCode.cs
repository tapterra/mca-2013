using System;

namespace ABS {

	/// <summary>
	/// Represents one of the price categories ("Adult", Child" etc.) for a given activity.
	/// </summary>
	public class PriceCode {
		
		#region Properties

		/// <summary>
		/// The ATCO Identification code for this PriceCode
		/// </summary>
		public string AID {
			get { return ( this.aid ); }
		}
		private string aid;

		/// <summary>
		/// Not sure what this is for, but the ATCO service needs it
		/// </summary>
		public string UID {
			get { return ( this.uid ); }
		}
		private string uid;

		/// <summary>
		/// The displayed label for this price category
		/// </summary>
		public string Label {
			get { return ( this.label ); }
		}
		private string label;
		
		/// <summary>
		/// The price for this guest-type
		/// </summary>
		public decimal Price {
			get { return ( this.price ); }
		}
		private decimal price;

		public string PriceStr {
			get {
				if ( Website.Current.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$ {0:F}", this.Price ) );
				}
			}
		}
		
		#endregion
		
		#region Constructors
		// We don't want any un-initialized instances
		private PriceCode() {}

		public PriceCode( string AID, string UID, string Label, decimal Price ) {
			this.aid = AID;
			this.uid = UID;
			this.label = Label;
			this.price = Price;
		}
		
		#endregion
		
	}

}
