﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ABS {

	/// <summary>
	/// Interface for voucher/inventory service providers
	/// </summary>
	public interface IVoucherService: IDisposable {

		/// <summary>
		/// A simple keyed listing of all known hotel IDs/titles
		/// </summary>
		Dictionary<string, Hotel> Hotels { get; }

		/// <summary>
		/// Generates a Region-specific list of Hotels from the Hotels property (above)
		/// </summary>
		/// <param name="RegionID">Region ID to filter by</param>
		/// <returns>The filtered list of Hotels</returns>
		Dictionary<string, Hotel> RegionHotels( string RegionID );

		/// <summary>
		/// Not sure if this is still used/works
		/// </summary>
		void FlushCache();

		/// <summary>
		/// Given an Activity instance with an ID and valid Schedule Start & End Dates, 
		/// populate the list of ScheduleDateTimes and PriceCodes for that period.
		/// </summary>
		bool GetActivitySchedule( Activity theActivity );

		/// <summary>
		/// Given the current Itinerary with a customer UID and at least one Booking, temporarily reserve
		/// the required inventory for each activity, and mark each booking as (un)successfully reserved.
		/// This will be called prior to check-out to insure available inventory for each booking prior to 
		/// processing the payment.
		/// </summary>
		/// <returns>True if all bookings were successfully reserved.</returns>
		bool ReserveBookings();

		/// <summary>
		/// Given an Itinerary instance with previously-reserved Bookings and confirmed CC payment info,
		/// post the payment info to ATCO, finalize the inventory, and generate data needed to produce 
		/// vouchers.
		/// </summary>
		bool PayForItinerary();

		/// <summary>
		/// Call when payment or inventory fails to cancel seats reserved prior to the failure.
		/// </summary>
		void CancelBookings( Itinerary itin, string TraceMessage );

		/// <summary>
		/// Given the current paid-for Itinerary, with a list of bookings and a list of corresponding 
		/// PrintableVouchers, produce the HTML string for the vouchers. This HTML will be suitable for 
		/// display in a browser or for delivery to the customer by email based on the value of 
		/// FullPage (false for email).
		/// </summary>
		/// <returns>HTML stream for the formatted vouchers</returns>
		string BuildVouchers( bool FullPage );

		/// <summary>
		/// Send the vouchers to the customer by email
		/// </summary>
		void SendVoucherEmail();

	}

}
