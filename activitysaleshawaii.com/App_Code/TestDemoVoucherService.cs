﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace ABS {

	/// <summary>
	/// Testing interface to the ATCO API
	/// </summary>
	public class TestDemoVoucherService : IVoucherService {

		#region IVoucherService Implementation

		/// <summary>
		/// A simple keyed listing of all known hotel AIDs/titles
		/// </summary>
		public Dictionary<string, Hotel> Hotels {
			get {
				// Create the dictionary for holding the key-value list
				Dictionary<string, Hotel> results = new Dictionary<string, Hotel>();
				// Load the dictionary
				try {
					rss feed = RssClient.GetRss( ConfigMgr.HotelsListRssUrl );
					foreach ( item i in feed.channel.item ) {
						// Parse the ATCO ID and hotel-name from the feed's current item
						string aid = i.guid.Text;
						string name = i.title;
						// Loop over the item categories and build the Hotel's list of regionAIDs
						Hotel hotel = new Hotel( aid, name );
						foreach ( category c in i.category ) {
							hotel.RegionAIDs.Add( c.regionId );
						}
						results[aid] = hotel;
					}
				} catch ( Exception e ) {
					ExcLogger.SendExceptionReport( "TestDemoVoucherService.Hotels", e.Message );
				}
				return ( results );
			}
		}

		/// <summary>
		/// Generates a Region-specific list of Hotels from the Hotels property (above)
		/// </summary>
		/// <param name="RegionAID">Region AID to filter by</param>
		/// <returns>The filtered list of Hotels</returns>
		public Dictionary<string, Hotel> RegionHotels( string RegionAID ) {
			// Loop over the global Hotels list and build a subset list for the given RegionAID
			Dictionary<string, Hotel> results = new Dictionary<string, Hotel>();
			Dictionary<string, Hotel> hotels = this.Hotels;
			foreach ( Hotel hotel in hotels.Values ) {
				if ( hotel.RegionAIDs.Contains( RegionAID ) ) {
					results.Add( hotel.AID, hotel );
				}
			}
			return ( results );
		}

		/// <summary>
		/// Given an Activity instance with an AID and valid Schedule Start & End Dates, 
		/// populate the list of ScheduleDateTimes and PriceCodes for that period.
		/// </summary>
		public bool GetActivitySchedule( Activity theActivity ) {
			bool result = true;
			theActivity.ScheduleDateTimes.Clear();
			for ( DateTime dt = theActivity.ScheduleDateTimes.StartDate ; dt < theActivity.ScheduleDateTimes.EndDate ; dt = dt.AddDays( 1 ) ) {
				ScheduleDateTime sdt = new ScheduleDateTime( dt, "InvUID", 10 );
				sdt.PriceCodes.Add( new PriceCode( string.Format( "A{0}A", theActivity.ID ), string.Format( "A{0}A", theActivity.ID ), "Adult", 120.00M ) );
				sdt.PriceCodes.Add( new PriceCode( string.Format( "A{0}C", theActivity.ID ), string.Format( "A{0}C", theActivity.ID ), "Child", 100.00M ) );
				sdt.PriceCodes.Add( new PriceCode( string.Format( "A{0}S", theActivity.ID ), string.Format( "A{0}S", theActivity.ID ), "Senior", 1200.00M ) );
				theActivity.ScheduleDateTimes.Add( sdt );
			}
			return ( result );
		}

		/// <summary>
		/// Given the current Itinerary with a customer UID and at least one Booking, temporarily reserve
		/// the required inventory for each activity, and mark each booking as (un)successfully reserved.
		/// This will be called prior to check-out to insure available inventory for each booking prior to 
		/// processing the payment.
		/// </summary>
		/// <returns>True if all bookings were successfully reserved.</returns>
		public bool ReserveBookings() {
			return ( true );
		}

		/// <summary>
		/// Given an Itinerary instance with previously-reserved Bookings and confirmed CC payment info,
		/// post the payment info to ATCO, finalize the inventory, and generate data needed to produce vouchers.
		/// </summary>
		public bool PayForItinerary() {
			Itinerary itin = Itinerary.Current;
			if ( itin.BookingsCount == 0 ) {
				// No ATCO bookings here (assume we've got at least one non-ATCO reservation though)
				return ( true );
			}
			int TransactionID = itin.VoucherTransactionID;
			// According to Kevin, we need to send the total discounted amount paid for ATCO bookings only here
			decimal PayAmount = itin.Bookings.TotalDiscountedPrice;
			string AuthCode = itin.PmtProcessor.AuthCode;
			string ServiceName = itin.PmtProcessor.ServiceName;
			// Submit the payment & booking info to ATCO and get the vouchers
			AtcoVoucherApi.PrintableVoucher[] vouchers = new AtcoVoucherApi.PrintableVoucher[itin.BookingsCount];
			int i = 0;
			foreach ( Booking b in itin.Bookings ) {
				AtcoVoucherApi.PrintableVoucher voucher = new AtcoVoucherApi.PrintableVoucher();
				voucher.ActivityAID = b.Activity.AID;
				voucher.ActivityDate = b.SelectedDateTime;
				voucher.ActivityDirections = "These are the directions";
				voucher.ActivityName = b.ActivityTitle;
				voucher.ActivityPhone = "(808) 555-1212";
				voucher.AgentName = "MC&A Hawaii";
				voucher.BookingDate = DateTime.Now;
				voucher.CheckinTime = b.SelectedDateTime.AddHours( -1 ).ToShortTimeString();
				voucher.ConfirmationNum = "66666";
				voucher.ConfirmedBy = "RKP";
				voucher.CustomerFirst = itin.CustomerFirstName;
				voucher.CustomerLast = itin.CustomerLastName;
				voucher.Hotel = b.HotelName;
				voucher.IssueDate = DateTime.Today;
				voucher.Notes = "This voucher is a mockup for testing purposes only";
				voucher.PickupLoc = "In front of hotel";
				voucher.PickupTime = voucher.CheckinTime;
				voucher.Room = b.HotelRoomNumber;
				voucher.ThirdParty = "";
				voucher.TransactionID = "123456";
				voucher.VendorName = "StarrTech Activities";
				DateTime seed = DateTime.Now;
				voucher.VoucherNumber = b.VoucherNumber = string.Format( "{0:00}{1:00}{2:00}{3:00}", seed.Hour, seed.Minute, seed.Second, i );
				vouchers[i] = b.Voucher = voucher;
				i++;
			}
			return ( true );
		}

		public void CancelBookings( Itinerary itin, string TraceMessage ) { }
		
		/// <summary>
		/// Given the current paid-for Itinerary, with a list of bookings and a list of corresponding PrintableVouchers,
		/// produce the HTML string for the vouchers. This HTML will be suitable for display in a browser or for 
		/// delivery to the customer by email based on the value of FullPage (false for email).
		/// </summary>
		/// <returns>HTML stream for the formatted vouchers</returns>
		public string BuildVouchers( bool FullPage ) {
			Itinerary itin = Itinerary.Current;
			StringBuilder sb = new StringBuilder();
			if ( itin.Bookings.Count == 0 ) {
				return ( "" );
			}
			if ( FullPage ) {
				// We're headed for display in the web page, so we need a full page header
				// Static HTML
				sb.Append( @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd"">" );
				sb.Append( @"<html><head><meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">" );
				// Page title
				sb.Append( @"<title>MC&amp;A - Activity Voucher</title>" );
				// Static HTML
				sb.Append( @"</head><center><body>" );
			} else {
				// We're generating an email voucher, so use a simple div container
				sb.Append( @"<div style=""page-break-after:always"">&nbsp;</div>" ); // trying to force a page-break on the first table below
			}
			sb.Append( @"<style type=""text/css""> <!-- body { margin-left: 10px; margin-top: 20px; margin-right: 10px; margin-bottom: 20px; } .style1 { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; } .body-text { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; } .small-text { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; line-height: 12px; } .header-text { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px; line-height: 20px; } td { vertical-align: top } --> </style>" );

			// For each booking, append the result of BuildActivityVoucherPanel( theBooking )
			foreach ( Booking b in itin.Bookings ) {
				sb.Append( BuildActivityVoucherPanel( b ) );
			}

			// Now build the Transaction Summary table
			sb.Append( @"<table width=""680"" border=""1"" cellpadding=""0"" cellspacing=""0"" bordercolor=""#666666"" bgcolor=""#FFFFFF"" style="""">" );
			sb.Append( @"<tr><td><table width=""680"" border=""0"" cellpadding=""10"" cellspacing=""0"">" );
			sb.Append( @"<tr><td width=""400"" align=""left"" valign=""top""><span class=""header-text"">TRANSACTION SUMMARY</span><br />" );
			// Control Number, Hotel, Hotel Room, Booking-Agent
			try { // ... in case the Voucher is null...
				sb.AppendFormat(
					@"<span class=""body-text""><strong>Control Number: {0}<br />Resort: {1}<br />Room: {2}<br />Booking Agent: {3} </strong></span></td>",
					itin.Bookings[0].Voucher.TransactionID,
					itin.Bookings[0].HotelName,
					itin.Bookings[0].HotelRoomNumber,
					itin.Bookings[0].Voucher.AgentName
				);
			} catch ( Exception e ) {
				ExcLogger.SendExceptionReport( "BuildVouchers", e.Message );
			}
			sb.AppendFormat(
				@"<td width=""236"" align=""left"" valign=""top"" class=""small-text"">{0}</td>",
				ConfigMgr.HostVendorAddress.Replace( "//", "<br />" )
			);
			// Static
			sb.Append( @"</tr><tr><td colspan=""2"" align=""left"" valign=""top"">" );
			sb.Append( @"<table width=""660"" border=""0"" cellspacing=""0"" cellpadding=""2""><tr>" );
			sb.Append( @"<td><table width=""100%"" border=""1"" cellpadding=""2"" cellspacing=""0"" bordercolor=""#666666"">" );
			sb.Append( @"<tr align=""center"" valign=""middle"" class=""small-text"">" );
			sb.Append( @"<td><strong>Voucher</strong></td>" );
			sb.Append( @"<td><strong>Name</strong></td>" );
			sb.Append( @"<td><strong>Pax</strong></td>" );
			sb.Append( @"<td><strong>Conf # </strong></td>" );
			sb.Append( @"<td><strong>Check In </strong></td>" );
			sb.Append( @"<td><strong>Activity</strong></td>" );
			//sb.Append( @"<td><strong>Book Date </strong></td>" );
			sb.Append( @"<td><strong>Activity Date </strong></td>" );
			if ( !Website.Current.IsNoPaymentSite ) {
				sb.Append( @"<td><strong>Gross Sale </strong></td>" );
				//sb.Append( @"<td><strong>Deposit</strong></td>" );
				//sb.Append( @"<td><strong>Tax</strong></td>" );
				sb.Append( @"<td><strong>Amt Paid </strong></td>" );
			}
			sb.Append( @"</tr>" );

			foreach ( Booking b in itin.Bookings ) {
				sb.Append( @"<tr class=""body-text"">" );
				// Voucher-Number
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0}</td>", b.VoucherNumber );
				// Customer last-name
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0}</td>", b.Voucher.CustomerLast );
				// Pax Count
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0}</td>", b.PaxCounts.TotalQty );
				// Confirmation Number
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0}</td>", b.Voucher.ConfirmationNum );
				// Check-In Time
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0}</td>", string.IsNullOrEmpty( b.Voucher.CheckinTime ) ? "&nbsp;" : b.Voucher.CheckinTime );
				// Activity Title
				sb.AppendFormat( @"<td align=""left"" valign=""top"">{0}</td>", b.Voucher.ActivityName );
				// Booking Date
				//sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:d}</td>", b.Voucher.BookingDate );
				// Activity Date
				sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:d}</td>", b.Voucher.ActivityDate );
				if ( !Website.Current.IsNoPaymentSite ) {
					// Gross Sale
					sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:C}</td>", b.TotalDiscountedPrice );
					// Deposit
					//sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:C}</td>", 0 );
					// Tax
					//sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:C}</td>", b.Tax );
					// Amount Paid
					sb.AppendFormat( @"<td align=""center"" valign=""middle"">{0:C}</td>", b.TotalDiscountedPricePlusTax );
				}
			}
			sb.Append( @"</tr></table></td></tr>" );
			if ( !Website.Current.IsNoPaymentSite ) {
				// Total Price
				sb.AppendFormat(
					@"<tr><td align=""right"" valign=""top"" class=""body-text""><strong>TOTAL: {0:C} </strong></td></tr>",
					itin.TotalDiscountedPrice
				);
				// Payment Type, Total Paid
				sb.AppendFormat(
					@"<tr><td align=""right"" valign=""top"" class=""body-text""><strong>{1}: {0:C} </strong></td></tr>",
					itin.TotalDiscountedPrice,
					itin.PmtProcessor.PaymentType
				);
			}
			sb.Append( @"</table></td></tr></table></td></tr></table>" );
			if ( FullPage ) {
				sb.Append( @"</body></center></html>" );
			}
			return ( sb.ToString() );
		}

		/// <summary>
		/// Send the vouchers to the customer by email
		/// </summary>
		public void SendVoucherEmail() {
			MailMessage mailer = new MailMessage();
			mailer.From = new MailAddress( ConfigMgr.VoucherEmailFromAddress );
			mailer.Bcc.Add( ConfigMgr.VoucherEmailBccAddresses );
			string devAddress = ConfigMgr.DevEmailAddress;
			if ( !string.IsNullOrEmpty( devAddress ) ) {
				mailer.Bcc.Add( devAddress );
			}
			mailer.To.Add( Customer.Current.EmailAddress );
			mailer.Body = BuildVoucherEmailBody();
			mailer.IsBodyHtml = true;
			mailer.Subject = string.Format( "{1}: {0}", ConfigMgr.VoucherEmailSubject, Customer.Current.FullName );
			SmtpClient client = new SmtpClient();
			try {
				client.Send( mailer );
			} catch ( Exception e ) {
				ExcLogger.SendExceptionReport( "SendVoucherEmail", e.Message );
			}
		}

		/// <summary>
		/// Enables the admin to flush the Hotels & Regions list caches
		/// </summary>
		public void FlushCache() {
			HttpContext.Current.Cache.Remove( HotelsListCacheKey );
			HttpContext.Current.Cache.Remove( RegionsListCacheKey );
		}

		public void Dispose() { }

		#endregion

		#region Private stuff

		private const string HotelsListCacheKey = "HOTELS_LIST";
		private const string RegionsListCacheKey = "REGIONS_LIST";
		private const string ActivitiesListCacheKey = "ACTIVITIES_LIST";

		#region Activities RSS Feed

		private class ActivityItem {
			public string Title;
			public string RegionAID;
			public ActivityItem( string title, string regionAID ) {
				this.Title = title;
				this.RegionAID = regionAID;
			}
		}

		/// <summary>
		/// A simple keyed listing of all known activity AIDs/titles
		/// </summary>
		private Dictionary<string, ActivityItem> Activities {
			get {
				Dictionary<string, ActivityItem> actList = HttpContext.Current.Cache[ActivitiesListCacheKey] as Dictionary<string, ActivityItem>;
				if ( actList == null ) {
					// Create the dictionary for holding the key-value list
					actList = new Dictionary<string, ActivityItem>();
					// Load the dictionary
					try {
						rss feed = RssClient.GetRss( ConfigMgr.ActivitiesListRssUrl );
						foreach ( item i in feed.channel.item ) {
							// Parse the ATCO ID and activity title from the feed's current item
							string aid = i.guid.Text;
							string title = i.title;
							string regionAID = i.category[0].regionId;
							// Add the item to the list
							actList[aid] = new ActivityItem( title, regionAID );
						}
						// Add it to the application cache, expire every 4 hours
						HttpContext.Current.Cache.Insert(
							ActivitiesListCacheKey, actList, null, DateTime.Now.AddHours( 4 ), TimeSpan.Zero, CacheItemPriority.AboveNormal, null
						);
					} catch ( Exception e ) {
						ExcLogger.SendExceptionReport( "TestDemoVoucherService.Activities", e.Message );
					}
				}
				return ( actList );
			}
		}

		#endregion

		#region Vouchers

		/// <summary>
		/// Build the HTML for the top activity-specific panel of the HTML voucher.
		/// </summary>
		/// <param name="theBooking">The booking to build the voucher-panel for.</param>
		/// <returns>HTML string for the voucher panel, part of the voucher page.</returns>
		private string BuildActivityVoucherPanel( Booking theBooking ) {
			Itinerary itin = Itinerary.Current;
			if ( theBooking.Voucher == null ) {
				// We need a better error message here...
				return ( string.Format( "<b>No voucher available for {0}</b>", theBooking.Activity.Title ) );
			}
			StringBuilder sb = new StringBuilder();
			sb.Append( @"<table width=""680"" border=""1"" cellpadding=""0"" cellspacing=""0"" bordercolor=""#666666"" bgcolor=""#FFFFFF"" style="""">" );
			sb.Append( @"<tr><td width=""680""><table width=""400"" border=""0"" cellpadding=""10"" cellspacing=""0""><tr>" );
			// Voucher logo URL
			sb.AppendFormat(
				@"<td align=""left"" valign=""top""><img src=""{0}"" width=""91"" height=""58"" align=""left""><span class=""small-text""><strong>",
				ConfigMgr.VoucherLogoUrl
			);
			// Vendor name & address block
			sb.Append( ConfigMgr.HostVendorAddress.Replace( "//", "<br />" ) );
			// Static HTML
			sb.Append( @"</strong></span></td>" );
			// 
			sb.AppendFormat(
				@"<td width=""236"" align=""left"" valign=""top""><span class=""header-text"">ACTIVITY VOUCHER<br />{0}</span><br />",
				theBooking.VoucherNumber
			);
			sb.Append( @"<span class=""body-text"">CUSTOMER COPY - Valid </span></td></tr><tr>" );
			sb.Append( @"<td align=""left"" valign=""top""><hr width=""400"" size=""1"" noshade class=""style1"">" );

			// Activity-Vendor name
			sb.AppendFormat( @"<span class=""header-text""><strong>{0}</strong><br />", theBooking.Voucher.VendorName );
			// Activity name
			sb.AppendFormat( @"{0}</span><br /><br />", theBooking.Voucher.ActivityName );
			sb.Append( @"<table width=""400"" border=""0"" cellspacing=""0"" cellpadding=""2"">" );
			// Booking date
			sb.AppendFormat(
				@"<tr><td width=""115"" class=""small-text"">ACTIVITY DATE: </td><td class=""body-text""><strong>{0:ddd, MMMM d, yyyy}</strong></td></tr><tr>",
				theBooking.Voucher.ActivityDate
			);
			// Activity Phone
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">ACTIVITY PHONE: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.ActivityPhone
			);
			// Check-in Time
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">CHECKIN TIME: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.CheckinTime
			);

			// Pickup Location
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">PICKUP LOCATION: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.PickupLoc
			);
			// Pickup Time
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">PICKUP TIME: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.PickupTime
			);
			// Notes
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">NOTES:</td><td class=""body-text"">{0}</td></tr><tr>",
				theBooking.Voucher.Notes
			);
			// Directions
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">DIRECTIONS:</td><td class=""body-text"">{0}</td></tr><tr>",
				theBooking.Voucher.ActivityDirections
			);
			sb.Append( @"<td width=""115"" class=""small-text"">&nbsp;</td><td class=""body-text"">&nbsp;</td></tr><tr>" );
			// Confirmation #
			sb.AppendFormat(
				@"<td width=""115"" height=""22"" class=""small-text"">CONFIRMATION #: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.ConfirmationNum
			);
			// Confirmed By
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">CONFIRMED BY: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.ConfirmedBy
			);
			// Static HTML
			sb.Append( @"<td width=""115"" class=""small-text"">&nbsp;</td><td class=""body-text"">&nbsp;</td></tr></table>" );
			sb.Append( @"<table width=""400"" border=""0"" cellspacing=""0"" cellpadding=""2""><tr>" );
			sb.Append( @"<td colspan=""2""><hr size=""1"" noshade class=""style1""></td><td>&nbsp;</td>" );
			sb.Append( @"<td colspan=""2""><hr size=""1"" noshade class=""style1""></td></tr><tr>" );
			// Client
			sb.AppendFormat(
				@"<td width=""70"" class=""small-text"">CLIENT:</td><td class=""header-text"">{0} {1} <span class=""small-text""><b>{2}</b></span></td><td>&nbsp;</td>",
				theBooking.Voucher.CustomerFirst, theBooking.Voucher.CustomerLast, itin.ContactPhone
			);
			// Booking Date
			sb.AppendFormat(
				@"<td class=""small-text"">BOOKING DATE: </td><td class=""body-text""><strong>{0:d}</strong></td></tr><tr>",
				theBooking.Voucher.BookingDate
			);
			// Hotel
			sb.AppendFormat(
				@"<td class=""small-text"">HOTEL: </td><td class=""body-text""><strong>{0}</strong></td><td>&nbsp;</td>",
				theBooking.Voucher.Hotel
			);
			// Issue Date
			sb.AppendFormat(
				@"<td class=""small-text"">ISSUE DATE: </td><td class=""body-text""><strong>{0:d} </strong></td></tr><tr>",
				theBooking.Voucher.IssueDate
			);
			// Room
			//sb.AppendFormat( 
			//  @"<td class=""small-text"">ROOM: </td><td class=""body-text""><strong>{0}</strong></td><td>&nbsp;</td>",
			//  theBooking.Voucher.Room
			//);
			// Agent
			sb.AppendFormat(
				@"<td class=""small-text"">AGENT: </td><td class=""body-text""><strong>{0}</strong></td></tr><tr>",
				theBooking.Voucher.AgentName
			);
			// 3rd Party
			sb.AppendFormat(
				@"<td class=""small-text"">3RD PARTY: </td><td class=""body-text""><strong>{0}</strong></td><td>&nbsp;</td>",
				theBooking.Voucher.ThirdParty
			);
			// Control Number
			sb.AppendFormat(
				@"<td width=""115"" class=""small-text"">CONTROL NUMBER: </td><td class=""body-text""><strong>{0}</strong></td>",
				theBooking.Voucher.TransactionID
			);
			// Static HTML
			sb.Append( @"</tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>" );
			sb.Append( @"<hr width=""400"" size=""1"" noshade class=""style1"">" );
			sb.Append( @"<span class=""small-text"">CANCELLATION/REFUND POLICY: There will be no refunds for cancellations made less than 72 hours prior to activity. Once the tour begins, no refund can be given. Please allow three business days from processing. THE ORIGINAL VOUCHER MUST BE RETURNED IN ORDER FOR US TO ISSUE A REFUND.</span>" );
			sb.Append( @"<hr width=""400"" size=""1"" noshade class=""style1"">" );
			sb.Append( @"<span class=""small-text""><strong>ATCO SOFTWARE INC </strong></span><br /></td>" );
			sb.Append( @"<td width=""236"" align=""left"" valign=""top""><hr width=""236"" size=""1"" noshade class=""style1"">" );
			sb.Append( @"<table width=""236"" border=""0"" cellspacing=""0"" cellpadding=""2"">" );
			sb.Append( @"<tr><td colspan=""2""><table width=""236"" border=""0"" cellspacing=""0"" cellpadding=""0"">" );

			foreach ( PaxCount pax in theBooking.PaxCounts ) {
				if ( pax.PaxQty > 0 ) {
					PriceCode pc = theBooking.SelectedScheduleDateTime.PriceCodes[pax.PriceCodeUID];
					sb.AppendFormat( @"<tr class=""small-text""><td align=""left"" valign=""top""><strong>{0} {1}</strong></td>", pax.PaxQty, pc.Label );
					sb.Append( @"<td align=""right"" valign=""top"">" );
					if ( !Website.Current.IsNoPaymentSite ) {
						sb.AppendFormat( @"<strong>@&nbsp;{0:C} = </strong>", pc.Price );
					}
					sb.Append( @"</td>" );
					sb.Append( @"<td align=""right"" valign=""top"">" );
					if ( !Website.Current.IsNoPaymentSite ) {
						sb.AppendFormat( @"<strong>{0:C}</strong>", pax.PaxQty * pc.Price );
					}
					sb.Append( @"</td></tr>" );
				}
			}
			sb.Append( @"</table></td></tr>" );
			if ( !Website.Current.IsNoPaymentSite ) {
				// Total price before any discount
				sb.AppendFormat(
					@"<tr><td class=""small-text"">TARIFF TOTAL: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0:C}</strong></td></tr>",
					theBooking.TotalBasePrice
				);
				// Discount applied to this booking
				sb.AppendFormat(
					@"<tr><td class=""small-text"">DISCOUNT: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0:C}</strong></td></tr>",
					theBooking.TotalDiscountAmount
				);
				// Not sure what this is?
				sb.AppendFormat(
					@"<tr><td align=""left"" valign=""top"" class=""small-text"">SUBTOTAL: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0:C}</strong></td></tr>",
					theBooking.TotalDiscountedPrice
				);
				// Total price net of discount
				sb.AppendFormat(
					@"<tr><td class=""small-text"">TOTAL: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0:C}</strong></td></tr>",
					theBooking.TotalDiscountedPricePlusTax
				);
				//sb.AppendFormat( 
				//	@"<tr><td align=""left"" valign=""top"" class=""small-text"">DEPOSIT: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0:C}</strong></td></tr>", 
				//	theBooking.TotalPrice 
				//);
				sb.AppendFormat(
					@"<tr><td align=""left"" valign=""top"" class=""small-text"">PAYMENT VIA: </td><td align=""right"" valign=""top"" class=""small-text""><strong>{0}</strong></td></tr>",
					itin.PmtProcessor.PaymentType
				);
				//sb.Append( @"<tr><td align=""left"" valign=""top"" class=""small-text"">PAYABLE AT CHECKIN: </td><td align=""right"" valign=""top"" class=""small-text""><strong>$0.00</strong></td></tr>" );
			}
			sb.Append( @"</table><br /></td></tr></table></td></tr></table>" );
			sb.Append( @"<div style=""page-break-after:always"">&nbsp;</div>" ); // force a page-break
			// We're done...
			return ( sb.ToString() );
		}

		/// <summary>
		/// Generate the HTML for the voucher email
		/// </summary>
		/// <returns>HTML body text for the voucher email</returns>
		private string BuildVoucherEmailBody() {
			StringBuilder body = new StringBuilder();
			body.Append( "<p><b>Thank You!</b></p>" );
			body.Append( "<br />" );
			body.Append( "<p>" );
			body.Append( "Your itinerary and payment have been successfully processed." );
			body.Append( "</p><p>" );
			body.Append( "Your activity vouchers are shown below. Please print the vouchers and be prepared to present the appropriate voucher for admission to the activities you have purchased." );
			body.Append( "</p><p>" );
			body.Append( "You will need to use the following information to log in to your account and view your itinerary at our website:" );
			body.Append( "<br />" );
			body.Append( "<br />" );
			if ( Customer.Current.ID < 0 || Customer.Current.CurrentItinerary.ID < 0 ) {
				ExcLogger.SendExceptionReport( "BuildVoucherEmailBody",
					string.Format( "Customer Email: {0}, Login: {1}", Customer.Current.EmailAddress, Customer.Current.CurrentItinerary.LoginAccountNumber )
				);
			}
			body.AppendFormat( "Email Address: <b>{0}</b>", Customer.Current.EmailAddress );
			body.Append( "<br />" );
			body.Append( "<br />" );
			body.AppendFormat( "Account Number: <b>{0}</b>", Customer.Current.CurrentItinerary.LoginAccountNumber );
			body.Append( "<br />" );
			body.Append( "</p>" );
			body.Append( "<br />" );
			body.Append( BuildVouchers( false ) );
			return ( body.ToString() );
		}

		#endregion

		#endregion

	}

}
