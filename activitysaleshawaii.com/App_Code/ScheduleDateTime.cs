using System;

namespace ABS {
	/// <summary>
	/// Represents Date/Time range for a given activity.
	/// </summary>
	public class ScheduleDateTime {
		
		#region Properties

		/// <summary>
		/// The Start Date for the activity
		/// </summary>
		public DateTime Value {
			get { return ( this.val ); }
			set { this.val = value; }
		}
		private DateTime val;

		/// <summary>
		/// The Inventory UID for this available date/time
		/// </summary>
		public string InventoryUID {
			get { return ( this.inventoryUID ); }
			set { this.inventoryUID = value; }
		}
		private string inventoryUID;

		/// <summary>
		/// Available inventory for the given date
		/// </summary>
		public int SeatsAvailable {
			get { return ( this.seatsAvailable ); }
			set { this.seatsAvailable = value; }
		}
		private int seatsAvailable;

		/// <summary>
		/// The collection of PriceCodes for this activity
		/// </summary>
		public PriceCodesList PriceCodes {
			get {
				if ( this.priceCodes == null ) {
					this.priceCodes = new PriceCodesList();
				}
				return ( this.priceCodes );
			}
		}
		private PriceCodesList priceCodes;

		#endregion
		
		#region Constructors
		
		// We don't want any un-initialized instances
		private ScheduleDateTime() {}

		public ScheduleDateTime( DateTime Value, string invUid, int seats ) {
			this.val = Value;
			this.InventoryUID = invUid;
			this.SeatsAvailable = seats;
		}
		
		#endregion
		
		#region Methods
		
		
		
		#endregion
		
	}
	
}
