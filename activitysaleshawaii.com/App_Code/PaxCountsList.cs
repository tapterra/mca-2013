using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of PaxCounts in a Booking
	/// </summary>
	public class PaxCountsList : KeyedCollection<string, PaxCount> {

		public Booking Owner {
			get { return ( this.owner ); }
		}
		private Booking owner = null;
		
		private PaxCountsList() {}
		
		public PaxCountsList( Booking owner ) : base() {
			this.owner = owner;
			this.LoadItems();
		}
		
		public int CountWithQty {
			get {
				int result = 0;
				foreach ( PaxCount item in this ) {
					result += ( item.PaxQty > 0 ? 1 : 0 );
				}
				return ( result );
			}
		}

		public int TotalQty {
			get {
				int result = 0;
				foreach ( PaxCount item in this ) {
					result += item.PaxQty;
				}
				return ( result );
			}
		}

		protected override string GetKeyForItem( PaxCount item ) {
			return ( item.PriceCodeUID );
		}

		private void LoadItems() {
			if ( this.owner == null || this.owner.ID <= 0 ) {
				return;
			}
			string sql = "select * from PAX_COUNTS where ( booking_id = @booking_id and is_deleted = 0 ) order by id";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@booking_id", this.owner.ID ) ) ) {
					PaxCount item = new PaxCount( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new PaxCount( this.owner );
					}
				}
			}
		}

		protected internal void Persist() {
			foreach ( PaxCount item in this ) {
				item.BookingID = this.Owner.ID;
				item.Persist();
			}
		}

	}
	
}
