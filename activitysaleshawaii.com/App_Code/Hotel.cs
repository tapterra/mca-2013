using System;
using System.Collections.Generic;

namespace ABS {
	/// <summary>
	/// Represents one of the hotels known by the system.
	/// </summary>
	public class Hotel {

		#region Properties

		/// <summary>
		/// The ATCO Identification code for this Hotel
		/// </summary>
		public string AID {
			get { return ( this.aid ); }
		}
		private string aid;

		/// <summary>
		/// The hotel's name
		/// </summary>
		public string Name {
			get { return ( this.name ); }
		}
		private string name;

		// RKP: Begin change - 20080813
		// RKP: Change the single-value RegionAID property into a list property, so that a given Hotel
		// RKP: can be associated with potentially more than one region. 
		
		///// <summary>
		///// The AID for the region (island) where the hotel is located
		///// </summary>
		//public string RegionAID {
		//  get { return ( this.regionAID ); }
		//}
		//private string regionAID;

		/// <summary>
		/// The list of AIDs for the regions (island and optional custom lists) where the hotel is located
		/// </summary>
		public List<string> RegionAIDs {
			get { return ( this._RegionAIDs ); }
		}
		private List<string> _RegionAIDs = new List<string>();
		// RKP: End change

		#endregion

		#region Constructors
		// We don't want any un-initialized instances
		private Hotel() { }

		// RKP: Begin change - 20080813
		// RKP: Since we're no longer limited to a single RegionAID for a given hotel, 
		// RKP: drop the RegionAID from the constructor.

		//public Hotel( string AID, string Name, string RegionAID ) {
		//  this.aid = AID;
		//  this.name = Name;
		//  this.regionAID = RegionAID;
		//}

		public Hotel( string AID, string Name ) {
			this.aid = AID;
			this.name = Name;
		}
		// RKP: End change

		#endregion

	}
}
