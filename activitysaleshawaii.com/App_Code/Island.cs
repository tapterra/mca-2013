using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Collections.ObjectModel;

namespace ABS {

	[SqlTable( "ISLANDS", SingularLabel = "Island", PluralLabel = "Islands", IsMarkedDeleted = true )]
	public class Island : BaseSqlPersistable {

		#region Properties

		#region Database Columns
		//[island_aid] [nvarchar](30) NOT NULL,
		[SqlColumn( "island_aid", DbType.String, Length = 30 )]
		public string AID {
			get { return ( this.aid ); }
			set { this.aid = value; }
		}
		private string aid;

		//[name] [nvarchar](50) NOT NULL,
		[SqlColumn( "name", DbType.String, Length = 50 )]
		public string Name {
			get { return ( this.name ); }
			set { this.name = value; }
		}
		private string name;

		//[seq] [int] NOT NULL,
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//[key_name] [nvarchar](20) NOT NULL,
		[SqlColumn( "key_name", DbType.String, Length = 20 )]
		public string KeyName {
			get { return ( this.keyName ); }
			set { this.keyName = value; }
		}
		private string keyName;

		//[comments] [ntext] 
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		#endregion

		#region Constructors

		// Access the base constructors
		public Island() : base() { }
		public Island( int ID ) : base( ID ) { }

		public Island( string key ) : base() {
			string sql = "select * from ISLANDS where ( key_name = @key_name ) order by seq";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@key_name", key ) );
			}
		}

		#endregion

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public static int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_islands";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public static int HighestSeq {
			get { return ( Island.ResetItemRanks() - 1 ); }
		}

		public void MoveUp() {
			Island.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( this.Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ISLANDS set seq = -1 where id = @id; ";
			// Move the item above me down to my spot
			sql += "update ISLANDS set seq = @seq where seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update ISLANDS set seq = @seq - 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		public void MoveDown() {
			// We can't demote if we're already at the end
			if ( this.Seq >= Island.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update ISLANDS set seq = -1 where id = @id; ";
			// Move the item below me up to my spot
			sql += "update ISLANDS set seq = @seq where seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update ISLANDS set seq = @seq + 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		#region Static Search Members

		public static void NewSearch() { }

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			string sql = @"select * from	ISLANDS ";
			if ( string.IsNullOrEmpty( Island.SortExpression ) ) {
				Island.SortExpression = "seq";
			}
			sql += string.Format( "where ( is_deleted = 0 ) order by {0}", Island.SortExpression );
			return ( sql );
		}

		public static List<Island> DoSearchList() {
			return ( SqlDatabase.DoSearch<Island>( Island.GetSearchSql() ) );
		}

		#endregion

	}

	public class Islands : KeyedCollection<int, Island> {
		
		public static string CacheKey = "ISLANDS";

		public static Islands Current {
			get {
				Islands current = HttpContext.Current.Cache[CacheKey] as Islands;
				if ( current == null ) {
					current = new Islands();
					HttpContext.Current.Cache.Insert( CacheKey, current, null, DateTime.Now.AddHours( 1 ), Cache.NoSlidingExpiration );
				}
				return ( current );
			}
		}

		private Islands() : base() {
			this.LoadItems();
		}

		protected override int GetKeyForItem( Island item ) {
			return ( item.ID );
		}

		private void LoadItems() {
			string sql = "select * from ISLANDS where ( is_deleted = 0 ) order by seq";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Island island = new Island();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, island ) ) {
						this.Add( island );
						island = new Island();
					}
				}
			}
		}

	}

}
