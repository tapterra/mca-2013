using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace ABS {

	/// <summary>
	/// Represents a guest's response for an activity options
	/// </summary>
	[SqlTable( "OPTION_RESPONSES", SingularLabel = "Option Response", PluralLabel = "Option Responses", IsMarkedDeleted = true )]
	public class OptionResponse : BaseSqlPersistable {
		
		#region Properties

		[SqlColumn( "guest_id", DbType.Int32 )]
		public int GuestID {
			get { return ( this.guestID ); }
			set { this.guestID = value; }
		}
		private int guestID;

		[SqlColumn( "option_id", DbType.Int32 )]
		public int OptionID {
			get { return ( this.optionID ); }
			set { this.optionID = value; }
		}
		private int optionID;

		[SqlColumn( "response", DbType.String, Length = 200 )]
		public string Response {
			get { return ( this.response ); }
			set { this.response = value; }
		}
		private string response;

		[SqlColumn( "question_text", DbType.String, Length = 250 )]
		public string QuestionText {
			get { return ( this.questionText ); }
			set { this.questionText = value; }
		}
		private string questionText;

		public string ControlID {
			get { return ( string.Format( "opt{0}", this.OptionID ) ); }
		}

		#endregion
		
		#region Constructors
		
		// Access the base constructors
		private OptionResponse() : base() { }
		public OptionResponse( int ID ) : base( ID ) { }

		public OptionResponse( int guestID, int optionID, string question, string response ) {
			this.GuestID = guestID;
			this.OptionID = optionID;
			this.QuestionText = question;
			this.Response = response;
		}

		protected internal OptionResponse( Guest guest ) : base() {
			//this.Owner = guest;
			this.GuestID = guest.ID;
		}

		#endregion

		public override string ToString() {
			return ( string.Format( "{0}: {1}", this.QuestionText, this.Response ) );
		}

	}
}
