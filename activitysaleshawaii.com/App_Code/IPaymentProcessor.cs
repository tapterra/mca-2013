﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	/// <summary>
	/// Status of a payment submitted for processing
	/// </summary>
	public enum PaymentProcessorResponses {
		NotSubmitted,								// General Failure - Transaction not successfully submitted
		NotCommitted,								// General Failure - Transaction not successfully committed
		ApprovedResponse,						// Transaction Approved
		DeclinedResponse,						// Transaction Declined
		ReferralRequestedResponse,	// Referral Requested
		AVSFailureResponse,					// Address Verification Failure
		CVV2FailureResponse,				// CVV2 Verification Failure
		CommErrorResponse,					// Transaction Failed - no transaction
		UnknownResponse							// Transaction Declined
	}

	public interface IPaymentProcessor {

		#region Properties

		/// <summary>
		/// Name of the processing service (to report to the voucher service).
		/// </summary>
		string ServiceName { get; }

		/// <summary>
		/// The customer's name on the credit card
		/// </summary>
		string CardHolderName { get; set; }

		/// <summary>
		/// The type of credit card (visa, mastercard, etc...)
		/// </summary>
		string CardType { get; set; }

		/// <summary>
		/// The credit card number
		/// </summary>
		string CardNumber { get; set; }

		/// <summary>
		/// The month the credit card expires on
		/// </summary>
		int CardExpiresMM { get; set; }

		/// <summary>
		/// The year the credit card expires on
		/// </summary>
		int CardExpiresYY { get; set; }

		/// <summary>
		/// The CVV number (usually last 3 digit number) on the back of the credit card
		/// </summary>
		string CardCVVNumber { get; set; }

		/// <summary>
		/// Line 1 of the billing street address for the customer (usually same as for the credit card)
		/// </summary>
		string BillingStreet1 { get; set; }

		/// <summary>
		/// Line 2 of the billing street address for the customer (usually same as for the credit card)
		/// </summary>
		string BillingStreet2 { get; set; }

		/// <summary>
		/// The billing city for the customer (usually same as for the credit card)
		/// </summary>
		string BillingCity { get; set; }

		/// <summary>
		/// The billing state for the customer (usually same as for the credit card)
		/// </summary>
		string BillingState { get; set; }

		/// <summary>
		/// The billing province for the customer (usually same as for the credit card)
		/// </summary>
		string BillingProvince { get; set; }

		/// <summary>
		/// The billing zipcode for the customer (usually same as for the credit card)
		/// </summary>
		string BillingZip { get; set; }

		/// <summary>
		/// The billing country for the customer (usually same as for the credit card)
		/// </summary>
		string BillingCountry { get; set; }

		/// <summary>
		/// The total amount of payment being charged
		/// </summary>
		decimal PaymentAmount { get; set; }

		/// <summary>
		/// The authorization code returned by the service for this payment
		/// </summary>
		string AuthCode { get; set; }

		/// <summary>
		/// The transaction ID assigned by the payment processing service when the payment is submitted.
		/// </summary>
		string TransactionID { get; set; }

		/// <summary>
		/// The Response Code returned by the payment processing service when the payment is submitted.
		/// </summary>
		string ResponseCode { get; set; }

		/// <summary>
		/// The Response Message returned by the payment processing service when the payment is submitted.
		/// </summary>
		string ResponseMessage { get; set; }

		/// <summary>
		/// String used for the PAYMENT VIA field on the vouchers
		/// </summary>
		string PaymentType { get; }

		#endregion

		#region Methods

		PaymentProcessorResponses DoAuthorizeTransaction();

		PaymentProcessorResponses DoCaptureTransaction();

		// Not currently being used
		PaymentProcessorResponses DoSaleTransaction();

		#endregion

	}

}
