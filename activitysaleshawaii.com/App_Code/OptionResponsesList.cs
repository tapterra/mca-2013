using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of OptionResponses in a Guest
	/// </summary>
	public class OptionResponsesList : List<OptionResponse> {

		public Guest Owner {
			get { return ( this.owner ); }
		}
		private Guest owner = null;

		public string ResponsesString {
			get {
				string result = "";
				foreach ( OptionResponse item in this ) {
					result += ( result == "" ? "" : ", " ) + item.ToString();
				}
				return ( result );
			}
		}

		private OptionResponsesList() {}
		
		public OptionResponsesList( Guest owner ) : base() {
			this.owner = owner;
			this.LoadItems();
		}

		private void LoadItems() {
			if ( this.owner == null || this.owner.ID <= 0 ) {
				return;
			}
			string sql = "select * from OPTION_RESPONSES where ( guest_id = @guest_id ) order by id";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@guest_id", this.owner.ID ) ) ) {
					OptionResponse item = new OptionResponse( this.owner );
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						this.Add( item );
						item = new OptionResponse( this.owner );
					}
				}
			}
		}

		public OptionResponse GetItemByOptionID( int orID ) {
			foreach ( OptionResponse item in this ) {
				if ( item.OptionID == orID ) {
					return ( item );
				}
			}
			return ( null );
		}

		protected internal void Persist() {
			foreach ( OptionResponse item in this ) {
				item.GuestID = this.Owner.ID;
				item.Persist();
			}
		}

	}

}
