using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	/// <summary>
	/// Represents one of the tourism activities (events) to be booked by the system
	/// </summary>
	[SqlTable( "ACTIVITY_GROUPS", SingularLabel = "Activity Group", PluralLabel = "Activity Groups", IsMarkedDeleted = true )]
	public class ActivityGroup : BaseSqlPersistable {

		#region Properties

		#region Database Columns

		//[website_id] [int] NULL,
		[SqlColumn( "website_id", DbType.Int32 )]
		public int WebsiteID {
			get { return ( this.websiteID ); }
			set { this.websiteID = value; }
		}
		private int websiteID = -1;

		//[created_dt] [datetime] NULL,
		[SqlColumn( "created_dt", DbType.DateTime )]
		public DateTime CreatedDateTime {
			get { return ( this.createdDateTime ); }
			set { this.createdDateTime = value; }
		}
		private DateTime createdDateTime = DateTime.Now;

		//[is_approved] [bit] NOT NULL,
		[SqlColumn( "is_approved", DbType.Boolean )]
		public bool IsApproved {
			get { return ( this.isApproved ); }
			set { this.isApproved = value; }
		}
		private bool isApproved = false;

		//[island_id] [int] NOT NULL,
		[SqlColumn( "island_id", DbType.Int32 )]
		public int IslandID {
			get { return ( this.islandID ); }
			set { this.islandID = value; }
		}
		private int islandID = -1;

		//[category_id] [int] NOT NULL,
		[SqlColumn( "category_id", DbType.Int32 )]
		public int CategoryID {
			get { return ( this.categoryID ); }
			set {
				this.categoryID = value;
				this.subcategoryID = -1;
			}
		}
		private int categoryID = -1;

		//[subcategory_id] [int] NOT NULL,
		[SqlColumn( "subcategory_id", DbType.Int32 )]
		public int SubcategoryID {
			get { return ( this.subcategoryID ); }
			set {
				this.subcategoryID = value;
				this.subcategory = null;
			}
		}
		private int subcategoryID = -1;

		//[name] [nvarchar](250) NOT NULL,
		[SqlColumn( "name", DbType.String, Length = 250 )]
		public string Title {
			get { return ( this.name ); }
			set { this.name = value; }
		}
		private string name = "";

		//[vendor_name] [nvarchar](250) NOT NULL,
		[SqlColumn( "vendor_name", DbType.String, Length = 250 )]
		public string VendorName {
			get { return ( this.vendorName ); }
			set { this.vendorName = value; }
		}
		private string vendorName = "";

		//[brief_description] [ntext] NOT NULL,
		[SqlColumn( "brief_description", DbType.String )]
		public string BriefDescription {
			get { return ( this.briefDescription ); }
			set { this.briefDescription = value; }
		}
		private string briefDescription = "";

		//[top_description] [ntext] NOT NULL,
		[SqlColumn( "top_description", DbType.String )]
		public string TopDescription {
			get { return ( this.topDescription ); }
			set { this.topDescription = value; }
		}
		private string topDescription = "";

		//[bottom_description] [ntext] NULL,
		[SqlColumn( "bottom_description", DbType.String )]
		public string BottomDescription {
			get { return ( this.bottomDescription ); }
			set { this.bottomDescription = value; }
		}
		private string bottomDescription = "";

		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments = "";

		//[reservation_price] [decimal] NOT NULL,
		[SqlColumn( "reservation_price", DbType.Currency )]
		public decimal ReservationPrice {
			get { return ( this.reservationPrice ); }
			set { this.reservationPrice = value; }
		}
		private decimal reservationPrice = 0M;

		#endregion

		#region Object collections

		public ActivitiesList Activities {
			get {
				if ( this.activities == null ) {
					this.activities = new ActivitiesList( this );
				}
				return ( this.activities );
			}
		}
		private ActivitiesList activities;

		public OptionsList Options {
			get {
				if ( this.options == null ) {
					this.options = new OptionsList( this );
				}
				return ( this.options );
			}
		}
		private OptionsList options;

		public PhotosList Photos {
			get {
				if ( this.photos == null ) {
					this.photos = new PhotosList( this );
				}
				return ( this.photos );
			}
		}
		private PhotosList photos;

		#endregion

		#region Exposed related properties

		/// <summary>
		/// Returns the URL to the thumbnail for a specific photo for this group, sized to a specified width
		/// </summary>
		/// <param name="index">Index to the desired photo</param>
		/// <param name="width">Thumbnail width</param>
		/// <returns>URL string</returns>
		public string SizedThumbnailUrl( int index, int width ) {
			string result = "";
			if ( this.Photos.Count > 0 ) {
				index = index < this.Photos.Count ? index : this.Photos.Count - 1;
				ActivityPhoto photo = this.Photos[index];
				result = photo.PhotoImage.ThumbUrl( width );
			}
			return ( result );
		}

		/// <summary>
		/// URL to the first photo in the list, for use on the catalog pages
		/// </summary>
		public string ThumbnailUrl {
			get {
				string result = "";
				if ( this.Photos.Count > 0 ) {
					ActivityPhoto photo = this.Photos[0];
					result = photo.PhotoImage.ThumbnailUrl;
				}
				return ( result );
			}
		}

		/// <summary>
		/// Used by the default publisher 
		/// </summary>
		public string ThumbUrl {
			get {
				return ( this.SizedThumbnailUrl( 0, 158 ) );
			}
		}

		public Island Island {
			get {
				if ( Islands.Current.Contains( this.IslandID ) ) {
					return ( Islands.Current[this.IslandID] );
				}
				return ( null );
			}
		}
		public string IslandName {
			get { return ( this.Island.Name ); }
		}

		public Website Website {
			get {
				if ( Websites.Current.ContainsKey( this.WebsiteID ) ) {
					return ( Websites.Current[this.WebsiteID] );
				}
				return ( null );
			}
		}
		public string WebsiteName {
			get { return ( this.Website == null ? "" : this.Website.Name ); }
		}

		public Subcategory Subcategory {
			get {
				if ( this.subcategory == null && this.SubcategoryID > 0 ) {
					this.subcategory = new Subcategory( this.SubcategoryID );
				}
				return ( this.subcategory );
			}
		}
		private Subcategory subcategory;

		public Category Category {
			get {
				if ( CategoriesList.Current.Contains( this.CategoryID ) ) {
					return ( CategoriesList.Current[this.CategoryID] );
				}
				return ( null );
			}
		}
		
		public string CatLabel {
			get {
				string catLabel = this.Category == null ? "" : this.Category.Label;
				string subLabel = this.Subcategory == null ? "" : ": " + this.Subcategory.Label;
				return ( string.Format( "{0}{1}", catLabel, subLabel ) ); 
			}
		}

		#endregion

		#endregion

		#region Constructors

		// Access the base constructors
		public ActivityGroup() : base() { }
		public ActivityGroup( int ID ) : base( ID ) { }

		#endregion

		#region Instance methods

		/// <summary>
		/// Copies the current instance (and all subobjects) for use with a new Website
		/// </summary>
		/// <param name="NewWebsiteID">ID of the new Website to copy this instance to</param>
		/// <returns>Deep clone of this instance, with all IDs set to -1, and new WebsiteID assigned.</returns>
		public ActivityGroup DeepCopy( int NewWebsiteID ) {
			// Create a shallow copy of this instance...
			ActivityGroup newGroup = (ActivityGroup) this.MemberwiseClone();
			newGroup.WebsiteID = NewWebsiteID;
			newGroup.ID = -1;
			newGroup.createdDateTime = DateTime.Now;
			newGroup.IsApproved = false;
			newGroup.Persist();
			// Copy the group's Activities...
			newGroup.Activities.Clear();
			foreach ( Activity oldAct in this.Activities ) {
				Activity newAct = oldAct.DeepCopy();
				newAct.ActivityGroupID = newGroup.ID;
				newAct.Persist();
				newGroup.Activities.Add( newAct );
			}
			// Copy the group's Photos...
			newGroup.Photos.Clear();
			foreach ( ActivityPhoto oldPhoto in this.Photos ) {
				ActivityPhoto newPhoto = oldPhoto.DeepCopy();
				newPhoto.ActivityGroupID = newGroup.ID;
				newPhoto.Persist();
				newGroup.Photos.Add( newPhoto );
			}
			// Copy the group's Options...
			newGroup.Options.Clear();
			foreach ( Option oldOption in this.Options ) {
				Option newOption = oldOption.DeepCopy();
				newOption.ActivityGroupID = newGroup.ID;
				newOption.Persist();
				newGroup.Options.Add( newOption );
			}
			return ( newGroup );
		}

		#endregion

		#region Static Publishing Search Methods

		public static List<ActivityGroup> GetActivityGroups( Category category, int IslandID ) {
			string sql = "select * from ACTIVITY_GROUPS where island_id = @island_id and category_id = @category_id and is_deleted = 0 and is_approved = 1 ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@island_id", IslandID ), db.NewParameter( "@category_id", category.ID ) ) ) {
					List<ActivityGroup> results = new List<ActivityGroup>();
					ActivityGroup item = new ActivityGroup();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						results.Add( item );
						item = new ActivityGroup();
					}
					return ( results );
				}
			}

		}

		public static List<ActivityGroup> GetActivityGroups( int IslandID, int SubcategoryID ) {
			string sql = "select * from ACTIVITY_GROUPS where island_id = @island_id and subcategory_id = @subcategory_id and is_deleted = 0 and is_approved = 1 ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql,
					db.NewParameter( "@island_id", IslandID ), db.NewParameter( "@subcategory_id", SubcategoryID ) 
				) ) {
					List<ActivityGroup> results = new List<ActivityGroup>();
					ActivityGroup item = new ActivityGroup();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, item ) ) {
						results.Add( item );
						item = new ActivityGroup();
					}
					return ( results );
				}
			}

		}

		#endregion

		#region Static Admin Search Members

		#region Properties

		public static new string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static new string sortExpression = "name asc";

		public static int SWebsiteID {
			get { return ( sWebsiteID ); }
			set { sWebsiteID = value; }
		}
		private static int sWebsiteID = -1;

		public static int SIslandID {
			get { return ( sIslandID ); }
			set { sIslandID = value; }
		}
		private static int sIslandID = -1;

		public static int SCategoryID {
			get { return ( sCategoryID ); }
			set { sCategoryID = value; }
		}
		private static int sCategoryID = -1;

		public static int SSubcategoryID {
			get { return ( sSubcategoryID ); }
			set { sSubcategoryID = value; }
		}
		private static int sSubcategoryID = -1;

		public static string STitle {
			get { return ( sTitle ); }
			set { sTitle = value; }
		}
		private static string sTitle = "";

		public static string SVendorName {
			get { return ( sVendorName ); }
			set { sVendorName = value; }
		}
		private static string sVendorName = "";

		public static int SIsApproved {
			get { return ( sIsApproved ); }
			set { sIsApproved = value; }
		}
		private static int sIsApproved = 0;
		// -1 == find unapproved
		//  1 == find approved
		//  0 == ignore

		#endregion

		#region Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			ActivityGroup.SWebsiteID = -1;
			ActivityGroup.SIslandID = -1;
			ActivityGroup.SCategoryID = -1;
			ActivityGroup.SSubcategoryID = -1;
			ActivityGroup.STitle = "";
			ActivityGroup.SVendorName = "";
			ActivityGroup.sIsApproved = 0;
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "name", ActivityGroup.STitle, ref delim );
			AddSearchExpr( whereBuilder, "vendor_name", ActivityGroup.SVendorName, ref delim );
			AddSearchExpr( whereBuilder, "website_id", ActivityGroup.SWebsiteID, ref delim );
			AddSearchExpr( whereBuilder, "island_id", ActivityGroup.SIslandID, ref delim );
			AddSearchExpr( whereBuilder, "category_id", ActivityGroup.SCategoryID, ref delim );
			AddSearchExpr( whereBuilder, "subcategory_id", ActivityGroup.SSubcategoryID, ref delim );
			switch ( ActivityGroup.SIsApproved ) {
				case -1: // find unapproved
					AddSearchExpr( whereBuilder, "is_approved", false, ref delim );
					break;
				case 1: // find approved
					AddSearchExpr( whereBuilder, "is_approved", true, ref delim );
					break;
			}
			AddSearchExpr( whereBuilder, "is_deleted", false, ref delim );

			string sql = @"select * from	ACTIVITY_GROUPS ";
			if ( whereBuilder.Length != 0 ) {
				sql += string.Format( " WHERE ( {0} ) ", whereBuilder );
			}
			if ( string.IsNullOrEmpty( ActivityGroup.SortExpression ) ) {
				ActivityGroup.SortExpression = "name";
			}
			sql += string.Format( " order by {0}", ActivityGroup.SortExpression );
			return ( sql );
		}

		public static List<ActivityGroup> DoSearchList() {
			return ( SqlDatabase.DoSearch<ActivityGroup>( ActivityGroup.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
