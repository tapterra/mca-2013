using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Web;
using StarrTech.WebTools.Data;
using System.Data;
using System.Web.Caching;

namespace ABS {

	public class CategoriesList : KeyedCollection<int, Category> {

		public static string CacheKey = "CATEGORIES";

		public static CategoriesList Current {
			get {
				CategoriesList current = HttpContext.Current.Cache[CacheKey] as CategoriesList;
				if ( current == null ) {
					current = new CategoriesList();
					HttpContext.Current.Cache.Insert( CacheKey, current, null, DateTime.Now.AddHours( 1 ), Cache.NoSlidingExpiration );
				}
				return ( current );
			}
		}

		private CategoriesList() : base() {
			this.LoadItems();
		}

		protected override int GetKeyForItem( Category item ) {
			return ( item.ID );
		}

		public Category GetItemByID( int id ) {
			foreach ( Category item in this ) {
				if ( item.ID == id ) {
					return ( item );
				}
			}
			return ( null );
		}

		private void LoadItems() {
			string sql = "select * from CATEGORIES order by seq";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Category category = new Category();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, category ) ) {
						this.Add( category );
						category = new Category();
					}
				}
			}
		}

	}

}
