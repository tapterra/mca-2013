using System;
using System.Collections.ObjectModel;

namespace ABS {
	/// <summary>
	/// Custom collection class for handling the list of PriceCodes in an Activity
	/// </summary>
	public class PriceCodesList : KeyedCollection<string, PriceCode> {

		#region Constructors
		
		public PriceCodesList() : base() {}
		
		#endregion

		protected override string GetKeyForItem( PriceCode item ) {
			return ( item.UID );
		}

	}
	
}
