using System;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {
	/// <summary>
	/// This is the guest information for activity booking
	/// </summary>
	[SqlTable( "GUESTS", SingularLabel = "Guest", PluralLabel = "Guests", IsMarkedDeleted = true )]
	public class Guest : BaseSqlPersistable {
		
		#region Properties

		#region Database Columns

		//[booking_id] [int] NOT NULL,
		[SqlColumn( "booking_id", DbType.Int32 )]
		public int BookingID {
			get { return ( this.bookingID ); }
			set { this.bookingID = value; }
		}
		private int bookingID = -1;

		//[pricecode_uid] [nvarchar](30) NOT NULL,
		[SqlColumn( "pricecode_uid", DbType.String, Length = 30 )]
		public string PriceCodeUID {
			get { return ( this.priceCodeUID ); }
			set { this.priceCodeUID = value; }
		}
		private string priceCodeUID;

		//[seq] [int] NOT NULL,
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//[first_name] [nvarchar](50) NOT NULL,
		[SqlColumn( "first_name", DbType.String, Length = 50 )]
		public string FirstName {
			get { return ( this.firstName ); }
			set { this.firstName = value; }
		}
		private string firstName;

		//[last_name] [nvarchar](100) NOT NULL,
		[SqlColumn( "last_name", DbType.String, Length = 100 )]
		public string LastName {
			get { return ( this.lastName ); }
			set { this.lastName = value; }
		}
		private string lastName;

		#endregion

		#region UI display properties
		
		public string GuestNumberDisplay {
			get { return ( string.Format( "Guest&nbsp;{0}", this.Owner.Guests.IndexOf( this ) + 1 ) ); }
		}

		public string GuestNameDisplay {
			get { return ( string.Format( "{0}&nbsp;{1}", this.FirstName, this.LastName ) ); }
		}

		public string GuestTypeDisplay {
			get { return ( this.Owner.PaxCounts[this.PriceCodeUID].PriceCodeLabel ); }
		}

		public string PriceDisplay {
			get {
				Website site = this.Owner.Itinerary.Website;
				if ( site.IsNoPaymentSite ) {
					return ( "&nbsp;" );
				} else {
					return ( string.Format( "US$&nbsp;{0:#,##0.00}", this.BasePrice ) );
				}
			}
		}
		
		public string OptionsDisplay {
			get { return ( this.OptionResponses.ResponsesString ); }
		}

		public bool OptionsDisplayVisible {
			get { return ( this.OptionResponses.ResponsesString.Trim().Length > 0 ); }
		}

		#endregion

		public Booking Owner {
			get {
				if ( this.owner == null ) {
					this.owner = new Booking( this.bookingID );
				}
				return ( this.owner ); 
			}
			set { 
				this.owner = value;
				this.bookingID = value.ID;
			}
		}
		private Booking owner; 

		/// <summary>
		/// The guest's first & last name
		/// </summary>
		public string FullName {
			get { return ( string.Format( "{0} {1}", this.firstName, this.lastName ) ); }
		}

		/// <summary>
		/// The collection of Option Responses for this guest
		/// </summary>
		public OptionResponsesList OptionResponses {
			get { 
				if ( this.optionResponses == null ) {
					this.optionResponses = new OptionResponsesList( this );
				}
				return ( this.optionResponses ); 
			}
		}
		private OptionResponsesList optionResponses;

		/// <summary>
		/// The base (pre-tax) price to charge for this guest
		/// </summary>
		public decimal BasePrice {
			get {
				// Find the PriceCode instance matching this guest's PriceCodeUID
				// TODO: Change this to look in the Booking's PaxCount collection
				//PriceCode pc = this.Owner.SelectedScheduleDateTime.PriceCodes[this.PriceCodeUID];
				decimal price = this.Owner.PaxCounts[this.PriceCodeUID].PriceCodePrice;
				// Return the PriceCode's ActualPrice
				return ( price );
			}
		}

		#endregion

		#region Constructors
		
		// Access the base constructors
		private Guest() : base() { }
		public Guest( int ID ) : base( ID ) { }

		protected internal Guest( Booking booking ) : base() {
			this.Owner = booking;
			this.bookingID = booking.ID;
		}

		public Guest( Booking booking, string priceCodeUid, int seq ) : base() {
			this.Owner = booking;
			this.bookingID = booking.ID;
			this.PriceCodeUID = priceCodeUid;
			this.Seq = seq;
		}

		#endregion

		public override bool Persist() {
			bool result = base.Persist();
			if ( result ) {
				this.OptionResponses.Persist();
			}
			return ( result );
		}

	}
}
