using System;
using System.Collections.Generic;
using System.Text;
using StarrTech.WebTools.Data;
using System.Data;

namespace ABS {

	[SqlTable( "ACTIVITY_PHOTOS", SingularLabel = "Photo", PluralLabel = "Photos", IsMarkedDeleted = true )]
	public class ActivityPhoto : BaseSqlPersistable {

		#region Properties

		#region Database Columns

		//[activity_group_id] [int] 
		[SqlColumn( "activity_group_id", DbType.Int32 )]
		public int ActivityGroupID {
			get { return ( this.activityGroupID ); }
			set { this.activityGroupID = value; }
		}
		private int activityGroupID = -1;

		//[seq] [int] 
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//[image_document_id] [int]
		[SqlColumn( "image_document_id", DbType.Int32 )]
		public int ImageDocumentID {
			get { return ( this.imageDocumentID ); }
			set { this.imageDocumentID = value; }
		}
		private int imageDocumentID = -1;

		//[comments] [ntext] 
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		#endregion

		#region Exposed ImageDocument properties

		public ImageDocument PhotoImage {
			get {
				if ( this.photoImage == null && this.ImageDocumentID > 0 ) {
					this.photoImage = new ImageDocument( this.ImageDocumentID );
				}
				return ( this.photoImage );
			}
			set {
				this.photoImage = value;
				if ( value != null ) {
					if ( this.photoImage.ID < 0 ) {
						this.photoImage.Persist();
					}
					this.imageDocumentID = this.photoImage.ID;
				}
			}
		}
		private ImageDocument photoImage;

		public string PhotoFileName {
			get { return ( this.PhotoImage == null ? "" : this.PhotoImage.FileName ); }
		}

		public string PhotoMimeType {
			get { return ( this.PhotoImage == null ? "" : this.PhotoImage.MimeType ); }
		}

		public int PhotoDataSize {
			get { return ( this.PhotoImage == null ? 0 : this.PhotoImage.DataSize ); }
		}

		public string PhotoDataSizeStr {
			get { return ( this.PhotoImage == null ? "" : string.Format( "{0:N0}K", this.PhotoImage.DataSize / 1024 ) ); }
		}

		public DateTime PhotoPostedDT {
			get { return ( this.PhotoImage == null ? DateTime.MinValue : this.PhotoImage.PostedDT ); }
		}

		public string PhotoPostedDTStr {
			get { return ( this.PhotoImage == null ? "" : this.PhotoImage.PostedDT.ToString( "d" ) ); }
		}

		public string PhotoThumbnailUrl {
			get { return ( this.PhotoImage == null ? "" : this.PhotoImage.ThumbnailUrl ); }
		}

		public string ThumbUrl {
			get { return ( this.PhotoImage == null ? "" : this.PhotoImage.ThumbUrl( 176 ) ); }
		}

		#endregion

		#endregion

		#region Constructors

		// Access the base constructors
		public ActivityPhoto() : base() { }
		public ActivityPhoto( int ID ) : base( ID ) { }

		#endregion

		#region Methods

		public override bool Persist() {
			if ( this.PhotoImage != null ) {
				this.PhotoImage.Persist();
				this.ImageDocumentID = this.PhotoImage.ID;
			}
			return base.Persist();
		}

		/// <summary>
		/// Produces a deep copy of this instance
		/// Copy will refer to the same ImageDocument instance as the original, until updated
		/// </summary>
		protected internal ActivityPhoto DeepCopy() {
			// Make a shallow copy of this instance
			ActivityPhoto newPhoto = (ActivityPhoto) this.MemberwiseClone();
			newPhoto.ID = -1;
			// nothing deeper here?
			return ( newPhoto );
		}

		#endregion

		#region Static Admin-Search Members

		#region Properties

		public static new string SortExpression {
			get { return ( sortExpression ); }
			set { sortExpression = value; }
		}
		protected static new string sortExpression = "seq asc";

		public static int SActivityGroupID {
			get { return ( sActivityGroupID ); }
			set { sActivityGroupID = value; }
		}
		private static int sActivityGroupID = -1;

		#endregion

		#region Methods

		/// <summary>
		/// Clear all the static search properties.
		/// </summary>
		public static void NewSearch() {
			ActivityPhoto.SActivityGroupID = -1;
		}

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			StringBuilder whereBuilder = new StringBuilder();
			string delim = "";
			AddSearchExpr( whereBuilder, "activity_group_id", ActivityPhoto.SActivityGroupID, ref delim );
			AddSearchExpr( whereBuilder, "is_deleted", false, ref delim );

			string sql = @"select * from	ACTIVITY_PHOTOS ";
			if ( whereBuilder.Length != 0 ) {
				sql += string.Format( " WHERE ( {0} ) ", whereBuilder );
			}
			if ( string.IsNullOrEmpty( ActivityPhoto.SortExpression ) ) {
				ActivityPhoto.SortExpression = "seq";
			}
			sql += string.Format( " order by {0}", ActivityPhoto.SortExpression );
			return ( sql );
		}

		public static List<ActivityPhoto> DoSearchList() {
			return ( SqlDatabase.DoSearch<ActivityPhoto>( ActivityPhoto.GetSearchSql() ) );
		}

		#endregion

		#endregion

	}

}
