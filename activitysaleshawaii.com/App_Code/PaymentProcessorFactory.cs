﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	public static class PaymentProcessorFactory {

		public static IPaymentProcessor NewPaymentProcessor() {
			// We can provider-ize this later
			IPaymentProcessor result;
			if ( ConfigMgr.IsPaymentDemoMode ) {
				result = new TestDemoPaymentProcessor();
			} else {
				result = new PFProPaymentProcessor();
			}
			return ( result );
		}

	}

}
