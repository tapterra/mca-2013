using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using VeriSign.Payments.Common;
using VeriSign.Payments.Common.Utility;
using VeriSign.Payments.DataObjects;
using VeriSign.Payments.Transactions;

namespace ABS {

	/// <summary>
	/// Summary description for PFProPaymentProcessor
	/// </summary>
	public class PFProPaymentProcessor : PaymentProcessor {

		#region IPaymentProcessor Implementation

		public override PaymentProcessorResponses DoAuthorizeTransaction() {
			AuthorizationTransaction transaction = new AuthorizationTransaction(
				this.PFProUserInfo, this.Connection, this.Invoice, this.CreditCardTender, PFProUtility.RequestId
			);
			return ( this.ProcessTransaction( transaction ) );
		}

		public override PaymentProcessorResponses DoCaptureTransaction() {
			CaptureTransaction transaction = new CaptureTransaction( this.PNRef,
				this.PFProUserInfo, this.Connection, PFProUtility.RequestId
			);
			return ( this.ProcessTransaction( transaction ) );
		}

		// Not currently being used
		public override PaymentProcessorResponses DoSaleTransaction() {
			SaleTransaction transaction = new SaleTransaction(
				this.PFProUserInfo, this.Connection, this.Invoice, this.CreditCardTender, PFProUtility.RequestId
			);
			return ( this.ProcessTransaction( transaction ) );
		}

		#endregion

		#region Private stuff

		private string PFProUserID {
			get { return ( this.userID ); }
		}
		private string userID = ConfigurationManager.AppSettings["PFPRO_USERID"];

		private string PFProVendorID {
			get { return ( this.vendorID ); }
		}
		private string vendorID = ConfigurationManager.AppSettings["PFPRO_VENDORID"];

		private string PFProPartnerID {
			get { return ( this.partnerID ); }
		}
		private string partnerID = ConfigurationManager.AppSettings["PFPRO_PARTNERID"];

		private string PFProPassword {
			get { return ( this.password ); }
		}
		private string password = ConfigurationManager.AppSettings["PFPRO_PASSWORD"];

		private string PNRef { get; set; }

		private UserInfo PFProUserInfo {
			get {
				if ( this.pfProUserInfo == null ) {
					this.pfProUserInfo = new UserInfo( this.PFProUserID, this.PFProVendorID, this.PFProPartnerID, this.PFProPassword );
				}
				return ( this.pfProUserInfo );
			}
		}
		private UserInfo pfProUserInfo;

		private PFProConnectionData Connection {
			get {
				if ( this.connection == null ) {
					// Create the PFPro Connection data object with the required connection details.
					// The PFPRO_HOST and CERT_PATH are properties defined in App config file.
					this.connection = new PFProConnectionData();
				}
				return ( this.connection );
			}
		}
		private PFProConnectionData connection;

		private Invoice Invoice {
			get {
				if ( this.invoice == null ) {
					// Create a new Invoice data object with the Amount, Billing Address etc. details.
					this.invoice = new Invoice();
					Currency currency = new Currency( this.PaymentAmount );
					currency.Truncate = true;
					currency.NoOfDecimalDigits = 2;
					this.invoice.Amt = currency;
					this.invoice.InvNum = Itinerary.Current.Customer.UID;
					
					// RKP: Requested by Wenona on 05/02/2008:
					this.invoice.Comment1 = Website.Current.JobNo;
					
					// Set the Billing Address details.
					BillTo billTo = new BillTo();
					billTo.Street = this.BillingStreet1;
					billTo.Zip = this.BillingZip;
					billTo.BillToStreet2 = this.BillingStreet2;
					billTo.City = this.BillingCity;
					billTo.State = this.BillingState;
					billTo.BillToCountry = this.BillingCountry;
					this.invoice.BillTo = billTo;
				}
				return ( this.invoice );
			}
		}
		private Invoice invoice;

		private CardTender CreditCardTender {
			get {
				// Return a full CreditCard / CardTender object structure
				// Create a new Payment Device - Credit Card data object.
				string yy = this.CardExpiresYY.ToString( "0000" ).Substring( 2 );
				CreditCard creditCard = new CreditCard( this.CardNumber, string.Format( "{0:00}{1}", this.CardExpiresMM, yy ) );
				creditCard.Cvv2 = this.CardCVVNumber;
				creditCard.Name = this.CardHolderName;

				// Create a new Tender - Card Tender data object.
				CardTender cardTender = new CardTender( creditCard );
				return ( cardTender );
			}
		}

		private static PaymentProcessorResponses MapResponseCode( int respcode ) {
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.UnknownResponse;
			if ( respcode == 0 ) {
				ccResult = PaymentProcessorResponses.ApprovedResponse;
			} else if ( respcode < 0 ) {
				ccResult = PaymentProcessorResponses.CommErrorResponse;
			} else if ( respcode == 12 ) {
				ccResult = PaymentProcessorResponses.DeclinedResponse;
			} else if ( respcode == 13 ) {
				ccResult = PaymentProcessorResponses.ReferralRequestedResponse;
			} else if ( respcode == 112 ) {
				ccResult = PaymentProcessorResponses.AVSFailureResponse;
			} else if ( respcode == 114 ) {
				ccResult = PaymentProcessorResponses.CVV2FailureResponse;
			} else {
				ccResult = PaymentProcessorResponses.UnknownResponse;
			}
			return ( ccResult );
		}

		private PaymentProcessorResponses ProcessTransaction( BaseTransaction transaction ) {

			// See if we're running in demo mode
			//if ( this.IsDemoMode ) {
			//  PaymentProcessorResponses demoResponse;
			//  if ( this.CardExpiresMM == 1 ) { // testing trigger for failure
			//    this.AuthCode = "";
			//    this.ResponseCode = PaymentProcessorResponses.DeclinedResponse.ToString();
			//    this.ResponseMessage = "TESTING - Payment Declined";
			//    demoResponse = PaymentProcessorResponses.DeclinedResponse;
			//  } else {
			//    this.AuthCode = "A123456";
			//    this.ResponseCode = PaymentProcessorResponses.ApprovedResponse.ToString();
			//    this.ResponseMessage = "";
			//    demoResponse = PaymentProcessorResponses.ApprovedResponse;
			//  }
			//  return ( demoResponse );
			//}

			// If this is a No-Payment site...
			if ( Website.Current.IsNoPaymentSite ) {
				// TODO Verify the proper "dummy" authcode for ATCO
				this.AuthCode = "NOPAYSITE";
				this.ResponseCode = PaymentProcessorResponses.ApprovedResponse.ToString();
				this.ResponseMessage = "";
				PaymentProcessorResponses dummyResponse = PaymentProcessorResponses.ApprovedResponse;
				return ( dummyResponse );
			}

			// If we made it this far, we're actually hitting the Verisign test/production server
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.NotSubmitted;
			Response response = transaction.SubmitTransaction();
			if ( response != null ) {
				// Create a new Client Information data object.
				ClientInfo clientInfo = new ClientInfo();
				// Set the client duration.
				clientInfo.ClientDuration = transaction.ClientInfo.ClientDuration;
				// Set the ClientInfo object of the transaction object.
				transaction.ClientInfo = clientInfo;
				// Submit the Commit transaction.
				ccResult = PaymentProcessorResponses.NotCommitted;
				CommitResponse commitResponse = transaction.SubmitCommitTransaction();
				// Process the transaction response 
				// Get the Transaction Response parameters.
				TransactionResponse transactionResponse = response.TransactionResponse;
				if ( transactionResponse != null ) {
					ccResult = PaymentProcessorResponses.UnknownResponse;
					this.PNRef = transactionResponse.Pnref;
					this.AuthCode = transactionResponse.AuthCode;
					this.ResponseCode = transactionResponse.Result;
					this.ResponseMessage = transactionResponse.RespMsg;
					ccResult = MapResponseCode( Convert.ToInt32( transactionResponse.Result ) );
				}

				#region Extras
				// Get the Fraud Response parameters.
				//FraudResponse FraudResp = Resp.FraudResponse;
				//if ( FraudResp != null ) {
				//  //Response.Write( "PREFPSMSG", FraudResp.PreFpsMsg );
				//  //Response.Write( "POSTFPSMSG", FraudResp.PostFpsMsg );
				//}
				// Display the commit transaction response parameters.
				//if ( commitResponse != null ) {
				//  //Response.Write( "COMMIT RESULT", commitResponse.Result );
				//  //Response.Write( "COMMIT RESPMSG", commitResponse.RespMsg );
				//}
				// Display the response.
				//Response.Write( PFProUtility.GetStatus( Resp ) );

				// Get the Transaction Context and check for any contained SDK specific errors (optional code).
				//Context TransCtx = Resp.TransactionContext;
				//if ( TransCtx != null && TransCtx.getErrorCount() > 0 ) {
				//  //Response.Write( "Transaction Errors", TransCtx.ToString() );
				//}
				// Get the Commit Context and check for any contained SDK specific errors (optional code).
				//if ( commitResponse != null ) {
				//  // Get the Commit Transaction Context and check for any contained SDK specific errors (optional code).
				//  Context CommitCtx = commitResponse.TransactionContext;
				//  if ( CommitCtx != null && CommitCtx.getErrorCount() > 0 ) {
				//    //Response.Write( "Commit Errors", CommitCtx.ToString() );
				//  }
				//}

				/* RESULT : RESPMSG:
						  1 : User authentication failed.
						  2 : Invalid tender. Your merchant bank account does not support the following credit card type that was submitted. 
						  3 : Invalid transaction type. Transaction type is not appropriate for this transaction. 
									For example, you cannot credit an authorization-only transaction.
						  4 : Invalid amount.
						  5 : Invalid merchant information. Processor does not recognize your merchant account information. 
									Contact your bank account acquirer to resolve this problem.
						  7 : Field format error. Invalid information entered. 
						  8 : Not a transaction server
						  9 : Too many parameters or invalid stream
						 10 : Too many line items
						 11 : Client time-out waiting for response
						 12 : Declined. Check the credit card number and transaction information to make sure they were entered correctly. 
									If this does not resolve the problem, have the customer call the credit card issuer to resolve.
						 13 : Referral. Transaction was declined but could be approved with a verbal authorization from the bank that issued the card. 
									Submit a manual Voice Authorization transaction and enter the verbal auth code. 
						 19 : Original transaction ID not found. The transaction ID you entered for this transaction is not valid. 
						 20 : Cannot find the customer reference number
						 22 : Invalid ABA number
						 23 : Invalid account number. Check credit card number and re-submit.
						 24 : Invalid expiration date. Check and re-submit.
						 25 : Transaction type not mapped to this host
						 26 : Invalid vendor account
						 27 : Insufficient partner permissions
						 28 : Insufficient user permissions
						 50 : Insufficient funds available
						 99 : General error
						100 : Invalid transaction returned from host
						101 : Time-out value too small
						102 : Processor not available
						103 : Error reading response from host
						104 : Timeout waiting for processor response. Try your transaction again.
						105 : Credit error. Make sure you have not already credited this transaction, or that this transaction ID is for a creditable transaction. 
									(For example, you cannot credit an authorization.)
						106 : Host not available
						107 : Duplicate suppression time-out
						108 : Void error. Make sure the transaction ID entered has not already been voided. 
									If not, then look at the Transaction Detail screen for this transaction to see if it has settled. 
									(The Batch field is set to a number greater than zero if the transaction has been settled). 
									If the transaction has already settled, your only recourse is a reversal (credit a payment or submit a payment for a credit).
						109 : Time-out waiting for host response
						111 : Capture error. Only authorization transactions can be captured.
						112 : Failed AVS check. Address and ZIP code do not match. An authorization may still exist on the cardholder's account.
						113 : Cannot exceed sales cap. For ACH transactions only.
						114 : CVV2 Mismatch. An authorization may still exist on the cardholder's account.
						1000 : Generic host error. This is a generic message returned by your credit card processor. 
					 				 The message itself will contain more information describing the error. 
					 */
				//Response.Write( "RESULT", transactionResponse.Result );
				//Response.Write( "PNREF", transactionResponse.Pnref );
				//Response.Write( "RESPMSG", transactionResponse.RespMsg );
				//Response.Write( "AUTHCODE", transactionResponse.AuthCode );
				//Response.Write( "AVSADDR", transactionResponse.AVSAddr );
				//Response.Write( "AVSZIP", transactionResponse.AVSZip );
				//Response.Write( "IAVS", transactionResponse.IAVS );
				//Response.Write( "CVV2MATCH", transactionResponse.CVV2Match );
				#endregion

			}
			return ( ccResult );
		}

		#endregion

	}

}

#region reference
/*
		public override PaymentProcessorResponses DoSaleTransaction() {
			#region See if we're running in demo mode
			if ( this.IsDemoMode ) {
				if ( this.CardExpiresMM == 1 ) { // testing trigger for failure
					this.AuthCode = "";
					this.ResponseCode = PaymentProcessorResponses.DeclinedResponse.ToString();
					this.ResponseMessage = "TESTING - Payment Declined";
					return ( PaymentProcessorResponses.DeclinedResponse );
				} else {
					this.AuthCode = "A123456";
					this.ResponseCode = PaymentProcessorResponses.ApprovedResponse.ToString();
					this.ResponseMessage = "";
					return ( PaymentProcessorResponses.ApprovedResponse );
				}
			}
			// If we made it this far, we're actually hitting the Verisign test/production server
			#endregion
			// Create a new Sale Transaction.
			SaleTransaction transaction = new SaleTransaction( 
				this.PFProUserInfo, this.Connection, this.Invoice, this.CreditCardTender, PFProUtility.RequestId 
			);
			// Submit the Transaction
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.NotSubmitted;
			Response response = transaction.SubmitTransaction();
			if ( response != null ) {
				// Create a new Client Information data object.
				ClientInfo clientInfo = new ClientInfo();
				// Set the client duration.
				clientInfo.ClientDuration = transaction.ClientInfo.ClientDuration;
				// Set the ClientInfo object of the transaction object.
				transaction.ClientInfo = clientInfo;
				// Submit the Commit transaction.
				ccResult = PaymentProcessorResponses.NotCommitted;
				CommitResponse commitResponse = transaction.SubmitCommitTransaction();

				// Process the transaction response 
				// Get the Transaction Response parameters.
				TransactionResponse transactionResponse = response.TransactionResponse;
				if ( transactionResponse != null ) {
					ccResult = PaymentProcessorResponses.UnknownResponse;
					this.PNRef = transactionResponse.Pnref;
					this.AuthCode = transactionResponse.AuthCode;
					this.ResponseCode = transactionResponse.Result;
					this.ResponseMessage = transactionResponse.RespMsg;
					ccResult = MapResponseCode( Convert.ToInt32( transactionResponse.Result ) );
				}
			}
			return ( ccResult );
		}
		public override PaymentProcessorResponses DoAuthorizeTransaction() {
			// Create a new Auth Transaction.
			AuthorizationTransaction transaction = new AuthorizationTransaction(
				this.PFProUserInfo, this.Connection, this.Invoice, this.CreditCardTender, PFProUtility.RequestId
			);
			// Submit the Transaction
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.NotSubmitted;
			Response response = transaction.SubmitTransaction();
			if ( response != null ) {
				// Create a new Client Information data object.
				ClientInfo clientInfo = new ClientInfo();
				// Set the client duration.
				clientInfo.ClientDuration = transaction.ClientInfo.ClientDuration;
				// Set the ClientInfo object of the transaction object.
				transaction.ClientInfo = clientInfo;
				// Submit the Commit transaction.
				ccResult = PaymentProcessorResponses.NotCommitted;
				CommitResponse commitResponse = transaction.SubmitCommitTransaction();

				// Get the Transaction Response parameters.
				TransactionResponse transactionResponse = response.TransactionResponse;
				if ( transactionResponse != null ) {
					ccResult = PaymentProcessorResponses.UnknownResponse;
					this.PNRef = transactionResponse.Pnref;
					this.AuthCode = transactionResponse.AuthCode;
					this.ResponseCode = transactionResponse.Result;
					this.ResponseMessage = transactionResponse.RespMsg;
					ccResult = MapResponseCode( Convert.ToInt32( transactionResponse.Result ) );
				}
			}
			return ( ccResult );
		}
		public override PaymentProcessorResponses DoCaptureTransaction() {
			// Create a new Capture transaction.
			CaptureTransaction transaction = new CaptureTransaction( this.PNRef,
				this.PFProUserInfo, this.Connection, PFProUtility.RequestId 
			);
			// Submit the Transaction
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.NotSubmitted;
			Response response = transaction.SubmitTransaction();
			if ( response != null ) {
				// Create a new Client Information data object.
				ClientInfo clientInfo = new ClientInfo();
				// Set the client duration.
				clientInfo.ClientDuration = transaction.ClientInfo.ClientDuration;
				// Set the ClientInfo object of the transaction object.
				transaction.ClientInfo = clientInfo;
				// Submit the Commit transaction.
				ccResult = PaymentProcessorResponses.NotCommitted;
				CommitResponse commitResponse = transaction.SubmitCommitTransaction();

				// Get the Transaction Response parameters.
				TransactionResponse transactionResponse = response.TransactionResponse;
				if ( transactionResponse != null ) {
					ccResult = PaymentProcessorResponses.UnknownResponse;
					this.PNRef = transactionResponse.Pnref;
					this.AuthCode = transactionResponse.AuthCode;
					this.ResponseCode = transactionResponse.Result;
					this.ResponseMessage = transactionResponse.RespMsg;
					ccResult = MapResponseCode( Convert.ToInt32( transactionResponse.Result ) );
				}
			}
			return ( ccResult );
		}

		public void DoCreditTransaction() {
			// Following is an example of a independent credit type of transaction.
			CreditTransaction transaction = new CreditTransaction( 
				this.PFProUserInfo, this.Connection, this.Invoice, this.CreditCardTender, PFProUtility.RequestId 
			);
			// Submit the Transaction
			PaymentProcessorResponses ccResult = PaymentProcessorResponses.NotSubmitted;
			Response response = transaction.SubmitTransaction();
			if ( response != null ) {
				// Create a new Client Information data object.
				ClientInfo clientInfo = new ClientInfo();
				// Set the client duration.
				clientInfo.ClientDuration = transaction.ClientInfo.ClientDuration;
				// Set the ClientInfo object of the transaction object.
				transaction.ClientInfo = clientInfo;
				// Submit the Commit transaction.
				ccResult = PaymentProcessorResponses.NotCommitted;
				CommitResponse commitResponse = transaction.SubmitCommitTransaction();

				// Get the Transaction Response parameters.
				TransactionResponse transactionResponse = response.TransactionResponse;
				if ( transactionResponse != null ) {
					//Console.WriteLine( "RESULT = " + transactionResponse.Result );
					//Console.WriteLine( "PNREF = " + transactionResponse.Pnref );
					//Console.WriteLine( "RESPMSG = " + transactionResponse.RespMsg );
					//Console.WriteLine( "AUTHCODE = " + transactionResponse.AuthCode );
					//Console.WriteLine( "AVSADDR = " + transactionResponse.AVSAddr );
					//Console.WriteLine( "AVSZIP = " + transactionResponse.AVSZip );
					//Console.WriteLine( "IAVS = " + transactionResponse.IAVS );
					//Console.WriteLine( "CVV2MATCH = " + transactionResponse.CVV2Match );
				}

				// Get the Fraud Response parameters.
				FraudResponse FraudResp = response.FraudResponse;
				if ( FraudResp != null ) {
					Console.WriteLine( "PREFPSMSG = " + FraudResp.PreFpsMsg );
					Console.WriteLine( "POSTFPSMSG = " + FraudResp.PostFpsMsg );
				}

				// Display the commit transaction response parameters.
				if ( commitResponse != null ) {
					Console.WriteLine( Environment.NewLine + "COMMIT RESULT = " + commitResponse.Result );
					Console.WriteLine( "COMMIT RESPMSG = " + commitResponse.RespMsg );
				}

				// Display the response.
				Console.WriteLine( Environment.NewLine + PFProUtility.GetStatus( response ) );

				// Get the Transaction Context and check for any contained SDK specific errors (optional code).
				Context TransCtx = response.TransactionContext;
				if ( TransCtx != null && TransCtx.getErrorCount() > 0 ) {
					Console.WriteLine( Environment.NewLine + "Transaction Errors = " + TransCtx.ToString() );
				}

				// Get the Commit Context and check for any contained SDK specific errors (optional code).
				if ( commitResponse != null ) {
					// Get the Commit Transaction Context and check for any contained SDK specific errors (optional code).
					Context CommitCtx = commitResponse.TransactionContext;
					if ( CommitCtx != null && CommitCtx.getErrorCount() > 0 ) {
						Console.WriteLine( Environment.NewLine + "Commit Errors = " + CommitCtx.ToString() );
					}
				}
			}
		}

		*/

#endregion

