using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Collections.ObjectModel;

namespace ABS {
	
	public class SubcategoriesList : KeyedCollection<int, Subcategory> {

		private int CategoryID = -1;

		public SubcategoriesList( int categoryID ) : base() {
			this.CategoryID = categoryID;
			this.LoadItems();
		}

		protected override int GetKeyForItem( Subcategory item ) {
			return ( item.ID );
		}

		private void LoadItems() {
			if ( this.CategoryID <= 0 ) {
				return;
			}
			string sql = string.Format( "select * from SUBCATEGORIES where ( category_id = {0} ) order by seq", this.CategoryID );
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql ) ) {
					Subcategory subcategory = new Subcategory();
					while ( SqlDatabase.InstantiateFromDataRdr( rdr, subcategory ) ) {
						this.Add( subcategory );
						subcategory = new Subcategory();
					}
				}
			}
		}

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_subcategories";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure, db.NewParameter( "@category_id", this.CategoryID ) );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public int HighestSeq {
			get { return ( this.ResetItemRanks() - 1 ); }
		}

		/// <summary>
		/// Increments the sequence-number of the item at the given sequence-number, moving it down in the sequence.
		/// </summary>
		/// <param name="Seq">Current sequence-number of the item to move.</param>
		public void MoveDown( int Seq ) {
			// We can't demote if we're already at the end
			if ( Seq >= this.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update SUBCATEGORIES set seq = -1 where category_id = @category_id and seq = @seq; ";
			// Move the Activity below me up to my spot
			sql += "update SUBCATEGORIES set seq = @seq where category_id = @category_id and seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update SUBCATEGORIES set seq = @seq + 1 where category_id = @category_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@category_id", this.CategoryID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

		/// <summary>
		/// Decrements the sequence-number of the item at the given sequence-number, moving it up in the sequence.
		/// </summary>
		/// <param name="Seq">Current sequence-number of the iyem to move.</param>
		public void MoveUp( int Seq ) {
			int result = this.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update SUBCATEGORIES set seq = -1 where category_id = @category_id and seq = @seq; ";
			// Move the item above me down to my spot
			sql += "update SUBCATEGORIES set seq = @seq where category_id = @category_id and seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update SUBCATEGORIES set seq = @seq - 1 where category_id = @category_id and seq = -1; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@category_id", this.CategoryID ),
					db.NewParameter( "@seq", Seq )
				);
			}
		}

	}

}
