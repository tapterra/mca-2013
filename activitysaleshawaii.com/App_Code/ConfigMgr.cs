using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.UI;

namespace ABS {

	/// <summary>
	/// Static class providing configuration parms to the ABS library & web-apps
	/// </summary>
	public static class ConfigMgr {

		#region Database-Driven (WEBSITES)

		/// <summary>
		/// ATCO-Assigned vendor ID for the host of this site (HOST_VENDOR_AID key in web.config).
		/// Needed to call any of the atco ws methods.
		/// </summary>
		public static string HostVendorAID {
			get { return ( Website.Current.HostVendorAid ); }
		}

		/// <summary>
		/// When true, use the mocked-up TestDemoPaymentProcessor
		/// </summary>
		public static bool IsPaymentDemoMode {
			get {
				if ( PaymentProcessorMode.ToUpper() == "TEST" ) {
					return ( true );
				}
				return ( Website.Current.IsPmtProcessorTestMode );
			}
		}

		/// <summary>
		/// When true, use the mocked-up TestDemoVoucherService
		/// </summary>
		public static bool IsVoucherDemoMode {
			get {
				if ( VoucherServiceMode.ToUpper() == "TEST" ) {
					return ( true );
				}
				if ( Website.Current == null ) {
					return ( false );
				}
				return ( Website.Current.IsBookingTestMode );
			}
		}

		/// <summary>
		/// "From" email address to use when sending vouchers
		/// </summary>
		public static string VoucherEmailFromAddress {
			get { return ( Website.Current.VoucherEmailFromAddress ); }
		}

		/// <summary>
		/// 
		/// </summary>
		public static string VoucherEmailBccAddresses {
			get { return ( Website.Current.VoucherEmailBccAddresses ); }
		}

		/// <summary>
		/// 
		/// </summary>
		public static string VoucherEmailSubject {
			get { return ( Website.Current.VoucherEmailSubject ); }
		}

		/// <summary>
		/// MCA's mailing address to display on the vouchers
		/// </summary>
		public static string HostVendorAddress {
			get { return ( Website.Current.HostVendorAddress ); }
		}

		/// <summary>
		/// Fully-qualified URL to the logo graphic for the vouchers
		/// </summary>
		public static string VoucherLogoUrl {
			get { return ( Website.Current.VoucherLogoUrl ); }
		}

		/// <summary>
		/// Email address(es) to send exception reports to
		/// </summary>
		public static string ExcReportsToAddress {
			get { return ( Website.Current.ExcReportsToAddress ); }
		}

		#endregion

		#region From web.config

		private static string PAYMENT_PROCESSOR_MODE = "PAYMENT_PROCESSOR_MODE";
		public static string PaymentProcessorMode {
			get { return ( ConfigurationManager.AppSettings[PAYMENT_PROCESSOR_MODE] ); }
		}

		private static string VOUCHER_SERVICE_MODE = "VOUCHER_SERVICE_MODE";
		public static string VoucherServiceMode {
			get { return ( ConfigurationManager.AppSettings[VOUCHER_SERVICE_MODE] ); }
		}

		private static string WEBSITE_JOBNO = "WEBSITE_JOBNO";
		/// <summary>
		/// MC&A Job-Number identifier for the current site
		/// </summary>
		public static string WebsiteJobNo {
			get {
				// RKP: We can't use a static private field for state storage here, the subsites will step on each other
				return ( ConfigurationManager.AppSettings[WEBSITE_JOBNO] );
			}
		}

		private static string PMT_PROCESSOR_SERVICE_NAME = "PMT_PROCESSOR_SERVICE_NAME";
		/// <summary>
		/// Service name for the CC payment processor (for ATCO)
		/// </summary>
		public static string PmtProcessorServiceName {
			get {
				if ( string.IsNullOrEmpty( pmtProcessorServiceName ) ) {
					pmtProcessorServiceName = ConfigurationManager.AppSettings[PMT_PROCESSOR_SERVICE_NAME];
				}
				return ( pmtProcessorServiceName );
			}
		}
		private static string pmtProcessorServiceName;

		private static string HOTELS_LIST_RSS_URL = "HOTELS_LIST_RSS_URL";
		/// <summary>
		/// URL To ATCO's RSS feed of known hotels
		/// </summary>
		public static string HotelsListRssUrl {
			get {
				if ( string.IsNullOrEmpty( hotelsListRssUrl ) ) {
					hotelsListRssUrl = ConfigurationManager.AppSettings[HOTELS_LIST_RSS_URL];
				}
				return ( hotelsListRssUrl );
			}
		}
		private static string hotelsListRssUrl;

		private static string ACTIVITIES_LIST_RSS_URL = "ACTIVITIES_LIST_RSS_URL";
		/// <summary>
		/// URL to the activities-list RSS feed
		/// </summary>
		public static string ActivitiesListRssUrl {
			get {
				if ( string.IsNullOrEmpty( activitiesListRssUrl ) ) {
					activitiesListRssUrl = ConfigurationManager.AppSettings[ACTIVITIES_LIST_RSS_URL];
				}
				return ( activitiesListRssUrl );
			}
		}
		private static string activitiesListRssUrl;

		#region AppendBooking exception error-message mappings

		// These are the codes from ATCO (embedded in the AppendBooking exception message)
		// Use these constants when testing for the exception-message codes
		internal static string ErrOversizedBlockCode = "OVRBLK";
		internal static string ErrBlockExpiredCode = "BLKEXP";
		internal static string ErrSoldOutCode = "SLDOUT";
		internal static string ErrGeneralCode = "GENERR";

		// These are the keys used in the web.config file (used in properties below to retrieve the msgs)
		// The codes from ATCO (above) may change, these don't have to
		private static string ERR_OVRBLK = "ERR_OVRBLK";
		private static string ERR_BLKEXP = "ERR_BLKEXP";
		private static string ERR_SLDOUT = "ERR_SLDOUT";
		private static string ERR_GENERR = "ERR_GENERR";
		private static string ERR_PAYMENT = "ERR_PAYMENT";
		private static string ERR_RESERVE_BOOKINGS = "ERR_RESERVE_BOOKINGS";

		/// <summary>
		/// Error message thrown when booking a party "too large for the internet"
		/// </summary>
		public static string ErrOversizedBlockMsg( string ActivityTitle, string BookingDateTimeString ) {
			if ( string.IsNullOrEmpty( _ErrOversizedBlockMsg ) ) {
				_ErrOversizedBlockMsg = ConfigurationManager.AppSettings[ERR_OVRBLK];
			}
			return ( string.Format( _ErrOversizedBlockMsg, ActivityTitle, BookingDateTimeString ) );
		}
		private static string _ErrOversizedBlockMsg;

		/// <summary>
		/// Error message thrown when booking for a date past the cut-off date for the block
		/// </summary>
		public static string ErrBlockExpiredMsg( string ActivityTitle, string BookingDateTimeString ) {
			if ( string.IsNullOrEmpty( _ErrBlockExpiredMsg ) ) {
				_ErrBlockExpiredMsg = ConfigurationManager.AppSettings[ERR_BLKEXP];
			}
			return ( string.Format( _ErrBlockExpiredMsg, ActivityTitle, BookingDateTimeString ) );
		}
		private static string _ErrBlockExpiredMsg;

		/// <summary>
		/// Error message shown when the activity/date is sold out
		/// </summary>
		public static string ErrSoldOutMsg( string ActivityTitle, string BookingDateTimeString ) {
			if ( string.IsNullOrEmpty( _ErrSoldOutMsg ) ) {
				_ErrSoldOutMsg = ConfigurationManager.AppSettings[ERR_SLDOUT];
			}
			return ( string.Format( _ErrSoldOutMsg, ActivityTitle, BookingDateTimeString ) );
		}
		private static string _ErrSoldOutMsg;

		/// <summary>
		/// General (unspecified) ATCO/web-service error
		/// </summary>
		public static string ErrGeneralMsg( string ActivityTitle, string BookingDateTimeString ) {
			if ( string.IsNullOrEmpty( _ErrGeneralMsg ) ) {
				_ErrGeneralMsg = ConfigurationManager.AppSettings[ERR_GENERR];
			}
			return ( string.Format( _ErrGeneralMsg, ActivityTitle, BookingDateTimeString ) );
		}
		private static string _ErrGeneralMsg;

		/// <summary>
		/// Error message shown when exceptions are thrown by the CC Processor
		/// </summary>
		public static string ErrPaymentMsg( string PmtProcessorMsg ) {
			if ( string.IsNullOrEmpty( _ErrPaymentMsg ) ) {
				_ErrPaymentMsg = ConfigurationManager.AppSettings[ERR_PAYMENT];
			}
			return ( string.Format( _ErrPaymentMsg, PmtProcessorMsg ) );
		}
		private static string _ErrPaymentMsg;

		/// <summary>
		/// Error message shown when an unexpected exception is thrown in AtcoVoucherService.ReserveBookings
		/// </summary>
		public static string ErrReserveBookingsMsg {
			get {
				if ( string.IsNullOrEmpty( _ErrReserveBookingsMsg ) ) {
					_ErrReserveBookingsMsg = ConfigurationManager.AppSettings[ERR_RESERVE_BOOKINGS];
				}
				return ( _ErrReserveBookingsMsg );
			}
		}
		private static string _ErrReserveBookingsMsg;

		// RKP End Change

		#endregion

		/// <summary>
		/// Flag to activate detailed email tracing for itinerary processing
		/// </summary>
		private static string SEND_ITIN_TRACE_EMAILS = "SEND_ITIN_TRACE_EMAILS";
		public static bool SendItinTraceEmails {
			get {
				if ( string.IsNullOrEmpty( _SendItinTraceEmails ) ) {
					_SendItinTraceEmails = ConfigurationManager.AppSettings[SEND_ITIN_TRACE_EMAILS];
					// If the option isn't defined in web.config, assume it should be false
					if ( string.IsNullOrEmpty( _SendItinTraceEmails ) ) { return ( false ); }
				}
				// No sense trying without an email adress to send it to
				if ( string.IsNullOrEmpty( DevEmailAddress ) ) { return ( false ); }
				return ( _SendItinTraceEmails.ToLower().Equals( "true" ) );
			}
		}
		private static string _SendItinTraceEmails;
		
		private static string DEV_EMAIL_ADDRESS = "DEV_EMAIL_ADDRESS";
		/// <summary>
		/// Email address to send all debugging reports to
		/// </summary>
		public static string DevEmailAddress {
			get {
				if ( string.IsNullOrEmpty( _DevEmailAddress ) ) {
					_DevEmailAddress = ConfigurationManager.AppSettings[DEV_EMAIL_ADDRESS];
				}
				return ( _DevEmailAddress );
			}
		}
		private static string _DevEmailAddress;

		#endregion

		public static string SitePageUrl( Page page, string pageName ) {
			return ( string.Format( "{0}{1}", page.AppRelativeTemplateSourceDirectory, pageName ) );
		}

		public static string SiteSslPageUrl( Page page, string pageName ) {
			string url = string.Format( "{0}{1}", page.AppRelativeTemplateSourceDirectory, pageName ).Replace( "~/", "" ).Replace( "~", "" );
#if DEBUG
			string pathPattern = "http://{0}{1}{2}";
#else
			string pathPattern = "https://{0}{1}{2}";
#endif
			url = string.Format( pathPattern, page.Request.Url.Authority, page.Request.ApplicationPath, url );
			return ( url );
		}


	}

}
