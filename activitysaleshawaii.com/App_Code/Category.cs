using System;
using StarrTech.WebTools.Data;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Collections.ObjectModel;

namespace ABS {

	[SqlTable( "CATEGORIES", SingularLabel = "Category", PluralLabel = "Categories", IsMarkedDeleted = false )]
	public class Category : BaseSqlPersistable {

		#region Properties

		#region Database Columns
		//[seq] [int] NOT NULL,
		[SqlColumn( "seq", DbType.Int32 )]
		public int Seq {
			get { return ( this.seq ); }
			set { this.seq = value; }
		}
		private int seq = -1;

		//[label] [nvarchar](50) NOT NULL,
		[SqlColumn( "label", DbType.String, Length = 50 )]
		public string Label {
			get { return ( this.label ); }
			set { this.label = value; }
		}
		private string label;

		//[comments] [ntext] NOT NULL,
		[SqlColumn( "comments", DbType.String )]
		public string Comments {
			get { return ( this.comments ); }
			set { this.comments = value; }
		}
		private string comments;

		//[key_name] [nvarchar](20) NOT NULL,
		[SqlColumn( "key_name", DbType.String, Length = 20 )]
		public string KeyName {
			get { return ( this.keyName ); }
			set { this.keyName = value; }
		}
		private string keyName;

		#endregion

		public SubcategoriesList Subcategories {
			get {
				if ( this.subcategories == null ) {
					this.subcategories = new SubcategoriesList( this.ID );
				}
				return ( this.subcategories );
			}
		}
		private SubcategoriesList subcategories;

		#endregion

		#region Constructors

		// Access the base constructors
		public Category() : base() { }
		public Category( int ID ) : base( ID ) { }
		public Category( string key ) : base() {
			string sql = "select * from CATEGORIES where ( key_name = @key_name ) order by seq";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.SelectInstance( this, sql, db.NewParameter( "@key_name", key ) );
			}
		}

		#endregion

		#region Methods

		public override bool Persist() {
			bool result = base.Persist();
			foreach ( Subcategory sub in this.subcategories ) {
				sub.CategoryID = this.ID;
				sub.Persist();
			}
			return ( result );
		}

		public override void Restore() {
			base.Restore();
			this.subcategories = new SubcategoriesList( this.ID );
		}

		public override bool Delete() {
			if ( ! this.IsValidToDelete() ) {
				return ( false );
			}
			foreach ( Subcategory sub in this.subcategories ) {
				sub.Delete();
			}
			return ( base.Delete() );
		}

		public override bool IsValidToDelete() {
			this.ValidationMessages.Clear();
			foreach ( Subcategory scat in this.Subcategories ) {
				if ( !scat.IsValidToDelete() ) {
					this.ValidationMessages.Add( "This Category can't be deleted because it is used by one or more Activity Groups." );
					return ( false );
				}
			}
			string sql = "select top 1 id from ACTIVITY_GROUPS where category_id = @category_id and is_deleted = 0";
			using ( SqlDatabase db = new SqlDatabase() ) {
				using ( IDataReader rdr = db.SelectRecords( sql, db.NewParameter( "@category_id", this.ID ) ) ) {
					if ( rdr.Read() ) {
						this.ValidationMessages.Add( "This Category can't be deleted because it is used by one or more Activity Groups." );
						return ( false );
					}
				}
			}
			return ( base.IsValidToDelete() );
		}

		#endregion

		#region Reordering Support

		/// <summary>
		/// Calls a sproc to renumber all the seq values from zero
		/// </summary>
		/// <returns>Total number of undeleted items</returns>
		public static int ResetItemRanks() {
			int result = -1;
			string sproc = "dbo.renumber_categories";
			using ( SqlDatabase db = new SqlDatabase() ) {
				result = db.Execute( sproc, CommandType.StoredProcedure );
			}
			return ( result );
		}

		/// <summary>
		/// Returns the highest sequence-number currently used by this list's items.
		/// </summary>
		public static int HighestSeq {
			get { return ( Category.ResetItemRanks() - 1 ); }
		}

		public void MoveUp() {
			Category.ResetItemRanks();
			// We can't promote if we're already at the top
			if ( this.Seq <= 0 ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update CATEGORIES set seq = -1 where id = @id; ";
			// Move the item above me down to my spot
			sql += "update CATEGORIES set seq = @seq where seq = @seq - 1; ";
			// Replace me in the spot I just vacated
			sql += "update CATEGORIES set seq = @seq - 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		public void MoveDown() {
			// We can't demote if we're already at the end
			if ( this.Seq >= Category.HighestSeq ) {
				return;
			}
			string sql = "";
			// Move me out of the way for a minute
			sql += "update CATEGORIES set seq = -1 where id = @id; ";
			// Move the item below me up to my spot
			sql += "update CATEGORIES set seq = @seq where seq = @seq + 1; ";
			// Replace me in the spot I just vacated
			sql += "update CATEGORIES set seq = @seq + 1 where id = @id; ";
			using ( SqlDatabase db = new SqlDatabase() ) {
				db.Execute( sql,
					db.NewParameter( "@id", this.ID ),
					db.NewParameter( "@seq", this.Seq )
				);
			}
		}

		#endregion

		#region Static Search Members

		public static void NewSearch() { }

		/// <summary>
		/// Builds a SQL query based on the current values of the static search properties.
		/// </summary>
		/// <returns>A SQL SELECT statement as a string.</returns>
		public static string GetSearchSql() {
			string sql = @"select * from	CATEGORIES ";
			if ( string.IsNullOrEmpty( Category.SortExpression ) ) {
				Category.SortExpression = "seq";
			}
			sql += string.Format( " order by {0}", Category.SortExpression );
			return ( sql );
		}

		public static List<Category> DoSearchList() {
			return ( SqlDatabase.DoSearch<Category>( Category.GetSearchSql() ) );
		}

		#endregion

	}

}
