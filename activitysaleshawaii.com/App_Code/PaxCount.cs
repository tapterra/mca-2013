using System;
using System.Data;
using StarrTech.WebTools.Data;

namespace ABS {
	
	/// <summary>
	/// This is the activity booking, passenger count information
	/// </summary>
	[SqlTable( "PAX_COUNTS", SingularLabel = "Pax Count", PluralLabel = "Pax Counts", IsMarkedDeleted = true )]
	public class PaxCount : BaseSqlPersistable {

		#region Properties

		//  [booking_id] [int] 
		[SqlColumn( "booking_id", DbType.Int32 )]
		public int BookingID {
			get { return ( this.bookingID ); }
			set { this.bookingID = value; }
		}
		private int bookingID = -1;

		//  [pricecode_uid] [nvarchar](30) 
		[SqlColumn( "pricecode_uid", DbType.String, Length = 30 )]
		public string PriceCodeUID {
			get { return ( this.priceCodeUID ); }
			set { this.priceCodeUID = value; }
		}
		private string priceCodeUID;

		//  [pricecode_aid] [nvarchar](30) 
		[SqlColumn( "pricecode_aid", DbType.String, Length = 30 )]
		public string PriceCodeAID {
			get { return ( this.priceCodeAID ); }
			set { this.priceCodeAID = value; }
		}
		private string priceCodeAID;

		//  [pricecode_label] [nvarchar](50) 
		[SqlColumn( "pricecode_label", DbType.String, Length = 50 )]
		public string PriceCodeLabel {
			get { return ( this.priceCodeLabel ); }
			set { this.priceCodeLabel = value; }
		}
		private string priceCodeLabel;

		//  [pricecode_price] [money] 
		[SqlColumn( "pricecode_price", DbType.Currency )]
		public Decimal PriceCodePrice {
			get { return ( this.priceCodePrice ); }
			set { this.priceCodePrice = value; }
		}
		private Decimal priceCodePrice;

		//  [pax_count] [int] 
		[SqlColumn( "pax_count", DbType.Int32 )]
		public int PaxQty {
			get { return ( this.paxQty ); }
			set { this.paxQty = value; }
		}
		private int paxQty;
		
		#endregion

		#region Constructors
		
		private PaxCount() {}

		public PaxCount( PriceCode priceCode, int qty ) {
			this.PriceCodeUID = priceCode.UID;
			this.PriceCodeAID = priceCode.AID;
			this.PriceCodeLabel = priceCode.Label;
			this.PriceCodePrice = priceCode.Price;
			this.PaxQty = qty;
		}

		protected internal PaxCount( Booking booking ) : base() {
			//this.Owner = booking;
			this.bookingID = booking.ID;
		}

		#endregion

		#region Methods



		#endregion
	}
}
