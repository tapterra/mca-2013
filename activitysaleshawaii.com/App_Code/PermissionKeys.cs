using System;
using System.Collections.Generic;
using System.Text;

namespace ABS {

	/// <summary>
	/// Summary description for PermissionKeys.
	/// </summary>
	public enum PermissionKeys {
		None,

		Admin,

		AdminGroups,
		AdminGroupsView,
		AdminGroupsAdd,
		AdminGroupsUpdate,
		AdminGroupsDelete,

		AdminUsers,
		AdminUsersView,
		AdminUsersAdd,
		AdminUsersUpdate,
		AdminUsersDelete,

		AdminWebsites,
		AdminWebsitesView,
		AdminWebsitesAdd,
		AdminWebsitesUpdate,
		AdminWebsitesDelete,

		AdminIslands,
		AdminIslandsView,
		AdminIslandsAdd,
		AdminIslandsUpdate,
		AdminIslandsDelete,

		AdminCategories,
		AdminCategoriesView,
		AdminCategoriesAdd,
		AdminCategoriesUpdate,
		AdminCategoriesDelete,

		AdminPromoCodes,
		AdminPromoCodesView,
		AdminPromoCodesAdd,
		AdminPromoCodesUpdate,
		AdminPromoCodesDelete,

		AdminActivities,
		AdminActivitiesView,
		AdminActivitiesAdd,
		AdminActivitiesUpdate,
		AdminActivitiesDelete,
		AdminActivitiesApprove,

		AdminCustomers,
		AdminCustomersView,
		AdminCustomersUpdate,

		AdminReservations,
		AdminReservationsView,
		AdminReservationsUpdate
	}

}
