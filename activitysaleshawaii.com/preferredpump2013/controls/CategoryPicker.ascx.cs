using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

using ABS;

public partial class CategoryPicker : System.Web.UI.UserControl {

	#region Properties

	public string IslandKey {
		get {
			object o = Session["CurrentIslandKey"];
			return ( o == null ? "oahu" : (string) o );
		}
		set { Session["CurrentIslandKey"] = value; }
	}

	public Island Island {
		get {
			if ( this.island == null ) {
				this.island = new Island( this.IslandKey );
			}
			return ( this.island );
		}
	}
	private Island island;

	public string CategoryKey {
		get {
			object o = Session["CurrentCategoryKey"];
			return ( o == null ? "activities" : (string) o );
		}
		set { Session["CurrentCategoryKey"] = value; }
	}

	public Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryKey );
			}
			return ( this.category );
		}
	}
	private Category category;

	#endregion

	#region Methods

	public void HotSpotClicked( Object sender, ImageMapEventArgs e ) {
		string[] args = e.PostBackValue.Split( ';' );
		string catkey = args[0];
		string islandkey = args[1];
		if ( !string.IsNullOrEmpty( catkey ) ) {
			this.CategoryKey = catkey;
		}
		if ( !string.IsNullOrEmpty( islandkey ) ) {
			this.IslandKey = islandkey;
		}
        Response.Redirect( ConfigMgr.SitePageUrl( this.Page, "catalog.aspx" ) );
	}

	#endregion

}