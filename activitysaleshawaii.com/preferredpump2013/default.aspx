<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/mauirs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha!</p>
<p class="PublisherDescription">Welcome to the Preferred Pump activity website. Within this website you will discover the many exciting tours being offered on the island of Maui. <br /><br />Feel free to browse this website and discover all of these activities you can book for your day of leisure. <Br /><Br />
Reservations will close on December 19, 2012 at 5:00 p.m. Hawaii Standard Time. Space is limited so make your reservations early.<Br /><Br />
Please note, you will be responsible for payment for any activities or tours you sign up for.<br />
<Br />
If you wish to reserve an activity after this date, you may do so at the hospitality desk on site or send your request to preferredpump2013@mcahawaii.com prior to arrival. Space will be based on availability.<br />
<Br />
<br />
To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities.<br />
<Br />
3. Select your tour. You will be asked for credit card information. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY OR TOUR.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab. <br />
<Br /><br /><Br />

We look forward to welcoming you to Paradise.<br />

			<br /><br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
