<%@ Page Language="C#" AutoEventWireup="true" CodeFile="completed.aspx.cs" Inherits="Completed" %>
<%@ import namespace="ABS" %>

<asp:content id="Main" contentplaceholderid="MainContent" runat="Server" >
	
	<script type="text/javascript">
		function printVoucher() {
			if ( document.getElementById != null ) {
				var html = "";
				var voucherPanel = document.getElementById( "voucherPanel" );
				if ( voucherPanel != null ) {
					html += voucherPanel.innerHTML;
				} else {
					window.print();
					return;
				}
				var printWin = window.open("","vouchers", 
					"height=100,width=100,top=0,left=0,directories=no,location=no,menubar=no,toolbar=no,status=no,resizeable=no" 
				);
				printWin.document.open();
				printWin.document.write(html);
				printWin.document.close();
				printWin.print();
				printWin.close();
			} else {
				window.print();
			}
		}
	</script>

	<div style="width:600px" >
		<font color="#ef5529" style="font-face: arial; font-size: 16pt;" face="Arial"><b>Thank You!</b></font><br /><br />
		<p class="instructions">
			Your itinerary has been processed and your payment has been successfully processed. <strong>Please print this page for your records and/or write down the Account Number that appears below. You will need the Account Number to access your order.<strong> 
		</p>
		<asp:panel runat="server" id="VoucherConfirmationMsg" visible="false">
			<p class="instructions">
				Your activity vouchers are shown below. 
				Please print the vouchers and be prepared to present the appropriate voucher 
				for admission to the activities you have purchased.
			</p>
			<p class="instructions">
				Copies of these vouchers have also been sent by email to the contact email-address that you provided.
			</p>
			<p>
				<input type="button" value="Print Vouchers" onclick="printVoucher(); return false;" class="StepButton" />
			</p>
		</asp:panel>
		<asp:panel runat="server" id="ResConfirmationMsg" visible="false">
			<p class="instructions">
				Your reservations are being processed and you will be notified via the contact telephone number or email address that you provided. If you are cancelling an activity prior to arriving onsite, please email wci2013@mcahawaii.com. All cancellations must be in writing.
			</p>
		</asp:panel>
		<p class="instructions">
			You can return to this site and view this itinerary by clicking the <b>My Account</b> link at the top of the page. 
			You will need the following information to log in to your account and view the itinerary: <br />
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email Address: <asp:label runat="server" id="EmailLoginLbl" font-bold="true" /><br />
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Number: <asp:label runat="server" id="AccountNoLbl" font-bold="true" /><br />
		</p>
		
	</div>

</asp:content>

<asp:content id="Vouchers" contentplaceholderid="FooterContent" runat="Server" >
	<div id="voucherPanel" style="width:500px" >
		<asp:literal runat="server" id="VoucherPanel" />
	</div>
</asp:content>
