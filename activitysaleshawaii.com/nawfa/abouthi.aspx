<%@ page language="C#" %>

<asp:content id="Content1" runat="server" contentplaceholderid="MainContent">
		<div style="float: left; margin: 0 20px 50px 0;">
		<img src="http://activitysaleshawaii.com/images/kilauea.jpg">
		</div>
		<div style="width: 500px; float: left;">
		<p class="PublisherGroupTitle">About Hawaii</p>
<p class="PublisherDescription">The Hawaiian Islands are one of the most beautiful places on earth. With its sweeping jade rainforests and shining golden beaches, it has aptly earned its nickname of Paradise. It was this pristine, unspoiled vision that greeted the first Polynesians as they arrived on Hawaii's shores over 500 years ago.<br />
<Br />
<b>About The Big Island</b><br>
It's easy to feel awed on Hawaii Island. From the molten magma flowing from Hawaii Volcanoes National Park to the snow-capped heights of Maunakea; from the green rainforests of the Hamakua Coast to the jet-black sands of Punaluu Beach; Hawaii Island is an unrivaled expression of the power of nature.
<br><br>
To avoid confusion with the name of the entire state, the Island of Hawaii is often called the "Big Island," and what an appropriate name it is. Nearly twice as big as all of the other Hawaiian Islands combined, its sheer size can be inspiring. You'll find all but two of the world's climatic zones within this island's shores.
<br><br>
The dramatic size and scope of the largest Hawaiian Island create a microcosm of environments and activities. On this island's vast tableau, you'll find everything from extravagant resorts and incredible golf courses to modest local towns and sacred Hawaiian historical sites, from the birthplace of King Kamehameha I to Hawaii's first missionary church in Historic Kailua Village (Kailua-Kona). With so much to see, it's best to experience the island in small pieces. There's plenty of room on Hawaii Island for your return.
<br><br>
			The average daytime temperature is 85 degrees F (29.4 C). Temperatures at night are approximately 10 degrees F. lower.
For travel to Hawaii, casual and lightweight clothing is recommended. Comfortable casual wear - shorts, sandals, comfortable shoes, bathing suits and cover-ups is the usual daytime attire. This attire should also be appropriate for recreation and sightseeing activities.<br />
			<br />
			<br>
			
		</p>
<p class="PublisherGroupTitle"> <br />

		</div>

</asp:content>
