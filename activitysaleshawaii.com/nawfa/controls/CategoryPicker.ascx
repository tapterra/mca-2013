<%@ control language="c#" autoeventwireup="true" codefile="CategoryPicker.ascx.cs" inherits="CategoryPicker" classname="CategoryPicker" %>

<asp:imagemap runat="server" id="CatPickerImage" 
 imageurl="/template/images/leftsidenavbar.gif"
 width="162" height="432" 
 hotspotmode="PostBack"
 onclick="HotSpotClicked"
>
	<asp:rectanglehotspot left="15" top="43"  right="151" bottom="62"  hotspotmode="Navigate" navigateurl="../abouthi.aspx" />
	<asp:rectanglehotspot left="22" top="91"  right="145" bottom="112" postbackvalue="leigreeting;" />
	<asp:rectanglehotspot left="36" top="66"  right="131" bottom="87"  postbackvalue="leigreeting;" />
	<asp:rectanglehotspot left="36" top="115" right="131" bottom="138" postbackvalue="activities;" />
	<asp:rectanglehotspot left="37" top="195" right="133" bottom="216" postbackvalue="spa;" /> 
	<asp:rectanglehotspot left="36" top="167" right="132" bottom="188" postbackvalue="golf;" />
	<asp:rectanglehotspot left="36" top="142" right="132" bottom="163" postbackvalue="dining;" />

	<asp:polygonhotspot coordinates="21,245,19,268,31,275,43,271,38,251,35,243"				postbackvalue=";kauai" />
	<asp:polygonhotspot coordinates="94,311,73,308,78,328,135,337,155,317,137,293,117,311"	postbackvalue=";bi" />
	<asp:polygonhotspot coordinates="101,266,109,287,131,286,144,262,99,260"				postbackvalue=";maui" />
	<asp:polygonhotspot coordinates="44,234,50,271,73,276,72,236,63,233"					postbackvalue=";oahu" />

</asp:imagemap> 
