using System;
using System.Web.UI.WebControls;

using ABS;

public partial class ActivitiesPublisher : System.Web.UI.UserControl {

	#region Properties

	public string IslandKey {
		get {
			object o = Session["CurrentIslandKey"];
			return ( o == null ? "oahu" : (string) o );
		}
		set {
			Session["CurrentIslandKey"] = value;
			// RKP: Begin Change - 20090205
			// RKP: Being anal, but we should dispose of any current Island
			this.island = null;
			// RKP: End Change
			this.Rebind();
		}
	}

	public Island Island {
		get {
			if ( this.island == null ) {
				this.island = new Island( this.IslandKey );
			}
			return ( this.island );
		}
	}
	private Island island;

	public string CategoryKey {
		get {
			object o = Session["CurrentCategoryKey"];
			return ( o == null ? "activities" : (string) o );
		}
		set {
			Session["CurrentCategoryKey"] = value;
			this.Rebind();
		}
	}

	public Category Category {
		get {
			if ( this.category == null ) {
				this.category = new Category( this.CategoryKey );
			}
			return ( this.category );
		}
	}
	private Category category;

	protected ActivityGroup SelectedGroup {
		get { return ( this.selGroup ); }
		set { this.selGroup = value; }
	}
	private ActivityGroup selGroup = null;

	#endregion

	#region Methods

	public void Rebind() {
		this.GroupDetailsPanel.Visible = false;
		this.SubCatRepeater.Visible = true;
		this.SubCatRepeater.DataSource = Subcategory.GetCatActivityGroups( this.Category, this.Island.ID );
		this.SubCatRepeater.DataBind();
	}

	// RKP: Begin Change - 20090205
	// RKP: This call to Rebind is redundant and causes the catalog to be loaded from the db twice
	//protected void Page_Load( object sender, EventArgs e ) {
	//  if ( !this.IsPostBack ) {
	//    this.Rebind();
	//  }
	//}
	// RKP: End Change

	// RKP: Begin Change - 20090206
	// RKP: Replacing the "MoreLink" link-button control with a plain old hyperlink + querystring
	// RKP: so that we can turn off the viewstate for the publisher's repeater to cut down on payload size.
	// RKP: To make this work, we replace the MoreInfoCommand method with a query-string handler in Page_Load.
	//protected void MoreInfoCommand( object sender, CommandEventArgs e ) {
	//  this.SelectedGroup = new ActivityGroup( Convert.ToInt32( e.CommandArgument ) );
	//  this.SubCatRepeater.Visible = false;
	//  this.GroupDetailsPanel.Visible = true;
	//  this.GroupDetailsPanel.DataBind();
	//}
	protected void Page_Load( object sender, EventArgs e ) {
		if ( !this.IsPostBack ) {
			string groupID = this.Request.QueryString["ID"];
			if ( string.IsNullOrEmpty( groupID ) ) {
				this.SubCatRepeater.Visible = true;
				this.GroupDetailsPanel.Visible = false;
			} else {
				this.SelectedGroup = new ActivityGroup( Convert.ToInt32( groupID ) );
				this.SubCatRepeater.Visible = false;
				this.GroupDetailsPanel.Visible = true;
				this.GroupDetailsPanel.DataBind();
			}
		}
	}
	// RKP: End Change

	#endregion

}
