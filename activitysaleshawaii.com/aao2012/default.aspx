<%@ page language="C#" %>


<asp:content id="Content1" runat="server" contentplaceholderid="MainContent" >
<img src="/images/oahurs.jpg" align=left style="margin-right:20px"/>
		<div style="width: 700px;"><p class="PublisherGroupTitle">Aloha American Association of Orthodontists Association</p>
<p class="PublisherDescription">Thank you for visiting the 112th Annual Session activity website.  Within this website, you will discover the exciting tours available on Oahu. <br /><br />From snorkeling with the Pacific's exotic marine life to dining over picture-perfect sunsets, Hawaii offers a rich variety of activities perfect for everyone. <br /><br />Feel free to browse this website and discover all of these activities for yourself. <Br /><Br />Please note: The website will be undergoing maintenance on Saturday, February 18 from 9:00 p.m. - 12:00 midnite (Hawaii Standard Time). Registration for activities will be temporarily unavailable during this time. We apologize for any inconvenience.<br><br>
Reservations will close on April 18, 2012 at 5:00 p.m. Hawaii Standard Time. Space is limited so make your reservations early.<br />
<Br />

To make a Reservation: <br />
<Br />1. Click on the 'Tours and Activities' link above.
<Br />
<Br />
2. Browse the tours and activities offered.<br />
<Br />
3. Select your tours. You will be asked for credit card information. Upon confirmation of an activity or tour, print the tour voucher and bring it with you to Hawaii.  YOU WILL NEED TO PRESENT YOUR PRINTED TOUR VOUCHER TO THE TOUR OPERATOR AT THE TIME OF YOUR ACTIVITY.<br />
<Br />
4. Should you need to review an activity, select the My Itinerary tab.  <br />
<Br />
5. If you need to review your order, select the My Account tab. <br />
<Br />
Let us know if we can be of service.  We look forward to welcoming you to Paradise.<br />

			<br /><br />
		</p>
<p class="PublisherGroupTitle">Mahalo!<br />

		</div>
	

</asp:content>
